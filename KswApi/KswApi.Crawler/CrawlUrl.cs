﻿using Abot.Crawler;
using Abot.Poco;
using KswApi.Interface.Objects;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net;
using System.Windows.Forms;

namespace KswApi.Crawler
{
    public partial class CrawlUrl : Form
    {
        private WebCrawler _crawler;
        private BackgroundWorker _worker;

        public CrawlUrl()
        {
            InitializeComponent();
        }

        private void CrawlUrl_Load(object sender, EventArgs e)
        {
            backgroundWorker1.WorkerReportsProgress = true;

            lblEnvironment.Text = "Currently pointed at: " + ClientFactory.GetEnvironment();
        }

        private void btnCrawl_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrWhiteSpace(txtCrawlUrl.Text))
            {
                MessageBox.Show("Please enter a valid URL", "Site to Crawl");

                return;
            }

            CrawlConfiguration config = new CrawlConfiguration
                {
                    IsExternalPageCrawlingEnabled = chkEnableExternalPageCrawling.Checked,
                    IsExternalPageLinksCrawlingEnabled = chkEnableExternalPageLinksCrawling.Checked,
                    UrlsToExclude = GetUrlExcludeList(),
                    UrlsToInclude = GetUrlIncludeList()
                };

            _crawler = new WebCrawler(config);

            backgroundWorker1.RunWorkerAsync(txtCrawlUrl.Text);
        }

        private List<string> GetUrlIncludeList()
        {
            List<string> result = new List<string>();

            if (lsUrlsToInclude.Items.Count == 0)
                return result;

            result.AddRange(from string item in lsUrlsToInclude.Items select item);

            return result;
        }

        private List<string> GetUrlExcludeList()
        {
            List<string> result = new List<string>();

            if (lsUrlsToExclude.Items.Count == 0)
                return result;

            result.AddRange(from string item in lsUrlsToExclude.Items select item);

            return result;
        }

        private void btnAddUrlToExclude_Click(object sender, EventArgs e)
        {
            lsUrlsToExclude.Items.Add(txtUrlToExclude.Text);

            txtUrlToExclude.Text = "";
        }

        private void btnAddUrlToInclude_Click(object sender, EventArgs e)
        {
            lsUrlsToInclude.Items.Add(txtUrlToInclude.Text);

            txtUrlToInclude.Text = "";
        }

        private void CrawlerProcessPageCrawlStarting(object sender, PageCrawlStartingArgs e)
        {
            PageToCrawl pageToCrawl = e.PageToCrawl;
            ShowUpdate(string.Format("About to crawl link {0} which was found on page {1}", pageToCrawl.Uri.AbsoluteUri, pageToCrawl.ParentUri.AbsoluteUri));
        }

        private void CrawlerProcessPageCrawlCompleted(object sender, PageCrawlCompletedArgs e)
        {
            CrawledPage crawledPage = e.CrawledPage;

            if (crawledPage.WebException != null || crawledPage.HttpWebResponse.StatusCode != HttpStatusCode.OK)
                ShowUpdate(string.Format("Crawl of page failed {0}", crawledPage.Uri.AbsoluteUri));
            else
            {
                ShowUpdate(string.Format("Crawl of page succeeded {0}.  Attempting to index...", crawledPage.Uri.AbsoluteUri));

                try
                {
                    ClientServices clientServices = ClientFactory.GetClient();
                    ExternalContent content = new ExternalContent
                        {
                            RawContent = crawledPage.RawContent,
                            Url = crawledPage.Uri.AbsoluteUri
                        };

                    clientServices.ExternalContent.IndexCrawledSite(content);

                    ShowUpdate("Content indexed successfully.");
                }
                catch (Exception ex)
                {
                    ShowUpdate("Error occured attempting to index content.  Error: " + ex.Message);
                }
            }

            if (string.IsNullOrEmpty(crawledPage.RawContent))
                ShowUpdate(string.Format("Page had no content {0}", crawledPage.Uri.AbsoluteUri));
        }

        private void CrawlerPageLinksCrawlDisallowed(object sender, PageLinksCrawlDisallowedArgs e)
        {
            CrawledPage crawledPage = e.CrawledPage;
            ShowUpdate(string.Format("Did not crawl the links on page {0} due to {1}", crawledPage.Uri.AbsoluteUri, e.DisallowedReason));
        }

        private void CrawlerPageCrawlDisallowed(object sender, PageCrawlDisallowedArgs e)
        {
            PageToCrawl pageToCrawl = e.PageToCrawl;
            ShowUpdate(string.Format("Did not crawl page {0} due to {1}", pageToCrawl.Uri.AbsoluteUri, e.DisallowedReason));
        }

        private void ShowUpdate(string update)
        {
            if (_worker.IsBusy)
                _worker.ReportProgress(0, update);
        }

        private void backgroundWorker1_DoWork(object sender, DoWorkEventArgs e)
        {
            _worker = (BackgroundWorker)sender;

            _crawler.PageCrawlStartingAsync += CrawlerProcessPageCrawlStarting;
            _crawler.PageCrawlCompletedAsync += CrawlerProcessPageCrawlCompleted;
            _crawler.PageCrawlDisallowedAsync += CrawlerPageCrawlDisallowed;
            _crawler.PageLinksCrawlDisallowedAsync += CrawlerPageLinksCrawlDisallowed;

            Uri urlToCrawl = new Uri(e.Argument.ToString());

            e.Result = _crawler.Crawl(urlToCrawl);
        }

        private void backgroundWorker1_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            txtResults.Text += e.UserState + "\r\n\r\n";
        }

        private void backgroundWorker1_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            CrawlResult result = (CrawlResult)e.Result;
            //Print some result data
            ShowUpdate(result.ErrorOccurred
                           ? string.Format("Crawl of {0} completed with error: {1}", result.RootUri.AbsoluteUri,
                                           result.ErrorMessage)
                           : string.Format("Crawl of {0} completed without error.", result.RootUri.AbsoluteUri));

            ShowUpdate(string.Format("Completed in {0}", result.Elapsed));
        }

        private void btnClearExcludeList_Click(object sender, EventArgs e)
        {
            lsUrlsToExclude.Items.Clear();
        }

        private void btnClearIncludeList_Click(object sender, EventArgs e)
        {
            lsUrlsToInclude.Items.Clear();
        }

    }
}
