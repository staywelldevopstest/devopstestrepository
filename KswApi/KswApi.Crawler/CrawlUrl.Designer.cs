﻿namespace KswApi.Crawler
{
    partial class CrawlUrl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.txtCrawlUrl = new System.Windows.Forms.TextBox();
            this.btnCrawl = new System.Windows.Forms.Button();
            this.txtResults = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.label3 = new System.Windows.Forms.Label();
            this.lsUrlsToExclude = new System.Windows.Forms.ListBox();
            this.txtUrlToExclude = new System.Windows.Forms.TextBox();
            this.btnAddUrlToExclude = new System.Windows.Forms.Button();
            this.btnClearExcludeList = new System.Windows.Forms.Button();
            this.btnClearIncludeList = new System.Windows.Forms.Button();
            this.btnAddUrlToInclude = new System.Windows.Forms.Button();
            this.txtUrlToInclude = new System.Windows.Forms.TextBox();
            this.lsUrlsToInclude = new System.Windows.Forms.ListBox();
            this.label4 = new System.Windows.Forms.Label();
            this.chkEnableExternalPageCrawling = new System.Windows.Forms.CheckBox();
            this.chkEnableExternalPageLinksCrawling = new System.Windows.Forms.CheckBox();
            this.lblEnvironment = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 274);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(69, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Site to Crawl:";
            // 
            // txtCrawlUrl
            // 
            this.txtCrawlUrl.Location = new System.Drawing.Point(87, 271);
            this.txtCrawlUrl.Name = "txtCrawlUrl";
            this.txtCrawlUrl.Size = new System.Drawing.Size(522, 20);
            this.txtCrawlUrl.TabIndex = 1;
            this.txtCrawlUrl.Text = "http://localhost/KswApi.Portal/html/testcontent.html";
            // 
            // btnCrawl
            // 
            this.btnCrawl.Location = new System.Drawing.Point(615, 269);
            this.btnCrawl.Name = "btnCrawl";
            this.btnCrawl.Size = new System.Drawing.Size(75, 23);
            this.btnCrawl.TabIndex = 2;
            this.btnCrawl.Text = "Crawl";
            this.btnCrawl.UseVisualStyleBackColor = true;
            this.btnCrawl.Click += new System.EventHandler(this.btnCrawl_Click);
            // 
            // txtResults
            // 
            this.txtResults.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtResults.Location = new System.Drawing.Point(15, 329);
            this.txtResults.Multiline = true;
            this.txtResults.Name = "txtResults";
            this.txtResults.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtResults.Size = new System.Drawing.Size(675, 334);
            this.txtResults.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 313);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Results";
            // 
            // backgroundWorker1
            // 
            this.backgroundWorker1.DoWork += new System.ComponentModel.DoWorkEventHandler(this.backgroundWorker1_DoWork);
            this.backgroundWorker1.ProgressChanged += new System.ComponentModel.ProgressChangedEventHandler(this.backgroundWorker1_ProgressChanged);
            this.backgroundWorker1.RunWorkerCompleted += new System.ComponentModel.RunWorkerCompletedEventHandler(this.backgroundWorker1_RunWorkerCompleted);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(82, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Urls To Exclude";
            // 
            // lsUrlsToExclude
            // 
            this.lsUrlsToExclude.FormattingEnabled = true;
            this.lsUrlsToExclude.Items.AddRange(new object[] {
            "http://yahoo.com/",
            "http://cnn.com/"});
            this.lsUrlsToExclude.Location = new System.Drawing.Point(15, 38);
            this.lsUrlsToExclude.Name = "lsUrlsToExclude";
            this.lsUrlsToExclude.Size = new System.Drawing.Size(287, 121);
            this.lsUrlsToExclude.TabIndex = 7;
            // 
            // txtUrlToExclude
            // 
            this.txtUrlToExclude.Location = new System.Drawing.Point(15, 168);
            this.txtUrlToExclude.Name = "txtUrlToExclude";
            this.txtUrlToExclude.Size = new System.Drawing.Size(206, 20);
            this.txtUrlToExclude.TabIndex = 8;
            // 
            // btnAddUrlToExclude
            // 
            this.btnAddUrlToExclude.Location = new System.Drawing.Point(227, 166);
            this.btnAddUrlToExclude.Name = "btnAddUrlToExclude";
            this.btnAddUrlToExclude.Size = new System.Drawing.Size(75, 23);
            this.btnAddUrlToExclude.TabIndex = 9;
            this.btnAddUrlToExclude.Text = "Add";
            this.btnAddUrlToExclude.UseVisualStyleBackColor = true;
            this.btnAddUrlToExclude.Click += new System.EventHandler(this.btnAddUrlToExclude_Click);
            // 
            // btnClearExcludeList
            // 
            this.btnClearExcludeList.Location = new System.Drawing.Point(227, 12);
            this.btnClearExcludeList.Name = "btnClearExcludeList";
            this.btnClearExcludeList.Size = new System.Drawing.Size(75, 23);
            this.btnClearExcludeList.TabIndex = 10;
            this.btnClearExcludeList.Text = "Clear";
            this.btnClearExcludeList.UseVisualStyleBackColor = true;
            this.btnClearExcludeList.Click += new System.EventHandler(this.btnClearExcludeList_Click);
            // 
            // btnClearIncludeList
            // 
            this.btnClearIncludeList.Location = new System.Drawing.Point(615, 12);
            this.btnClearIncludeList.Name = "btnClearIncludeList";
            this.btnClearIncludeList.Size = new System.Drawing.Size(75, 23);
            this.btnClearIncludeList.TabIndex = 15;
            this.btnClearIncludeList.Text = "Clear";
            this.btnClearIncludeList.UseVisualStyleBackColor = true;
            this.btnClearIncludeList.Click += new System.EventHandler(this.btnClearIncludeList_Click);
            // 
            // btnAddUrlToInclude
            // 
            this.btnAddUrlToInclude.Location = new System.Drawing.Point(615, 166);
            this.btnAddUrlToInclude.Name = "btnAddUrlToInclude";
            this.btnAddUrlToInclude.Size = new System.Drawing.Size(75, 23);
            this.btnAddUrlToInclude.TabIndex = 14;
            this.btnAddUrlToInclude.Text = "Add";
            this.btnAddUrlToInclude.UseVisualStyleBackColor = true;
            this.btnAddUrlToInclude.Click += new System.EventHandler(this.btnAddUrlToInclude_Click);
            // 
            // txtUrlToInclude
            // 
            this.txtUrlToInclude.Location = new System.Drawing.Point(403, 168);
            this.txtUrlToInclude.Name = "txtUrlToInclude";
            this.txtUrlToInclude.Size = new System.Drawing.Size(206, 20);
            this.txtUrlToInclude.TabIndex = 13;
            // 
            // lsUrlsToInclude
            // 
            this.lsUrlsToInclude.FormattingEnabled = true;
            this.lsUrlsToInclude.Items.AddRange(new object[] {
            "http://localhost/KswApi.Portal/html/MoreTestContent.html"});
            this.lsUrlsToInclude.Location = new System.Drawing.Point(403, 38);
            this.lsUrlsToInclude.Name = "lsUrlsToInclude";
            this.lsUrlsToInclude.Size = new System.Drawing.Size(287, 121);
            this.lsUrlsToInclude.TabIndex = 12;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(400, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(79, 13);
            this.label4.TabIndex = 11;
            this.label4.Text = "Urls To Include";
            // 
            // chkEnableExternalPageCrawling
            // 
            this.chkEnableExternalPageCrawling.AutoSize = true;
            this.chkEnableExternalPageCrawling.Location = new System.Drawing.Point(15, 207);
            this.chkEnableExternalPageCrawling.Name = "chkEnableExternalPageCrawling";
            this.chkEnableExternalPageCrawling.Size = new System.Drawing.Size(171, 17);
            this.chkEnableExternalPageCrawling.TabIndex = 16;
            this.chkEnableExternalPageCrawling.Text = "Enable External Page Crawling";
            this.chkEnableExternalPageCrawling.UseVisualStyleBackColor = true;
            // 
            // chkEnableExternalPageLinksCrawling
            // 
            this.chkEnableExternalPageLinksCrawling.AutoSize = true;
            this.chkEnableExternalPageLinksCrawling.Location = new System.Drawing.Point(14, 230);
            this.chkEnableExternalPageLinksCrawling.Name = "chkEnableExternalPageLinksCrawling";
            this.chkEnableExternalPageLinksCrawling.Size = new System.Drawing.Size(199, 17);
            this.chkEnableExternalPageLinksCrawling.TabIndex = 17;
            this.chkEnableExternalPageLinksCrawling.Text = "Enable External Page Links Crawling";
            this.chkEnableExternalPageLinksCrawling.UseVisualStyleBackColor = true;
            // 
            // lblEnvironment
            // 
            this.lblEnvironment.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lblEnvironment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblEnvironment.ForeColor = System.Drawing.Color.Red;
            this.lblEnvironment.Location = new System.Drawing.Point(87, 295);
            this.lblEnvironment.Name = "lblEnvironment";
            this.lblEnvironment.Size = new System.Drawing.Size(603, 20);
            this.lblEnvironment.TabIndex = 18;
            this.lblEnvironment.Text = "Currently pointed at:";
            this.lblEnvironment.TextAlign = System.Drawing.ContentAlignment.TopRight;
            // 
            // CrawlUrl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(702, 675);
            this.Controls.Add(this.lblEnvironment);
            this.Controls.Add(this.chkEnableExternalPageLinksCrawling);
            this.Controls.Add(this.chkEnableExternalPageCrawling);
            this.Controls.Add(this.btnClearIncludeList);
            this.Controls.Add(this.btnAddUrlToInclude);
            this.Controls.Add(this.txtUrlToInclude);
            this.Controls.Add(this.lsUrlsToInclude);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.btnClearExcludeList);
            this.Controls.Add(this.btnAddUrlToExclude);
            this.Controls.Add(this.txtUrlToExclude);
            this.Controls.Add(this.lsUrlsToExclude);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtResults);
            this.Controls.Add(this.btnCrawl);
            this.Controls.Add(this.txtCrawlUrl);
            this.Controls.Add(this.label1);
            this.Name = "CrawlUrl";
            this.Text = "Crawl Site";
            this.Load += new System.EventHandler(this.CrawlUrl_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtCrawlUrl;
        private System.Windows.Forms.Button btnCrawl;
        private System.Windows.Forms.TextBox txtResults;
        private System.Windows.Forms.Label label2;
        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListBox lsUrlsToExclude;
        private System.Windows.Forms.TextBox txtUrlToExclude;
        private System.Windows.Forms.Button btnAddUrlToExclude;
        private System.Windows.Forms.Button btnClearExcludeList;
        private System.Windows.Forms.Button btnClearIncludeList;
        private System.Windows.Forms.Button btnAddUrlToInclude;
        private System.Windows.Forms.TextBox txtUrlToInclude;
        private System.Windows.Forms.ListBox lsUrlsToInclude;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkEnableExternalPageCrawling;
        private System.Windows.Forms.CheckBox chkEnableExternalPageLinksCrawling;
        private System.Windows.Forms.Label lblEnvironment;
    }
}

