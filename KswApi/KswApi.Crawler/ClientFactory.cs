﻿
namespace KswApi.Crawler
{
    public static class ClientFactory
    {
        private const string WEB_PORTAL_CLIENT_ID = "29eeff76-85d0-4f70-94ed-a49072524692";
        private const string CLIENT_SECRET = "MxSVFBGt0W5EP-EXhAHZstWidOFSyyy.yC.R3opR4dsHboGycTy9aANpShQuybGp";
        private const string SERVICE_URI = "http://10.10.20.158/kswapi";
        private const string AUTH_SERVER_URL = "http://10.10.20.158/KswApi.Portal";

        public static ClientServices GetClient()
        {
            return new ClientServices(AUTH_SERVER_URL, SERVICE_URI, WEB_PORTAL_CLIENT_ID, CLIENT_SECRET);
        }

        public static string GetEnvironment()
        {
            return SERVICE_URI;
        }
    }
}
