﻿using System.Collections.Generic;

namespace KswApi.Reflection.Objects
{
	public class ServiceInfo
	{
		public string CodeName { get; set; }
		public string ServiceName { get; set; }
		public string UriSignature { get; set; }
		public List<OperationInfo> Operations { get; set; }
		public List<ServiceInfo> Services { get; set; }
	}
}
