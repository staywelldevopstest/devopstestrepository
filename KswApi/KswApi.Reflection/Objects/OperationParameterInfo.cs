﻿using System;

namespace KswApi.Reflection.Objects
{
	public class OperationParameterInfo
	{
		public string CodeName { get; set; }
		public string WebName { get; set; }
		public Type Type { get; set; }
	}
}
