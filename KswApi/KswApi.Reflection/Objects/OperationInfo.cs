﻿using System.Collections.Generic;
using System.Reflection;

namespace KswApi.Reflection.Objects
{
	public class OperationInfo
	{
		public OperationInfo()
		{
			Values = new Dictionary<string, object>();
			Parameters = new List<OperationParameterInfo>();
		}

		public string Verb { get; set; }
		public string MethodName { get; set; }
		public string MethodSignature { get; set; }
		public string UriSignature { get; set; }
		public MethodInfo Method { get; set; }
		public Dictionary<string, object> Values { get; private set; }
		public List<OperationParameterInfo> Parameters { get; private set; }
	}
}
