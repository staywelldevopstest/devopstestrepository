﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using KswApi.Reflection.Framework;

namespace KswApi.Reflection
{
    public static class Reflector
    {
		public static string GetPath<T>(Expression<Func<T, object>> propertyExpression, string separator)
		{
			PropertyPathVisitor<T> visitor = new PropertyPathVisitor<T>();
			return string.Join(separator, visitor.Visit(propertyExpression));
		}

		public static IEnumerable<string> GetPath<T>(Expression<Func<T, object>> propertyExpression)
		{
			PropertyPathVisitor<T> visitor = new PropertyPathVisitor<T>();
			return visitor.Visit(propertyExpression);
		}

		public static IEnumerable<string> GetPath(Expression propertyExpression)
		{
			PropertyPathVisitor visitor = new PropertyPathVisitor();
			return visitor.Visit(propertyExpression);
		}

    }
}
