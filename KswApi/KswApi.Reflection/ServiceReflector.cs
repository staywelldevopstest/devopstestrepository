﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Reflection.Objects;

namespace KswApi.Reflection
{
	public class ServiceReflector
	{
		public ServiceInfo ReflectService(Type type, string name = null)
		{
			if (string.IsNullOrEmpty(name))
				name = type.Name;

			return ReflectServiceRecursively(string.Empty, name, type);
		}

		public OperationInfo GetOperation(MethodInfo method, string baseName = null)
		{
			string verb = GetVerb(method);

			if (string.IsNullOrEmpty(verb))
				return null;

			return CreateOperation(method, baseName, verb);
		}

		private ServiceInfo ReflectServiceRecursively(string baseName, string name, Type type)
		{
			ServiceInfo serviceInfo = new ServiceInfo
				                          {
											  CodeName = name,
											  ServiceName = GetUriBase(type),
											  Operations = new List<OperationInfo>(),
											  Services = new List<ServiceInfo>()
				                          };

			PropertyInfo[] properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty);

			IEnumerable<PropertyInfo> sortedProperties = properties.OrderBy(item => item.Name);

			foreach (PropertyInfo property in sortedProperties)
			{
				if (property.PropertyType.IsPrimitive || property.PropertyType.IsValueType)
					continue;

				IgnoreDataMemberAttribute ignoreAttribute = (IgnoreDataMemberAttribute) Attribute.GetCustomAttribute(property, typeof (IgnoreDataMemberAttribute));
				if (ignoreAttribute != null)
					continue;

				serviceInfo.Services.Add(ReflectServiceRecursively(
					string.IsNullOrEmpty(baseName) ? property.Name : baseName + '.' + property.Name,
					property.Name, property.PropertyType));
			}

			MethodInfo[] methods = type.GetMethods(BindingFlags.Public | BindingFlags.Instance);

			IEnumerable<MethodInfo> sortedMethods = methods.OrderBy(item => item.Name);

			foreach (MethodInfo method in sortedMethods)
			{
				if (method.IsSpecialName)
					continue;

				OperationInfo operation = GetOperation(method, baseName);

				if (operation == null)
					continue;

				serviceInfo.Operations.Add(operation);
			}

			return serviceInfo;
		}

		private OperationInfo CreateOperation(MethodInfo method, string baseName, string verb)
		{
			OperationInfo operation = new OperationInfo
				       {
						   Method = method,
						   MethodName = method.Name,
						   MethodSignature = string.IsNullOrEmpty(baseName) ? method.Name : baseName + '.' + method.Name,
						   UriSignature = GetUriSignature(method),
						   Verb = verb
				       };

			ParameterInfo[] parameters = method.GetParameters();

			foreach (ParameterInfo parameter in parameters)
			{
				string webName = parameter.Name;

				MessageParameterAttribute attribute =
					(MessageParameterAttribute) Attribute.GetCustomAttribute(parameter, typeof (MessageParameterAttribute));

				if (attribute != null && !string.IsNullOrEmpty(attribute.Name))
					webName = attribute.Name;

				operation.Parameters.Add(new OperationParameterInfo
					                         {
						                         CodeName = parameter.Name,
												 WebName = webName,
												 Type = parameter.ParameterType
					                         });
			}

			return operation;
		}

		private string GetUriSignature(MethodInfo method)
		{
			string operationName = GetOperationName(method);

			if (operationName.StartsWith("/"))
				return operationName;

			string uriBase = GetUriBase(method);

			if (string.IsNullOrEmpty(operationName))
				return uriBase;

			return uriBase + '/' + operationName;
		}

		private string GetUriBase(Type type)
		{
			ServiceContractAttribute contract = (ServiceContractAttribute)Attribute.GetCustomAttribute(type, typeof(ServiceContractAttribute));

			if (contract == null)
			{
				foreach (Type baseType in type.GetInterfaces())
				{
					contract = (ServiceContractAttribute)Attribute.GetCustomAttribute(baseType, typeof(ServiceContractAttribute));

					if (contract != null)
						break;
				}
			}

			if (contract == null)
				return type.Name;

			return contract.Name;
		}

		private string GetUriBase(MethodInfo method)
		{
			return GetUriBase(method.DeclaringType);
		}

		private string GetOperationName(MethodInfo method)
		{
			string template = GetUriTemplate(method);

			if (template != null)
				return template;

			return method.Name;
		}

		private string GetUriTemplate(MethodInfo method)
		{
			WebInvokeAttribute webInvoke = (WebInvokeAttribute) Attribute.GetCustomAttribute(method, typeof (WebInvokeAttribute));

			if (webInvoke != null && webInvoke.UriTemplate != null)
				return webInvoke.UriTemplate;

			WebGetAttribute webGet = (WebGetAttribute) Attribute.GetCustomAttribute(method, typeof(WebGetAttribute));
			
			if (webGet != null && webGet.UriTemplate != null)
				return webGet.UriTemplate;

			return null;
		}

		private string GetVerb(MethodInfo method)
		{
			WebInvokeAttribute webInvoke = (WebInvokeAttribute)Attribute.GetCustomAttribute(method, typeof(WebInvokeAttribute));

			if (webInvoke != null)
				return string.IsNullOrEmpty(webInvoke.Method) ? "POST" : webInvoke.Method;

			WebGetAttribute webGet = (WebGetAttribute)Attribute.GetCustomAttribute(method, typeof(WebGetAttribute));

			if (webGet != null)
				return "GET";

			return null;
		}

	}
}
