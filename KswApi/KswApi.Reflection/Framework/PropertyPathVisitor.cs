﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace KswApi.Reflection.Framework
{
	internal class PropertyPathVisitor<T>
	{
		private List<string> _list;

		public IEnumerable<string> Visit(Expression<Func<T, object>> expression)
		{
			_list = new List<string>();

			VisitExpression(expression.Body);

			return _list;
		}

		#region Private Methods

		private void VisitExpression(Expression expression)
		{
			switch (expression.NodeType)
			{
				case ExpressionType.MemberAccess:
					MemberExpression memberExpression = (MemberExpression)expression;
					if (memberExpression.Expression != null && memberExpression.Expression.NodeType != ExpressionType.Parameter)
						VisitExpression(memberExpression.Expression);
					_list.Add(memberExpression.Member.Name);
					break;
				case ExpressionType.Convert:
					VisitExpression(((UnaryExpression) expression).Operand);
					break;
				default:
					throw new ArgumentException("Expression did not refer to a property.");
			}
		}

		#endregion
	}

	internal class PropertyPathVisitor
	{
		private List<string> _list;

		public IEnumerable<string> Visit(Expression expression)
		{
			LambdaExpression lambda = expression as LambdaExpression;
			
			if (lambda == null)
				throw new ArgumentException("Expression did not refer to a property.");
			
			_list = new List<string>();

			VisitExpression(lambda.Body);

			return _list;
		}

		private void VisitExpression(Expression expression)
		{
			switch (expression.NodeType)
			{
				case ExpressionType.MemberAccess:
					MemberExpression memberExpression = (MemberExpression) expression;
					if (memberExpression.Expression != null && memberExpression.Expression.NodeType != ExpressionType.Parameter)
						VisitExpression(memberExpression.Expression);
					_list.Add(memberExpression.Member.Name);
					break;
				case ExpressionType.Convert:
					VisitExpression(((UnaryExpression) expression).Operand);
					break;
				default:
					throw new ArgumentException("Expression did not refer to a property.");
			}
		}
	}
}
