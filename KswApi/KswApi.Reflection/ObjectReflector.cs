﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace KswApi.Reflection
{
	public class ObjectReflector
	{
		private const int NUMBER_OF_ELEMENTS_IN_LISTS = 2;
		private const int MAXIMUM_NESTING = 2;
		private const int DEFAULT_ENUM_OFFSET = 1;
		private int _listPosition;
		private readonly Dictionary<Type, int> _loopDetector = new Dictionary<Type, int>();

		private ObjectReflector()
		{

		}

		public static object Instantiate(Type type)
		{
			ObjectReflector reflector = new ObjectReflector();

			return reflector.Create(type);
		}

		private object Create(Type type)
		{
			if (type == typeof(void))
				return null;

			if (type.IsAbstract || type.IsInterface)
				return null;

			if (type.IsEnum)
				return CreateEnum(type);

			TypeCode code = Type.GetTypeCode(type);

			switch (code)
			{
				case TypeCode.Empty:
					return null;
				case TypeCode.Object:
					return CreateObject(type);
				case TypeCode.DBNull:
					return null;
				case TypeCode.Boolean:
					return true;
				case TypeCode.Char:
					return 'x';
				case TypeCode.SByte:
				case TypeCode.Byte:
				case TypeCode.Int16:
				case TypeCode.UInt16:
				case TypeCode.Int32:
				case TypeCode.UInt32:
				case TypeCode.Int64:
				case TypeCode.UInt64:
					return 7;
				case TypeCode.Single:
				case TypeCode.Double:
				case TypeCode.Decimal:
					return 7.7;
				case TypeCode.DateTime:
					return DateTime.UtcNow;
				case TypeCode.String:
					return "Value";
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private object CreateEnum(Type type)
		{
			Array array = Enum.GetValues(type);

			if (array.Length == 0)
				return 0;

			int offset = DEFAULT_ENUM_OFFSET + _listPosition;

			if (offset < array.Length)
				return array.GetValue(offset);

			return array.GetValue(array.Length - 1);
		}

		private object CreateObject(Type type)
		{
			if (type == typeof(Guid))
				return Guid.NewGuid();

			if (type == typeof(TimeSpan))
				return TimeSpan.FromSeconds(.1234567);

			if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
				return Create(Nullable.GetUnderlyingType(type));

			if (typeof(ICollection).IsAssignableFrom(type))
			{
				// handle any collection classes here
				Type listInterface = type.GetInterfaces().FirstOrDefault(
						item => item.IsGenericType && item.GetGenericTypeDefinition() == typeof(IList<>));

				if (listInterface != null)
					return CreateListObject(type, listInterface);

				Type dictionaryInterface =
					type.GetInterfaces().FirstOrDefault(
						item => item.IsGenericType && item.GetGenericTypeDefinition() == typeof (IDictionary<,>));

				if (dictionaryInterface != null)
					return CreateDictionaryObject(type, dictionaryInterface);
			}

			if (typeof(Stream).IsAssignableFrom(type))
				return null;

			return CreateClassObject(type);
		}

		private object CreateClassObject(Type type)
		{
			int loop;
			if (_loopDetector.TryGetValue(type, out loop) && loop >= MAXIMUM_NESTING)
				return null;

			loop++;
			_loopDetector[type] = loop;

			ConstructorInfo constructor = type.GetConstructor(Type.EmptyTypes);

			if (constructor == null)
				throw new InvalidOperationException(string.Format("Type {0} has no public parameterless constructor.", type.Name));

			object obj = constructor.Invoke(null);

			if (obj == null)
				return null;

			PropertyInfo[] properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

			foreach (PropertyInfo property in properties)
			{
				if (!property.CanWrite)
					continue;
				
				object propertyObject = Create(property.PropertyType);

				if (propertyObject == null)
					continue;

				property.SetValue(obj, propertyObject, null);
			}

			loop--;
			if (loop == 0)
				_loopDetector.Remove(type);
			else
				_loopDetector[type] = loop;

			return obj;
		}

		private object CreateListObject(Type type, Type listInterface)
		{
			Type genericType = listInterface.GetGenericArguments()[0];

			int loop;
			if (_loopDetector.TryGetValue(genericType, out loop) && loop >= MAXIMUM_NESTING)
				return null;

			ConstructorInfo constructor = type.GetConstructor(Type.EmptyTypes);

			if (constructor == null)
				throw new InvalidOperationException(string.Format("Type {0} has no public parameterless constructor.", type.Name));

			IList result = (IList)constructor.Invoke(null);

			int position = _listPosition;

			for (int i = 0; i < NUMBER_OF_ELEMENTS_IN_LISTS; i++)
			{
				_listPosition = i;
				result.Add(Create(genericType));
			}

			_listPosition = position;

			return result;
		}

		private object CreateDictionaryObject(Type type, Type listInterface)
		{
			Type[] types = listInterface.GetGenericArguments();
			Type genericTypeKey = types[0];
			Type genericTypeValue = types[1];

			int loop;
			if (_loopDetector.TryGetValue(genericTypeKey, out loop) && loop >= MAXIMUM_NESTING)
				return null;

			ConstructorInfo constructor = type.GetConstructor(Type.EmptyTypes);

			if (constructor == null)
				throw new InvalidOperationException(string.Format("Type {0} has no public parameterless constructor.", type.Name));

			IDictionary result = (IDictionary)constructor.Invoke(null);

			int position = _listPosition;

			for (int i = 0; i < NUMBER_OF_ELEMENTS_IN_LISTS; i++)
			{
				_listPosition = i;
				result.Add(Create(genericTypeKey), Create(genericTypeValue));
			}

			_listPosition = position;

			return result;
		}
	}
}
