﻿using System;
using KswApi.Common.Interfaces;
using ServiceStack.Redis;

namespace KswApi.Caching.Caches
{
	public class RedisCache : ICache
	{
		private readonly PooledRedisClientManager _cache;
		
		public RedisCache(params string[] servers)
		{
			_cache = new PooledRedisClientManager(servers);
		}

		public void Clear()
		{
			_cache.FlushAll();
		}

		public void Set<T>(string key, T value)
		{
			_cache.Set(key, value);
		}

		public void Set<T>(string key, T value, TimeSpan expiration)
		{
			_cache.Set(key, value, expiration);
		}

		public T Get<T>(string key)
		{
			return _cache.Get<T>(key);
		}

		public void Remove(string key)
		{
			_cache.Remove(key);
		}
	}
}
