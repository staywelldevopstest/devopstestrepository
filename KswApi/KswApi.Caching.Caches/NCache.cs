﻿using System;
using Alachisoft.NCache.Web.Caching;
using KswApi.Common.Interfaces;

namespace KswApi.Caching.Caches
{
	public class NCache : ICache, IDisposable
	{
		private readonly Cache _cache;

		public NCache(string cacheName)
		{
			_cache = Alachisoft.NCache.Web.Caching.NCache.InitializeCache(cacheName);
		}

		public void Clear()
		{
			_cache.Clear();
		}

		public void Set<T>(string key, T value)
		{
			_cache.Insert(key, value);
		}

		public void Set<T>(string key, T value, TimeSpan expiration)
		{
			CacheItem cacheItem = new CacheItem(value)
			                      	{
			                      		SlidingExpiration = expiration
			                      	};

			_cache.Insert(key, cacheItem);
		}

		public T Get<T>(string key)
		{
			return (T) _cache.Get(key);
		}

		public void Remove(string key)
		{
			_cache.Remove(key);
		}

		public void Dispose()
		{
			_cache.Dispose();
		}
	}
}
