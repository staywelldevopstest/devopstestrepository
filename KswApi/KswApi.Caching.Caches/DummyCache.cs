﻿using System;
using KswApi.Common.Interfaces;

namespace KswApi.Caching.Caches
{
	/// <summary>
	/// This is an ICache implementation that makes
	/// all calls go to the database, solves issues
	/// where the cache can not be initialized.
	/// </summary>
	public class DummyCache : ICache
	{
		public void Clear()
		{
		}

		public void Set<T>(string key, T value)
		{
		}

		public void Set<T>(string key, T value, TimeSpan expiration)
		{
		}

		public T Get<T>(string key)
		{
			return default(T);
		}

		public void Remove(string key)
		{
		}
	}
}
