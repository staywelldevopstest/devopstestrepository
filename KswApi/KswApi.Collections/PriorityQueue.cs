﻿using System;
using System.Collections.Generic;

namespace KswApi.Collections
{
	public class PriorityQueue<T>
	{
		private const string EMPTY_QUEUE = "The queue is empty.";

		private readonly List<T> _list = new List<T>();
		private IComparer<T> _comparer = Comparer<T>.Default;

		public IComparer<T> Comparer
		{
			get { return _comparer; }
			set
			{
				if (value == null)
					throw new ArgumentNullException();
				_comparer = value;
			}
		}

		public void Push(T item)
		{
			int current = _list.Count;
			
			_list.Add(item);
			
			while (true)
			{
				if (current == 0)
				{
					_list[0] = item;
					return;
				}

				int parent = (current - 1) / 2;

				T parentItem = _list[parent];

				int compare = _comparer.Compare(item, parentItem);

				if (compare >= 0)
				{
					_list[current] = item;
					return;
				}

				_list[current] = parentItem;

				current = parent;
			}
		}

		public void Clear()
		{
			_list.Clear();
		}

		public bool Remove(Predicate<T> match)
		{
			int location = _list.FindIndex(match);

			if (location < 0)
				return false;

			RemoveAt(location);

			return true;
		}

		public int RemoveAll(Predicate<T> match)
		{
			for (int count = 0; ; count++)
			{
				int location = _list.FindIndex(match);

				if (location < 0)
					return count;

				RemoveAt(location);
			}
		}

		public bool Remove(T item)
		{
			int location = _list.IndexOf(item);
			
			if (location < 0)
				return false;

			RemoveAt(location);

			return true;
		}

		public T Pop()
		{
			T item;
			if (!TryPop(out item))
				throw new InvalidOperationException(EMPTY_QUEUE);
			return item;
		}

		public bool TryPop(out T item)
		{
			int count = _list.Count;

			if (count == 0)
			{
				item = default(T);
				return false;
			}

			item = _list[0];

			count--;

			T currentItem = _list[count];

			_list.RemoveAt(count);

			if (count == 0)
				return true;

			int current = 0;

			while (true)
			{
				int child = current * 2 + 1;

				if (child >= count)
				{
					_list[current] = currentItem;
					return true;
				}

				T childItem = _list[child];

				int child2 = child + 1;
				int compare;

				if (child2 < count)
				{
					T childItem2 = _list[child2];
					compare = _comparer.Compare(childItem, childItem2);

					if (compare > 0)
					{
						child = child2;
						childItem = childItem2;
					}
				}

				compare = _comparer.Compare(currentItem, childItem);

				if (compare <= 0)
				{
					_list[current] = currentItem;
					return true;
				}

				_list[current] = childItem;

				current = child;
			}
		}

		public T Peek()
		{
			if (_list.Count == 0)
				throw new InvalidOperationException(EMPTY_QUEUE);

			return _list[0];
		}

		public bool TryPeek(out T item)
		{
			if (_list.Count == 0)
			{
				item = default(T);
				return false;
			}

			item = _list[0];
			return true;
		}

		public int Count
		{
			get { return _list.Count; }
		}

		#region Private Methods
		public void RemoveAt(int location)
		{
			int end = _list.Count - 1;
			
			T item = _list[end];

			_list.RemoveAt(end);

			if (end == 0 || end == location)
				return;

			int current = location;

			// rebalance above
			while (true)
			{
				if (current == 0)
				{
					_list[0] = item;
					return;
				}

				int parent = (current - 1) / 2;

				T parentItem = _list[parent];

				int compare = _comparer.Compare(item, parentItem);

				if (compare >= 0)
				{
					break;
				}

				_list[current] = parentItem;

				current = parent;
			}

			// rebalance below
			while (true)
			{
				int child = current * 2 + 1;

				if (child >= end)
				{
					_list[current] = item;
					return;
				}

				T childItem = _list[child];

				int child2 = child + 1;
				int compare;

				if (child2 < end)
				{
					T childItem2 = _list[child2];
					compare = _comparer.Compare(childItem, childItem2);

					if (compare > 0)
					{
						child = child2;
						childItem = childItem2;
					}
				}

				compare = _comparer.Compare(item, childItem);

				if (compare <= 0)
				{
					_list[current] = item;
					return;
				}

				_list[current] = childItem;

				current = child;
			}
		}

		#endregion
	}
}
