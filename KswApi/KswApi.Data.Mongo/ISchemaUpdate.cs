﻿using MongoDB.Driver;

namespace KswApi.Data.Mongo
{
	public interface ISchemaUpdate
	{
		bool Update(MongoDatabase database);
		string Description { get; }
	}
}
