﻿using KswApi.Data.Mongo.DatabaseUpdates;
using KswApi.Data.Mongo.Framework;

namespace KswApi.Data.Mongo
{
    public class SchemaUpdater
    {
        public void UpdateSchema()
        {
            SchemaUpdateRegistrar worker = new SchemaUpdateRegistrar();

            worker.RegisterUpdate<ConvertDateTimesToStringsUpdate>();
            worker.RegisterUpdate<AddKeywordToSmsNumberDataUpdate>();
            worker.RegisterUpdate<AddLicenseIdToApplicationUpdate>();
            worker.RegisterUpdate<SmsNumberDataRouteTypeEnumUpdate>();
            worker.RegisterUpdate<ConvertAccessTokenRightsToList>();
            worker.RegisterUpdate<AddCaseInsensitiveClientName>();
            worker.RegisterUpdate<ClientUserStateUpdate>();
			worker.RegisterUpdate<RemoveLegacyContentBodyUpdate>();
			worker.RegisterUpdate<UpdateIcd10TaxonomyToIncludeDash>();
            worker.RegisterUpdate<UpdateBucketSegmentSlugsToLowercase>();
        }
    }
}
