﻿namespace KswApi.Data.Mongo.Constants
{
    class Constant
    {
        public const string MainConnectionStringName = "KswApi";
        public const string TestConnectionStringName = "KswApi";
        public const string LogConnectionStringName = "KswApi";
        public const string ClientManagementConnectionStringName = "KswApi";

        public const string MongoId = "_id";
    }
}
