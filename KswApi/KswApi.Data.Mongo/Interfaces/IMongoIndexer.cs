﻿using System;
using System.Linq.Expressions;
using KswApi.Data.Mongo.Framework;

namespace KswApi.Data.Mongo.Interfaces
{
	internal interface IMongoIndexer<T>
	{
		void EnsureIndex(IndexDirection direction, params Expression<Func<T, object>>[] memberExpressions);
		void EnsureIndex(params MultilevelExpression<T>[] expressions);
		void EnsureIndex(params IndexItem<T>[] memberExpressions);

		void EnsureUnique(IndexDirection direction, params Expression<Func<T, object>>[] memberExpressions);
		void EnsureUnique(params MultilevelExpression<T>[] expressions);
		void EnsureUnique(params IndexItem<T>[] memberExpressions);

		void RemoveIndex(IndexDirection direction, params Expression<Func<T, object>>[] memberExpressions);
		void RemoveIndex(params MultilevelExpression<T>[] expressions);
		void RemoveIndex(params IndexItem<T>[] memberExpressions);
	}
}
