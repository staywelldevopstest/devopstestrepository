﻿namespace KswApi.Data.Mongo.Interfaces
{
	internal interface IDataSetConsumer
	{
		IDataSetProvider Provider { set; }
	}
}
