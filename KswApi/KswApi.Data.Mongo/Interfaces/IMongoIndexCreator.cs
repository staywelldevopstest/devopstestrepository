﻿using MongoDB.Driver;

namespace KswApi.Data.Mongo.Interfaces
{
	internal interface IMongoIndexCreator<T>
	{
		void CreateIndexes(IMongoIndexer<T> collection);
	}
}
