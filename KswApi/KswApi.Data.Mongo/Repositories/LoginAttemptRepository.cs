﻿using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;

namespace KswApi.Data.Mongo.Repositories
{
	class LoginAttemptRepository : Base<LoginAttempt>, ILoginAttemptRepository
	{
		public void Add(LoginAttempt loginAttempt)
		{
			DataSet.Add(loginAttempt);
		}
	}
}
