﻿using KswApi.Poco.Sms;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace KswApi.Data.Mongo.Repositories
{
    internal class SmsMessageLogRepository : Base<SmsMessageLog>, ISmsMessageLogRepository
    {
        public void Add(SmsMessageLog messageLog)
        {
            DataSet.Add(messageLog);
        }

        public SmsMessageLog Get(Guid messageLogId)
        {
            return DataSet.SingleOrDefault(item => item.Id == messageLogId);
        }

        public IEnumerable<SmsMessageLog> GetByLicense(Guid licenseId)
        {
            return DataSet.Where(item => item.LicenseId == licenseId);
        }

        public int Count(Expression<Func<SmsMessageLog, bool>> expression)
        {
            return DataSet.Count(expression);
        }
    }
}
