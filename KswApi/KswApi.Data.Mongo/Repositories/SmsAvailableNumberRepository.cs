﻿using KswApi.Interface.Objects;
using KswApi.Interface.Objects.SmsGateway;
using KswApi.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace KswApi.Data.Mongo.Repositories
{
	internal class SmsAvailableNumberRepository : Base<SmsSharedNumber>, ISmsAvailableNumberRepository
	{
		public void Add(SmsSharedNumber number)
		{
			DataSet.Add(number);
		}

		public void Update(SmsSharedNumber number)
		{
			DataSet.Update(number);
		}

		public void Delete(SmsSharedNumber number)
		{
			DataSet.Remove(number.Id);
		}

		public SmsSharedNumber GetByNumber(string number)
		{
			return DataSet.SingleOrDefault(item => item.Number == number);
		}

		public IEnumerable<SmsSharedNumber> Get(Expression<Func<SmsSharedNumber, bool>> expression)
		{
			return DataSet.Where(expression);
		}
	}
}
