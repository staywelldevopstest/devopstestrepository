﻿using KswApi.Poco.Sms;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;
using System.Linq;

namespace KswApi.Data.Mongo.Repositories
{
    internal class SmsNumberDataLogRepository : Base<SmsNumberDataLog>, ISmsNumberDataLogRepository
    {
        public void Add(SmsNumberDataLog numberDataLog)
        {
            DataSet.Add(numberDataLog);
        }

        public void Update(SmsNumberDataLog numberDataLog)
        {
            DataSet.Update(numberDataLog);
        }

        public SmsNumberDataLog Get(System.Guid numberId)
        {
            return DataSet.SingleOrDefault(item => item.NumberId == numberId);
        }

        public int Count(System.Guid licenseId, System.Guid numberId, System.DateTime startDate, System.DateTime endDate)
        {
            return DataSet.Count(number => number.LicenseId == licenseId && number.NumberId != numberId &&
                                          (number.CreatedDate < endDate && (number.DeletedDate > startDate || number.DeletedDate == null)));
        }

        public int Count(System.Guid licenseId, System.DateTime startDate, System.DateTime endDate)
        {
            return DataSet.Count(number => number.LicenseId == licenseId && (number.CreatedDate < endDate && (number.DeletedDate > startDate || number.DeletedDate == null)));
        }
    }
}
