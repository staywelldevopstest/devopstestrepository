﻿using KswApi.Data.Mongo.Framework;
using KswApi.Data.Mongo.Interfaces;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KswApi.Data.Mongo.Repositories
{
    internal class LicenseRepository : Base<License>, ILicenseRepository
    {
        public override void CreateIndexes(IMongoIndexer<License> indexer)
        {
			indexer.EnsureIndex(IndexDirection.Ascending, license => license.ParentLicenseId);
			indexer.EnsureIndex(IndexDirection.Ascending, license => license.Name);

			// remove the index on client id
			indexer.RemoveIndex(IndexDirection.Ascending, license => license.ClientId);

			// and add the index on client id AND status
            indexer.EnsureIndex(IndexDirection.Ascending, license => license.ClientId, license => license.Status);
        }

        public void Add(License client)
        {
            DataSet.Add(client);
        }

        public void Update(License client)
        {
            DataSet.Update(client);
        }

        public void Delete(License license)
        {
            DataSet.Update(license);
        }

        public License GetById(Guid id)
        {
            return DataSet.SingleOrDefault(license => license.Id == id && license.Status != LicenseStatus.Inactive);
        }

        public License GetByApplicationId(Guid id)
        {
            return DataSet.SingleOrDefault(license => license.Applications.Contains(id) && license.Status != LicenseStatus.Inactive);
        }

	    public IEnumerable<License> GetByStatus(LicenseStatus status, int offset, int count, SortOption<License> sort)
	    {
			return DataSet.Where(license => license.Status == status).SortBy(sort).Skip(offset).Take(count);
	    }

	    public IEnumerable<License> GetByQueryAndStatus(string query, LicenseStatus status, int offset, int count, SortOption<License> sortOption)
	    {
			return DataSet.Where(license => license.Name.ToLower().Contains(query.ToLower()) && license.Status == status).SortBy(sortOption).Skip(offset).Take(count);
	    }

	    public IEnumerable<License> GetByClientIdAndStatus(Guid clientId, LicenseStatus status, SortOption<License> sortOption)
	    {
			return DataSet.Where(license => license.ClientId == clientId && license.Status == status).SortBy(sortOption);
	    }

		public IEnumerable<License> GetByClientIdQueryAndStatus(Guid clientId, string query, LicenseStatus status, int offset, int count, SortOption<License> sortOption)
		{
			return DataSet.Where(license => license.ClientId == clientId && license.Name.ToLower().Contains(query.ToLower()) && license.Status == status).SortBy(sortOption).Skip(offset).Take(count);
		}
		
		public IEnumerable<License> GetByClientIdAndStatus(Guid clientId, LicenseStatus status)
	    {
		    return DataSet.Where(license => license.ClientId == clientId && license.Status == status);
	    }

	    public IEnumerable<License> GetByClientIdCreatedDateAndStatus(Guid clientId, DateTime createdDate, LicenseStatus status)
	    {
			return DataSet.Where(license => license.ClientId == clientId && license.CreatedDate <= createdDate && license.Status == status);
	    }

	    public int CountByStatus(LicenseStatus status)
	    {
		    return DataSet.Count(license => license.Status == status);
	    }

	    public int CountByQueryAndStatus(string query, LicenseStatus status)
	    {
			return DataSet.Count(license => license.Name.ToLower().Contains(query.ToLower()) && license.Status == status);
	    }

	    public int CountByClientIdAndStatus(Guid clientId, LicenseStatus status)
	    {
			return DataSet.Count(license => license.ClientId == clientId && license.Status == status);
	    }

	    public int CountByClientIdQueryAndStatus(Guid clientId, string query, LicenseStatus status)
	    {
		    return
			    DataSet.Count(
				    license =>
				    license.ClientId == clientId && license.Status == status &&
				    license.Name.ToLower().Contains(query.ToLower()));
	    }
    }
}
