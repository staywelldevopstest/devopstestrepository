﻿using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KswApi.Data.Mongo.Repositories
{
	class CopyrightRepository : Base<Copyright>, ICopyrightRepository
	{
		public void Add(Copyright copyright)
		{
			DataSet.Add(copyright);
		}

		public void Update(Copyright copyright)
		{
			DataSet.Update(copyright);
		}

		public void Delete(Guid id)
		{
			DataSet.Remove(id);
		}

		public Copyright GetCopyright(Guid id)
		{
			return DataSet.FirstOrDefault(item => item.Id == id);
		}

		public IEnumerable<Copyright> GetCopyrights(int offset, int count, string query, List<Guid> exclude)
		{
			string lowercaseQuery = query.ToLower();

			if (exclude == null || exclude.Count == 0)
			{
				return DataSet.Where(
					item =>
					item.Value.ToLower().Contains(lowercaseQuery)
					).OrderBy(copyright => copyright.DateModified).Skip(offset).Take(count);
			}

			return DataSet.Where(
				item =>
				item.Value.ToLower().Contains(lowercaseQuery)
				&& !exclude.Contains(item.Id)
				).OrderBy(copyright => copyright.DateModified).Skip(offset).Take(count);

		}

		public IEnumerable<Copyright> GetCopyrights(int offset, int count, List<Guid> exclude)
		{
			if (exclude == null || exclude.Count == 0)
				return DataSet.OrderBy(copyright => copyright.DateModified).Skip(offset).Take(count);

			return DataSet.Where(item => !exclude.Contains(item.Id)).OrderBy(copyright => copyright.DateModified).Skip(offset).Take(count);
		}

		public int Count(List<Guid> exclude)
		{
			if (exclude == null || exclude.Count == 00)
				return DataSet.Count();

			return DataSet.Count(
					item => !exclude.Contains(item.Id));
		}

		public int Count(string query, List<Guid> exclude)
		{
			string lowercaseQuery = query.ToLower();

			if (exclude == null || exclude.Count == 0)
			{
				return DataSet.Count(
						item =>
						item.Value.ToLower().Contains(lowercaseQuery));
			}

			return DataSet.Count(
					item =>
					item.Value.ToLower().Contains(lowercaseQuery)
					&& !exclude.Contains(item.Id));
		}
	}
}
