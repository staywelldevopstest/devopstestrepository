﻿using KswApi.Data.Mongo.Framework;
using KswApi.Poco.Sms;
using KswApi.Repositories.Enums;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KswApi.Data.Mongo.Repositories
{
    internal class SmsSessionRepository : Base<SmsSession>, ISmsSessionRepository
    {
        public override void CreateIndexes(Interfaces.IMongoIndexer<SmsSession> indexer)
        {
            indexer.EnsureIndex(IndexDirection.Ascending, item => item.SubscriberNumber, item => item.ServiceNumber, item => item.Keyword, item => item.State);
            indexer.EnsureIndex(IndexDirection.Ascending, item => item.ServiceNumber, item => item.Keyword, item => item.State);
        }

        public void Add(SmsSession session)
        {
            DataSet.Add(session);
        }

        public void Update(SmsSession session)
        {
            DataSet.Update(session);
        }

        public void Delete(SmsSession session)
        {
            DataSet.Remove(session.Id);
        }

        public void Delete(Guid id)
        {
            DataSet.Remove(id);
        }

        public SmsSession GetById(Guid id)
        {
            return DataSet.SingleOrDefault(item => item.Id == id);
        }

        public IEnumerable<SmsSession> GetByNumbersAndKeywordAndState(string subscriberNumber, string serviceNumber, string keyword, SmsSessionState state)
        {
            return DataSet.Where(item => item.SubscriberNumber == subscriberNumber && item.ServiceNumber == serviceNumber && item.Keyword == keyword && item.State == state);
        }

        public IEnumerable<SmsSession> GetByServiceNumberAndKeywordAndState(string serviceNumber, string keyword, SmsSessionState state)
        {
            return DataSet.Where(item => item.ServiceNumber == serviceNumber && item.Keyword == keyword && item.State == state);
        }

	    public IEnumerable<SmsSession> GetByLastRequest(DateTime expiration, int count)
	    {
		    return DataSet.Where(item => item.LastRequestDate < expiration).Take(count);
	    }

	    public IEnumerable<SmsSession> GetByNumbersAndState(string subscriberNumber, string serviceNumber, SmsSessionState state)
        {
            return DataSet.Where(item => item.SubscriberNumber == subscriberNumber && item.ServiceNumber == serviceNumber && item.State == state);
        }

   
    }
}
