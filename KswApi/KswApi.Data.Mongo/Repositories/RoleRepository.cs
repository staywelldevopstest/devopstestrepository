﻿using System;
using System.Collections.Generic;
using System.Linq;
using KswApi.Interface.Objects;
using KswApi.Repositories.Interfaces;

namespace KswApi.Data.Mongo.Repositories
{
	internal class RoleRepository : Base<Role>, IRoleRepository
    {
		public Role GetById(Guid id)
		{
			return DataSet.SingleOrDefault(item => item.Id == id);
		}

		public Role GetByName(string name)
        {
            return DataSet.SingleOrDefault(item => item.Name == name);
        }

        public IEnumerable<Role> GetAll()
        {
            return DataSet;
        }

        public bool Contains(string roleName)
        {
            return DataSet.Any(item => item.Name == roleName);
        }

        public void Add(Role role)
        {
            DataSet.Add(role);
        }

        public void Update(Role role)
        {
            DataSet.Update(role);
        }

        public void Delete(Role role)
        {
            DataSet.Remove(role.Id);
        }
    }
}
