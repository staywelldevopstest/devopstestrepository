﻿using KswApi.Data.Mongo.Framework;
using KswApi.Poco.Administration;
using KswApi.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KswApi.Data.Mongo.Repositories
{
    internal class AuthorizationGrantRepository : Base<AuthorizationGrant>, IAuthorizationGrantRepository
    {
        public override void CreateIndexes(Interfaces.IMongoIndexer<AuthorizationGrant> indexer)
        {
            indexer.EnsureIndex(IndexDirection.Ascending, item => item.Code);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.Expiration);
        }

        public void Add(AuthorizationGrant grant)
        {
            DataSet.Add(grant);
        }

        public void Delete(AuthorizationGrant grant)
        {
            DataSet.Remove(grant.Id);
        }

        public AuthorizationGrant GetByCode(string code)
        {
            return DataSet.SingleOrDefault(item => item.Code == code);
        }

        public AuthorizationGrant GetById(Guid id)
        {
            return DataSet.SingleOrDefault(item => item.Id == id);
        }

	    public IEnumerable<AuthorizationGrant> GetByExpiration(DateTime expiration, int count)
	    {
		    return DataSet.Where(item => item.Expiration < expiration).Take(count);
	    }

	    public void Delete(Guid id)
        {
            DataSet.Remove(id);
        }

    }
}
