﻿using System.Linq;
using KswApi.Poco.Tasks;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;

namespace KswApi.Data.Mongo.Repositories
{
	internal class TestDataRepository : Base<TestData>, ITestDataRepository
	{
		public TestData GetTestData()
		{
			TestData data = DataSet.FirstOrDefault();

			if (data != null)
				return data;

			data = new TestData
						{
							Key = "The Key",
							Data = "This is Mongo, baby!"
						};

			DataSet.Add(data);

			return DataSet.FirstOrDefault(item => item.Key == "The Key");
		}
	}
}
