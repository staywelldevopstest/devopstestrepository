﻿using System;
using System.Collections.Generic;
using KswApi.Data.Mongo.Interfaces;

namespace KswApi.Data.Mongo.Repositories
{
	internal abstract class ExtendedBase<TGenericType> : IMongoIndexCreator<TGenericType>, IDataSetConsumer where TGenericType : class
	{
		readonly Dictionary<Type, object> _dictionary = new Dictionary<Type, object>();

		public IDataSetProvider Provider { get; set; }

		public IDataSet<TData> DataSet<TData>()
		{
			object obj;
			if (_dictionary.TryGetValue(typeof(TData), out obj))
				return (IDataSet<TData>) obj;

			IDataSet<TData> set = Provider.GetSet<TData>();

			_dictionary.Add(typeof(TData), set);

			return set;
		}

		#region Implementation of IMongoIndexCreator<T>

		public void CreateIndexes(IMongoIndexer<TGenericType> collection)
		{
			
		}

		#endregion
	}
}
