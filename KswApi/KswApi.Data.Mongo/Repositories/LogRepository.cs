﻿using KswApi.Data.Mongo.Framework;
using KswApi.Data.Mongo.Interfaces;
using KswApi.Interface.Objects;
using KswApi.Poco.Logging;
using KswApi.Repositories.Enums;
using KswApi.Repositories.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace KswApi.Data.Mongo.Repositories
{
	internal class LogRepository : Base<Log>, ILogRepository
	{
		public override void CreateIndexes(IMongoIndexer<Log> indexer)
		{
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.Date);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.Date);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.Client);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.Type);
		}

		public void Add(Log log)
		{
			DataSet.Add(log);
		}

		public Log Get(Guid logId)
		{
			return DataSet.SingleOrDefault(item => item.Id == logId);
		}

		public IEnumerable<Log> Get(int offset, int count, LogSortOption sort)
		{
			return count == 0 ? (IEnumerable<Log>)new List<Log>()
				: DataSet.SortBy(sort).Skip(offset).Take(count);
		}

		public IEnumerable<Log> Get(int offset, int count, Expression<Func<Log, bool>> expression, LogSortOption sort)
		{
			return DataSet.Where(expression).SortBy(sort).Skip(offset).Take(count);
		}

		public IEnumerable<Log> Get(int offset, int count, bool includeWarnings, bool includeErrors, bool includeInfo, DateTime fromDate, DateTime toDate, string clientId, string licenseId, LogSortOption sort)
		{
			//var data = DataSet.Where(log => log.Date <= toDate && log.Date >= fromDate);

			//if (includeWarnings && includeErrors && includeInfo) //All three
			//    data = data.Where(log => log.SubType == LogItemSubType.Warning || log.SubType == LogItemSubType.Exception || log.SubType == LogItemSubType.Information);
			//else if (includeWarnings && includeErrors && !includeInfo) // Not info
			//    data = data.Where(log => log.SubType == LogItemSubType.Warning || log.SubType == LogItemSubType.Information);
			//else if (includeWarnings && !includeErrors && includeInfo) // Not errors
			//    data = data.Where(log => log.SubType == LogItemSubType.Warning || log.SubType == LogItemSubType.Exception);
			//else if (!includeWarnings && includeErrors && includeInfo) // Not warnings
			//    data = data.Where(log => log.SubType == LogItemSubType.Information || log.SubType == LogItemSubType.Exception);
			//else if (!includeWarnings && !includeErrors && includeInfo) // Not warnings and errors
			//    data = data.Where(log => log.SubType == LogItemSubType.Information);
			//else if (!includeWarnings && includeErrors && !includeInfo) // Not warnings and info
			//    data = data.Where(log => log.SubType == LogItemSubType.Exception);
			//else if (includeWarnings && !includeErrors && !includeInfo) // Not errros and info
			//    data = data.Where(log => log.SubType == LogItemSubType.Warning);

			//Guid clientIdGuid;
			//Guid licenseIdGuid;

			//if (!string.IsNullOrWhiteSpace(clientId) && clientId != Guid.Empty.ToString() && Guid.TryParse(clientId, out clientIdGuid))
			//    data = data.Where(log => log.Client == clientIdGuid);

			//if (!string.IsNullOrWhiteSpace(licenseId) && licenseId != Guid.Empty.ToString() && Guid.TryParse(licenseId, out licenseIdGuid))
			//    data = data.Where(log => log.License == licenseIdGuid.ToString());

			return DataSet.Where(log => log.Date <= toDate && log.Date >= fromDate && log.SubType == LogItemSubType.Warning || log.SubType == LogItemSubType.Exception || log.SubType == LogItemSubType.Information).SortBy(sort).Skip(offset).Take(count);
		}

		public int Count()
		{
			return DataSet.Count();
		}

		public int Count(Expression<Func<Log, bool>> expression)
		{
			return DataSet.Where(expression).Count();
		}
	}
}
