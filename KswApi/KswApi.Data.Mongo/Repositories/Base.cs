﻿using KswApi.Data.Mongo.Interfaces;

namespace KswApi.Data.Mongo.Repositories
{
	internal abstract class Base<T> : IMongoIndexCreator<T>, IDataSetConsumer where T : class
	{
		private IDataSet<T> _dataSet;

		public IDataSetProvider Provider { get; set; }

		public IDataSet<T> DataSet
		{
			get { return (_dataSet ?? (_dataSet = Provider.GetSet<T>())); }
		}

		public virtual void CreateIndexes(IMongoIndexer<T> indexer)
		{
			
		}
	}
}
