﻿using KswApi.Poco.Sms;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;
using System;
using System.Linq;

namespace KswApi.Data.Mongo.Repositories
{
    class SmsBillingReportDataRepository : Base<SmsBillingReportLicenseData>, ISmsBillingReportDataRepository
    {
        public void Add(SmsBillingReportLicenseData reportData)
        {
            DataSet.Add(reportData);
        }

        public void Update(SmsBillingReportLicenseData reportData)
        {
            DataSet.Update(reportData);
        }

        public SmsBillingReportLicenseData Get(Guid licenseId, Interface.Enums.Month month, int year)
        {
            return DataSet.SingleOrDefault(item => item.LicenseId == licenseId && item.Month == month && item.Year == year);
        }
    }
}
