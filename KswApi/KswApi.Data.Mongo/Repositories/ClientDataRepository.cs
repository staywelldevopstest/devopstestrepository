﻿using KswApi.Data.Mongo.Framework;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KswApi.Data.Mongo.Repositories
{
    internal class ClientDataRepository : Base<Client>, IClientRepository
    {
		public override void CreateIndexes(Interfaces.IMongoIndexer<Client> indexer)
		{
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.DateAdded);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.Status, item => item.CaseInsensitiveName);
		}

        public void Add(Client client)
        {
            DataSet.Add(client);
        }

        public void Update(Client client)
        {
            DataSet.Update(client);
        }

        //We are soft deleting clients.  That's why we are upating it instead of removing
        public void Delete(Client client)
        {
            DataSet.Update(client);
        }

        public bool Contains(string filter, ClientStatus status)
        {
            return DataSet.Any(item => item.ClientName.ToLower() == filter.ToLower() && item.Status == status);
        }

        public Client GetById(Guid id)
        {
            return DataSet.SingleOrDefault(item => item.Id == id && item.Status != ClientStatus.Deleted);
        }

        public IEnumerable<Client> GetByStatus(ClientStatus status, int offset, int count, SortOption<Client> sort)
        {
            return DataSet.Where(item => item.Status == status).SortBy(sort).Skip(offset).Take(count);
        }

        public IEnumerable<Client> GetByDateAddedAndStatus(DateTime dateAdded, ClientStatus status)
        {
            return DataSet.Where(item => item.DateAdded <= dateAdded && item.Status == status);
        }

        public IEnumerable<Client> GetByDateAdded(DateTime dateAdded)
        {
            return DataSet.Where(item => item.DateAdded <= dateAdded);
        }

        public IEnumerable<Client> GetByQueryAndStatus(string query, ClientStatus status, int offset, int count, SortOption<Client> sort)
        {
            return DataSet.Where(item => item.ClientName.ToLower().Contains(query.ToLower()) && item.Status == status).SortBy(sort).Skip(offset).Take(count);
        }

        public int Count(ClientStatus status)
        {
            return DataSet.Count(item => item.Status == status);
        }

        public int Count(string query, ClientStatus status)
        {
            return DataSet.Count(item => item.ClientName.ToLower().Contains(query.ToLower()) && item.Status == status);
        }
    }
}
