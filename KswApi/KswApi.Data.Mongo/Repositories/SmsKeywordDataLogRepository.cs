﻿using KswApi.Poco.Sms;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace KswApi.Data.Mongo.Repositories
{
    internal class SmsKeywordDataLogRepository : Base<SmsKeywordDataLog>, ISmsKeywordDataLogRepository
    {
        public void Add(SmsKeywordDataLog keywordDataLog)
        {
            DataSet.Add(keywordDataLog);
        }

        public void Update(SmsKeywordDataLog keywordDataLog)
        {
            DataSet.Update(keywordDataLog);
        }

        public SmsKeywordDataLog GetById(Guid id)
        {
            return DataSet.SingleOrDefault(item => item.Id == id);
        }

        public IEnumerable<SmsKeywordDataLog> Get(Expression<Func<SmsKeywordDataLog, bool>> expression)
        {
            return DataSet.Where(expression);
        }

        public SmsKeywordDataLog GetByKeywordId(Guid keywordId)
        {
            return DataSet.SingleOrDefault(item => item.KeywordId == keywordId);
        }

        public int Count(Guid licenseId, string number, DateTime startDate, DateTime endDate)
        {
            return DataSet.Count(
                                keyword => keyword.LicenseId == licenseId && keyword.Number == number &&
                                           (keyword.CreateDate < endDate &&
                                            (keyword.DeletedDate > startDate || keyword.DeletedDate == null)));
        }
    }
}
