﻿using System;
using System.Collections.Generic;
using System.Linq;
using KswApi.Poco.Administration;
using KswApi.Repositories.Interfaces;

namespace KswApi.Data.Mongo.Repositories
{
    internal class DomainUserRepository : Base<DomainUser>, IDomainUserRepository
    {
        public void Add(DomainUser domainUser)
        {
            DataSet.Add(domainUser);
        }

        public void Update(DomainUser domainUser)
        {
            DataSet.Update(domainUser);
        }

        //public void Delete(Guid id)
        //{
        //    DataSet.Remove(id);
        //}

        public DomainUser GetById(Guid id)
        {
            return DataSet.FirstOrDefault(item => item.Id == id);
        }

        public IEnumerable<DomainUser> GetByIds(List<Guid> ids)
        {
            return DataSet.Where(item => ids.Contains(item.Id));
        }
    }
}
