﻿using System;
using System.Collections.Generic;
using System.Linq;
using KswApi.Data.Mongo.Framework;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;

namespace KswApi.Data.Mongo.Repositories
{
	class PasswordTokenRepository : Base<PasswordToken>, IPasswordTokenRepository
	{
		public override void CreateIndexes(Interfaces.IMongoIndexer<PasswordToken> indexer)
		{
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.Token);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.UserId);
		}

		public PasswordToken GetById(Guid id)
		{
			return DataSet.FirstOrDefault(item => item.Id == id);
		}

		public IEnumerable<PasswordToken> GetByUserId(Guid id)
		{
			return DataSet.Where(item => item.UserId == id);
		}

		public PasswordToken GetByToken(string token)
		{
			return DataSet.FirstOrDefault(item => item.Token == token);
		}

		public void Add(PasswordToken passwordToken)
		{
			DataSet.Add(passwordToken);
		}

		public void DeleteById(Guid id)
		{
			DataSet.Remove(id);
		}
	}
}
