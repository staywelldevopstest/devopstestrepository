﻿using KswApi.Poco.Tasks;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;

namespace KswApi.Data.Mongo.Repositories
{
    internal class TaskStatusRepository : Base<TaskStatusData>, ITaskStatusRepository
    {
        public void Add(TaskStatusData statusData)
        {
            DataSet.Add(statusData);
        }
    }
}
