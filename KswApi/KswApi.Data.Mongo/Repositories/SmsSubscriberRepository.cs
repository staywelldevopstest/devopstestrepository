﻿using System;
using System.Linq;
using KswApi.Data.Mongo.Framework;
using KswApi.Data.Mongo.Interfaces;
using KswApi.Poco.Sms;
using KswApi.Repositories.Interfaces;

namespace KswApi.Data.Mongo.Repositories
{
	internal class SmsSubscriberRepository : Base<SmsSubscriber>, ISmsSubscriberRepository
	{
		public override void CreateIndexes(IMongoIndexer<SmsSubscriber> indexer)
		{
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.SubscriberNumber, item => item.ServiceNumber);
			indexer.EnsureIndex(MultilevelExpression<SmsSubscriber>.WithEnumerable(item => item.Sessions).WithProperty(item => item.SessionId).Ascending());
		}

		public void Add(SmsSubscriber subscriber)
		{
			DataSet.Add(subscriber);
		}

		public SmsSubscriber GetByNumbers(string subscriberNumber, string serviceNumber)
		{
			return DataSet.SingleOrDefault(item => item.SubscriberNumber == subscriberNumber && item.ServiceNumber == serviceNumber);
		}

		public void Update(SmsSubscriber subscriber)
		{
			DataSet.Update(subscriber);
		}

		public SmsSubscriber GetBySessionId(Guid sessionId)
		{
			return DataSet.SingleOrDefault(item => item.Sessions.Any(session => session.SessionId == sessionId));
		}

		public void Delete(SmsSubscriber subscriber)
		{
			DataSet.Remove(subscriber.Id);
		}
	}
}
