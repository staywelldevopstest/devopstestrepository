﻿using KswApi.Data.Mongo.Framework;
using KswApi.Poco.Enums;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace KswApi.Data.Mongo.Repositories
{
    internal class ClientUserRepository : Base<ClientUser>, IClientUserRepository
    {
        public void Add(ClientUser clientUser)
        {
            DataSet.Add(clientUser);
        }

        public void Update(ClientUser clientUser)
        {
            DataSet.Update(clientUser);
        }

        public void Delete(Guid id)
        {
	        ClientUser clientUser = DataSet.First(x => x.Id == id);
			clientUser.State = ClientUserState.Deleted;
			DataSet.Update(clientUser);
        }

        public ClientUser GetUser(Guid id)
        {
            return DataSet.FirstOrDefault(item => item.Id == id && item.State != ClientUserState.Deleted);
        }

        public ClientUser GetUserByEmail(string email)
        {
            return DataSet.FirstOrDefault(item => item.EmailAddress.ToLower() == email.ToLower() && item.State != ClientUserState.Deleted);
        }

        public bool ExistsByEmail(string email)
        {
            return DataSet.Any(item => item.EmailAddress.ToLower() == email.ToLower() && item.State != ClientUserState.Deleted);
        }

        public IEnumerable<ClientUser> GetClientUsers(int offset, int count, string query, SortOption<ClientUser> sort, Expression<Func<ClientUser, bool>> filter)
        {
            string lowercaseQuery = query.ToLower();

            return
                DataSet.Where(filter).Where(
                    item => 
                    (
						item.FirstName.ToLower().Contains(lowercaseQuery) ||
						item.LastName.ToLower().Contains(lowercaseQuery) ||
						item.EmailAddress.ToLower().Contains(lowercaseQuery)
					) && item.State != ClientUserState.Deleted).SortBy(sort).Skip(offset).Take(count);
        }

        public IEnumerable<ClientUser> GetClientUsers(int offset, int count, SortOption<ClientUser> sort, Expression<Func<ClientUser, bool>> filter)
        {
            return DataSet.Where(item => item.State != ClientUserState.Deleted).Where(filter).SortBy(sort).Skip(offset).Take(count);
        }

        public IEnumerable<ClientUser> GetByClientId(Guid clientId)
        {
            return DataSet.Where(item => item.ClientId == clientId && item.State != ClientUserState.Deleted);
        }

        public int Count(Expression<Func<ClientUser, bool>> filter)
        {
            return DataSet.Where(item => item.State != ClientUserState.Deleted).Count(filter);
        }

        public int Count(string query, Expression<Func<ClientUser, bool>> filter)
        {
            string lowercaseQuery = query.ToLower();

            return
                DataSet.Where(filter).Count(
                    item =>
                    item.FirstName.ToLower().Contains(lowercaseQuery) ||
                    item.LastName.ToLower().Contains(lowercaseQuery) ||
                    item.EmailAddress.ToLower().Contains(lowercaseQuery) &&
                    item.State != ClientUserState.Deleted);
        }
    }
}
