﻿using System.Linq;
using KswApi.Poco.Tasks;
using KswApi.Repositories.Interfaces.Tasks;

namespace KswApi.Data.Mongo.Repositories.Tasks
{
	class ExecutingTaskRepository : ExtendedBase<ExecutingTask<object>>, IExecutingTaskRepository
	{
		public void AddIfNotFound<TData>(ExecutingTask<TData> task)
		{
			DataSet<ExecutingTask<TData>>().Add(task);
		}

		#region Implementation of IExecutingTaskRepository

		public void Add<TData>(ExecutingTask<TData> task)
		{
			DataSet<ExecutingTask<TData>>().Add(task);
		}

		public ExecutingTask<TData> GetById<TData>(string id)
		{
			return DataSet<ExecutingTask<TData>>().FirstOrDefault(item => item.Id == id);
		}

		public bool Acquire<TData>(string id)
		{
			return
				DataSet<ExecutingTask<TData>>().Update(item => item.Id == id && item.State == TaskState.Waiting, item => item.State,
				                                       TaskState.Active) > 0;

		}

		public void Update<TData>(string id, TData data)
		{
			DataSet<ExecutingTask<TData>>().Update(item => item.Id == id, item => item.Data, data);
		}

		public void Release(string id)
		{
			DataSet<ExecutingTask<object>>().Update(item => item.Id == id && item.State == TaskState.Active, item => item.State, TaskState.Waiting);
		}

		public void Delete(string id)
		{
			DataSet<ExecutingTask<object>>().Remove(item => item.Id == id);
		}

		#endregion
	}
}
