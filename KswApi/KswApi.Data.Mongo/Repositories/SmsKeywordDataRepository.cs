﻿using System;
using System.Collections.Generic;
using System.Linq;
using KswApi.Poco.Sms;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;

namespace KswApi.Data.Mongo.Repositories
{
	internal class SmsKeywordDataRepository : Base<SmsKeywordData>, ISmsKeywordDataRepository
	{
		public void Add(SmsKeywordData number)
		{
			DataSet.Add(number);
		}

		public void Update(SmsKeywordData number)
		{
			DataSet.Update(number);
		}

		public void Delete(SmsKeywordData number)
		{
			DataSet.Remove(number.Id);
		}

		public SmsKeywordData GetByNumberAndKeyword(string number, string keyword)
		{
			return DataSet.SingleOrDefault(item => item.Number == number && item.Keyword == keyword);
		}

		public IEnumerable<SmsKeywordData> GetByNumberAndLicenseId(string number, Guid license)
		{
			return DataSet.Where(item => item.Number == number && item.LicenseId == license);
		}

		public IEnumerable<SmsKeywordData> GetByNumberId(Guid numberId)
		{
			return DataSet.Where(item => item.NumberId == numberId);
		}

		public SmsKeywordData GetById(Guid id)
		{
			return DataSet.SingleOrDefault(item => item.Id == id);
		}
	}
}
