﻿using System;
using System.Collections.Concurrent;
using System.Linq;
using System.Reflection;
using KswApi.Data.Mongo.Framework;
using KswApi.Data.Mongo.Interfaces;
using KswApi.Repositories.Interfaces;
using MongoDB.Driver;

namespace KswApi.Data.Mongo.Repositories
{
	internal class MongoRepository : IRepository, IDataSetProvider
	{
		private readonly MongoServer _server;
		private readonly MongoDatabase _database;
		private readonly IDataSetProvider _dataSetProvider;
		private static readonly ConcurrentDictionary<Type, ConstructorInfo> Repositories = new ConcurrentDictionary<Type, ConstructorInfo>();

		public MongoRepository(string connectionString, string databaseName)
		{
			MongoClient client = new MongoClient(connectionString);
			
			_server = client.GetServer();
			
			_database = _server.GetDatabase(databaseName);
			
			_dataSetProvider = this;
		}

		/// <summary>
		/// This constructor added to allow testing
		/// </summary>
		/// <param name="dataSetProvider"></param>
		public MongoRepository(IDataSetProvider dataSetProvider)
		{
			_dataSetProvider = dataSetProvider;
		}


		public T GetRepository<T>() where T : class
		{
			ConstructorInfo constructor = Repositories.GetOrAdd(typeof (T), FindRepository(typeof (T)));

			T repository = constructor.Invoke(null) as T;

			IDataSetConsumer consumer = repository as IDataSetConsumer;

			if (consumer != null)
				consumer.Provider = _dataSetProvider;

			return repository;
		}

		private ConstructorInfo FindRepository(Type type)
		{
			if (!type.IsInterface)
				throw new ArgumentException(string.Format("The requested repository type was not an interface: {0}.", type.Name));

			Assembly assembly = Assembly.GetExecutingAssembly();
			Type typeInfo = assembly.GetTypes().SingleOrDefault(type.IsAssignableFrom);

			if (typeInfo == null)
				throw new ArgumentException(string.Format("The requested repository type does not exist in the Mongo data layer: {0}.", type.Name));

			ConstructorInfo constructor = typeInfo.GetConstructor(Type.EmptyTypes);

			if (constructor == null)
				throw new ArgumentException(string.Format("The repository does not have a public no-parameter constructor: {0}.", type.Name));

			return constructor;
		}

		public void Commit()
		{
			if (_dataSetProvider != this)
				_dataSetProvider.Commit();
		}

		public IDataSet<T> GetSet<T>()
		{
			// handle generic types
			string name = typeof (T).Name;
			
			int index = name.IndexOf('`');

			name = index < 0 ? name : name.Substring(0, index);
			
			MongoCollection<T> collection = _database.GetCollection<T>(name);

			return new MongoDataSet<T>(collection);
		}
	}
}
