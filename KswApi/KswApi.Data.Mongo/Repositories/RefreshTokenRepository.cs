﻿using System;
using System.Collections.Generic;
using System.Linq;
using KswApi.Data.Mongo.Framework;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;

namespace KswApi.Data.Mongo.Repositories
{
	internal class RefreshTokenRepository : Base<RefreshToken>, IRefreshTokenRepository
	{
		public override void CreateIndexes(Interfaces.IMongoIndexer<RefreshToken> indexer)
		{
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.Token);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.UserDetails.Id);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.Expiration);
		}

		public RefreshToken GetByToken(string token)
		{
			return DataSet.SingleOrDefault(item => item.Token == token);
		}

		public IEnumerable<RefreshToken> GetByExpiration(DateTime expiration)
		{
			return DataSet.Where(item => item.Expiration < expiration);
		}

		public void Add(RefreshToken token)
		{
			DataSet.Add(token);
		}

		public void DeleteByUserId(Guid id)
		{
			DataSet.Remove(item => item.UserDetails.Id == id);
		}

		public void Delete(RefreshToken token)
		{
			DataSet.Remove(token.Id);
		}

		public void DeleteByExpiration(DateTime expiration)
		{
			DataSet.Remove(item => item.Expiration < expiration);
		}

		public void Delete(Guid id)
		{
			DataSet.Remove(id);
		}
	}
}
