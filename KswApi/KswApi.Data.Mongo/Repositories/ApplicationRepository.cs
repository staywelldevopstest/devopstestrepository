﻿using KswApi.Data.Mongo.Framework;
using KswApi.Interface.Objects;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace KswApi.Data.Mongo.Repositories
{
    internal class ApplicationRepository : Base<Application>, IApplicationRepository
    {
		public override void CreateIndexes(Interfaces.IMongoIndexer<Application> indexer)
		{
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.Addresses);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.DateAdded);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.Name);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.RedirectUri);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.Type);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.LicenseId);
		}

        public void Add(Application client)
        {
            DataSet.Add(client);
        }

        public void Update(Application client)
        {
            DataSet.Update(client);
        }

        public Application GetById(Guid id)
        {
            return DataSet.SingleOrDefault(item => item.Id == id);
        }

        public IEnumerable<Application> GetApplications(int offset, int count, SortOption<Application> sortOption)
        {
            return DataSet.SortBy(sortOption).Skip(offset).Take(count);
        }

        public IEnumerable<Application> GetApplications(int offset, int count, Expression<Func<Application, bool>> expression, SortOption<Application> sortOption)
        {
            return DataSet.Where(expression).SortBy(sortOption).Skip(offset).Take(count);
        }

        public int Count()
        {
            return DataSet.Count();
        }

        public int Count(Expression<Func<Application, bool>> expression)
        {
            return DataSet.Count(expression);
        }

        public void Delete(Application application)
        {
            DataSet.Remove(application.Id);
        }

	    public void DeleteByLicenseId(Guid licenseId)
	    {
		    DataSet.Remove(item => item.LicenseId == licenseId);
	    }
    }
}
