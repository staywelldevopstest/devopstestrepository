﻿using KswApi.Common.Objects;
using KswApi.Data.Mongo.Framework;
using KswApi.Data.Mongo.Interfaces;
using KswApi.Repositories.Interfaces;
using System;
using System.Linq;

namespace KswApi.Data.Mongo.Repositories
{
    internal class AccessTokenRepository : Base<AccessToken>, IAccessTokenRepository
    {
        public override void CreateIndexes(IMongoIndexer<AccessToken> indexer)
        {
            indexer.EnsureIndex(IndexDirection.Ascending, item => item.Token);
            indexer.EnsureIndex(IndexDirection.Ascending, item => item.Expiration);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.UserDetails.Id);
        }

        public AccessToken GetByToken(string token)
        {
            return DataSet.SingleOrDefault(item => item.Token == token);
        }

        public void Add(AccessToken token)
        {
            DataSet.Add(token);
        }

        public void Delete(AccessToken token)
        {
            DataSet.Remove(token.Id);
        }

        public void Delete(Guid id)
        {
            DataSet.Remove(id);
        }

	    public void DeleteByUserId(Guid id)
	    {
		    DataSet.Remove(item => item.UserDetails.Id == id);
	    }

	    public void DeleteByExpiration(DateTime expiration)
	    {
		    DataSet.Remove(item => item.Expiration < expiration);
	    }

	    public void Update(AccessToken token)
	    {
		    DataSet.Update(token);
	    }
    }
}
