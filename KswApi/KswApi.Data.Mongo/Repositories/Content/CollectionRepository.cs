﻿using System;
using System.Collections.Generic;
using System.Linq;
using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Data.Mongo.Repositories.Content
{
    internal class CollectionRepository : Base<Collection>, ICollectionRepository
    {
        public IEnumerable<Collection> GetByIds(List<Guid> ids)
        {
            return DataSet.Where(item => ids.Contains(item.Id));
        }

	    public void Add(Collection collection)
        {
            DataSet.Add(collection);
        }

        public Collection GetById(Guid guid)
        {
            return DataSet.SingleOrDefault(x => x.Id == guid);
        }

        public void Update(Collection collection)
        {
            DataSet.Update(collection);
        }

        public IEnumerable<Collection> GetAll()
        {
            return DataSet;
        }

	    public void IncrementLicenseCount(Guid id, int count)
	    {
		    DataSet.Increment(item => item.Id == id, item => item.LicenseCount, count);
	    }

	    public void Delete(Collection collection)
        {
            DataSet.Remove(collection.Id);
        }
    }
}
