﻿using System;
using System.Linq;
using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Data.Mongo.Repositories.Content
{
	internal class ContentRevisionRepository : Base<ContentRevision>, IContentRevisionRepository
	{
		#region Implementation of IContentRevisionRepository

		public void Add(ContentRevision content)
		{
			DataSet.Add(content);
		}

		public void DeleteById(Guid id)
		{
			DataSet.Remove(id);
		}

		public ContentRevision GetById(Guid id)
		{
			return DataSet.FirstOrDefault(item => item.Id == id);
		}

		#endregion
	}
}
