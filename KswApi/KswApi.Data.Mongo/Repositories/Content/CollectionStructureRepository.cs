﻿using System;
using System.Collections.Generic;
using System.Linq;
using KswApi.Data.Mongo.Framework;
using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Data.Mongo.Repositories.Content
{
	internal class CollectionStructureRepository : Base<CollectionStructure>, ICollectionStructureRepository
	{
		public override void CreateIndexes(Interfaces.IMongoIndexer<CollectionStructure> indexer)
		{
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.ParentId);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.Path);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.ItemId);
		}

		#region Implementation of ICollectionStructureRepository

		public void Add(CollectionStructure structure)
		{
			DataSet.Add(structure);
		}

		public void Update(CollectionStructure structure)
		{
			DataSet.Update(structure);
		}

		public CollectionStructure GetById(Guid id)
		{
			return DataSet.FirstOrDefault(item => item.Id == id);
		}

		public IEnumerable<CollectionStructure> GetByPathElement(Guid id)
		{
			return DataSet.Where(item => item.Path.Contains(id));
		}

		public void DeleteByIds(List<Guid> ids)
		{
			DataSet.Remove(item => ids.Contains(item.Id));
		}

		public IEnumerable<CollectionStructure> GetWithChildrenById(Guid id)
		{
			return DataSet.Where(item => item.Id == id || item.ParentId == id);
		}

		public bool ExistsByItemIdAndCollectionId(Guid itemId, List<Guid> collectionIds)
		{
			return DataSet.Count(item => item.ItemId == itemId && collectionIds.Contains(item.CollectionId)) > 0;
		}

		public IEnumerable<CollectionStructure> GetByItemId(Guid id)
		{
			return DataSet.Where(item => item.ItemId == id);
		}

		public IEnumerable<CollectionStructure> GetByItemIds(List<Guid> ids)
		{
			return DataSet.Where(item => ids.Contains(item.ItemId));
		}

		#endregion
	}
}
