﻿using System;
using System.Collections.Generic;
using System.Linq;
using KswApi.Data.Mongo.Framework;
using KswApi.Data.Mongo.Interfaces;
using KswApi.Interface.Objects.Content;
using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Data.Mongo.Repositories.Content
{
	internal class ContentHistoryRepository : Base<ContentHistory>, IContentHistoryRepository
	{
		public override void CreateIndexes(IMongoIndexer<ContentHistory> indexer)
		{
			indexer.EnsureIndex(IndexDirection.Descending, item => item.ContentId, item => item.Time);
		}

		#region Implementation of IContentHistoryRepository

		public void Add(ContentHistory history)
		{
			DataSet.Add(history);
		}

		public void DeleteById(Guid id)
		{
			DataSet.Remove(id);
		}

		public ContentHistory GetById(Guid id)
		{
			return DataSet.FirstOrDefault(item => item.Id == id);
		}

		public IEnumerable<ContentHistory> GetByContentId(Guid id)
		{
			return DataSet.Where(item => item.ContentId == id).OrderByDescending(item => item.Time);
		}

		public IEnumerable<ContentHistory> GetByTimeAndType(DateTime time, ContentRevisionType type, int count)
		{
			return DataSet.Where(item => item.Time < time && item.RevisionType == type).Take(count);
		}

        public ContentHistory GetByContentIdRevisionTypeAndOnOrBeforeDate(Guid contentId, ContentRevisionType[] revisionTypes, DateTime onOrBefore)
        {
            return DataSet.Where(item => item.ContentId == contentId && item.Time < onOrBefore && revisionTypes.Contains(item.RevisionType))
                                    .OrderByDescending(x => x.Time).FirstOrDefault();
        }

		#endregion
        
    }
}
