﻿using System;
using System.Linq;
using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Data.Mongo.Repositories.Content
{
    internal class CollectionRevisionRepository : Base<CollectionRevision>, ICollectionRevisionRepository
	{
		#region Implementation of ICollectionRevisionRepository

		public void Add(CollectionRevision collection)
		{
			DataSet.Add(collection);
		}

		public CollectionRevision GetById(Guid id)
		{
			return DataSet.FirstOrDefault(item => item.Id == id);
		}

        public CollectionRevision GetByCollectionId(Guid id)
        {
            return DataSet.FirstOrDefault(item => item.Collection.Id == id);
        }

		#endregion
    }
}
