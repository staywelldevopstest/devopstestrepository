﻿using System.Collections.Generic;
using System.Linq;
using KswApi.Data.Mongo.Framework;
using KswApi.Data.Mongo.Interfaces;
using KswApi.Interface.Enums;
using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Data.Mongo.Repositories.Content
{
    internal class HierarchicalTaxonomyRepository : Base<HierarchicalTaxonomy>, IHierarchicalTaxonomyRepository
    {
        public void Clear(HierarchicalTaxonomyType type)
        {
            DataSet.Remove(x => x.Type == type);           
        }

        public void Add(HierarchicalTaxonomy hierarchicalTaxonomy)
        {
            DataSet.Add(hierarchicalTaxonomy);
        }

        public IEnumerable<HierarchicalTaxonomy> GetHierarchicalTaxonomiesByPaths(HierarchicalTaxonomyType type, string[] paths)
        {
            return DataSet.Where(taxonomy => taxonomy.Type == type && paths.Contains(taxonomy.Path));
        }
        public IEnumerable<HierarchicalTaxonomy> GetHierarchicalTaxonomiesByPath(HierarchicalTaxonomyType type, string path)
        {
            return DataSet.Where(taxonomy => taxonomy.Type == type && taxonomy.Path == path.ToLower());
        }

        public HierarchicalTaxonomy GetHierarchicalTaxonomyBySlugAndPath(HierarchicalTaxonomyType type, string slug, string path)
        {
            return DataSet.SingleOrDefault(taxonomy => taxonomy.Type == type && taxonomy.Slug == slug.ToLower() && taxonomy.Path == path.ToLower());
        }

        public override void CreateIndexes(IMongoIndexer<HierarchicalTaxonomy> indexer)
        {
            //indexer.EnsureIndex(IndexDirection.Ascending, taxonomy => new {taxonomy.Type, taxonomy.Path});
            indexer.EnsureIndex(IndexDirection.Ascending, taxonomy => taxonomy.Path);
        }

        public IEnumerable<HierarchicalTaxonomy> GetHierarchicalTaxonomiesByFullPaths(List<string> fullPaths)
        {
            return DataSet.Where(item => fullPaths.Contains(item.FullPath));
        }
    }
}
