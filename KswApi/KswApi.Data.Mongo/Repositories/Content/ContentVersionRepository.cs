﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using KswApi.Data.Mongo.Framework;
using KswApi.Data.Mongo.Interfaces;
using KswApi.Interface.Enums;
using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Data.Mongo.Repositories.Content
{
	internal class ContentVersionRepository : Base<ContentVersion>, IContentVersionRepository
	{
		public override void CreateIndexes(IMongoIndexer<ContentVersion> indexer)
		{
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.Title);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.Slug);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.BucketId, item => item.LegacyId);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.BucketId, item => item.Slug);
		}

		public void Add(ContentVersion content)
		{
			DataSet.Add(content);
		}

		public void Update(ContentVersion content)
		{
			DataSet.Update(content);
		}

		public void Delete(ContentVersion content)
		{
			DataSet.Remove(content.Id);
		}

		public ContentVersion GetById(Guid id)
		{
			return DataSet.SingleOrDefault(item => item.Id == id);
		}

		public ContentVersion GetByPath(string bucketIdOrSlug, string contentIdOrSlug)
		{
			return DataSet.FirstOrDefault(item => item.Paths.Any(value => value.BucketIdOrSlug == bucketIdOrSlug && value.ContentIdOrSlug == contentIdOrSlug) && item.Status == ObjectStatus.Active);
		}

		public IEnumerable<ContentVersion> GetByTitle(int offset, int count)
		{
			return DataSet.OrderBy(item => item.Title).Skip(offset).Take(count);
		}

		public IEnumerable<ContentVersion> GetByIds(IEnumerable<Guid> ids)
		{
			return DataSet.Where(item => ids.Contains(item.Id));
		}

		public IEnumerable<ContentVersion> GetByBucketId(Guid bucketId)
		{
			return DataSet.Where(item => item.BucketId == bucketId && item.Status != ObjectStatus.Deleted);
		}

		public ContentVersion GetByBucketAndLegacyId(Guid bucketId, string legacyId)
		{
			return DataSet.FirstOrDefault(item => item.BucketId == bucketId && item.LegacyId == legacyId);
		}

		public IEnumerable<ContentVersion> GetByBucketIdsAndSlugs(List<IdAndSlug> idsAndSlugs)
		{
			IEnumerable<Expression<Func<ContentVersion, bool>>> expressions = idsAndSlugs.Select(item =>
                (Expression<Func<ContentVersion, bool>>)(version => version.BucketId == item.Id && version.Slug == item.Slug && version.Status == ObjectStatus.Active)
            );

			return DataSet.Or(expressions);
		}

		public int Count()
		{
			return DataSet.Count();
		}

		public IEnumerable<ContentVersion> GetAll()
		{
			return DataSet;
		}
	}
}
