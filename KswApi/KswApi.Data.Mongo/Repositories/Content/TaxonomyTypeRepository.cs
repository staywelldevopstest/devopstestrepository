﻿using System.Collections.Generic;
using System.Linq;
using KswApi.Data.Mongo.Framework;
using KswApi.Data.Mongo.Interfaces;
using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Data.Mongo.Repositories.Content
{
    internal class TaxonomyTypeRepository : Base<TaxonomyType>, ITaxonomyTypeRepository
    {
        public void Add(TaxonomyType taxonomyType)
        {
            DataSet.Add(taxonomyType);
        }

        public void Clear()
        {
            DataSet.Remove(x => true);
        }

        public IEnumerable<TaxonomyType> GetTaxonomyTypes()
        {
            return DataSet;
        }

        public TaxonomyType GetTaxonomyTypeBySlug(string slug)
        {
            return DataSet.SingleOrDefault(taxonomy => taxonomy.Slug == slug.ToLower());
        }

        public override void CreateIndexes(IMongoIndexer<TaxonomyType> indexer)
        {
            indexer.EnsureIndex(IndexDirection.Ascending, taxonomyType => taxonomyType.Slug);
        }
    }
}
