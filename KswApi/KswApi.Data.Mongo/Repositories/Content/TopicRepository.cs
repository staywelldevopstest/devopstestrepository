﻿using System;
using System.Collections.Generic;
using System.Linq;
using KswApi.Data.Mongo.Framework;
using KswApi.Data.Mongo.Interfaces;
using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Data.Mongo.Repositories.Content
{
	class TopicRepository : Base<Topic>, ITopicRepository
	{
		public override void CreateIndexes(IMongoIndexer<Topic> indexer)
		{
			indexer.EnsureUnique(IndexDirection.Ascending, item => item.Slug);
		}

		#region Implementation of ITopicRepository

		public void Add(Topic topic)
		{
			DataSet.Add(topic);
		}

		public void Update(Topic topic)
		{
			DataSet.Update(topic);
		}

		public IEnumerable<Topic> GetByIds(List<Guid> ids)
		{
			return DataSet.Where(item => ids.Contains(item.Id));
		}

		public Topic GetBySlug(string slug)
		{
			return DataSet.FirstOrDefault(item => item.Slug == slug);
		}

		public IEnumerable<Topic> GetBySlugs(List<string> slugs)
		{
			return DataSet.Where(item => slugs.Contains(item.Slug));
		}

		public bool Exists(string slug)
		{
			return DataSet.Count(item => item.Slug == slug) > 0;
		}

		public void DeleteByIds(List<Guid> ids)
		{
			DataSet.Remove(item => ids.Contains(item.Id));
		}

		public Topic GetById(Guid guid)
		{
			return DataSet.FirstOrDefault(item => item.Id == guid);
		}

		#endregion
	}
}
