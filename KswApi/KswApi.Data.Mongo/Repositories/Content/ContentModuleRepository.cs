﻿using KswApi.Interface.Objects.Content;
using KswApi.Repositories.Interfaces.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KswApi.Data.Mongo.Repositories.Content
{
	internal class ContentModuleRepository: Base<ContentModule>, IContentModuleRepository
	{
		public void Add(ContentModule contentModule)
		{
			DataSet.Add(contentModule);
		}

		public void Update(ContentModule contentModule)
		{
			DataSet.Update(contentModule);
		}

		public void Delete(ContentModule contentModule)
		{
			DataSet.Remove(contentModule.Id);
		}

		public ContentModule GetById(Guid id)
		{
			return DataSet.SingleOrDefault(x => x.Id == id);
		}

		public ContentModule GetByLicenseId(Guid licenseId)
		{
			return DataSet.SingleOrDefault(x => x.LicenseId == licenseId);
		}

		public IEnumerable<ContentModule> GetByBucketId(Guid bucketId)
		{
			return DataSet.Where(x => x.BucketIds.Contains(bucketId));
		}
	}
}
