﻿using System.Collections.Generic;
using System.Linq;
using KswApi.Poco.ThreeM;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Data.Mongo.Repositories.Content
{
	internal class ThreeMItemRepository : Base<ThreeMItem>, IThreeMItemRepository
	{
		#region Implementation of IExternallyIndexedContentRepository

		public void Upsert(ThreeMItem content)
		{
			DataSet.Upsert(content);
		}

		public IEnumerable<ThreeMItem> Get(long previousItem)
		{
			return DataSet.Where(item => item.Id > previousItem);
		}

		public void Delete(long id)
		{
			DataSet.Remove(item => item.Id == id);
		}

		#endregion
	}
}
