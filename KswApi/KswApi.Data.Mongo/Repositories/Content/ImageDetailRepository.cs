﻿using KswApi.Interface.Enums;
using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces.Content;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KswApi.Data.Mongo.Repositories.Content
{
	class ImageDetailRepository : Base<ImageDetail>, IImageDetailRepository
	{
		public void Add(ImageDetail contentImageMeta)
		{
			DataSet.Add(contentImageMeta);
		}

		public void Update(ImageDetail contentImageMeta)
		{
			DataSet.Update(contentImageMeta);
		}

		public ImageDetail GetById(Guid id)
		{
			return DataSet.FirstOrDefault(item => item.Id == id);
		}

		public ImageDetail GetByPath(string bucketIdOrSlug, string imageIdOrSlug)
		{
			return DataSet.FirstOrDefault(item => item.Paths.Any(value => value.BucketIdOrSlug == bucketIdOrSlug && value.ContentIdOrSlug == imageIdOrSlug) && (item.Status == ObjectStatus.Active || item.Status == ObjectStatus.Uploading));
		}

		public IEnumerable<ImageDetail> GetByIds(List<Guid> ids)
		{
			return DataSet.Where(item => ids.Contains(item.Id));
		}

		public ImageDetail GetBySlug(string slug)
		{
			return DataSet.FirstOrDefault(item => item.Slug == slug);
		}

		public void DeleteById(Guid id)
		{
			DataSet.Remove(id);
		}

		public IEnumerable<ImageDetail> GetAll()
		{
			return DataSet;
		}

		public IEnumerable<ImageDetail> GetByBucketId(Guid id)
		{
			return DataSet.Where(item => item.BucketId == id);
		}
	}
}
