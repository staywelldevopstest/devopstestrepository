﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using KswApi.Data.Mongo.Framework;
using KswApi.Interface.Enums;
using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Data.Mongo.Repositories.Content
{
    internal class ContentPublishedRepository : Base<ContentPublished>, IContentPublishedRepository
    {
        #region Implementation of IPublishedContentVersionRepository

        public override void CreateIndexes(Interfaces.IMongoIndexer<ContentPublished> indexer)
        {
            indexer.EnsureIndex(IndexDirection.Ascending, item => item.BucketId, item => item.LegacyId);
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.BucketId, item => item.Slug);
        }

        public ContentPublished GetById(Guid id)
        {
            return DataSet.FirstOrDefault(item => item.Id == id);
        }

        public void Update(ContentPublished content)
        {
            DataSet.Update(content);
        }

        public bool Exists(Guid id)
        {
            return DataSet.Count(item => item.Id == id) > 0;
        }

        public IEnumerable<ContentPublished> GetByIds(List<Guid> ids)
        {
            return DataSet.Where(item => ids.Contains(item.Id));
        }

        public IEnumerable<ContentPublished> GetAll()
        {
            return DataSet;
        }

        public IEnumerable<ContentPublished> GetByBucketIdsAndSlugs(List<IdAndSlug> idsAndSlugs)
        {
            IEnumerable<Expression<Func<ContentPublished, bool>>> expressions = idsAndSlugs.Select(item =>
                (Expression<Func<ContentPublished, bool>>)(version => version.BucketId == item.Id && version.Slug == item.Slug && version.Status == ObjectStatus.Active)
            );

            return DataSet.Or(expressions);
        }

        public ContentPublished GetByPath(string bucketIdOrSlug, string contentIdOrSlug)
        {
            return DataSet.SingleOrDefault(item => item.Paths.Any(value => value.BucketIdOrSlug == bucketIdOrSlug && value.ContentIdOrSlug == contentIdOrSlug) && item.Status == ObjectStatus.Active);
        }

        public void DeleteById(Guid id)
        {
            DataSet.Remove(id);
        }

        public ContentPublished GetByBucketAndLegacyId(Guid bucketId, string legacyId)
        {
            return DataSet.SingleOrDefault(item => item.BucketId == bucketId && item.LegacyId == legacyId);
        }

        public void Add(ContentPublished content)
        {
            DataSet.Add(content);
        }

        #endregion
    }
}
