﻿using System;
using System.Collections.Generic;
using System.Linq;
using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Data.Mongo.Repositories.Content
{
	class ContentCoreRepository : Base<ContentCore>, IContentCoreRepository
	{
		#region Implementation of IContentMasterRepository

		public void Add(ContentCore core)
		{
			DataSet.Add(core);
		}

		public void Update(ContentCore core)
		{
			DataSet.Update(core);
		}

		public ContentCore GetById(Guid id)
		{
			return DataSet.FirstOrDefault(item => item.Id == id);
		}

		public IEnumerable<ContentCore> GetByIds(IEnumerable<Guid> ids)
		{
			return DataSet.Where(item => ids.Contains(item.Id));
		}

		public IEnumerable<ContentCore> GetByCopyrightId(Guid copyrightId)
		{
			return DataSet.Where(item => item.CopyrightId == copyrightId);
		}

		public void DeleteById(Guid id)
		{
			DataSet.Remove(id);
		}

		public IEnumerable<ContentCore> GetAll()
		{
			return DataSet;
		}

		#endregion
	}
}
