﻿using System;
using System.Collections.Generic;
using System.Linq;
using KswApi.Data.Mongo.Framework;
using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Data.Mongo.Repositories.Content
{
	class LicensedCollectionRepository : Base<LicensedCollection>, ILicensedCollectionRepository
	{
		public override void CreateIndexes(Interfaces.IMongoIndexer<LicensedCollection> indexer)
		{
			indexer.EnsureUnique(
				new IndexItem<LicensedCollection> { Direction = IndexDirection.Ascending, PropertyExpression = item => item.LicenseId },
				new IndexItem<LicensedCollection> { Direction = IndexDirection.Ascending, PropertyExpression = item => item.CollectionId });
		}

		#region Implementation of ILicensedCollectionRepository

		public IEnumerable<LicensedCollection> GetByLicenseId(Guid id)
		{
			return DataSet.Where(item => item.LicenseId == id);
		}

		public IEnumerable<LicensedCollection> GetByLicenseIdAndCollectionIds(Guid licenseId, List<Guid> collectionIds)
		{
			return DataSet.Where(item => item.LicenseId == licenseId && collectionIds.Contains(item.CollectionId));
		}

		public void Add(LicensedCollection collection)
		{
			DataSet.Add(collection);
		}

		public void DeleteByLicenseId(Guid id)
		{
			DataSet.Remove(item => item.LicenseId == id);
		}

		public void DeleteByCollectionId(Guid id)
		{
			DataSet.Remove(item => item.CollectionId == id);
		}

		public void DeleteByLicenseIdAndCollectionId(Guid licenseId, Guid collectionId)
		{
			DataSet.Remove(item => item.LicenseId == licenseId && item.CollectionId == collectionId);
		}

		public int CountByCollectionId(Guid id)
		{
			return DataSet.Count(item => item.CollectionId == id);
		}

		#endregion
	}
}
