﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using KswApi.Common.Extensions;
using KswApi.Data.Mongo.Framework;
using KswApi.Interface.Enums;
using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Data.Mongo.Repositories.Content
{
	internal class ContentBucketRepository : Base<ContentBucket>, IContentBucketRepository
	{
		public override void CreateIndexes(Interfaces.IMongoIndexer<ContentBucket> indexer)
		{
			indexer.EnsureIndex(IndexDirection.Ascending, item => item.Slug);
		}

		public void Add(ContentBucket bucket)
		{
			DataSet.Add(bucket);
		}

		public IEnumerable<ContentBucket> GetByStatus(ObjectStatus status, int offset, int count)
		{
			if (count == 0)
				return new ContentBucket[0];

			return DataSet.Where(item => item.Status == status).OrderBy(item => item.CaseInsensitiveName).Skip(offset).Take(count);
		}

		public ContentBucket GetByLegacyId(int id)
		{
            return DataSet.FirstOrDefault(item => item.LegacyId == id && item.Status == ObjectStatus.Active);
		}

		public IEnumerable<ContentBucket> GetByStatusAndSearch(ObjectStatus status, ContentBucketSearch search, int offset, int count)
		{
			return DataSet.Where(GetSearchExpression(status, search)).OrderBy(item => item.CaseInsensitiveName).Skip(offset).Take(count);
		}

		public IEnumerable<ContentBucket> GetByCopyrightIdForAllStatuses(Guid copyrightId)
		{
            return DataSet.Where(item => item.CopyrightId == copyrightId);
		}

		public ContentBucket GetById(Guid id)
		{
			return DataSet.SingleOrDefault(item => item.Id == id);
		}

        public IEnumerable<ContentBucket> GetByIds(IEnumerable<Guid> ids)
        {
            return DataSet.Where(item => ids.Contains(item.Id));
        }

        public IEnumerable<ContentBucket> GetBySlugs(IEnumerable<string> slugs)
        {
            return DataSet.Where(item => item.Status == ObjectStatus.Active && slugs.Contains(item.Slug));
        }

		public ContentBucket GetByStatusAndSlug(ObjectStatus status, string slug)
		{
			return DataSet.SingleOrDefault(item => item.Slug == slug && item.Status == status);
		}

		public void Update(ContentBucket bucket)
		{
			DataSet.Update(bucket);
		}

		public void Delete(ContentBucket bucket)
		{
			DataSet.Remove(bucket.Id);
		}

		public int CountByIdsAndStatus(List<Guid> ids, ObjectStatus status)
		{
			return DataSet.Count(item => ids.Contains(item.Id) && item.Status == status);
		}

		public int CountByStatus(ObjectStatus status)
		{
			return DataSet.Count(item => item.Status == status);
		}

		public int CountByStatusAndSearch(ObjectStatus status, ContentBucketSearch search)
		{
			return DataSet.Count(GetSearchExpression(status, search));
		}

		#region Private Methods

		private Expression<Func<ContentBucket, bool>> GetSearchExpression(ObjectStatus status, ContentBucketSearch search)
		{
			ParameterExpression parameter = Expression.Parameter(typeof(ContentBucket));

			List<Expression> expressions = new List<Expression>();

			if (!string.IsNullOrEmpty(search.Query))
			{
				Expression<Func<ContentBucket, string>> property = item => item.CaseInsensitiveName;

				expressions.Add(Expression.Call(
					Expression.Property(parameter, property.GetProperty()),
					"Contains",
					null,
					Expression.Constant(search.Query)));
			}

			if (search.ReadOnly.HasValue)
			{
				Expression<Func<ContentBucket, bool>> property = item => item.ReadOnly;
				expressions.Add(Expression.Equal(Expression.Property(parameter, property.GetProperty()), Expression.Constant(search.ReadOnly.Value)));
			}

			if (search.Origin.HasValue)
			{
				Expression<Func<ContentBucket, Guid>> property = item => item.OriginId;
				expressions.Add(Expression.Equal(Expression.Property(parameter, property.GetProperty()), Expression.Constant(search.Origin.Value)));
			}

			if (search.LegacyId.HasValue)
			{
				Expression<Func<ContentBucket, int?>> property = item => item.LegacyId;
				expressions.Add(Expression.Equal(Expression.Property(parameter, property.GetProperty()), Expression.Constant(search.LegacyId, typeof(int?))));
			}

			if (search.Type != null && search.Type.Count > 0)
			{
				Expression<Func<ContentBucket, ContentType>> property = item => item.Type;
				Expression combinedExpression = null;
				foreach (ContentType type in search.Type)
				{
					Expression typeExpression = Expression.Equal(Expression.Property(parameter, property.GetProperty()), Expression.Constant(type, typeof(ContentType)));
					combinedExpression = combinedExpression == null ?
						typeExpression :
						Expression.OrElse(combinedExpression, typeExpression);
				}

				expressions.Add(combinedExpression);
			}

			if (search.CopyrightId.HasValue)
			{
				Expression<Func<ContentBucket, Guid?>> property = item => item.CopyrightId;
				expressions.Add(Expression.Equal(Expression.Property(parameter, property.GetProperty()), Expression.Constant(search.CopyrightId, typeof(Guid?))));
			}
			else if (search.DefaultCopyright)
			{
				Expression<Func<ContentBucket, Guid?>> property = item => item.CopyrightId;
				expressions.Add(Expression.Equal(Expression.Property(parameter, property.GetProperty()), Expression.Constant(null, typeof(Guid?))));
			}

			Expression<Func<ContentBucket, ObjectStatus>> statusProperty = item => item.Status;
			Expression expression = Expression.Equal(Expression.Property(parameter, statusProperty.GetProperty()), Expression.Constant(status));

			foreach (Expression current in expressions)
				expression = Expression.AndAlso(current, expression);

			return Expression.Lambda<Func<ContentBucket, bool>>(expression, parameter);
		}

		#endregion
	}
}
