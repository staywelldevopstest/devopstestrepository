﻿using System;
using System.Collections.Generic;
using System.Linq;
using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Data.Mongo.Repositories.Content
{
	class ContentSegmentRepository : Base<ContentSegment>, IContentSegmentRepository
	{
		public void Add(ContentSegment content)
		{
			DataSet.Add(content);
		}

		public void Update(ContentSegment content)
		{
			DataSet.Update(content);
		}

		public void DeleteById(Guid id)
		{
			DataSet.Remove(id);
		}

		public ContentSegment GetById(Guid id)
		{
			return DataSet.SingleOrDefault(item => item.Id == id);
		}

		public IEnumerable<ContentSegment> GetByContentId(Guid contentId)
		{
			return DataSet.Where(item => item.ContentId == contentId);
		}
	}
}
