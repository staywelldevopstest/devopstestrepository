﻿using System;
using System.Collections.Generic;
using System.Linq;
using KswApi.Poco.Content;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Data.Mongo.Repositories.Content
{
	internal class ContentOriginRepository : Base<ContentOrigin>, IContentOriginRepository
	{
		public void Add(ContentOrigin origin)
		{
			DataSet.Add(origin);
		}

		public ContentOrigin GetById(Guid id)
		{
			return DataSet.SingleOrDefault(item => item.Id == id);
		}

		public void Update(ContentOrigin origin)
		{
			DataSet.Update(origin);
		}

		public void Delete(ContentOrigin origin)
		{
			DataSet.Remove(origin.Id);
		}

		public IEnumerable<ContentOrigin> GetAll()
		{
			return DataSet;
		}

		public IEnumerable<ContentOrigin> GetByQuery(string query)
		{
			return DataSet.Where(item => item.CaseInsensitiveName.Contains(query));
		}
	}
}
