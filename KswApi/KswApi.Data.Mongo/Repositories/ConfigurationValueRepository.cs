﻿using System.Collections.Generic;
using System.Linq;
using KswApi.Poco;
using KswApi.Repositories.Interfaces;

namespace KswApi.Data.Mongo.Repositories
{
	internal class ConfigurationValueRepository : ExtendedBase<ConfigurationValue<object>>, IConfigurationValueRepository
	{
		#region Implementation of IConfigurationValueRepository

		public ConfigurationValue<TData> Get<TData>(string id)
		{
			return DataSet<ConfigurationValue<TData>>().SingleOrDefault(item => item.Id == id);
		}

		public void Add<TData>(ConfigurationValue<TData> value)
		{
			DataSet<ConfigurationValue<TData>>().Add(value);
		}

		public void Update<TData>(ConfigurationValue<TData> value)
		{
			DataSet<ConfigurationValue<TData>>().Update(value);
		}

        //public IEnumerable<ConfigurationValue<TData>> GetByType<TData>(Poco.Enums.ConfigurationValueType type)
        //{
        //    return DataSet<ConfigurationValue<TData>>().Where(item => item.Type == type);
        //}

        #endregion
    }
}
