﻿using KswApi.Interface.Enums;
using KswApi.Poco.Sms;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KswApi.Data.Mongo.Repositories
{
	internal class SmsNumberDataRepository : Base<SmsNumberData>, ISmsNumberDataRepository
	{
		public void Add(SmsNumberData number)
		{
			DataSet.Add(number);
		}

		public void Update(SmsNumberData number)
		{
			DataSet.Update(number);
		}

		public void Delete(SmsNumberData number)
		{
			DataSet.Remove(number.Id);
		}

		public IEnumerable<SmsNumberData> GetByLicenseId(Guid licenseId)
		{
			return DataSet.Where(item => item.LicenseId == licenseId);
		}

		public SmsNumberData GetByNumberAndLicenseId(string number, Guid licenseId)
		{
			return DataSet.SingleOrDefault(item => item.Number == number && item.LicenseId == licenseId);
		}

		public IEnumerable<SmsNumberData> GetByNumber(string number)
		{
			return DataSet.Where(item => item.Number == number);
		}

		public SmsNumberData GetOneByNumber(string number)
		{
			// to get a single result or null if multiple:
			List<SmsNumberData> numbers = DataSet.Where(item => item.Number == number).Take(2).ToList();
			return numbers.Count == 1 ? numbers[0] : null;
		}

		public IEnumerable<SmsNumberData> GetByNumberAndRouteType(string number, SmsNumberRouteType routeType)
		{
			return DataSet.Where(item => item.RouteType == routeType && item.Number == number);
		}

		public SmsNumberData GetById(Guid id)
		{
			return DataSet.SingleOrDefault(item => item.Id == id);
		}
	}
}
