﻿namespace KswApi.Data.Mongo.Enums
{
	internal enum UpdateExecutionState
	{
		Pending = 1,
		Executing = 2,
		Applied = 3,
		Failed = 4
	}
}
