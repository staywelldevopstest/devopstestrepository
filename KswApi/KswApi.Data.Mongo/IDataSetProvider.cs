﻿namespace KswApi.Data.Mongo
{
    public interface IDataSetProvider
    {
        IDataSet<T> GetSet<T>();
        void Commit();
    }
}
