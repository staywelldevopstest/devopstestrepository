﻿using KswApi.Poco.Enums;
using KswApi.Repositories.Objects;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace KswApi.Data.Mongo.DatabaseUpdates
{
    class ClientUserStateUpdate : ISchemaUpdate
    {
        public bool Update(MongoDatabase database)
        {
            MongoCollection<ClientUser> userCollection = database.GetCollection<ClientUser>(typeof(ClientUser).Name);

            MongoCursor cursor = userCollection.Find(Query.NotExists("State"));

            foreach (ClientUser user in cursor)
            {
                if (user.Disabled == null) continue;

                if (user.State == ClientUserState.Deleted) continue;

                user.State = user.Disabled.Value ? ClientUserState.Disabled : ClientUserState.Active;
                user.Disabled = null;

                userCollection.Save(user);
            }

            return true;
        }

        public string Description
        {
            get { return "Updates the users state to facilitate the soft deleting of users."; }
        }
    }
}
