﻿using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Driver;

namespace KswApi.Data.Mongo.DatabaseUpdates
{
	/// <summary>
	/// This is done to allow proper precision: Mongo date times have less precision
	/// than .NET date times.
	/// </summary>
	public class ConvertDateTimesToStringsUpdate : ISchemaUpdate
	{
		public bool Update(MongoDatabase database)
		{
			IEnumerable<string> collectionNames = database.GetCollectionNames();
			foreach (string collectionName in collectionNames)
			{
				MongoCollection collection = database.GetCollection(collectionName);
				MongoCursor<BsonDocument> cursor = collection.FindAllAs<BsonDocument>();
				foreach (BsonDocument document in cursor)
				{
					ConvertDocument(document);
					collection.Save(document);
				}
			}

			return true;
		}

		public string Description
		{
			get { return "Update all date times to use strings for precision reasons."; }
		}

		private void ConvertDocument(BsonDocument document)
		{
			foreach (BsonElement element in document.Elements)
			{
				if (element.Value.IsBsonDocument)
					ConvertDocument(element.Value.AsBsonDocument);
				else if (element.Value.BsonType == BsonType.DateTime)
				{
					element.Value = new BsonString(element.Value.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ss.FFFFFFFK"));
				}
			}
		}
	}
}
