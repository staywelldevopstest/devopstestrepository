﻿using System;
using KswApi.Interface.Objects;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace KswApi.Data.Mongo.DatabaseUpdates
{
	class AddLicenseIdToApplicationUpdate : ISchemaUpdate
	{
		public bool Update(MongoDatabase database)
		{
			MongoCollection<License> licenseCollection = database.GetCollection<License>(typeof(License).Name);

			MongoCollection<Application> applicationCollection = database.GetCollection<Application>(typeof(Application).Name);

			foreach (License license in licenseCollection.FindAll())
			{
				if (license.Applications == null)
					continue;

				foreach (Guid applicationId in license.Applications)
				{
					Application application = applicationCollection.FindOne(Query<Application>.EQ(item => item.Id, applicationId));
					
					if (application == null)
						continue;

					application.LicenseId = license.Id;

					applicationCollection.Save(application);
				}
			}

			return true;
		}

		public string Description { get { return "Adds the correct LicenseId to the corresponding Application."; } }
	}
}
