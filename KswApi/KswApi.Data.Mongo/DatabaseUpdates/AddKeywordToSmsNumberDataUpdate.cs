﻿using System.Collections.Generic;
using KswApi.Interface.Enums;
using KswApi.Poco.Sms;
using KswApi.Repositories.Objects;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace KswApi.Data.Mongo.DatabaseUpdates
{
	class AddKeywordToSmsNumberDataUpdate : ISchemaUpdate
	{
		public bool Update(MongoDatabase database)
		{
			MongoCollection<SmsKeywordData> keywordCollection = database.GetCollection<SmsKeywordData>(typeof(SmsKeywordData).Name);

			MongoCollection<SmsNumberData> numberCollection = database.GetCollection<SmsNumberData>(typeof(SmsNumberData).Name);

			MongoCursor numberCursor = numberCollection.Find(Query.NotExists("Keywords"));

			foreach (SmsNumberData number in numberCursor)
			{
				if (number.RouteType == SmsNumberRouteType.CatchAll)
				{
					number.Keywords = null;
					numberCollection.Save(number);
				}
				else
				{
					number.Keywords = new List<SmsNumberKeywordItem>();

					MongoCursor<SmsKeywordData> keywords = keywordCollection.Find(Query.EQ("NumberId", new BsonBinaryData(number.Id)));

					foreach (SmsKeywordData keyword in keywords)
					{
						number.Keywords.Add(new SmsNumberKeywordItem { Keyword = keyword.Keyword, KeywordId = keyword.Id });
					}

					numberCollection.Save(number);
				}
			}

			return true;
		}

		public string Description
		{
			get { return "Adds the existing keywords in SmsKeywordData to the Keywords list in SmsKeywordNumber."; }
		}
	}
}
