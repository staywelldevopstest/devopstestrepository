﻿using MongoDB.Driver;

namespace KswApi.Data.Mongo.DatabaseUpdates
{
	class RemoveLegacyContentBodyUpdate : ISchemaUpdate
	{
		public bool Update(MongoDatabase database)
		{
			if (database.CollectionExists("ContentBody"))
				database.DropCollection("ContentBody");

			return true;
		}

		public string Description { get { return "Removes the ContentBody collection which is no longer used."; } }
	}
}
