﻿using KswApi.Interface.Objects.Content;
using KswApi.Poco.Content;
using MongoDB.Driver;

namespace KswApi.Data.Mongo.DatabaseUpdates
{
    public class UpdateBucketSegmentSlugsToLowercase : ISchemaUpdate
    {
        public bool Update(MongoDatabase database)
        {
            MongoCollection<ContentBucket> buckets = database.GetCollection<ContentBucket>(typeof(ContentBucket).Name);

            MongoCursor cursor = buckets.FindAll();

            foreach (ContentBucket bucket in cursor)
            {
                foreach (ContentBucketSegment segment in bucket.Segments)
                {
                    segment.Slug = segment.Slug.ToLower();
                }

                buckets.Save(bucket);
            }

            return true;
        }

        public string Description
        {
            get { return "Update all bucket segment slugs to lowercase."; }
        }
    }
}
