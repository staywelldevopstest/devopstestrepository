﻿using KswApi.Common.Objects;
using MongoDB.Bson;
using MongoDB.Driver;

namespace KswApi.Data.Mongo.DatabaseUpdates
{
	class ConvertAccessTokenRightsToList : ISchemaUpdate
	{
		public bool Update(MongoDatabase database)
		{
			MongoCollection accessTokenCollection = database.GetCollection(typeof(AccessToken).Name);

			MongoCursor<BsonDocument> accessTokens = accessTokenCollection.FindAllAs<BsonDocument>();

			foreach (BsonDocument accessToken in accessTokens)
			{
				BsonElement element = accessToken.GetElement("Rights");
				if (element != null && element.Value.BsonType == BsonType.String)
				{
					string value = element.Value.AsString;
					element.Value = string.IsNullOrEmpty(value) ?
						new BsonArray(0) :
						new BsonArray(value.Split(','));

					accessTokenCollection.Save(accessToken);
				}
			}

			return true;

		}

		public string Description { get { return "Converts existing access token right strings to lists."; } }

	}
}
