﻿using KswApi.Interface.Objects;
using MongoDB.Driver;

namespace KswApi.Data.Mongo.DatabaseUpdates
{
	class AddCaseInsensitiveClientName : ISchemaUpdate
	{
		public bool Update(MongoDatabase database)
		{
			MongoCollection<Client> clientCollection = database.GetCollection<Client>(typeof(Client).Name);

			MongoCursor<Client> numberCursor = clientCollection.FindAllAs<Client>();

			foreach (Client client in numberCursor)
			{
				if (client.ClientName != null)
					client.CaseInsensitiveName = client.ClientName.ToLower();
				clientCollection.Save(client);
			}

			return true;

		}

		public string Description { get { return "Adds a case-insensitive property to Clients so that they can be sorted properly alphabetically."; } }
	}
}
