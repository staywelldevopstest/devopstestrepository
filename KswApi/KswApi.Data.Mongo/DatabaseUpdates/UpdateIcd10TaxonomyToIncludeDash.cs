﻿using System.Collections.Generic;
using KswApi.Logic.Framework.Constants;
using KswApi.Poco.Content;
using MongoDB.Driver;

namespace KswApi.Data.Mongo.DatabaseUpdates
{
	class UpdateIcd10TaxonomyToIncludeDash : ISchemaUpdate
	{
		#region Implementation of ISchemaUpdate

		public bool Update(MongoDatabase database)
		{
			MongoCollection<ContentCore> coreCollection = database.GetCollection<ContentCore>(typeof(ContentCore).Name);

			foreach (ContentCore core in coreCollection.FindAll())
			{
				if (core.Taxonomies == null)
					continue;

				if (!core.Taxonomies.ContainsKey("icd-10cm") && !core.Taxonomies.ContainsKey("icd-10pcs"))
					continue;
				List<TaxonomyValue> list;
				
				if (core.Taxonomies.TryGetValue("icd-10cm", out list))
				{
					core.Taxonomies.Remove("icd-10cm");
					core.Taxonomies[Constant.Taxonomies.Slugs.ICD_10_CM] = list;
				}

				if (core.Taxonomies.TryGetValue("icd-10pcs", out list))
				{
					core.Taxonomies.Remove("icd-10pcs");
					core.Taxonomies[Constant.Taxonomies.Slugs.ICD_10_PCS] = list;
				}

				coreCollection.Save(core);
			}

			return true;
		}

		public string Description { get { return "Modifies icd 10 codes from icd-10cm and icd-10pcs to icd-10-cm and icd-10-pcs."; } }

		#endregion
	}
}
