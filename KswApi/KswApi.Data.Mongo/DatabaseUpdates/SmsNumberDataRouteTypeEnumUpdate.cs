﻿using KswApi.Interface.Enums;
using KswApi.Poco.Sms;
using KswApi.Repositories.Objects;
using MongoDB.Driver;

namespace KswApi.Data.Mongo.DatabaseUpdates
{
	class SmsNumberDataRouteTypeEnumUpdate : ISchemaUpdate
	{
		public bool Update(MongoDatabase database)
		{
			MongoCollection<SmsNumberData> numberCollection = database.GetCollection<SmsNumberData>(typeof(SmsNumberData).Name);

			MongoCursor numberCursor = numberCollection.FindAll();

			foreach (SmsNumberData number in numberCursor)
			{
				switch ((int) number.RouteType)
				{
					case 0:
						number.RouteType = SmsNumberRouteType.Keyword;
						break;
					case 1:
						number.RouteType = SmsNumberRouteType.CatchAll;
						break;
					default:
						continue;
				}

				numberCollection.Save(number);
			}

			return true;
		}

		public string Description
		{
			get { return "Updates SmsNumberData to use the new route type enum, which includes the \"unknown\" value"; }
		}
	}
}
