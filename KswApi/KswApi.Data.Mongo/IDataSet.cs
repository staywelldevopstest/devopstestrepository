﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace KswApi.Data.Mongo
{
    public interface IDataSet<T> : IOrderedQueryable<T>
    {
		IEnumerable<Guid> IdsWhere(Expression<Func<T, bool>> expression);
	    T PropertiesOf(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] properties);
	    IEnumerable<T> Or(IEnumerable<Expression<Func<T, bool>>> expressions);
		IEnumerable<T> And(IEnumerable<Expression<Func<T, bool>>> expressions);
        void Add(T item);
        void Update(T item);
		long Update<TMember>(Expression<Func<T, bool>> expression, Expression<Func<T, TMember>> memberExpression, TMember value);
	    void Upsert(T item);
	    void Increment(Expression<Func<T, bool>> expression, Expression<Func<T, int>> propertyExpression, int count);
		void Increment(Expression<Func<T, bool>> expression, Expression<Func<T, long>> propertyExpression, long count);
        void Remove(Guid id);
	    void Remove(Expression<Func<T, bool>> expression);
    }
}
