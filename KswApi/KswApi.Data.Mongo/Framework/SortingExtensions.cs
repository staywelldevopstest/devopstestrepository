﻿using System;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using KswApi.Common.Extensions;
using KswApi.Interface.Objects;
using KswApi.Poco.Logging;
using KswApi.Repositories.Enums;
using KswApi.Repositories.Objects;

namespace KswApi.Data.Mongo.Framework
{
	static internal class SortingExtensions
	{
		private static MethodInfo _orderByMethod;
		private static MethodInfo OrderByMethod
		{
			get
			{
				return _orderByMethod ??
				       (_orderByMethod = typeof (Queryable).GetMethods(BindingFlags.Public | BindingFlags.Static)
							.First(item => item.Name == "OrderBy" && item.GetParameters().Length == 2));
			}
		}

		private static MethodInfo _orderByDescendingMethod;
		private static MethodInfo OrderByDescendingMethod
		{
			get
			{
				return _orderByDescendingMethod ??
					   (_orderByDescendingMethod = typeof(Queryable).GetMethods(BindingFlags.Public | BindingFlags.Static)
							.First(item => item.Name == "OrderByDescending" && item.GetParameters().Length == 2));
			}
		}

		public static IQueryable<Log> SortBy(this IQueryable<Log> queryable, LogSortOption sort)
		{
			switch (sort)
			{
				case LogSortOption.DateDescending:
					return queryable.OrderByDescending(item => item.Date);
				case LogSortOption.DateAscending:
					return queryable.OrderBy(item => item.Date);
				default:
					throw new ArgumentOutOfRangeException("sort");
			}
		}

		public static IOrderedQueryable<T> SortBy<T>(this IQueryable<T> queryable, SortOption<T> sortOption)
		{
			MethodInfo method;
			
			switch (sortOption.Direction)
			{
				case SortDirection.Ascending:
					method = OrderByMethod;
					break;
				case SortDirection.Descending:
					method = OrderByDescendingMethod;
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			Expression expression = sortOption.Expression;
			
			if (expression == null)
			{
				ParameterExpression parameter = Expression.Parameter(typeof (T));
				expression = Expression.Lambda(Expression.Property(parameter, sortOption.Property), parameter);
			}
			
			return (IOrderedQueryable<T>) queryable.Provider.CreateQuery<T>(
				Expression.Call(
					null,
					method.MakeGenericMethod(typeof (T), sortOption.Property.PropertyType),
					new[]
						{
							queryable.Expression,
							Expression.Quote(expression)
						}
					));
		}
	}
}
