﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace KswApi.Data.Mongo.Framework
{
	internal class MultilevelExpression<T>
	{
		private readonly List<Expression> _expressions;
		private readonly IndexDirection _direction;

		private MultilevelExpression(List<Expression> expressions, IndexDirection direction)
		{
			_expressions = expressions;
			_direction = direction;
		}

		public static Part<T, TProperty> WithProperty<TProperty>(Expression<Func<T, TProperty>> expression)
		{
			return new Part<T, TProperty>(expression);
		}

		public static Part<T, TProperty> WithEnumerable<TProperty>(Expression<Func<T, IEnumerable<TProperty>>> expression)
		{
			return new Part<T, TProperty>(expression);
		}

		public IEnumerable<Expression> Expressions
		{
			get { return _expressions; }
		}

		public IndexDirection Direction
		{
			get { return _direction; }	
		}

		internal class Part<TRoot, TCurrent>
		{
			private readonly List<Expression> _expressions;

			public Part(Expression<Func<T, TCurrent>> expression)
			{
				_expressions = new List<Expression>() { expression };
			}

			public Part(Expression<Func<T, IEnumerable<TCurrent>>> expression)
			{
				_expressions = new List<Expression>() { expression };
			}

			private Part(List<Expression> expressions)
			{
				_expressions = expressions;
			}

			public MultilevelExpression<TRoot> Ascending()
			{
				return new MultilevelExpression<TRoot>(_expressions, IndexDirection.Ascending);
			}

			public MultilevelExpression<TRoot> Descending()
			{
				return new MultilevelExpression<TRoot>(_expressions, IndexDirection.Descending);
			}

			public Part<TRoot, TProperty> WithProperty<TProperty>(Expression<Func<TCurrent, TProperty>> expression)
			{
				_expressions.Add(expression);
				return new Part<TRoot, TProperty>(_expressions);
			}

			public Part<TRoot, TProperty> WithEnumerable<TProperty>(Expression<Func<TCurrent, IEnumerable<TProperty>>> expression)
			{
				_expressions.Add(expression);
				return new Part<TRoot, TProperty>(_expressions);
			}
		}
	}

}
