﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using KswApi.Data.Mongo.Constants;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;

namespace KswApi.Data.Mongo.Framework
{
	internal class MongoDataSet<T> : IDataSet<T>
	{
		private readonly MongoCollection<T> _collection;
		private IQueryable<T> _queryable;

		private IQueryable<T> Queryable
		{
			get { return _queryable ?? (_queryable = new MongoQueryable<T>(new MongoQueryProvider(_collection))); }
		}

		public MongoDataSet(MongoCollection<T> collection)
		{
			_collection = collection;
		}

		public void Add(T item)
		{
			_collection.Insert(item);
		}

		public void Update(T item)
		{
			_collection.Save(item);
		}

		public long Update<TMember>(Expression<Func<T, bool>> expression, Expression<Func<T, TMember>> memberExpression, TMember value)
		{
			IMongoQuery query = Query<T>.Where(expression);

			UpdateBuilder<T> builder = new UpdateBuilder<T>();

			builder.Set(memberExpression, value);

			WriteConcernResult result = _collection.Update(query, builder);

			return result.DocumentsAffected;
		}

		public void Upsert(T item)
		{
			_collection.Save(item);
		}

		public IEnumerable<T> Or(IEnumerable<Expression<Func<T, bool>>> expressions)
		{
			return _collection.Find(Query.Or(expressions.Select(Query<T>.Where)));
		}

		public IEnumerable<T> And(IEnumerable<Expression<Func<T, bool>>> expressions)
		{
			return _collection.Find(Query.And(expressions.Select(Query<T>.Where)));
		}

		public void Increment(Expression<Func<T, bool>> expression, Expression<Func<T, int>> propertyExpression, int count)
		{
			_collection.FindAndModify(Query<T>.Where(expression), SortBy.Null, MongoDB.Driver.Builders.Update<T>.Inc(propertyExpression, count));
		}

		public void Increment(Expression<Func<T, bool>> expression, Expression<Func<T, long>> propertyExpression, long count)
		{
			_collection.FindAndModify(Query<T>.Where(expression), SortBy.Null, MongoDB.Driver.Builders.Update<T>.Inc(propertyExpression, count));
		}

		public IEnumerable<Guid> IdsWhere(Expression<Func<T, bool>> expression)
		{
			MongoCursor<Guid> cursor = _collection.FindAs<Guid>(Query<T>.Where(expression));
			cursor.SetFields(Fields.Include("_id"));
			return cursor;
		}

		public IEnumerable<T> PropertiesWhere(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] properties)
		{
			MongoCursor<T> cursor = _collection.FindAs<T>(Query<T>.Where(expression));
			cursor.SetFields(Fields<T>.Include(properties));
			return cursor;
		}

		public T PropertiesOf(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] properties)
		{
			return _collection.FindAs<T>(Query<T>.Where(expression)).SetLimit(1).SetFields(Fields<T>.Include(properties)).FirstOrDefault();
		}

		public void Remove(Guid id)
		{
			IMongoQuery query = Query.EQ(Constant.MongoId, id);

			_collection.Remove(query);
		}

		public void Remove(Expression<Func<T, bool>> expression)
		{
			IMongoQuery query = Query<T>.Where(expression);

			_collection.Remove(query);
		}

		public IEnumerator<T> GetEnumerator()
		{
			return Queryable.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return Queryable.GetEnumerator();
		}

		public Expression Expression { get { return Queryable.Expression; } }

		public Type ElementType { get { return Queryable.ElementType; } }

		public IQueryProvider Provider { get { return Queryable.Provider; } }
	}
}
