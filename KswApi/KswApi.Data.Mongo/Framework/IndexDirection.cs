﻿namespace KswApi.Data.Mongo.Framework
{
	public enum IndexDirection
	{
		Ascending,
		Descending
	}
}
