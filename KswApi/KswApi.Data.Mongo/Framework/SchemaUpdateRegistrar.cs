﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Threading;
using KswApi.Common.Configuration;
using KswApi.Data.Mongo.Constants;
using KswApi.Data.Mongo.Enums;
using KswApi.Data.Mongo.Interfaces;
using KswApi.Data.Mongo.Objects;
using KswApi.Logging;
using KswApi.Reflection;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace KswApi.Data.Mongo.Framework
{
	internal class SchemaUpdateRegistrar
	{
		#region Constants

		private const int PENDING_TIMEOUT_IN_SECONDS = 10;
		private const int PENDING_WAIT_BETWEEN_RETRIES_IN_SECONDS = 1;
		private const int EXECUTING_TIMEOUT_IN_MINUTES = 5;
		private const int EXECUTING_WAIT_BETWEEN_RETRIES_IN_SECONDS = 5;

		#endregion

		#region Class Member Fields

		private MongoDatabase _database;
		private MongoCollection<MongoSchemaUpdate> _updateCollection;

		#endregion

		#region Constructors

		public SchemaUpdateRegistrar()
		{
			InitializeDatabase();
		}

		#endregion

		#region Public Methods

		public void RegisterUpdate<TUpdate>() where TUpdate : ISchemaUpdate, new()
		{
			if (!WaitForUpdateOk<TUpdate>())
				return;

			TUpdate update = new TUpdate();

			MongoSchemaUpdate updateSlot = GetUpdateSlot(update);

			if (updateSlot == null)
				return;

			try
			{
				update.Update(_database);

				updateSlot.State = UpdateExecutionState.Applied;

				_updateCollection.Save(updateSlot);

				Logger.Log(Severity.Information, string.Format("Database update applied: {0}.", typeof(TUpdate).Name), update.Description);
			}
			catch (Exception exception)
			{
				updateSlot.Message = exception.ToString();

				_updateCollection.Save(updateSlot);

				Logger.Log(exception);
			}
		}

		#endregion

		#region Private Methods

		private void InitializeDatabase()
		{
			ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[Constant.MainConnectionStringName];

			if (settings == null || string.IsNullOrEmpty(settings.ConnectionString))
				throw new ConfigurationErrorsException("No connection string found");

			string databaseName = Settings.Current.Database.Name;

			MongoClient client = new MongoClient(settings.ConnectionString);

			MongoServer server = client.GetServer();

			_database = server.GetDatabase(databaseName);

			_updateCollection = _database.GetCollection<MongoSchemaUpdate>(typeof(MongoSchemaUpdate).Name);

			_updateCollection.EnsureIndex(IndexKeys<MongoSchemaUpdate>.Ascending(item => item.Name));
		}

		private bool WaitForUpdateOk<TUpdate>() where TUpdate : ISchemaUpdate, new()
		{
			while (true)
			{
				List<MongoSchemaUpdate> schemaUpdates =
					_updateCollection.Find(Query<MongoSchemaUpdate>.EQ(item => item.Name, typeof(TUpdate).Name)).ToList();

				// an update has completed, no need to apply
				if (schemaUpdates.Any(item => item.State == UpdateExecutionState.Applied))
					return false;

				// no active updates, continue
				if (schemaUpdates.All(item => item.State == UpdateExecutionState.Failed))
					return true;

				MongoSchemaUpdate executingUpdate = schemaUpdates.FirstOrDefault(item => item.State == UpdateExecutionState.Executing);

				if (executingUpdate != null)
				{
					WaitForExecutingUpdate(executingUpdate);
					continue;
				}

				MongoSchemaUpdate pendingUpdate = schemaUpdates.FirstOrDefault(item => item.State == UpdateExecutionState.Pending);

				if (pendingUpdate != null)
					WaitForPendingUpdate(pendingUpdate);
			}
		}

		private bool WaitForExecutingUpdate(MongoSchemaUpdate schemaUpdate)
		{
			DateTime start = DateTime.UtcNow;

			while (true)
			{
				Thread.Sleep(TimeSpan.FromSeconds(EXECUTING_WAIT_BETWEEN_RETRIES_IN_SECONDS));

				schemaUpdate = _updateCollection.FindOne(Query<MongoSchemaUpdate>.EQ(item => item.Id, schemaUpdate.Id));

				if (schemaUpdate == null)
					return false;

				if (schemaUpdate.State == UpdateExecutionState.Applied)
					return true;

				if (schemaUpdate.State != UpdateExecutionState.Executing)
					return false;

				if (DateTime.UtcNow - start < TimeSpan.FromMinutes(EXECUTING_TIMEOUT_IN_MINUTES))
				{
					schemaUpdate.State = UpdateExecutionState.Failed;

					schemaUpdate.Message = string.Format("Timed out at {0}.", DateTime.UtcNow);

					_updateCollection.Save(schemaUpdate);

					return false;
				}
			}
		}

		private void WaitForPendingUpdate(MongoSchemaUpdate schemaUpdate)
		{
			DateTime start = DateTime.UtcNow;

			while (true)
			{
				Thread.Sleep(TimeSpan.FromSeconds(PENDING_WAIT_BETWEEN_RETRIES_IN_SECONDS));

				schemaUpdate = _updateCollection.FindOne(Query<MongoSchemaUpdate>.EQ(item => item.Id, schemaUpdate.Id));

				if (schemaUpdate == null)
					return;

				if (schemaUpdate.State == UpdateExecutionState.Executing)
					return;

				if (schemaUpdate.State != UpdateExecutionState.Pending)
					return;

				if (DateTime.UtcNow - start < TimeSpan.FromSeconds(PENDING_TIMEOUT_IN_SECONDS))
				{
					schemaUpdate.State = UpdateExecutionState.Failed;

					schemaUpdate.Message = string.Format("Timed out at {0}.", DateTime.UtcNow);

					_updateCollection.Save(schemaUpdate);

					return;
				}
			}
		}

		private MongoSchemaUpdate GetUpdateSlot<TUpdate>(TUpdate update) where TUpdate : ISchemaUpdate, new()
		{
			MongoSchemaUpdate newSchemaUpdate = new MongoSchemaUpdate
			{
				Name = typeof(TUpdate).Name,
				Description = update.Description,
				ExecutionDate = DateTime.UtcNow,
				State = UpdateExecutionState.Pending
			};

			_updateCollection.Save(newSchemaUpdate);

			while (true)
			{
				List<MongoSchemaUpdate> schemaUpdates = _updateCollection.Find(Query<MongoSchemaUpdate>.EQ(item => item.Name, typeof(TUpdate).Name)).ToList();

				if (schemaUpdates.Where(item => item.State != UpdateExecutionState.Failed).All(item => item.Id == newSchemaUpdate.Id))
				{
					newSchemaUpdate.State = UpdateExecutionState.Executing;
					_updateCollection.Save(newSchemaUpdate);
					return newSchemaUpdate;
				}

				if (schemaUpdates.Any(item => item.State == UpdateExecutionState.Applied))
					return null;

				MongoSchemaUpdate executingUpdate = schemaUpdates.FirstOrDefault(item => item.State == UpdateExecutionState.Executing);

				if (executingUpdate != null)
				{
					if (!WaitForExecutingUpdate(executingUpdate))
						return null;

					continue;
				}

				// multiple pending updates, remove the current one and re-check
				_updateCollection.Remove(Query<MongoSchemaUpdate>.EQ(item => item.Id, newSchemaUpdate.Id));

				Random random = new Random();

				int randomWait = random.Next(0, PENDING_TIMEOUT_IN_SECONDS);

				Thread.Sleep(TimeSpan.FromSeconds(randomWait));

				if (!WaitForUpdateOk<TUpdate>())
					return null;

				newSchemaUpdate.Id = default(Guid);

				_updateCollection.Save(newSchemaUpdate);
			}
		}

		#endregion
	}
}
