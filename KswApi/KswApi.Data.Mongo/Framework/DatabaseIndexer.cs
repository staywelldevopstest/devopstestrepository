﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using KswApi.Common.Configuration;
using KswApi.Data.Mongo.Constants;
using KswApi.Data.Mongo.Interfaces;
using KswApi.Logging;
using KswApi.Reflection;
using MongoDB.Driver;
using MongoDB.Driver.Builders;

namespace KswApi.Data.Mongo.Framework
{
	public class DatabaseIndexer
	{
		private MongoDatabase _database;

		public DatabaseIndexer()
		{
			InitializeDatabase();
		}

		public void RegisterIndexes()
		{
			Type[] types = Assembly.GetExecutingAssembly().GetTypes();

			foreach (Type type in types)
			{
				Type mongoCreatorInterface = type.GetInterfaces().FirstOrDefault(item => item.IsGenericType
						&& item.GetGenericTypeDefinition() == typeof(IMongoIndexCreator<>));

				if (mongoCreatorInterface == null)
					continue;

				ConstructorInfo constructor = type.GetConstructor(Type.EmptyTypes);

				if (constructor == null)
					continue;

				Type genericType = mongoCreatorInterface.GetGenericArguments()[0];

				try
				{
					MongoCollection collection = _database.GetCollection(genericType.Name);

					object indexCreator = constructor.Invoke(null);

					MethodInfo method = mongoCreatorInterface.GetMethod("CreateIndexes");

					ConstructorInfo mongoIndexerConstructor = typeof(DatabaseIndexer.MongoIndexer<>).MakeGenericType(genericType).GetConstructor(new[] { typeof(MongoCollection) });

					if (mongoIndexerConstructor == null)
						continue;

					object mongoIndexer = mongoIndexerConstructor.Invoke(new object[] { collection });

					method.Invoke(indexCreator, new[] { mongoIndexer });
				}
				catch (Exception exception)
				{
					// log any exceptions and continue
					Logger.Log(exception);
				}
			}
		}

		#region Private Methods

		private void InitializeDatabase()
		{
			ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[Constant.MainConnectionStringName];

			if (settings == null || string.IsNullOrEmpty(settings.ConnectionString))
				throw new ConfigurationErrorsException("No connection string found");

			string databaseName = Settings.Current.Database.Name;

			MongoClient client = new MongoClient(settings.ConnectionString);

			MongoServer server = client.GetServer();

			_database = server.GetDatabase(databaseName);
		}

		#endregion

		#region Private Classes

		private class MongoIndexer<T> : IMongoIndexer<T>
		{
			private readonly MongoCollection _collection;

			public MongoIndexer(MongoCollection collection)
			{
				_collection = collection;
			}

			#region Public Methods

			public void EnsureIndex(IndexDirection direction, params Expression<Func<T, object>>[] memberExpressions)
			{
				_collection.EnsureIndex(GetIndex(direction, memberExpressions));
			}

			public void EnsureIndex(params MultilevelExpression<T>[] memberExpressions)
			{
				_collection.EnsureIndex(GetIndex(memberExpressions));
			}

			public void EnsureIndex(params IndexItem<T>[] memberExpressions)
			{
				_collection.EnsureIndex(GetIndex(memberExpressions));
			}

			public void EnsureUnique(IndexDirection direction, params Expression<Func<T, object>>[] memberExpressions)
			{
				_collection.EnsureIndex(GetIndex(direction, memberExpressions), IndexOptions.SetUnique(true));
			}

			public void EnsureUnique(params MultilevelExpression<T>[] expressions)
			{
				_collection.EnsureIndex(GetIndex(expressions), IndexOptions.SetUnique(true));
			}

			public void EnsureUnique(params IndexItem<T>[] memberExpressions)
			{
				_collection.EnsureIndex(GetIndex(memberExpressions), IndexOptions.SetUnique(true));
			}

			public void RemoveIndex(IndexDirection direction, params Expression<Func<T, object>>[] memberExpressions)
			{
				IMongoIndexKeys index = GetIndex(direction, memberExpressions);
				if (_collection.IndexExists(index))
					_collection.DropIndex(index);
			}

			public void RemoveIndex(params MultilevelExpression<T>[] memberExpressions)
			{
				IMongoIndexKeys index = GetIndex(memberExpressions);
				if (_collection.IndexExists(index))
					_collection.DropIndex(index);
			}

			public void RemoveIndex(params IndexItem<T>[] memberExpressions)
			{
				IMongoIndexKeys index = GetIndex(memberExpressions);
				if (_collection.IndexExists(index))
					_collection.DropIndex(index);
			}

			#endregion

			#region Private Methods

			private IMongoIndexKeys GetIndex(IndexDirection direction, params Expression<Func<T, object>>[] memberExpressions)
			{
				switch (direction)
				{
					case IndexDirection.Ascending:
						return IndexKeys<T>.Ascending(memberExpressions);
					case IndexDirection.Descending:
						return IndexKeys<T>.Descending(memberExpressions);
					default:
						throw new ArgumentOutOfRangeException("direction");
				}
			}

			private IMongoIndexKeys GetIndex(params IndexItem<T>[] memberExpressions)
			{
				IndexKeysBuilder index = new IndexKeysBuilder();

				foreach (IndexItem<T> item in memberExpressions)
				{
					switch (item.Direction)
					{
						case IndexDirection.Ascending:
							index.Ascending(Reflector.GetPath(item.PropertyExpression).ToArray());
							break;
						case IndexDirection.Descending:
							index.Descending(Reflector.GetPath(item.PropertyExpression).ToArray());
							break;
						default:
							throw new ArgumentOutOfRangeException("memberExpressions");
					}
				}

				return index;
			}

			private IMongoIndexKeys GetIndex(params MultilevelExpression<T>[] expressions)
			{
				IndexKeysBuilder index = new IndexKeysBuilder();

				foreach (MultilevelExpression<T> item in expressions)
				{
					IEnumerable<string> strings = item.Expressions.SelectMany(Reflector.GetPath);

					switch (item.Direction)
					{
						case IndexDirection.Ascending:
							index.Ascending(strings.ToArray());
							break;
						case IndexDirection.Descending:
							index.Descending(strings.ToArray());
							break;
						default:
							throw new ArgumentOutOfRangeException("expressions");
					}
				}

				return index;
			}

			#endregion
		}

		#endregion
	}
}
