﻿using System;
using System.Linq.Expressions;

namespace KswApi.Data.Mongo.Framework
{
	internal class IndexItem<T>
	{
		public IndexDirection Direction { get; set; }
		public Expression<Func<T, object>> PropertyExpression { get; set; }
	}
}
