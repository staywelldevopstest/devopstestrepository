﻿using System.IO;
using System;
using KswApi.Repositories.Interfaces;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;
using KswApi.Interface.Objects;

namespace KswApi.Data.Mongo.BinaryStore
{
	public class GridFsStore : IBinaryStore
	{
		//When ready to deploy, should be reading this value in during initialization
		private readonly string _connectionString;
		private readonly string _databaseName;
		private readonly MongoClient _client;
		private readonly MongoServer _server;
		private readonly MongoDatabase _database;
		private readonly MongoGridFS _gridFs;

		#region Constructor(s)

		public GridFsStore(string connectionString, string databaseName, string type)
		{
			_connectionString = connectionString;
			_databaseName = databaseName;

			_client = new MongoClient(_connectionString);
			
			_server = _client.GetServer();
			
			_database = _server.GetDatabase(_databaseName);
			
			MongoGridFSSettings settings = new MongoGridFSSettings
			{
				Root = type
			};

			_gridFs = new MongoGridFS(_database, settings);
		}

		#endregion

		#region Public Methods 

		public void EnsureIndexes()
		{
			_gridFs.EnsureIndexes();
		}
		
		public bool Save(Guid id, Stream stream, string contentType, DateTime time)
		{
			MongoGridFSCreateOptions createOptions = new MongoGridFSCreateOptions
			{
				ContentType = contentType,
				UploadDate = time,
					
			};
			
			_gridFs.Upload(stream, id.ToString(), createOptions);

			return true;
		}

		public StreamResponse Retrieve(Guid id)
		{
			MongoGridFSFileInfo file = _gridFs.FindOne(id.ToString());

			if (file == null)
				return null;

			MongoGridFSStream fileStream = file.OpenRead();
			var response = new StreamResponse(fileStream);
			response.ContentType = file.ContentType;

			return response;
		}

		public void Delete(Guid id)
		{
			_gridFs.Delete(id.ToString());
		}

		public void Rename(Guid source, Guid destination)
		{
			_gridFs.MoveTo(source.ToString(), destination.ToString());
		}

		public override string ToString()
		{
			return "GridFS";
		}

		#endregion

	}
}
