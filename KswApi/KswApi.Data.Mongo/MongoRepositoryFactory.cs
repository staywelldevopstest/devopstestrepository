﻿using System;
using System.Configuration;
using KswApi.Common.Configuration;
using KswApi.Data.Mongo.BinaryStore;
using KswApi.Data.Mongo.Constants;
using KswApi.Data.Mongo.Repositories;
using KswApi.Repositories.Enums;
using KswApi.Repositories.Interfaces;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.IdGenerators;

namespace KswApi.Data.Mongo
{
    public class MongoRepositoryFactory : IRepositoryFactory, IBinaryStoreFactory
    {
        private readonly string _connectionString;
        private readonly string _databaseName;
        private readonly IDataSetProvider _dataSetProvider;

        public MongoRepositoryFactory()
        {
        }

        /// <summary>
        /// This constructor provided only to allow deployment applications to run
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="databaseName"></param>
        public MongoRepositoryFactory(string connectionString, string databaseName)
        {
            _connectionString = connectionString;
            _databaseName = databaseName;
        }

        /// <summary>
        /// This constructor provided to allow testing
        /// </summary>
        public MongoRepositoryFactory(IDataSetProvider dataSetProvider)
        {
            _dataSetProvider = dataSetProvider;
        }

        public IRepository GetRepository(RepositoryType type)
        {
            if (_dataSetProvider != null)
                return new MongoRepository(_dataSetProvider);

            return new MongoRepository(GetConnectionString(type), GetDatabaseName());
        }

        private string GetConnectionStringName(RepositoryType type)
        {
            switch (type)
            {
                case RepositoryType.Main:
                    return Constant.MainConnectionStringName;
                case RepositoryType.Test:
                    return Constant.TestConnectionStringName;
                case RepositoryType.Log:
                    return Constant.LogConnectionStringName;
                case RepositoryType.Client:
                    return Constant.ClientManagementConnectionStringName;
                default:
                    throw new ArgumentOutOfRangeException("type");
            }
        }

        private string GetConnectionString(RepositoryType type)
        {
            if (_connectionString != null)
                return _connectionString;

            string name = GetConnectionStringName(type);
            ConnectionStringSettings settings = ConfigurationManager.ConnectionStrings[name];
            
            if (settings == null)
                throw new ConfigurationErrorsException(string.Format("Configuration files do not contain a connection string for: {0}.", name));
            
            return settings.ConnectionString;
        }

        private string GetDatabaseName()
        {
            if (_databaseName != null)
                return _databaseName;
			
	        string environment = Settings.Current.Database.Name;
            
            if (string.IsNullOrEmpty(environment))
                throw new ConfigurationErrorsException(string.Format("No app setting is found for: {0}.", AppSetting.Environment));
            
            return environment;
        }

        static MongoRepositoryFactory()
        {
            BsonSerializer.RegisterIdGenerator(typeof(Guid), CombGuidGenerator.Instance);
        }

	    #region Implementation of IBinaryStoreFactory

	    public IBinaryStore GetStore(string name)
	    {
            return new GridFsStore(GetConnectionString(RepositoryType.Main), GetDatabaseName(), name);
	    }

	    #endregion
    }
}
