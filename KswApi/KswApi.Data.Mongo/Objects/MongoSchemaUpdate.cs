﻿using System;
using KswApi.Data.Mongo.Enums;

namespace KswApi.Data.Mongo.Objects
{
	class MongoSchemaUpdate
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string Description { get; set; }
		public DateTime ExecutionDate { get; set; }
		public UpdateExecutionState State { get; set; }
		public string Message { get; set; }
	}
}
