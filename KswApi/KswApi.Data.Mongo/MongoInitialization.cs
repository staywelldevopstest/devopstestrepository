﻿using System;
using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Options;
using MongoDB.Bson.Serialization.Serializers;

namespace KswApi.Data.Mongo
{
	public class MongoInitialization
	{
		public void InitializeClientSettings()
		{
			// date times should be serialized as strings to allow full precision
			DateTimeSerializationOptions dateTimeSerializationOptions = new DateTimeSerializationOptions(DateTimeKind.Utc, BsonType.String);
			DateTimeSerializer dateTimeSerializer = new DateTimeSerializer(dateTimeSerializationOptions);
			BsonSerializer.RegisterSerializer(typeof(DateTime), dateTimeSerializer);
			
			// Don't throw FileFormatException if a document has an attribute not on the class
			ConventionPack pack = new ConventionPack { new IgnoreExtraElementsConvention(true) };
			ConventionRegistry.Register("ignoreExtraElements", pack, type => true);
		}
	}
}
