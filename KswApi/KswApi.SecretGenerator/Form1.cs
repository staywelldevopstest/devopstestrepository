﻿using System;
using System.Windows.Forms;
using KswApi.Common;

namespace KswApi.SecretGenerator
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			textBox.Text = TokenGenerator.CreateSecret();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			textBox.Text = TokenGenerator.CreateSecret();
		}
	}
}
