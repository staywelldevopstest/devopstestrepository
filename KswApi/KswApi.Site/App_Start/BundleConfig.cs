﻿using System.Web.Optimization;

namespace KswApi.Site.App_Start
{
    public class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            #region Javascript Bundles

            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery/jquery-1.*",
                        "~/Scripts/jquery/jquery.inFieldLabel.js",
                        "~/Scripts/jquery/jquery.ba-hashchange.js",
                        "~/Scripts/jquery/jquery.topzindex.js",
                        "~/Scripts/jquery/jquery.pstrength-min.1.2.js",
                        "~/Scripts/Common/jquery.extensions.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery/jquery-ui-1.9.1.custom.min.js",
                        "~/Scripts/jquery/jquery.ui.selectmenu.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery/jquery.unobtrusive*",
                        "~/Scripts/jquery/jquery.validate*"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            // On updating plupload, rename plupload.full.min.js to plupload.full.js
            bundles.Add(new ScriptBundle("~/bundles/plupload").Include(
                "~/Scripts/plupload/js/plupload.full.js",
                "~/Scripts/plupload/js/jquery.plupload.queue/jquery.plupload.queue.js"
                ));

            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                        "~/Scripts/Common/InFieldLabel.js",
                        "~/Scripts/Common/FirstInputFocus.js",
                        "~/Scripts/Common/Callback.js",
                        "~/Scripts/Common/Service.js",
                        "~/Scripts/Common/Form.js",
                        "~/Scripts/Common/Page.js",
                        "~/Scripts/Common/Html.js",
                        "~/Scripts/Common/Helper.js",
                        "~/Scripts/Common/Extensions.js",
                        "~/Scripts/Common/Popup.js",
                        "~/Scripts/Common/Tab.js",
                        "~/Scripts/Common/Pager.js",
                        "~/Scripts/Common/QueriedPager.js",
                        "~/Scripts/Common/ResourceProvider.js",
                        "~/Scripts/Common/Initialization.js",
                        "~/Scripts/Common/Menu.js",
                        "~/Scripts/Common/MenuItem.js",
                        "~/Scripts/Common/Navigation.js",
                        "~/Scripts/Common/Checkbox.js",
                        "~/Scripts/Common/DropDown.js",
                        "~/Scripts/Common/Warning.js",
                        "~/Scripts/Common/Timeout.js",
                        "~/Scripts/Wait.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/clientdata").Include(
                        "~/Scripts/Administration/ClientData*"));

            bundles.Add(new ScriptBundle("~/bundles/clientdataaddedit").Include(
                        "~/Scripts/Administration/ClientDataAddEdit.js"));

            bundles.Add(new ScriptBundle("~/bundles/clientlicense").Include(
                        "~/Scripts/Administration/ClientLicense*"));

            bundles.Add(new ScriptBundle("~/bundles/user/password").Include(
                        "~/Scripts/User/Password.js"));

            bundles.Add(new ScriptBundle("~/bundles/administration").Include(
                        "~/Scripts/Administration/ClientEditor.js",
                        "~/Scripts/Administration/ClientList.js",
                        "~/Scripts/Administration/CopyrightEditor.js",
                        "~/Scripts/Administration/ContentModuleEditor.js",
                        "~/Scripts/Administration/ApplicationList.js",
                        "~/Scripts/Administration/LicenseFlyoutItem.js",
                        "~/Scripts/Administration/LicenseFlyout.js",
                        "~/Scripts/Administration/ClientList.js",
                        "~/Scripts/Administration/ModuleList.js",
                        "~/Scripts/Administration/SmsModuleEditor.js",

                        "~/Scripts/Administration/ContentBucketEditor.js",
                        "~/Scripts/Administration/ContentBucketDetailView.js",

                        "~/Scripts/Administration/Pages/Administration.js",
                        "~/Scripts/Administration/Pages/Dashboard.js",
                        "~/Scripts/Administration/Pages/Logs.js",
                        "~/Scripts/Administration/Pages/LicenseEditor.js",
                        "~/Scripts/Content/Pages/ContentList.js",

                        "~/Scripts/Content/Pages/ContentManagementList.js",
                        "~/Scripts/Content/Pages/CollectionView.js",
                        "~/Scripts/Content/Controls/ImageSearchItem.js",
                        "~/Scripts/Content/Controls/ArticleSearchItem.js",
                        "~/Scripts/Content/Controls/CollectionSearchItem.js",
                        "~/Scripts/Content/Controls/BucketSearchItem.js",
                        "~/Scripts/Content/Controls/ContentSearch.js",
                        "~/Scripts/Content/Pages/ContentView.js",
                        "~/Scripts/Content/Controls/MetaDataEditor.js",
                        "~/Scripts/Content/Controls/CollectionSelector.js",

                        "~/Scripts/Content/Pages/ImageUploader.js",
                        "~/Scripts/Content/Controls/ImageUploadProgress.js",
                        "~/Scripts/Content/Controls/ImageUploadEditor.js",
                        "~/Scripts/Content/Pages/ImageEditor.js",
                        "~/Scripts/Content/Factories/MetadataFactory.js",
                        "~/Scripts/Content/Controls/SingleOriginBucketSelector.js",
                        "~/Scripts/Content/Controls/BucketSelector.js",
                        "~/Scripts/Content/Controls/BucketWizard.js",
                        "~/Scripts/Content/Controls/CopyrightSelector.js",
                        "~/Scripts/Content/Controls/ImageSelector.js",
                        "~/Scripts/Content/Controls/ImagePopup.js",
                        "~/Scripts/Administration/Pages/Copyrights.js",
                        "~/Scripts/Administration/Pages/ContentBuckets.js",
                        "~/Scripts/Administration/Pages/Reports.js",
                        "~/Scripts/Administration/Pages/ClientUsers.js",

                        "~/Scripts/Administration/Navigation.js",

                        "~/Scripts/DateRangePicker/date.js",
                        "~/Scripts/DateRangePicker/daterangepicker.jquery.compressed.js"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/client").Include(
                        "~/Scripts/Client/Dashboard.js",
                        "~/Scripts/Client/AppSettings.js",
                        "~/Scripts/Client/Navigation.js",
                        "~/Scripts/Client/UserEditor.js"
                            ));

            bundles.Add(new ScriptBundle("~/bundles/login").Include(
                        "~/Scripts/Login.js"));

            bundles.Add(new ScriptBundle("~/bundles/DateRangePicker").Include(
                        "~/Scripts/DateRangePicker/date.js",
                        "~/Scripts/DateRangePicker/daterangepicker.jquery.compressed.js"));

            #endregion

            #region CSS Bundles

            bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));

            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));

            bundles.Add(new StyleBundle("~/Content/responsive").Include("~/Content/Responsive.css"));

            #endregion

        }
    }
}