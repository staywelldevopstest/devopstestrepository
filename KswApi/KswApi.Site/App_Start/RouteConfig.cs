﻿using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;

namespace KswApi.Site.App_Start
{
	public class RouteConfig
	{
		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				"Service",
				"Service/{verb}/{*path}",
				new { controller = "Service", action = "Index" },
				new[] { "KswApi.Site.Controllers" }
			);

			routes.MapHttpRoute(
				"DefaultApi",
				"api/{controller}/{id}",
				new { id = UrlParameter.Optional },
				new[] { "KswApi.Site.Controllers" }
			);

			routes.MapRoute(
				"Default",
				"{controller}/{action}/{id}",
				new { controller = "Home", action = "Index", id = UrlParameter.Optional },
				new[] { "KswApi.Site.Controllers" }
			);
		}
	}
}