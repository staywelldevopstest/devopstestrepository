﻿var Wait = {
	show: function () {
		$('body').append($('<div>')
			.css('position', 'fixed')
			
			.css('top', 0)
			.css('width', '100%')
			.css('text-align', 'center')
			.addClass('waitMessage')
			.append($('<span>')
				.css('padding', '3px')
				.css('font-height', '14px')
				.css('background-color', '#fff')
				.text('Please wait...')));
		
	},
	
	hide: function () {
		$('.waitMessage').remove();
	}
};