﻿MenuItem = function() {
    var _caption;
    var _callBack;
    var _subMenus;
    var _requiredRight;
    var _kswid;
	var _isUserMenu = false;
	
	function getIsUserMenu() {
		return _isUserMenu;
	}
	
	function setIsUserMenu(value) {
		_isUserMenu = value;
	}

    function getCaption() {
        return _caption;
    }

    function setCaption(value) {
        _caption = value;
    }
    
    function getCallBack() {
        return _callBack;
    }
    
    function executeCallBack() {
        _callBack();
    }
    
    function setCallBack(callBack) {
        _callBack = callBack;
    }
    
    function getSubMenu() {
        return _subMenus;
    }
    
    function setSubMenu(values) {
        _subMenus = values;
    }
    
    function addSubMenu(subMenu) {
        _subMenus.add(subMenu);
    }
    
    function getSubMenuByIndex(index) {
        return _subMenus[index];
    }
    
    function setRequiredRight(value) {
        _requiredRight = value;
    }

    function getRequiredRight() {
        return _requiredRight;
    }
    
    function setDataKswId(value) {
        _kswid = value;
    }
    
    function getDataKswId() {
        return _kswid;
    }
	
	function select() {
		// placeholder, reset in Menu class
	}
	
	return {
    	GetIsUserMenu: getIsUserMenu,
    	SetIsUserMenu: setIsUserMenu,
        GetCaption: getCaption,
        SetCaption: setCaption,
        GetCallBack: getCallBack,
        SetCallBack: setCallBack,
        ExecuteCallBack: executeCallBack,
        GetSubMenu: getSubMenu,
        SetSubMenu: setSubMenu,
        AddSubMenu: addSubMenu,
        GetSubMenuByIndex: getSubMenuByIndex,
        SetRequiredRight: setRequiredRight,
        GetRequiredRight: getRequiredRight,
        SetDataKswId: setDataKswId,
        GetDataKswId: getDataKswId,
    	select: select
    };
};
