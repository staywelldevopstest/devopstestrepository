﻿var Tab = function (name) {
	var self = {
		addTab: addTab,
		setContent: setContent,
		setTitleArea: setTitleArea,
		setOptionsArea: setOptionsArea,
		setActiveTab: setActiveTab
	};

	var _name = name,
	    _tabs = new Array(),
	    _tabCallBacks = new Array(),
	    _tabCount = 0,
	    _tabArea = null,
	    _tabTopArea = null,
	    _tabContent = null,
		_optionsArea = null;

    var activeTabIndex;

	construct();

	function construct() {
		var result = Html.div('tabControl', '');

		if (_name)
			result.attr('name', _name);

		_tabArea = Html.div('tabsArea', '');
		_tabTopArea = Html.div('tabTopArea blackGradient', '');
		_tabContent = Html.div('tabContent', '');

		result.append(_tabArea);
		result.append(_tabTopArea);
		result.append(_tabContent);
		result.append(Html.breakDiv());

		self = $.extend(result, self);
	}

	function addTab(tabName, selectedCallBack, select) {
		_tabCount++;

		var tab = Html.div('', tabName);

		var tabNumber = _tabCount - 1;

		tab.addClass('tab inactiveTab blackGradient');
		tab.attr('tabIndex', tabNumber);
		tab.attr('data-kswid', 'tab' + tabName.replace(' ', ''));
		tab.disableSelection();

		if (selectedCallBack) {
			tab.click(function () {
				tabIndexChanged(tab.attr('tabIndex'));
			});
		}
		else {
			tab.addClass('disabled');
		}

		_tabCallBacks[_tabCount - 1] = selectedCallBack;

		_tabs[_tabCount - 1] = tab;

		if (_optionsArea)
			_optionsArea.before(tab);
		else
			_tabArea.append(tab);
		
		if (select || (typeof select == 'undefined' && _tabs && _tabs.length == 1))
			setActiveTab(_tabs.length - 1);
	}

	function setTitleArea(topAreaContent) {
		_tabTopArea.empty();

		_tabTopArea.append(topAreaContent);
	}

	function setOptionsArea(content) {
		if (_optionsArea) {
			_optionsArea.empty().append(content);
			return;
		}
		
		_optionsArea = Html.div('left', content);
		_tabArea.append(_optionsArea);
	}

	function setContent(content) {
		_tabContent.empty();
		_tabContent.append(content);
	}

	function setActiveTab(tabIndex) {
		tabIndexChanged(tabIndex);
	}

	function tabIndexChanged(tabIndex) {
	    if (tabIndex == activeTabIndex)
	        return;
		
		_tabArea.find('.tab').each(function (index, element) {
			if (tabIndex == index) {
				$(element).addClass('activeTab');
				$(element).removeClass('inactiveTab blackGradient');
			} else {
				$(element).addClass('inactiveTab blackGradient');
				$(element).removeClass('activeTab');
			}
		});

		activeTabIndex = tabIndex;
	    
		var callback = _tabCallBacks[tabIndex];
		if (callback)
			callback(self);
	}

	return self;
}