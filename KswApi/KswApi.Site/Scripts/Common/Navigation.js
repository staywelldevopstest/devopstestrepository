﻿
var Navigation = (function () {
	var self = {
		load: load,
		set: set,
		get: get,
		goto: goto
	};

	var hashChangedValue = null, skipSet = false;
	var nav;

	function load() {
		if (!Global.User)
			return;
		switch (Global.User.Type) {
			case 'ClientUser':
				if (typeof Client != 'undefined' && typeof Client.Navigation != 'undefined')
					nav = Client.Navigation;
				break;
			case 'DomainUser':
			    if (typeof Administration != 'undefined' && typeof Administration.Navigation != 'undefined')
			        nav = Administration.Navigation;
			    break;
		}

		if (nav) {
		    var promise = nav.initialize();
		    if(promise)
		        promise.done(hashChange);
		} else {
		    hashChange();
		}
	}

	function hashChange() {
	    $(window).hashchange(processHashChange);
	    processHashChange();
	}
    
	function set() {
		if (skipSet) {
			skipSet = false;
			return;
		}

		var array = $.makeArray(arguments);

		var tag = encodeHashTag(array);

		hashChangedValue = tag;

		location.hash = tag;
	}
	
	function get() {
		var hashTag = getHashTag();
		return decodeHashTag(hashTag);
	}

	function getHashTag() {
		var hashTag = location.hash;

		if (hashTag.length > 0 && hashTag.charAt(0) == '#') {
			return hashTag.substring(1);
		}

		return hashTag;
	}

	function processHashChange() {
		var hashTag = getHashTag();

		if (hashTag == hashChangedValue)
			return;

		hashChangedValue = hashTag;

		var array = decodeHashTag(hashTag);

		var name = array[0];

		var args = array.slice(1);

		for (var i = 0; i < array.length; i++) {
			args[i] = args[i];
		}

		skipSet = true;

		if (nav)
			nav.show.call(nav, name, args);
	}
	
	function goto() {
		var array = $.makeArray(arguments);
		
		var name = array[0];

		var args = array.slice(1);

		var tag = encodeHashTag(array);

		hashChangedValue = tag;

		location.hash = tag;

		skipSet = true;
		
		if (nav)
			nav.show.call(nav, name, args);
	}

	function encodeHashTag(array) {
		for (var i = 0; i < array.length; i++) {
			var item = array[i];
			if (typeof item == 'string') {
				// global replace to encode values
				item = item.split('$').join('$$');
				item = item.split('/').join('$/');
				array[i] = item;
			}
		}

		return array.join('/');
	}

	// this is to get around a firefox issue where the hashtag is always auto-decoded
	function decodeHashTag(tag) {
		var escape = false;
		var array = [];
		var current = [];

		// string builder variation to parse the tag
		for (var i = 0; i < tag.length; i++) {
			var c = tag.charAt(i);
			if (escape) {
				current.push(c);
				escape = false;
			}
			else if (c == '$') {
				escape = true;
			}
			else if (c == '/') {
				array.push(current.join(''));
				current = [];
			}
			else {
				current.push(c);
			}
		}

		if (current.length > 0) {
			array.push(current.join(''));
		}

		return array;
	}

	return self;
})();