﻿
var Html = {
	image: function (classes, path) {
		path = Global.ApplicationPath + 'Content/themes/base/images/' + path;
		var ret = $('<img>').attr('src', path);
		if (classes)
			ret.addClass(classes);
		return ret;
	},

	checkBox: function (id, classes, dataKswapiAttr, text, isChecked, callback) {
		//<div id="includeWarnings" class="defaultCheckBox checked" data-kswid="inputSeverityWarn"></div>
		//<div class="defaultCheckBoxLabel">Warn</div>
		var checkBoxDivArea = Html.div('', '');

		var checkBox = Html.div('defaultCheckBox', '');

		if (id != null || id != '')
			checkBox.attr('id', id);

		if (dataKswapiAttr != null || id != '')
			checkBox.attr('data-kswid', dataKswapiAttr);

		if (isChecked)
			checkBox.addClass('checked');

		var checkBoxLabel = Html.div('defaultCheckBoxLabel', text);
		checkBox.click(function () {
			var checked;
			if ($(this).hasClass('checked')) {
				$(this).removeClass('checked');
				checked = false;
			} else {
				$(this).addClass('checked');
				checked = true;
			};

			if (callback)
				callback(checked);
		});

		checkBoxDivArea.append(checkBox);
		checkBoxDivArea.append(checkBoxLabel);

		return checkBoxDivArea;
	},

	anchorButton: function (classes, text, onClick) {
		var ret = $('<a>').attr('href', 'javascript:');

		if (classes)
			ret.addClass(classes);

		if (text) {
			if (typeof text == 'string')
				ret.text(text);
			else
				ret.append(text);
		}


		if (onClick)
			ret.click(onClick);

		return ret;
	},

	anchor: function (classes, contents) {
		var ret = $('<a>').attr('href', 'javascript:;');

		if (classes)
			ret.addClass(classes);

		for (var i = 1; i < arguments.length; i++) {
			if (arguments[i]) {
				ret.append(arguments[i]);
			}
		}

		return ret;
	},

	anchorUrl: function (classes, href, text, target) {
		var ret = $('<a>').attr('href', href);
		ret.attr('target', target);

		if (classes)
			ret.addClass(classes);

		ret.append(text);

		return ret;
	},

	emailAnchor: function (classes, email) {
		var ret = $('<a>').attr('href', 'mailto:' + email);

		if (classes)
			ret.addClass(classes);

		for (var i = 1; i < arguments.length; i++) {
			if (arguments[i]) {
				ret.append(arguments[i]);
			}
		}

		return ret;
	},

	div: function (classes, contents) {
		var div = $('<div>');
		if (classes)
			div.addClass(classes);
		for (var i = 1; i < arguments.length; i++) {
			if (arguments[i]) {
				div.append(arguments[i]);
			}
		}
		return div;
	},
	
	span: function (classes, contents) {
		var span = $('<span>');
		if (classes)
			span.addClass(classes);
		for (var i = 1; i < arguments.length; i++) {
			if (arguments[i]) {
				span.append(arguments[i]);
			}
		}
		return span;
	},

	twoColumnContent: function (columnClasses, columnOne, columnTwo) {

		var first = $('<div>')
			.append(columnOne);

		var second =
			$('<div>')
				.append(columnTwo);

		if (columnClasses) {
			first.addClass(columnClasses);
			second.addClass(columnClasses);
		}

		return Html.div(null, Html.div(null, first, second), Html.breakDiv());
	},

	textBox: function (classes, name, value) {
		var ret = $('<input>')
			.attr('type', 'text');
		if (classes)
			ret.addClass(classes);
		if (name)
			ret.attr('name', name);
		if (value)
			ret.val(value);
		return ret;
	},

	textArea: function (classes, name, value) {
		var ret = $('<textarea>');
		if (classes)
			ret.addClass(classes);
		if (name)
			ret.attr('name', name);
		if (value)
			ret.val(value);
		return ret;
	},

	button: function (classes, text, onClick) {
		var ret = $('<button>')
			.attr('type', 'button');
		if (classes)
			ret.addClass(classes);
		if (text)
			ret.text(text);
		if (onClick)
			ret.click(onClick);
		return ret;
	},

	label: function (classes, text) {
		var ret = $('<label>');
		if (classes)
			ret.addClass(classes);
		if (text)
			ret.text(text);
		return ret;
	},

	radioButton: function (classes, name, value, checked, labelClasses, labelText) {
		var ret = $('<input>')
			.attr('type', 'radio');
		if (classes)
			ret.addClass(classes);
		if (name)
			ret.attr('name', name);
		if (value)
			ret.attr('value', value);
		if (checked)
			ret.attr('checked', 'checked');
		if (labelText) {
			ret = ret.add(Html.label(labelClasses, labelText)
				.click(function () {
					ret.attr('checked', 'checked');
				}));
		}
		return ret;
	},

	breakDiv: function () {
		return $('<div>')
			.addClass('break');
	},
	
	spacer: function (height) {
		var ret = $('<div>')
			.addClass('spacer');
		if (height)
			ret.height(height);
		return ret;
	},

	loading: function () {
		return Html.div('Loading...');
	},

	editAndDeleteButtons: function (onEdit, onDelete) {
		var deleteButton = Html.anchorButton('iconDelete', ' ', onDelete);
		deleteButton.kswid('buttonDelete');

		var editButton = Html.anchorButton('iconEdit', ' ', onEdit);
		editButton.kswid('buttonEdit');

		return Html.div(
			'itemModifyButtonsArea',
			deleteButton,
			editButton
		).hide();
	},

	closeButton: function (onClick) {
		return Html.div(
			'itemModifyButtonsArea',
			Html.anchorButton('iconClose', ' ', onClick)
		);
	},

	deleteButton: function (onClick) {
		return Html.div(
			'itemModifyButtonsArea',
			Html.anchorButton('iconDelete', ' ', onClick)
		);
	},

	hidden: function (name, value) {
		var ret = $('<input>').attr('type', 'hidden');
		if (name)
			ret.attr('name', name);
		if (value)
			ret.val(value);
		return ret;
	},

	dropDown: function (classes, name, options, selected) {
		return new DropDown(classes, name, options, selected);
	},

	searchList: function () {
		return new Html.internal.SearchList();
	},

	tab: function (name) {
		var tab = new Html.internal.Tab();
		return tab.create(name);
	},
	labeledField: function (classes, labelClasses, labelText, field) {
		var label = new Html.internal.FieldLabel();

		return label.create(classes, labelClasses, labelText, field);
	},

	br: function () {
		return $('<br/>');
	},

	saveAndCancelButtons: function (saveText, cancelText, onSave, onCancel) {
		return Html.div('',
			Html.button('defaultButton defaultButtonBox addButton right',
				(saveText ? saveText : 'Save'),
				function () {
					if (onSave)
						onSave();
				}
			).kswid('buttonSave'),
			Html.button('defaultWhiteButton defaultButtonBox defaultAnchorbutton right',
				cancelText ? cancelText : 'Cancel',
				function () {
					if (onCancel)
						onCancel();
				}
			).kswid('buttonCancel')
		);
	},

	labelBinding: function (classes, text, boundElement) {
		var boundId = boundElement.attr('id');
		var label = $('<label>');
		label.attr('for', boundId);
		label.addClass(classes);
		label.text(text);

		label.click(function () {
			boundElement.click();
		});

		return label;
	}
};

Html.internal = {};

Html.internal.FieldLabel = function () { };

Html.internal.FieldLabel.prototype = {
	_label: null,
	_field: null,
	create: function (classes, labelClasses, labelText, field) {
		var self = this;

		var ret = Html.div(classes);

		ret.css({
			'position': 'relative'
		});

		var label = Html.div(labelClasses, labelText);

		label.css({
			'background': 'none',
			'position': 'absolute',
			'top': 0,
			'left': 0,
			'pointer-events': 'none'
		});

		label.click(function () { field.focus(); });

		if (field)
			ret.append(field);

		ret.append(label);

		this._label = label;
		this._field = field;

		this.update();

		field.on('paste', function () { self.hide(); })
			.change(function () { self.update(); })
			.on('keyup', function () { self.update(); })
			.on('val', function () { self.update(); })
			.blur(function () { self.update(); });

		return ret;
	},
	hide: function () {
		this._label.hide();
	},
	show: function () {
		this._label.show();
	},
	update: function () {
		if (!this._field) {
			this.hide();
			return;
		}

		if (this._field.val() != '') {
			this.hide();
		} else {
			this.show();
		}
	}
};

Html.internal.SearchList = function () {
	var controlHtml;
	var dataRow;
	var dataColumn;
	var listColumns;
	var listRows;
	var columnsToShow;
	var selectedValues;

	function create(textFields, data, target, callback) {
		columnsToShow = textFields;

		ResourceProvider.getSearchListControl(function (html) {
			controlHtml = html.find('#searchListControl');

			listColumns = controlHtml.find('#listColumns');
			listRows = controlHtml.find('#listData');
			selectedValues = controlHtml.find('#selectedValueArea');

			dataColumn = listColumns.find('.listColumn').remove();
			dataRow = listRows.find('.listRow').remove();

			setupEvents();

			populateColumns();

			target.empty().append(controlHtml);

			callback();
		});
	}

	function setupEvents() {
		controlHtml.find('.searchButton').click(function () {

			Callback.submit('Administration/GetClients', { 'offset': 0, 'count': 100, search: controlHtml.find('.searchTextBox').val() }, function (list) {
				populateControl(list);
			});

		});
	}

	function populateColumns() {
		listColumns.empty();

		for (var i = 0; i < columnsToShow.length; i++) {
			listColumns.append(dataColumn.clone().text(columnsToShow[i]));
		}
	}

	function populateControl(data) {
		listRows.empty();

		if (!data || data.Items.length == 0) {
			listRows.append('No results.  Please try a different search term');

			return;
		}

		for (var i = 0; i < data.Items.length; i++) {
			var listRow = dataRow.clone();
			var dataRecord = data.Items[i];

			listRow.find('.listDataValue').text(dataRecord.Id);
			listRow.find('.listCell').text(dataRecord.ClientName);

			listRow.click(function () {
				listRows.find('.listRow').removeClass('selected');

				$(this).addClass('selected');

				selectedValues.find('#selectedValue').text($(this).find('.listDataValue').text());
				selectedValues.find('#selectedText').text($(this).find('.listCell').text());
			});

			listRows.append(listRow);
		}
	}

	function getSelectedValue() {
		return selectedValues.find('#selectedValue').text();
	}

	function setSelectedValue(id, value) {
		selectedValues.find('#selectedValue').text(id);
		selectedValues.find('#selectedText').text(value);
	}

	return {
		create: create,
		getSelectedValue: getSelectedValue,
		setSelectedValue: setSelectedValue
	};
};