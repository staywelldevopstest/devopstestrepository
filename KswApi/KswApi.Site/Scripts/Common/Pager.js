﻿var Pager = function (pageSize, callback) {

	var self = {
		create: create,
		update: update,
		reset: reset,
		refresh: refresh,
		gotoPage: gotoPage
	};

	var count = 0,
	    current = 0,
		pager = null;

	init();

	function init() {
		var div = Html.div();

		pager = Html.div('pagerLinkArea');

		div.append(createGotoPage, pager);
			
		self = $.extend(div, self);
	}

	function create() {
		callback(0, pageSize);

		return self;
	}
	
	function update(total) {
		count = total;

		pager.empty();

		if (count <= 0) {
			return true;
		}
		
		var pageCount = count / pageSize;
		
		if (current >= pageCount) {
			gotoPage(current);
			return false;
		}

		var previous = (current > 0)
			? Html.anchorButton('iconPreviousPage left', ' ')
				.click(function () {
					current--;
					callback(pageSize * current, pageSize);
				})
			: Html.div('iconPreviousPageDisabled left', ' ');

	    previous.kswid('pagePrevious');

		pager.append(Html.div('pagerPrevButton', previous));

		var pageLinks = Html.div('pagerPageLinks', ' ');

		for (var i = 0; i < pageCount; i++) {
			pageLinks.append(createPageLink(i));
		}

		pager.append(pageLinks);

		var next = (current + 1 < pageCount)
					? Html.anchorButton('iconNextPage', ' ')
						.click(function () {
							current++;
							callback(pageSize * current, pageSize);
						})
					: Html.div('iconNextPageDisabled', ' ');

	    next.kswid('pageNext');

	    pager.append(Html.div('pagerNextButton', next));

		return true;
	}
	
	function reset() {
		callback(0, pageSize);
	}
	
	function refresh() {
		callback(pageSize * current, pageSize);
	}
	
	function gotoPage(page) {
		current = page;
		if (current * pageSize >= count) {
			current = Math.floor(count / pageSize) - 1;
		}
		
		if (current < 0)
			current = 0;
		
	    callback(pageSize * current, pageSize);
	}
	
	function createGotoPage() {
		var gotoPageArea = Html.div('pagerGotoPageArea', '');
		var gotoPageLabel = Html.div('pagerGotoPageLabel', 'Go to page:');
		var gotoPageInput = Html.textBox('pagerGotoPageInput textBox', '').kswid('goPageTextbox');

		gotoPageInput.enter(function() {
			gotoPage(gotoPageInput.val() - 1);
		});

		var gotoPageButton = Html.button('pagerGotoPageButton', '', function () {
			gotoPage(gotoPageInput.val() - 1);

			return false;
		}).kswid('goPageButton');

		gotoPageButton.attr('data-kswid', 'buttonGoPage');

		gotoPageArea.append(gotoPageButton);
		gotoPageArea.append(gotoPageInput);
		gotoPageArea.append(gotoPageLabel);

		return gotoPageArea;
	}
	
	function createPageLink(page) {
		var type = (page == current ? 'pagerSelectedPage' : 'pagerPageLinks');

		var pageLink = Html.div('pageLink', Html.anchorButton(type, (page + 1).toString(), function () {
			current = page;
			callback(pageSize * page, pageSize);
		}));

		pageLink.kswid('pageLink');

	    return pageLink;
	}

	return self;
};
