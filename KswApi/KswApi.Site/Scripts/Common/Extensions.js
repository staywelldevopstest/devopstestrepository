﻿(function () {
	if (typeof Array.prototype.indexOf != 'function') {
		Array.prototype.indexOf = function (item) {
			for (var i = 0; i < this.length; i++) {
				if (this[i] == item)
					return i;
			}
			return -1;
		};
	}

	if (typeof Array.prototype.contains != 'function') {
		Array.prototype.contains = function (item) {
			for (var i = 0; i < this.length; i++) {
				if (this[i] == item)
					return true;
			}
			return false;
		};
	}

	if (typeof String.prototype.endsWith !== 'function') {
		String.prototype.endsWith = function (suffix) {
			return this.indexOf(suffix, this.length - suffix.length) !== -1;
		};
	}

	if (typeof Array.prototype.remove != 'function') {
		Array.prototype.remove = function (item) {
			if (typeof item == 'function') {
				for (var i = 0; i < this.length; i++) {
					if (item(this[i])) {
						this.splice(i, 1);
						return;
					}
				}
			}
			else {
				for (var i = 0; i < this.length; i++) {
					if (this[i] == item) {
						this.splice(i, 1);
						return;
					}
				}
			}
		};
	}

	if (typeof Array.prototype.replace != 'function') {
		Array.prototype.replace = function (item, newItem) {
			for (var i = 0; i < this.length; i++) {
				if (this[i] == item) {
					this[i] = newItem;
					return;
				}
			}
		};
	}

	if (typeof Array.prototype.removeFirst != 'function') {
		Array.prototype.removeFirst = function (closure) {
			for (var i = 0; i < this.length; i++) {
				var current = this[i];
				if (closure(current)) {
					this.splice(i, 1);
					return current;
				}
			}
			return null;
		};
	}

	if (typeof Array.prototype.removeAll != 'function') {
		Array.prototype.removeAll = function (closure) {
			for (var i = 0; i < this.length;) {
				var current = this[i];
				if (closure(current)) {
					this.splice(i, 1);
					continue;
				}
				i++;
			}
			return null;
		};
	}

	if (typeof Array.prototype.where != 'function') {
		Array.prototype.where = function (delegate) {
			var result = [];
			for (var i = 0; i < this.length; i++) {
				var cursor = this[i];
				if (delegate(cursor) === true) {
					result.push(this[i]);
				}
			}
			return result;
		};
	}

	if (typeof Array.prototype.firstOrNull != 'function') {
		Array.prototype.firstOrNull = function () {
			if (arguments.length == 0 || typeof arguments[0] != 'function')
				return (this.length > 0) ? this[0] : null;

			var delegate = arguments[0];
			for (var i = 0; i < this.length; i++) {
				var cursor = this[i];
				if (delegate(cursor) === true) {
					return cursor;
				}
			}
			
			return null;
		};
	}

	if (typeof Array.prototype.select != 'function') {
		Array.prototype.select = function (projection) {
			var result = [];
			for (var i = 0; i < this.length; i++) {
				result.push(projection(this[i]));
			}
			return result;
		};
	}

	if (typeof String.prototype.contains != 'function') {
		String.prototype.contains = function (searchString, startIndex) {
			return -1 !== String.prototype.indexOf.call(this, searchString, startIndex);
		};
	}

	if (typeof Array.prototype.forEach != 'function') {
		Array.prototype.forEach = function (fn, scope) {
			var i, len;
			for (i = 0, len = this.length; i < len; i++) {
				if (i in this) {
					fn.call(scope, this[i], i, this);
				}
			}
		};
	}
    
	if (typeof String.prototype.trim != 'function') {
	    String.prototype.trim = function () {
	        return this.replace(/^\s+|\s+$/g, '');
	    };
	}
    
})();