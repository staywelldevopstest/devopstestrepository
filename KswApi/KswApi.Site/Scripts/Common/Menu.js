﻿var Menu = function () {
	var self = {
		init: init,
		initMenu: initMenu,
		addMenuItem: addMenuItem,
		setMenuItems: setMenuItems
	};

	var deferred = $.Deferred();
    
	var settingsIcon, settingsFlyout, body;
	var menuItems;
	var menu;
	var menuLinks;
	var menuSeperator;
	var menuLink;
	var subMenu;
	var subMenuLink;
	var subMenuSeperator;
	var userMenuLink;
	var _showCallback;
    
	function init() {
		if (!Global.User)
			return;

		initSettingsFlyout();
	}

	function initMenu(menuItemsArray, showCallback) {
		if (!Global.User)
			return null;

		_showCallback = showCallback;
	    
		if (menuItemsArray)
			menuItems = menuItemsArray;

		ResourceProvider.getMenuHtml(function (data) {
			menu = data.find('.mainMenu');

			menuLinks = menu.find('#menuLinks');
			menuSeperator = menu.find('#menuSeperator').remove();
			menuLink = menu.find('#menuLink').remove();

			subMenu = menu.find('#subHeaderContainer').remove();
			subMenuLink = subMenu.find('#subMenuLink').remove();
			subMenuSeperator = subMenu.find('#subMenuSeperator').remove();

			userMenuLink = menu.find('#userMenuLink').remove();

			initSettingsFlyout();

			if (menuItems) {
			    setupMenu(menuItems);
			}
		    
		    // alert that initialization is finished
		    deferred.resolve();
		});

	    return deferred.promise();
	}

	function addMenuItem(menuItem) {
		menuItems.add(menuItem);
	}

	function setMenuItems(menuItemsArray) {
		menuItems = menuItemsArray;
	}

	function setupMenu() {
		menuLinks.empty();

		menu.find('.menuHeading').text(Global.User.FullName);

		if (!menuItems)
			return;

		var userMenu = settingsFlyout.find('.menuUserSpecific');

		var mainMenuItems = menuItems.where(function (menuItem) { return !menuItem.GetIsUserMenu(); });
		var userMenuItems = menuItems.where(function (menuItem) { return menuItem.GetIsUserMenu(); });

		var item;
		for (var i = 0; i < mainMenuItems.length; i++) {
			item = mainMenuItems[i];
			if (Global.User && (Global.User.Rights.contains(item.GetRequiredRight()) || item.GetRequiredRight() == 'show_always')) {
				var newMenuLink = menuLink.clone();

				//Add caption and the data-kswid
				newMenuLink.text(item.GetCaption());
				newMenuLink.attr('data-kswid', item.GetDataKswId());

				//If we have a sub menu then we need to show it instead of the click event
				if (!item.GetSubMenu())
					setupMenuClickEvent(newMenuLink, item);
				else
					setupSubMenu(newMenuLink, item);

				menuLinks.append(newMenuLink);

				//Add a seperator until we are at the last menuItem
				if (i < menuItems.length - 1)
					menuLinks.append(menuSeperator.clone());
			}
		}

		for (i = 0; i < userMenuItems.length; i++) {
			item = userMenuItems[i];
			var newUserMenuLink = userMenuLink.clone();

			newUserMenuLink.text(item.GetCaption());
			newUserMenuLink.attr('data-kswid', item.GetDataKswId());
			setupMenuClickEvent(newUserMenuLink, item, false, true);

			userMenu.append(newUserMenuLink);
		}

		menu.on("click", ".showsHome", "default", _showCallback);
	    
		$('.mainHeader').empty().append(menu);
	}

	function setupMenuClickEvent(menuItem, item, isSubMenu, isUserMenu) {
		menuItem.click(function () {
			selectMenu(item, isSubMenu, isUserMenu, true);
		});

		item.select = function () {
			selectMenu(item, isSubMenu, isUserMenu, false);
		};
	}

	function selectMenu(item, isSubMenu, isUserMenu, runCallback) {
		if (!isSubMenu) {
			$('#mainHeaderContainer').css('height', '50px');
			$('.mainContent').css('top', (Global.User.Type == 'ClientUser') ? '155px' : '120px');
			$('.mainHeader').find('#subHeaderContainer').remove();
		}

		if (runCallback)
			item.ExecuteCallBack();

		if (typeof isUserMenu != "undefined") {
			if (isUserMenu) {
				hideSettingsFlyout();
			}
		}
	}

	function setupSubMenu(menuItem, item) {
	    menuItem.click(function () {
			showSubMenu(menuItem, item, true);
		});

	    item.select = function () {
			showSubMenu(menuItem, item, false);
		};
	}

	function showSubMenu(menuItem, item, runCallback) {
		$('mainHeader').find('#subHeaderContainer').remove();

		var subMenuArea = subMenu.clone();

		var subMenus = item.GetSubMenu();

		for (var i = 0; i < subMenus.length; i++) {
			var subMenuItem = subMenus[i];
			var newSubMenuLink = subMenuLink.clone();

			newSubMenuLink.text(subMenuItem.GetCaption());
			newSubMenuLink.attr('data-kswid', subMenuItem.GetDataKswId());

			setupMenuClickEvent(newSubMenuLink, subMenuItem, true);

			subMenuArea.append(newSubMenuLink);

			//Add a seperator until we are at the last menuItem
			if (i < subMenus.length - 1)
				subMenuArea.append(menuSeperator.clone());
		}

		$('#mainHeaderContainer').css('height', '100px');
		$('.mainContent').css('top', '170px');

		$('.mainHeader').find('#subHeaderContainer').remove();

		$('.mainHeader').append(subMenuArea);

		if (runCallback)
			subMenus[0].ExecuteCallBack();
	}

	function initSettingsFlyout() {

		settingsIcon = menu.find('#mainMenuSettings');
		settingsFlyout = menu.find('#menuFlyout');
		body = $('body');

		settingsFlyout.find('#signOut').click(function () {
			logout();
		});

		settingsFlyout.click(function (event) {
			event.stopPropagation();
		});

		settingsIcon.click(function (event) {
			event.stopPropagation();
			settingsFlyout.toggle();
			if (settingsFlyout.is(':visible')) {
				body.click(hideSettingsFlyout);
			}
		});
	}

	function logout() {
		location.href = Global.ApplicationPath + 'Logout';
	}

	function hideSettingsFlyout() {
		body.unbind('click', hideSettingsFlyout);
		settingsFlyout.hide();
	}

	return self;
};