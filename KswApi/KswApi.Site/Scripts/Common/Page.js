﻿var Page = {
	eventCounter: 0,
	clear: function (callback) {
		var container = $('#mainContent');
		var children = container.children();
		var total = children.length;

		if (total == 0) {
			if (callback)
				callback();
			return;
		}

		children.customHide(function () {
			children.detach();
			if (callback)
				callback();
		});
	},
	switchPage: function (content, callback) {
		Page.clear(function () {
			Page.show(content, callback);
		});
	},
	show: function (content, callback) {
		if (content) {
			var container = $('#mainContent');
			content.hide();
		    container.empty();
			container.append(content);
			
			content.customShow(callback);
		}
	},
	showSideBar: function (content, callback) {
		if (content) {
			var container = $('#mainContent');
			var sidebar = container.find('.listOptionsArea');
			
			container.append(content);

			content.customShow(callback);
		}
	},
	setTitle: function (title) {
	    var container = $('#title');
	    container.kswid('title');
		container.empty();
		container.append(title);
	},
	getTitle: function () {
	    var container = $('#title');
        if(container)
            return container.html();
	    return null;
	}
	,
	setContentTitle: function (title) {
		var container = $('#contentTitleBar');
		container.empty();

		if (typeof title == 'string') {
			title = Html.div('contentTitle', title);
		}

		container.append(title);
	}
};