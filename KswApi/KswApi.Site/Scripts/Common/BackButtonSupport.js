﻿///* File Created: August 7, 2012 */

//var backButtonSupport = { };

////This is used to supress the event when we are clicking on links to prevent the action from firing twice.
//backButtonSupport.supressHashChangeEvent = false;

//$(function () {
//	$(window).load(function() {
//		if (location.hash == "") {
//			$('#divContent').html('');
//			return;
//		}
		
//		var actionName = location.hash.substring(1, location.hash.length);

//		//MVC url /controller/action
//		var url = "/" + location.pathname.replace('/', '') + "/" + actionName;

//		//ajax get and replace the contents in 'divContent' with the ActionResult data
//		$.ajax({
//			url: url,
//			success: function (data) {
//				$('#divContent').html(data);
//			}
//		});
//	});

//	//the hashchange event is what we are using to know when the back/forward button is pressed
//	$(window).hashchange(function () {
//		//if we need to supress the event just return
//		if (backButtonSupport.supressHashChangeEvent) {
//			backButtonSupport.supressHashChangeEvent = false;
//			return;
//		}

//		//Gets the action name from the url and remove the hashtag
//		var actionName = location.hash.substring(1, location.hash.length);

//		//This is here so that we don't get the index action put into the divContent when the hash tag is empty
//		if (location.hash == "") {
//			$('#divContent').html('');
//			return;
//		}

//		//MVC url /controller/action
//		var url = "/" + location.pathname.replace('/', '') + "/" + actionName;

//		//ajax get and replace the contents in 'divContent' with the ActionResult data
//		$.ajax({
//			url: url,
//			success: function (data) {
//				$('#divContent').html(data);
//			}
//		});
//	});
//});

////adds the hashtag to the url
//backButtonSupport.AddHashTag = function(hashtag) {
//	backButtonSupport.supressHashChangeEvent = true;
//	window.location.hash = hashtag;
//};
