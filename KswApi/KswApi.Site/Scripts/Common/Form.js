﻿(function ($) {
	$.fn.form = function () {
		var self = this;

		construct();

		function construct() {
			replaceControls();
			addInlineLabels();
		}

		function replaceControls() {
			self.find('input[type="checkbox"]').each(function () {
				replaceCheckbox($(this));
			});
			self.find('input[type="radio"]').each(function () {
				replaceRadio($(this));
			});
		}

		function addInlineLabels() {
			self.find('input[data-label],textarea[data-label]').each(function () {
				addInlineLabel($(this));
			});
		}

		function addInlineLabel(item) {
			var text = item.attr('data-label');
			if (!text)
				return;
			var classes = item.data('label-class') || '';
			
			classes += ' textboxLabel';
			
			item.labelField(classes, text);
		}

		function replaceCheckbox(checkbox) {
			if (checkbox.data('replacement')) {
				updateCheckbox();
				return;
			}

			var label = $('<label>');
			var classes = checkbox.attr('class');
			label.attr('class', classes);
			label.addClass('checkbox');

			checkbox.data('replacement', label);

			updateCheckbox();

			label.click(toggle);

			checkbox.before(label);
			checkbox.hide();
			
			if (checkbox.is(':disabled')) {
				label.addClass('disabled');
			}

			var id = checkbox.attr('id');
			if (!id)
				return;

			self.find('label[for="' + id + '"]').addClass('clickable');

			checkbox.change(updateCheckbox);
			checkbox.on('remove', function() {
				label.remove();
			});

			function toggle() {
				var selected = oldProp.call(checkbox, 'checked');

				if (selected)
					oldProp.call(checkbox, 'checked', false);
				else
					oldProp.call(checkbox, 'checked', true);

				updateCheckbox();
			}

			function updateCheckbox(evt) {
				if (evt) {
					evt.preventDefault();
					evt.stopPropagation();
				}
				
				if (checkbox.is(':checked')) {
					label.addClass('selected');
				} else {
					label.removeClass('selected');
				}

				if (!evt)
					checkbox.change();
			}
		}
		
		function replaceRadio(radio) {
			if (radio.data('replacement')) {
				updateRadio();
				return;
			}

			var label = $('<label>');
			var classes = radio.attr('class');
			label.attr('class', classes);
			label.addClass('radio');

			radio.data('replacement', label);

			updateRadio();

			label.click(toggle);

			radio.before(label);
			radio.hide();

			var id = radio.attr('id');
			if (!id)
				return;

			self.find('label[for="' + id + '"]').addClass('clickable').click(toggle);

			radio.change(updateRadio);
			radio.on('remove', function() {
				label.remove();
			});

			function toggle() {
				oldProp.call(radio, 'checked', true);

				updateRadio();
			}

			function updateRadio(evt) {
				if (radio.is(':checked')) {
					label.addClass('selected');
					
					var name = radio.attr("name");

					if (!name)
						return;

					self.find('input[name="' + name + '"]').not(radio).trigger('change');
				} else {
					label.removeClass('selected');
				}
				
				if (!evt)
					radio.change();
			}
		}

		return self;

	};

	// override prop to allow form checkboxes/radios to update like the real thing
	var oldProp = $.fn.prop;
	$.fn.prop = function () {
		var self = this;
		oldProp.apply(self, arguments);

		if (arguments.length < 2)
			return;

		if (this.data('replacement')) {
			switch (arguments[0]) {
				case 'checked':
					self.change();
			}
		}
	};
})(jQuery);
