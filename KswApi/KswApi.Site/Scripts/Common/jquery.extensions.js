﻿(function ($) {
	$.fn.enter = function (callback) {
		this.keyup(function (e) {
			if (e.keyCode == 13) {
				callback();
			}
		});
		return this;
	};

	$.fn.escape = function (callback) {
		this.keyup(function (e) {
			if (e.keyCode == 27) {
				callback();
			}
		});
		return this;
	};

	$.fn.customHide = function (callback) {
		if (this.length < 1) {
			if (callback)
				callback();
			return;
		}

		if (this.length == 1) {
			this.hide('fade', {}, 250, callback);
			return;
		}

		var total = this.length;

		var current = 0;

		this.hide('fade', {}, 250, function () {
			current++;
			if (current >= total) {
				if (callback)
					callback();
			}
		});
	};

	$.fn.hideAndRemove = function (callback) {
		var self = this;
		this.customHide(function () {
			self.remove();
			if (callback)
				callback();
		});
	};

	$.fn.hideAndDetach = function (callback) {
		var self = this;
		this.customHide(function () {
			self.detach();
			if (callback)
				callback();
		});
	};

	$.fn.customShow = function (callback) {
		if (this.length < 1) {
			if (callback)
				callback();
			return;
		}

		if (this.length == 1) {
			$.fn.show.call(this, 'fade', {}, 250, callback);
			return;
		}

		var total = this.length;

		var current = 0;

		this.show('fade', {}, 250, function () {
			current++;
			if (current >= total) {
				if (callback)
					callback();
			}
		});
	};

	$.fn.swapWith = function (content, callback, detach) {
		content.hide();

		this.after(content);

		if (detach) {
			this.hideAndDetach(function () {
				content.customShow(callback);
			});
		}
		else {
			this.hideAndRemove(function () {
				content.customShow(callback);
			});
		}
	};

	$.fn.swapContent = function (content, callback, detach) {
		content.hide();

		var children = this.children();

		this.append(content);

		if (detach) {
			children.hideAndDetach(function () {
				content.customShow(callback);
			});
		}
		else {
			children.hideAndRemove(function () {
				content.customShow(callback);
			});
		}
	};

	$.fn.showOnHover = function (target) {
		var self = this;
		target.hover(function () {
			self.show();
		},
		function () {
			self.hide();
		});
		return this;
	};

	$.fn.kswid = function (id) {
		this.attr('data-kswid', id);

		return this;
	};

	$.fn.id = function (id) {
		this.attr('id', id);

		return this;
	};

	$.fn.checkBox = function () {
		this.each(function () {
			setup($(this));
		});

		function setup(checkbox) {
			checkbox.click(function () {
				if (checkbox.hasClass('checked'))
					checkbox.removeClass('checked');
				else
					checkbox.addClass('checked');
			});

			var id = checkbox.attr('id');
			if (!id)
				return;

			var parent = checkbox.parent();

			if (!parent)
				return;

			parent.find('label[for="' + id + '"]')
				.addClass('clickable')
				.click(function () {
					checkbox.trigger('click');
				});
		}

		return this;
	};

	var oldVal = $.fn.val;
	$.fn.val = function (value) {
		var result = oldVal.apply(this, arguments);

		if (value) {
			this.trigger('val');
		}

		return result;
	};

	$.fn.labelField = function (classes, text) {
		var base = this;
		var container = $('<label>');
		var label = $('<label>');

		label.addClass(classes);
		label.text(text);

		container.css({
			width: 0,
			height: 0,
			position: 'relative',
			left: 0,
			top: 0,
			display: 'block',
			'float': 'left'
		});

		container.addClass('fieldLabelContainer');

		label.css({
			'background': 'none',
			'position': 'absolute',
			'top': 0,
			'left': 0,
			'pointer-events': 'none',
			'cursor': 'text'
		});

		label.click(function (evt) {
			evt.preventDefault();
			base.focus();
		});

		container.append(label);
		this.before(container);

		this.on('paste', function () { hide(); })
			.change(function () { update(); })
			.on('keyup', function () { update(); })
			.on('val', function () { update(); })
			.blur(function () { update(); });

		update();

		function hide() {
			container.hide();
		}

		function show() {
			container.show();
		}

		function update() {
			if (base.val() != '') {
				hide();
			} else {
				show();
			}
		}

		return this;
	};

	Array.prototype.remove = function (value) {
		for (var i = 0; i < this.length; i++) {
			if (this[i] == value) {
				this.splice(i, 1);
				return this;
			}
		}
		return this;
	};

	$.fn.wait = function (modifier) {
		var self = this;
		var container = $('<label>');
		var label = $('<label>');
		container.css({
			position: 'relative',
			width: 0,
			height: 0,
			'float': 'left'
		});

		container.addClass('waitWrapper');

		label.addClass('wait');
		if (modifier)
			label.addClass(modifier);

		label.width(this.width());
		label.height(this.height());

		var css = ['padding-left', 'padding-right', 'padding-top', 'padding-bottom', 'margin-left', 'margin-right', 'margin-top', 'margin-bottom'];

		$.each(css, function (index, name) {
			var value = self.css(name);
			if (value)
				label.css(name, value);
		});

		var offset = this.offset();
		var parent = this.parent();
		var parentOffset = parent.offset();

		var leftPadding = parseInt(parent.css('padding-left'));
		var topPadding = parseInt(parent.css('padding-top'));

		label.css({
			display: 'block',
			position: 'absolute',
			left: offset.left - parentOffset.left - leftPadding,
			top: offset.top - parentOffset.top - topPadding
		});

		var array = this.data('wait');
		if (!array)
			array = [];
		array.push(container);
		this.data('wait', array);

		this.css('visibility', 'hidden');
		this.parent().prepend(container);
		container.append(label);

	};

	$.fn.endWait = function () {
		var array = this.data('wait');
		if (!array || array.length == 0)
			return;

		var item = array.shift();
		item.remove();
		this.data('wait', array);
		if (array.length == 0)
			this.css('visibility', 'visible');
	};

	$.namespace = function (value) {
		var parts = value.split('.'), parent = window;
		var current = window;
		for (var i = 0; i < parts.length; i++) {
			if (typeof parent[parts[i]] == 'undefined') {
				current = { };
				parent[parts[i]] = current;
				parent = current;
			}
		}
		return current;
	};
})(jQuery);