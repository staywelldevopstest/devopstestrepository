﻿$(function () {

	Initialization.start();

});

var Initialization = (function () {
	var preloadedImages = [];

	function start() {

		// prevent ie from changing location when an image is dragged over
		$('body').on('dragover', function (e) { return false; })
			.on('dragenter', function (e) { return false; });

		$(document).on("keydown", function (e) {
		    if (e.which === 8 && !$(e.target).is("input:not([readonly]), textarea")) {
		        e.preventDefault();
		    }
		});
	    
		preloadImages();

		Navigation.load();

		if (!Global.User)
			return;

		if (Timeout)
			Timeout.initialize();
	}

	function preloadImages() {
		var urls = [
			'Content/themes/base/images/icons/icon_settings_hover.png',
			'Content/themes/base/images/waiting.gif',
			'Content/themes/base/images/action-waiting.gif',
			'Content/themes/base/images/action-waiting.gif',
			'Content/themes/base/images/Icons/icon_radio_selected.png',
			'Content/themes/base/images/Icons/icon_radio_unselected.png'
		];

		for (var i = 0; i < urls.length; i++) {
			var image = new Image();
			image.src = Global.ApplicationPath + urls[i];
			preloadedImages[i] = image;
		}
	}

	return {
		start: start
	};

})();