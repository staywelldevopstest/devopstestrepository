﻿var Callback = (function() {
	var self = {
		fromPage: fromPage,
		submitPage: submitPage,
		fromContainer: fromContainer,
		submitContainer: submitContainer,
		submitMain: submitMain,
		submit: submit,
		showError: showError
	};
	
	var FORM_SELECTOR = 'select, input:not([type]), input[type="text"], input[type="password"],textarea,input[type="hidden"],input[type="radio"]:checked,input[type="checkbox"]:checked';

	function fromPage() {
		var inputs = $(FORM_SELECTOR);
		var params = {};
		inputs.each(function () {
			var name = $(this).attr('name');
			if (name) {
				params[name] = $(this).val();
			}
		});
		return params;
	}

	function submit(options) {
		if (typeof options == 'string') {
			return send({
				service: arguments[0],
				params: arguments[1],
				success: arguments[2],
				failure: arguments[3],
				method: 'POST'
			});
		}
		else {
		    return send(options);
		}
	}
	
	function send(options) {

		var defaultOptions = {
			method: 'POST',
			params: {}
		};

		options = $.extend(defaultOptions, options);

		if (options.form) {
			var params = fromContainer(options.form);
			options.params = $.extend(params, options.params);
		}		
		
		var service = options.service;
		if (service.charAt(0) != '/')
			service = '/' + service;

        //if(options.params)
		//    var target = options.params.target;
	    
		return $.ajax(Global.ApplicationPath + 'Callback' + service, {
			traditional: true,
			type: options.method,
			data: options.params,
			async: true,
			cache: false,
			success: function (data) {
				if (options.success)
					options.success(data, options.params);
			},
			error: function (xhr, status, errorThrown) {
				handleError(xhr, status, errorThrown, options.failure);
			}
		});
	}

	function submitPage(target, success, failure) {

		var params = fromPage();

		return $.ajax({
			url: target,
			type: 'POST',
			data: params,
			success: function (data) {
				if (success)
					success(data);
			},
			error: function (xhr, status, errorThrown) {
				handleError(xhr, status, errorThrown, failure);
			}
		});
	}

	function handleError(xhr, status, errorThrown, failure) {
		if (xhr.status && (xhr.status == 401 || xhr.status == 403))
			location.href = Global.LoginPath;

		if (xhr.responseText) {
			try {
				var error = $.parseJSON(xhr.responseText);

				if (error && error.StatusCode && (xhr.status == 401 || xhr.status == 403))
					location.href = Global.LoginPath;

				if (error && error.Details) {
					if (failure)
						failure(error);
					else
						showError(error.Details);
					return;
				}
			} catch (e) {
			}
		}

		var message = {
			StatusCode: 500,
			StatusDescrition: 'Communication Error',
			Details: errorThrown ? ('An error occurred communicating with the server: ' + errorThrown)
				: 'Cannot communicate with the server.'
		};

		if (failure)
			failure(message);
		else
			showError(message.Details);
	}
	
	function showError(error) {
		if (Warning) {
			var warning = new Warning(error);
			warning.show(3500);
		}
		else {
			alert(error);
		}
	}

	function fromContainer(container) {
		if (typeof container == 'string')
			container = $(container);

		var inputs = container.find(FORM_SELECTOR);

		var params = {};
		inputs.each(function () {
			var name = $(this).attr('name');

			if (name) {
				if (name.indexOf(':') > 0) {
					name = name.substring(0, name.indexOf(':'));
				}

				params[name] = $(this).val();
			}
		});
		return params;
	}

	function submitContainer(target, container, parameters, success) {
		var submitParams = fromContainer(container);

		if (parameters)
			submitParams = $.extend(submitParams, parameters);

		Callback.submit(target, submitParams, success);
	}

	function submitMain(target, parameters, success) {
		submitContainer(target, '#mainContent', parameters, success);
	}

	return self;
})();
