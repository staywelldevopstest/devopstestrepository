﻿
var Popup = function () {
	var self = {
		show: show,
		close: close
	};

	var overlay = null, frame = null;

	function show(content, settings) {
		if (!settings)
			settings = {};

		if (content.length > 1)
			throw 'Invalid popup usage: the content must contain only one parent object.';

		var positioner = Html.div().css({
			position: 'relative'
			//'text-align': 'center'
		});

		var inner = Html.div().css({
			'width': '700px',
			'margin': '50px auto'
		});
		
		if (settings.wide) {
			inner.css('width', '900px');
		}

		frame = Html.div().hide();

		if (settings.classes) {
			frame.addClass(settings.classes);
		}

		//if (settings.autoSize)
		//	frame.addClass('auto-size');
		var z = $.topZIndex();

		frame.css({
			'z-index': z + 2,
			position: 'fixed',
			top: 0,
			left: 0,
			right: 0,
			bottom: 0,
			overflow: 'auto'
		});

		inner.append(content);
		frame.append(positioner);
		positioner.append(inner);

		overlay = Html.div();
		overlay.css({
			position: 'fixed',
			left: 0,
			right: 0,
			top: 0,
			bottom: 0,
			'z-index': z + 1,
			opacity: .7,
			background: '#fefefe'
		});

		var body = $('body');
		body.css({
			overflow: 'hidden'
		});
		body.append(overlay, frame);

		//onBodyResize();

		//$(window).resize(onBodyResize);

		overlay.fadeIn(600);
		frame.fadeIn(600);
	}

	function onBodyResize() {
		// calculate the values for center alignment
		var dialogTop = 90;
		var dialogLeft = ($(window).width() - $('.dialog-box').width()) / 2;
		frame.css({ position: 'fixed', top: dialogTop, left: dialogLeft + 'px' });
	}

	function close(callback) {
		// By using remove, we are unbinding any events tied to the frame or overlay.
		// This can cause undesirable effects when a dialog should only be created once and events are bound at creation
		frame.fadeOut(600, function () {
			$(window).unbind('resize', onBodyResize);
			//frame.remove();
		});

		frame.css({
			overflow: 'hidden'
		});

		$('body').css({
			overflow: ''
		});

		overlay.fadeOut(600, function () {
			overlay.remove();
			if (callback)
				callback();
		});
	}

	return self;
};

