﻿
var ResourceProvider = (function () {
	var self = {
		getDashboard: getDashboard,
		getClientDashboard: getClientDashboard,
		getLogHtml: getLogHtml,
		getTestContentHtml: getTestContentHtml,
		getContentHtml: getContentHtml,
		getReportsHtml: getReportsHtml,
		getClientHtml: getClientHtml,
		getContentBuckets: getContentBuckets,
		getContentBucketEditor: getContentBucketEditor,
		getClientUserHtml: getClientUserHtml,
		getSearchListControl: getSearchListControl,
		getContentBucketDetailView: getContentBucketDetailView,
		getLicensesHtml: getLicensesHtml,
		getMenuHtml: getMenuHtml,
		getSmsModuleHtml: getSmsModuleHtml,
		getCopyrights: getCopyrights,
		getAdministration: getAdministration,
		getHtml: getHtml,
		getUserEditor: getUserEditor,
		gettAppSettingsBar: getAppSettingsBar,
		
		getContentManagementListHtml: getContentManagementListHtml
		//getCollectionHtml: getCollectionHtml
	};

	var cache = {};

	function getAdministration(target) {
		getHtml('Administration/Administration.html', target);
	}

	function getCopyrights(target) {
		getHtml('Administration/Copyrights.html', target);
	}

	function getDashboard(target) {
		getHtml('Administration/Dashboard.html', target);
	}
	
	function getClientDashboard(target) {
		getHtml('ClientUser/Dashboard.html', target);
	}
	
	function getLogHtml(target) {
		getHtml('Administration/Logs.html', target);
	}
	
	function getSmsModuleHtml(target) {
		getHtml('Administration/SmsModule.html', target);
	}
	
	function getTestContentHtml(target) {
		getHtml('Administration/TestContentEdit.html', target);
	};
	
	function getContentHtml(target) {
		getHtml('Administration/Content.html', target);
	};
	




	
	function getContentManagementListHtml(target) {
	    getHtml('Content/ContentManagementList.html', target);
	};
    

    // dead...
	//function getCollectionHtml(target) {
	//    getHtml('Content/Collections.html', target);
	//};
    




    
	function getLicensesHtml(target) {
		getHtml('Administration/Licenses.html', target);
	}
	
	function getReportsHtml(target) {
		getHtml('Administration/Reports.html', target);
	};
	
	function getClientHtml(target) {
		getHtml('Administration/Clients.html', target);
	}

	function getClientUserHtml(target) {
		getHtml('Administration/ClientUsers.html', target);
	}

	function getContentBuckets(target) {
		getHtml('Administration/ContentBuckets.html', target);
	}

	function getContentBucketEditor(target) {
		getHtml('Administration/ContentBucketEditor.html', target);
	}

	function getContentBucketDetailView(target) {
		getHtml('Administration/ContentBucketDetailView.html', target);
	}
	
	function getSearchListControl(target) {
		getHtml('Controls/SearchList.html', target);
	}

	function getMenuHtml(target) {
		getHtml('Controls/Menu.html', target);
	}
	
	function getUserEditor(target) {
		getHtml('ClientUser/UserEditor.html', target);
	}
	
	function getAppSettingsBar(target) {
		getHtml('ClientUser/AppSettingsBar.html', target);
	}
	
	function getHtml(path, target) {
		if (typeof target == 'function') {
			getResource(path, function (data) {
				data = $(data);
				if (!data)
					alert('The html resource is invalid: ' + path + '.');
				target($(data));
			});
		}
		else {
			getResource(path, target);
		}
	};

	function getResource(path, target) {
		if (!target)
			return;

		var key = path.toLowerCase();

		var resource = cache[key];
		
		if (resource) {
			callTarget(target, resource);
			return;
		}

		var url = Global.ApplicationPath + 'Resource/' + path;

		$.ajax(url, {
			cache: false,
			success: function (data) {
				cache[key] = data;
				callTarget(target, data);
			},
			error: function (xhr, status, errorThrown) {
				debugger;
				var warning = new Warning('Cannot access a required resource: ' + Global.ApplicationPath + 'Resource/' + path + '.');
				warning.show(3500);
			}
		});

		return;
	};
	
	function callTarget(target, data) {
		if (typeof target == 'function')
			target(data);
		else if (target.append)
			target.append(data);
	}
	
	return self;
})();
