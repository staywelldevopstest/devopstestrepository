﻿var InputFocus = {};

InputFocus.SetFocus = function () {
    //Set focus scripts.  These run in order so the last one that applies is the one that wins.
    //Sets focus to the first input 
    var firstInput = $("input[type=text]");
        
    if (!$(firstInput).hasClass('noFocus'))
        firstInput.focus();

    //If you have a different item you want focus give it this class
    $(".defaultfocus").first().focus();

    //This will set the focus to a a control with add focus
    $(".addfocus").first().focus();
};

InputFocus.SetFocusToClass = function (className) {
    $(classToFocus).first().focus();
};

$(document).ready(function () {
    InputFocus.SetFocus();
});