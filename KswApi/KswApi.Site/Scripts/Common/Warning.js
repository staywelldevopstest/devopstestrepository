﻿
(function() {
	var internal = {
		count: 0,
		container: null,
		zIndex: 0
	};

	window.Warning = function(content) {
		var self = {
			show: show,
			hide: hide
		};

		var timer = null;
		var element = null;

		function show(timeout) {
			hide();
			
			if (!internal.container)
				createContainer();
			

			internal.container.css('z-index', $.topZIndex() + 1);
			internal.count++;
			element = $('<div class="warning">');
			element.append(content);
			element.hide();
			internal.container.append(element);
			element.slideDown();

			if (timer)
				clearTimeout(timer);

			if (timeout) {
				timer = setTimeout(onTimeout, timeout);
			}
		}

		function createContainer() {
			internal.container = $('<div>').css({
				position: 'fixed',
				top: '0px',
				left: '0px',
				width: '100%'
			});

			$('body').prepend(internal.container);
		}

		function onTimeout() {
			timer = null;
			hide();
		}

		function hide() {
			if (timer)
				clearTimeout(timer);

			if (element) {
				var temp = element;

				element = null;

				temp.slideUp(function () {
					temp.remove();
					internal.count--;
					if (internal.count <= 0) {
						if (internal.container) {
							internal.container.remove();
							internal.container = null;
						}
					}
				});
			}
		}

		return self;
	};

})();
