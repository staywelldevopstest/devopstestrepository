﻿var Service = (function () {
	var self = {
		call: call,
		post: post,
		get: get,
		put: put,
		del: del
	};
	
	var FORM_SELECTOR = 'select, input:not([type]), input[type="text"], input[type="password"],textarea,input[type="hidden"],input[type="radio"]:checked,input[type="checkbox"]:checked';

	function call(options) {
		if (typeof options == 'string') {
			send({
				method: arguments[0],
				service: arguments[1],
				data: arguments[2],
				success: arguments[3]
			});
		}
		else {
			send(options);
		}
	}
	
	function post(options) {
		if (typeof options == 'string') {
			send({
				method: 'POST',
				service: arguments[0],
				data: arguments[1],
				success: arguments[2]
			});
		}
		else {
			send($.extend(options, { method: 'POST' }));
		}
	}
	
	function get(options) {
		if (typeof options == 'string') {
			send({
				method: 'GET',
				service: arguments[0],
				data: arguments[1],
				success: arguments[2]
			});
		}
		else {
			send($.extend(options, { method: 'GET' }));
		}
	}
	
	function put(options) {
		if (typeof options == 'string') {
			send({
				method: 'PUT',
				service: arguments[0],
				data: arguments[1],
				success: arguments[2]
			});
		}
		else {
			send($.extend(options, { method: 'PUT' }));
		}
	}
	
	function del(options) {
		if (typeof options == 'string') {
			send({
				method: 'DELETE',
				service: arguments[0],
				data: arguments[1],
				success: arguments[2]
			});
		}
		else {
			send($.extend(options, { method: 'DELETE' }));
		}
	}
	
	function send(options) {

		var defaultOptions = {
			method: 'POST',
			data: {}
		};

		options = $.extend(defaultOptions, options);

		if (options.form) {
			var data = getFormData(options.form);
			options.data = $.extend(data, options.data);
		}

		var service = options.service;
		if (service.charAt(0) != '/')
			service = '/' + service;

		if (options.method == 'POST' || options.method == 'PUT') {
			// params as json body
			$.ajax(Global.ApplicationPath + 'Service/' + options.method + service, {
				traditional: true,
				type: 'POST',
				data: JSON.stringify(options.data),
				contentType: 'application/json; charset=utf-8',
				async: true,
				cache: false,
				success: function (result) {
					if (options.success)
						options.success(result);
				},
				error: function (xhr, status, errorThrown) {
					handleError(xhr, status, errorThrown, options.failure);
				}
			});
		}
		else {
			// params as query parameters
			$.ajax(Global.ApplicationPath + 'Service/' + options.method + service, {
				traditional: true,
				type: 'GET',
				data: options.data,
				async: true,
				cache: false,
				success: function (result) {
					if (options.success)
						options.success(result);
				},
				error: function (xhr, status, errorThrown) {
					handleError(xhr, status, errorThrown, options.failure);
				}
			});
		}
	}

	function handleError(xhr, status, errorThrown, failure) {
		if (xhr.status && (xhr.status == 401 || xhr.status == 403))
			location.href = Global.LoginPath;

		if (xhr.responseText) {
			try {
				var error = $.parseJSON(xhr.responseText);

				if (error && error.StatusCode && (xhr.status == 401 || xhr.status == 403))
					location.href = Global.LoginPath;

				if (error && error.Details) {
					if (failure)
						failure(error);
					else
						showError(error.Details);
					return;
				}
			} catch (e) {
			}
		}

		var message = {
			StatusCode: 500,
			StatusDescrition: 'Communication Error',
			Details: errorThrown ? ('An error occurred communicating with the server: ' + errorThrown)
				: 'Cannot communicate with the server.'
		};

		if (failure)
			failure(message);
		else
			showError(message.Details);
	}

	function showError(error) {
		if (Warning) {
			var warning = new Warning(error);
			warning.show(3500);
		}
		else {
			alert(error);
		}
	}
	
	function getFormData(form) {
		if (typeof form == 'string')
			form = $(form);

		var inputs = form.find(FORM_SELECTOR);

		var params = {};
		inputs.each(function () {
			var name = $(this).attr('name');

			if (name) {
				if (name.indexOf(':') > 0) {
					name = name.substring(0, name.indexOf(':'));
				}

				params[name] = $(this).val();
			}
		});
		return params;
	}

	return self;
})();
