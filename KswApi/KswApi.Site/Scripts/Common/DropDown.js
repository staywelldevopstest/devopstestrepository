﻿DropDown = function (classes, name, options, selected, valueTransform) {
    
    var self = {
        expand: expand,
        selectIndex: selectIndex,
        select: select,
        retract: retract,
        val: val,
        setLabel: setLabel,
        setOptions: setOptions,
        data: data,
        selectOption: selectOption
    };

    // ReSharper disable InconsistentNaming
    var _active = false,
        _main = null,
        _dropdown = null,
        _hidden = null,
        _label = null,
        _labelValue = null,
        _originalZIndex = null,
        _options = null,
        _selectedOption = null,
        _selectedValue = null;
    // ReSharper restore InconsistentNaming

    construct();

    function construct() {
        _label = Html.div('current');

        _label.disableSelection();

        _hidden = $('<input type="hidden">');

        if (name) {
            _hidden.attr('name', name);
            _hidden.attr('id', name);
            _hidden.data('name', name); // CHECK WHICH OF THESE IS NECESSARY...
        }
        
        _main = Html.div(classes,
			Html.div('arrow'),
			_label,
			_hidden
			);

        var zIndex = $.topZIndex() + 1;

        _main.css({
            'position': 'relative',
            'z-index': zIndex
        });

        setOptions(options);

        _main.mouseleave(function () {
            if (_active) {
                retract();
            }
        });

        _main.click(function () {
            if (_active)
                retract();
            else
                expand();
        });

        //_main = ret;

        if (selected)
            select(selected);

        self = $.extend(_main, self);
    }

    function setLabel(value) {

        if (typeof value == 'string') {
            _labelValue = value;
        } else {
            _labelValue = null;
        }

        updateLabelText();
    }

    function setOptions(newOptions, triggerChange) {
        _selectedOption = null;
        _selectedValue = null;
        _options = [];
        
        triggerChange = triggerChange == null ? true : triggerChange;

        setLabel(_labelValue);

        if (newOptions) {
            if (newOptions instanceof Array) {
                for (var i = 0; i < newOptions.length; i++) {
                    _options.push(newOptions[i]);
                }
            }
            else {
                for (var option in newOptions) {
                    // id:value pairs
                    // e.g. List of applications: {'0cb0f52a-82a2-4864-9b4b-a12100530d08': 'applicationName',...}
                    //          => {'name': applicationName, 'value': '0cb0f52a-82a2-4864-9b4b-a12100530d08'}

                    // TODO: should use displayValue instead of 'name'
                    var newOption = { 'name': newOptions[option], 'value': option };
                    _options.push(newOption);
                }
            }
        }
        // select first option by default
        if (newOptions && !_labelValue)
            selectOption(_options[0]);

        updateLabelText();
        if (_main && triggerChange)
            _main.trigger('change');
    }

    // TODO: use valueTransform
    function val(value) {
        if (value) {
            select(value);
            return _hidden.val();
        }
        else {
            return _hidden.val();
        }
    }

    function data() {
        
        // this is a nasty schism....
        if (arguments.length > 0) {
            debugger;
            return $.fn.data.apply(self, arguments);
        }
        //return _hidden.data('data');
        if (!_selectedOption)
            return null;
        
        return _selectedOption.data;
    }

    function expand() {
        _active = true;

        var main = _main;
        main.addClass('active');
        _originalZIndex = main.css('z-index');
        main.css({ 'z-index': $.topZIndex() + 1 });

        var list = Html.div('list');
        _dropdown = list;

        list.css({
            'left': -1,
            'top': main.outerHeight() - 1,
            'width': main.outerWidth() - 2
        });

        var count = 0;

        $.each(_options, function (index, option) {
            var item = createItem(option);
            list.append(item);
            count++;
        });

        if (count > 10) {
            list.css({
                'max-height': '230px',
                'overflow-y': 'scroll'
            });
        }

        main.append(list);
    }

    function retract() {
        _active = false;
        _dropdown.remove();
        _main.removeClass('active');
        _main.css({ 'z-index': _originalZIndex });
    }

    function createItem(option) {
        if (!option)
            return null;

        var item = Html.div('item', option.name);
        item.attr('title', option.name);

        // begging for it's own class...
        item.option = option;

        item.disableSelection();

        if (option.isDatepicker)
            item.click(function (event) {
                event.stopPropagation();
                _dropdown.remove();
                var picker = Html.div('picker');
                _dropdown = picker;
                picker.datepicker(
                     {
                         onSelect: function (date) {
                             retract();
                             item.option.value = date;
                             selectOption(item.option);
                         }
                     }
                 );
                picker.css({
                    'left': -1,
                    'top': _main.outerHeight() - 1,
                    'width': _main.outerWidth() - 2
                });

                _main.append(picker);
            });
        else
            item.click(function (event) {
                event.stopPropagation();
                retract();
                selectOption(item.option);
            });

        return item;
    }

    function selectOption(option, triggerChange) {
        // trigger change by default
        triggerChange = triggerChange == null ? true : triggerChange;

        var optionChanged = option != _selectedOption;
        var valueChanged = option.value != _selectedValue; // since allowing value of option to be altered

        if (optionChanged || valueChanged) {
            _selectedOption = option;
            _selectedValue = option.value;
            
            setHiddenValue(option);

            updateLabelText();
            if (triggerChange)
                _main.trigger('change');
        }
    }

    function setHiddenValue(option) {
        if (valueTransform) {
            var transformed = valueTransform(option);
            _hidden.val(transformed);
            
            // update the option value
            if (option.displayActualValue)
                option.value = transformed;
        } else {
            _hidden.val(option.value);
        }
    }
    
    function updateLabelText() {
        if (!_selectedOption) {
            // display default label
            labelText(_labelValue);
        }
        else {
            if (_selectedOption.displayActualValue)
                labelText(_selectedOption.value);
            else
                labelText(_selectedOption.name);
        }
    }

    function select(value, triggerChange) {
        // select the matching option or the option with the matching value...
        for (var i = 0; i < _options.length; i++) {
            if(_options[i].value == value || _options[i] == value) {
                selectOption(_options[i], triggerChange);
                return;
            }
        }
    }

    function selectIndex(index) {
        // what if the selected index is for an option with variable value (e.g. 'Specific Date')?

        var option = _options[index];
        selectOption(option);
    }

    function labelText(value) {
        if (typeof value == "string") {
            _label.text(value);
            _label.attr('title', value);
            return _label;
        }
        else {
            return _label.text();
        }
    }

    return self;
};