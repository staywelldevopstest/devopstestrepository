﻿/// <reference path="../jquery/jquery-1.8.2.js" />
/// <reference path="../Common/jquery.extensions.js" />
/// <reference path="../Common/Html.js" />

//Convention states to name all jQuery wrapped variables with a preceding $
//var QueriedPager = function ($container, itemsPerPage, maximumNumberOfVisiblePages, reappendOnNavigation, pageNavigationCallback) {
var QueriedPager = function (options) {
	var self = {
			gotoPage: navigateToPage,
			updatePager: updatePager,
			refresh: function () {
				navigateToPage(1);
			},
			refreshCurrentPage: function () {
				navigateToPage(_currentPage);
			}
		},
	    defaults = {
	    	'itemsPerPage': 10,
	    	'maximumNumberOfVisiblePages': 10
	    },
	    _currentPage = 1,
	    _pageCount = 1,
	    _itemsPerPage = 10,
	    _maximumNumberOfVisiblePages = 10,
	    _pageNavigationCallback = null,
	    $pagerContainer = null,
	    $back = Html.anchorButton('iconPreviousPage left', ' ').kswid('pagePrevious'),
	    $next = Html.anchorButton('iconNextPage', ' ').css('display', 'block').kswid('pageNext');
	    
	function construct(options) {
		_currentPage = 1;
		$pagerContainer = Html.div('', '');

		if (options != null) {
			if (typeof options.itemsPerPage !== "undefined") {
				_itemsPerPage = options.itemsPerPage;
			}
			
			if (typeof options.maxVisiblePages !== "undefined") {
				_maximumNumberOfVisiblePages = options.maxVisiblePages;
			}
			
			if (typeof options.pageNavigationCallback !== "undefined") {
				_pageNavigationCallback = options.pageNavigationCallback;
			}
		}

		self = $.extend(self, $pagerContainer);
	}
	
	function navigateToPage(page) {
		var pageToNavigateTo = parseInt(page, 10);

		if (isNaN(pageToNavigateTo)) {
			pageToNavigateTo = 1;
		}

		if (pageToNavigateTo > _pageCount) {
			pageToNavigateTo = _pageCount;
		}

		if (pageToNavigateTo < 1) {
			pageToNavigateTo = 1;
		}

		_currentPage = pageToNavigateTo;
		_pageNavigationCallback(_currentPage, _itemsPerPage, (_currentPage - 1) * _itemsPerPage);
	}

	function updatePager(totalItems) {
		_pageCount = Math.floor((totalItems + _itemsPerPage - 1) / _itemsPerPage);

		var $pagerLinkArea = Html.div('pagerLinkArea', ' ');

		$pagerContainer.html('');
		if (_pageCount > 1) {
			$pagerContainer.append(createGotoPage());
			
			$pagerLinkArea.append(Html.div('pagerPrevButton', $back));
			$pagerLinkArea.append(createPageLinks(_currentPage, _pageCount));
			$pagerLinkArea.append(Html.div('pagerNextButton', $next));
			$pagerLinkArea.on('click', '.' + 'iconPreviousPage', navigateBackPage);
			$pagerLinkArea.on('click', '.' + 'iconNextPage', navigateNextPage);
			$pagerLinkArea.on('click', '.pageLink', function() {
				var $pageClicked = $(this),
				    pageIndex = parseInt($pageClicked.find('a').html(), 10);
				navigateToPage(pageIndex);
				return false;
			});

		}
		$pagerContainer.append($pagerLinkArea);

		enforceButtonStates(_currentPage);
	}

	function createGotoPage() {
		var $gotoPageArea = Html.div('pagerGotoPageArea', '');
		var $gotoPageLabel = Html.div('pagerGotoPageLabel', 'Go to page:');
		var $gotoPageInput = Html.textBox('pagerGotoPageInput textBox', '').kswid('goPageTextbox');
		var $gotoPageButton = Html.button('pagerGotoPageButton', '', function() {
			navigateToPage($gotoPageInput.val());
			return false;
		}).kswid('goPageButton');

		$gotoPageInput.enter(function () {
			navigateToPage($gotoPageInput.val());
		});
		
		$gotoPageButton.attr('data-kswid', 'buttonGoPage');

		$gotoPageArea.append($gotoPageButton);
		$gotoPageArea.append($gotoPageInput);
		$gotoPageArea.append($gotoPageLabel);
		
		return $gotoPageArea;
	}

	function createPageLinks(selectedPage, pageCount) {
		var $pageLinks = Html.div('pagerPageLinks', ' '),
		    startPage = 2,
		    endPage = pageCount,
		    pageRange = (_maximumNumberOfVisiblePages - 2),
		    halfPageCount = parseInt(pageCount / 2, 10),
		    halfPageRange = parseInt(pageRange / 2, 10);
		    
	    if (pageCount > _maximumNumberOfVisiblePages) {
	    	if (selectedPage <= halfPageCount) {
	    		startPage = Math.max(selectedPage - halfPageRange, 2);
	    		endPage = startPage + pageRange;
	    	}
	    	else {
	    		endPage = Math.min(selectedPage + halfPageRange + 1, pageCount);
	    		startPage = endPage - pageRange;
	    	}
	    }

	    $pageLinks.append(createPageLink(selectedPage, 1));
		    
		if (startPage > 2) {
		    $pageLinks.append($('<span>').text('...').addClass('pagerPageLinks'));
		}

	    for (var pageIndex = startPage; pageIndex < endPage; pageIndex++) {
	    	$pageLinks.append(createPageLink(selectedPage, pageIndex));
	    }
		    
		if (endPage < pageCount) {
			$pageLinks.append($('<span>').text('...').addClass('pagerPageLinks'));
		}

		$pageLinks.append(createPageLink(selectedPage, pageCount));

		return $pageLinks;
	}

	function createPageLink(selectedPage, pageIndex) {
	    var pageLinkClass = (pageIndex == selectedPage ? 'pagerSelectedPage' : 'pagerPageLinks'),
			$pageLink = Html.div('pageLink', Html.anchorButton(pageLinkClass, pageIndex).kswid('pageLink'));

	    $pageLink.attr('data-pageIndex', pageIndex);
	    return $pageLink;
	}

	function navigateBackPage() {
		if (_currentPage > 1) {
			navigateToPage.call(this, _currentPage - 1.0);
		}
	}

	function navigateNextPage() {
		if (_currentPage < _pageCount) {
			navigateToPage.call(this, _currentPage + 1.0);
		}
	}

	function enforceButtonStates(page) {
	    if (1 < page) {
	    	$back.removeClass('iconPreviousPageDisabled');
	    	$back.addClass('iconPreviousPage');
		} else {
	    	$back.removeClass('iconPreviousPage');
	    	$back.addClass('iconPreviousPageDisabled');
	    }

		if (page < _pageCount) {
		    $next.removeClass('iconNextPageDisabled');
		    $next.addClass('iconNextPage');
		} else {
		    $next.removeClass('iconNextPage');
		    $next.addClass('iconNextPageDisabled');
		}
	}

	construct(options);

	return self;
};

//Example
//_pager = new QueriedPager({
//	itemsPerPage: LOGS_PER_PAGE,
//	maxVisiblePages: MAXIMUM_NUMBER_OF_VISIBLE_PAGES,
//	pageNavigationCallback: function (page, itemsPerPage, offset) {
//		var promise = $.Deferred(function (def) {
//			var itemCount;

//			$.ajax({
//				url: Global.ApplicationPath + 'Ajax/Logging',
//				data: getParameters(page - 1),
//				type: 'POST',
//				success: function (logs) {
//					populate(logs);
//					itemCount = logs.Total;
//				}
//			}).done(function () {
//				def.resolve(itemCount);
//			});
//		});

//		return promise;
//	}
//});