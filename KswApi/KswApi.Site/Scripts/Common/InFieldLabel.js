﻿/// <reference path="../_references.js" />
/* File Created: July 31, 2012 */

var inFieldLabel = { };

inFieldLabel.SetupLoginInFieldLabels = function () {
    $('#loginUser label').show();
    $("#loginUser label").inFieldLabels();
};

inFieldLabel.SetupClientDataInFieldLabels = function () {
    $('#addClientData label').show();
    $('#addClientData label').inFieldLabels();
};

inFieldLabel.SetupClientLicenseInFieldLabels = function () {
    $('#addClientLicense label').show();
    $('#addClientLicense label').inFieldLabels();
};
