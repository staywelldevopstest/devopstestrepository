﻿var checkBoxClick = function clicked(checkBox) {
    if ($(checkBox).hasClass('checked')) {
        $(checkBox).removeClass('checked');
    } else {
        $(checkBox).addClass('checked');
    }
}
