﻿// hide event handling methods

var Timeout = function (settings) {
	var self = {
		start: start,
		resume: resume,
		stop: stop,
		set: set
	};

	var TIMEOUT_IN_MINUTES = 20,
		COUNTDOWN_IN_SECONDS = 120,
		POLL_INTERVAL_IN_SECONDS = 60;

	var running = false, lastEvent, timer = null, warning = false, dirty = false;

	construct();

	function construct() {
		var defaultSettings = {
			timeout: TIMEOUT_IN_MINUTES * 60 * 1000,
			countdown: COUNTDOWN_IN_SECONDS * 1000,
			poll: POLL_INTERVAL_IN_SECONDS * 1000,
			onPoll: null,
			onWarning: null,
			onCountdown: null,
			onTimeout: null
		};

		settings = $.extend(defaultSettings, settings);

		if (settings.timeout < 0)
			settings.timeout = 0;
		if (settings.countdown > settings.timeout)
			settings.countdown = settings.timeout;
	}

	function set(timeout) {
		var now = Date.now();
		settings.nextWarning = new Date(+now + (timeout - settings.countdown));
		settings.nextTimeout = new Date(+now + timeout);
		next(now);
	}

	function start() {
		if (running)
			return;

		running = true;

		var now = Date.now();

		lastEvent = now;

		settings.startTime = now;

		if (settings.onPoll && settings.poll) {
			settings.nextPoll = new Date(+now + settings.poll);
		}

		settings.nextWarning = new Date(+now + (settings.timeout - settings.countdown));
		settings.nextTimeout = new Date(+now + settings.timeout);

		next(now);

		registerEvents();
	}

	function next(now) {
		if (timer)
			clearTimeout(timer);

		var array = [
			settings.nextTimeout,
			settings.nextWarning,
			settings.nextPoll,
			settings.nextCount
		];

		var min = null;

		for (var i = 0; i < array.length; i++) {
			var current = array[i];
			if (current) {
				if (!min || current < min)
					min = current;
			}
		}

		if (!min)
			return;

		var nextEvent = min - now;
		if (nextEvent < 0)
			nextEvent = 0;

		timer = setTimeout(onTimer, nextEvent);
	}

	function resume() {
		var now = Date.now();
		lastEvent = now;

		settings.nextWarning = new Date(+now + (settings.timeout - settings.countdown));
		settings.nextTimeout = new Date(+now + settings.timeout);
		settings.nextCount = null;
		warning = false;

		next(now);
	}

	function registerEvents() {
		var events = ['mousemove', 'keydown', 'DOMMouseScroll', 'mousewheel', 'mousedown'];
		$.each(events, function (index, value) {
			$(document).on(value, onEvent);
		});
	}

	function unregisterEvents() {
		var events = ['mousemove', 'keydown', 'DOMMouseScroll', 'mousewheel', 'mousedown'];
		$.each(events, function (index, value) {
			$(document).off(value, onEvent);
		});
	}

	function dispatch(now) {

		if (now >= settings.nextTimeout) {

			stop();
			
			if (settings.onTimeout)
				settings.onTimeout();

			return;
		}

		if (warning) {
			updateCountdown(now);
		}
		else {

			if (dirty) {
				settings.nextTimeout = new Date(+lastEvent + settings.timeout);
				settings.nextWarning = new Date(+lastEvent + (settings.timeout - settings.countdown));
				dirty = false;
			}
			
			if (settings.nextWarning && now >= settings.nextWarning && settings.onWarning) {
				warning = true;
				settings.onWarning();
				settings.nextCount = now;
				settings.nextWarning = null;
				updateCountdown(now);
				return;
			}
			
			if (settings.nextPoll && now >= settings.nextPoll && settings.onPoll) {
				if (lastEvent)
					settings.onPoll(+now - lastEvent);
				else
					settings.onPoll(0);
				settings.nextPoll = new Date(+now + settings.poll);
			}
		}
	}
	
	function updateCountdown(now) {
		if (settings.nextCount && now >= settings.nextCount && settings.onCountdown) {
			var secondsLeft = Math.floor(+(settings.nextTimeout - now) / 1000);
			settings.onCountdown(secondsLeft);
			// next countdown is now + 1 second
			settings.nextCount = new Date(+now + 1000);
		}
	}

	function onEvent() {
		if (warning)
			return;
		lastEvent = Date.now();
		dirty = true;
	}

	function stop() {
		if (!running)
			return;

		running = false;
		
		if (timer)
			clearTimeout(timer);

		unregisterEvents();

		settings.nextTimeout = null;
		settings.nextWarning = null;
		settings.nextPoll = null;
		settings.nextCount = null;
	}

	function onTimer() {
		timer = null;

		var now = Date.now();

		dispatch(now);

		if (running)
			next(now);
	}

	return self;
};


Timeout.initialize = function () {
	startSessionTimeout();

	function startSessionTimeout() {
		if (this.running)
			return;
		
		if (!Global.User)
			return;

		this.running = true;

		var warning = null, countdownContainer = null;

		var timeout = new Timeout({
			timeout: Global.User.TimeoutInMilliseconds,
			countdown: Global.User.TimeoutCountdownInMilliseconds,
			poll: Global.User.TimeoutPollIntervalInMilliseconds,
			onTimeout: logout,
			onWarning: onWarning,
			onCountdown: onCountdown,
			onPoll: onPoll
		});

		timeout.start();

		function logout() {
			location.href = Global.ApplicationPath + 'Logout';
		}

		function onWarning() {
			var content = $('<div><b><i>Attention!</i></b> You will be logged out in <span id="dialog-countdown"></span> due to inactivity. <a id="idletimeout-resume" href="javascript:;">Click Here to Continue Using KSWAPI</a></div>');
			content.find('#idletimeout-resume').click(onResume);
			countdownContainer = content.find('#dialog-countdown');

			warning = new Warning(content);
			warning.show();
		}

		function onCountdown(count) {
			if (countdownContainer) {
				var seconds = (count % 60);
				var text = Math.floor(count / 60) + ':';
				if (seconds < 10)
					text += '0';
				text += seconds;
				countdownContainer.text(text);
			}
		}

		function onResume() {
			warning.hide();
			timeout.resume();
		}

		function onPoll(milliseconds) {
			Service.get({
				service: 'Authorization/KeepAlive',
				data: { idleMilliseconds: milliseconds },
				success: onPollResponse
			});
		}

		function onPollResponse(data) {
			timeout.set(data.TimeoutInMilliseconds);
		}

	}
};
