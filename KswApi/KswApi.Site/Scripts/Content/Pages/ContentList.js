﻿var Content = Content || {};
Content.Pages = Content.Pages || {};

Content.Pages.ContentList = function () {
	var self = {
		show: show,
		home: home,
		update: update,
		refreshCurrentPage: refreshCurrentPage
	};

	var ITEMS_PER_PAGE = 10,
	    MAX_VISIBLE_PAGES = 10,
	    contentItems = null,
	    contentListArea = null,
	    bucketSelector,
	    bucketSelectorElement,
	    initialSearchState = true,
	    toolbar,
	    readOnly,
	    created = false;

	var template;

	var controls = {
		bucketElements: {}
	};

	var filters = {
		buckets: [],
		query: ''
	};

	construct();

	function construct() {
		self = $.extend(Html.div(), self);
	}

	function onContentViewClose() {
	    show(true);
	}
    
	function show(refresh) {
		readOnly = !Helper.userHasRight('Manage_Content');

		if (!template) {
			ResourceProvider.getContentHtml(onHtml);

			return;
		}

		if (!created)
			createView();

		if (initialSearchState) {
			controls.initialSearch.show();
		}
		else {

		}

		Page.setTitle('CONTENT MANAGEMENT');

		setupToolbar();

		contentListArea.show();

		Page.switchPage(self, function () {
			if (initialSearchState) {
				controls.initialSearch.find('#initialContentSearchInput').select();
			}
			else {
				toolbar.find('#contentSearchInput').select();
			}
		});

		if (!initialSearchState && refresh)
			controls.pager.refresh();

		Navigation.set('content');
	}

	function onHtml(html) {
		setupTemplate(html);

		if (template)
			show();
	}

	function home() {
		if (bucketSelectorElement) {
			bucketSelectorElement.hide('drop',
				{
					direction: 'right',
					duration: 500,
					complete: function () {
						bucketSelectorElement.remove();
						bucketSelectorElement = null;
						bucketSelector = null;
						completeHome();
					}
				});
		}
		else {
			completeHome();
		}
	}

	function completeHome() {
		if (initialSearchState)
			controls.initialSearch.show();

		Page.setTitle('CONTENT MANAGEMENT');

		setupToolbar();

		contentListArea.show();

		if (initialSearchState) {
			controls.initialSearch.find('#initialContentSearchInput').select();
		}
		else {
			toolbar.find('#contentSearchInput').select();
		}
	}

	function setupTemplate(html) {
		template = {
			toolbar: html.find('#contentTitleBar').remove(),
			selectedFilter: html.find('#selectedFilter').remove(),
			noSelectedFilter: html.find('#noSelectedFilters').remove(),
			contentListItem: html.find('#contentListItem').remove(),
			imageListItem: html.find('#imageListItem').remove(),
			deleteDialog: html.find('#deleteContentDialog').remove(),
			resultPanel: html.find('#resultPanel').remove(),
			optionsPanel: html.find('#searchOptions').remove(),
			initialSearch: html.find('#initialContentSearch').remove()
		};
	}

	function refreshCurrentPage() {
		controls.pager.refreshCurrentPage();
	}

	function createView() {

		initialSearchState = true;

		var pager = new QueriedPager({
			itemsPerPage: ITEMS_PER_PAGE,
			maxVisiblePages: 10,
			pageNavigationCallback: function (page, itemsPerPage, offset) {
				var filter = $.extend({}, filters);

				filter['$skip'] = parseInt(offset, 10);
				filter['$top'] = parseInt(itemsPerPage, 10);
				filter.types = ['Content', 'Image'];
				filter.includeDrafts = true;

				Service.get({
					service: 'Content',
					data: filter,
					success: function (data) {
						onData(data, offset);
					}
				});
			}
		});

		Page.setTitle('CONTENT MANAGEMENT');

		setupToolbar();

		controls = {
			pager: pager,
			results: template.resultPanel.clone(),
			options: template.optionsPanel.clone(),
			initialSearch: template.initialSearch.clone(),
			list: Html.div('split left')
		};

		controls.list.append(controls.initialSearch, controls.results);

		self.append(controls.options, controls.list, Html.div('clear'));

		contentListArea = controls.results.find('#contentList');

		controls.options.find('#contentFilterResetButton').click(function () {
			//Cannot add applyFilters() to resetFilters because of initial search functionality
			resetFilters();
		});

		controls.options.find('#contentFilterApplyButton').click(applyFilters);

		controls.options.find('#bucketFilterOption').click(showBucketFilter);

		controls.results.hide();

		if (initialSearchState) {
			controls.initialSearch.show();
		}
		else {
			controls.initialSearch.hide();
		}

		var searchField = controls.initialSearch.find('#initialContentSearchInput');
		searchField.enter(applyFilters);

		controls.initialSearch.find('#initialContentSearchButton').click(applyFilters);

		resetFilters();

		created = true;
	}

	function resetFilters() {
		if (bucketSelector) {
			bucketSelector.clear();
		}

		var bucketId;
		for (bucketId in controls.bucketElements) {
			controls.bucketElements[bucketId].remove();
		}

		controls.bucketElements = {};
		filters.buckets = [];

		if (!initialSearchState) {
			initialSearchState = true;

			contentListArea.find('#contentArea').empty();
			var value = toolbar.find('#contentSearchInput').val();
			controls.initialSearch.find('#initialContentSearchInput').val(value);
			toolbar.find('#searchArea').hide();
			controls.initialSearch.find('#initialContentSearchInput').select();
			controls.results.hide();
		}

		home();

		controls.initialSearch.find('#initialContentSearchInput').select();
	}

	function getFilters() {
		if (initialSearchState) {
			filters.query = controls.initialSearch.find('#initialContentSearchInput').val();
		}
		else {
			filters.query = toolbar.find('#contentSearchInput').val();
		}

		filters.titleStartsWith = '';
		filters.includeAlternateTitles = false;
		filters.sort = '';
	}

	function applyFilters() {
		getFilters();

		if (bucketSelectorElement) {
			bucketSelectorElement.remove();
			bucketSelectorElement = null;
			bucketSelector = null;
		}

		controls.pager.refresh();
	}

	function onData(data, offset) {

		if (0 < offset && offset >= data.Total) {
			controls.pager.gotoPage(data.Total / ITEMS_PER_PAGE);
		}
		else {
			if (bucketSelectorElement) {
				bucketSelectorElement.remove();
				bucketSelectorElement = null;
				bucketSelector = null;
			}

			controls.results.show();
			contentListArea.show();

			populate(data.Items);

			contentListArea.find('#resultCount').text(data.Total + ' results');

			toolbar.find('#contentSearchInput').select();

			self.find('#pager').append(controls.pager);
			controls.pager.updatePager(data.Total);
		}
	}

	function populate(items) {
		contentItems = items;

		if (initialSearchState) {
			initialSearchState = false;
			var value = controls.initialSearch.find('#initialContentSearchInput').val();
			toolbar.find('#contentSearchInput').val(value);
			toolbar.find('#searchArea').show();
			controls.initialSearch.hide();
		}

		var contentArea = contentListArea.find('#contentArea');

		contentArea.empty();

		for (var i = 0; i < items.length; i++) {
			contentArea.append(createContentItem(items[i]));
		}
	}

	function addBucketFilter(bucket) {

		var selectedFilterArea = controls.options.find('#bucketSelectedFilters');
		var newSelectedFilterDiv = template.selectedFilter.clone();

		newSelectedFilterDiv.find('.filterTitle').text(bucket.Name);
		newSelectedFilterDiv.find('.iconRemove').click(function () {
			newSelectedFilterDiv.remove();
			if (bucketSelector) {
				bucketSelector.unselect(bucket);
			}
			filters.buckets.remove(bucket.Id);
			delete controls.bucketElements[bucket.Id];
		});
		selectedFilterArea.append(newSelectedFilterDiv);
		filters.buckets.push(bucket.Id);
		controls.bucketElements[bucket.Id] = newSelectedFilterDiv;
	}

	function removeBucketFilter(bucket) {
		var item = controls.bucketElements[bucket.Id];
		if (!item)
			return;
		item.remove();
		delete controls.bucketElements[bucket.Id];
		filters.buckets.remove(bucket.Id);
	}

	function showBucketFilter() {
		if (bucketSelectorElement) {
			home();
			return;
		}

		controls.initialSearch.hide();
		contentListArea.hide();

		bucketSelector = new Content.Controls.BucketSelector(true, null, filters.buckets);
		bucketSelector.on('selectionChanged', onBucketFilterChanged);

		var closeButton = Html.button('defaultButtonSmall defaultWhiteButton', 'CLOSE').kswid('closebucketFilterArea');

		closeButton.click(home);

		var actionBar = Html.div('bottomActionBar', Html.div('right', closeButton));

		bucketSelectorElement = Html.div('itemBox dark',
			Html.div('',
				Html.div('bucketSelectorTitle', 'SELECT YOUR BUCKET'),
				bucketSelector,
				actionBar
			)
		).hide();

		controls.list.append(bucketSelectorElement);
		bucketSelectorElement.show('drop',
			{
				direction: 'right',
				duration: 500,
				complete: function () {
					bucketSelector.focus();
				}
			});
	}

	function onBucketFilterChanged(evt, bucket, selected) {
		if (selected)
			addBucketFilter(bucket);
		else
			removeBucketFilter(bucket);
	}

	function setupToolbar() {
		if (!template)
			return;

		toolbar = template.toolbar.clone();

		if (Administration.Navigation.hasPermission('Manage_Content')) {
			toolbar.find('#addContent').click(function () {
				showCreateNew();
			});
		}
		else {
			toolbar.find('#addContent').remove();
		}

		toolbar.find('#contentSearchInput').enter(applyFilters);
		toolbar.find('#contentSearchButton').click(applyFilters);

		if (!initialSearchState) {
			toolbar.find('#contentSearchInput').val(filters.query);
			toolbar.find('#searchArea').show();
		}

		Page.setContentTitle(toolbar);
	}

	function update(content) {
		if (!contentItems)
			return;

		for (var i = 0; i < contentItems.length; i++) {
			var current = contentItems[i];
			if (current.Id == content.Id) {
				contentItems[i] = content;
			}
		}
	}

	function createContentItem(content) {
		if (content.Type == 'Image')
			return createImageItem(content);
		else
			return createArticleItem(content);
	}

	function createImageItem(image) {

		var element = template.imageListItem.clone();

		var imageElement = element.find('#image');

		imageElement.data('uri', image.Uri);

		// nonce on the end is to force ie to get the latest version
		imageElement.attr('src', image.Uri + '.75x75?_=' + $.now());

		element.find('#title').text(image.Title);

		element.find('#summary').text(image.Blurb);

		element.find('#type').text('(' + image.Format.toUpperCase() + ')');

		if (readOnly) {
			element.find('#clientUserActions').remove();
		}
		else {
			element.find('#title').click(function () {
				editImageContent(image);
			});

			element.find('#contentEditAction').click(function () {
				editImageContent(image);
			});

			element.find('#contentDeleteAction').click(function () {
				deleteContent(image);
			});
		}

		var meta = element.find('#metadata1');
		addContentMetadata(meta, 'Bucket', image.Bucket.Name, 'bucket');
		addContentMetadata(meta, 'Origin', image.OriginName, 'origin');

		meta = element.find('#metadata2');
		addContentMetadata(meta, 'Created', Helper.getFormattedDate(image.DateAdded), 'created');
		addContentMetadata(meta, 'Modified', Helper.getFormattedDate(image.DateModified), 'modified');

		return element;
	}

	function createArticleItem(content) {
		var element = template.contentListItem.clone();

		element.find('#contentDataTitle').text(content.Title).click(function () {
			editContent(content);
		});

		var revisionArea = element.find('#revisions');

		if (content.Published) {
			var published = $('<a href="javascript:;" class="clickable">Published</label>');

			published.click(function () {
				viewPublished(content);
			});

			revisionArea.append(published, $('<label>, </label>'));
		}

		var draft = $('<a href="javascript:;" class="clickable">Draft</a>');
		draft.click(function () {
			editContent(content);
		});

		revisionArea.append(draft);

		element.find('#contentListPreviewArea').text(content.Blurb);

		if (readOnly) {
			element.find('#clientUserActions').remove();
		}
		else {
			element.find('#contentEditAction').click(function () {
				editContent(content);
			});

			element.find('#contentDeleteAction').click(function () {
				deleteContent(content);
			});
		}

		var metadata = element.find('#metadata1');

		addContentMetadata(metadata, 'Bucket', content.Bucket.Name, 'bucket');
		addContentMetadata(metadata, 'Origin', content.OriginName, 'origin');
		addContentMetadata(metadata, 'Languages', getLanguages(content), 'languages');
		addContentMetadata(metadata, 'Age Range', Helper.getAgeRange(content.AgeCategories), 'ageRange');

		metadata = element.find('#metadata2');
		if (content.LastReviewedDate)
			addContentMetadata(metadata, 'Last Reviewed', Helper.getFormattedDate(content.LastReviewedDate), 'lastReview');
		addContentMetadata(metadata, 'Created', Helper.getFormattedDate(content.DateAdded), 'created');
		addContentMetadata(metadata, 'Modified', Helper.getFormattedDate(content.DateModified), 'modified');

		// add published when finished

		return element;
	}

	function getLanguages(content) {
		var label = $('<label>');

		for (var i = 0; i < content.Versions.length; i++) {
			if (i != 0)
				label.append(', ');
			var item = content.Versions[i];

			label.append(createLanguageLink(content, item));
		}
		return label;
	}

	function createLanguageLink(content, item) {
		var text = Helper.getFormattedLanguageCode(item.Language.Code);
		if (item.Master)
			text = '*' + text;
		return Html.anchor(null, text).kswid('languageItem').click(function () {
		    var newEditor = new Content.Pages.ContentView(item.Id, content.Bucket.Id, self, null, onContentViewClose);
			newEditor.showDraft();
		});
	}

	function addContentMetadata(div, label, value, kswid) {
		if (!div.is(':empty')) {
			div.append(' | ');
		}

		div.append($('<label class="bold">').text(label + ': '));

		if (typeof value == 'string')
			div.append($('<label>').text(value).kswid(kswid));
		else
			div.append(value);
	}

	function deleteContent(content) {
		var popup = new Popup();
		var dialog = template.deleteDialog.clone();
		dialog.find('#type').text(content.Type.toLowerCase());
		dialog.find('#deleteContentName').text(content.Title);
		dialog.find('#deleteContentCancel').click(function () {
			popup.close();
		});

		var publishedConfirm = dialog.find('#deletePublishedConfirm');
		if (content.Published) {
			publishedConfirm.click(function () {
				Service.del({
					service: 'Content/' + content.Bucket.Id + '/' + content.Id,
					data: { publishedOnly: true },
					success: function () {
						controls.pager.refreshCurrentPage();
						popup.close();
					}
				});
			});
		}
		else {
			publishedConfirm.remove();
		}

		dialog.find('#deleteContentConfirm').click(function () {
			Service.del({
				service: 'Content/' + content.Bucket.Id + '/' + content.Id,
				success: function () {
					controls.pager.refreshCurrentPage();
					popup.close();
				}
			});
		});

		popup.show(dialog);
	}

	function viewPublished(content) {
	    var contentView = new Content.Pages.ContentView(content.Id, content.Bucket.Id, self, null, onContentViewClose);
		contentView.showPublished();
	}

	function editContent(content) {
	    var contentView = new Content.Pages.ContentView(content.Id, content.Bucket.Id, self, null, onContentViewClose);
		contentView.showDraft();
	}

	function editImageContent(image) {
		var editor = new Content.ImageEditor(image, self);
		editor.show();
	}

	function showCreateNew() {
		Page.setContentTitle('Add Content');

		var wizard = new Content.Controls.BucketWizard();

		wizard.on('cancel', function () {
			bucketSelector = null;
			bucketSelectorElement = null;
			Page.switchPage(self);
			home();
		});

		wizard.on('submit', function (evt, bucket) {
			createContent(bucket);
		});

		Page.switchPage(wizard, function () {
			wizard.focus();
		});
	}

	function createContent(bucket) {

		switch (bucket.Type) {
			case 'Image':
				var editor = Content.Pages.ImageUploader(bucket);
				editor.show();

				break;
			default:
			    var editor = Content.Pages.ContentView(null, bucket, self, null, onContentViewClose);
				editor.showDraft();
				break;
		}

	}

	return self;
};
