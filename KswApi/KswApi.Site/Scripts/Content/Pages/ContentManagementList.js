﻿var Content = Content || {};
Content.Pages = Content.Pages || {};

Content.Pages.ContentManagementList = function (title, addTitle, listType, service, filterTypes) {

    // CHANGE TO PARAMS
    filterTypes = ['Collection']; //['Content', 'Image'];//
    service = 'Collections'; //'Content';//
    //
    
    // do something like this instead??
    var listTypes =
        [
            {
                'type': 'Content',
                'service': 'Content',
                'filterTypes': ['Content', 'Image']
            }
        ];
    

    var self = {
        show: show,
        home: home,
        update: update,
        refreshCurrentPage: refreshCurrentPage
    };

    var ITEMS_PER_PAGE = 10,
	    MAXIMUM_NUMBER_OF_VISIBLE_PAGES = 10,
	    searchItems = null,
	    contentListArea = null,
	    bucketSelector,
	    bucketSelectorElement,
	    initialSearchState = true,
	    toolbar,
	    readOnly,
	    created = false;

    var template;

    //var listHtml = null;
    
    var controls = {
        bucketElements: {}
    };

    var filters = {
        buckets: [],
        query: ''
    };
    
    var searchItemConstructors = {
        'Image': Content.Controls.ImageSearchItem,
        'Article': Content.Controls.ArticleSearchItem,
        'Text': Content.Controls.ArticleSearchItem,
        'Video': Content.Controls.ArticleSearchItem,
        'Content': Content.Controls.ArticleSearchItem, // default for Content
        'Collections': Content.Controls.CollectionSearchItem,
        'Bucket': Content.Controls.BucketSearchItem
    };
    
    var editors = {
        'Image': Content.Pages.ImageUploader,
        'Content': Content.Pages.ContentView,
        'Collections': Content.Pages.CollectionView
    };

    construct();

    function construct() {
        self = $.extend(Html.div(), self);
    }

    function onContentViewClose() {
        show(true);
    }

    function show(refresh) {
        readOnly = !Helper.userHasRight('Manage_Content');

        if (!template) {
            ResourceProvider.getHtml('Content/ContentManagementList.html', onHtml);
            return;
        }

        if (!created)
            createView();

        if (initialSearchState) {
            controls.initialSearch.show();
        }
        else {

        }

        Page.setTitle(title);

        setupToolbar();

        contentListArea.show();

        Page.switchPage(self, function () {
            if (initialSearchState) {
                controls.initialSearch.find('#initialSearchInput').select();
            }
            else {
                toolbar.find('#searchInput').select();
            }
        });

        if (!initialSearchState && refresh)
            controls.pager.refresh();

        Navigation.set(listType.toLowerCase());
    }

    function onHtml(html) {
        setupTemplate(html);

        if (template)
            show();
    }

    function home() {
        if (bucketSelectorElement) {
            bucketSelectorElement.hide('drop',
				{
				    direction: 'right',
				    duration: 500,
				    complete: function () {
				        bucketSelectorElement.remove();
				        bucketSelectorElement = null;
				        bucketSelector = null;
				        completeHome();
				    }
				});
        }
        else {
            completeHome();
        }
    }

    function completeHome() {
        if (initialSearchState)
            controls.initialSearch.show();

        Page.setTitle(title);

        setupToolbar();

        contentListArea.show();

        if (initialSearchState) {
            controls.initialSearch.find('#initialSearchInput').select();
        }
        else {
            toolbar.find('#searchInput').select();
        }
    }

    function setupTemplate(html) {
        template = {
            toolbar: html.find('#contentTitleBar').remove(),
            selectedFilter: html.find('#selectedFilter').remove(),
            noSelectedFilter: html.find('#noSelectedFilters').remove(),
            


            // search items
            contentListItem: html.find('#contentListItemTemplate').remove(),
            imageListItem: html.find('#imageListItemTemplate').remove(),
            bucketListItem: html.find('#bucketListItemTemplate').remove(),
            collectionListItem: html.find('#collectionListItemTemplate').remove(),

            // search keys
            contentSearchKey: html.find('#contentSearchKey').remove(),
            bucketSearchKey: html.find('#bucketSearchKey').remove(),
            collectionSearchKey: html.find('#collectionSearchKey').remove(),

            // dialogs
            deleteContentDialog: html.find('#deleteContentDialog').remove(),
            deleteBucketDialog: html.find('#deleteBucketDialog').remove(),
            deleteCollectionDialog: html.find('#deleteCollectionDialog').remove(),
            
            // Going forward, use the generic delete confirmation dialog where possible
            deleteConfirmationDialog: html.find('#delete-confirmation').remove(),

            resultPanel: html.find('#resultPanel').remove(),
            searchOptionsPanel: html.find('#searchOptions').remove(),
            initialSearch: html.find('#initialSearch').remove()
        };
    }

    function refreshCurrentPage() {
        controls.pager.refreshCurrentPage();
    }
    
    function createView() {
        initialSearchState = true;
        
        var pager = new QueriedPager({
            itemsPerPage: ITEMS_PER_PAGE,
            maxVisiblePages: MAXIMUM_NUMBER_OF_VISIBLE_PAGES,
            pageNavigationCallback: function (page, itemsPerPage, offset) {
                //var filter = appliedFilter;
                var filter = $.extend({}, filters);

                filter['$skip'] = parseInt(offset, 10);
                filter['$top'] = parseInt(itemsPerPage, 10);
                filter.types = filterTypes;

                Service.get({
                    service: service,
                    data: filter,
                    success: function (data) {
                        onData(data, offset);
                    }
                });
            }
        });

        Page.setTitle(title);

        setupToolbar();

        controls = {
            pager: pager,
            results: template.resultPanel.clone(),
            searchOptions: template.searchOptionsPanel.clone(),
            initialSearch: template.initialSearch.clone(),
            list: Html.div('split left')
        };

        var searchKey = getSearchKey();
        if (searchKey)
            controls.initialSearch.find('#searchKeyContainer').append(searchKey);

        setupSearchFilter();

        controls.list.append(controls.initialSearch, controls.results);

        self.append(controls.searchOptions, controls.list, Html.div('clear'));

        contentListArea = controls.results.find('#contentList');

        controls.searchOptions.find('#contentFilterResetButton').click(function () {
            //Cannot add applyFilters() to resetFilters because of initial search functionality
            resetFilters();
        });

        controls.searchOptions.find('#contentFilterApplyButton').click(applyFilters);

        controls.searchOptions.find('#bucketFilterOption').click(showBucketFilter);

        controls.results.hide();

        if (initialSearchState) {
            controls.initialSearch.show();
        }
        else {
            controls.initialSearch.hide();
        }


        var searchField = controls.initialSearch.find('#initialSearchInput');
        searchField.enter(applyFilters);

        controls.initialSearch.find('#initialSearchButton').click(applyFilters);  // change back to initialSearchButton <==========================================================================

        resetFilters();

        created = true;
    }

    function setupSearchFilter() {
        var toDate = new Date();
        var fromDate = new Date();

        var searchOptions = controls.searchOptions;
        var createdBetween =  searchOptions.find('#createdBetween');
        var defaultDateRange = Helper.getFormattedDate(fromDate) + ' - ' + Helper.getFormattedDate(toDate);
        
        createdBetween.val(defaultDateRange);
        
        var v = createdBetween.val();
        createdBetween.daterangepicker();
        

        var modifiedBetween = searchOptions.find('#modifiedBetween');
        modifiedBetween.val(defaultDateRange);
        modifiedBetween.daterangepicker();


        var expiresDropDown = new DropDown('dropDown normalizedDropdown', 'expires', null, null, null /*getExpiresDate*/).kswid('expiresDropDown');
        var defaultOption = { 'name': 'ALL', 'value': 'ALL' };
        var options = [defaultOption];
        expiresDropDown.setOptions(options, null);
        searchOptions.find('#expiresDropDownContainer').append(expiresDropDown);


        searchOptions.form();
    }

    function resetFilters() {
        if (bucketSelector) {
            bucketSelector.clear();
        }

        var bucketId;
        for (bucketId in controls.bucketElements) {
            controls.bucketElements[bucketId].remove();
        }

        controls.bucketElements = {};
        filters.buckets = [];

        if (!initialSearchState) {
            initialSearchState = true;

            contentListArea.find('#contentArea').empty();
            var value = toolbar.find('#searchInput').val();
            controls.initialSearch.find('#initialSearchInput').val(value);
            toolbar.find('#searchArea').hide();
            controls.initialSearch.find('#initialSearchInput').select();
            controls.results.hide();
        }

        home();

        controls.initialSearch.find('#initialSearchInput').select();
    }

    function getFilters() {
        if (initialSearchState) {
            filters.query = controls.initialSearch.find('#initialSearchInput').val();
        }
        else {
            filters.query = toolbar.find('#searchInput').val();
        }

        filters.titleStartsWith = '';
        filters.includeAlternateTitles = false;
        filters.sort = '';
    }

    function applyFilters() {
        getFilters();

        if (bucketSelectorElement) {
            bucketSelectorElement.remove();
            bucketSelectorElement = null;
            bucketSelector = null;
        }

        controls.pager.refresh();
    }

    function onData(data, offset) {

        if (0 < offset && offset >= data.Total) {
            controls.pager.gotoPage(data.Total / ITEMS_PER_PAGE);
        }
        else {
            if (bucketSelectorElement) {
                bucketSelectorElement.remove();
                bucketSelectorElement = null;
                bucketSelector = null;
            }

            controls.results.show();
            contentListArea.show();

            populate(data.Items);

            contentListArea.find('#resultCount').text(data.Total + ' results');

            toolbar.find('#searchInput').select();

            self.find('#pager').append(controls.pager);
            controls.pager.updatePager(data.Total);
        }
    }

    function populate(items) {
        searchItems = items;

        if (initialSearchState) {
            initialSearchState = false;
            var value = controls.initialSearch.find('#initialSearchInput').val();
            toolbar.find('#searchInput').val(value);
            toolbar.find('#searchArea').show();
            controls.initialSearch.hide();
        }

        var contentArea = contentListArea.find('#contentArea');

        contentArea.empty();

        for (var i = 0; i < items.length; i++) {
            var item = createSearchItem(items[i]);
            contentArea.append(item);
        }
    }

    function addBucketFilter(bucket) {

        var selectedFilterArea = controls.searchOptions.find('#bucketSelectedFilters');
        var newSelectedFilterDiv = template.selectedFilter.clone();

        newSelectedFilterDiv.find('.filterTitle').text(bucket.Name);
        newSelectedFilterDiv.find('.iconRemove').click(function () {
            newSelectedFilterDiv.remove();
            if (bucketSelector) {
                bucketSelector.unselect(bucket);
            }
            filters.buckets.remove(bucket.Id);
            delete controls.bucketElements[bucket.Id];
        });
        selectedFilterArea.append(newSelectedFilterDiv);
        filters.buckets.push(bucket.Id);
        controls.bucketElements[bucket.Id] = newSelectedFilterDiv;
    }

    function removeBucketFilter(bucket) {
        var item = controls.bucketElements[bucket.Id];
        if (!item)
            return;
        item.remove();
        delete controls.bucketElements[bucket.Id];
        filters.buckets.remove(bucket.Id);
    }

    function showBucketFilter() {
        if (bucketSelectorElement) {
            home();
            return;
        }

        controls.initialSearch.hide();
        contentListArea.hide();

        bucketSelector = new Content.Controls.BucketSelector(true, null, filters.buckets);
        bucketSelector.on('selectionChanged', onBucketFilterChanged);

        var closeButton = Html.button('defaultButtonSmall defaultWhiteButton', 'CLOSE').kswid('closebucketFilterArea');

        closeButton.click(home);

        var actionBar = Html.div('bottomActionBar', Html.div('right', closeButton));

        bucketSelectorElement = Html.div('itemBox dark',
			Html.div('',
				Html.div('bucketSelectorTitle', 'SELECT YOUR BUCKET'),
				bucketSelector,
				actionBar
			)
		).hide();

        controls.list.append(bucketSelectorElement);
        bucketSelectorElement.show('drop',
			{
			    direction: 'right',
			    duration: 500,
			    complete: function () {
			        bucketSelector.focus();
			    }
			});
    }

    function onBucketFilterChanged(evt, bucket, selected) {
        if (selected)
            addBucketFilter(bucket);
        else
            removeBucketFilter(bucket);
    }

    function getSearchKey() {
        if (!template)
            return null;

        var searchKey = template.collectionSearchKey.clone();
        return searchKey;
    }

    function setupToolbar() {
        if (!template)
            return;

        toolbar = template.toolbar.clone();

        if (Administration.Navigation.hasPermission('Manage_Content')) {
            var addButton = toolbar.find('#addItem');
            switch(listType) {
                case 'Collections':
                    addButton.text('ADD NEW COLLECTION');
                    break;
                case 'Buckets':
                    addButton.text('ADD NEW BUCKET');
                    break;
                case 'Content':
                    addButton.text('ADD NEW CONTENT');
                    break;
                default:
                    addButton.text('ADD NEW ITEM');
                    break;
            }
            addButton.click(function () {
                showCreateNew();
            });
        }
        else {
            toolbar.find('#addItem').remove();
        }

        toolbar.find('#searchInput').enter(applyFilters);
        toolbar.find('#searchButton').click(applyFilters);

        if (!initialSearchState) {
            toolbar.find('#searchInput').val(filters.query);
            toolbar.find('#searchArea').show();
        }

        Page.setContentTitle(toolbar);
    }

    function update(item) {
        if (!searchItems)
            return;

        for (var i = 0; i < searchItems.length; i++) {
            var current = searchItems[i];
            if (current.Id == item.Id) {
                searchItems[i] = item;
            }
        }
    }

    function createSearchItem(item) {
        var itemConstructor = null;
        
        switch (listType) {
            case 'Content':
                if (searchItemConstructors.hasOwnProperty(item.Type))
                    itemConstructor = searchItemConstructors[item.Type];
                else
                    itemConstructor = searchItemConstructors[listType];
                break;
            default:
                itemConstructor = searchItemConstructors[listType];
        }

        if (itemConstructor)
            return itemConstructor(self, item, template, readOnly, onEdit, refreshCurrentPage);

        return null;
    }

    function onEdit(item)
    {
        debugger;
        //var editor = editors[item.type];
        //editor.show(item);
    }
    
    function showCreateNew(wizard) {
        Page.setContentTitle(addTitle);

        // var wizard = new Content.Controls.BucketWizard();
        if (wizard) {
            wizard.on('cancel', function() {
                
                // need to make this generic:
                bucketSelector = null;
                bucketSelectorElement = null;
                
                Page.switchPage(self);
                home();
            });

            wizard.on('submit', function(evt, bucket) {
                // need to make this generic:
                createContent(bucket);
            });

            Page.switchPage(wizard, function() {
                wizard.focus();
            });
        }
        else {

            var editor = null;
            
            // TODO: make all constructors use same signature---use an object?
            if (listType == 'Collections')
                editor = new editors[listType](Page.getTitle(), null, self, false);
            else
                editor = new editors[listType](Page.getTitle(), null, self, onContentViewClose);
            editor.show();
        }
    }
    
    function createContent(bucket) {
        switch (bucket.Type) {
            case 'Image':
                var editor = editors['Image'](bucket);
                editor.show();

                break;
            default:
                var editor1 = editors['Content'](null, bucket, self);
                editor1.show();
                break;
        }

    }
    
    return self;
};