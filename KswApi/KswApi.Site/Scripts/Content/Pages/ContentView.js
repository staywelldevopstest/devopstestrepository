﻿var Content = Content || {};
Content.Pages = Content.Pages || {};

// remove this
Content.Controls = Content.Controls || {};

Content.Pages.ContentView = function (content, bucket, contentList, language, onClose) {
    var self = {
        showDraft: showDraft,
        showPublished: showPublished
    };

    var form,
	    languageDropdown = null,
	    _content = content,
	    _current = null,
	    _published = null,
	    isNew = (!content || language),
	    template, tab,
	    editing = false,
	    readOnly = !Helper.userHasRight('Manage_Content');

    var autosaveTracking = {
        INTERVAL_IN_SECONDS: 5,
        timer: null,
        lastModificationTime: null,
        lastValue: null,
        element: null
    };

    function showDraft() {
        getResources(function () {
            getContent(function () {
                _current = _content;
                setupTab(true);
            });
        });
    }

    function showPublished() {
        readOnly = true;

        getResources(function () {
            getPublished(function () {
                _current = _published;
                setupTab(false);
            });
        });
    }

    function getResources(callback) {
        if (!template)
            getHtml(callback);
        else
            getBucket(callback);
    }

    function getHtml(callback) {
        ResourceProvider.getHtml('Content/ContentView.html', function (html) {
            onHtml(html, callback);
        });
    }

    function getBucket(callback) {
        if (typeof bucket == 'string') {
            Callback.submit('Content/Bucket', { id: bucket }, function (data) {
                bucket = data;
                callback();
            });
        }
        else {
            callback();
        }
    }

    function getContent(callback) {
        if (typeof _content == 'string') {
            Service.get({
                service: 'Content/' + bucket.Id + '/' + _content,
                data: { draft: true, includeBody: true },
                success: function (data) {
                    _content = data;
                    callback();
                },
                failure: function (data) {
                    var warning = new Warning(data.Details);
                    warning.show(5000);
                }
            });
        }
        else {
            callback();
        }
    }

    function getPublished(callback) {
        if (_published) {
            callback();
        }
        else {
            var id = typeof _content == 'string' ? _content : _content.Id;

            Service.get({
                service: 'Content/' + bucket.Id + '/' + id,
                data: { draft: false, includeBody: true },
                success: function (data) {
                    _published = data;
                    callback();
                },
                failure: function (data) {
                    var warning = new Warning(data.Details);
                    warning.show(5000);
                }
            });
        }
    }

    function onHtml(html, callback) {
        // order is important here, the metadata elements must be removed
        // before the form
        template = {
            servicelinesValue: html.find('#servicelinesValueTemplate').remove(),
            readOnlyservicelinesValue: html.find('#readOnlyservicelinesValueTemplate').remove(),
            serviceLines: html.find('#servicelinesTemplate').remove(),
            taxonomyValue: html.find('#taxonomyValueTemplate').remove(),
            readOnlyTaxonomyValueTemplate: html.find('#readOnlyTaxonomyValueTemplate').remove(),
            taxonomy: html.find('#taxonomyTemplate').remove(),
			readOnlyCustomAttributeValue: html.find('#readOnlyCustomAttributeValueTemplate').remove(),
			readOnlyCustomAttribute: html.find('#readOnlyCustomAttributeTemplate').remove(),
			customAttributeValue: html.find('#customAttributeValueTemplate').remove(),
			customAttribute: html.find('#customAttributeTemplate').remove(),
            segment: html.find('#segmentItemView').remove(),
            segmentListItem: html.find('#segmentItem').remove(),
            segmentBarItem: html.find('#segmentBarItem').remove(),
            addAction: html.find('#addAction').remove(),
            separator: html.find('#metadataSeparator').remove(),
            alternateTitle: html.find('#alternateTitleTemplate').remove(),
            author: html.find('#authorTemplate').remove(),
            editor: html.find('#editorTemplate').remove(),
            reviewer: html.find('#reviewerTemplate').remove(),
            onlineSource: html.find('#onlineSourceTemplate').remove(),
            onlineSourceView: html.find('#onlineOriginatingSourceView').remove(),
            printSource: html.find('#printSourceTemplate').remove(),
            printSourceView: html.find('#printOriginatingSourceView').remove(),
            recommendedSite: html.find('#recommendedSiteTemplate').remove(),
            recommendedSiteView: html.find('#recommendedSiteView').remove(),
            form: html.find('#contentView').remove(),
            viewToolbar: html.find('#viewContentToolbar').remove(),
            editToolbar: html.find('#editContentToolbar').remove(),
            serviceLineView: html.find('#viewServiceLine').remove()
        };

        getBucket(callback);
    }

    function setupTab(draft) {
        if (tab)
            return;

        tab = new Tab();

        tab.addTab('Draft (editing)', showDraftTab, draft);

        if (_published || (_current && _current.Published)) {
            tab.addTab('Published (read only)', showPublishedTab, !draft);
        }

        if (content)
            Navigation.set('content', _current.Bucket.Slug, _current.Slug);

        Page.setTitle('CONTENT MANAGEMENT');

        Page.switchPage(tab);

        //collapseAllExpandersBut('ancillaryTaxonomiesExpander');
    }

    function showDraftTab() {
        getContent(function () {
            _current = _content;
            tab.setOptionsArea(createLanguageOptions());
            populateDraft();
        });
    }

    function showPublishedTab() {
        getPublished(function () {
            _current = _published;
            tab.setOptionsArea(createLanguageOptions());
            populateReadonly();
        });
    }

    function updateTab() {
        tab.setContent(form);

        form.find('[name="title"]').select();
    }

    function populateDraft() {
        _current = _content;

        if (readOnly) {
            populateReadonly();
            return;
        }

        form = template.form.clone();

        if (_current) {
            // if there's a language, we're creating a sibling
            if (_current.Master && !language)
                showEdit();
            else
                showDerived();
        } else {
            showEdit();
        }

        form.form();

        initExpanders();

        updateTab();
    }

    function populateReadonly() {
        form = template.form.clone();

        var contentTitle = template.viewToolbar.clone();

        contentTitle.find('#finished').click(close);

        Page.setContentTitle(contentTitle);

        if (languageDropdown)
            languageDropdown.hide();

        form.find('#additionalMetadata1').remove();
        form.find('#additionalMetadata2').remove();

        viewBucket();
        viewTitle();
        viewSlug();
        viewInvertedTitle();
        viewAlternateTitles();
        viewDates();
        viewLegacyId();

        viewAgeCategory();
        viewGender();
        viewBlurb();
        viewGunningFog();
        viewFleschKincaid();
        viewCopyright();
        viewAuthors();
        viewEditors();
        viewReviewers();
        viewOnlineSources();
        viewPrintSources();
        viewRecommendedSites();

        viewTaxonomies();

        viewSegments();

        populateHistory();

        form.form();

        initExpanders();

        updateTab();
    }

    function showEdit() {
        var contentTitle = template.editToolbar.clone();

        contentTitle.find('#cancel').click(close);
        contentTitle.find('#saveAsDraft').click(saveAsDraft);
        contentTitle.find('#saveAndPublish').click(saveAndPublish);

        Page.setContentTitle(contentTitle);

        languageDropdown.css('display', '');

        viewBucket();
        editTitle();
        editSlug();
        editLegacyId();

        editInvertedTitle();
        editAlternateTitles();
        editDates();
        editAgeCategory();
        editGender();
        editBlurb();
        editGunningFog();
        editFleschKincaid();
        editCopyright();
        editAuthors();
        editEditors();
        editReviewers();
        editOnlineSources();
        editPrintSources();
        editRecommendedSites();

        editSegments();

        setupSegmentSorter();

        populateHistory();

        editTaxonomies();
    }

    function showDerived() {
        var contentTitle = template.editToolbar.clone();

        form.find('#additionalMetadata1').remove();
        form.find('#additionalMetadata2').remove();

        contentTitle.find('#cancel').click(close);
        contentTitle.find('#saveAsDraft').click(saveAsDraft);
        contentTitle.find('#saveAndPublish').click(saveAndPublish);

        Page.setContentTitle(contentTitle);

        viewBucket();
        editTitle();
        editSlug();
        editInvertedTitle();
        editAlternateTitles();
        viewDates();

        viewAgeCategory();
        viewGender();
        editBlurb();
        viewGunningFog();
        viewFleschKincaid();
        viewCopyright();
        viewAuthors();
        viewEditors();
        viewReviewers();
        viewOnlineSources();
        viewPrintSources();
        viewRecommendedSites();

        editSegments();

        setupSegmentSorter();

        populateHistory();
    }

    function editTaxonomies() {
        getTaxonomyTypes(false);
        editAncillaryTaxonomies();
    }

    function viewTaxonomies() {
        form.find('#medicalTaxonomyBar').remove();
        form.find('#ancillaryTaxonomyBar').remove();
        getTaxonomyTypes(true);
        viewAncillaryTaxonomies();
    }

    function editDates() {
        form.find('#lastReviewedDate').remove();
        form.find('#postingDate').remove();

        var now = Helper.getFormattedDate(new Date());
        var lastReviewed = form.find('#lastReviewedPicker');
        var posting = form.find('#postingDatePicker');

        if (isNew) {
            lastReviewed.val(now);
            posting.val(now);
        } else {
            if (_current.LastReviewedDate)
                lastReviewed.val(Helper.dateFromDateTime(_current.LastReviewedDate));
            else
                lastReviewed.val(now);

            if (_current.PostingDate)
                posting.val(Helper.dateFromDateTime(_current.PostingDate));
            else
                posting.val(now);

            form.find('#updatedDate').text(Helper.getFormattedDate(_current.DateUpdated));
            form.find('#modifiedDate').text(Helper.getFormattedDate(_current.DateModified));
            form.find('#createdDate').text(Helper.getFormattedDate(_current.DateAdded));
        }

        lastReviewed.datepicker({ dateFormat: "m/d/yy" });
        posting.datepicker({ dateFormat: "m/d/yy" });
    }

    function viewDates() {
        form.find('#lastReviewedPicker').remove();
        form.find('#postingDatePicker').remove();

        if (_current.LastReviewedDate) {
            form.find('#lastReviewedDate').text(Helper.getFormattedDate(_current.LastReviewedDate));
        }

        if (_current.PostingDate) {
            form.find('#postingDate').text(Helper.getFormattedDate(_current.PostingDate));
        }

        if (!isNew) {
            form.find('#updatedDate').text(Helper.getFormattedDate(_current.DateUpdated));
            form.find('#modifiedDate').text(Helper.getFormattedDate(_current.DateModified));
            form.find('#createdDate').text(Helper.getFormattedDate(_current.DateAdded));
        }
    }

    function createLanguageOptions() {
        var div = Html.div('left').kswid('languageList');

        div.append('&nbsp;&nbsp;&nbsp;Language: ');

        if (language) {
            div.append(Html.label(null, Helper.getFormattedLanguageCode(language.Code)));
            return div;
        }

        if (!isNew) {
            for (var i = 0; i < _current.Versions.length; i++) {
                if (i != 0)
                    div.append(', ');
                var item = _current.Versions[i];

                if (item.Language.Code == _current.Language.Code)
                    div.append(createCurrentLanguageItem(item));
                else
                    div.append(createLanguageLink(item));
            }

            div.append('&nbsp;&nbsp;&nbsp;');
        }

        if (!readOnly) {
            languageDropdown = new DropDown('dropDown', null, null).kswid('languageDropDown');

            if (!isNew)
                languageDropdown.setLabel('Add a Language');

            div.append(languageDropdown);

            languageDropdown.change(onLanguageChange);

            getLanguages();
        }

        return div;
    }

    function getLanguages() {
        var data = {};
        if (_current) {
            data['id'] = _current.Id;
            data['bucketId'] = _current.Bucket.Id;
        }
        Callback.submit('Content/GetLanguages', data, onLanguages);
    }

    function onLanguages(data) {
        var languages = [];
        for (var i = 0; i < data.Items.length; i++) {
            var current = data.Items[i];
            languages.push({
                name: current.Name + ' (' + Helper.getFormattedLanguageCode(current.Code) + ')',
                value: current.Code,
                data: current
            });
        }

        languageDropdown.setOptions(languages);
    }

    function onLanguageChange() {
        var lang = languageDropdown.data();
        if (!lang) {
            return;
        } else if (isNew) {
            updateSlug();
        } else {
            // create a new language version
            var newEditor = new Content.Pages.ContentView(_current, bucket, contentList, lang, onClose);
            newEditor.showDraft();
        }
    }

    function createCurrentLanguageItem(item) {
        var text = Helper.getFormattedLanguageCode(item.Language.Code);
        if (item.Master)
            text = '*' + text;
        return Html.label('bold', text).kswid('currentLanguageItem');
    }

    function createLanguageLink(item) {
        var text = Helper.getFormattedLanguageCode(item.Language.Code);
        if (item.Master)
            text = '*' + text;
        return Html.anchor(null, text).kswid('languageItem').click(function () {
            var newEditor = new Content.Pages.ContentView(item.Id, bucket, contentList, null, onClose);
            newEditor.showDraft();
        });
    }

    function close() {
        if (onClose) {
            onClose();
        } else {
            contentList = new Content.Pages.ContentList();
            contentList.show();
        }
    }

    //#region metadata methods

    function viewBucket() {
        form.find('#bucket').text(bucket.Name);
    }

    function viewTitle() {
        form.find('#editTitle').remove();
        form.find('#title').text(_current.Title);
    }

    function editTitle() {
        form.find('#viewTitle').remove();
        var element = form.find('#titleValue');
        if (_current)
            element.val(_current.Title);

        if (isNew) {
            element
		        .keypress(function (e) {
		            var cursor = Helper.getSelectionStart(e.target);
		            // ignore leading whitespace
		            if (e.which == 32 && cursor == 0) {
		                e.preventDefault();
		            }
		        })
		        .keyup(function () {
		            updateSlug(); // $(this).val());
		        });
        }
    }

    function viewLegacyId() {
        form.find('#editLegacyId').remove();
        form.find('#legacyId').text(_current.Title);
    }

    function editLegacyId() {
        form.find('#viewLegacyId').remove();
        var element = form.find('#legacyIdValue');
        if (_current)
            element.val(_current.LegacyId);
    }

    function viewSlug() {
        form.find('#editSlug').remove();
        form.find('#slug').text(_current.Slug);
    }

    function editSlug() {
        form.find('#viewSlug').remove();

        var slugButton = form.find('#newContentSlug');

        if (isNew) {
            updateSlug();

            slugButton.click(function () {
                slugButton.hide();
                form.find('#slugValue').hide();
                var input = form.find('#slugInputValue');
                input.show();
                input.select();
            });
        } else {
            if (_current)
                form.find('#slugValue').text(_current.Slug);
            slugButton.hide();
        }
    }

    function updateSlug() {

        var value = Helper.getSlug(form.find('#titleValue').val());

        var selectedLanguage = null;

        if (language)
            selectedLanguage = language.Code;
        else if (languageDropdown) {
            selectedLanguage = languageDropdown.val();
        }

        if (selectedLanguage && selectedLanguage != 'en')
            value = value + '-' + selectedLanguage;

        var params = {
            slug: value,
            title: form.find('#titleValue').val()
        };

        Service.get('Content/' + bucket.Slug + '/Slugs', params, function (data) {
            form.find('#slugValue').text(data.Value);
            form.find('#slugInputValue').val(data.Value);
        });
    }

    function viewInvertedTitle() {
        form.find('#editInvertedTitle').remove();
        form.find('#invertedTitle').text(_current.InvertedTitle);
    }

    function editInvertedTitle() {
        form.find('#viewInvertedTitle').remove();
        if (_current)
            form.find('#invertedTitle').val(_current.InvertedTitle);
    }

    function viewAlternateTitles() {
        form.find('#editAlternateTitles').remove();
        var container = form.find('#alternateTitleView');
        if (!_current.AlternateTitles || _current.AlternateTitles.length == 0) {
            container.append($('<div>').text('(None)'));
        } else {
            for (var i = 0; i < _current.AlternateTitles.length; i++) {
                container.append($('<div>').text(_current.AlternateTitles[i]));
            }
        }
    }

    function editAlternateTitles() {
        form.find('#viewAlternateTitles').remove();

        var element = form.find('#editAlternateTitles');

        var metadataEditor = new Content.Controls.MetadataEditor(null, element, template.alternateTitle, setGenericMetadata, null, true);

        if (_current && _current.AlternateTitles)
            metadataEditor.show(_current.AlternateTitles);
        else
            metadataEditor.show();
    }

    function viewAgeCategory() {
        form.find('#editAgeCategory').remove();
        var container = form.find('#ageCategoryView');
        for (var i = 0; i < _current.AgeCategories.length; i++) {
            container.append($('<div>').text(_current.AgeCategories[i]));
        }
    }

    function editAgeCategory() {
        form.find('#viewAgeCategory').remove();

        var element = form.find('#editAgeCategory');
        if (_current && _current.AgeCategories) {
            for (var i = 0; i < _current.AgeCategories.length; i++) {
                element.find('[value="' + _current.AgeCategories[i] + '"]').prop('checked', true);
            }
        }
    }

    function viewGender() {
        form.find('#editGender').remove();
        form.find('#genderView').text(_current.Gender);
    }

    function editGender() {
        form.find('#viewGender').remove();

        if (_current && _current.Gender) {
            form.find('#editGender [value="' + _current.Gender + '"]').prop('checked', true);
        }
    }

    function viewBlurb() {
        form.find('#editBlurb').remove();
        if (_current.Blurb)
            form.find('#blurb').text(_current.Blurb);
    }

    function editBlurb() {
        form.find('#viewBlurb').remove();

        if (_current && _current.Blurb)
            form.find('#blurbTextArea').text(_current.Blurb);
    }

    function viewGunningFog() {
        form.find('#editGunningFog').remove();
        if (!_current.GunningFogReadingLevel) {
            form.find('#viewGunningFog').remove();
        } else {
            form.find('#gunningFogValue').text(_current.GunningFogReadingLevel);
        }
    }

    function setupAction(actionListId, element, title, visible) {
        var action = template.addAction.clone();

        action.find('#title').text(title);
        action.find('#addIcon').click(function () {
            action.hide();
            element.show();
        });

        element.find('#removeMetadata').click(function () {
            element.hide();
            element.find('input[type="checkbox"]').prop('checked', false);
            element.find('input[type="radio"]').prop('checked', false);
            element.find('#container').empty();
            action.show();
        });

        if (visible) {
            action.hide();
        } else {
            element.hide();
        }

        form.find(actionListId).append(action);
    }

    function editGunningFog() {
        form.find('#viewGunningFog').remove();

        var element = form.find('#editGunningFog');

        var visible = false;
        if (_current && _current.GunningFogReadingLevel) {
            element.find('[value="' + _current.GunningFogReadingLevel + '"]').prop('checked', true);
            visible = true;
        }

        setupAction('#addMetadata1', element, 'Gunning Fog Reading Level', visible);
    }

    function viewFleschKincaid() {
        form.find('#editFleschKincaid').remove();
        if (!_current.FleschKincaidReadingLevel) {
            form.find('#viewFleschKincaid').remove();
        } else {
            form.find('#fleschKincaidValue').text(_current.FleschKincaidReadingLevel);
        }
    }

    function editFleschKincaid() {
        form.find('#viewFleschKincaid').remove();

        var element = form.find('#editFleschKincaid');
        var visible = false;

        if (_current && _current.FleschKincaidReadingLevel) {
            element.find('input[value="' + _current.FleschKincaidReadingLevel + '"]').prop('checked', true);
            visible = true;
        }

        setupAction('#addMetadata1', element, 'Flesch-Kincaid Reading Level', visible);
    }

    function viewCopyright() {
        form.find('#editCopyright').remove();
        form.find('#copyrightValue').html(_current.Copyright);
    }

    function editCopyright() {
        form.find('#viewCopyright').remove();

        var copyrightSelector = new Content.Controls.CopyrightSelector('copyrightId');
        form.find('#editCopyright').prepend(copyrightSelector);

        if (_current && _current.CopyrightId) {
            copyrightSelector.val(_current.CopyrightId);
        } else if (bucket.CopyrightId) {
            copyrightSelector.val(bucket.CopyrightId);
        } else {
            copyrightSelector.val(null);
        }
    }

    function viewAuthors() {
        form.find('#editAuthors').remove();
        var element = form.find('#viewAuthors');
        if (!_current.Authors || _current.Authors.length == 0) {
            element.remove();
        } else {
            var container = element.find('#container');
            for (var i = 0; i < _current.Authors.length; i++) {
                container.append($('<div>').text(_current.Authors[i]));
            }
        }
    }

    function setGenericMetadata(offset, element, value) {
        element.find('#value').val(value);
    }

    function isReadOnlyTaxonomyData(element, data) {
        if (data && data.Source !== 'Ksw')
            return true;
        return false;
    }

    function setGenericMetadata2(offset, element, data) {
        //if (isReadOnlyTaxonomyData(element, data)) {
        //    var readonlyValueElement = template.readOnlyTaxonomyValueTemplate.clone();
        //    element.replaceWith(readonlyValueElement);
        //    element = readonlyValueElement;
        //}


        // Could extend this to parse complex objects
        for (var key in data) {
            var metaData = element.find('[data-name="' + Helper.lowerCaseFirstLetter(key) + '"]');
            if (metaData) {
                metaData.val(data[key]);
            }
        }

        // Do this for each item with input type != hidden...
        if (isReadOnlyTaxonomyData(element, data)) {
            var inputElement = element.find('#metaDataValue');
            var labelElement = element.find('#metaDataValueLabel');
            var displayValue = '(External) ' + inputElement.val();
            labelElement.text(displayValue);
            inputElement.hide();
            //labelElement.show();

            // do this in addItem...
            element.find('#removeMetaDataItem').remove();
        }
        else {
            labelElement = element.find('#metaDataValueLabel');
            labelElement.hide();
        }
    }

    function editAuthors() {
        form.find('#viewAuthors').remove();

        var element = form.find('#editAuthors');

        var metadataEditor = new Content.Controls.MetadataEditor('Authors', element, template.author, setGenericMetadata);

        if (_current && _current.Authors)
            metadataEditor.show(_current.Authors);
        else if (isNew)
            metadataEditor.show();
        else
            metadataEditor.hide();

        form.find('#addMetadata2').append(metadataEditor.getActuator());
    }

    function getServicelinesDropDown(dropdown, path, callback, slug, triggerChange) {
        var data = {};
        data.path = path;
        data.target = dropdown;
        data.slug = slug;
        data.triggerChange = triggerChange;

        Callback.submit('Content/SearchServiceLines', data, callback);
    }

    function onGetServiceLinesDropDown(data, params) {
        var options = [];
        for (var i = 0; i < data.Items.length; i++) {
            var current = data.Items[i];
            options.push({
                name: current.Value, //.Name,
                value: current.Slug, //current.Code,
                data: current
            });
        }

        var target = params.target;
        options.sort(function (a, b) {
            var x = a.name.toLowerCase();
            var y = b.name.toLowerCase();
            return x < y ? -1 : x > y ? 1 : 0;
        });
        target.setOptions(options, params.triggerChange);

        if (params.slug) {
            target.select(params.slug, params.triggerChange);
        }
    }

    function onPageKeywordChange(event) {
        var pageKeyword = event.data.pageKeyword.data();
        var serviceline = event.data.serviceline.data();
        var control = event.data.control;

        var path = control.find('[data-name="path"]');
        var slug = control.find('[data-name="slug"]');

        if (pageKeyword != null) {
            path.val(pageKeyword.Path);
            slug.val(pageKeyword.Slug);

        }
        else if (serviceline != null) {
            var pathValue = serviceline.Path + '/' + serviceline.Slug;
            path.val(pathValue);
        }
        else {
            path.val(null);
            slug.val(null);
        }
    }

    function onServicelineChange(event) {

        var serviceline = event.data.serviceline;
        var pageKeyword = event.data.pageKeyword;
        var data = serviceline.data();

        pageKeyword.setLabel('Optional Page Keyword...');

        if (!data) {
            pageKeyword.setOptions(null);
        } else {
            var path = data.Path + '/' + data.Slug;
            getServicelinesDropDown(pageKeyword, path, onGetServiceLinesDropDown);
        }
    }

    function onAudienceChange(event) {

        var audience = event.data.audience;
        var serviceline = event.data.serviceline;
        var data = audience.data();

        if (!data) {
            serviceline.setOptions(null);
        } else {
            var path = data.Slug;
            getServicelinesDropDown(serviceline, path, onGetServiceLinesDropDown);
        }
    }

    function createServicelineTemplate(data) {
        var control = template.servicelinesValue.clone();

        if (!readOnly) {
            var audienceDropDown = new DropDown('dropDown dropDownLarge  bottomMarginSmall', null, null).kswid('audienceDropDown'); // why new?
            var servicelineDropDown = new DropDown('dropDown dropDownLarge bottomMarginSmall', null, null).kswid('servicelineDropDown');
            var pageKeywordDropDown = new DropDown('dropDown dropDownLarge bottomMarginSmall', null, null).kswid('pageKeywordDropDown');

            audienceDropDown.setLabel('Select Audience');
            servicelineDropDown.setLabel('Select Serviceline');
            pageKeywordDropDown.setLabel('Optional Page Keyword...');


            control.find('#audience').append(audienceDropDown);
            control.find('#serviceline').append(servicelineDropDown);
            control.find('#pageKeyword').append(pageKeywordDropDown);

            var controls = { audience: audienceDropDown, serviceline: servicelineDropDown, pageKeyword: pageKeywordDropDown, control: control };

            // set up events
            audienceDropDown.change(controls, onAudienceChange);
            servicelineDropDown.change(controls, onServicelineChange);
            pageKeywordDropDown.change(controls, onPageKeywordChange);

            if (data) {
                // decompose the path
                var path = data.Path;
                var split = data.Path.split('/');
                var audience = split[0];
                var serviceline = split[1];

                getServicelinesDropDown(audienceDropDown, '', onGetServiceLinesDropDown, audience, false);
                getServicelinesDropDown(servicelineDropDown, audience, onGetServiceLinesDropDown, serviceline, false);
                getServicelinesDropDown(pageKeywordDropDown, path, onGetServiceLinesDropDown, data.Slug, false);

                // set the hidden values
                // should use onPageKeywordChange or extract that code to another function...
                var pathInput = control.find('[data-name="path"]');
                var slugInput = control.find('[data-name="slug"]');
                pathInput.val(path);
                slugInput.val(data.Slug);
            }
            else {
                getServicelinesDropDown(audienceDropDown, '', onGetServiceLinesDropDown);
            }


        }

        return control;
    }

    function editServiceLines() {
        var taxonomiesContainer = form.find('#mainAncillaryTaxonomiesArea');
        var actuator = Content.Controls.Actuator(taxonomiesContainer, 'Service Line', template.serviceLines, createServicelineTemplate, null, null, null, false, true);
        form.find('#addAncillaryTaxonomies').append(actuator.getActuatorControl());

        // servicelines content
        if (_current && _current.Servicelines) {
            for (var i = 0; i < _current.Servicelines.length; i++) {
                var serviceline = _current.Servicelines[i];
                actuator.showEditor([serviceline]);
            }
        }
    }

    function viewServiceLines() {
        if (!_current || !_current.Servicelines)
            return;

        var taxonomiesContainer = form.find('#mainAncillaryTaxonomiesArea');

        for (var i = 0; i < _current.Servicelines.length; i++) {
            var item = _current.Servicelines[i];
            var element = template.serviceLineView.clone();
            var list = element.find('#container');
            for (var j = 0; j < item.PathValues.length; j++) {
                list.append(Html.div('', item.PathValues[j]));
            }
            taxonomiesContainer.append(element);
        }


    }

    function populateTags() {
        var taxonomiesContainer = form.find('#mainAncillaryTaxonomiesArea');
        var actuator = Content.Controls.Actuator(taxonomiesContainer, 'Tags', null, null, null, null, null, false, true);
        form.find('#addAncillaryTaxonomies').append(actuator.getActuatorControl());
    }

	function viewCustomAttributes() {
		var taxonomiesContainer = form.find('#mainAncillaryTaxonomiesArea');

		if (_current && _current.CustomAttributes) {
			for (var i = 0; i < _current.CustomAttributes.length; i++) {
				var value = _current.CustomAttributes[i];
				var element = template.readOnlyCustomAttribute.clone();
				element.find('#metaDataTitle').text(value.Name);
				var list = element.find('#metaDataContainer');
				for (var j = 0; j < value.CustomAttributeValues.length; j++) {
					var element2 = template.readOnlyCustomAttributeValue.clone();
					element2.find('#metaDataValue').text(value.CustomAttributeValues[j].Values);
					list.append(element2);
				}

				taxonomiesContainer.append(element);
			}
		}
	}

	function editCustomAttributes() {
		var taxonomiesContainer = form.find('#mainAncillaryTaxonomiesArea');
		var actuator = Content.Controls.Actuator(taxonomiesContainer, 'Custom Attribute', template.customAttribute,
			createCustomAttribute, setCustomAttribute, null, null, false, true);
		// actuator
		form.find('#addAncillaryTaxonomies').append(actuator.getActuatorControl());

		// custom attribute content
		if (_current && _current.CustomAttributes) {
			for (var i = 0; i < _current.CustomAttributes.length; i++) {
				actuator.showEditor([_current.CustomAttributes[i]]);
			}
		}
	}

	function createCustomAttribute(value) {
		if (value) {
			var controls = [];
			for (var i = 0; i < value.CustomAttributeValues.length; i++)
				controls[i] = (readOnly ? template.readOnlyCustomAttributeValue : template.customAttributeValue).clone();
			return controls;
		} else {
			return (readOnly ? template.readOnlyCustomAttributeValue : template.customAttributeValue).clone();
		}
	}

	function setCustomAttribute(itemElement, value, index, editorBody) {
		editorBody.form();
		if (!value) return;
		editorBody.find('#metaDataTitle').val(value.Name);
		itemElement.find('#metaDataValue').val(value.CustomAttributeValues[index].Values);
	}

    function viewAncillaryTaxonomies() {
        viewServiceLines();
		viewCustomAttributes();
    }

    function editAncillaryTaxonomies() {
        editServiceLines();
        populateTags();
		editCustomAttributes();
    }

    function editTaxonomy(taxonomyType) {
        var taxonomiesContainer = form.find('#mainTaxonomiesArea');
        var taxonomy = template.taxonomy.clone();

        // Set slug
        var slug = taxonomy.find("[data-name='slug']");
        slug.attr('value', taxonomyType.Slug);

        // Set template title
        taxonomy.find('#metaDataTitle').text(taxonomyType.Name);

        var metadataEditor = new Content.Controls.MetadataEditor2(taxonomyType.Name, taxonomy, template.taxonomyValue, setGenericMetadata2, null, null, taxonomiesContainer, true);

        if (_current && _current.Taxonomies) {
            var matches = $.grep(_current.Taxonomies, function (e) { return e.Slug === taxonomyType.Slug; });
            if (matches) {
                // should check for matches.length > 1...
                var match = matches[0];
            }
        }
        if (match)
            metadataEditor.show(match.Items);
        else
            metadataEditor.hide();

        // actuator
        form.find('#addMedicalTaxonomies').append(metadataEditor.getActuator());
    }

    function viewTaxonomy(taxonomyType) {
        if (!_current || !_current.Taxonomies)
            return;

        var match = _current.Taxonomies.firstOrNull(function (e) { return e.Slug === taxonomyType.Slug; });

        if (!match || !match.Items || match.Items.length == 0)
            return;

        form.find('#taxonomyValueTemplate').remove();

        var taxonomiesContainer = form.find('#mainTaxonomiesArea');
        var taxonomy = template.taxonomy.clone();

        // could just use a readOnlyTaxonomy template instead...
        taxonomy.find('#addMetaDataItem').remove();
        taxonomy.find('#removeMetadata').remove();

        // Set template title
        taxonomy.find('#metaDataTitle').text(taxonomyType.Name);

        // Add taxonomy values
        var valueContainer = taxonomy.find('#metaDataContainer'); // Could generalize for consumption by metadataeditor2


        if (match) {
            // add all taxonomy values
            for (var i = 0; i < match.Items.length; i++) {
                var newValue = template.readOnlyTaxonomyValueTemplate.clone();
                newValue.find('#metaDataValue').text(match.Items[i].Value);                                        // use metaDataValue elsewhere, too
                valueContainer.append(newValue);
            }
        }

        // append to container
        taxonomiesContainer.append(taxonomy);
    }

    function getTaxonomyTypes(readonly) {
        form.find('#viewTaxonomyTemplate').remove();

        Callback.submit('Content/GetTaxonomyTypes', null, function (data) {
            onTaxonomyTypes(data, readonly);
        });
    }

    function onTaxonomyTypes(data, readonly) {
        data.Items.sort(function (a, b) {
            var x = a.Name.toLowerCase();
            var y = b.Name.toLowerCase();
            return x < y ? -1 : x > y ? 1 : 0;
        });
        for (var i = 0; i < data.Items.length; i++) {
            if (readonly)
                viewTaxonomy(data.Items[i]);
            else
                editTaxonomy(data.Items[i]);
        }
    }

    function viewEditors() {
        form.find('#editEditors').remove();
        var element = form.find('#viewEditors');
        if (!_current.OnlineEditors || _current.OnlineEditors.length == 0) {
            element.remove();
        } else {
            var container = element.find('#container');
            for (var i = 0; i < _current.OnlineEditors.length; i++) {
                container.append($('<div>').text(_current.OnlineEditors[i]));
            }
        }
    }

    function editEditors() {
        form.find('#viewEditors').remove();

        var element = form.find('#editEditors');

        var metadataEditor = new Content.Controls.MetadataEditor('Online Editors', element, template.editor, setGenericMetadata);

        if (_current && _current.OnlineEditors)
            metadataEditor.show(_current.OnlineEditors);
        else
            metadataEditor.hide();

        form.find('#addMetadata2').append(metadataEditor.getActuator());
    }

    function viewReviewers() {
        form.find('#editReviewers').remove();
        var element = form.find('#viewReviewers');
        if (!_current.OnlineMedicalReviewers || _current.OnlineMedicalReviewers.length == 0) {
            element.remove();
        } else {
            var container = element.find('#container');
            for (var i = 0; i < _current.OnlineMedicalReviewers.length; i++) {
                container.append($('<div>').text(_current.OnlineMedicalReviewers[i]));
            }
        }
    }

    function editReviewers() {
        form.find('#viewReviewers').remove();

        var element = form.find('#editReviewers');

        var metadataEditor = new Content.Controls.MetadataEditor('Online Medical Reviewers', element, template.reviewer, setGenericMetadata);

        if (_current && _current.OnlineMedicalReviewers)
            metadataEditor.show(_current.OnlineMedicalReviewers);
        else
            metadataEditor.hide();

        form.find('#addMetadata2').append(metadataEditor.getActuator());

    }

    function viewOnlineSources() {
        form.find('#editOnlineSources').remove();
        var element = form.find('#viewOnlineSources');
        if (!_current.OnlineOriginatingSources || _current.OnlineOriginatingSources.length == 0) {
            element.remove();
        } else {
            var container = element.find('#container');

            for (var i = 0; i < _current.OnlineOriginatingSources.length; i++) {
                if (i != 0)
                    container.append($('<div class="borderSeperator clear someSpace">'));

                var current = _current.OnlineOriginatingSources[i];
                var div = template.onlineSourceView.clone();
                div.find('#source').text(current.Source);
                div.find('#uri').text(current.Uri);
                div.find('#title').text(current.Title);
                div.find('#date').text(Helper.getFormattedDate(current.Date));
                container.append(div);
            }
        }
    }

    function editOnlineSources() {
        form.find('#viewOnlineSources').remove();

        var element = form.find('#editOnlineSources');

        var metadataEditor = new Content.Controls.MetadataEditor('Online Originating Sources', element, template.onlineSource, setOnlineSource, template.separator);

        if (_current && _current.OnlineOriginatingSources)
            metadataEditor.show(_current.OnlineOriginatingSources);
        else if (isNew)
            metadataEditor.show();
        else
            metadataEditor.hide();

        form.find('#addMetadata2').append(metadataEditor.getActuator());
    }

    function setOnlineSource(offset, element, value) {

        var date = element.find('.smallDatePickerBox');

        if (value) {
            element.find('#sourceTitle').val(value.Title);
            element.find('#source').val(value.Source);
            element.find('#uri').val(value.Uri);
            date.val(Helper.getFormattedDate(value.Date));
        }
        else {
            date.val(Helper.getFormattedDate(new Date()));
        }

        date.datepicker({ dateFormat: "m/d/yy" });
    }

    function viewPrintSources() {
        form.find('#editPrintSources').remove();
        var element = form.find('#viewPrintSources');
        if (!_current.PrintOriginatingSources || _current.PrintOriginatingSources.length == 0) {
            element.remove();
        } else {
            var container = element.find('#container');

            for (var i = 0; i < _current.PrintOriginatingSources.length; i++) {
                if (i != 0)
                    container.append($('<div class="borderSeperator clear someSpace">'));

                var current = _current.PrintOriginatingSources[i];
                var div = template.printSourceView.clone();
                div.find('#source').text(current.Source);
                div.find('#title').text(current.Title);
                div.find('#date').text(Helper.getFormattedDate(current.Date));
                container.append(div);
            }
        }
    }

    function editPrintSources() {
        form.find('#viewPrintSources').remove();

        var element = form.find('#editPrintSources');

        var metadataEditor = new Content.Controls.MetadataEditor('Print Originating Sources', element, template.printSource, setPrintSource, template.separator);

        if (_current && _current.PrintOriginatingSources)
            metadataEditor.show(_current.PrintOriginatingSources);
        else
            metadataEditor.hide();

        form.find('#addMetadata2').append(metadataEditor.getActuator());
    }

    function setPrintSource(offset, element, value) {
        var date = element.find('.smallDatePickerBox');

        if (value) {
            element.find('#title').val(value.Title);
            date.val(Helper.getFormattedDate(value.Date));
        }
        else {
            date.val(Helper.getFormattedDate(new Date()));
        }

        date.datepicker({ dateFormat: "m/d/yy" });
    }

    function viewRecommendedSites() {
        form.find('#editRecommendedSites').remove();
        var element = form.find('#viewRecommendedSites');

        if (!_current.RecommendedSites || _current.RecommendedSites.length == 0) {
            form.find('#viewRecommendedSites').remove();
        } else {
            var container = element.find('#container');

            for (var i = 0; i < _current.RecommendedSites.length; i++) {
                if (i != 0)
                    container.append($('<div class="borderSeperator clear someSpace">'));

                var current = _current.RecommendedSites[i];
                var div = template.recommendedSiteView.clone();
                div.find('#title').text(current.Title);
                div.find('#uri').text(current.Uri);
                container.append(div);
            }
        }
    }

    function editRecommendedSites() {
        form.find('#viewRecommendedSites').remove();

        var element = form.find('#editRecommendedSites');

        var metadataEditor = new Content.Controls.MetadataEditor('Recommended Sites', element, template.recommendedSite, setRecommendedSite, template.separator);

        if (_current && _current.RecommendedSites)
            metadataEditor.show(_current.RecommendedSites);
        else
            metadataEditor.hide();

        form.find('#addMetadata2').append(metadataEditor.getActuator());
    }

    function setRecommendedSite(offset, element, value) {
        var date = element.find('#dateAccessed');

        if (value) {
            element.find('#title').val(value.Title);
            element.find('#uri').val(value.Uri);
            date.val(value.Date);
        }
    }

    //#endregion

    function getNonFormData() {
        var data = getAgeCategories();

        if (isNew && !language)
            data['LanguageCode'] = languageDropdown.val();

        data = $.extend(data, getSegmentData());

        return data;
    }

    function saveAsDraft(evt) {
        save(evt, false);
    }

    function saveAndPublish(evt) {
        save(evt, true);
    }

    function save(evt, publish) {
        if (evt)
            evt.preventDefault();

        stopAutosaveTimer();

        var data = getNonFormData();

        data['publish'] = publish;

        if (bucket)
            data['bucketId'] = bucket.Id;

        if (!isNew) {
            data['id'] = _current.Id;

            Callback.submit({
                service: 'Content/UpdateContent',
                params: data,
                form: form,
                success: onSave
            });
        } else {
            if (language)
                data['LanguageCode'] = language.Code;

            if (_current)
                data['MasterId'] = _current.MasterId;

            Callback.submit({
                service: 'Content/CreateContent',
                params: data,
                form: form,
                success: onSave
            });
        }
    }

    function getSegmentData() {
        var segments = {};
        var i = 0;

        if (Content.Pages.ContentView.tinyMceInitialized)
            tinymce.triggerSave(true, true);

        form.find('#segmentList').children().each(function () {
            var element = $(this);
            var prefix = 'segments.' + i + '.';

            segments[prefix + 'idOrSlug'] = element.find('#segmentId').val();
            segments[prefix + 'customName'] = element.find('#customSegmentTitle').text();

            var bodyElement = element.find('textarea');
            if (bodyElement.length > 0) {
                segments[prefix + 'body'] = bodyElement.val();
            }
            else {
                segments[prefix + 'body'] = element.data('value');
            }

            i++;
        });

        return segments;
    }

    function getAgeCategories() {
        var ages = {};
        var i = 0;

        form.find('#editAgeCategory input[type="checkbox"]:checked').each(function () {
            var prefix = 'ageCategories.' + i;

            ages[prefix] = $(this).val();

            i++;
        });

        return ages;
    }


    function onSave(data) {
        _current = data;

        close(true);
    }

    function createSegmentView(segment) {
        var element = template.segment.clone();
        var name = segment.Name;
        if (segment.Required)
            name = '*' + name;
        element.find('#name').text(name);
        if (typeof segment.CustomName == 'string')
            element.find('#customName').text(segment.CustomName);
        else
            element.find('#customName').text(segment.Name);
        element.find('#body').html(segment.Body);
        return element;
    }

    function createSegmentBarItem(segment) {
        var name = segment.Name;
        var element = $('<div>')
			.addClass('floatLeft addedSegment clear');
        if (segment.Required) {
            name = '*' + name;
            element.addClass('bold');
        }
        element.text(name);
        element.attr('title', segment.Name);
        return element;
    }

    function createSegmentBarBucketItem(segment) {
        var name = segment.Name;
        var element = $('<div>')
			.addClass('floatLeft clear');
        if (segment.Required) {
            name = '*' + name;
            element.addClass('bold');
        }
        element.text(name);
        element.attr('title', segment.Name);
        return element;
    }

    function viewSegments() {
        form.find('#dragToReorderPrompt').remove();

        var container = form.find('#segmentList');
        var bar = form.find('#addedSegmentList');
        var bucketSegments = bucket.Segments ? bucket.Segments.slice() : [];

        bar.empty();
        form.find('#availableSegmentList').empty();

        if (_current.Segments) {
            for (var i = 0; i < _current.Segments.length; i++) {
                var current = _current.Segments[i];
                var bucketSegment = bucketSegments.removeFirst(function (item) { return item.Id == current.Id; });

                if (bucketSegment) {
                    current.Required = bucketSegment.Required;
                }

                container.append(createSegmentView(current));
                bar.append($('<div class="clear small spacer">'));
                bar.append(createSegmentBarItem(current));
            }
        }
    }

    function initExpander(expander, expansion) {
        var expanderElement = form.find(expander);
        var expansionElement = form.find(expansion);
        expanderElement.click(function () {
            if (expansionElement.is(':visible')) {
                expansionElement.hide();
                expanderElement.removeClass('iconExpandedBig');
                expanderElement.addClass('iconCollapsedBig');
            } else {
                expansionElement.show();
                expanderElement.removeClass('iconCollapsedBig');
                expanderElement.addClass('iconExpandedBig');
            }
        });
    }

    function collapseAllExpandersBut(expanderId) {
        var expanders = form.find('[id$=Expander]:not([id=' + expanderId + '])');
        expanders.click();
    }

    function initExpanders() {
        initExpander('#metadataExpander', '#metadataExpansion');
        initExpander('#additionalMetadataExpander', '#additionalMetadataExpansion');
        initExpander('#segmentExpander', '#segmentExpansion');
        initExpander('#historyExpander', '#historyExpansion');
        initExpander('#medicalTaxonomiesExpander', '#medicalTaxonomiesExpansion');
        initExpander('#ancillaryTaxonomiesExpander', '#ancillaryTaxonomiesExpansion');
    }

    function editSegments() {
        var segmentViewList = form.find('#segmentList');
        var availableSegmentList = form.find('#availableSegmentList');
        var addedSegmentList = form.find('#addedSegmentList');
        addedSegmentList.empty();
        availableSegmentList.empty();
        segmentViewList.empty();
        var i, segment;

        var bucketSegments = [];

        if (bucket.Segments) {
            for (i = 0; i < bucket.Segments.length; i++) {
                bucketSegments.push(bucket.Segments[i]);
            }
        }

        if (_current && _current.Segments) {
            for (i = 0; i < _current.Segments.length; i++) {
                var value = _current.Segments[i];

                segment = bucketSegments.removeFirst(function (item) {
                    return item.Id == value.Id;
                });

                if (!segment)
                    continue;

                var added = createSegmentBarElement(segment, value);

                addedSegmentList.append(added);

                var element = createSegmentElement(segment, value);

                added.data('edit', element);
                element.data('side', added);

                segmentViewList.append(element);
            }
        }

        for (i = 0; i < bucketSegments.length; i++) {
            segment = bucketSegments[i];
            if (segment.Required) {
                var available = createSegmentBarElement(segment, null);

                addedSegmentList.append(available);

                element = createSegmentElement(segment, null);

                available.data('edit', element);
                element.data('side', available);

                segmentViewList.append(element);
            }
            else {
                availableSegmentList.append(createSegmentBarElement(segment, null));
            }
        }
    }

    function createSegmentBarElement(segment, value) {

        var element = template.segmentBarItem.clone();

        if (segment) {
            var name = segment.Name;
            if (segment.Required) {
                name = '*' + name;

            } else {
                element.removeClass('addedSegment');
                element.removeClass('requiredSegment');

                element.find('#availableSegmentRequiredIndicator').hide();
            }

            element.find('#name').text(name).attr('title', segment.Name);
        }
        else {
            if (value)
                element.find('#name').text(value.Name).attr('title', value.Name);

            element.removeClass('addedSegment');
            element.removeClass('requiredSegment');
        }

        element.find('#add').click(function () { addSegment(element, segment); });

        if (value || (segment && segment.Required)) {
            element.find('#move').css('visibility', 'visible');
            element.find('#add').hide();
            element.addClass('sortable');
        }
        else {
            element.find('#move').css('visibility', 'hidden');
        }

        return element;
    }

    function addSegment(sideItem, segment) {
        sideItem.detach();
        sideItem.find('#move').css('visibility', 'visible');
        sideItem.find('#add').hide();
        sideItem.addClass('sortable addedSegment');

        var segmentView = createSegmentElement(segment, null);

        if (editing)
            disableSegment(segmentView);

        sideItem.data('edit', segmentView);
        segmentView.data('side', sideItem);

        form.find('#addedSegmentList').append(sideItem);
        form.find('#segmentList').append(segmentView);
    }

    function setupSegmentSorter() {

        var addedList = form.find('#addedSegmentList');
        var segmentList = form.find('#segmentList');

        addedList.sortable({

            "update": function (event, ui) {
                var element = ui.item.data('edit');
                if (!element)
                    return;

                var textArea = element.find('textarea');
                var id = textArea.attr('id');

                if (id) {
                    tinymce.execCommand('mceRemoveEditor', false, id);
                }

                element.detach();
                var index = ui.item.index();
                if (index == 0) {
                    segmentList.prepend(element);
                }
                else {
                    var children = segmentList.children();
                    if (index >= children.length) {
                        segmentList.append(element);
                    }
                    else {
                        children.eq(index).before(element);
                    }
                }

                if (id) {
                    tinymce.settings.directionality = getDirection();
                    tinymce.execCommand('mceAddEditor', false, id);
                    var ed = tinymce.get(id);
                    ed.dir = 'rtl';
                }
            }
        });
    }

    function createSegmentElement(segment, value) {
        var element = template.segmentListItem.clone();

        element.find('#segmentId').val(segment.Id);

        var name = segment.Name;
        if (segment.Required) {
            name = '*' + name;
            var removeButton = element.find('#removeSegment');

            if (removeButton) {
                removeButton.remove();
            }
        }
        else {
            element.find('#removeSegment').click(function () {
                removeSegment(element);
            });
        }

        element.find('#segmentTitle').text(name);

        element.find('#editSegmentAction').click(function () { editSegment(element); });

        var bodyDiv = element.find('#segmentBody');

        var customName = element.find('#customSegmentTitle');

        if (value) {
            bodyDiv.html(value.Body);

            // prevent image caching for ie
            bodyDiv.find('img').each(function () {
                var img = $(this);
                var src = img.attr('src');
                if (!src)
                    return;
                if (src.indexOf('?') >= 0)
                    return;
                src = src + '?_=' + $.now();
                img.attr('src', src);
            });

            element.data('value', value.Body);
        }
        else {
            element.data('value', '');
        }

        if (value && typeof value.CustomName == 'string') {
            customName.text(value.CustomName);
        }
        else {
            customName.text(segment.Name);
        }

        return element;
    }

    function removeSegment(element) {
        var sideItem = element.data('side');

        if (!sideItem) {
            return;
        }

        element.remove();

        sideItem.detach();

        sideItem.find('#move').css('visibility', 'hidden');
        sideItem.find('#add').show();
        sideItem.removeClass('addedSegment');
        form.find('#availableSegmentList').prepend(sideItem);
    }

    function disableSegment(segment) {
        segment.find('#segmentBody').addClass('disabled');
        segment.find('#segmentTitle').addClass('disabled');
        segment.find('#customSegmentTitle').addClass('disabled');
        segment.find('#editSegmentAction').hide();
    }

    function enableSegment(segment) {
        segment.find('#segmentBody').removeClass('disabled');
        segment.find('#segmentTitle').removeClass('disabled');
        segment.find('#customSegmentTitle').removeClass('disabled');
        segment.find('#editSegmentAction').show();
    }

    function enableAllSegments() {
        var segments = form.find('#segmentList').children();
        segments.each(function () {
            enableSegment($(this));
        });
    }

    function onEndSegmentEdit(segment, update) {
        editing = false;
        var textArea = segment.find('textarea');

        stopAutosaveTimer();

        var newValue = null;

        if (update) {
            tinymce.triggerSave(true, true);

            newValue = textArea.val();

            segment.data('value', newValue);

            autosave();
        }

        autosaveTracking.element = null;

        var id = textArea.attr('id');
        if (id) {
            tinymce.execCommand('mceRemoveEditor', false, id);
        }

        textArea.remove();

        var segmentBody = segment.find('#segmentBody');

        if (update) {
            segmentBody.empty();

            segmentBody.show();

            segmentBody.html(newValue);
        }
        else {
            segmentBody.show();
        }

        segment.find('#segmentDataArea').show();
        segment.find('#removeSegment').show();

        segment.find('#undoOrUpdateToolbar').hide();

        var customSegmentItemTitle = segment.find('#customSegmentTitle');
        var customSegmentItemTitleTextBox = segment.find('#customTitleTextbox');

        customSegmentItemTitle.text(customSegmentItemTitleTextBox.val());
        customSegmentItemTitle.show();

        customSegmentItemTitleTextBox.remove();

        enableAllSegments();
    }

    function editSegment(segmentElement) {
        editing = true;
        var textArea = Html.textArea('segmentEditTextArea', null, '').kswid('segEditor');
        var previousValue = segmentElement.data('value');

        var customSegmentItemTitle = segmentElement.find('#customSegmentTitle');
        var customSegmentItemTitleTextBox = Html.textBox('customSegmentItemTitleTextBox defaultTextBox').attr('id', 'customTitleTextbox').val(customSegmentItemTitle.text()).kswid('additionalSegTextbox');

        segmentElement.find('#editSegmentAction').hide();
        segmentElement.find('#removeSegment').hide();

        var otherSegments = form.find('#segmentList').children().not(segmentElement);
        otherSegments.each(function () {
            disableSegment($(this));
        });

        textArea.val(previousValue);

        segmentElement.append(textArea);

        segmentElement.find('#undoOrUpdateToolbar').show();

        segmentElement.find('#undoSegmentChange').unbind();
        segmentElement.find('#undoSegmentChange').click(function () {
            onEndSegmentEdit(segmentElement, false);
        });

        segmentElement.find('#updateSegment').unbind();
        segmentElement.find('#updateSegment').click(function () {
            onEndSegmentEdit(segmentElement, true);
        });

        segmentElement.find('#segmentDataArea').hide();

        customSegmentItemTitle.hide();
        segmentElement.find('.segmentTitleArea').append(customSegmentItemTitleTextBox);

        if (bucket.Type == "Article") {
            textArea.focus();
            initEditor(textArea);
        }
        else {
            textArea.unbind('keyup');
            textArea.bind('keyup', onSegmentModification);
            textArea.focus();
        }

        autosaveTracking.element = textArea;
        autosaveTracking.lastValue = previousValue;
    }

    function getDirection() {
        var current = null;
        if (isNew) {
            if (language)
                current = language;
            else if (languageDropdown)
                current = languageDropdown.data();
        }
        else {
            if (_current)
                current = _current.Language;
        }

        if (current && current.Direction == 'RightToLeft')
            return 'rtl';

        return 'ltr';
    }

    function initEditor(textArea) {

        var id = tinymce.DOM.uniqueId();

        textArea.attr('id', id);

        initializeTinyMce(id);

        tinymce.settings.directionality = getDirection();
        tinymce.execCommand('mceAddEditor', false, id);
    }

    function initializeTinyMce(id) {
        tinymce.baseURL = Global.ApplicationPath + 'Scripts/tinymce';
        tinymce.init({
            mode: 'none',
            relative_urls: false,
            remove_script_host: false,
            auto_focus: id,
            theme: 'modern',
            toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | ' +
				'bullist numlist outdent indent | link insertImage | ltr rtl',
            menu: {
                file: { title: 'File', items: 'print' },
                edit: { title: 'Edit', items: 'undo redo | cut copy paste pastetext | selectall | searchreplace' },
                insert: { title: 'Insert', items: 'insertImage link | charmap anchor insertdatetime' },
                view: { title: 'View', items: 'visualblocks visualaid | preview fullscreen' },
                format: { title: 'Format', items: 'bold italic underline strikethrough superscript subscript | formats | removeformat' },
                table: { title: 'Table', items: 'inserttable tableprops deletetable | cell row column' },
                tools: { title: 'Tools', items: 'code' }
            },
            contextmenu: "link insertImage inserttable | cell row column deletetable",
            plugins: [
				"advlist autolink lists link charmap print image preview anchor directionality",
				"searchreplace visualblocks code fullscreen",
				"insertdatetime media table contextmenu paste moxiemanager"
            ],
            setup: function (ed) {
                ed.addButton('insertImage', {
                    icon: 'image',
                    tooltip: 'Insert image',
                    stateSelector: 'img:not([data-mce-object])',
                    onclick: function () {
                        onAddImage(ed);
                    }
                });

                ed.addMenuItem('insertImage', {
                    icon: 'image',
                    text: 'Insert image',
                    onclick: function () { onAddImage(ed); },
                    context: 'insert',
                    prependToContext: true
                });

                ed.on('keyup', onSegmentModification);
            }
        });

        Content.Pages.ContentView.tinyMceInitialized = true;
    }

    function onAddImage(ed) {
        var popup = new Content.Controls.ImagePopup(true);
        popup.on('submit', function (evt, images) { onImagesSelected(evt, images, ed); });
        popup.show();
    }

    function onImagesSelected(evt, images, ed) {
        for (var i = 0; i < images.length; i++) {
            var current = images[i];
            ed.focus();
            var range = ed.selection.getRng();
            var node = ed.getDoc().createElement('img');
            var uri = current.Uri;

            if (current.Size)
                uri += '.' + current.Size;

            node.src = uri;
            node.alt = current.Title;
            range.insertNode(node);
        }
    }

    function onSegmentModification() {
        autosaveTracking.lastModificationTime = new Date();
        if (!autosaveTracking.timer)
            autosaveTracking.timer = setTimeout(onAutosaveTimer, autosaveTracking.INTERVAL_IN_SECONDS * 1000);
    }

    function onAutosaveTimer() {

        if (!autosaveTracking.lastModificationTime)
            return;

        var target = new Date(autosaveTracking.lastModificationTime);
        target.setSeconds(target.getSeconds() + autosaveTracking.INTERVAL_IN_SECONDS);

        var now = new Date();

        if (now >= target) {
            stopAutosaveTimer();
            autosave();
            return;
        }

        if (autosaveTracking.timer)
            autosaveTracking.timer = setTimeout(onAutosaveTimer, target.getMilliseconds() - now.getMilliseconds());
    }

    function stopAutosaveTimer() {
        if (autosaveTracking.timer) {
            clearTimeout(autosaveTracking.timer);
            autosaveTracking.timer = null;
        }
    }

    function autosave() {
        if (isNew)
            return;

        if (typeof autosaveTracking.lastValue == 'string') {
            if (Content.Pages.ContentView.tinyMceInitialized)
                tinymce.triggerSave();
            var value = autosaveTracking.element.val();
            if (value == autosaveTracking.lastValue)
                return;
            autosaveTracking.lastValue = value;
        }

        var data = getNonFormData();

        data['id'] = _current.Id;
        data['bucketId'] = bucket.Id;

        Callback.submit({
            service: 'Content/AddHistoryContent',
            params: data,
            form: form,
            success: onAutosave
        });
    }

    function populateHistory() {
        if (!_current)
            return;
        Callback.submit('Content/GetHistory', { bucketId: _current.Bucket.Id, id: _current.Id }, onHistory);
    }

    function onHistory(data) {
        var list = form.find('#historyList');

        for (var i = 0; i < data.Items.length; i++) {
            var item = data.Items[i];
            list.append(getHistoryItem(item.Id, item.Time, item.RevisionType));
        }
    }

    function getHistoryItem(id, time, type) {
        var text = Helper.getFormattedDateTime(time);
        if (type)
            text = '(' + type + ') ' + text;

        var item = $('<a>').attr('href', 'javascript:;').addClass('block bitOSpace').text(text).kswid('historyItem');
        item.click(function () {
            loadHistoryItem(id);
        });
        return item;
    }

    function loadHistoryItem(id) {
        Callback.submit('Content/GetHistoryContent', { bucketId: bucket.Id, contentId: _current.Id, id: id }, onHistoryItem);
    }

    function onHistoryItem(data) {
        var newEditor = new Content.Pages.ContentView(data, bucket, contentList, null, onClose);
        newEditor.showDraft();
    }

    function onAutosave(data) {
        var list = form.find('#historyList');
        list.prepend(getHistoryItem(data.Id, data.Time, data.RevisionType));
    }

    return self;
};

// element = control. should add logic for control container
Content.Controls.MetadataEditor2 = function (title, editor, itemTemplate, setter, separator, emptyOnShow, editorContainer, removeOnHide) {
    var self = {
        show: show,
        hide: hide,
        getActuator: getActuator
    };

    var container = null;
    var actuator = null;
    var editorTemplate = null;

    construct();

    function construct() {
        var label = Html.label('clear detailTitleFont').text(title);
        var icon = Html.div('iconAdd').kswid('additionalMetadataAdd');

        actuator = Html.div(null, label, icon).kswid('additionalMetadata');

        icon.click(function () {
            show();
        });

        editorTemplate = editor.clone();
        constructEditor(false);
        hide();
    }

    function constructEditor(cloneEditorTemplate) {
        if (cloneEditorTemplate)
            editor = editorTemplate.clone();

        container = editor.find('#metaDataContainer');
        var removeElementIcon = editor.find('#removeMetadata');

        if (removeElementIcon.length > 0) {
            removeElementIcon.unbind('click').click(function () {
                hide();
            });
        }

        editor.find('#addMetaDataItem').click(function () { addItem(); });
    }

    function show(value) {
        prepEditor();

        container.empty();
        if (value) {
            for (var i = 0; i < value.length; i++) {
                addItem(value[i]);
            }
        }
        else if (!emptyOnShow) {
            addItem();
        }

        if (actuator)
            actuator.hide();
        editor.show();
    }

    function prepEditor() {
        if (!editorContainer[0].contains(editor)) {
            constructEditor(true);

            // Set names
            var id = editor.attr('id');
            var count = editorContainer.children('[id="' + id + '"]').length;

            editorContainer.append(editor);
            var name = editor.data('name');

            if (name) {
                var newName = name + '.' + count;
                editor.attr('name', name + '.' + count);

                // this needs to be recursive...
                editor.children('[data-name]').each(function () {
                    var currentName = newName + '.' + $(this).data('name');

                    $(this).attr('name', currentName);
                });
            } else {
                // should throw an exception here
                //debugger;
            }
        }
    }

    function addItem(value) {
        var itemElement = itemTemplate.clone();
        var id = itemElement.attr('id');
        var count = container.children('[id="' + id + '"]').length;

        setNames(count, itemElement);

        setter(count, itemElement, value);

        itemElement.find('#removeMetaDataItem').click(function () {
            var i = 0;

            var previous = itemElement.prev();
            if (previous.is('.borderSeperator'))
                previous.remove();
            else {
                var next = itemElement.next();
                if (next.is('.borderSeperator'))
                    next.remove();
            }

            itemElement.remove();

            container.children(':not(.borderSeperator)').each(function () {
                setNames(i, $(this));
                i++;
            });
        });

        itemElement.form();

        if (count > 0 && separator)
            container.append(separator.clone());

        container.append(itemElement);
    }

    function updateEditorDataNames() {
        // update names
        var id = editorTemplate.attr('id');
        var editors = editorContainer.children('[id="' + id + '"]');
        editors.each(function () {
            //var child = $(this);
            //var index = editors.index(child);
            //setEditorNames(index, child);
            var currentEditor = $(this);
            var index = editors.index(currentEditor);
            var name = currentEditor.attr('name');

            if (name) {
                var oldName = name;
                var newName = name.replace(/\.\d+$/, '.' + index);
                currentEditor.attr('name', newName);

                if (oldName != newName) {
                    currentEditor.find('[data-name]').each(function () {
                        var newChildName = $(this).attr('name').replace(oldName, newName);
                        $(this).attr('name', newChildName);
                    });
                }
            } else {
                debugger;
            }
        });
    }

    // is there any reason not to remove on hide?
    function hide() {
        if (removeOnHide) {
            if (editor)
                editor.remove();
            editor = null;
            container = null;

            updateEditorDataNames();
        } else {
            editor.hide();

            // clear data
            container.empty();
            editor.find('input[type="checkbox"]').prop('checked', false);
            editor.find('input[type="radio"]').prop('checked', false);
            editor.find('#metaDataContainer').empty();
        }

        // show actuator
        if (actuator)
            actuator.show();
    }

    function getActuator() {
        return actuator;
    }

    function setNames(offset, itemElement) {
        var rootName = getDataParentName(container);

        itemElement.find('[data-name]').each(function () {
            var current = $(this);
            var name = current.data('name');

            if (rootName) {
                if (name)
                    current.attr('name', rootName + '.' + offset + '.' + name);
                else
                    current.attr('name', rootName + '.' + offset);
            }
            else if (name) {
                //debugger;
                current.attr('name', name + '.' + offset);
            }
        });
    }

    function getDataParentName(elem) {
        var name = null;

        if (!elem)
            return name;

        var dataParent = elem.closest('[data-name]');

        if (dataParent) {

            if (name)
                name = dataParent.attr('name') + '.' + name;
            else
                name = dataParent.attr('name');
        }

        return name;
    }

    function getDataName(currentElement, name) {
        if (!currentElement)
            return name;

        var elem = currentElement.closest('[data-name]');

        if (elem) {

            if (name)
                name = elem.data('name') + '.' + name;
            else
                name = elem.data('name');

            if (elem[0] === editor[0]) { // we have reached the top (eg taxonomyTemplate) 
                return name;
            }

            if (editor.closest(elem).length > 0) { // we have gone past the top...should not happen
                return name;
            }

            if (elem == container) {
                // we have reached item container. we should never reach here
            }

            return getDataName(elem.parent(), name);
        }

        return name;
    }

    return self;
};

Content.Controls.MetadataEditor = function (title, element, itemTemplate, setter, separator, emptyOnShow) {
    var self = {
        show: show,
        hide: hide,
        getActuator: getActuator
    };

    var container = element.find('#container'), actuator = null;

    construct();
    function construct() {
        var removeElementIcon = element.find('#removeMetadata');

        if (removeElementIcon.length > 0) {
            var label = Html.label('clear detailTitleFont').text(title);
            var icon = Html.div('iconAdd').kswid('additionalMetadataAdd');

            actuator = Html.div(null, label, icon).kswid('additionalMetadata');

            icon.click(function () {
                show();
            });

            element.find('#removeMetadata').unbind('click').click(function () {
                element.hide();
                element.find('input[type="checkbox"]').prop('checked', false);
                element.find('input[type="radio"]').prop('checked', false);
                element.find('#container').empty();
                actuator.show();
            });
        }

        element.find('#add').click(function () { addItem(); });

        hide();
    }

    function show(value) {
        container.empty();
        if (value) {
            for (var i = 0; i < value.length; i++) {
                addItem(value[i]);
            }
        }
        else if (!emptyOnShow) {
            addItem();
        }

        if (actuator)
            actuator.hide();
        element.show();
    }

    function addItem(value) {
        var itemElement = itemTemplate.clone();

        var count = container.children(':not(.borderSeperator)').length;

        setNames(count, itemElement);

        setter(count, itemElement, value);

        itemElement.find('#remove').click(function () {
            var i = 0;

            var previous = itemElement.prev();
            if (previous.is('.borderSeperator'))
                previous.remove();
            else {
                var next = itemElement.next();
                if (next.is('.borderSeperator'))
                    next.remove();
            }

            itemElement.remove();

            container.children(':not(.borderSeperator)').each(function () {
                setNames(i, $(this));
                i++;
            });
        });

        itemElement.form();

        if (count > 0 && separator)
            container.append(separator.clone());

        container.append(itemElement);
    }

    function hide() {
        element.hide();
        if (actuator)
            actuator.show();
        container.empty();
    }

    function getActuator() {
        return actuator;
    }

    function setNames(offset, itemElement) {
        var elementName = element.data('name');

        itemElement.find('[data-name]').each(function () {
            var current = $(this);
            var name = current.data('name');

            if (elementName) {
                if (name)
                    current.attr('name', elementName + '.' + offset + '.' + name);
                else
                    current.attr('name', elementName + '.' + offset);
            }
            else if (name) {
                current.attr('name', name + '.' + offset);
            }

        });
    }

    return self;
};
