﻿var Content = Content || {};
Content.ImageEditor = function (detail, contentList) {
	var self = {
		getData: getData,
		show: show
	};

	var form,
		template = null,
		shouldShow = false,
	    uploader,
	    replacement,
		readOnly = !Helper.userHasRight('Manage_Content');

	construct();

	function construct() {
		form = Html.div();

		self = $.extend(form, self);

		if (typeof detail == 'string') {
			var bucketId = contentList;
			contentList = null;
			getDetail(bucketId, detail);
			return;
		}

		getHtml();
	}

	function getDetail(bucket, slug) {
		Service.get({
			service: 'Content/' + bucket + '/Images/Details/' + slug,
			success: onDetail
		});
	}

	function getHtml() {
		Navigation.set('images', detail.Bucket.Slug, detail.Slug);

		ResourceProvider.getHtml('Content/ImageEditor.html', onHtml);
	}

	function onDetail(data) {
		detail = data;

		getHtml();
	}

	function show() {
		if (!template) {
			shouldShow = true;
			return;
		}

		Page.setTitle('CONTENT MANAGEMENT');

		var toolbar = template.toolbar.clone();

		toolbar.find('#cancel').click(close);
		toolbar.find('#delete').click(deleteImage);
		toolbar.find('#saveAsDraft').click(save);

		Page.setContentTitle(toolbar);

		Page.switchPage(self);

		initPlUpload();
	}

	function close() {
		if (contentList) {
			contentList.show(true);
		} else {
			contentList = new Content.Pages.ContentList();
			contentList.show();
		}
	}

	function save(evt) {
		evt.preventDefault();
		var data = {
			bucketId: detail.Bucket.Id,
			imageId: detail.Id,
			replacement: replacement
		};

		Callback.submit({
			service: 'Content/UpdateImageDetail',
			params: data,
			success: close,
			form: self
		});
	}

	function initPlUpload() {
		uploader = new plupload.Uploader({
			container: 'upload-container',
			browse_button: 'replace-image',
			max_file_size: '5mb',
			runtimes: 'html5,html4',
			unique_names: true,
			url: 'Callback/Content/UploadImage?bucketId=' + detail.Bucket.Id,
			multi_selection: false,
			filters: [
				{ title: 'Image Files', extensions: 'jpg,gif,png' },
				{ title: 'Document Files', extensions: 'pdf,docx,xlsx' }
			]
		});

		uploader.init();

		uploader.bind('FilesAdded', onFilesAdded);

		uploader.bind('FileUploaded', onFileUploaded);

		uploader.bind('Error', onUploadError);
	}

	function onFilesAdded() {
		uploader.start();
	}

	function onFileUploaded(up, file, response) {
		var data = JSON.parse(response.response);

		replacement = data.Id;

		var container = form.find('#imageContainer');

		container.empty();

		var image = $('<img>').addClass('itemBox dark constrained').css('overflow', 'visible').attr('src', data.Uri + '?_=' + $.now());

		container.append(image);
	}

	function onUploadError(up, error) {
		var message = 'Cannot upload image "' + error.file.name + '." Image is too large or in an unrecognized format.';

		var warning = new Warning(message);

		warning.show(3500);
	}

	function onHtml(html) {

		template = {
			deleteDialog: html.find('#deleteContentDialog').remove(),
			alternateTitle: html.find('#alternateTitleTemplate').remove(),
			editor: html.find('#image-editor').remove(),
			toolbar: html.find('#editImageToolbar').remove()
		};

		form.append(template.editor.clone());

		form.find('#title').val(detail.Title);
		form.find('#blurb').val(detail.Blurb);

		if (detail.Tags)
			form.find('#tags').val().split(detail.Tags.join);

		viewBucket();
		viewSlug();

		if (readOnly) {
			viewTitle();
			viewInvertedTitle();
			viewAlternateTitles();
			viewBlurb();
			viewDates();
			viewLegacyId();
		}
		else {
			editTitle();
			editInvertedTitle();
			editAlternateTitles();
			editBlurb();
			editDates();
			editLegacyId();
		}

		var container = form.find('#imageContainer');

		container.empty();

		var image = $('<img>').addClass('itemBox dark constrained').attr('src', detail.Uri + '?_=' + $.now());

		container.append(image);

		initExpander('#metadataExpander', '#metadataExpansion');

		if (shouldShow)
			show();
	}

	function viewBucket() {
		form.find('#bucket').text(detail.Bucket.Name);
	}

	function viewTitle() {
		form.find('#editTitle').remove();
		form.find('#title').text(detail.Title);
	}

	function viewLegacyId() {
		form.find('#editLegacyId').remove();
		form.find('#legacyId').text(detail.Title);
	}

	function editLegacyId() {
		form.find('#viewLegacyId').remove();
		var element = form.find('#legacyIdValue');
		element.val(detail.LegacyId);
	}

	function editTitle() {
		form.find('#viewTitle').remove();
		var element = form.find('#titleValue');
		element.val(detail.Title);
	}

	function viewInvertedTitle() {
		form.find('#editInvertedTitle').remove();
		form.find('#invertedTitle').text(detail.InvertedTitle);
	}

	function editInvertedTitle() {
		form.find('#viewInvertedTitle').remove();
		if (detail)
			form.find('#invertedTitle').val(detail.InvertedTitle);
	}

	function viewAlternateTitles() {
		form.find('#editAlternateTitles').remove();
		var container = form.find('#alternateTitleView');
		if (!detail.AlternateTitles || detail.AlternateTitles.length == 0) {
			container.append($('<div>').text('(None)'));
		} else {
			for (var i = 0; i < detail.AlternateTitles.length; i++) {
				container.append($('<div>').text(detail.AlternateTitles[i]));
			}
		}
	}

	function editAlternateTitles() {
		form.find('#viewAlternateTitles').remove();

		var element = form.find('#editAlternateTitles');

		var metadataEditor = new Content.Controls.MetadataEditor(null, element, template.alternateTitle, setGenericMetadata, null, true);

		if (detail && detail.AlternateTitles)
			metadataEditor.show(detail.AlternateTitles);
		else
			metadataEditor.show();
	}

	function setGenericMetadata(offset, element, value) {
		element.find('#value').val(value);
	}

	function viewBlurb() {
		form.find('#editBlurb').remove();
		if (detail.Blurb)
			form.find('#blurb').text(detail.Blurb);
	}

	function editBlurb() {
		form.find('#viewBlurb').remove();

		if (detail && detail.Blurb)
			form.find('#blurbTextArea').text(detail.Blurb);
	}

	function viewSlug() {
		form.find('#editSlug').remove();
		form.find('#slug').text(detail.Slug);
	}

	function editDates() {
		form.find('#lastReviewedDate').remove();
		form.find('#postingDate').remove();

		var now = Helper.getFormattedDate(new Date());
		var posting = form.find('#postingDatePicker');

		if (detail.PostingDate)
			posting.val(Helper.dateFromDateTime(detail.PostingDate));
		else
			posting.val(now);

		form.find('#createdDate').text(Helper.getFormattedDate(detail.DateAdded));

		posting.datepicker({ dateFormat: "m/d/yy" });
	}

	function viewDates() {
		form.find('#postingDatePicker').remove();

		if (detail.PostingDate) {
			form.find('#postingDate').text(Helper.getFormattedDate(detail.PostingDate));
		}

		form.find('#createdDate').text(Helper.getFormattedDate(detail.DateAdded));
	}

	function getData() {

		return {
			title: form.find('#title').val(),
			blurb: form.find('#blurb').val(),
			//tags: form.find('#tags').val().split(','), // for when tags are added back in
			slug: form.find('#slugInputValue').val(),
			imageId: detail.Id,
			bucketId: detail.BucketId
		};
	}

	function deleteImage() {
		var popup = new Popup();
		var dialog = template.deleteDialog.clone();
		dialog.find('#type').text(detail.Type.toLowerCase());
		dialog.find('#deleteContentName').text(detail.Title);
		dialog.find('#deleteContentCancel').click(function () {
			popup.close();
		});
		dialog.find('#deleteContentConfirm').click(function () {
			Service.del({
				service: 'Content/' + content.Bucket.Id + '/' + content.Id,
				success: function () {

					popup.close();
					close();
				}
			});
		});
		popup.show(dialog);
	}

	function initExpander(expander, expansion) {
		var expanderElement = form.find(expander);
		var expansionElement = form.find(expansion);
		expanderElement.click(function () {
			if (expansionElement.is(':visible')) {
				expansionElement.hide();
				expanderElement.removeClass('iconExpandedBig');
				expanderElement.addClass('iconCollapsedBig');
			} else {
				expansionElement.show();
				expanderElement.removeClass('iconCollapsedBig');
				expanderElement.addClass('iconExpandedBig');
			}
		});
	}

	return self;
};
