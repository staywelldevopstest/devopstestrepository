﻿var Content = Content || {};
Content.Pages = Content.Pages || {};

Content.Pages.ImageUploader = function (bucket, list) {
	var self = {
		show: show
	};

	var uploader;
	var imageUploadProgresses = [];
	var imageUploadMetas = [];
	var template;
	var form, selectFilesForm = null, uploadForm = null, toolbar;

	function show() {
		ResourceProvider.getHtml('Content/ImageUploader.html', onHtml);
	}

	function onHtml(html) {
		template = {
			fileSelection: html.find('#upload-selection').remove(),
			upload: html.find('#upload').remove(),
			toolbar: html.find('#uploadImageToolbar').remove()
		};

		showFileSelection();
	}

	function showFileSelection() {
		if (!selectFilesForm) {
			selectFilesForm = template.fileSelection.clone();

			selectFilesForm.find('#cancel').click(cancelFileSelection);

			selectFilesForm.hide();

			form = Html.div();
			form.append(selectFilesForm);
			Page.switchPage(form);
			Page.setTitle('IMAGE UPLOAD');

			toolbar = template.toolbar.clone();

			toolbar.find('#title').text('Add an Image > ' + bucket.Name);

			toolbar.find('#finish').click(finish);

			toolbar.find('#cancel').click(close);

			toolbar.find('#buttons').hide();

			Page.setContentTitle(toolbar);
		}

		if (uploadForm) {
			uploadForm.customHide(function () {
				selectFilesForm.customShow(onFileSelectorVisible);
			});
		}
		else {
			selectFilesForm.customShow(onFileSelectorVisible);
		}
	}
	
	function cancelFileSelection() {
		if (uploadForm) {
			selectFilesForm.customHide(function () {
				toolbar.find('#buttons').show();
				uploadForm.customShow();
			});
		}
		else {
			close();
		}
	}

	function onFileSelectorVisible() {
		if (uploader)
			uploader.destroy();

		uploader = new plupload.Uploader({
			container: 'upload-container',
			browse_button: 'selectFiles',
			max_file_size: '5mb',
			drop_element: 'dragTarget',
			runtimes: 'html5,html4',
			unique_names: true,
			url: 'Callback/Content/UploadImage?bucketId=' + bucket.Id,
			multi_selection: true,
			filters: [
				{ title: 'Image Files', extensions: 'jpg,gif,png' },
				{ title: 'Document Files', extensions: 'pdf,docx,xlsx' }
			]
		});

		uploader.init();

		if (uploader.runtime == 'html4') {
			selectFilesForm.on('drop', function (e) {
				var warning = new Warning('Your browser does not support drag and drop uploading.');
				warning.show(3500);
				e.stopPropagation();
			});
		}

		uploader.bind('FilesAdded', onFilesAdded);

		uploader.bind('UploadProgress', function (up, file) {

			var imageUploadProgress = getImageUploadProgress(file.id);

			imageUploadProgress.progressChanged(file.status, file.loaded, file.size);
		});

		uploader.bind('FileUploaded', onFileUploaded);

		uploader.bind('Error', onUploadError);
	}

	function onFilesAdded(up, files) {
	    showUploadForm(files);		
	}

	function showUploadForm(files, hideFinish) {
        if (!uploadForm) {
            uploadForm = template.upload.clone();
            uploadForm.hide();
            form.append(uploadForm);
        }
        
        uploadForm.find('#uploadMore').unbind().click(showFileSelection);

        var uploadList = uploadForm.find('#list');

	    if (hideFinish) {
            toolbar.find('#finish').hide();
        }
	    else
            toolbar.find('#finish').show();
	    
        for (var i = 0; i < files.length; i++) {
            var imageUploadProgress = Content.ImageUploadProgress(files[i].id);
            imageUploadProgresses.push(imageUploadProgress);
            uploadList.append(imageUploadProgress.view());
        }

        selectFilesForm.customHide(function () {
            toolbar.find('#buttons').show();
            uploadForm.customShow();
            uploader.start();
        });
    }

    function onUploadError(up, error) {
	    var message = 'Cannot upload image "' + error.file.name + '."';
	    
        // TODO: handle other error types
	    if (error.code == plupload.FILE_SIZE_ERROR)
	        message += ' Image is too large.';
	    else
	        message += ' Image is in an unrecognized format.';
	    
		var progress = getImageUploadProgress(error.file.id);
		if (!progress) {
		    // plupload throws an error if file is too large BEFORE the event FilesAdded is fired
		    var files = [error.file];
		    showUploadForm(files);
		    progress = getImageUploadProgress(error.file.id);
		}

	    progress.showError(message);
	}

	function onFileUploaded(up, file, response) {
		var data = JSON.parse(response.response);

		var imageUploadProgress = getImageUploadProgress(file.id);
		imageUploadProgress.remove();

		var imageUploadMeta = new Content.ImageUploadEditor(data);
		imageUploadMetas.push(imageUploadMeta);
		uploadForm.find('#list').append(imageUploadMeta);
	}

	function close() {
		if (list)
			list.show();

		list = new Content.Pages.ContentList();
		list.show();
	}

	function finish() {
	    var deferreds = imageUploadMetas.map(function(item, i) {
	        var imageMeta = item.getData();
	        return Callback.submit(
	            'Content/UpdateImageDetail',
	            imageMeta,
	            function() {
	                item.makeSlugReadOnly();
	            }
	        );
	    });

	    $.when.apply(null, deferreds).then(onFinished);
	}

	function onFinished() {
        close();
	}
    
    function getImageUploadProgress(fileId) {
		return imageUploadProgresses.where(function (item) { return item.id() == fileId; }).firstOrNull();
	}

	return self;
};