﻿var Content = Content || {};
Content.Pages = Content.Pages || {};


Content.Pages.CollectionView = function (title, collection, contentManagementList, clone) {
	var self = {
		show: show
	};

	var form,
	    _topicCount = 0,
	    _contentCount = 0,
	    isNew = !collection || clone,
        _topicContainer,
        collectionObject = null,
	    template,
        expiresDropDown = null,
	    readOnly = !Helper.userHasRight('Manage_Content');

	var autosaveTracking = {
		INTERVAL_IN_SECONDS: 5,
		timer: null,
		lastModificationTime: null,
		lastValue: null,
		element: null
	};

	function onContentViewClose() {
	    show(false);
	}
    
	function show(refresh) {
	    if (refresh == false) {
	        Navigation.set('collections', collectionObject.Slug);
	        showEditContentTitle();
		    Page.switchPage(form);
		}
		else {
			getResources(function () {
				getCollection(setupCollection);
			});
		}

		Page.setTitle(title);
	}

	function getResources(callback) {
		if (!template)
			getHtml(callback);
		else {
			callback();
		}
	}

	function getHtml(callback) {
		ResourceProvider.getHtml('Content/CollectionView.html', function (html) {
			onHtml(html, callback);
		});
	}

	function getCollection(callback) {
		if (typeof collection == 'string') {
			// get collection
			Service.get({
				service: 'Collections/' + collection,
				data: { includeChildren: true, recursive: true },
				success: function (data) {
					collectionObject = data;
					callback();
				},
				failure: function (data) {
					var warning = new Warning(data.Details);
					warning.show(5000);
				}
			});
		}
		else {
			// assume collection is a collection object
			collectionObject = collection;
			callback();
		}
	}

	// TODO: Refactor
	function onHtml(html, callback) {
		// order is important here, the metadata elements must be removed
		// before the form
		template = {
			addAction: html.find('#addAction').remove(),
			separator: html.find('#metadataSeparator').remove(),
			topic: html.find('#topic-template').remove(),
			form: html.find('#collectionView').remove(),
			viewToolbar: html.find('#viewCollectionToolbar').remove(),
			editToolbar: html.find('#editCollectionToolbar').remove(),
			contentItem: html.find('#contentItemTemplate').remove(),
			confirmDelete: html.find('#delete-confirmation').remove(),

			//searchItemTemplate: html.find('#content-search-item-template').remove(),
			searchContentTemplate: html.find('#search-content-template').remove()

		};

		callback();
	}

	function setupCollection() {
		//Page.setTitle('COLLECTION MANAGEMENT');

		if (collectionObject && !clone)
			Navigation.set('collections', collectionObject.Slug);

		if (clone) {
			clonify();
		}

		form = template.form.clone();

		_topicContainer = form.find('#topic-container');

		if (!readOnly)
			showEdit();
		else
			showReadOnly();

		initExpanders();

		Page.switchPage(form);
	}

	// TODO: refactor wrt showEdit
	function showReadOnly() {
		var contentTitle = template.viewToolbar.clone();

		debugger;
		// look at contentTitle...
		contentTitle.find('#finished').click(close);

		Page.setContentTitle(contentTitle);

		//form.find('#additionalMetadata1').remove();
		//form.find('#additionalMetadata2').remove();

		viewTitle();
		viewSlug();

		// call form() on mainform here. form() is also called individually on items
		form.form();
	}

	function clonify() {
		collectionObject.Title += ' - copy';
		collectionObject.Id = null;
		collectionObject.CreatedBy = null;
		collectionObject.DateAdded = null;
		collectionObject.DateModified = null;
		collectionObject.Slug = null;
		updateSlug(collectionObject.Title);
	}

	function showEditContentTitle() {
	    var contentTitle = template.editToolbar.clone();

	    contentTitle.find('#cancel').click(close);
	    contentTitle.find('#save').click(save);
	    if (isNew) {
	        contentTitle.find('#delete').remove();
	    } else {
	        contentTitle.find('#delete').click(deleteCollection);
	    }

	    Page.setContentTitle(contentTitle);
	}

	function showEdit() {
	    showEditContentTitle();

		editImage();
		editTitle();
		editSlug();
		showExpires();
		populateFields();
		setupTopicEvents();

		// call form() on mainform here. form() is also called individually on items
		form.form();

		editTopics();
	}

	function setupTopicEvents() {
		form.find('#addTopic').click(addTopic);
		form.find('#addContent').click(function () { searchForContent($(), _topicContainer); });
	}

	function searchForContent(topic, container) {

		form.find('#collection-items').hide();
		var mainArea = form.find('#items-area');
		var search = template.searchContentTemplate.clone();
		mainArea.prepend(search);  // do this differently

		var searchControlContainer = search.find('#search-area');
		var searchControl = Content.Controls.ContentSearch(searchControlContainer, false, self);
		searchControlContainer.append(searchControl);

		search.find('#cancel').click(cancelContentSearch);
		search.find('#addSelected').click({ topic: topic, container: container, search: searchControl }, addSelectedContent);

		if (container.attr('id') != 'topic-container') {
			var titleElement = topic.find('#title').first();
			var titleValue = titleElement.text() || titleElement.val() || '(Empty Title)';
			search.find('#topic-title').text(titleValue);
		} else {
			search.find('#target').text('Add to Collection');
		}

		searchControl.show();
	}

	function hideContentSearch() {
		form.find('#search-content-template').remove();
		form.find('#collection-items').show();
	}

	function cancelContentSearch() {
		hideContentSearch();
	}

	function addSelectedContent(event) {

		// data: { topic: topic, container: container, search: searchControl }
		var selectedItems = event.data.search.getSelected();
		for (var i = 0; i < selectedItems.length; i++) {
			var item = createContentItem(selectedItems[i]);
			event.data.container.prepend(item);

			var expander = event.data.topic.find('#expander');
			expander.trigger('click');
		}

		hideContentSearch();
		
		_contentCount++;
		updateChildCounts();
	}

	function createContentItem(content) {
		var contentItem = null;
		if (content) {
			// should we instead use a function for contentItem so we can store/retrieve values more easily?
			contentItem = template.contentItem.clone();

			var detailsExpansion = contentItem.find('#expansion');
			var detailsExpander = contentItem.find('#expander');
			detailsExpansion.hide();

			detailsExpander.click(function () {
				if (detailsExpansion.is(':visible')) {
					detailsExpansion.hide();
					detailsExpander.text('Details');
				} else {
					detailsExpansion.show();
					detailsExpander.text('Close Details');
				}
			});

			contentItem.find('#slug').text(content.Slug);
			contentItem.data('bucket-slug', content.Bucket.Slug);
			contentItem.find('#lastReviewed').text(content.LastReviewedDate);
			contentItem.find('#servicelines').text(getServicelinesString(content.Servicelines));
			contentItem.find('#blurb').text(content.Blurb);
			contentItem.find('#view-article').click(function () { viewArticle(content); });
			contentItem.find('#remove').click(function () {
				confirmRemoveContent(contentItem, contentItem);
			});
			contentItem.find('#flag-comment').val(content.FlagComment);
			contentItem.data('slug', content.Slug);
			contentItem.find('#title').text(content.Title);

			contentItem.find('#disable').click(contentItem, disableContent);
			if (content.Disabled)
				disableContent({ data: contentItem });

			contentItem.find('#flag-Content')
		        .change(contentItem, onFlagChanged)
	            .attr('checked', content.Flagged);

			contentItem.form();
		}
		return contentItem;
	}

	function getServicelinesString(servicelines) {
		var x = [];
		if (servicelines) {
			for (var i = 0; i < servicelines.length; i++) {
				x.push('[' + servicelines[i].PathValues.join(':') + ']');
			}
		}
		return x.join(', ');
	}

	function disableContent(event) {
		var item = event.data;
		var button = item.find('#disable');
		var icon = item.find('#icon');

		if (item.data('disabled')) {
			button.text('Disable this Article');
			item.data('disabled', false);
			icon.removeClass('disabled');

		} else {
			button.text('Enable this Article');
			item.data('disabled', true);
			icon.addClass('disabled');
		}
	}

	function viewArticle(content) {
	    var contentView = new Content.Pages.ContentView(content.Slug, content.Bucket.Slug, self, null, onContentViewClose);
		contentView.showPublished();
	}

	function onFlagChanged(event) {
		var contentItem = event.data;

		var checked = $(event.target).attr('checked');
		var flagCommentArea = contentItem.find('#flag-comment-container');
		var flagComment = contentItem.find('#flag-comment');

		if (checked) {
			flagCommentArea.show();
		}
		else {
			flagCommentArea.hide();
			flagComment.find('#flag-comment').val('');
		}
	}

	function addTopic() {
		_topicContainer.prepend(createNewTopic(false));
		_topicCount++;
		updateChildCounts();
	}

	function createNewTopic(alternate) {
		var element = template.topic.clone();

		element.data('type', 'new');
		element.find('[data-option="existing"]').remove();

		setupTopic(element, alternate);

		return element;
	}

	function createExistingTopic(item, alternate) {

		var element = template.topic.clone();

		element.data('type', 'existing');
		element.data('slug', item.Slug);
		element.data('title', item.Title);
		element.find('[data-option="new"]').remove();
		element.find('#expansion').hide();

		setupTopic(element, alternate);

		element.find('#title').text(item.Title);
		element.find('#description').val(item.Description);

		if (item.ImageUri) {
			element.find('#small-image')
		        .attr('src', item.ImageUri + '.35x35')
		        .attr('alt', item.Title)
		        .show();
			element.find('#image')
		        .attr('src', item.ImageUri + '.150x150')
				.data('uri', item.ImageUri)
			    .attr('alt', item.Title);
		}

		if (item.Flagged) {
			var flag = element.find('#flag');
			flag.removeClass('disabled');
			element.find('#flag-comment-container').show();
			element.find('#flag-comment').val(item.FlagComment);
		}

		element.form();

		if (item.Items && item.Items.length > 0) {
			populateChildren(element.find('#children'), item.Items, !alternate);
		}

		return element;
	}

	function setupTopic(element, alternate) {
		var box = element.find('#box');
		if (alternate)
			box.addClass('alternate');

		var itemForm = element.find('#item-form');

		var detailsLink = element.find('#details');
		var expansion = element.find('#expansion');
		var expander = element.find('#expander');
		var children = element.find('#children');

		var imageElement = itemForm.find('#image');
		var imageIcon = itemForm.find('#small-image');
		var titleContainer = itemForm.find('#title-container');
		itemForm.find('#change-image').click(function () {
			onChangeImage(imageElement, imageIcon, titleContainer);
		});

		children.data('box', box);

		expander.click(function () {
			if (children.is(':visible')) {
				expander.removeClass('iconExpandedBig');
				expander.addClass('iconCollapsedBig');
				children.hide();
			} else {
				expander.removeClass('iconCollapsedBig');
				expander.addClass('iconExpandedBig');
				children.show();
			}
		});

		var flag = element.find('#flag');
		flag.click(function () {
			if (flag.hasClass('disabled')) {
				flag.removeClass('disabled');
				itemForm.find('#flag-comment-container').show();
			}
			else {
				flag.addClass('disabled');
				itemForm.find('#flag-comment-container').hide();
				itemForm.find('#flag-comment').val('');
			}
		});

		var lock = element.find('#lock');
		lock.click(function () {
			if (lock.hasClass('disabled')) {
				lock.removeClass('disabled');
				itemForm.find('#lock-on').show();
			}
			else {
				lock.addClass('disabled');
				itemForm.find('#lock-on').hide();
			}
		});

		var dynamic = element.find('#dynamic');
		dynamic.click(function () {
			if (dynamic.hasClass('disabled')) {
				dynamic.removeClass('disabled');
				itemForm.find('#dynamic-on').show();
			}
			else {
				dynamic.addClass('disabled');
				itemForm.find('#dynamic-on').hide();
			}
		});

		detailsLink.click(function () {
			if (expansion.is(':visible')) {
				detailsLink.text('Details');
				expansion.hide();
			} else {
				detailsLink.text('Close Details');
				expansion.show();
			}
		});

		element.find('#add-topic').click(function () {
			if (!children.is(':visible')) {
				expander.removeClass('iconCollapsedBig');
				expander.addClass('iconExpandedBig');
				children.show();
			}

			var newTopic = createNewTopic(!alternate);
			children.prepend(newTopic);

			_topicCount++;
			updateChildCounts();

			newTopic.find('#title').focus();
		});

		element.find('#add-content').click(function () {
			searchForContent(element, children);
		});

		element.find('#delete-topic-button').click(function () {
			confirmTopicDeletion(element, itemForm);
		});

		element.find('#remove').click(function () {
			confirmTopicDeletion(element, itemForm);
		});
	}

	function confirmTopicDeletion(element, itemForm) {
		var dialog = template.confirmDelete.clone();
		var titleElement = itemForm.find('#title');
		var titleValue = titleElement.text() || titleElement.val() || '(Empty Title)';

		dialog.find('#title').text(titleValue);

		var popup = new Popup();

		dialog.find('#cancel-delete').click(function () {
			popup.close();
		});

		dialog.find('#confirm-delete').click(function () {
			element.remove();
			popup.close();
			_topicCount--;
			updateChildCounts();
		});
		popup.show(dialog);
	}

	function confirmRemoveContent(element, itemForm) {
		var dialog = template.confirmDelete.clone();
		var titleElement = itemForm.find('#title');
		var titleValue = titleElement.text() || titleElement.val() || '(Empty Title)';

		dialog.find('#title').text(titleValue);
		dialog.find('#action').text('REMOVE');
		dialog.find('#type').text('content');

		var popup = new Popup();

		dialog.find('#cancel-delete').click(function () {
			popup.close();
		});

		dialog.find('#confirm-delete')
	        .text('REMOVE')
	        .click(function () {
	        	element.remove();
	        	popup.close();
	        	_contentCount--;
	        	updateChildCounts();
	        });

		popup.show(dialog);
	}

	function editImage() {
		var imageElement = form.find('#collectionImage');

		form.find('#changeImage').click(function () {
			onChangeImage(imageElement);
		});

		if (collectionObject) {
			if (collectionObject.ImageUri) {
				imageElement.data('uri', collectionObject.ImageUri);
				imageElement.attr('src', collectionObject.ImageUri + ".150x150")
					.attr('alt', collectionObject.Title);
			}
		}
	}

	function onChangeImage(element, imageIcon, titleContainer) {
		var popup = new Content.Controls.ImagePopup(false, true, true);
		popup.on('submit', function (evt, image) { onImagesSelected(evt, image, element, imageIcon, titleContainer); });
		popup.show();
	}

	function onImagesSelected(evt, images, imageElement, imageIcon, titleContainer) {
		var image = images[0];

		imageElement.attr('src', image.Uri + '.150x150')
					.attr('alt', image.Title);

		if (imageIcon) {
			imageIcon.attr('src', image.Uri + '.35x35');
			imageIcon.show();

			if (titleContainer) {
				titleContainer.removeClass('indent-30');
				titleContainer.addClass('indent-70');
			}
		}

		imageElement.data('uri', image.Uri);
	}

	function populateFields() {

		if (collectionObject) {
			var details = form.find('#collectionDetails');
			details.find('#descriptionTextArea').val(collectionObject.Description);
			details.find('#createdDate').text(Helper.getFormattedDate(collectionObject.DateAdded));
			details.find('#numLicenses').text(collectionObject.LicenseCount);
			details.find('#numLicenses').text(collectionObject.LicenseCount);
			if (collectionObject.CreatedBy != null)
				details.find('#createdBy').text(collectionObject.CreatedBy.FullName);

			// remaining fields...
		}
	}

	function editTopics() {
		if (!collectionObject || !collectionObject.Items)
			return;

		populateChildren(_topicContainer, collectionObject.Items, false);

		updateChildCounts();
	}
	
	function updateChildCounts() {
		form.find('#numTopics').text(_topicCount);
		form.find('#numContent').text(_contentCount);
	}

	function populateChildren(container, items, alternate) {
		for (var i = 0; i < items.length; i++) {
			container.append(populateChild(items[i], alternate));
		}

		form.find('.sort-container').sortable({
			connectWith: '.sort-container',
			update: onReorder
		});
	}

	function onReorder(event, ui) {
		var parent = ui.item.parent().data('box');
		if (!parent || parent.hasClass('alternate')) {
			ui.item.find('#box').removeClass('alternate');
		}
		else {
			ui.item.find('#box').addClass('alternate');
		}
	}

	function populateChild(item, alternate) {
		switch (item.Type) {
			case 'Topic':
				_topicCount++;
				return createExistingTopic(item, alternate);
			case 'Content':
				_contentCount++;
				return createContentItem(item);
		}

		return null;
	}

	function showExpires() {
		if (readOnly) {

		} else {
			expiresDropDown = new DropDown('dropDown medium', 'expires', null, null, getExpiresDate).kswid('expiresDropDown');

			var defaultOption = { 'name': 'Never', 'value': 'Never' };
			var specificDateOption = { 'name': 'Specific date', 'value': null, 'isDatepicker': true, 'displayActualValue': true };
			var options = [
				defaultOption,
				{ 'name': '2 days', 'value': '2 days', 'add': { 'days': 2 }, 'displayActualValue': true },
				{ 'name': '1 week', 'value': '1 week', 'add': { 'days': 7 }, 'displayActualValue': true },
				{ 'name': '1 month', 'value': '1 month', 'add': { 'months': 1 }, 'displayActualValue': true },
				{ 'name': '6 months', 'value': '6 months', 'add': { 'months': 6 }, 'displayActualValue': true },
				{ 'name': '1 year', 'value': '1 year', 'add': { 'years': 1 }, 'displayActualValue': true },
				specificDateOption
			];
			expiresDropDown.setOptions(options, null);
			if (collectionObject) {
				if (!collectionObject.Expires)
					expiresDropDown.selectOption(defaultOption);
				else {
					specificDateOption.value = Helper.getFormattedDate(collectionObject.Expires);
					expiresDropDown.selectOption(specificDateOption);
				}
			} else {
				expiresDropDown.selectOption(defaultOption);
			}

			form.find('#expireOptions').append(expiresDropDown);
		}
	}

	function getExpiresDate(option) {
		if (option.isDatepicker)
			return option.value;

		if (option.add) {
			var date = new Date();

			if (option.add.years)
				date.addYears(option.add.years);
			if (option.add.months)
				date.addMonths(option.add.months);
			if (option.add.days)
				date.addDays(option.add.days);
			return Helper.getFormattedDate(date);
		}

		return null;
	}

	function close() {
	    stopAutosaveTimer();

	    if (contentManagementList) {
			contentManagementList.show(true);
		} else {
			contentManagementList = new Content.Pages.ContentManagementList('Collection Management', 'Add Collection', 'Collections');
			contentManagementList.show();
		}
	}

	function viewTitle() {
		form.find('#editTitle').remove();
		form.find('#title').text(collectionObject.Title);
	}

	function editTitle() {
		form.find('#viewTitle').remove();
		var element = form.find('#titleValue');
		if (collectionObject)
			element.val(collectionObject.Title);

		if (isNew) {
			// this seems a little busy...
			// updateSlug() is async, so no way to guarantee that the last update corresponds to the last keystroke...
		    element
		        .keypress(function(e) {
		            var cursor = Helper.getSelectionStart(e.target);
		            // ignore leading whitespace
		            if (e.which == 32 && cursor == 0) {
		                e.preventDefault();
		            }
		        })
		        .keyup(function() {
		            updateSlug($(this).val());
		        });
		}
	}

    function viewSlug() {
		form.find('#editSlug').remove();
		form.find('#slug').text(collectionObject.Slug);
	}

	function editSlug() {
		form.find('#viewSlug').remove();

		var slugButton = form.find('#editSlug');

		if (isNew) {
			slugButton.click(function () {
			    slugButton.hide();
			    form.find('#slugValue').hide();
			    var input = form.find('#slugInputValue');
			    
				input.show();
				input.select();
			});
		} else {
		    if (collectionObject) {
				form.find('#slugValue').text(collectionObject.Slug);
				form.find('#slugInputValue').val(collectionObject.Slug);
			}
			slugButton.hide();
		}
	}

	function updateSlug(value) {
	    if (!value)
	        return;
	    
	    value = Helper.getSlug(value);
		var params = {
			slug: value
		};

		Service.get('Collections/Slugs', params, function (data) {
			form.find('#slugValue').text(data.Value);
			form.find('#slugInputValue').val(data.Value);
		});
	}

	function getNonFormData() {
		var data = {};

		if (!isNew)
			data['id'] = collectionObject.Id;

		data.imageUri = form.find('#collectionImage').data('uri');

		data.items = getTopics(_topicContainer);

		return data;
	}

	function getTopics(container) {
		var children = container.children();

		if (children.length == 0)
			return null;

		var result = [];

		children.each(function () {
			var element = $(this);
			switch (element.data('type')) {
				case 'new':
					var itemForm = element.find('#item-form');
					var flag = !itemForm.find('#flag').hasClass('disabled');
					result.push({
						type: 'Topic',
						title: itemForm.find('#title').val(),
						description: itemForm.find('#description').val(),
						flagged: flag,
						flagComment: flag ? itemForm.find('#flag-comment').val() : null,
						items: getTopics(element.find('#children').first()),
						imageUri: itemForm.find('#image').data('uri')
					});
					break;
				case 'existing':
					itemForm = element.find('#item-form');
					flag = !itemForm.find('#flag').hasClass('disabled');
					result.push({
						type: 'Topic',
						itemReference: { idOrSlug: element.data('slug') },
						title: element.data('title'),
						description: element.find('#description').val(),
						flagged: flag,
						flagComment: flag ? itemForm.find('#flag-comment').val() : null,
						items: getTopics(element.find('#children').first()),
						imageUri: itemForm.find('#image').data('uri')
					});
					break;
				case 'content':
					result.push({
						type: 'Content',
						itemReference: { idOrSlug: element.data('slug'), bucketIdOrSlug: element.data('bucket-slug') },
						flagged: element.find('#flag-Content').attr('checked') == 'checked',
						flagComment: element.find('#flag-comment').val(),
						disabled: element.data('disabled')
					}
			        );
					break;
			}
		});

		return result.length > 0 ? result : null;
	}

    function deleteCollection(evt) {
        if (evt)
            evt.preventDefault();

        var popup = new Popup();
        var dialog = template.confirmDelete.clone();
        dialog.find('#type').text('collection');
        dialog.find('#title').text(collectionObject.Title);
        dialog.find('#cancel-delete').click(function() {
            popup.close();
        });
        
        dialog.find('#confirm-delete').click(function() {
            Service.del({
                service: 'Collections/' + collectionObject.Slug,
                success: function() {
                    popup.close();
                    close();
                }
            });
        });
        popup.show(dialog);
    }

    function save(evt) {
		// what is default behaviour in this case?
		if (evt)
			evt.preventDefault();

		stopAutosaveTimer();

		var data = getNonFormData();

		// Submit
		if (!isNew) {
			Service.put({
				service: 'Collections/' + collectionObject.Slug,
				data: data,
				form: form,
				success: onSave
			});
		} else {
			Service.post({
				service: 'Collections',
				data: data,
				form: form,
				success: onSave
			});
		}
	}

	function onSave(data) {
		collectionObject = data;
		close(true);
	}

	function initExpander(expander, expansion) {
		expander.click(function () {
			if (expansion.is(':visible')) {
				expansion.hide();
				expander.removeClass('iconExpandedBig');
				expander.addClass('iconCollapsedBig');
			} else {
				expansion.show();
				expander.removeClass('iconCollapsedBig');
				expander.addClass('iconExpandedBig');
			}
		});
	}

	//function collapseAllExpandersBut(expanderId) {
	//    var z = form.find('[id$=Expander]:not([id=' + expanderId + '])');
	//    z.click();
	//}

	function initExpanders() {
		initExpander(form.find('#detailExpander'), form.find('#detailExpansion'));
		initExpander(form.find('#licensesExpander'), form.find('#licensesExpansion'));
	}

	// TODO: autosavetimer should be its own class
	function onAutosaveTimer() {

		if (!autosaveTracking.lastModificationTime)
			return;

		var target = new Date(autosaveTracking.lastModificationTime);
		target.setSeconds(target.getSeconds() + autosaveTracking.INTERVAL_IN_SECONDS);

		var now = new Date();

		if (now >= target) {
			stopAutosaveTimer();
			autosave();
			return;
		}

		if (autosaveTracking.timer)
			autosaveTracking.timer = setTimeout(onAutosaveTimer, target.getMilliseconds() - now.getMilliseconds());
	}

	function stopAutosaveTimer() {
		if (autosaveTracking.timer) {
			clearTimeout(autosaveTracking.timer);
			autosaveTracking.timer = null;
		}
	}

	function autosave() {
		if (isNew)
			return;

		if (typeof autosaveTracking.lastValue == 'string') {
			debugger;

			var value = autosaveTracking.element.val();
			if (value == autosaveTracking.lastValue)
				return;
			autosaveTracking.lastValue = value;
		}

		var data = getNonFormData();

		data['id'] = collectionObject.Id;

		// handle this correctly...
		debugger;

		//Callback.submit({
		//    service: 'Collection/AddHistoryCollection',
		//    params: data,
		//    form: form,
		//    success: onAutosave
		//});
	}

	function onAutosave(data) {
		// nothing
	}

	return self;


};