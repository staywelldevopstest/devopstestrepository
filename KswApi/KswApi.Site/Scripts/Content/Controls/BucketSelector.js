﻿var Content = Content || {};
Content.Controls = Content.Controls || {};

// triggers events:
// 'selectionChanged' (evt, bucket, selected) -- this is bubbled up from SingleOriginBucketSelector

Content.Controls.BucketSelector = function (multiSelect, buckets, selectedBuckets, hideFilter) {

	var self = {
		unselect: unselect,
		getSelectedIds: getSelectedIds,
		clear: clear,
		focus: focus,
		getSelected: getSelected,
		selectTypes: selectTypes
	};

	var kswSelector, otherSelector, focusOnData = false, bucketTypes = null;

	construct();

	function construct() {
		var div = Html.div();
		self = $.extend(div, self);

		if (buckets)
			populate();
		else
			getBuckets();
	}
	
	function clear() {
		kswSelector.clear();
		otherSelector.clear();
	}

	function getSelected() {
		var results = kswSelector.getSelected();
		return results.concat(otherSelector.getSelected());
	}
	
	function getSelectedIds() {
		var results = kswSelector.getSelectedIds();
		return results.concat(otherSelector.getSelectedIds());
	}

	function unselect(bucket) {
		kswSelector.unselect(bucket.Id);
	}
	
	function selectTypes(types) {
		bucketTypes = types;
		if (kswSelector)
			kswSelector.selectTypes(types);
		if (otherSelector)
			otherSelector.selectTypes(types);
	}

	function getBuckets() {
		Callback.submit({
			service: 'Content/Buckets',
			params: { 'readOnly': null, 'origin': null, 'count': 100, 'offset': 0 },
			success: function (data) {
				buckets = data.Items;
				populate();
			}
		});
	}
	
	function populate() {
		var kswBuckets = [];
		var otherBuckets = [];

		for (var i = 0; i < buckets.length; i++) {
			var current = buckets[i];
			if (current.OriginType == 'Ksw')
				kswBuckets.push(current);
			else
				otherBuckets.push(current);
		}

		kswSelector = new Content.Controls.SingleOriginBucketSelector(multiSelect, hideFilter, "Krames StayWell", selectedBuckets, kswBuckets, null);
		otherSelector = new Content.Controls.SingleOriginBucketSelector(multiSelect, hideFilter, "Other", selectedBuckets, otherBuckets, null);
		
		if (bucketTypes && bucketTypes.length > 0) {
			kswSelector.selectTypes(bucketTypes);
			otherSelector.selectTypes(otherSelector);
		}

		kswSelector.on('selectionChanged', onKswSelect);
		otherSelector.on('selectionChanged', onOtherSelect);

		self.append(kswSelector);
		self.append(Html.div('clear'));
		self.append(otherSelector);
		
		if (focusOnData) {
			focusOnData = false;
			focus();
		}
	}

	function onKswSelect(evt, bucket, selected) {
		if (!multiSelect && selected)
			otherSelector.clear();
	}

	function onOtherSelect(evt, bucket, selected) {
		if (!multiSelect && selected)
			kswSelector.clear();
	}
	
	function focus() {
		if (kswSelector)
			kswSelector.focus();
		else
			focusOnData = true;
	}

	return self;
};
