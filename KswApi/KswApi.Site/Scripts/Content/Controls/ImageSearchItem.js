﻿var Content = Content || {};
Content.Controls = Content.Controls || {};


Content.Controls.ImageSearchItem = function (content, template, readOnly, onEdit, onDelete) {

	// define and return self instead?
	return createImageItem();


	function createImageItem(image) {

		var element = template.imageListItem.clone();

		var imageElement = element.find('#image');

		imageElement.data('uri', image.Uri);

		// nonce on the end is to force ie to get the latest version
		imageElement.attr('src', image.Uri + '.75x75?_=' + $.now());

		element.find('#title').text(image.Title);

		element.find('#summary').text(image.Blurb);

		element.find('#type').text('(' + image.Format.toUpperCase() + ')');

		if (readOnly) {
			element.find('#clientUserActions').remove();
		}
		else {
			element.find('#title').click(function () {
				editImageContent(image);
			});

			element.find('#contentEditAction').click(function () {
				editImageContent(image);
			});

			element.find('#contentDeleteAction').click(function () {
				deleteContent(image);
			});
		}

		var meta = element.find('#metadata1');
		addContentMetadata(meta, 'Bucket', image.Bucket.Name, 'bucket');
		addContentMetadata(meta, 'Origin', image.OriginName, 'origin');

		meta = element.find('#metadata2');
		addContentMetadata(meta, 'Created', Helper.getFormattedDate(image.DateAdded), 'created');
		addContentMetadata(meta, 'Modified', Helper.getFormattedDate(image.DateModified), 'modified');

		return element;
	}

	// COMMON BETWEEN SEARCH ITEM TYPES:
	function editImageContent(image) {
		var editor = new Content.ImageEditor(image, self);
		editor.show();
	}

	function addContentMetadata(div, label, value, kswid) {
		if (!div.is(':empty')) {
			div.append(' | ');
		}

		div.append($('<label class="bold">').text(label + ': '));

		if (typeof value == 'string')
			div.append($('<label>').text(value).kswid(kswid));
		else
			div.append(value);
	}

	function deleteContent() {
		var popup = new Popup();
		var dialog = template.deleteContentDialog.clone();
		dialog.find('#type').text(content.Type.toLowerCase());
		dialog.find('#deleteContentName').text(content.Title);
		dialog.find('#deleteContentCancel').click(function () {
			popup.close();
		});
		dialog.find('#deleteContentConfirm').click(function () {
			Service.del({
				service: 'Content/' + content.Bucket.Id + '/' + content.Id,
				success: function () {
					if (onDelete)
						onDelete();

					popup.close();
				}
			});
		});
		popup.show(dialog);
	}
};