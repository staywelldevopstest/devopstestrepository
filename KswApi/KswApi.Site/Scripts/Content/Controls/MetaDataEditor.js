﻿var Content = Content || {};
Content.Controls = Content.Controls || {};

Content.Controls.Actuator = function (editorContainer, title, editorTemplate, itemCreateMethod, setter, separator, emptyOnShow, hideOnShowEditor, spawnEditor) {
	var self = {
		showEditor: showEditor,
		getActuatorControl: getActuatorControl
	};

	var actuatorControl = null;
	var deleteEditorOnHide = true;

	construct();

	function construct() {
		var icon = Html.div('iconAdd').kswid('additionalMetadataAdd');
		var label = Html.label('clear detailTitleFont').text(title);

		actuatorControl = Html.div(null, label, icon).kswid('additionalMetadata');

		// setup add editor
		icon.click(function () {
			showEditor();
		});

		hideEditor();
	}

	function getActuatorControl() {
		return actuatorControl;
	}

	function hideEditor() {
		// need logic here...
		var editor;

		if (editor)
			editor.hide();

		if (actuatorControl) {
			actuatorControl.show();
		}
	}

	function removeEditor(editorBody) {
		if (hideOnShowEditor) {
			actuatorControl.show();
		}

		if (deleteEditorOnHide) {
			// remove from container
			editorBody.remove();
			
			// rename remaining editors
			updateEditorDataNames();
		}
	}
	
	function updateEditorDataNames()
	{
		// update names
		var id = editorTemplate.attr('id');
		var editors = editorContainer.children('[id="' + id + '"]');
		editors.each(function() {
			var currentEditor = $(this);
			var index = editors.index(currentEditor);
			var name = currentEditor.attr('name');

			if (name) {
				var oldName = name;
				var newName = name.replace(/\.\d+$/, '.' + index);
				currentEditor.attr('name', newName);

				if (oldName != newName) {
					currentEditor.find('[data-name]').each(function () {
						var newChildName = $(this).attr('name').replace(oldName, newName);
						$(this).attr('name', newChildName);
					});
				}
			} else {
				debugger;
			}
		});
	}

	function showEditor(value) {
		if (!editorTemplate)
			return;

		if (hideOnShowEditor) // hideOnShowEditor is a proxy for 1:1
			actuatorControl.hide();

		var editor;
		if (spawnEditor) {
			var editorBody = editorTemplate.clone();
			editorBody.find('#metaDataTitle').text(title);
			editor = Content.Controls.MetadataEditorX(title, editorBody, itemCreateMethod, setter, separator, emptyOnShow, removeEditor, editorContainer);

			// Set editor name <======= Should set all names from the item container up...
			var id = editorBody.attr('id');
			var count = editorContainer.children('[id="' + id + '"]').length;
			var name = editorBody.data('name');
			if (name) {
				var newName = name + '.' + count;
				editorBody.attr('name', newName);

				// add to container
				editorContainer.append(editorBody);
				//Content.Controls.MetadataEditorX.setNamesX(editorContainer, editorContainer);
				Content.Controls.MetadataEditorX.setNamesX(editorBody, editorBody);
			} else {
				// should throw an exception here
				//debugger;
			}
		} else {
			editor = null; // add logic...

			// do stuff...
		}

		// show editor
		editor.show(value);
	}
	
	return self;
};

Content.Controls.MetadataEditorX = function (title, editorBody, itemCreateMethod, setter, separator, emptyOnShow, onRemoveEditor, editorContainer) {
	var self = {
		show: show,
		hide: hide
	};

	var items = editorBody.find('#metaDataContainer');

	construct();

	function construct() {
		var removeElementIcon = editorBody.find('#removeMetadata');

		if (removeElementIcon.length > 0) {

			editorBody.find('#removeMetadata').unbind('click').click(function () {
				removeEditor();
			});
		}

		editorBody.find('#addMetaDataItem').click(function () { addItem(); });

		hide();
	}

	function emptyItems() {
		items.empty();
	}

	function hide() {
		editorBody.hide();
		emptyItems();
	}

	function removeEditor() {
		editorBody.hide();
		editorBody.find('input[type="checkbox"]').prop('checked', false);
		editorBody.find('input[type="radio"]').prop('checked', false);
		editorBody.find('#metaDataContainer').empty();
		if (onRemoveEditor)
			onRemoveEditor(editorBody);
	}

	function addItem(value) {
		var itemElement = itemCreateMethod(value);
		
		if (isArray(itemElement)) {
			$(itemElement).each(function (index) { configureItemElement($(this), value, index); });
		} else {
			configureItemElement(itemElement, value);
		}
	}

	function isArray(testObject) {
		return testObject && !(testObject.propertyIsEnumerable('length'))
			&& typeof testObject === 'object' && typeof testObject.length === 'number';
	}

	function configureItemElement(itemElement, value, index) {
		// readonly logic - make general... readOnlyView class?
		var labelElement = itemElement.find('#metaDataValueLabel');
		labelElement.hide();

		var id = itemElement.attr('id');
		var count = items.children('[id="' + id + '"]').length;

		items.append(itemElement);
		Content.Controls.MetadataEditorX.setNamesX(editorBody, editorBody);
		if (setter)
			setter(itemElement, value, index, editorBody);

		itemElement.find('#removeMetaDataItem').click(function () {
			var i = 0;

			var previous = itemElement.prev();
			if (previous.is('.borderSeperator'))
				previous.remove();
			else {
				var next = itemElement.next();
				if (next.is('.borderSeperator'))
					next.remove();
			}

			itemElement.remove();
			items.children(':not(.borderSeperator)').each(function () {
				setNames(i, $(this));
				i++;
			});
		});

		itemElement.form();

		if (count > 0 && separator)
			items.append(separator.clone());
	}
	
	function show(value) {
		items.empty();
		if (value) {
			for (var i = 0; i < value.length; i++) {
				addItem(value[i]);
			}
		}
		else if (!emptyOnShow) {
			addItem();
		}

		editorBody.show();
	}

	function getOffset(dataElement) {
		var dataParent = dataElement.parent().closest('[data-name]');
		var id = dataElement.attr('id');
		var count = dataParent.children('[id="' + id + '"]').length;
		return count;
	}

	
	function setNames(offset, itemElement) {
		var rootName = getDataParentName(itemElement); 
		itemElement.find('[data-name]').each(function () {
			var current = $(this);
			var name = current.data('name');

			if (rootName) {
				if (name)
					current.attr('name', rootName + '.' + offset + '.' + name);
				else
					current.attr('name', rootName + '.' + offset);
			}
			else if (name) {
				current.attr('name', name + '.' + offset);
			}
		});
	}

	function getDataParentName(elem) {
		var name = null;

		if (!elem)
			return name;

		var dataParent = elem.closest('[data-name]');

		if (dataParent[0]) {

			if (name)
				name = dataParent.attr('name') + '.' + name;
			else
				name = dataParent.attr('name');
		}

		return name;
	}

	// rename and clean up
	function getDataName(currentElement, name) {
		if (!currentElement)
			return name;

		var elem = currentElement.closest('[data-name]');

		if (elem[0]) {

			if (name)
				name = elem.data('name') + '.' + name;
			else
				name = elem.data('name');

			if (elem[0] === editorBody[0]) { // we have reached the top (eg taxonomyTemplate, servicelinesTemplate) 
				return name;
			}

			if (editorBody.closest(elem).length > 0) { // we have gone past the top...should not happen
				return name;
			}

			if (elem[0] === items[0]) {
				// we have reached item container. we should never reach here
			}

			return getDataName(elem.parent(), name);
		}

		return name;
	}

	return self;
};

Content.Controls.MetadataEditorX.setNamesX = function (currentElement, currentDataElement) {
	var dataChildren = currentElement.children('[data-name]');
	var nonDataChildren = currentElement.children(':not([data-name])');
	var parentName = currentDataElement.attr('name');

	dataChildren.each(function () {
		var child = $(this);
		var childName = child.data('name');
		var newChildName;

		if (childName) {
			if (parentName) {
				var selector = '[data-name=' + child.data('name') + ']';
				//var dataSiblingsCount = child.siblings(selector).length;
				var ending = parentName.split('.').pop();
				var parentEndsInNumeric = $.isNumeric(ending);
				if(parentEndsInNumeric) // kind of a hack...
				{
					newChildName = parentName + '.' + childName;
				} else {
					var index = currentDataElement.find(selector).index(child);
					newChildName = parentName + '.' + index + '.' + childName;
				}
				child.attr('name', newChildName);
			}
			if (child.has('[data-name]').length > 0)
				Content.Controls.MetadataEditorX.setNamesX(child, child);
		} else {
			// should this happen?
			debugger;
		}
	});
	
	nonDataChildren.has('[data-name]').each(function () {
		// Recursive call
		Content.Controls.MetadataEditorX.setNamesX($(this), currentDataElement);
	});

}