﻿var Content = Content || {};
Content.ImageUploadEditor = function (detail, contentList) {
    var self = {
        getData: getData,
        show: show,
        makeSlugReadOnly: makeSlugReadOnly
    },
	    form,
		template = null,
		shouldShow = false;

    construct();
    function construct() {
        form = Html.div();

        self = $.extend(form, self);

        ResourceProvider.getHtml('Content/ImageUploadEditor.html', onHtml);
    }

    function show() {
        if (!template) {
            shouldShow = true;
            return;
        }

        Page.setTitle('CONTENT MANAGEMENT');

        var toolbar = template.toolbar.clone();

        toolbar.find('#cancel').click(close);
        toolbar.find('#saveAsDraft').click(save);

        Page.setContentTitle(toolbar);

        Page.switchPage(self);
    }

    function close() {
        if (contentList) {
            contentList.show(true);
        } else {
            contentList = new Content.Pages.ContentList();
            contentList.show();
        }
    }

    function makeSlugReadOnly() {
        form.find('#newContentSlug').remove();
        form.find('#title').unbind('keyup');
    }

    function save(evt) {
        evt.preventDefault();
        var data = {
            bucketId: detail.Bucket.Id,
            imageId: detail.Id
        };
        Callback.submit({
            service: 'Content/UpdateImageDetail',
            params: data,
            success: close,
            form: self
        });
    }

    function onHtml(html) {

        template = {
            editor: html.find('#image-upload-editor').remove(),
            toolbar: html.find('#editImageToolbar').remove()
        };

        form.append(template.editor.clone());

        form.find('#preview').attr('src', detail.Uri + '.75x75');

        form.find('#title').val(detail.Title);
        form.find('#blurb').val(detail.Blurb);

        if (detail.Tags)
            form.find('#tags').val().split(detail.Tags.join);

        if (detail.Slug) {
            viewSlug();
        }
        else {
            editSlug();
        }

        if (shouldShow)
            show();
    }

    function viewSlug() {
        form.find('#editSlug').remove();
        form.find('#slug').text(detail.Slug);
    }

    function editSlug() {
        form.find('#viewSlug').remove();

        var title = form.find('#title');

        title
            .keypress(function (e) {
                var cursor = Helper.getSelectionStart(e.target);
                // ignore leading whitespace
                if (e.which == 32 && cursor == 0) {
                    e.preventDefault();
                }
            })
	        .keyup(updateSlug);

        var slugButton = form.find('#newContentSlug');

        updateSlug();

        slugButton.click(function () {
            form.find('#slugValue').hide();
            form.find('#slugEdit').show();
            var input = form.find('#slugInputValue');
            input.show();
            input.select();
        });
    }

    function updateSlug() {
        var slug = Helper.getSlug(form.find('#title').val());

        var params = {
            id: detail.Id,
            bucketId: detail.Bucket.Id,
            slug: slug,
            title: form.find('#title').val()
        };

        Callback.submit('Content/GetImageSlug', params, function (data) {
            form.find('#slugValue').text(data.Value);
            form.find('#slugInputValue').val(data.Value);
        });
    }

    function getData() {

        return {
            title: form.find('#title').val(),
            blurb: form.find('#blurb').val(),
            //tags: form.find('#tags').val().split(','), // for when tags are added back in
            slug: form.find('#slugInputValue').val(),
            imageId: detail.Id,
            bucketId: detail.Bucket.Id
        };
    }

    return self;
};