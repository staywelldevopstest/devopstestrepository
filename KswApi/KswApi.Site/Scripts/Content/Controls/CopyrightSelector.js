﻿var Content = Content || {};
Content.Controls = Content.Controls || {};

Content.Controls.CopyrightSelector = function (name) {
	//var DEFAULT_COPYRIGHT = "copyright-default";
	var defaultId;
	var PAGE_SIZE = 100;
	var self = {
		val: val
	};

	var form,
	    editButton,
	    label,
	    list,
	    originalId = null,
	    textValue,
	    defaultValue,
	    values = null,
	    total,
	    elements = {},
	    expanded = false,
		hiddenField;

	construct();

	function construct() {
		form = $('<div>');

		if (!name)
			name = 'copyrightId';

		hiddenField = $('<input type="hidden">').attr('name', name);
		form.append(hiddenField);

		label = $('<div class="itemTitle aligned floatLeft" data-kswid="copyright">Copyright</div>');
		editButton = $('<a class="floatRight defaultButtonSmall defaultWhiteButton defaultButtonBox defaultAnchorbutton contentEditToolBarButton" href="javascript:void(0);" data-kswid="copyrightChange">Edit</a>');
		textValue = $('<div data-kswid="copyrightData">');

		editButton.click(expand);

		list = $('<div class="wrapped">');

		form.append(label);
		form.append(editButton);
		form.append('<div class="clear">');
		form.append(textValue);
		form.append('<div class="clear">');
		form.append(list);
		form.append('<div class="clear">');

		self = $.extend(form, self);
	}

	function createCopyrightOption(id, value, isDefault) {
		var div = $('<div class="clear" data-kswid="copyrightResult">');
		var radio = $('<input type="radio" data-kswid="copyrightSelect">');
		radio.attr('id', id);

		radio.attr('name', name);

		radio.attr('value', id);
		radio.change(function () {
			if (radio.is(':checked')) {
				if (isDefault) {
					label.text('Copyright (default)');
				} else {
					label.text('Copyright');
				}
			}
		});

		div.append(radio);
		var radioLabel = $('<label class="radioLabel" data-kswid="copyrightDesc">').text(value);
		if (isDefault) {
			radioLabel.prepend($('<label class="bold clickable">').text('(default) '));
		}
		radioLabel.attr('for', id);
		div.append(radioLabel);
		return div;
	}

	function expand() {
		expanded = true;
		populate();
	}

	function getData() {
		var offset = 0;
		if (values != null)
			offset = values.length;
		Callback.submit('Administration/GetCopyrights', { offset: offset, count: PAGE_SIZE, includeBucketCount: false }, onData);
	}

	function onData(data) {
		total = data.Total;
		if (values == null) {
			defaultValue = data.Default.Value;
			if (!defaultValue)
				defaultValue = '';
			values = [];

		}

		defaultId = data.Default.Id;
		for (var i = 0; i < data.Items.length; i++)
			values.push(data.Items[i]);

		if (values.length < total && data.Items.length >= PAGE_SIZE)
			getData();
		else
			populate();
	}

	function populate() {
		if (values == null) {
			getData();
			return;
		}

		if (!expanded) {

			if (originalId) {
				for (var i = 0; i < values.length; i++) {
					if (values[i].Id == originalId) {
						setValue(values[i].Id, values[i].Value || '');
						return;
					}
				}
			}

			setValue(null, defaultValue);
			return;
		}

		editButton.hide();

		textValue.empty();

		list.empty();

		if (hiddenField) {
			hiddenField.remove();
			hiddenField = null;
		}

		var option = createCopyrightOption(defaultId, defaultValue, true);

		elements[null] = getRadio(option);

		list.append(option);

		for (i = 0; i < values.length; i++) {
			var current = values[i];
			option = createCopyrightOption(current.Id, current.Value, false);
			elements[current.Id] = getRadio(option);
			list.append(option);
		}

		list.form();

		selectValue(originalId);
	}

	function getRadio(element) {
		return element.find('input[type="radio"]');
	}

	function selectValue(id) {
		var selected = elements[id];
		if (selected)
			selected.prop('checked', true);
	}

	function setValue(id, text) {
		if (expanded) {
			selectValue(id);
			return;
		}

		originalId = id;

		if (typeof text == 'undefined') {
			populate();
		}
		else {
			textValue.text(text);
			var title = 'Copyright';
			if (id == null)
				title = title + ' (default)';
			label.text(title);
		}

		if (hiddenField)
			hiddenField.val(id);
	}

	function val(id, text) {
		if (typeof id != 'undefined') {
			setValue(id, text);
			return self;
		}
		else {
			if (expanded) {
				var value = list.find('input[type="radio"]:checked').attr('id');
				if (!value)
					return null;
				return value;
			}
			else {
				setValue(0);
			}
		}
	}

	return self;
};