﻿var Content = Content || {};
Content.Controls = Content.Controls || {};

Content.Controls.BucketSearchItem = function (content, template, readOnly, onEdit, onDelete) {

    return createBucketSearchItem();

    function createBucketSearchItem(data) {
        var bucket = template.bucketListItemTemplate.clone();

        bucket.find('#name').text(data.Name).click(function () {
            viewBucketDetails(data);
        });

        bucket.find('#description').text(data.Description);
        bucket.find('#type').text(data.TypeDescription);
        bucket.find('#origin').text(data.OriginName);

        var iconName = 'icon' + data.Type;

        bucket.find('#icon').addClass(iconName);

        bucket.find('#delete').click(function () {
            confirmDeleteBucket(data);
        });

        bucket.find('#edit').click(function () {
            editBucket(data);
        });

        bucket.find('#clone').click(function () {
            cloneBucket(data);
        });

        if (data.Segments) {
            bucket.find('#segments').text(data.Segments.length);
        }
        else
            bucket.find('#segmentGroup').remove();

        if (data.LegacyId)
            bucket.find('#legacyId').text(data.LegacyId);
        else
            bucket.find('#legacyIdGroup').remove();

        if (!data.Constraints) {
            bucket.find('#charLimitGroup').remove();
        }
        else {
            if (data.Constraints.Length)
                bucket.find('#charLimit').text(data.Constraints.Length);
            else
                bucket.find('#charLimitGroup').remove();
        }

        return bucket;
    }

    function confirmDeleteBucket(bucket) {
        var popup = new Popup();
        var confirmation = template.bucketListItemTemplate.clone();

        confirmation.find('#name').text(bucket.Name);
        confirmation.find('#cancelButton').click(function () {
            popup.close();
        });
        confirmation.find('#submitButton').click(function () {
            deleteBucket(bucket, popup);
        });
        popup.show(confirmation);
    }
    
    function deleteBucket(bucket, popup) {
        Callback.submit('Content/DeleteBucket', { id: bucket.Id }, function () {
            popup.close();
            if (onDelete)
                onDelete();
        });
    }
    
    function cloneBucket(bucket) {
        // clone the bucket
        var newBucket = $.extend(true, {}, bucket);

        var editor = new Administration.ContentBucketEditor(self, newBucket, true);
        editor.show();
    }

    function editBucket(bucket) {
        var editor = new Administration.ContentBucketEditor(self, bucket);
        editor.show();
    }

    function viewBucketDetails(bucket) {
        var view = Administration.ContentBucketDetailView(self, bucket);
        view.show();
    }
};