﻿var Content = Content || {};
Content.Controls = Content.Controls || {};
Content.Pages = Content.Pages || {};

Content.Controls.CollectionSearchItem = function (owner, collection, template, readOnly, onEdit) {

    return createCollectionSearchItem();

    function createCollectionSearchItem() {
        var item = template.collectionListItem.clone();

        item.find('#name').text(collection.Title).click(function () {
            viewItem(collection);
        });

        // title
        item.find('#name').text(collection.Title);

        // description?
        item.find('#description').text(collection.Description);

        // blurb?
        item.find('#contentListPreviewArea').text(collection.Blurb);

        // thumbnail
        if (collection.ImageUri)
            item.find('#thumbnail')
                .attr('src', collection.ImageUri + ".50x50")
                .attr('alt', collection.Title)
                .click(function () {
                    viewItem(collection);
                });
        else
            item.find('#thumbnail').remove();

        if (readOnly) {
            item.find('#clientUserActions').remove();
        } else {
            item.find('#edit').click(function () {
                viewItem(collection);
            });

            item.find('#clone').click(function () {
                cloneItem(collection);
            });

            item.find('#delete').click(function () {
                deleteItem(collection);
            });
        }

        // metadata 
        var metaDataArea = item.find('#metadataArea');
        var metaTemplate = metaDataArea.find('#metadata').remove();

        var metaLine1 = metaTemplate.clone();
        metaDataArea.append(metaLine1);

        addMetadata(metaLine1, 'Expires', Helper.getFormattedDate(collection.Expires), 'Never', 'expires');
        addMetadata(metaLine1, 'Created', Helper.getFormattedDate(collection.DateAdded), 'N/A', 'created');
        addMetadata(metaLine1, 'Modified', Helper.getFormattedDate(collection.DateModified), 'N/A', 'modified');


        var metaLine2 = metaTemplate.clone();
        metaDataArea.append(metaLine2);
        addMetadata(metaLine2, 'Created by', collection.CreatedBy, 'N/A', 'createdBy');
        addMetadata(metaLine2, 'Used by', collection.LicenseCount + ' License' + (collection.LicenseCount == 1 ? '' : 's'), 'N/A', 'usedBy');


        return item;
    }

    function addMetadata(div, label, value, defaultValue, kswid) {
        if (!div.is(':empty')) {
            div.append(' | ');
        }

        if (value == null)
            value = defaultValue;

        div.append($('<label class="bold">').text(label + ': '));

        if (typeof value == 'string')
            div.append($('<label>').text(value).kswid(kswid));
        else
            div.append(value);
    }

    function deleteItem() {
        var popup = new Popup();
        var dialog = template.deleteConfirmationDialog.clone();
        dialog.find('#type').text('collection');
        dialog.find('#title').text(collection.Title);
        dialog.find('#cancel-delete').click(function () {
            popup.close();
        });
        dialog.find('#confirm-delete').click(function () {
            Service.del({
                service: 'Collections/' + collection.Slug,
                success: function () {
                    owner.refreshCurrentPage();
                    popup.close();
                }
            });
        });
        popup.show(dialog);
    }

    function viewItem() {
        var collectionView = new Content.Pages.CollectionView(Page.getTitle(), collection.Id, owner);
        collectionView.show();
    }

    function cloneItem() {
        var collectionView = new Content.Pages.CollectionView(Page.getTitle(), collection.Id, owner, true);
        collectionView.show();
    }
};