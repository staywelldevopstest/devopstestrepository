﻿var Content = Content || {};
Content.Controls = Content.Controls || {};

Content.Controls.SingleOriginBucketSelector = function (multiSelect, hideFilter, originName, selectedBuckets, allBuckets) {
    var self = {
        getSelected: getSelected,
        getSelectedIds: getSelectedIds,
        clear: clear,
        unselect: unselect,
        focus: focus,
        selectTypes: selectTypes
    },
		listArea,
	    bucketSearchInput,
		appliedQuery,
		expandedArea,
		bucketLookup = {};
    
    construct();

    function construct() {
        selectedBuckets = selectedBuckets || [];
        allBuckets = allBuckets || [];
        appliedQuery = '';

        self = $.extend(Html.div('itemBox'), self);
        self.kswid('origin');

        createExpander().appendTo(self);
        Html.div('floatLeft contentFilterTitle', originName).kswid('originName').appendTo(self);

        expandedArea = Html.div();
        expandedArea.attr('id', 'kswExpansion');

        createQueryBox().appendTo(expandedArea);
        Html.div('clear').appendTo(expandedArea);

        var filters = createFilterTypes();
        Html.div('centered', filters).appendTo(expandedArea);
        Html.div('clear').appendTo(expandedArea);

        listArea = Html.div();

        if (allBuckets)
            populate();
        else
            getBuckets();

        listArea.appendTo(expandedArea);

        expandedArea.appendTo(self);
        Html.div('clear').appendTo(self);
    }

    function clear() {
        listArea.find('.bucketSelect').each(function () {
            var element = $(this);
            if (element.hasClass(bucketState())) {
                unselectBucket(element);
            }
        });
    }

    function unselect(bucketId) {
        var element = bucketLookup[bucketId];
        if (element)
            element.removeClass(bucketState());
    }

    function getBuckets() {
        listArea.empty();
        listArea.append(Html.div('waiting'));
        Callback.submit({
            service: 'Content/Buckets',
            params: { 'readOnly': null, 'origin': null, 'count': 100, 'offset': 0 },
            success: function (data) {
                allBuckets = data.Items;
                populate();
            }
        });
    }

    function populate() {
        listArea.empty();
        bucketLookup = {};
        if (allBuckets.length == 0) {
            listArea.append(Html.div(null, '0 results'));
        }

        for (var bucketIndex = 0; bucketIndex < allBuckets.length; bucketIndex++) {
            addBucket(allBuckets[bucketIndex]);
        }

        updateFiltering();
    }

    function addBucket(bucket) {
        var bucketDiv = Html.div('bucketListItem');
        bucketDiv.attr('id', 'bucketCheckRow');
        bucketDiv.attr('data-bucket-type', bucket.Type);

        var bucketSelect = Html.div('bucketSelect ' + bucketSelector());
        bucketSelect.kswid('bucketSelect');
        bucketSelect.data('bucket', bucket);
        bucketSelect.click(function () { toggleBucketChecked(bucketSelect); });

        bucketLookup[bucket.Id] = bucketSelect;

        if (selectedBuckets.contains(bucket.Id)) {
            bucketSelect.addClass(bucketState());
        }
        bucketSelect.appendTo(bucketDiv);

        var bucketName = Html.div('bucketName bucketListPadding clickable');
        bucketName.text(bucket.Name);
        bucketName.attr('title', bucket.Name);
        bucketName.kswid('bucketName');
        bucketName.click(function () {
            toggleBucketChecked(bucketSelect);
        });

        bucketName.appendTo(bucketDiv);

        bucketDiv.appendTo(listArea);
    }

    //Expander
    function createExpander() {
        var expansionState = 'expanded';

        var expander = Html.div('floatLeft contentFilterIcon iconExpandedBig clickable').kswid('originExpand');
        expander.click(function () {
            if (expansionState == 'expanded') {
                expansionState = 'collapsed';
                expander.removeClass('iconExpandedBig');
                expander.addClass('iconCollapsedBig');
                expandedArea.hide();
            } else {
                expansionState = 'expanded';
                expander.addClass('iconExpandedBig');
                expander.removeClass('iconCollapsedBig');
                expandedArea.show();
            }
        });

        return expander;
    }

    function selectAll() {
        listArea.find('.bucketSelect').each(function () {
            selectBucket($(this));
        });
    }

    function deselectAll() {
        listArea.find('.bucketSelect').each(function () {
            unselectBucket($(this));
        });
    }

    //Bucket filter by query
    function createQueryBox() {
        var queryArea = Html.div('floatRight');

        var searchTextArea = Html.div('searchTextArea');
        bucketSearchInput = Html.textBox('searchTextBox').kswid('inputSearch');
        bucketSearchInput.attr('id', 'bucketSearchInput');
        bucketSearchInput.enter(applyQuery);
        bucketSearchInput.appendTo(searchTextArea);
        searchTextArea.appendTo(queryArea);

        var bucketSearchButton = Html.button('defaultButton searchButton', '');
        bucketSearchButton.kswid('buttonSearch');
        queryArea.append(bucketSearchButton);
        bucketSearchButton.click(applyQuery);

        return queryArea;
    }

    function applyQuery() {
        appliedQuery = bucketSearchInput.val();
        updateFiltering();
    }

    function filterByQuery() {
        listArea.find('div.bucketListItem').each(function () {
            var element = $(this);
            var bucketName = element.find('div.bucketName').text();

            if (!bucketName.toUpperCase().contains(appliedQuery.toUpperCase())) {
                element.hide();
            }
        });
    }

    //Bucket filter by types
    function createFilterTypes() {
        var bucketTypeFilterArea = Html.div('bitOSpace');
        bucketTypeFilterArea.attr('id', 'bucketTypeFilterArea');
        bucketTypeFilterArea.css({
            'margin-left': 'auto',
            'margin-right': 'auto',
            'display': 'inline-block'
        });

        var showDiv = Html.div('floatLeft dataTitle', 'Show:');

        bucketTypeFilterArea.append(Html.breakDiv(), $('<br>'), showDiv);

        appendBucketFilterType(bucketTypeFilterArea, 'Article', 'articleBucketType', 'iconArticle');
        appendBucketFilterType(bucketTypeFilterArea, 'Plain Text', 'plainTextBucketType', 'iconText');
        appendBucketFilterType(bucketTypeFilterArea, 'Video', 'videoBucketType', 'iconVideo');
        appendBucketFilterType(bucketTypeFilterArea, 'Image', 'imageBucketType', 'iconImage');

        bucketTypeFilterArea.append(Html.breakDiv(), $('<br>'));

        if (hideFilter)
            bucketTypeFilterArea.hide();

        return bucketTypeFilterArea;
    }

    function appendBucketFilterType(bucketTypeFilterArea, displayText, id, iconClass) {
        var bucketType = Html.div('floatLeft defaultCheckBox').kswid(id);
        bucketType.attr('id', id);
        bucketType.click(toggleBucketTypeChecked);

        var bucketTypeIcon = Html.labelBinding('floatLeft bucketFilterTypeIconMargin clickable', '', bucketType);
        bucketTypeIcon.addClass(iconClass);

        var bucketTypeLabel = Html.labelBinding('floatLeft defaultCheckBoxLabel clickable', displayText, bucketType);

        bucketTypeIcon.appendTo(bucketTypeFilterArea);
        bucketType.appendTo(bucketTypeFilterArea);
        bucketTypeLabel.appendTo(bucketTypeFilterArea);
    }

    function toggleBucketTypeChecked() {
        var element = $(this);
        if (element.hasClass('checked')) {
            unselectBucketType(element);
        }
        else {
            selectBucketType(element);
        }
    }

    function selectTypes(types) {
        var boxes = self.find('#bucketTypeFilterArea .defaultCheckBox');
        boxes.removeClass('checked');

        for (var i = 0; i < types.length; i++) {
            switch (types[i]) {
                case 'Image':
                    boxes.filter('#imageBucketType').addClass('checked');
                    break;
                case 'Video':
                    boxes.filter('#videoBucketType').addClass('checked');
                    break;
                case 'Article':
                    boxes.filter('#articleBucketType').addClass('checked');
                    break;
                case 'PlainText':
                    boxes.filter('#plainTextBucketType').addClass('checked');
                    break;
            }
        }

        updateFiltering();
    }

    function unselectBucketType(bucketType) {
        bucketType.removeClass('checked');
        updateFiltering();
    }

    function selectBucketType(bucketType) {
        bucketType.addClass('checked');
        updateFiltering();
    }

    function filterByType() {
        var typeFilters = self.find('#bucketTypeFilterArea');

        var filterTypes = [];

        if (!typeFilters.find('#articleBucketType').hasClass('checked')) {
            filterTypes.push("Article");
        }

        if (!typeFilters.find('#plainTextBucketType').hasClass('checked')) {
            filterTypes.push("Text");
        }

        if (!typeFilters.find('#videoBucketType').hasClass('checked')) {
            filterTypes.push("Video");
        }

        if (!typeFilters.find('#imageBucketType').hasClass('checked')) {
            filterTypes.push("Image");
        }

        var filtersChecked = filterTypes.length;
        if (filtersChecked < 4) {
            for (var i = 0; i < filtersChecked; i++) {
                listArea.find('div[data-bucket-type="' + filterTypes[i] + '"]').each(function () {
                    var element = $(this);
                    element.hide();
                });
            }
        }
    }

    //UI updating
    function updateFiltering() {
        listArea.find('div.bucketListItem').show();
        filterByType();
        filterByQuery();
    }

    //Bucket selection functions
    function toggleBucketChecked(element) {
        if (element.hasClass(bucketState())) {
            if (multiSelect) {
                unselectBucket(element);
            }
        }
        else {
            selectBucket(element);
        }
    }

    function unselectBucket(element) {
        var state = bucketState();
        if (element.hasClass(state)) {
            element.removeClass(state);
            self.trigger('selectionChanged', [element.data('bucket'), false]);
        }
    }

    function selectBucket(element) {
        var bucketDisplay = element.parent().css('display');
        if (bucketDisplay != 'none') {
            if (!multiSelect) {
                deselectAll();
            }

            var state = bucketState();
            if (!element.hasClass(state)) {
                element.addClass(state);
                self.trigger('selectionChanged', [element.data('bucket'), true]);
            }
        }
    }

    function bucketState() {
        return multiSelect ? 'checked' : 'radioButtonSelected';
    }

    function bucketSelector() {
        return multiSelect ? 'defaultCheckBox' : ' defaultRadioButton aligned';
    }

    //Public functions
    function getSelectedIds() {
        var newBucketSelection = [];
        listArea.find('.bucketSelect').each(function () {
            var element = $(this);
            if (element.hasClass(bucketState())) {
                newBucketSelection.push(element.data('bucket').Id);
            }
        });

        return newBucketSelection;
    }

    function getSelected() {
        var newBucketSelection = [];
        listArea.find('.bucketSelect').each(function () {
            var element = $(this);
            if (element.hasClass(bucketState())) {
                newBucketSelection.push(element.data('bucket'));
            }
        });

        return newBucketSelection;
    }

    function focus() {
        bucketSearchInput.select();
    }

    return self;
};