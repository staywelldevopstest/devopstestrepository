﻿var Content = Content || {};
Content.Controls = Content.Controls || {};

Content.Controls.ImagePopup = function (multiSelect, hideFilter, hideThumbnailSizeOptions) {
	var self = {
		show: show
	};

	var popup, container, bucket;

	construct();

	function construct() {
		container = Html.div();
		self = $.extend(container, self);
	}

	function show() {
		container.empty();

		var wizard = new Content.Controls.BucketWizard(null, null, hideFilter);
		wizard.selectTypes(['Image']);
		wizard.on('cancel', close);
		wizard.on('submit', function (evt, data) {
			evt.stopPropagation();
			onBucketSelected(data);
		});

		container.append(wizard);

		popup = new Popup();
		popup.show(container, { classes: 'full', wide: true });
	}

	function close() {
		popup.close();
	}

	function onBucketSelected(data) {
		bucket = data;
		container.empty();
		var tab = new Tab();
		tab.addTab('Select an image', showImageSelector);
		// this tab to be added in a future iteration
		// tab.addTab('Upload an Image', showImageUploader);
		container.append(tab);
	}

	function showImageSelector(tab) {
		var div = Html.div();
		var closeButton = Html.button('defaultButtonSmall defaultWhiteButton', 'CANCEL').kswid('cancel');
		closeButton.click(close);
		var continueButton = Html.button('defaultButton defaultButtonSmall disabled', 'INSERT SELECTED IMAGES');
		var actionBar = Html.div('bottomActionBar', Html.div('right', closeButton, continueButton));
		var imageSelector = new Content.Controls.ImageSelector(bucket, tab, multiSelect, hideThumbnailSizeOptions);

		imageSelector.on('change', function (evt, images) {
			if (images.length == 0)
				continueButton.addClass('disabled');
			else
				continueButton.removeClass('disabled');
		});

		continueButton.click(function () {
			var images = imageSelector.getImages();
			
			if (images.length == 0)
				return;
			
			close();
			
			self.trigger('submit', [images]);
		});

		div.append(imageSelector, actionBar);

		tab.setContent(div);
	}

	function getImages() {

	}

	function showImageUploader(tab) {
		// add upload stuff here
		var div = Html.div();

		tab.setContent(div);
	}

	return self;
};
