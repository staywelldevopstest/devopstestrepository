﻿$.namespace('Content.Controls');

Content.Controls.CollectionSelector = function () {
	var self = {
		show: show,
		getSelected: getSelected,
		onSubmit: onSubmit
	};

	var _form, _popup, _pager, _template, _searchTerms = {}, _selectedLookup = {}, _onSubmit = [], _selected = [];

	function show(previouslySelected) {
		_selectedLookup = {};
		if (previouslySelected) {
			for (var i = 0; i < previouslySelected.length; i++) {
				_selectedLookup[previouslySelected[i]] = true;
			}
		}
		$.Deferred(getHtml)
			.done(setup);
	}

	function getHtml(deferred) {
		if (_template) {
			deferred.resolve();
			return;
		}

		ResourceProvider.getHtml('Content/CollectionSelector.html', function (data) {
			setupTemplate(data);
			deferred.resolve();
		});
	}
	
	function onSubmit() {
		for (var i = 0; i < arguments.length; i++) {
			if (typeof arguments[i] == 'function')
				_onSubmit.push(arguments[i]);
		}
	}

	function setupTemplate(data) {
		_template = {
			form: data.find('#form').remove(),
			item: data.find('#item-template').remove()
		};
	}

	function getCollections(page, count, offset) {
		Service.get({
			service: 'Collections',
			data: {
				$skip: offset,
				$top: count,
				query: _searchTerms.query
			},
			success: onCollections
		});
	}

	function onCollections(data) {

		_pager.updatePager(data.Total);
		
		var list = _form.find('#list');

		list.empty();
		
		for (var i = 0; i < data.Items.length; i++) {
			list.append(createItem(data.Items[i]));
		}
	}
	
	function createItem(data) {
		var element = _template.item.clone();

		element.find('#title').text(data.Title).click(function () {
			var returnPath = Navigation.get();
			cancel();
			var owner = {
				show: function () {
					Navigation.goto.apply(null, returnPath);
				}
			};
			var collectionView = new Content.Pages.CollectionView(Page.getTitle(), data.Id, owner);
			collectionView.show();
			return;
		});

		element.find('#blurb').text(data.Description);
		element.find('#expires').text(data.Expires ? Helper.getFormattedDate(data.Expires) : 'Never');
		element.find('#created').text(Helper.getFormattedDate(data.DateAdded));
		element.find('#modified').text(Helper.getFormattedDate(data.DateModified));
		element.find('#created-by').text(data.CreatedBy.FullName);
		if (data.ImageUri) {
			element.find('#image').attr('src', data.ImageUri + '.35x35');
		}
		
		if (_selectedLookup[data.Id]) {
			element.find('#select').prop('disabled', true);
			element.find('#already-selected').show();
		}
		else {
			element.find('#select').change(function() {
				if ($(this).is(':checked')) {
					_selected.push(data);
				} else {
					_selected.remove(function(item) {
						return item.Id == data.Id;
					});
				}
			});
		}

		element.form();
		return element;
	}

	function setup() {
		_form = _template.form.clone();

		_form.find('#cancel').click(cancel);
		_form.find('#submit').click(submit);
		
		_pager = new QueriedPager({
			pageNavigationCallback: getCollections
		});
		
		_form.find('#pager-container').append(_pager);
		_form.find('#search-query').enter(search);
		
		_popup = new Popup();
		_popup.show(_form, { wide: true });

		_pager.refresh();
	}
	
	function search() {
		_searchTerms.query = _form.find('#search-query').val();
		_pager.refresh();
	}
	
	function cancel() {
		if (_popup) {
			_popup.close();
			_popup = null;
		}
	}
	
	function submit() {
		for (var i = 0; i < _onSubmit.length; i++)
			_onSubmit[i](self);

		if (_popup) {
			_popup.close();
			_popup = null;
		}
	}
	
	function getSelected() {
		return _selected;
	}

	return self;
};
