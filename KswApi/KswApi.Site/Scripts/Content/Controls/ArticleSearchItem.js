﻿var Content = Content || {};
Content.Controls = Content.Controls || {};

Content.Controls.ArticleSearchItem = function (content, template, readOnly, onEdit, onDelete) {

	// define and return self instead?
	return createArticleItem();


	// self
	//var self = {
	//    createArticleItem: createArticleItem,
	//};

	function createArticleItem() {
		var element = template.contentListItem.clone();

		element.find('#contentDataTitle').text(content.Title).click(function () {
			editContent(content);
		});

		var draft = $('<a href="javascript:;" class="clickable">Draft</a>');
		draft.click(function () {
			editContent(content);
		});

		var revisionArea = element.find('#revisions');

		revisionArea.append(draft);

		element.find('#contentListPreviewArea').text(content.Blurb);

		if (readOnly) {
			element.find('#clientUserActions').remove();
		} else {
			element.find('#contentEditAction').click(function () {
				editContent(content);
			});

			element.find('#contentDeleteAction').click(function () {
				deleteContent(content);
			});
		}

		var metadata = element.find('#metadata1');

		addContentMetadata(metadata, 'Bucket', content.Bucket.Name, 'bucket');
		addContentMetadata(metadata, 'Origin', content.OriginName, 'origin');
		addContentMetadata(metadata, 'Languages', getLanguages(content), 'languages');
		addContentMetadata(metadata, 'Age Range', Helper.getAgeRange(content.AgeCategories), 'ageRange');

		metadata = element.find('#metadata2');
		if (content.LastReviewedDate)
			addContentMetadata(metadata, 'Last Reviewed', Helper.getFormattedDate(content.LastReviewedDate), 'lastReview');
		addContentMetadata(metadata, 'Created', Helper.getFormattedDate(content.DateAdded), 'created');
		addContentMetadata(metadata, 'Modified', Helper.getFormattedDate(content.DateModified), 'modified');


		// add published when finished

		return element;
	}

	function addContentMetadata(div, label, value, kswid) {
		if (!div.is(':empty')) {
			div.append(' | ');
		}

		div.append($('<label class="bold">').text(label + ': '));

		if (typeof value == 'string')
			div.append($('<label>').text(value).kswid(kswid));
		else
			div.append(value);
	}

	function getLanguages() {
		var label = $('<label>');

		for (var i = 0; i < content.Versions.length; i++) {
			if (i != 0)
				label.append(', ');
			var item = content.Versions[i];

			label.append(createLanguageLink(content, item));
		}
		return label;
	}

	function createLanguageLink(item) {
		var text = Helper.getFormattedLanguageCode(item.Language.Code);
		if (item.Master)
			text = '*' + text;
		return Html.anchor(null, text).kswid('languageItem').click(function () {
		    var newEditor = new Content.Pages.ContentView(item.Id, content.Bucket.Id, self, null, onContentViewClose);
			newEditor.show();
		});
	}

	function deleteContent() {
		var popup = new Popup();
		var dialog = template.deleteContentDialog.clone();
		dialog.find('#type').text(content.Type.toLowerCase());
		dialog.find('#deleteContentName').text(content.Title);
		dialog.find('#deleteContentCancel').click(function () {
			popup.close();
		});
		dialog.find('#deleteContentConfirm').click(function () {
			Service.del({
				service: 'Content/' + content.Bucket.Id + '/' + content.Id,
				success: function () {

					//controls.pager.refreshCurrentPage();
					if (onDelete)
						onDelete();

					popup.close();
				}
			});
		});
		popup.show(dialog);
	}

	function editContent() {
	    var contentView = new Content.Pages.ContentView(content.Id, content.Bucket.Id, self, null, onContentViewClose);
		contentView.show();
	}

    function onContentViewClose() {
        //
    }

//return self;
};