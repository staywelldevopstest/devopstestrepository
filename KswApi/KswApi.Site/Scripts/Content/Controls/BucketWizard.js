﻿var Content = Content || {};
Content.Controls = Content.Controls || {};

// triggers events:
// 'selectionChanged' (evt, bucket, selected)
// 'submit' (evt, bucket)
// 'cancel' (evt)

Content.Controls.BucketWizard = function (buckets, selectedBuckets, hideFilter) {
	var self = {
		focus: focus,
		selectTypes: selectTypes
	};

	var bucketSelector;

	construct();
	
	function construct() {
	    bucketSelector = new Content.Controls.BucketSelector(false, buckets, selectedBuckets, hideFilter);

		var closeButton = Html.button('defaultButtonSmall defaultWhiteButton', 'CANCEL').kswid('cancel');
		var continueButton = Html.button('defaultButton defaultButtonSmall disabled', 'USE BUCKET AND CONTINUE').kswid('continue');

		closeButton.click(cancel);

		var enabled = false;

		bucketSelector.on('selectionChanged', function (evt, bucket, selected) {
			if (!enabled && selected) {
				continueButton.removeClass('disabled');
				continueButton.click(submit);
				enabled = true;
			}
		});

		var actionBar = Html.div('bottomActionBar', Html.div('right', closeButton, continueButton));

		var bucketSelectorElement = Html.div('itemBox dark',
			Html.div('',
				Html.div('bucketSelectorTitle', 'SELECT YOUR BUCKET'),
				bucketSelector,
				actionBar
			)
		);

		self = $.extend(bucketSelectorElement, self);
	}
	
	function selectTypes(types) {
		bucketSelector.selectTypes(types);
	}
	
	function submit(evt) {
		evt.preventDefault();
		self.trigger('submit', bucketSelector.getSelected().firstOrNull());
	}

	function cancel(evt) {
		evt.preventDefault();
		self.trigger('cancel');
	}
	
	function focus() {
		bucketSelector.focus();
	}

	return self;
}