﻿var Content = Content || {};
Content.ImageUploadProgress = function (imageId) {
	var self = {
		view: view,
		progressChanged: progressChanged,
		id: id,
		remove: remove,
		showError: showError
	},
		container,
		progressBar,
		progressCaption;

	function view() {
		container = Html.div('itemBox');
		progressBar = Html.div('');
		progressCaption = $('<span></span>');
		progressCaption.appendTo(progressBar);
		progressBar.appendTo(container);

		progressChanged();

		return container;
	}

	function progressChanged(status, bytesCompleted, bytesTotal) {
		var captionText = "Waiting to upload, please wait...";

		switch (status) {
			case plupload.QUEUED:
				captionText = "Queued for upload, please wait...";
				break;
			case plupload.UPLOADING:
				captionText = "Uploading Progress: " + (100.0 * bytesCompleted / bytesTotal);
				break;
			case plupload.DONE:
				captionText = "Upload completed";
				break;
		}

		progressCaption.text(captionText);
	}

	function id() {
		return imageId;
	}

	function remove() {
		container.detach();
	}

	function showError(error) {
		container.addClass('error');
		progressCaption.text(error);
	}

	return self;
};