﻿var Content = Content || {};
Content.Controls = Content.Controls || {};
Content.Controls.ImageSelector = function (bucket, tab, multiSelect, hideThumbnailSizeOptions) {

    var self = {
        getImages: getImages
    };

    var selectedArray = [];
    var typeArray = [];

    var list, options, template, pager, searchBox;

    construct();

    function construct() {
        list = Html.div();
        var div = Html.div(null, list);
        self = $.extend(div, self);

        getHtml();
    }

    function getHtml() {
        ResourceProvider.getHtml('Content/ImageList.html', onHtml);
    }

    function onHtml(html) {
        template = {
            imageSettings: html.find('#imageSettings').remove(),
            options: html.find('#imageListOptions').remove(),
            imageItem: html.find('#selectImageTemplate').remove(),
            singleSelectImageItem: html.find('#selectSingleImageTemplate').remove(),
            search: html.find('#searchBar').remove()
        };

        options = template.options.clone();

        options.form();

        // this needs to follow the form() call to prevent multiple updates
        options.find('input[type="checkbox"]').change(function (evt) {
            evt.stopPropagation();
            var current = $(this);
            if (current.is(':checked')) {
                typeArray.push(current.val());
            }
            else {
                typeArray.remove(current.val());
            }

            search();
        });

        if (hideThumbnailSizeOptions)
            options.find('#thumbnailSizeContainer').hide();

        self.append(Html.div('split right', options));

        list = Html.div('split wide-popup');

        list.change(function (evt) { evt.stopPropagation(); });

        self.find('[name="thumbnailSize"]')
			.change(changeThumbnailSize);

        pager = new QueriedPager({
            itemsPerPage: 10,
            maxVisiblePages: 10,
            pageNavigationCallback: getBucketImages
        });

        self.append(list, pager);

        if (tab) {
            var searchBar = template.search.clone();
            searchBar.form();
            tab.setTitleArea(searchBar);
            searchBox = searchBar.find('#searchTextBox');
            searchBox.enter(search);
            searchBar.find('#searchButton').click(search);
            searchBox.select();
        }

        pager.refresh();
    }

    function changeThumbnailSize(evt) {
        evt.stopPropagation();
        var size = getSize();
        list.children().each(function () {
            var current = $(this).find('#image');
            var data = current.data('uri');
            if (!data)
                return;
            current.attr('src', data + '.' + size);
        });
    }

    function getBucketImages(page, itemsPerPage, offset) {

        var data = {
            buckets: [bucket.Id],
            '$top': itemsPerPage,
            '$skip': offset,
            types: ['Image']
        };

        if (typeArray.length > 0) {
            data.formats = [];
            for (var i = 0; i < typeArray.length; i++)
                data.formats.push(typeArray[i].split(','));
        }

        if (searchBox)
            data.query = searchBox.val();

        Service.get({
            service: 'Content',
            data: data,
            success: onImages
        });
    }

    function getSize() {
        return self.find('[name="thumbnailSize"]:checked').val();
    }

    function onImages(images) {
        list.empty();
        pager.updatePager(images.Total);
        var size = getSize();
        for (var i = 0; i < images.Items.length; i++) {
            list.append(createItem(images.Items[i], size));
        }

        list.form();

        if (!images.TypeCounts)
            return;

        var total = 0;
        var count = images.TypeCounts.Types['Jpg'] || 0;
        options.find('[for="jpg"]').text('JPG (' + count + ')');
        total += count;

        count = images.TypeCounts.Types['Png'] || 0;
        options.find('[for="png"]').text('PNG (' + count + ')');
        total += count;

        count = images.TypeCounts.Types['Tiff'] || 0;
        options.find('[for="tiff"]').text('TIFF (' + count + ')');
        total += count;

        count = images.TypeCounts.Total - total;
        options.find('[for="other"]').text('Other (' + count + ')');
    }

    function createItem(image, size) {

        var element = null;

        if (multiSelect)
            element = template.imageItem.clone();
        else
            element = template.singleSelectImageItem.clone();


        element.find("#select").val(image.Id);
        element.find('#select').change(function (evt) {
            evt.stopPropagation();
            updateSelection($(this), image, element);
        });

        var imageElement = element.find('#image');

        imageElement.data('uri', image.Uri);
        imageElement.attr('src', image.Uri + '.' + size);

        element.find('#summary').text(image.Blurb);
        element.find('#title').text(image.Title);

        element.find('#type').text('(' + image.Format.toUpperCase() + ')');

        var meta = element.find('#metadata');

        addMeta(meta, 'Created', Helper.getFormattedDate(image.DateAdded), 'created');
        addMeta(meta, 'Modified', Helper.getFormattedDate(image.DateModified), 'modified');

        //element.form();
        return element;
    }

    function addMeta(div, label, value, kswid) {
        if (!div.is(':empty')) {
            div.append(' | ');
        }

        div.append($('<label class="bold">').text(label + ': '));

        if (typeof value == 'string')
            div.append($('<label>').text(value).kswid(kswid));
        else
            div.append(value);
    }

    function getImages() {
        return selectedArray;
    }

    function updateSelection(checkbox, image, element) {
        // radiobutton: checked?
        if (checkbox.is(':checked')) {

            // TODO: make this a separate option
            if (multiSelect) {
                var settingsElement = createImageSettings(checkbox, image, element);
                settingsElement.hide();
                element.find('#imageSettingContainer').append(settingsElement);
                settingsElement.slideDown(250);
            }

            selectedArray.push(image);
        }
        else {
            element.find('#imageSettings').slideUp(250, function () {
                $(this).remove();
            });

            selectedArray.removeAll(function (item) { return item.Id == image.Id; });
        }

        self.trigger('change', [selectedArray]);
    }

    function createImageSettings(checkbox, image) {
        var element = template.imageSettings.clone();

        // for the size selector
        var custom = element.find('#custom');
        setupSettingRadios(element, image, ['thumb', 'small', 'medium', 'large', 'original', 'custom']);
        var widthElement = element.find('#width'),
		    heightElement = element.find('#height');

        element.find('#originalLabel').text('Original(' + image.Width + 'x' + image.Height + ')');

        widthElement.val(image.Width).change(function (evt) {
            evt.stopPropagation();
            var value = parseInt($(this).val());
            if (!value || value <= 0) {
                var warning = new Warning('Invalid width.');
                warning.show(3500);
                widthElement.val(image.Width);
            }

            var sizeValue = widthElement.val() + 'x' + heightElement.val();

            custom.val(sizeValue);

            if (custom.is(':checked'))
                image.Size = sizeValue;
        });

        heightElement.val(image.Height).change(function (evt) {
            evt.stopPropagation();
            var value = parseInt($(this).val());
            if (!value || value <= 0) {
                var warning = new Warning('Invalid height.');
                warning.show(3500);
                heightElement.val(image.Height);
            }

            var sizeValue = widthElement.val() + 'x' + heightElement.val();

            custom.val(sizeValue);

            if (custom.is(':checked'))
                image.Size = sizeValue;
        });

        element.form();

        return element;
    }

    function search() {
        pager.refresh();
    }

    function setupSettingRadios(element, image, types) {
        for (var i = 0; i < types.length; i++) {
            var type = types[i];
            var current = element.find('#' + type);
            current.attr('id', type + '-' + image.Id);
            current.attr('name', 'size-' + image.Id);
            current.change(function (evt) {
                evt.stopPropagation();
                if (!$(this).is(':checked'))
                    return;
                image.Size = $(this).val();
            });
            element.find('[for="' + type + '"]').attr('for', type + '-' + image.Id);
        }
    }

    return self;
};
