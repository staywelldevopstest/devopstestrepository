﻿var Content = Content || {};
Content.Factories = Content.Factories || {};

Content.Factories.MetadataFactory = function (template) {
	var self = {
		getOnlineEditorsView: getOnlineEditorsView,
		getOnlineEditors: getOnlineEditors
	};


	function getOnlineEditorsView(editors) {
		var div = template.find('#onlineEditorsView').clone();

		if (!editors || editors == 0)
			return null;

		var container = view.find('#onlineEditors');
		for (var i = 0; i < content.OnlineEditors.length; i++) {
			container.append($('<div>').text(content.OnlineEditors[i]));
		}

		return div;
	}

	function getOnlineEditors(editors) {
		var div = template.find('#onlineEditors').clone();

		var container = view.find('#onlineEditors');
		for (var i = 0; i < content.OnlineEditors.length; i++) {
			container.append($('<div>').text(content.OnlineEditors[i]));
		}

		return div;
	}

	return self;
};