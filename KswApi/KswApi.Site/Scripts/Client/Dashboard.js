﻿var Client = Client || {};
Client.Pages = Client.Pages || {};

Client.Pages.Dashboard = function () {
	var form;

	function show() {
		ResourceProvider.getClientDashboard(function(data) {
			form = data;

			Page.setTitle('DASHBOARD');
			Page.setContentTitle('');
			Page.switchPage(form);
		});
	}

	return {
		show: show
	};
};