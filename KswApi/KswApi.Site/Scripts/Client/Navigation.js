﻿var Client = Client || {};

Client.Navigation = (function() {
	var self = {
		hasPermission: hasPermission,
		initialize: initialize,
		showContent: showContent,
		showProducts: showProducts,
		showLogs: showLogs,
		showEditUser: showEditUser,
		showHome: showHome,
		show: show
	};

    var deferred = $.Deferred();
	var callbacks = {};
	var menu = new Menu();
	var appSettings = new Client.AppSettings();
    
	function showCallback(event) {
	    var value = callbacks[event.data];
	    if (value)
	        value.apply(this, value);
	}
    
	function show(name, args) {
		if (!name) {
			showHome.apply(this, args);
			return;
		}

		var value = callbacks[name];
		if (value)
			value.apply(this, args);
	}

	function initialize() {
		callbacks['dashboard'] = showHome;
		callbacks['content'] = showContent;
		callbacks['products'] = showProducts;
		callbacks['logs'] = showLogs;

		var dashboardMenu = new MenuItem();
		var contentMenu = new MenuItem();
		var productsMenu = new MenuItem();
		var logsMenu = new MenuItem();

		//Gear - User Settings menu
		var editUserMenu = new MenuItem();

		editUserMenu.SetCaption('Edit Profile');
		editUserMenu.SetDataKswId('editUser');
		editUserMenu.SetIsUserMenu(true);
		editUserMenu.SetRequiredRight('show_always');
		editUserMenu.SetCallBack(function () { showEditUser(); });

		dashboardMenu.SetCaption('DASHBOARD');
		dashboardMenu.SetDataKswId('dashboard');
		dashboardMenu.SetRequiredRight('show_always');
		dashboardMenu.SetCallBack(function () { showHome(); });

		contentMenu.SetCaption('CONTENT');
		contentMenu.SetDataKswId('content');
		contentMenu.SetRequiredRight('show_always');
	    contentMenu.SetCallBack(function() { showContent(); });

		productsMenu.SetCaption('PRODUCTS');
		productsMenu.SetDataKswId('products');
		productsMenu.SetRequiredRight('show_always');
		productsMenu.SetCallBack(function() { showProducts(); });

		logsMenu.SetCaption('LOGS');
		logsMenu.SetDataKswId('logs');
		logsMenu.SetRequiredRight('show_always');
		logsMenu.SetCallBack(function () { showLogs(); });

		var menuItems = [dashboardMenu, contentMenu, productsMenu, logsMenu, editUserMenu];

		var menuPromise = menu.initMenu(menuItems, showCallback);
        var settingsPromise = appSettings.init();

        $('.mainContent').css('top', '155px');

        $.when(menuPromise, settingsPromise).done(function () {
	        deferred.resolve();
        });
	    
	    return deferred.promise();
	}

    function showHome() {
		if (!Global.User)
			return;

		Navigation.set('dashboard');

		var dashboard = Client.Pages.Dashboard();
		dashboard.show();
		appSettings.resetAppSettingsBar();
	}
	
	function showContent() {
	    Navigation.set('content');
	    appSettings.resetAppSettingsBar();
	}
	
	function showProducts() {
	    Navigation.set('products');
	    appSettings.resetAppSettingsBar();
	}
	
	function showLogs() {
	    Navigation.set('logs');
	    appSettings.resetAppSettingsBar();
	}
	
	function showEditUser() {
		Navigation.set('editUser');

		var editUser = new ClientUser.UserEditor();
		editUser.show();
	}

	function allow(right) {
		if (Global.User && Global.User.Rights.contains(right))
			return true;

		location.href = Global.LoginPath;
		return false;
	}

	function hasPermission(right) {
		return Global.User && Global.User.Rights.contains(right);
	}

	return self;
})();
