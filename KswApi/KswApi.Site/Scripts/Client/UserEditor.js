﻿var ClientUser = ClientUser || { };

ClientUser.UserEditor = function () {
	var self = {
			show: show
		},
	    form;
	
	function show() {
		Callback.submit(
			'User/GetUserSettings',
			{ },
			function (data) {
				var firstName = Html.textBox('defaultTextBox', 'firstName', data.FirstName),
				    lastName = Html.textBox('defaultTextBox', 'lastName', data.LastName),
				    emailAddress = Html.textBox('defaultTextBox', 'emailAddress', data.EmailAddress),
				    phoneNumber = Html.textBox('defaultTextBox', 'phoneNumber', data.PhoneNumber);

				form =
					Html.div('listContentArea userSettingsContainer',
						Html.div('detailContentArea',
							Html.div('userSettings itemBox',
							Html.labelBinding('itemSubTitleSmall userSettingsLabel', 'First Name', firstName),
							Html.labeledField('userSettingsTextField', 'inlineLabel', 'First Name...', firstName),
							Html.labelBinding('itemSubTitleSmall userSettingsLabel', 'Last Name', lastName),
							Html.labeledField('userSettingsTextField', 'inlineLabel', 'Last Name...', lastName),
							Html.breakDiv(),
							Html.labelBinding('itemSubTitleSmall userSettingsLabel', 'Email Address', emailAddress),
							Html.labeledField('userSettingsTextField', 'inlineLabel', 'Email Address...', emailAddress),
							Html.labelBinding('itemSubTitleSmall userSettingsLabel', 'Phone Number', phoneNumber),
							Html.labeledField('userSettingsTextField', 'inlineLabel', 'Phone Number...', phoneNumber),
							Html.breakDiv(),
							Html.div('right',
								Html.anchorButton('defaultButton defaultButtonSmall', 'Save', onSaveClick)
							),
							Html.breakDiv()
						)
					)
				);

				Page.setTitle('EDIT PROFILE');
				Page.setContentTitle('');
				Page.switchPage(form);
			}
		);
	}
	
	function onSaveClick() {
		var firstName = form.find('input[name="firstName"]').val(),
			lastName = form.find('input[name="lastName"]').val(),
			emailAddress = form.find('input[name="emailAddress"]').val(),
			phoneNumber = form.find('input[name="phoneNumber"]').val();

		Callback.submit(
			'User/UpdateUserSettings',
			{
				'firstName': firstName,
				'lastName': lastName,
				'emailAddress': emailAddress,
				'phoneNumber': phoneNumber,
				'notes': ''
			},
			function (data) {
				Global.User.FirstName = firstName;
				Global.User.LastName = lastName;
				Global.User.EmailAddress = emailAddress;
				Global.User.FullName = firstName + " " + lastName;

				$('div[data-kswid="settingsUser"]').text(Global.User.FullName);

				Client.Navigation.showHome();
			}
		);
	}

	function onSaveSuccess() {
		
	}

	return self;
}