﻿var Client = Client || {};

Client.AppSettings = function () {
	var self = {
			'init': init,
			'getSelectedLicenseId': getSelectedLicenseId,
			'getSelectedApplicationId': getSelectedApplicationId,
			resetAppSettingsBar: resetAppSettingsBar

		},
	    form,
	    keyContainer,
		apiKey,
	    apiKeyReadOnly,
		apiKeyRevealed,
		apiKeyContainer,
		secretKey,
		secretKeyReadOnly,
		secretKeyRevealed,
	    secretKeyContainer,
		licenses = null,
	    licenseOptions,
	    licenseDropDown,
		applicationDropDownContainer,
		applicationDropDown,
		viewSecretButton,
	    viewApiButton,
		resetSecretButton,
		resetSecretDialog,
		resetSecretPopup;

    var deferred = $.Deferred();

	function init() {
		form = $('.mainHeaderWhiteSeperator');
		form.css('height', '29px');

		secretKeyContainer = $('<div></div>').css({
			position: 'absolute',
			width: '600px',
			left: '50%',
			marginLeft: '-300px',
			zIndex: $.topZIndex() + 1
		});

		apiKeyContainer = $('<div></div>').css({
			position: 'absolute',
			width: '600px',
			left: '50%',
			marginLeft: '-300px',
			zIndex: $.topZIndex() + 1
		});

		ResourceProvider.gettAppSettingsBar(function (data) {
			resetSecretPopup = new Popup();
			resetSecretDialog = data.find('#resetAppSecretDialog').remove();
			
			resetSecretDialog.on('click', '#resetAppSecretCancel', function () {
				resetSecretPopup.close();
			});
			resetSecretDialog.on('click', '#resetAppSecretConfirm', function() {
				var licenseId = getSelectedLicenseId();
				var license = licenses.where(function (license) { return license.Id == licenseId; }).firstOrNull();

				var applicationId = getSelectedApplicationId();
				var application = license.Applications.where(function (application) { return application.Id == applicationId; }).firstOrNull();

				Callback.submit(
					'User/ResetLicenseApplicationSecret',
					{ 'licenseId': licenseId, 'applicationId': applicationId },
					function (appData) {
						application.Secret = appData.Secret;
						showSecret();
						resetSecretPopup.close();
					}
				);
			});
			 
			Callback.submit(
				'User/GetUserLicenseDetailList',
				{},
				populateLicenseDropDown
			);
		});

		return deferred.promise();
	}
    
	function populateLicenseDropDown(data) {
		licenseOptions = [];
		licenses = data.Licenses;

		keyContainer = $('<div></div>');

		//Api key
		var apiKeyLabel = Html.div('appSettingsBarLabel', 'API KEY:');
		apiKeyLabel.appendTo(keyContainer);

		apiKey = Html.div('appSettingsBarCaption', '***').kswid('apiKey');
		apiKey.appendTo(keyContainer);
		
		apiKeyReadOnly = Html.textBox('appSettingsBarCaption textBox secretKeyReadOnly').kswid('apiKey');
		apiKeyReadOnly.attr('readonly', 'readonly');
		apiKeyReadOnly.click(function() {
			var input = this;
			input.select();
		});

		viewApiButton = $('<a class="eye" href="javascript:"></a>');
		viewApiButton.addClass('inlineBlock left');
		viewApiButton.kswid('buttonAppViewApi');
		viewApiButton.click(toggleApi);
		viewApiButton.appendTo(keyContainer);

		//Secret key
		var secretKeyLabel = Html.div('appSettingsBarLabel', 'SECRET KEY:');
		secretKeyLabel.appendTo(keyContainer);

		secretKey = Html.div('appSettingsBarCaption', '***').kswid('secretKey');
		secretKey.appendTo(keyContainer);

		secretKeyReadOnly = Html.textBox('appSettingsBarCaption textBox secretKeyReadOnly').kswid('secretKey');
		secretKeyReadOnly.attr('readonly', 'readonly');
		secretKeyReadOnly.click(function() {
			var input = this;
			input.select();
		});

		viewSecretButton = $('<a class="eye" href="javascript:"></a>');
		viewSecretButton.addClass('inlineBlock left');
		viewSecretButton.kswid('buttonAppViewSecret');
		viewSecretButton.click(toggleSecret);
		viewSecretButton.appendTo(keyContainer);

		resetSecretButton = $('<a class="iconEdit" href="javascript:"></a>');
		resetSecretButton.addClass('inlineBlock left');
		resetSecretButton.kswid('buttonResetSecret');
		resetSecretButton.click(resetSecret);
		resetSecretButton.appendTo(keyContainer);

		for (var i = 0; i < data.Licenses.length; i++) {
			var license = data.Licenses[i];
			licenseOptions[i] = { 'name': license.Name, 'value': license.Id };
			licenseOptions[i].applicationOptions = [];
			
			if (typeof license.Applications != "undefined") {
				for (var j = 0; j < license.Applications.length; j++) {
					var application = license.Applications[j];
					licenseOptions[i].applicationOptions[j] = { 'name': application.Name, 'value': application.Id };
				}
			}
		}

		licenseDropDown = new DropDown(
			'dropDown',
			'license',
			licenseOptions,
			null);
		licenseDropDown.change(onLicenseChange);
		licenseDropDown.css({'float': 'left', 'marginRight': '20px'});

		applicationDropDown = generateApplicationDropDown(licenseDropDown.val());
		applicationDropDownContainer = Html.div('applicationDropDownContainer', applicationDropDown);
		applicationDropDownContainer.css('float', 'left');
		populateApiKeyAndSecret(getSelectedLicenseId(), getSelectedApplicationId());

		var appBarContainer = Html.div();
		appBarContainer.css({
			'textAlign': 'center',
			'width': '800px',
			'marginRight': 'auto',
			'marginLeft': 'auto'
		});

		appBarContainer.append(Html.div('appSettingsBarLabel', 'Current License'));
		appBarContainer.append(licenseDropDown);
		appBarContainer.append(Html.div('appSettingsBarLabel', 'App'));
		appBarContainer.append(applicationDropDownContainer);
		appBarContainer.append(keyContainer);

		form.append(appBarContainer);

	    initializationCompleted();
	}

	function initializationCompleted() {
	    deferred.resolve();
	}

    function onLicenseChange() {
		populateApplicationDropDown(getSelectedLicenseId());
	};

	function populateApplicationDropDown(licenseId) {
		applicationDropDown = generateApplicationDropDown(licenseId);
		applicationDropDownContainer.html(applicationDropDown);
		populateApiKeyAndSecret(getSelectedLicenseId(), getSelectedApplicationId());
	}
	
	function generateApplicationDropDown(licenseId) {
		var licenseOption = licenseOptions.where(function(license) { return license.value == licenseId; }).firstOrNull();
		var applicationOptions = licenseOption.applicationOptions;

		if (applicationOptions.length == 0) {
			applicationOptions = [{ 'name': 'No Applications', 'value': null }];
			keyContainer.hide();
		}
		else {
			keyContainer.show();
		}

		var dropDown = new DropDown(
			'dropDown',
			'application',
			applicationOptions
		);
		dropDown.css('marginRight', '15px');
		dropDown.change(onApplicationChange);

		return dropDown;
	}
	
	function onApplicationChange() {
		populateApiKeyAndSecret(getSelectedLicenseId(), getSelectedApplicationId());
	}
	
	function populateApiKeyAndSecret(licenseId, applicationId) {
		hideApi(function() {

			var license = licenses.where(
				function(license) {
					return license.Id == licenseId;
				}).firstOrNull();

			if (license != null) {
				if (typeof license.Applications != "undefined") {
					var app = license.Applications.where(function(application) { return application.Id == applicationId; }).firstOrNull();

					if (app != null) {
						apiKeyReadOnly.val(app.Id);
					}
				}
			}

			hideSecret();
		});
	}
	
	function toggleApi(event) {
		if (apiKeyRevealed) {
			if (apiKeyRevealed.is(':visible')) {
				hideApi();
				return;
			}
		}

		showApi();
	}
	
	function showApi() {
		var licenseId = getSelectedLicenseId();
		var license = licenses.where(function (license) { return license.Id == licenseId; }).firstOrNull();

		if (license != null && typeof license.Applications != "undefined") {
			var applicationId = getSelectedApplicationId();
			var application = license.Applications.where(function (application) { return application.Id == applicationId; }).firstOrNull();

			if (!apiKeyRevealed) {
				apiKeyRevealed = Html.div('inlineBlock',
					'API Key',
					apiKeyReadOnly
				);

				apiKeyRevealed.css({
					'backgroundColor': '#efefef',
					'padding': '15px',
					'borderRadius': '0 0 10px 10px'
				});

				apiKeyContainer.insertAfter(form);
				apiKeyContainer.append(apiKeyRevealed);
			}

			apiKeyReadOnly.val(application.Id);

			hideSecret();

			apiKeyRevealed.slideDown();
		}
	}
	  
	function hideApi(callback) {
		if (apiKeyRevealed) {
			apiKeyRevealed.slideUp(400, callback);
		}
	}

	function resetAppSettingsBar() {
		if (licenses == null)
			return;
		
	    if (licenseDropDown != null) {
	        licenseDropDown.selectIndex(0);
	    }
	    populateApplicationDropDown(getSelectedLicenseId());
	    
	    hideApi();
	    hideSecret();
	}
    
	function toggleSecret(event) {
		if (secretKeyRevealed) {
			if (secretKeyRevealed.is(':visible')) {
				hideSecret();
				return;
			}
		}
		
		showSecret();
	}

	function showSecret() {
		var licenseId = getSelectedLicenseId();
		var license = licenses.where(function(license) { return license.Id == licenseId; }).firstOrNull();

		if (license != null && typeof license.Applications != "undefined") {
			var applicationId = getSelectedApplicationId();
			var application = license.Applications.where(function (application) { return application.Id == applicationId; }).firstOrNull();

			if (!secretKeyRevealed) {
				secretKeyRevealed = Html.div('inlineBlock',
					'Secret Key',
					secretKeyReadOnly
				);

				secretKeyRevealed.css({
					'backgroundColor': '#efefef',
					'padding': '15px',
					'borderRadius': '0 0 10px 10px'
			});

				secretKeyContainer.insertAfter(form);
				secretKeyContainer.append(secretKeyRevealed);
			}

			secretKeyReadOnly.val(application.Secret);

			hideApi();

			secretKeyRevealed.slideDown();
		}
	}

	function hideSecret() {
		if (secretKeyRevealed) {
			secretKeyRevealed.slideUp();
		}
	}
	
	function resetSecret() {
		resetSecretPopup.show(resetSecretDialog);
	}

	function getSelectedLicenseId() {
		if (licenseDropDown)
			return licenseDropDown.val();
		return null;
	}
	
	function getSelectedApplicationId() {
		return applicationDropDown.val();
	}

	return self;
};