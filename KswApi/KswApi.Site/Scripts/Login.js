﻿/// <reference path="Common/_commonReference.js" />

$(document).ready(function () {
	Login.initialize();
});

var Login = (function () {

	var self = {
		initialize: initialize
	};

	function initialize() {
		var email = $('#email');
		var password = $('#password');

		email.labelField('textboxLabel offset', 'User Name...');
		password.labelField('textboxLabel offset', 'Password...');

		$('#forgotPassword').click(forgotPassword);

		email.enter(submit);
		password.enter(submit);
		$('#submitLogin').click(submit);
	}

	function forgotPassword() {
		var dialog = Login.ResetPasswordDialog();
		dialog.show();
	}

	function submit() {
		clearError();
		$('#submitLogin').wait();

		// because we're doing a postback,
		// the timeout helps the waiting cursor to show up
		setTimeout(onSubmit, 1);
	}
	
	function onSubmit() {

	var email = $('#email');
		var password = $('#password');

		if (!validate()) {
			$('#submitLogin').endWait();
			email.select();

			return;
		}

		var form = $('#loginForm');

		form.find('[name="user_name"]').val(email.val());
		form.find('[name="password"]').val(password.val());

		form.submit();
	}

	function clearError() {
		$('#loginError').empty('');
	}

	function validate() {
		var errors = [];
		var email = $('#email').val();
		var password = $('#password').val();

		if (!email)
			errors.push('User name is required.');

		if (!password)
			errors.push('Password is required.');

		if (errors.length == 0)
			return true;

		var container = $('#loginError');
		$.each(errors, function (index, error) {
			container.append($('<div>').text(error));
		});
		
		return false;
	}

	return self;
})();

Login.ResetPasswordDialog = function () {
	var self = {
		show: show,
		close: close
	};

	var popup, dialog, submitButton;

	construct();

	function construct() {
		dialog = $('.template #forgotPasswordDialog').clone();

		popup = new Popup();

		dialog.find('#email').enter(submit);

		dialog.find('#cancelButton').click(close);

		submitButton = dialog.find('#submitButton');

		submitButton.click(submit);
	}

	function submit() {
		dialog.find('#forgotPasswordError').empty();
		submitButton.wait('action');

		var params = {
			email: dialog.find('#email').val()
		};

		Callback.submit('Administration/ResetPassword', params, onSuccess, onFailure);
	}

	function onSuccess() {
		close();
	}

	function onFailure(error) {
		submitButton.endWait();
		dialog.find('#forgotPasswordError').text(error.Details);
		dialog.find('#email').select();
	}

	function close() {
		popup.close();
	}

	function show() {
		popup.show(dialog);
		dialog.find('#email').select();
	}

	return self;
};
