﻿/// <reference path="Common/Html.js" />

$(document).ready(function () {
    TabTest.CreateTab();
});

var TabTest = {};

TabTest.CreateTab = function() {
    var tabTestDiv = $('#tabTest');

    var tab = new Tab('tabTest');

    tab.addTab('Short Codes', function(div) {
        TabTest.ShowContentA(div);
    });

    tab.addTab('Numbers', function(div) {
        TabTest.ShowContentB(div);
    });

    tab.setTitleArea(Html.div('defaultButton', 'Add Short Code'), function () {
        TabTest.ButtonClick();
    });

    tabTestDiv.append(tab);
};

TabTest.ShowContentA = function(contentArea) {
    contentArea.append(Html.div('', 'Content A'));
};

TabTest.ShowContentB = function (contentArea) {
    contentArea.append(Html.div('', 'Content b'));
};

TabTest.ShowContentC = function () {
    $('.tabContent').append(Html.div('', 'Content 3'));
};

TabTest.ButtonClick = function() {
    alert('button clicked');
};
