﻿var User = User || {};

$(function() {
	// initialize the password boxes
	User.Password.initialize();
});

User.Password = (function () {
	var self = {
		initialize: initialize
	};
	
	function initialize() {
		var password = $('#password');
		var confirm = $('#confirm');
		var button = $('#submitPassword');

		password.labelField('textboxLabel offset', 'Password...');
		confirm.labelField('textboxLabel offset', 'Confirm Password...');

		password.pstrength();

		password.enter(submit);
		confirm.enter(submit);
		button.click(submit);
		
		password.select();
	}
	
	function clearError() {
		$('#passwordError').empty();
	}
	
	function onError(error) {
		$('#passwordError').text(error.Details);
		$('#password').select();
	}
	
	function submit() {
		clearError();
		var form = $('#passwordForm');
		var params = Callback.fromContainer(form);
		Callback.submit('Administration/SetPassword', params, onSuccess, onError);
	}
	
	function onSuccess() {
		$('#passwordForm').hide();
		$('#passwordSuccess').show();
	}

	return self;
})();
