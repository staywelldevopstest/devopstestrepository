﻿/// <reference path="../Common/jquery.extensions.js" />
/// <reference path="../Common/Html.js" />
/// <reference path="../Common/Callback.js" />
/// <reference path="../Common/Pager.js" />
/// <reference path="../Common/Page.js" />
/// <reference path="../Common/QueriedPager.js"/>

var Administration = Administration || {};

Administration.ClientList = function () { };

Administration.ClientList.prototype = {
	MAX_ITEMS_PER_PAGE: 10,
	MAX_PAGES_VISIBLE: 10,
	_listArea: null,
	_queriedPager: null,
	_searchBox: null,
	_total: null,
	_clientList: null,
	_template: null,
	_appliedQuery: null,
	show: function () {
		Page.setTitle('CLIENTS');
		Page.setContentTitle(this.createToolBar());
		Page.switchPage(this.create());
		this._searchBox.focus();
	},
	create: function () {
		var self = this;
		
		this._listArea = Html.div();

		var listArea = Html.div('listContentArea', Html.div('detailContentArea', this._listArea));

		ResourceProvider.getClientHtml(function (html) {
			self._template = html.find('#clientTemplate').remove();

			self._queriedPager = new QueriedPager({
				itemsPerPage: self.MAX_ITEMS_PER_PAGE,
				maxVisiblePages: self.MAX_PAGES_VISIBLE,
				pageNavigationCallback: function (page, itemsPerPage, offset) {
					var search = self._searchBox ? self._searchBox.val() : null;

					Callback.submit(
						'Administration/GetClients',
						{ 'offset': offset, 'count': itemsPerPage, search: search },
						function (list) {
							self.populate(list, self._template);
							self._queriedPager.updatePager(list.Total);
						}
					);
				}
			});

			listArea.append(self._queriedPager);
			self._queriedPager.gotoPage(1);
		});
		
		listArea.kswid('listClientDetails');

		return listArea;
	},
	populate: function (list, template) {
		this._clientList = list;
		this._total = list.Total;

		var newListArea = Html.div();

		if (list.Total > 0) {
			for (var i = 0; i < list.Items.length; i++) {
				var item = new Administration.ClientEditor(list.Items[i], this, template);
				var view = item.createView();
				newListArea.append(view);
			}
		}
		else {
			newListArea.append("No clients were found matching the specified criteria.");
		}

		this._listArea.swapWith(newListArea);
		
		this._listArea = newListArea;
	},
	refresh: function () {
		this._queriedPager.gotoPage(1);
	},
	updateList: function (list) {
	    //update pager stuff
	    var self = this;

		this._total--;

		if (list.Items.length > 0) {
		    for (var i = 0; i < list.Items.length; i++) {
		        var client = list.Items[i];
		        var isFound = false;
				//Compare the current list with the returning list and find the one (should only be one) that isn't already in the list
		        for (var j = 0; j < self._clientList.Items.length; j++) {
		            var newClient = self._clientList.Items[j];

				    if (client.Id == newClient.Id) {
				        isFound = true;
				        
				        break;
				    }
				}
		        
				if (!isFound) {
				    self._clientList.Items.push(list.Items[i]);

		            var item = new Administration.ClientEditor(list.Items[i], this, self._template);
		            var view = item.createView();

		            self._listArea.append(view);
		        }
		    }
		}
	},
	createToolBar: function () {
		var self = this;
		this._searchBox = Html.textBox('searchTextBox');

		var addNewClientButton = Html.anchorButton('defaultButton defaultButtonBox defaultAnchorbutton',
			'Add New Client',
			function() {
				var editor = new Administration.ClientEditor(null, this, self._template);
				self._listArea.prepend(editor.createEditor());
				editor.focus();
			}
		);

		addNewClientButton.kswid('buttonAddClient');

		this._searchBox.kswid('inputSearch');
		this._searchBox.enter(function () { self.search(); });

		var searchButton = Html.button('defaultButton searchButton', ' ', function() { self.search(); });
			

		searchButton.kswid('buttonSearch');

		return Html.div(null, addNewClientButton, Html.div('searchArea', Html.div('searchTextArea', this._searchBox ), searchButton ));
	},
	search: function () {
		this._queriedPager.gotoPage(1);
	}
};

