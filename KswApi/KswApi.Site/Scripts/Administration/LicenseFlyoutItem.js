﻿/// <reference path="../Common/_commonReference.js" />

var Administration = Administration || {};

Administration.LicenseFlyoutItem = function (client, license, flyout) {
	this._license = license;
	this._client = client;
	this._flyout = flyout;
};

Administration.LicenseFlyoutItem.prototype = {
	_client: null,
	_license: null,
	_flyout: null,
	create: function (isChild) {
		var self = this;
		var client = this._client;
		var license = this._license;

		var editButtons = Html.editAndDeleteButtons(
			function () {
				Administration.Navigation.showLicense(client.Id, license.Id);
			},
			function () {
				self.showDeleteConfirmation();
			}).hide();

		var item = Html.div(
			'itemBox',
			editButtons,
			Html.div('flyoutTitle', license.Name),
			Html.breakDiv(),
			self.createApplication(license.Applications)
		)
			.attr('data-kswid', 'divDetailLicense')
			.hover(function () {
				editButtons.show();
			}, function () {
				editButtons.hide();
			});
		
		if (isChild) {
			item.addClass('childBox');
		}
		
		if (license.ChildLicenses) {
			for (var i = 0; i < license.ChildLicenses.length; i++) {
				var child = new Administration.LicenseFlyoutItem(client, license.ChildLicenses[i], this._flyout);
				item.append(child.create(true));
			}
		}

		return item;
	},

	createApplication: function (applications) {
		var self = this;

		if (!applications || !applications.length || applications.length <= 0)
			return null;

		var details = this.createApplicationDetails(applications[0]);

		if (applications.length == 1) {
			return Html.div('separate',
				Html.label('label', 'Application: '),
				Html.label('value', applications[0].Name).attr('data-kswid', 'txtApplicationName'),
				Html.breakDiv(),
				details
			);
		}

		var options = {};
		var dictionary = {};
		for (var i = 0; i < applications.length; i++) {
			options[applications[i].Id] = applications[i].Name;
			dictionary[applications[i].Id] = applications[i];
		}

		var dropDown = Html.dropDown('dropDown left small', null, options);
		dropDown.attr('data-kswid', 'selectLicApp');

		dropDown.change(function () {
			var newDetails = self.createApplicationDetails(dictionary[dropDown.val()]);
			newDetails.hide();
			details.after(newDetails);
			details.customHide(function () {
				details.remove();
				newDetails.customShow();
				details = newDetails;
			});
		});

		return Html.div('separate',
			Html.label('label left aligned', 'Application: '),
			dropDown,
			Html.breakDiv(),
			details
		);
	},
	createApplicationDetails: function (application) {
		var mask = '***************************';
		var secret = Html.label('value wrapped', mask);
		var secretShown = false;
		return Html.div(null,
			Html.div(null,
				Html.label('label', 'App ID: '),
				Html.label('value wrapped', application.Id).attr('data-kswid', 'txtApplicationType')
			),
			Html.div(null,
				Html.label('label', 'App Secret: '),
				secret,
				' ',
				Html.anchorButton('eye', ' ', function () {
					secretShown = !secretShown;
					if (secretShown)
						secret.text(application.Secret);
					else
						secret.text(mask);

				}).attr('data-kswid', 'buttonAppViewSecret')
			)
		);
	},
	showDeleteConfirmation: function (license) {
		var self = this;

		var popup = new Popup();

		var dialog = Html.div(
			'dialog',
			Html.spacer(),
			Html.div('centered',
				Html.label(null, 'Are you sure you would like to '),
				Html.label('emphasized', 'DELETE'),
				Html.label(null, ' license')
			),
			Html.spacer(),
			Html.div('large', this._license.Name),
			Html.spacer(),
			Html.div(null, 'The following information will be'),
		    Html.label('emphasized', 'PERMANENTLY'),
			Html.label(null, ' deleted:'),
		    Html.div(null, 
		        Html.spacer(),
		        Html.div(null, 'Short Codes associated to the license'),
		        Html.div(null, 'Long Codes associated to the license'),
		        Html.div(null, 'Keywords associated to the license'),
			    Html.spacer()
		    ).kswid('delLicenseText'),
			Html.div('padded buttonStrip',
				Html.button('defaultWhiteButton  spaced', 'Cancel', function () {
				    popup.close();
				}).kswid('buttonDeleteCancel'),
				Html.button('defaultOrangeButton  spaced', 'Delete License', function () {
				    Callback.submit('Administration/DeleteLicense', { licenseId: self._license.Id }, function () {
				        popup.close();
				        self._flyout.refresh();
				    });
				}).kswid('buttonDeleteConfirm')
			),
			Html.breakDiv()
		).kswid('dialogDelete');

		popup.show(dialog);
	}
};
