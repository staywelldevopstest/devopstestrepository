﻿var Administration = Administration || {};

Administration.Navigation = (function () {
	var self = {
		hasPermission: hasPermission,
		initialize: initialize,
		showHome: showHome,
		showClients: showClients,
		showClientUsers: showClientUsers,
		showBuckets: showBuckets,
		showLicense: showLicense,
		showLogs: showLogs,
		showContent: showContent,
		showCollections: showCollections,
		showTestContent: showTestContent,
		showReports: showReports,
		showCopyrights: showCopyrights,
		showAdministration: showAdministration,
		setLogPage: setLogPage,
		show: show
	};

	var callbacks = {};
	var menu = new Menu(), contentMenu;

	function showCallback(event) {
		var value = callbacks[event.data];
		if (value)
			value.apply(this, value);
		else
			showHome();
	}

	function show(name, args) {
		if (!name) {
			showHome.apply(this, args);
			return;
		}

		var value = callbacks[name];
		if (value)
			value.apply(this, args);
		else
			showHome();
	}

	function initialize() {
		callbacks['dashboard'] = showHome;
		callbacks['clients'] = showClients;
		callbacks['clientUsers'] = showClientUsers;
		callbacks['license'] = showLicense;
		callbacks['logs'] = showLogs;
		callbacks['content'] = internalShowContent;
		callbacks['images'] = showImageEditor;
		callbacks['buckets'] = showBuckets;
	    callbacks['collections'] = showCollections;
		callbacks['reports'] = showReports;
		callbacks['testcontent'] = showTestContent;
		callbacks['copyrights'] = showCopyrights;
		callbacks['administration'] = showAdministration;

		var dashboardMenu = new MenuItem();
		var clientsMenu = new MenuItem();
		var usersMenu = new MenuItem();
		var logsMenu = new MenuItem();
		var reportsMenu = new MenuItem();
		contentMenu = new MenuItem();
		var administrationMenu = new MenuItem();

		dashboardMenu.SetCaption('DASHBOARD');
		dashboardMenu.SetDataKswId('dashboard');
		dashboardMenu.SetRequiredRight('show_always');
		dashboardMenu.SetCallBack(function () { showHome(); });

		clientsMenu.SetCaption('CLIENTS');
		clientsMenu.SetDataKswId('clients');
		clientsMenu.SetRequiredRight('Manage_Client');
		clientsMenu.SetCallBack(function () { showClients(); });

		usersMenu.SetCaption('USERS');
		usersMenu.SetDataKswId('users');
		usersMenu.SetRequiredRight('Read_ClientUsers');
		usersMenu.SetCallBack(function () { showClientUsers(); });

		logsMenu.SetCaption('LOGS');
		logsMenu.SetDataKswId('logs');
		logsMenu.SetRequiredRight('Read_Logs');
		logsMenu.SetCallBack(function () { showLogs(); });

		reportsMenu.SetCaption('REPORTS');
		reportsMenu.SetDataKswId('reports');
		reportsMenu.SetRequiredRight('Read_Reports');
		reportsMenu.SetCallBack(function () { showReports(); });

		administrationMenu.SetCaption('ADMINISTRATION');
		administrationMenu.SetDataKswId('administration');
		administrationMenu.SetRequiredRight('Manage_Administration');
		administrationMenu.SetCallBack(function () { showAdministration(); });

		var contentManagementSubMenu = new MenuItem();
		var bucketManagementSubMenu = new MenuItem();
		var collectionManagementSubMenu = new MenuItem();
	    
		contentManagementSubMenu.SetCaption('CONTENT MANAGEMENT');
		contentManagementSubMenu.SetDataKswId('contentManagement');
		contentManagementSubMenu.SetRequiredRight('Read_Content');
		contentManagementSubMenu.SetCallBack(function () { showContent(); });

		bucketManagementSubMenu.SetCaption('BUCKET MANAGEMENT');
		bucketManagementSubMenu.SetDataKswId('bucketManagement');
		bucketManagementSubMenu.SetRequiredRight('Manage_Content');
		bucketManagementSubMenu.SetCallBack(function () { showBuckets(); });

		collectionManagementSubMenu.SetCaption('COLLECTION MANAGEMENT');
		collectionManagementSubMenu.SetDataKswId('collectionManagement');
		collectionManagementSubMenu.SetRequiredRight('Manage_Content');
		collectionManagementSubMenu.SetCallBack(function () { showCollections(); });

		contentMenu.SetCaption('CONTENT');
		contentMenu.SetDataKswId('content');
		contentMenu.SetRequiredRight('Read_Content');
		contentMenu.SetSubMenu([contentManagementSubMenu, bucketManagementSubMenu, collectionManagementSubMenu]);

		var menuItems = [dashboardMenu, clientsMenu, usersMenu, logsMenu, reportsMenu, contentMenu, administrationMenu];

	    // return deferred.promise() object
		return menu.initMenu(menuItems, showCallback);
	}

	function showHome() {
		if (!Global.User)
			return;

		Navigation.set('dashboard');

		var dashboard = Administration.Pages.Dashboard();
		dashboard.show();
	}

	function showClients() {
		if (!allow('Manage_Client'))
			return;

		Navigation.set('clients');

		var list = new Administration.ClientList();

		list.show();
	}

	function showClientUsers() {
		if (!allow('Read_ClientUsers'))
			return;

		Navigation.set('clientUsers');

		var users = new Administration.Pages.ClientUsers();

		users.show();
	}

	function showLicense(clientId, licenseId) {
		if (!allow('Manage_Client'))
			return;

		Navigation.set('license', clientId, licenseId);

		Administration.Pages.LicenseEditor.show(clientId, licenseId);
	}

	function showLogs() {

		if (!allow('Read_Logs'))
			return;

		Navigation.set('logs');

		var logs = new Administration.Pages.Logs();

		logs.show.apply(logs, arguments);
	}

	function showContent(bucketSlug, slug) {
		if (!allow('Read_Content'))
			return;

		internalShowContent(bucketSlug, slug);
	}

	function internalShowContent(bucketSlug, slug) {
	    contentMenu.select();
	    
		if (!allow('Read_Content'))
			return;

		if (slug) {
			showContentEditor(bucketSlug, slug);
		}
		else {
			showContentList();
		}
	}

	function showContentEditor(bucketSlug, slug) {
		Navigation.set('content', bucketSlug, slug);
		var editor = new Content.Pages.ContentView(slug, bucketSlug);
		editor.showDraft();
	}

	function showImageEditor(bucketSlug, slug) {
		Navigation.set('images', bucketSlug, slug);
		var editor = new Content.ImageEditor(slug, bucketSlug);
		editor.show();
	}

	function showContentList() {
		Navigation.set('content');

		var content = new Content.Pages.ContentList();

		content.show();
	}


	function showTestContent() {
		if (!allow('Manage_Content'))
			return;

		Navigation.set('testcontent');

		var content = new Administration.Pages.TestContent();

		content.show();

	}

	function showBuckets() {
		if (!allow('Manage_Content'))
			return;

		Navigation.set('buckets');

		var buckets = new Administration.Pages.ContentBuckets();

		buckets.show();
	}

	function showCollections(idOrSlug) {

	    if (!allow('Manage_Content'))
	        return;
        
	    if (!allow('Read_Content'))
	        return;
	    
	    Navigation.set('collections', idOrSlug);
	    contentMenu.select();
	    
	    if (idOrSlug) {
	        var collectionView = new Content.Pages.CollectionView('Collection Management', idOrSlug, null);
	        collectionView.show();
	    }
	    else {
	        var collections = new Content.Pages.ContentManagementList('Collection Management', 'Add Collection', 'Collections');//, 'Content', null);//  'Add Collection', 'Edit Collection'); <== these should be set by the respective pages...
	        collections.show();
	    }
	    
//	    Navigation.set('collections');

	}
    
	function showReports() {
		if (!allow('Read_Reports'))
			return;

		Navigation.set('reports');

		var reports = new Administration.Pages.Reports();

		reports.show();
	}

	function showCopyrights() {
		if (!allow('Manage_Administration'))
			return;

		Navigation.set('copyrights');

		var copyrights = new Administration.Pages.Copyrights();

		copyrights.show();
	}

	function showAdministration() {
		if (!allow('Manage_Administration'))
			return;

		Navigation.set('administration');

		var administration = new Administration.Pages.Administration();

		administration.show();
	}

	function setLogPage() {
		var args = ['logs'];

		args = args.concat($.makeArray(arguments));
		Navigation.set.apply(this, args);
	}

	function allow(right) {
		if (Global.User && Global.User.Rights.contains(right))
			return true;

		location.href = Global.LoginPath;
		return false;
	}

	function hasPermission(right) {
		return Global.User && Global.User.Rights.contains(right);
	}

	return self;
})();
