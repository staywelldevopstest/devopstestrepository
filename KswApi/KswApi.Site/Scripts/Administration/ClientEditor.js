﻿var Administration = Administration || {};
var x = 0;
Administration.ClientEditor = function (client, clientList, template) {
	var self = {
		createView: createView,
		createEditor: createEditor,
		setLicenseCount: setLicenseCount,
		focus: focus
	};

	var container;

	function createView() {
		container = template.clone();

		var noteContainer = container.find('#noteContainer'),
		    expander = container.find('#expander'),
		    editButtons = container.find('#modifyButtons'),
			flyout,
		    notesVisible = false;

		update();

		container.hover(function () {
			editButtons.show();
		}, function () {
			editButtons.hide();
		});

		editButtons.find('#edit').click(function (event) {
			event.stopPropagation();
			var old = container;
			var editor = createEditor();
			old.swapWith(editor, function () {
				focus();
			});
		});

		editButtons.find('#delete').click(function (event) {
			event.stopPropagation();
			showDeleteDialog();
		});

		container.find('#licenseButton').click(function (event) {
			event.stopPropagation();

			if (!flyout) {
				flyout = new Administration.LicenseFlyout(client, container, self);
			}

			flyout.toggle();
		});

		container.find('#clientBox').click(function () {
			notesVisible = !notesVisible;
			if (notesVisible) {
				noteContainer.show('blind', { 'direction': 'up' }, 400);
				expander.addClass('iconExpanded');
				expander.removeClass('iconCollapsed');
			}
			else {
				noteContainer.hide('blind', { 'direction': 'up' }, 400);
				expander.addClass('iconCollapsed');
				expander.removeClass('iconExpanded');
			}
		});

		return container;
	}

	function focus() {
		container.find('input[name="name"]').select();
	}

	function createEditor() {
		var editor, id = null, name = null, webSite = null, contactName = null, email = null, phone = null, notes = null;
		if (client) {
			id = client.Id;
			name = client.ClientName;
			webSite = client.ClientWebSite;
			contactName = client.ContactName;
			email = client.ContactEmail;
			phone = client.ContactPhone;
			notes = client.Notes;
		}

		editor = Html.div('itemBox',
			Html.div('iconClose')
				.click(function () {
					cancel(editor);
				}
			),
			Html.div('addContentAreaTitle', client ? 'Edit Client' : 'Add New Client'),
			Html.breakDiv(),
			Html.div('iconRequired'),
			Html.labeledField('left wrapper', 'inlineLabel', 'Client Name...',
				Html.textBox('defaultTextBox wide', 'clientName', name).kswid('inputClientName')
			),
			Html.labeledField('right wrapper', 'inlineLabel', 'Website...',
				Html.textBox('defaultTextBox  wide', 'clientWebsite', webSite).kswid('inputClientWeb')
			),
			Html.spacer(),
			Html.breakDiv(),
			Html.twoColumnContent('twoColumnDefault',
				Html.div(null,
					Html.div('addContentAreaSubtitle', 'Primary Point of Contact'),
					Html.div('addClientDataContactName',
						Html.labeledField('wrapper', 'inlineLabel', 'Contact Name...',
							Html.textBox('defaultTextBox', 'contactName', contactName).kswid('inputContactName')
						)
					),
					Html.div('addClientDataContactEmail',
						Html.labeledField('wrapper', 'inlineLabel', 'Email...',
							Html.textBox('defaultTextBox', 'contactEmail', email).kswid('inputContactEmail')
						)
					),
					Html.div('addClientDataContactPhone',
						Html.labeledField('wrapper', 'inlineLabel', 'Phone...',
							Html.textBox('defaultTextBox', 'contactPhone', phone).kswid('inputContactPhone')
						)
					)
				),
				Html.div(null,
					Html.div('addContentAreaSubtitle', 'Notes'),
					Html.br(),
					Html.labeledField('wrapper', 'textAreaLabel', 'Notes...',
						Html.textArea('textArea', 'notes', notes).kswid('inputClientNotes')
					)
				)
			),
			Html.div('addClientDataButtonsArea',
				Html.button('defaultButton defaultButtonBox addButton',
					(client ? 'Save' : 'Add and Manage Licenses'),
					function () {
						var serviceMethod = client ? Service.put : Service.post;
						var service = client ? 'Administration/Clients/' + client.Id : 'Administration/Clients';
						serviceMethod({
							service: service,
							form: editor,
							success: function (newClient) {
								var isNewClient = !client;

								client = newClient;

								//If it is a new client we need to go to the license create screen.  Otherwise just close the edit window
								if (isNewClient) {
									Administration.Pages.LicenseEditor.showNew(client);
								} else {
									var view = createView();

									editor.swapWith(view);
								}
							}
						});
					}
				).kswid('buttonClientSave'),
				Html.anchorButton('defaultWhiteButton defaultButtonBox defaultAnchorbutton',
					'Cancel',
					function () {
						cancel(editor);
					}
				).kswid('buttonClientCancel')
			),
			Html.breakDiv()
		).kswid('divClientAdd');

		container = Html.div('relative', editor);

		return container;
	}

	function cancel() {
		if (container) {
			if (client) {
				var old = container;
				var view = createView();
				old.swapWith(view);
			}
			else {
				remove();
			}
		}
	}

	function remove() {
		if (container)
			container.hideAndRemove();
	}

	function setLicenseCount(count) {
		container.find('#licenseCount').text(count);
	}

	function update(modifiedClient) {
		if (modifiedClient)
			client = modifiedClient;

		var website = '';

		if (client.ClientWebSite && client.ClientWebSite != '') {
			if (client.ClientWebSite.indexOf('http') == -1)
				website = "http://" + client.ClientWebSite;
			else
				website = client.ClientWebSite;
		}

		container.find('#clientName').text(client.ClientName);
		container.find('#dateAdded').text(Helper.dateFromDateTime(client.DateAdded));

		container.find('#website')
			.attr('href', website)
			.text(client.ClientWebSite)
			.click(function (event) {
				event.stopPropagation();
			});

		container.find('#licenseCount').text(client.Licenses ? client.Licenses.length : 0);

		if (client.ContactName && client.ContactName != '') {
			container.find('#contactTitle').show();
			container.find('#contact').text(client.ContactName).show();
		}
		else {
			container.find('#contactTitle').hide();
			container.find('#contact').hide();
		}

		if (client.ContactPhone && client.ContactPhone != '') {
			container.find('#phoneTitle').show();
			container.find('#phone').text(client.ContactPhone).show();
		}
		else {
			container.find('#phoneTitle').hide();
			container.find('#phone').hide();
		}

		if (client.ContactEmail && client.ContactEmail != '') {
			container.find('#emailTitle').show();
			container.find('#email').text(client.ContactEmail).show();
		}
		else {
			container.find('#emailTitle').hide();
			container.find('#email').hide();
		}

		container.find('#note').text(client.Notes);
	}

	function showDeleteDialog() {
		var popup = new Popup();

		var cancelButton = Html.button('defaultWhiteButton spaced', 'Cancel', function () { popup.close(); });

		cancelButton.kswid('buttonDeleteCancel');

		var deleteButton = Html.button('defaultOrangeButton spaced', 'Delete Client', function () {
			Service.del({
				service: 'Administration/Clients/' + client.Id,
				success: function () {
					clientList.refresh();
					popup.close();
				}
			});
		});

		deleteButton.kswid('buttonDeleteConfirm');

		var dialog = Html.div(
			'dialog',
			Html.spacer(),
			Html.div('centered',
				Html.label(null, 'Are you sure you would like to '),
				Html.label('emphasized', 'DELETE'),
				Html.label(null, ' client')
			),
			Html.spacer(),
			Html.div('large', client.ClientName),
			Html.spacer(),
			Html.div(null,
				Html.div(null, 'The following information will be'),
				Html.label('emphasized', 'PERMANENTLY'),
				Html.label(null, ' deleted:'),
				Html.spacer(),
				Html.div(null, 'Short Codes associated to all licenses'),
				Html.div(null, 'Long Codes associated to all licenses'),
				Html.div(null, 'Keywords associated to all licenses'),
				Html.spacer()
			).kswid('delClientText'),
			Html.div('padded buttonStrip',
				cancelButton,
				deleteButton
			),
			Html.breakDiv()
		);

		dialog.kswid('deleteClientDialog');

		popup.show(dialog);
	}

	return self;
};
