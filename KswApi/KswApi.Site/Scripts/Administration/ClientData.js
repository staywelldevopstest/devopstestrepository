﻿/// <reference path="_clientReference.js" />

var ClientData = {};

$(document).ready(function () {
	//Create all the expand events
	ClientData.suppressExpand = false;

	$('.noExpandOnClick').click(function () {
		ClientData.suppressExpand = true;
	});

	$('.clientDataLicenseInfo').each(function () {
		var current = $(this);
		var container = current.closest('.clientDataDetail');
		var id = container.find('.hiddenClientId').val();
		var name = container.find('.hiddenClientName').val();
		$(this).click(function (event) {
			event.stopImmediatePropagation();
			Administration.LicenseFlyout.show(id, name, current);
		});
	});

	$('.clientDataDetail').click(function () {
		if (ClientData.suppressExpand == false) {
			
			ClientDataDetailExpander.CreateClientDataDetailClickEvents(this, 'clientDataDetailExpanded', '.clientDataNotes', 90, '#clientDataExpander');
		}
		else {
			ClientData.suppressExpand = false;
		}
	});

	//Create the hover events
	$('.clientDataDetail').hover(
		function () {
			$(this).find('.iconEdit').show();
			$(this).find('.iconDelete').show();
		},
		function () {
			$(this).find('.iconEdit').hide();
			$(this).find('.iconDelete').hide();
		}
	);
	
	$(window).resize(function () {
		//only do it if the dialog box is not hidden
		if (!$('#dialog-box').is(':hidden')) ClientData.ShowPopup();
	});

});

ClientData.show = function() {
	location.href = Global.ApplicationPath + 'ClientDataManagement';
};

ClientData.CloseDeleteDialog = function HideDeleteDialog() {
	var actionButtonUrl = new String($('#deleteClientButton').attr('href'));
	
	//We need to remove the guid at the end of the url or else we will keep adding the Id to it when we show it.
	$('#deleteClientButton').attr('href', actionButtonUrl.substr(0, actionButtonUrl.length - 37));

	$("#deleteClient").dialog("close");
	$('#dialog-overlay, #dialog-box').hide();

	return false;
};

//Popup dialog
ClientData.ShowPopup = function () {

	// get the screen height and width  
	var maskHeight = $(document).height();
	var maskWidth = $(window).width();

	// calculate the values for center alignment
	var dialogTop = (maskHeight / 3) - ($('#dialog-box').height()) - 120;
	var dialogLeft = (maskWidth / 2) - ($('#dialog-box').width() / 2) - 110;

	$('#dialog-overlay').fadeIn(600);
	$('#dialog-box').fadeIn(600);
	// assign values to the overlay and dialog box
	$('#dialog-box').css({ top: dialogTop, left: dialogLeft }).show();
	$('#dialog-overlay').css({ height: maskHeight, width: maskWidth, top: -120, left: -110 }).show();
};

