﻿var Administration = Administration || {};

Administration.SettingsFlyout = function () { };

Administration.SettingsFlyout.show = function (container) {
    var menu = new Administration.SettingsFlyout();
    menu.show(container);
}

// class definition
Administration.SettingsFlyout.prototype = {
    _flyout: null,
    _anchor: null,
    show: function (container) {
        this._anchor = container;
    },
    hide: function () {
        var self = this;

        if (self._flyout) {
            self._anchor.data('SettingsFlyout', null);

            self._flyout.hide('drop', { direction: 'left' }, 'slow', function () {
                self._flyout.remove();
            });
        }
    },
    showFlyout: function (data) {
        this._flyout = this.createFlyout(data);
        this._flyout.hide();
        this._flyout.show('drop', { direction: 'left' }, 'slow');
        this._anchor.data('SettingsFlyout', this);
    },
    createFlyout: function (data) {
        var self = this;

        return $('<div>')
			.addClass('flyout')
			.addClass('itemBox')
			.click(function (e) { e.stopPropagation(); })
			.append(
				$('<div>')
					.addClass('iconClose')
					.text(' ')
					.click(function () { self.hide(); }),
				$('<div>')
					.addClass('flyoutTitle')
					.text(data.ClientName + ' Licenses'),
				$('<div>')
					.addClass('break')
					.append($('<a>')
						.addClass('defaultButton')
						.addClass('defaultButtonBox')
						.text('Log out')
						.click())
			);
    }
};
