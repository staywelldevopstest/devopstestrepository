﻿var Administration = Administration || {};

Administration.Pages = Administration.Pages || {};

Administration.Pages.ContentBuckets = function () {
	var self = {
		show: show,
		refresh: refresh,
		refreshCurrentPage: refreshCurrentPage
	};

	var ITEMS_PER_PAGE = 10,
	    MAXIMUM_NUMBER_OF_VISIBLE_PAGES = 10;
	var searchBox,
	    page,
	    bucketTemplate,
	    list,
	    origin,
	    originContainer,
	    deleteTemplate,
	    typeFilters,
		appliedFilter,
		appliedQuery;
	var _pager;

	function refresh() {
		_pager.refresh();
	}
	
	function refreshCurrentPage() {
		_pager.refreshCurrentPage();
	}

	function show() {
		ResourceProvider.getContentBuckets(onHtml);
	}

	function onHtml(html) {
		create(html);

		Callback.submit('Content/BucketOrigins', null, onOrigins);
	}

	function finishShow() {
		Page.setTitle('BUCKETS');

		Page.setContentTitle(createToolBar());

		Page.switchPage(page, function () { searchBox.focus(); });
	}

	function create(html) {
		_pager = new QueriedPager({
			itemsPerPage: ITEMS_PER_PAGE,
			maxVisiblePages: MAXIMUM_NUMBER_OF_VISIBLE_PAGES,
			pageNavigationCallback: function(pageIndex, itemsPerPage, offset) {
				var filter = appliedFilter;
				filter.offset = parseInt(offset, 10);
				filter.count = parseInt(itemsPerPage, 10);

				Callback.submit(
					'Content/Buckets',
					filter,
					function (buckets) {
						onData(buckets, offset);
					}
				);
			}
		});

		page = html.find('#pageTemplate').remove();

		bucketTemplate = html.find('#bucketTemplate').remove();

		deleteTemplate = html.find('#deleteConfirm').remove();
		
		typeFilters = page.find('#typeFilters .defaultCheckBox');

		originContainer = page.find('#originContainer');

		list = page.find('#bucketList');

		page.find('.defaultCheckBox').checkBox();

		page.find('#pagerContainer').append(_pager);

		setupEvents();

		applyFilters();
		return page;
	}
	
	function setupEvents() {
		page.find('#applyFilters').click(applyFilters);
		page.find('#resetFilters').click(resetFilters);
	}
	
	function resetFilters() {
		page.find('#legacyIdFilter').val('');
		page.find('#readOnly').removeClass('checked');
		typeFilters.addClass('checked');
		if (origin) {
			origin.selectIndex(0);
		}
		
		applyFilters();
	}

	function applyFilters() {
		appliedFilter = {};

		var legacyId = page.find('#legacyIdFilter').val();
		if (legacyId) {
			appliedFilter.legacyId = legacyId;
		}
		
		var readOnly = page.find('#readOnly').hasClass('checked');
		if (readOnly) {
			appliedFilter.readOnly = true;
		}
		
		var type = [];

		if (typeFilters.filter('#typeArticle').hasClass('checked')) {
			type.push('Article');
		}
		if (typeFilters.filter('#typeText').hasClass('checked')) {
			type.push('Text');
		}
		if (typeFilters.filter('#typeImage').hasClass('checked')) {
			type.push('Image');
		}
		if (typeFilters.filter('#typeVideo').hasClass('checked')) {
			type.push('Video');
		}

		if (type.length != 4)
			appliedFilter.type = type;

		if (type.length == 0)
			appliedFilter.type = ['None'];

		if (origin) {
			var originVal = origin.val();

			if (originVal) {
				appliedFilter.origin = originVal;
			}
		}

		appliedFilter.query = appliedQuery;

		_pager.refresh();
	}

	function applyQuery() {
		if (searchBox) {
			var query = searchBox.val();

			appliedQuery = query;
			appliedFilter.query = appliedQuery;

			_pager.refresh();
		}
	}

	function onData(data, offset) {
		if (0 < offset && offset >= data.Total) {
			_pager.gotoPage(data.Total / ITEMS_PER_PAGE);
		}
		else {
			populate(data);
			_pager.updatePager(data.Total);
		}
	}
	
	function populate(buckets) {
		list.empty();

		if (buckets.Total == 0) {
			list.append(Html.div('itemBox', 'No results found.'));
		}
		else {
			for (var i = 0; i < buckets.Items.length; i++) {
				list.append(createBucket(buckets.Items[i]));
			}
		}
	}

	function onOrigins(origins) {
		originContainer.empty();
		var options = {
			'': 'All'
		};
		for (var i = 0; i < origins.Items.length; i++)
			options[origins.Items[i].Id] = origins.Items[i].Name;

		origin = Html.dropDown('dropDown', null, options).kswid('originFilter');
		originContainer.append(origin);

		finishShow();
	}

	function createBucket(data) {
		var bucket = bucketTemplate.clone();

		bucket.find('#name').text(data.Name).click(function () {
			viewBucketDetails(data);
		});

		bucket.find('#description').text(data.Description);
		bucket.find('#type').text(data.TypeDescription);
		bucket.find('#origin').text(data.OriginName);

		var iconName = 'icon' + data.Type;

		bucket.find('#icon').addClass(iconName);

		bucket.find('#delete').click(function () {
			confirmDelete(data);
		});

		bucket.find('#edit').click(function () {
			editBucket(data);
		});

		bucket.find('#clone').click(function () {
			cloneBucket(data);
		});

		if (data.Segments) {
			bucket.find('#segments').text(data.Segments.length);
		}
		else
			bucket.find('#segmentGroup').remove();

		if (data.LegacyId)
			bucket.find('#legacyId').text(data.LegacyId);
		else
			bucket.find('#legacyIdGroup').remove();

		if (!data.Constraints) {
			bucket.find('#charLimitGroup').remove();
		}
		else {
			if (data.Constraints.Length)
				bucket.find('#charLimit').text(data.Constraints.Length);
			else
				bucket.find('#charLimitGroup').remove();
		}

		return bucket;
	}

	function cloneBucket(bucket) {
	    // clone the bucket
	    debugger;
		var newBucket = $.extend(true, {}, bucket);
		
		var editor = new Administration.ContentBucketEditor(self, newBucket, true);
		editor.show();
	}

	function editBucket(bucket) {
		var editor = new Administration.ContentBucketEditor(self, bucket);
		editor.show();
	}

	function viewBucketDetails(bucket) {
		var view = Administration.ContentBucketDetailView(self, bucket);
		view.show();
	}

	function confirmDelete(bucket) {
		var popup = new Popup();
		var confirmation = deleteTemplate.clone();

		confirmation.find('#name').text(bucket.Name);
		confirmation.find('#cancelButton').click(function () {
			popup.close();
		});
		confirmation.find('#submitButton').click(function () {
			deleteBucket(bucket, popup);
		});
		popup.show(confirmation);
	}

	function deleteBucket(bucket, popup) {
		Callback.submit('Content/DeleteBucket', { id: bucket.Id }, function () {
			popup.close();
			_pager.refreshCurrentPage();
		});
	}

	function createToolBar() {
		searchBox = Html.textBox('searchTextBox');

		var addNewClientButton = Html.anchorButton('defaultButton defaultButtonBox defaultAnchorbutton',
			'New Bucket',
			function () {
				var editor = new Administration.ContentBucketEditor(self);
				editor.show();
			}
		).kswid('addBucket');

		addNewClientButton.kswid('buttonAddClient');

		searchBox.kswid('inputSearch');
		searchBox.enter(applyQuery);

		var searchButton = Html.button('defaultButton searchButton', ' ', applyQuery);

		searchButton.kswid('buttonSearch');

		return Html.div(null, addNewClientButton, Html.div('searchArea', Html.div('searchTextArea', searchBox), searchButton));
	}

	return self;
};