﻿/// <reference path="../Common/_commonReference.js" />
$.namespace('Administration.Pages');

Administration.Pages.LicenseEditor = (function () {
	var self = {
		showNew: showNew,
		show: show
	};

	var _form, _template, _client, _license, _license;

	if (typeof clientId == "object")
		client = clientId;

	function showNew(client) {
		_form = null;
		_client = client;
		_license = null;
		getResources(setupNew);
	}

	function show(client, license) {
		_form = null;
		_client = client;
		_license = license;
		getResources(setupModuleView);
	}

	function setupNew() {
		_form = _template.creator.clone();
		_form.find('#parent-selector-container').append(createParentLicenseDropDown());
		_form.form();
		_form.find('#cancel').click(function () {
			if (!_license) {
				Administration.Navigation.showClients();
			} else {
				Administration.LicenseFlyout.refreshCurrent();
				setupModuleView();
			}
		});
		_form.find('#save').click(save);

		Page.switchPage(_form, focus);
	}

	function setupModuleView() {
		_form = _template.moduleView.clone();
		_form.find('#name').text(_license.Name);
		var parent = _form.find('#parent-license');
		if (!_license.ParentLicenseId) {
			parent.text('None');
		}
		else {
			Service.get({
				service: 'Administration/Licenses/' + _license.ParentLicenseId,
				success: function (data) {
					parent.text(data.Name);
				}
			});
		}
		
		_form.find('#application-container').append(new Administration.ApplicationList(_license));

		var moduleList = new Administration.ModuleList(_license);

		_form.find('#module-list').append(moduleList.create());
		_form.find('#available-module-list').append(moduleList.createSideBar());

		Page.switchPage(_form);
	}

	function getResources(callback) {
		$.when($.Deferred(getHtml), $.Deferred(getClient), $.Deferred(getLicense))
			.done(callback);
	}

	function getHtml(deferred) {
		if (_template) {
			deferred.resolve();
			return;
		}

		ResourceProvider.getHtml('Administration/LicenseEditor.html', function (data) {
			setupTemplate(data);
			deferred.resolve();
		});
	}

	function setupTemplate(data) {
		_template = {
			titleBar: data.find('#license-title-bar').remove(),
			moduleView: data.find('#license-module-view').remove(),
			creator: data.find('#license-creator').remove()
		};
	}

	function getClient(deferred) {
		if (typeof _client != 'string') {
			deferred.resolve();
			return;
		}

		Service.get({
			service: 'Administration/Clients/' + _client,
			success: function (data) {
				_client = data;
				deferred.resolve();
			}
		});
	}

	function getLicense(deferred) {
		if (typeof _license != 'string') {
			deferred.resolve();
			return;
		}

		Service.get({
			service: 'Administration/Licenses/' + _license,
			success: function (data) {
				_license = data;
				deferred.resolve();
			}
		});
	}

	function showView() {
		var moduleList = new Administration.ModuleList(license);

		var view = Html.div('listContentArea',
			createView(license),
			new Administration.ApplicationList(license),
			moduleList.create()
		);

		var sideBar = Html.div('listOptionsFrame',
			Html.div('listOptionsArea',
				moduleList.createSideBar())).kswid('divOptions');

		setTitle();

		Page.switchPage(Html.div(null, view, sideBar));
	}

	function showForm() {
		var subTitle;
		var form = create();

		if (license) {
			populateForm(form);

			subTitle = client.ClientName + ' > ' + license.Name;
		} else {
			subTitle = client.ClientName + ' > New License';
		}
		Page.setContentTitle(subTitle);
		Page.switchPage(form, function () {
			focus();
		});
	}

	function setTitle() {
		var subTitle = client.ClientName + ' > ';

		Callback.submit('Administration/GetClientLicenseNames', { clientId: client.Id }, function (data) {

			if (data.length == 1) {
				Page.setContentTitle(subTitle + license.Name);
			}

			var ret = Html.div(null, Html.div('contentTitle', subTitle));

			var options = {};
			for (var i = 0; i < data.length; i++) {
				var current = data[i];
				options[current.Id] = current.Name;
			}
			var dropDown = Html.dropDown('darkDropDown', null, options);
			dropDown.val(license.Id);
			dropDown.change(function () {
				getLicense(dropDown.val());
			});
			ret.empty();
			ret.append(Html.div('contentTitle', client.ClientName + ' >&nbsp;'), Html.div('contentTitleControl', dropDown));

			Page.setContentTitle(ret);
		});
	}

	function populateForm(form) {
		form.find('[name="name"]').val(license.Name);
		form.find('[name="notes"]').val(license.Notes);
	}

	function create() {
		//return Html.div('listContentArea',
		//			createForm()
		//		);
		if (licenseId) {
			createEditor();
		} else {
			createCreator();
		}
	}

	function createEditor() {
		var element = _template.editor.clone();
		return element;
	}

	function createCreator() {
		_form = _template.creator.clone();

		var subTitle;

		if (license) {
			populateForm(form);

			subTitle = client.ClientName + ' > ' + license.Name;
		} else {
			subTitle = client.ClientName + ' > New License';
		}

		Page.setContentTitle(subTitle);
		Page.switchPage(form, function () {
			focus();
		});
	}

	function save() {
		if (_license) {
			Service.put({
				service: 'Administration/Licenses/' + _license.Id,
				form: _form,
				data: { clientId: _client.Id },
				success: function (data) {
					_license = data;
					Administration.LicenseFlyout.refreshCurrent();
					setupModuleView();
				}
			});
		} else {
			Service.post({
				service: 'Administration/Licenses',
				form: _form,
				data: { clientId: _client.Id },
				success: function (data) {
					_license = data;
					Administration.LicenseFlyout.refreshCurrent();
					setupModuleView();
				}
			});
		}
	}

	function createForm() {

		//form = Html.div(
		//	'itemBox listItem form',
		//    Html.div(null , 
		//	    Html.div('iconClose')
		//			    .click(function () {
		//				    Administration.Navigation.showClients();
		//			    }),
		//		    Html.div(
		//			    'formSection',
		//			    Html.div('iconRequired', ' '),
		//			    Html.labeledField('wrapper', 
		//				    'large inlineLabel',
		//				    'License Name...',
		//				    Html.textBox('defaultTextBox', 'name')
		//					    .attr('data-kswid', 'inputLicenseName')
		//			    )),
		//		    Html.twoColumnContent('twoColumnDefault', 
		//			    Html.div(null,
		//				    Html.div('itemSubTitle', 'Parent License'),
		//				    Html.breakDiv(),
		//				    createParentLicenseDropDown()
		//			    ),
		//			    Html.div(null,
		//				    Html.div('itemSubTitle', 'Notes'),
		//				    Html.breakDiv(),
		//				    Html.div('textAreaPadding',
		//					    Html.labeledField('wrapper', 
		//						    'textAreaLabel',
		//						    'Notes...',
		//						    Html.textArea('textArea', 'notes')
		//							    .attr('data-kswid', 'inputLicenseNotes'))
		//				    )
		//			    )
		//		    )
		//        ).id('newItemContainer'),
		//        Html.breakDiv(),
		//		Html.div('padded buttonStrip',
		//			Html.button('defaultWhiteButton spaced', 'Cancel', function () {
		//			    if (!license) {
		//			    	Administration.Navigation.showClients();
		//			    } else {
		//			        Administration.LicenseFlyout.refreshCurrent();
		//			        showView();
		//			    }
		//			}).attr('data-kswid', 'buttonLicenseCancel'),
		//			Html.button('defaultButton spaced', 'Save License',
		//				function () {
		//					if (license) {
		//						Callback.submitMain('Administration/UpdateLicense',
		//							{ clientId: client.Id, licenseId: license.Id },
		//							function (data) {
		//								license = data;
		//								Administration.LicenseFlyout.refreshCurrent();
		//								showView();
		//							});
		//					}
		//					else {
		//						Callback.submitMain('Administration/CreateLicense',
		//							{ clientId: client.Id },
		//							function (data) {
		//								license = data;
		//								Administration.LicenseFlyout.refreshCurrent();
		//								showView();
		//							});
		//					}
		//				}).attr('data-kswid', 'buttonLicenseSave')
		//		),
		//		Html.breakDiv()
		//	);
		//return form;
	}

	function focus() {
		if (_form)
			_form.find('input[name="name"]').select();
	}

	function createView() {
		debugger;
		var element = _template.editor.clone();
		return element;
		return Html.div(
			'itemBox listItem',
			Html.anchorButton('iconClose', ' ',
				function () {
					Administration.Navigation.showClients();
				}).attr('data-kswid', 'buttonLicenseClose').show(),
			Html.anchorButton('iconEdit', ' ',
				function () {
					Page.clear(function () {
						showForm(license);
					}, true);
				}).attr('data-kswid', 'buttonLicenseEdit').show(),
			Html.div('itemSubTitle', 'License:')
				.css('float', 'left'),
			Html.div('itemTitle', license.Name)
				.attr('data-kswid', 'txtLicenseName'),
			Html.breakDiv(),
			Html.twoColumnContent('twoColumnDefault',
				Html.div(null,
					Html.div('itemSubTitle', 'Parent License'),
					createParentLicenseView()
				),
				Html.div(null,
					Html.div('itemSubTitle', 'Notes'),
					Html.div(null, license.Notes).attr('data-kswid', 'txtLicenseNotes')
				)
			),
			Html.breakDiv()
		);
	}

	function createParentLicenseView() {
		if (!license.ParentLicenseId)
			return Html.div(null, 'None').attr('data-kswid', 'txtLicenseParent');

		var container = Html.div(null, 'Loading...');

		Callback.submit('Administration/GetLicense', { licenseId: (license.ParentLicenseId) }, function (data) {
			container.empty();
			container.append(data.Name);
			container.attr('data-kswid', 'txtLicenseParent');
		});

		return container;
	}

	function createParentLicenseDropDown() {

		if (_license && _license.Children)
			return Html.div(null, "None (this license is a parent)");

		var container = Html.div(null, 'Loading...');
		var service = 'Administration/Clients/' + _client.Id + '/AvailableParentLicenses';

		Service.get({
			service: service,
			data: _license ? { licenseId: _license.Id } : null,
			success: function (data) {
				var options = { '': 'None' };

				for (var i = 0; i < data.Items.length; i++) {
					var licenseData = data.Items[i];
					options[licenseData.Id] = licenseData.Name;
				}

				var dropdown = Html.dropDown('dropDown', 'parentLicenseId', options);
				dropdown.attr('data-kswid', 'inputParentLicense');
				if (_license && _license.ParentLicenseId) {
					dropdown.val(_license.ParentLicenseId);
				}

				container.empty();
				container.append(dropdown);
			}
		});

		return container;
	}

	return self;
})();
