﻿var Administration = Administration || {};
Administration.Pages = Administration.Pages || {};

Administration.Pages.ClientUsers = function () {
    var USERS_PER_PAGE = 10;
	var MAXIMUM_NUMBER_OF_VISIBLE_PAGES = 10;

	var form;
	var listArea;
	var clientUserDiv;
	var toolbarDiv;
	var viewDiv;
	var createDiv;
	var deleteDiv;
	var disableDiv;
	var enableDiv;
	var permissionsDopdown;
	var searchListControl;
	var sortOptionsDropdown;
	var appliedFilter,
	    appliedQuery;
	var _pager;
	var _popup;
    
	var isUsingNewPager = true;

	function show() {
		ResourceProvider.getClientUserHtml(function (data) {
			_pager = new QueriedPager({
				itemsPerPage: USERS_PER_PAGE,
				maxVisiblePages: MAXIMUM_NUMBER_OF_VISIBLE_PAGES,
				pageNavigationCallback: function (page, itemsPerPage, offset) {
					var filter = appliedFilter;
					filter.offset = parseInt(offset, 10);
					filter.count = parseInt(itemsPerPage, 10);

					Callback.submit(
						'Administration/GetClientUsers',
						filter,
						function (clientUsers) {
							onData(clientUsers, offset);
						}
					);
				}
			});

			form = data.find('#clientUserAdministration');

			listArea = form.find('#clientUserDetailsContentArea');
			clientUserDiv = form.find('#clientUser').remove();
			toolbarDiv = form.find('#contentTitleBar').remove();
			viewDiv = form.find('#viewContentArea').remove();
			createDiv = form.find('#createContentArea').remove();
			deleteDiv = form.find('#deleteContentArea').remove();
			disableDiv = form.find('#disableContentArea').remove();
			enableDiv = form.find('#enableContentArea').remove();

			var sortOptionsDropDown = form.find('#sortOptionsSelect');

			sortOptionsDropdown = getSortOptionsDropDown();

			sortOptionsDropDown.append(sortOptionsDropdown);

			setupEvents();
			Page.setTitle('USERS');
			Page.setContentTitle(toolbarDiv);
			Page.switchPage(form);

			resetFilters();
		});
	}

	function setupEvents() {
		form.find('#logFilterResetButton').click(resetFilters);
		toolbarDiv.find('#clientUserSearch').click(applyQuery);
		form.find('#logFilterApplyButton').click(applyFilters);

		form.find('#includeAdmin').click(function () {
			checkBoxCheck(form.find('#includeAdmin'));
		});

		form.find('#includeUser').click(function () {
			checkBoxCheck(form.find('#includeUser'));
		});

		form.find('#includeNone').click(function () {
			checkBoxCheck(form.find('#includeNone'));
		});

		toolbarDiv.find('#addClientUser').click(function () {
			showEditPopup();
		});
	}

	function resetFilters() {
		//toolbarDiv.find('.searchTextBox').val('');

		if (!form.find('#includeAdmin').hasClass('checked'))
			form.find('#includeAdmin').addClass('checked');

		if (!form.find('#includeUser').hasClass('checked'))
			form.find('#includeUser').addClass('checked');

		if (!form.find('#includeNone').hasClass('checked'))
			form.find('#includeNone').addClass('checked');

		//sortOptionsDropdown.select('First Name');

		form.find('#sortOptionsDropDown').val('FirstName');
		form.find('#sortOptionsSelect').find('.current').text('First Name');
		//form.find("#sortOptionsDropDown option[text='FirstName']").attr("selected", "selected");

		form.find('#clientUserDetailsContentArea').empty();

		applyFilters();
	}

	function applyFilters() {
		appliedFilter = {};

		appliedFilter = Callback.fromContainer(form);
		
		appliedFilter['includeAdmin'] = form.find('#includeAdmin').hasClass('checked');
		appliedFilter['includeUser'] = form.find('#includeUser').hasClass('checked');
		appliedFilter['includeNone'] = form.find('#includeNone').hasClass('checked');
		appliedFilter['sort'] = form.find('#sortOptionsSelect').find(':selected').val();

		appliedFilter['searchQuery'] = appliedQuery;

		_pager.refresh();
	}
	
	function applyQuery() {
		appliedQuery = toolbarDiv.find('.searchTextBox').val();
		appliedFilter.searchQuery = appliedQuery;
		_pager.refresh();
	}

	function onData(data, offset) {
		if (0 < offset && offset >= data.Total) {
			_pager.gotoPage(data.Total / USERS_PER_PAGE);
		}
		else {
			populate(data);
			_pager.updatePager(data.Total);
		}
	}

	function populate(users) {
		listArea.empty();

		if (!users || users.Items.length == 0)
			listArea.append('No results found.');
		else {
			for (var i = 0; i < users.Items.length; i++) {
				var item = users.Items[i];
				var html = clientUserDiv.clone();

				html.find('.hiddenId').text(item.Id);
				html.find('.clientUserDataName').text(item.FirstName + ' ' + item.LastName);
				html.find('#clientUserName').text(item.ClientName);
				html.find('#clientUserPermissions').text(item.PermissionType);
				html.find('#clientUserCreated').text(Helper.dateFromDateTime(item.Created));
				if (item.LastLogin)
					html.find('#clientUserLastLogin').text(Helper.dateFromDateTime(item.LastLogin));
				else
					html.find('#clientUserLastLogin').text('N/A');

				if (!item.EmailAddress || item.EmailAddress == '')
					html.find('#clientUserEmailArea').hide();
				else
					html.find('#clientUserEmail').text('').append(Html.emailAnchor('emailAnchor', item.EmailAddress));

				if (!item.PhoneNumber || item.PhoneNumber == '')
					html.find('#clientUserPhoneArea').hide();
				else
					html.find('#clientUserPhone').text(item.PhoneNumber);

				if (!item.Disabled) {
					html.find('#clientUserIcon').addClass('iconEnabledClientUser');
					html.find('#clientUserIcon').removeClass('iconDisabledClientUser');
					html.find('.clientUserDisabled').addClass('isNotVisible');

					html.find('#clientUserActionEnable').hide();
					html.find('#clientUserActionDisable').show();
				} else {
					html.find('#clientUserIcon').addClass('iconDisabledClientUser');
					html.find('#clientUserIcon').removeClass('iconEnabledClientUser');
					html.find('.clientUserDisabled').removeClass('isNotVisible');

					html.find('#clientUserActionEnable').show();
					html.find('#clientUserActionDisable').hide();
				}

				setupUserEvents(html, item);

				listArea.append(html);
			}
		}

		listArea.append(_pager);
	}

	function showEditPopup(user) {
		_popup = new Popup();

		var popupContent = createDiv.clone();

		popupContent.find('#disabledUser').click(function () {
			checkBoxCheck(popupContent.find('#disabledUser'));
		});

		popupContent.find('#createUserCancel').click(function () {
			_popup.close();
		});

		popupContent.find('#createUserSave').click(function () {
			//var clientId = searchListControl.getSelectedValue();

		    var userId;

			if (user)
				userId = user.Id;
			else
				userId = null;

			Callback.submit(user ? 'Administration/UpdateClientUser' : 'Administration/CreateClientUser', getCreateEditUserParameters(popupContent, userId), function (newClient) {
				_popup.close();

				_pager.refreshCurrentPage();
			},
			function (error) {
				alert(error.Details);

				Wait.hide();
			});
		});

		var firstNameTextBox = popupContent.find('#firstName');
		var lastNameTextBox = popupContent.find('#lastName');
		var emailTextBox = popupContent.find('#emailAddress');
		var phoneNumberTextBox = popupContent.find('#phoneNumber');

		var firstNameFieldLabel = popupContent.find('.firstNameFieldLabel');
		var lastNameFieldLabel = popupContent.find('.lastNameFieldLabel');
		var emailFieldLabel = popupContent.find('.emailFieldLabel');
		var phoneFieldLabel = popupContent.find('.phoneFieldLabel');

		firstNameFieldLabel.append(Html.labeledField('', 'textAreaLabel', 'First Name...', firstNameTextBox));
		lastNameFieldLabel.append(Html.labeledField('', 'textAreaLabel', 'Last Name...', lastNameTextBox));
		emailFieldLabel.append(Html.labeledField('', 'textAreaLabel', 'Email Address...', emailTextBox));
		phoneFieldLabel.append(Html.labeledField('', 'textAreaLabel', 'Phone Number...', phoneNumberTextBox));

		searchListControl = Html.searchList();

		var columns = ['Client Name'];

		searchListControl.create(columns, null, popupContent.find('#searchList'), function () {
			setSearchListControlValues(user);
		});

		if (user) {
			popupContent.find('#newUserPrompt').remove();

			firstNameTextBox.val(user.FirstName);
			lastNameTextBox.val(user.LastName);
			emailTextBox.val(user.EmailAddress);
			phoneNumberTextBox.val(user.PhoneNumber);
			popupContent.find('.hiddenId').text(user.Id);

			if (user.Disabled)
				popupContent.find('#disabledUser').addClass('checked');
			else
				popupContent.find('#disabledUser').removeClass('checked');

			if (user.Locked) {
				var container = popupContent.find('#unlockUserContainer');

				container.show();

				var link = container.find('#unlockUserLink');

				link.click(function () {
					Callback.submit('Administration/UnlockClientUser', { id: user.Id }, function () {
						container.hide();
					});
				});
			}

			var resetPasswordContainer = popupContent.find('#resetPasswordContainer');

			resetPasswordContainer.show();

			resetPasswordContainer.find('#resetPassword').checkBox();

			permissionsDopdown = getUserPermissionsDropDown(user.PermissionType);

			popupContent.find('#createEditDialogTitle').text('Edit ' + user.FirstName + ' ' + user.LastName);
		} else {
			popupContent.find('#disabledUser').removeClass('checked');

			permissionsDopdown = getUserPermissionsDropDown(null);

			popupContent.find('#createEditdialogTitle').text('Add New Client User');
		}
	    
		popupContent.find('#createUserPermissionSelect').empty().append(permissionsDopdown);

		_popup.show(popupContent);
	}

	function setSearchListControlValues(user) {
		if (user) {
			searchListControl.setSelectedValue(user.ClientId, user.ClientName);
		}
	}

	function getUsers() {
		var selectedPage = listArea.find('.pagerSelectedPage').text();

		if (selectedPage == '')
			selectedPage = 1;

		submit(getSearchParameters(selectedPage - 1), false);
	}
	
	function submit(params, updateUriOnResponse) {
		Wait.show();

		Callback.submit('Administration/GetClientUsers', params, function (users) {
			populate(users);
			Wait.hide();
		},
		function (error) {
			alert(error.Details);

			Wait.hide();
		});
	}

	function getSearchParameters(page) {
		var params = Callback.fromContainer(form);

		params['offset'] = page * USERS_PER_PAGE;
		params['count'] = USERS_PER_PAGE;
		params['searchQuery'] = toolbarDiv.find('.searchTextBox').val();
		params['includeAdmin'] = form.find('#includeAdmin').hasClass('checked');
		params['includeUser'] = form.find('#includeUser').hasClass('checked');
		params['includeNone'] = form.find('#includeNone').hasClass('checked');
		params['sort'] = form.find('#sortOptionsSelect').find(':selected').val();

		return params;
	}

	function getCreateEditUserParameters(page, id) {
		var params = Callback.fromContainer(page);
		//string firstName, string lastName, string emailAddress, string phone, string clientId, bool disabled, string permissionType

		if (id)
			params['id'] = page.find('.hiddenId').text();

		params['clientId'] = page.find('#selectedValue').text();
		params['disabled'] = page.find('#disabledUser').hasClass('checked');
		params['resetPassword'] = page.find('#resetPassword').hasClass('checked');

		return params;
	}

	function setupUserEvents(html, item) {
		html.find('#clientUserActionEdit').click(function () {
			editClientUser(item);
		});

		html.find('#clientUserActionDelete').click(function () {
			deleteClientUser(item);
		});

		html.find('#clientUserActionDisable').click(function () {
			disableClientUser(item);
		});

		html.find('#clientUserActionEnable').click(function () {
			enableClientUser(item);
		});

		html.find('.clientUserDataName').click(function () {
			viewClientUser(item);
		});
	}

	function checkBoxCheck(checkBox) {
		if ($(checkBox).hasClass('checked')) {
			$(checkBox).removeClass('checked');
		} else {
			$(checkBox).addClass('checked');
		}
	}

	function getUserPermissionsDropDown(selectedValue) {
		if (!selectedValue || selectedValue == '')
			selectedValue = Helper.clientUserPermissions[0];

		var dropDown = Html.dropDown('floatRight dropDown', 'permissionType', Helper.clientUserPermissions, selectedValue);

		dropDown.attr('id', 'permissionTypeDropDown');

		dropDown.kswid('createUserPermissions');

		return dropDown;
	}

	function getSortOptionsDropDown() {
		var sortOptionDictionary = [{ name: 'First Name', value: 'FirstName' }, { name: 'Last Name', value: 'LastName' }, { name: 'Created Date', value: 'Created' }];
		//Took these out as we are not traking login yet and client name is not stored in the database anymore
		//{ name: 'Client Name', value: 'ClientName' }, , { name: 'Last Login', value: 'LastLogin' }

		var dropDown = Html.dropDown('floatRight dropDown', 'sortOptionsDropDown', sortOptionDictionary, sortOptionDictionary[0]);

		return dropDown;
	}

	function showDeletePopup(user) {
		_popup = new Popup();

		var popupContent = deleteDiv.clone();

		popupContent.find('.hiddenId').text(user.Id);
		popupContent.find('#deleteClientUserName').text(user.FirstName + ' ' + user.LastName);

		popupContent.find('#deleteUserCancel').click(function () {
			_popup.close();
		});

		popupContent.find('#deleteUserConfirm').click(function () {
			var userId = popupContent.find('.hiddenId').text();

			Callback.submit('Administration/DeleteClientUser', { id: userId }, function (newClient) {
				_popup.close();

				_pager.refreshCurrentPage();

				_popup.close();
			});
		});

		_popup.show(popupContent);
	}

	function showDisablePopup(user) {
		_popup = new Popup();

		var popupContent = disableDiv.clone();

		popupContent.find('#disableClientUserName').text(user.FirstName + ' ' + user.LastName);

		popupContent.find('#disableUserCancel').click(function () {
			_popup.close();
		});

		popupContent.find('#disableUserConfirm').click(function () {
			changeUserStatus(user, true);

			_popup.close();
		});

		_popup.show(popupContent);
	}

	function changeUserStatus(user, status) {
		user.Disabled = status;

		Callback.submit('Administration/UpdateClientUser', { id: user.Id, firstName: user.FirstName, lastName: user.LastName, emailAddress: user.EmailAddress, phoneNumber: user.PhoneNumber, clientId: user.ClientId, disabled: user.Disabled, permissionType: user.PermissionType }, function (newClient) {
			getUsers();
		});
	}

	function showEnablePopup(user) {
		_popup = new Popup();

		var popupContent = enableDiv.clone();

		popupContent.find('#enableClientUserName').text(user.FirstName + ' ' + user.LastName);

		popupContent.find('#enableUserCancel').click(function () {
			_popup.close();
		});

		popupContent.find('#enableUserConfirm').click(function () {
			changeUserStatus(user, false);

			_popup.close();
		});

		_popup.show(popupContent);
	}

	function viewClientUser(user) {
		_popup = new Popup();

		var popupContent = viewDiv.clone();

		popupContent.find('#viewUserFirstName').text(user.FirstName);
		popupContent.find('#viewUserLastName').text(user.LastName);
		popupContent.find('#viewUserEmail').text(user.EmailAddress);
		popupContent.find('#viewUserPhone').text(user.PhoneNumber);
		popupContent.find('#viewUserClientName').text(user.ClientName);
		popupContent.find('#permission').text(user.PermissionType);

		if (user.Disabled)
			popupContent.find('#viewUserDiabled').addClass('checked');
		else
			popupContent.find('#viewUserDiabled').removeClass('checked');

		popupContent.find('#createEditDialogTitle').text('View ' + user.FirstName + ' ' + user.LastName);

		permissionsDopdown = getUserPermissionsDropDown(user.PermissionType);

		popupContent.find('#buttonClose').click(function () {
			_popup.close();
		});

		_popup.show(popupContent);
	}

	function editClientUser(item) {
		showEditPopup(item);
	}

	function deleteClientUser(item) {
		showDeletePopup(item);
	}

	function disableClientUser(item) {
		showDisablePopup(item);
	}

	function enableClientUser(item) {
		showEnablePopup(item);
	}

	$(window).on('hashchange', function () {
	    if (_popup != null)
	        _popup.close();
	});
    
    return {
		show: show
    };
};

