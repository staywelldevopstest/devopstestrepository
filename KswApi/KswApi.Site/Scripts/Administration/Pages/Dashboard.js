﻿var Administration = Administration || {};
Administration.Pages = Administration.Pages || {};

Administration.Pages.Dashboard = function () {
	var form;
	function show() {
		ResourceProvider.getDashboard(function (data) {
			form = Html.div();

			form.append(data.find('#shortCodeWidget'));

			if (Global.User) {
				if (Global.User.Rights.contains('Read_Logs')) {
					form.append(data.find('#logsWidget'));
					form.find('#viewLogsButton').click(function () {
						Administration.Navigation.showLogs();
					});
				}

				if (Global.User.Rights.contains('Manage_Client')) {
					form.append(data.find('#clientWidget'));
					form.find('#manageClientButton').click(function () {
						Administration.Navigation.showClients();
					});
				}
				
				if (Global.User.Rights.contains('Manage_Content')) {
					form.append(data.find('#contentWidget'));
					form.find('#manageContentButton').click(function () {
						Administration.Navigation.showContent();
					});
				}
			}

			Page.setTitle('DASHBOARD');
			Page.setContentTitle('');
			Page.switchPage(form);
		});
	}

	return {
		show: show
	};
};