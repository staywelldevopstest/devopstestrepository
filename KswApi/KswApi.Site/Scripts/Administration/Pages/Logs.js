﻿var Administration = Administration || {};
Administration.Pages = Administration.Pages || {};

Administration.Pages.Logs = function () {
	var LOGS_PER_PAGE = 10,
	    MAXIMUM_NUMBER_OF_VISIBLE_PAGES = 10;

	var form,
	    appliedFilter,
	    appliedQuery,
	    _pager;

	function show() {
		var showArguments = arguments;

		ResourceProvider.getLogHtml(function (data) {
			_pager = new QueriedPager({
				itemsPerPage: LOGS_PER_PAGE,
				maxVisiblePages: MAXIMUM_NUMBER_OF_VISIBLE_PAGES,
				pageNavigationCallback: function (page, itemsPerPage, offset) {
					var filter = appliedFilter;
					filter.offset = parseInt(offset, 10);
					filter.count = parseInt(itemsPerPage, 10);

					Callback.submit(
						'Logging/Index',
						filter,
						onData
					);
				}
			});
			
			form = data.find('#logSearchForm');

			setupEvents();

			if (showArguments.length >= 2 && showArguments[0] == 'view') {
				getLog(showArguments[1]);
			}
			else if (showArguments.length >= 6 && showArguments[0] == 'list') {
				initFilters.apply(this, showArguments);
			}
			else {
				resetFilters();
			}

			applyFilters();

			Page.setTitle('LOGS');
			Page.setContentTitle('Notifications');
			Page.switchPage(form);
			form.find('.logDateRangePicker').daterangepicker();
		});
	}
	
	function setupEvents() {
		form.find('#includeWarnings').click(function () {
			checkBoxCheck(form.find('#includeWarnings'));
		});

		form.find('#includeErrors').click(function () {
			checkBoxCheck(form.find('#includeErrors'));
		});

		form.find('#includeInfo').click(function () {
			checkBoxCheck(form.find('#includeInfo'));
		});

		form.find('#logFilterApplyButton').click(applyFilters);
		form.find('#logFilterResetButton').click(function () {
			Administration.Navigation.setLogPage();
			resetFilters();
			applyFilters();
		});
	}

	function resetFilters() {
		var toDate = new Date();
		var fromDate = new Date();
		fromDate.setDate(fromDate.getDay() - 7);

		form.find('.defaultCheckBox').addClass('checked');
		form.find('.logDateRangePicker').val(Helper.getFormattedDate(fromDate) + ' - ' + Helper.getFormattedDate(toDate));
		$("#clientFilter option[text='All Clients']").attr("selected", "selected");
		$("#licenseFilter option[text='All Licenses']").attr("selected", "selected");
	}
	
	function applyFilters() {
		appliedFilter = {};
		
		appliedFilter = Callback.fromContainer(form);

		appliedFilter['IncludeWarnings'] = form.find('#includeWarnings').hasClass('checked');
		appliedFilter['IncludeErrors'] = form.find('#includeErrors').hasClass('checked');
		appliedFilter['IncludeInfo'] = form.find('#includeInfo').hasClass('checked');
		appliedFilter['DateRange'] = form.find('.logDateRangePicker').val();
		appliedFilter['ClientFilter'] = form.find('#clientFilter').find(':selected').val();
		appliedFilter['LicenseFilter'] = form.find('#licenseFilter').find(':selected').val();

		_pager.refresh();
	}

	function onData(data) {
		populate(data);
		_pager.updatePager(data.Total);
	}

	function populate(logs) {
		var singleLogDetailArea = form.find('#logSingleDetailContentArea');
		singleLogDetailArea.hide();

		var logList = form.find('#logDetailsContentArea');
		logList.empty();
		logList.show();

		if (logs == 'No Results Found') {
			logList.html(Html.div("logNoResults", logs));
		} else {
			for (var i = 0; i < logs.Items.length; i++) {
				var log = logs.Items[i];

				var div = getLogListDiv(log);

				logList.append(div);
			}
			logList.append(_pager);
		}
	}

	function getLog(id) {
		Callback.submit('Reporting/GetLog', { id: id }, function(data) {
			showDetailView(data, false);
		});
	}

	function initFilters(view, page, includeWarnings, includeErrors, includeInfo, dateRange, clientFilter, licenseFilter) {
		if (includeWarnings ==  'true')
			form.find('#includeWarnings').addClass('checked');
		else
			form.find('#includeWarnings').removeClass('checked');

		if (includeErrors == 'true')
			form.find('#includeErrors').addClass('checked');
		else
			form.find('#includeErrors').removeClass('checked');

		if (includeInfo == 'true')
			form.find('#includeInfo').addClass('checked');
		else
			form.find('#includeInfo').removeClass('checked');


		form.find('.logDateRangePicker').val(dateRange);
	}

	function checkBoxCheck(checkBox) {
		if ($(checkBox).hasClass('checked')) {
			$(checkBox).removeClass('checked');
		} else {
			$(checkBox).addClass('checked');
		}
	}

	function getLogListDiv(log) {
		var div = $('<div>').addClass('logDetail')
			.click(function () { showDetailView(log, true); })
			.append(
				$('<div>') //classless div
					.append(
						$('<div>').addClass('logDetailMainInfo')
							.append(
								$('<div>').addClass(getDetailTypeIconClass(log)),
								$('<div>').addClass('logDetailType').text(getDetailTypeText(log)),
								$('<div>').addClass('logDetailTitle').text(log.Title)
							)
					)
					.append(
						$('<div>').addClass('logDetailAdditionalInfo')
							.append(
								$('<div>').addClass('logDetailDate').text(Helper.getFormattedDateTime(log.Date)),
								$('<div>').addClass('logDetailVerb').text(log.Verb),
								$('<div>').addClass('logDetailUrl').text(log.Path)
							)
					)
			)
			.append(
				$('<div>').addClass('logDetailData').text(log.Message)
			);

		return div;
	}

	function createDetailForm(log) {
		var logDate = new Date(log.Date);

		//Date and ID at the top
		var detailDateIdDiv = Html.div('logDateIdArea', '');
		var detailTitleDateDiv = Html.div('logDate', logDate.toLocaleString());
		detailTitleDateDiv.attr('data-kswid', 'logDetDate');

		var detailTitleIdDiv = Html.div('logId', 'ID:' + log.Id);

		detailDateIdDiv.append(detailTitleDateDiv);
		detailDateIdDiv.append(detailTitleIdDiv);

		//Title Area
		var detailTitleDiv = Html.div('logSingleDetailTitle', log.Title);
		detailTitleDiv.attr('data-kswid', 'logDetTitle');

		var detailTitleDiv2 = Html.div('logSingleDetailTitle', "HTTP Status Code <div class='logHttpStausCode'>" + log.ResponseCode + "</div>");
		var detailTitleAreaDiv = Html.div('logSingleDetailTitleArea');
		var detailDiv = Html.div('logSingleDetailArea', '');

		detailTitleAreaDiv.append(detailTitleDiv);
		detailTitleAreaDiv.append(detailTitleDiv2);
		detailDiv.append(detailTitleAreaDiv);

		//Request Sub Section
		var subSectionTitle = Html.div('logSingleDetailSectionTitle', 'REQUEST');
		var subSectionSubTitle = Html.div('logSingleDetailSectionSubtitle', ' - What you sent KSW API');
		var subSection = Html.div('logSingleDetailSection', '');

		subSection.append(Html.div('logSingleDetailSectionUrl', 'URL: '));
		subSection.append(Html.div('logSingleDetailSectionUrl', log.Verb).attr('data-kswid', 'logDetUrlType'));
		subSection.append(Html.div('logSingleDetailSectionUrlValue', log.Path).attr('data-kswid', 'logDetUrl'));

		//Body Section
		var bodySectionTitle = Html.div('logSingleDetailSectionTitle', 'BODY');
		var body = Html.div('logSingleDetailSection', '');

		body.append(Html.div('logSingleDefaultSectionBody', log.Body));

		//add everything to the content area
		var contentArea = Html.div('', '');
		contentArea.empty();

		contentArea.append(detailDateIdDiv);
		contentArea.append(detailDiv);
		contentArea.append(subSectionTitle);
		contentArea.append(subSectionSubTitle);
		contentArea.append(subSection);
		contentArea.append(bodySectionTitle);
		contentArea.append(body);

		return contentArea;
	}

	function showDetailView(log, updateUriOnResponse) {
		if (updateUriOnResponse)
			Administration.Navigation.setLogPage('view', log.Id);
		
		var listContentArea = form.find('#logDetailsContentArea');
		listContentArea.hide();

		var singleLogDetailArea = form.find('#logSingleDetailContentArea');
		singleLogDetailArea.empty();
		singleLogDetailArea.show();

		var detailForm = createDetailForm(log);

		singleLogDetailArea.append(detailForm);
		singleLogDetailArea.show();
	}

	function getDetailTypeIconClass(log) {
		if (log.SubType == 'Exception')
			return 'logDetailTypeIcon iconLogException';
		else if (log.SubType == 'Warning')
			return 'logDetailTypeIcon iconLogWarning';
		else
			return 'logDetailTypeIcon iconLogInfo';
	}

	function getDetailTypeText(log) {
		if (log.SubType == 'Exception')
			return 'ERROR';
		else if (log.SubType == 'Warning')
			return 'WARNING';
		else
			return 'INFO';
	}
	
	return {
		show: show
	};
};
