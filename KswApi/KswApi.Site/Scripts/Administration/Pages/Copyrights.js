﻿var Administration = Administration || {};
Administration.Pages = Administration.Pages || {};

Administration.InsertMode = Administration.InsertMode || {
	INSERT_DEFAULT: 1,
	INSERT_EXISTING: 2,
	INSERT_NEW: 3
};

Administration.Pages.Copyrights = function () {
	var MAX_ITEMS_PER_CALL = 100;

	var $copyrightItemTemplate,
		$copyrightEditor,
	    $newCopyrightEditor,
	    $defaultCopyright,
		$listArea;

	var form,
	    template,
		self = {
			'show': show,
			'createCopyright': createCopyright
		};
	
	function show() {
		ResourceProvider.getCopyrights(function (data) {
			template = {
				bucketListItem: data.find('#bucketListItem').remove(),
				lastBucketListItem: data.find('#lastBucketListItem').remove()
			};

			form = data.find('#copyrightSection');
			$listArea = form.find('#copyrightList');

			$copyrightItemTemplate = data.find('#copyrightItemTemplate');
			$copyrightEditor = data.find('#copyrightEditor');

			Page.setTitle("COPYRIGHT MANAGER");
			Page.setContentTitle(createToolBar());
			Page.switchPage(form);

			$newCopyrightEditor = new Administration.CopyrightEditor({ 'template': $copyrightEditor, '$copyrightTemplate': $copyrightItemTemplate.clone(), 'createCopyright': createCopyright, '$listArea': form.find('#copyrightList') });
			form.prepend($newCopyrightEditor);

			update();
		});
	}
	
	function update() {
		Callback.submit(
			'Administration/GetCopyrights',
			{ 'offset': 0, 'count': 20, 'query': '', 'includeBucketCount': true },
			populate
		);
	}
	
	function populate(copyrights) {
		$listArea.empty();

		$defaultCopyright = null;
		while (copyrights.Items.length > 0)
		{
			var copyright = copyrights.Items.splice(0, 1)[0];
			createCopyright(copyright, Administration.InsertMode.INSERT_EXISTING);
		}
		
		if (copyrights.Default) {
			createCopyright(copyrights.Default, Administration.InsertMode.INSERT_DEFAULT);
		}
	}
	
	function createCopyright(copyright, uiInsertMode) {
		var $copyright = $copyrightItemTemplate.clone();
		$copyright.find('#copyrightContent').html(copyright.Value);

		if (uiInsertMode == Administration.InsertMode.INSERT_DEFAULT) {
			$copyright.kswid('globalCopyright');
			$copyright.find('#copyrightDefaultArea').css('display', 'block');
			$copyright.find('#copyrightDeleteContainer').css('display', 'none');
			$copyright.find('input[name="isDefault"]').val(true);
		}
		else {
			$copyright.find('input[name="copyrightId"]').val(copyright.Id);
		}

		if (copyright.BucketCount) {
			$copyright.find('#copyrightBuckets').text(copyright.BucketCount + ' buckets');
		}
		else {
			$copyright.find('#copyrightBuckets').text('0 buckets');
		}

		var $copyrightEditorClone = $copyrightEditor.clone();
		var $copyrightInlineEditor = new Administration.CopyrightEditor({ 'template': $copyrightEditorClone, '$copyright': $copyright, 'copyright': copyright });
		$copyright.append($copyrightInlineEditor);

		setupCopyrightEvents($copyright, $copyrightInlineEditor);

		switch (uiInsertMode) {
			case Administration.InsertMode.INSERT_DEFAULT:
				$listArea.prepend($copyright);
				break;
			case Administration.InsertMode.INSERT_EXISTING:
				$listArea.append($copyright);
				break;
			case Administration.InsertMode.INSERT_NEW:
				$listArea.find('div[data-kswid="globalCopyright"]').after($copyright);
				break;
		}
	}

	function setupCopyrightEvents($item, $copyrightInlineEditor) {
		$item.find('#copyrightEdit').click(function() {
			$copyrightInlineEditor.show();
		});

		$item.find('#copyrightClone').click(function () {
			var value = $item.find('#copyrightContent').html();
			$newCopyrightEditor.find('textarea[name="copyrightEditorTextArea"]').val(value);
			$newCopyrightEditor.show();
		});

		$item.find('#copyrightDelete').click(function () {
			showDeleteDialog($item);
		});

		$item.find('#copyrightBuckets').click(function() {
			toggleBucketList($item);
		});
	}

	function createToolBar() {
		var addNewCopyrightButton = Html.anchorButton('defaultButton defaultButtonBox defaultAnchorbutton',
			'Add New Copyright',
			function () {
				$newCopyrightEditor.fadeIn();
			}
		).kswid('addCopyright');
		
		return Html.div(null, addNewCopyrightButton);
	}

	function toggleBucketList($item) {
		var $bucketList = $item.find('#copyrightBucketList');
		var bucketListContents = $bucketList.html();
		
		if (!bucketListContents) {
			var isDefault = $item.find('input[name="isDefault"]').val();
			var copyrightId = isDefault ? '' : $item.find('input[name="copyrightId"]').val();

			fetchBuckets($bucketList, copyrightId, 0, MAX_ITEMS_PER_CALL, 0);
		}

		$item.find('#copyrightBucketList').toggle();
	}
	
	function fetchBuckets($bucketList, copyrightId, offset, count, currentItemIndex) {
		$.ajax({
			url: Global.ApplicationPath + 'Content/Buckets',
			data: { 'offset': offset, 'count': count, 'query': '', 'copyrightId': copyrightId },
			type: 'GET',
			success: function(buckets) {
				while (buckets.Items.length > 0) {
					var bucket = buckets.Items.splice(0, 1)[0];

					var $bucketItem = template.bucketListItem.clone();
					$bucketItem.kswid('bucketName');
					$bucketItem.text(bucket.Name);
					$bucketList.append($bucketItem);

					currentItemIndex++;
				}
				
				if (offset + count < buckets.Total) {
					fetchBuckets($bucketList, copyrightId, offset + MAX_ITEMS_PER_CALL, MAX_ITEMS_PER_CALL, currentItemIndex);
				}
				else {
					$bucketList.append(template.lastBucketListItem.clone());
				}
			}
		});
	}

	function showDeleteDialog($item) {
		var copyrightId = $item.find('input[name="copyrightId"]').val();
		var copyrightContent = $item.find('#copyrightContent').html();

		var popup = new Popup();

		var cancelButton = Html.button('defaultWhiteButton spaced', 'Cancel', function () { popup.close(); });

		cancelButton.kswid('buttonDeleteCancel');

		var deleteButton = Html.button('defaultOrangeButton spaced', 'Delete Copyright', function () {
			$.ajax({
				url: Global.ApplicationPath + 'Administration/DeleteCopyright',
				data: { 'copyrightId': copyrightId },
				type: 'POST',
				success: function (copyrights) {
					update();
					popup.close();
				},
				failure: function () {

				}
			});
		});

		deleteButton.kswid('buttonDeleteConfirm');

		var dialog = Html.div(
			'dialog',
			Html.spacer(),
			Html.div('centered',
				Html.label(null, 'Are you sure you would like to '),
				Html.label('emphasized', 'DELETE'),
				Html.label(null, ' copyright')
			),
			Html.spacer(),
			Html.div('large', copyrightContent),
			Html.spacer(),
			Html.div(null,
				Html.div(null, 'All content and content buckets that use this copyright will be reassigned to use the default copyright.'),
				Html.spacer()
			).kswid('delCopyrightText'),
			Html.div('padded buttonStrip',
				cancelButton,
				deleteButton
			),
			Html.breakDiv()
		);

		dialog.kswid('deleteCopyrightDialog');

		popup.show(dialog);
	}

	return self;
};