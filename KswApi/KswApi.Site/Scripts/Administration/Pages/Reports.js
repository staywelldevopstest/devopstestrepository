﻿var Administration = Administration || {};
Administration.Pages = Administration.Pages || {};

Administration.Pages.Reports = function () {
    var form;
    var reportData;
    var reportSection;
    var licenseDataGroup;
    var showing = false;

    function show() {
        if (showing)
            return;
        
        ResourceProvider.getReportsHtml(function (data) {
            showing = true;
            
            form = data.find('#reportsForm');
            reportData = data.find('#reportData');
            reportSection = reportData.find('.reportSection').remove();
            licenseDataGroup = reportSection.find('.reportLicenseDataGroup').remove();

            reportData.empty();

            reportData.append('Select filter options');

            var monthDropDownDiv = data.find('#dropdowns');
            monthDropDownDiv.append(getMonthDropdown(), '&nbsp;&nbsp;&nbsp;&nbsp;', getYearDropDown());
            
            setupEvents();

            Page.setTitle('REPORTS');
            Page.setContentTitle('');
            Page.switchPage(form);
            
            showing = false;
        });
    }
    
    function setupEvents() {
        form.find('#showReportData').click(function () {
            var month = form.find("[name='monthDropdown']").find(':selected').val();
            var year = form.find("[name='yearDropdown']").find(':selected').val();

            getReportData(month, year);
        });

        form.find('#exportToCsv').click(function () {
            var month = form.find("[name='monthDropdown']").find(':selected').val();
            var year = form.find("[name='yearDropdown']").find(':selected').val();

            location.href = "Reports/GetCsvExport?month= " + month + "&year=" + year;
        });
    }

    function getMonthDropdown() {
        var dropDown = Html.dropDown('dropDown medium floatLeft', 'monthDropdown', Helper.monthNameDictionary, new Date().getMonth() + 1);

        dropDown.kswid('month');
        
        return dropDown;
    }
    
    function getYearDropDown() {
        var yearList = {};
        var date = new Date();
        
        for (var i = 2013; i <= date.getFullYear(); i++) {
            yearList[i] = i;
        }

        var dropDown = Html.dropDown('dropDown small', 'yearDropdown', yearList, date.getFullYear());

        dropDown.kswid('year');

        return dropDown;
    }
    
    function getReportData(month, year) {
        Callback.submit('Reports/GetSmsBillingReport', { month: month, year: year }, function (data) {
            showReportData(data);
        });
    }

    function showReportData(data) {
        reportData.empty();

        if (!data || data.ClientData.lenght == 0)
            reportData.append("No Data Available");
        else {
            for (var i = 0; i < data.ClientData.length; i++) {
                var client = data.ClientData[i];
            
                var newReportSection = reportSection.clone();

                newReportSection.find('.reportClientName').text(client.ClientName);

                for (var j = 0; j < client.Licenses.length; j++) {
                    var license = client.Licenses[j];
                
                    var newLicenseDataGroup = licenseDataGroup.clone();
                
                    newLicenseDataGroup.find('.groupLicenseName').text(license.LicenseName);

                    newLicenseDataGroup.find('.shortCodeKeywordCount').text(license.ShortCodeKeywordCount);
                    newLicenseDataGroup.find('.longCodeCount').text(license.LongCodeCount);
                    newLicenseDataGroup.find('.outgoingMessageCount').text(license.OutgoingMessageCount);
                    newLicenseDataGroup.find('.incomingMessageCount').text(license.IncomingMessageCount);

                    newReportSection.append(newLicenseDataGroup);
                }

                reportData.append(newReportSection);
            }
        }
    }

    return {
        show: show
    };
};
