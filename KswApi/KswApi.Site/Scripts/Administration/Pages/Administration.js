﻿var Administration = Administration || {};
Administration.Pages = Administration.Pages || {};

Administration.Pages.Administration = function () {
	var form,
		self = {
			'show': show
		};

	function show() {
		ResourceProvider.getAdministration(function (data) {
			form = data.find('#administration');
			Page.setTitle("ADMINISTRATION");
			Page.setContentTitle('Administration');
			Page.switchPage(form);
		});
	}

	return self;
};