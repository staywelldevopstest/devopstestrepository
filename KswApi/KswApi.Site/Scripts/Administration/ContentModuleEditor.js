﻿Administration.ContentModuleEditor = function (license, module, moduleList) {
	var self = {
		create: create
	};

	var _buckets,
	    _collections,
	    _form,
	    _template,
	    _bucketSelector,
	    _selectedBuckets,
		_collectionPager,
		_selectedCollections;


	function create() {
		_form = Html.div('', 'Loading...');

		$.when($.Deferred(getBuckets), $.Deferred(getCollections), $.Deferred(getHtml))
			.done(populateForm);

		return _form;
	}

	function populateForm() {
		_form.empty();

		_form.append(_template.form.clone());

		_form.find('#edit-buckets').click(editBuckets);

		_form.find('#add-collection').click(addCollections);

		setupCollections();

		populateBuckets();
	}

	function populateBuckets() {
		var list = _form.find('#bucket-list');
		list.empty();
		_selectedBuckets = [];
		for (var i = 0; i < _buckets.Items.length; i++) {
			var current = _buckets.Items[i];
			list.append(createBucketItem(current));
			_selectedBuckets.push(current.Id);
		}
	}

	function createBucketItem(data) {
		var element = _template.item.clone();
		element.text(data.Name);
		return element;
	}

	function getBuckets(deferred) {
		Service.get({
			service: 'Administration/Licenses/' + license.Id + '/Modules/Content/Buckets',
			success: function (data) {
				_buckets = data;
				deferred.resolve();
			}
		});
	}

	function getHtml(deferred) {
		ResourceProvider.getHtml('Administration/LicenseEditor.html', function (data) {
			setupTemplate(data);
			deferred.resolve();
		});
	}

	function getCollections(deferred) {
		Service.get({
			service: 'Administration/Licenses/' + license.Id + '/Modules/Content/Collections',
			success: function (data) {
				_collections = data;
				
				_selectedCollections = [];
				for (var i = 0; i < data.Items.length; i++)
					_selectedCollections.push(data.Items[i].Id);

				deferred.resolve();
			}
		});
	}

	function setupCollections() {
		_collectionPager = new QueriedPager({
			pageNavigationCallback: showCollections
		});

		_form.find('#total-collection-count').text(_collections.Total);
		_form.find('#show-collection-details').click(function() {
			var element = $(this);
			
			if (element.data('details-hidden')) {
				element.data('details-hidden', false);
				element.text('hide details');
				_form.find('[data-detail]').show();
			}
			else {
				element.data('details-hidden', true);
				element.text('show details');
				_form.find('[data-detail]').hide();
			}
		});

		_collectionPager.updatePager(_collections.Total);

		_form.find('#pager-container').append(_collectionPager);

		_collectionPager.refresh();
	}

	function showCollections(page, count, offset) {
		var list = _form.find('#collection-list');

		list.empty();

		for (var i = offset; i < _collections.Items.length && i < offset + count; i++) {
			list.append(createCollectionItem(_collections.Items[i]));
		}
	}

	function createCollectionItem(data) {
		var element = _template.collectionItem.clone();

		element.find('#title').text(data.Title).click(function () {
			var returnPath = Navigation.get();
			var owner = {
				show: function () {
					Navigation.goto.apply(null, returnPath);
				}
			};
			var collectionView = new Content.Pages.CollectionView(Page.getTitle(), data.Id, owner);
			collectionView.show();
			return;
		});

		element.find('#blurb').text(data.Description);
		element.find('#expires').text(data.Expires ? Helper.getFormattedDate(data.Expires) : 'Never');
		element.find('#created').text(Helper.getFormattedDate(data.DateAdded));
		element.find('#modified').text(Helper.getFormattedDate(data.DateModified));
		element.find('#created-by').text(data.CreatedBy.FullName);
		if (data.ImageUri) {
			element.find('#image').attr('src', data.ImageUri + '.35x35');
		}
		element.find('#used-by').text(data.LicenseCount + ' License' + (data.LicenseCount == 1 ? '' : 's'));

		element.find('#remove').click(function () {
			Service.del({
				service: 'Administration/Licenses/' + license.Id + '/Modules/Content/Collections/' + data.Id,
				success: function () {
					$.Deferred(getCollections).done(function() {
						_collectionPager.refreshCurrentPage();
						_form.find('#total-collection-count').text(_collections.Total);
					});
				}
			});
		});

		return element;
	}

	function setupTemplate(data) {
		_template = {
			form: data.find('#content-module').remove(),
			item: data.find('#content-module-item').remove(),
			bucketDialog: data.find('#bucket-select-dialog').remove(),
			collectionItem: data.find('#content-module-collection').remove()
		};
	}

	function editBuckets() {
		_bucketSelector = new Content.Controls.BucketSelector(
			true,
			null,
			_selectedBuckets
		);

		var popup = new Popup();

		var dialog = _template.bucketDialog.clone();

		dialog.find('#cancel').click(function () {
			popup.close();
		});

		dialog.find('#update').click(function () {
			var ids = _bucketSelector.getSelectedIds();

			Service.put({
				service: 'Administration/Licenses/' + license.Id + '/Modules/Content/Buckets',
				data: { items: ids },
				success: function (data) {
					_buckets = data;
					_selectedBuckets = ids;
					populateBuckets();
					popup.close();
				}
			});
		});

		dialog.find('#bucket-selector').append(_bucketSelector);

		popup.show(dialog);
	}

	function addCollections() {
		var collectionSelector = new Content.Controls.CollectionSelector();
		collectionSelector.onSubmit(onCollectionsSelected);
		collectionSelector.show(_selectedCollections);
	}

	function onCollectionsSelected(selector) {
		var selected = selector.getSelected();

		if (selected.length == 0)
			return;

		var ids = [];
		for (var i = 0; i < selected.length; i++) {
			ids.push(selected[i].Id);
		}

		Service.post({
			service: 'Administration/Licenses/' + license.Id + '/Modules/Content/Collections',
			data: { items: ids },
			success: function () {
				$.Deferred(getCollections).done(function () {
					_collectionPager.refreshCurrentPage();
					_form.find('#total-collection-count').text(_collections.Total);
				});
			}
		});
	}

	return self;
};