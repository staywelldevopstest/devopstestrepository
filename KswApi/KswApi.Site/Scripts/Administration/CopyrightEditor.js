﻿var Administration = Administration || {};
Administration.CopyrightEditor = function (options) {//template, $copyright, copyright, newCopyrightTemplate) {
	var $container,
	    self = {
		    'show': show
	    };
	
	construct();

	function construct() {
		$container = options.template.clone();

		if (options.copyright) {
			$container.find('#copyrightEditorMessage').text('Edit Copyright:');
		}
		else {
			$container.find('#copyrightEditorMessage').text('New Copyright:');
		}

		setupEvents();
	}

	function show() {
		if (options) {
			if (options.copyright) {
				$container.find('textarea[name="copyrightEditorTextArea"]').val(options.copyright.Value);
			}
		}

		$container.fadeIn();
	}

	function setupEvents() {
		$container.find('a[data-kswid="copyrightCancel"]').click(function () {
			reset();
		});

		$container.find('textarea[name="copyrightEditorTextArea"]').keypress(function(e) {
			if (e.which == 13) {
				e.preventDefault();
			}
		});

		$container.find('button[data-kswid="saveCopyright"]').click(function () {
			var value = $container.find('textarea[name="copyrightEditorTextArea"]').val();

			if (options.copyright) {
				Callback.submit(
					'Administration/UpdateCopyright',
					{ 'copyrightId': options.copyright.Id || 'default', 'newContent': value },
					onUpdateData
				);
			}
			else {
				Callback.submit(
					'Administration/CreateCopyright',
					{ 'content': value },
					onCreateData
				);
			}
		});
	}
	
	function onUpdateData(data) {
		options.copyright.Value = data.Value;
		options.$copyright.find('#copyrightContent').html(options.copyright.Value);
		reset();
	}
	
	function onCreateData(data)
	{
		var newCopyright = { 'Id': data.Id, 'Value': data.Value };
		options.createCopyright(newCopyright, Administration.InsertMode.INSERT_NEW);
		reset();
	}
	
	function reset() {
		$container.fadeOut();
		$container.find('textarea[name="copyrightEditorTextArea"]').val('');
	}

	return $.extend($container, self);
};