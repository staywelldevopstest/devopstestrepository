﻿/// <reference path="../Common/_commonReference.js" />

var Administration = Administration || {};

Administration.ApplicationList = function (license) {
	var self = {

	};

	var template, list;

	construct();

	function construct() {
		ResourceProvider.getLicensesHtml(onHtml);

		var div = Html.div();

		self = $.extend(div, self);
	}

	function onHtml(html) {

		template = {
			confirmDelete: html.find('#deleteApplicationConfirmation').remove(),
			applicationManager: html.find('#applicationManagerTemplate').remove(),
			applicationView: html.find('#applicationViewTemplate').remove(),
			applicationEditor: html.find('#applicationEditorTemplate').remove(),
			jsonpSite: html.find('#jsonpSite').remove(),
			resetAppSecretDialog: html.find('#resetAppSecretDialog').remove()
		};

		getApplications();
	}

	function getApplications() {

		Callback.submit('Administration/GetApplications', { licenseId: license.Id }, onApplications);

	}

	function onApplications(data) {
		self.empty();

		var applicationManager = template.applicationManager.clone();

		list = applicationManager.find('#list');

		applicationManager.find('#addApplication').click(function () {
			var form = new Administration.ApplicationEditor(license, null, template);
			list.prepend(form);
		});

		self.append(applicationManager);

		if (data.Items) {
			for (var i = 0; i < data.Items.length; i++) {
				var view = new Administration.ApplicationView(license, data.Items[i], template);
				list.append(view);
			}
		}
	}

	return self;
};

Administration.ApplicationView = function (license, application, template) {
	var self = {

	};

	construct();

	function construct() {
		var view = template.applicationView.clone();
		view.find('#name').text(application.Name);
		view.find('#dateAdded').text(Helper.dateFromDateTime(application.DateAdded));
		view.find('#type').text(application.Type);
		view.find('#hoverButtons').showOnHover(view);
		view.find('#delete').click(function () {
			confirmDelete(application, view);
		});

		view.find('#applicationId').text(application.Id);

		var viewSecretButton = view.find('#viewSecret');
		viewSecretButton.click(function () {
			toggleSecret(view);
		});

		view.find('#edit').click(function () {
			var form = new Administration.ApplicationEditor(license, application, template);
			view.swapWith(form);
		});

		view.find('#resetAppSecret').click(function() {
			var dialog = template.resetAppSecretDialog.clone();
			
			var popup = new Popup();
			popup.show(dialog);

			dialog.find('#resetAppSecretCancel').click(function() {
				popup.close();
			});

			dialog.find('#resetAppSecretConfirm').click(function () {
				Callback.submit(
					'User/ResetLicenseApplicationSecret',
					{ 'licenseId': license.Id, 'applicationId': application.Id },
					function (appData) {
						application.Secret = appData.Secret;
						showSecret(view);
						popup.close();
					}
				);
			});
			
		});

		self = $.extend(view, self);
	}
	
	function toggleSecret(view) {
		if (view.find('#applicationSecret').text().contains('*')) {
			showSecret(view);
		}
		else {
			hideSecret(view);
		}
	}
	
	function showSecret(view) {
		view.find('#applicationSecret').text(application.Secret);
	}
	
	function hideSecret(view) {
		view.find('#applicationSecret').text('***************************');
	}

	function confirmDelete() {

		var dialog = template.confirmDelete.clone();

		var popup = new Popup();

		popup.show(dialog);

		dialog.find('#applicationName').text(application.Name);

		dialog.find('#cancelButton').click(function () {

			popup.close();

		});

		dialog.find('#deleteButton').click(function () {

			Callback.submit('Administration/DeleteApplication', { licenseId: license.Id, applicationId: application.Id }, function () {

				popup.close();

				self.hideAndRemove();

				Administration.LicenseFlyout.refreshCurrent();
			});

		});
	}

	return self;
};

Administration.ApplicationEditor = function (license, application, template) {
	var self = {

	};

	var siteList, siteCount = 0;

	construct();

	function construct() {
		var form = template.applicationEditor.clone();

		form.find('input[name="name"]').labelField('textboxLabel', 'Application Name...');
		form.find('textarea[name="description"]').labelField('textboxLabel', 'Description...');
		form.find('#cancel').click(cancel);
		form.find('#close').click(cancel);
		form.find('#save').click(save);
		var jsonpEnabled = form.find('#jsonpEnabled').checkBox();
		jsonpEnabled.click(function () {
			if (jsonpEnabled.hasClass('checked')) {
				siteCount = 0;
				addJsonpSite('');
				form.find('#jsonpDetails').show();
			} else {
				var details = form.find('#jsonpDetails');
				details.hide();
				details.find('#jsonpSiteList').empty();
				siteCount = 0;
			}
		});

		form.find('#addJsonpSite').click(function () { addJsonpSite(); });
		form.find('#jsonpDetails').hide();

		siteList = form.find('#jsonpSiteList');

		self = $.extend(form, self);

		if (application)
			populateForm();
	}

	function addJsonpSite(website) {

		var form = template.jsonpSite.clone();

		siteList.append(form);

		var siteTextbox = form.find('#website');
		siteTextbox.attr('name', 'jsonpWebsites.' + siteCount);
		if (website)
			siteTextbox.val(website);

		siteCount++;

		form.find('#number').text(siteCount + '.');
		form.find('#delete').click(function () {
			form.remove();
			siteCount--;
			refreshSites();
		});

		self.find('#jsonpSiteCount').text(siteCount);
	}

	function refreshSites() {
		siteCount = 0;
		siteList.children().each(function () {
			var form = $(this);
			form.find('#website').attr('name', 'jsonpWebsites.' + siteCount);
			siteCount++;
			form.find('#number').text(siteCount + '.');
		});

		self.find('#jsonpSiteCount').text(siteCount);
	}

	function populateForm() {
		self.find('input[name="name"]').val(application.Name);
		self.find('textarea[name="description"]').val(application.Description);

		if (application.EnableJsonp) {
			self.find('#jsonpEnabled').addClass('checked');
			self.find('#jsonpDetails').show();

			if (application.JsonpWebsites) {
				for (var i = 0; i < application.JsonpWebsites.length; i++) {
					addJsonpSite(application.JsonpWebsites[i]);
				}
			}
		}
		
		switch (application.Type) {
			case 'Confidential':
				self.find('input[name="type"]').eq(0).attr('checked', 'checked');
				break;
			case 'Public':
				self.find('input[name="type"]').eq(1).attr('checked', 'checked');
				break;
		}
	}

	function save() {
		var url;
		var parms = {
			licenseId: license.Id,
			enableJsonp: self.find('#jsonpEnabled').hasClass('checked')
		};

		if (application) {
			url = 'Administration/UpdateApplication';
			parms.applicationId = application.Id;
		}
		else {
			url = 'Administration/CreateApplication';
		}

		Callback.submitContainer(url,
							self,
							parms,
							onSave);
	}

	function onSave(data) {
		application = data;
		Administration.LicenseFlyout.refreshCurrent();
		var view = new Administration.ApplicationView(license, data, template);
		self.swapWith(view);
	}

	function cancel() {
		if (application) {
			var view = new Administration.ApplicationView(license, application, template);
			self.swapWith(view);
		}
		else {
			self.hideAndRemove();
		}
	}

	return self;
};


