﻿var Administration = Administration || {};

Administration.ModuleList = function (license) {
	this._license = license;
};

Administration.ModuleList.prototype = {
	_license: null,
	_main: null,
	_sideBar: null,
	_modules: {},

	create: function () {
		this._main = Html.div(null);
		
		this._sideBar = Html.div(null, 'Loading Modules...');
		
		var self = this;

		Callback.submit('Administration/GetModules', { licenseId: this._license.Id }, function (moduleList) {
			
			self.populate(moduleList);
		});

		return this._main;
	},

	createSideBar: function () {
		return this._sideBar;
	},

	populate: function (moduleList) {
		this._sideBar.empty();

		for (var i = 0; i < moduleList.Items.length; i++)
			this._main.append(this.createMainItem(moduleList.Items[i]));

		for (i = 0; i < moduleList.Available.length; i++)
			this._sideBar.append(this.createSideBarItem(moduleList.Available[i]));

	},

	highlightModule: function (type, highlight) {
		if (type) {
			var item = this._modules[type];
			if (!item)
				return;
			if (highlight) {
				item.addClass('attention');
			}
			else {
				item.removeClass('attention');
			}
		}
	},

	createSideBarItem: function (module) {
		var self = this;
		var checkBox;
		checkBox = Html.div('clear module', Html.checkBox(null, null, null, module.Name, false, function () {
			Callback.submit('Administration/AddModule', { licenseId: self._license.Id, moduleType: module.Type }, function (returnModule) {
				checkBox.remove();

				self._main.prepend(self.createMainItem(returnModule));
			});
		}).kswid('div' + module.Name.replace(' ', '')));
	    
		return checkBox;
	},

	createMainItem: function (module) {
		var self = this;

		var view = Html.div('itemBox listItem').kswid('div' + module.Type + 'Module');

		this._modules[module.Type] = view;

		var buttons = Html.deleteButton(
			function () {

				self.showDeleteConfirmation(module, function () {

					view.hideAndRemove();

					delete self._modules[module.Type];

					self._sideBar.append(self.createSideBarItem(module));

				});
			}).kswid('buttonRemoveModule');

		view.append(
			buttons,
			Html.div('itemTitle', module.Name),
			Html.div('someSpace'),
			this.createSpecificModule(module),
			Html.breakDiv()
		);

		return view;
	},

	createSpecificModule: function (module) {
		var item;

		switch (module.Type) {
			case 'SmsGateway':
				item = new Administration.SmsModuleEditor(this._license, module, this);
				break;
			case 'Content':
				item = new Administration.ContentModuleEditor(this._license, module, this);
				break;
			default:
				return null;
		}

		return item.create(module);
	},

	showDeleteConfirmation: function (module, callback) {
		var self = this;

		var popup = new Popup();
		var text = null, subText = null;

		switch (module.Type) {
			case 'SmsGateway':
				text = Html.div('large', 'Are you sure you would like to remove the SMS module from this license?');
				subText = 'This license will no longer be able to send or receive SMS messages';
				break;
			case 'Content':
				text = Html.div('large', 'Are you sure you would like to remove the Content module from this license?');
				subText = 'This license will no longer be able to access content.';
				break;
			case 'ManageContent':
				text = Html.div('large', 'Are you sure you would like to remove the Content Creation module from this license?');
				subText = 'This license will no longer be able to create or manage content.';
				break;
			default:
				text = Html.div('large', 'Are you sure you would like to remove the module this license?');
				subText = 'All associated features will be disabled for the license.';
				break;
		}

		var dialog = Html.div(
			'dialog',
			Html.spacer(),
			Html.div('centered',
				text
			),
			Html.spacer(),
			Html.div(null, subText),
			Html.spacer(),
			Html.div('buttonStrip',
				Html.button('defaultWhiteButton', 'Cancel', function () {
					popup.close();
				}).kswid('buttonDeleteCancel'),
				Html.button('defaultOrangeButton', 'Remove', function () {
					Callback.submit('Administration/RemoveModule', { licenseId: self._license.Id, moduleType: module.Type }, function (returnModule) {
						callback(returnModule);

						popup.close();
					});
				}).kswid('buttonDeleteConfirm')
			),
			Html.breakDiv()
		).kswid('deleteModuleDialog');

		popup.show(dialog);
	}
};
