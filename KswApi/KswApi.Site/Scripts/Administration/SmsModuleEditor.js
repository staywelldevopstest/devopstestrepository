﻿var Administration = Administration || {};

Administration.SmsModuleEditor = function (license, module, moduleList) {

	var self = {
		create: create,
		addAvailableShortCode: addAvailableShortCode,
		updateShortCodeHeader: updateShortCodeHeader,
		showNumberList: showNumberList,
		updateList: updateList,
		highlightModule: highlightModule
	};

	var type = null,
	    tab = null,
	    empty = true,
	    container = null,
	    list = null,
	    placeHolder = null;

	// Public:
	function create() {
		var view = Html.div(null);

		view.swapContent(createMain());

		return view;
	}
	
	function addAvailableShortCode(value) {
		module.AvailableShortCodes.push(value);
		module.AvailableShortCodes.sort();
	}

	// Private:
	function createMain() {

		container = Html.div(null, Html.div('Loading...'));

		Callback.submit('Administration/GetSmsGatewayModule', { licenseId: license.Id }, function (data) {
			populate(data);
		});

		return container;
	}
	
	function populate(data) {
		module = data;

		tab = new Tab();

		tab.addTab('Short Codes', function () {
			showShortCodeList();
		});

		tab.addTab('Numbers', function () {
			showNumberList();
		});

		container.swapContent(tab);
	}

	function showShortCodeList() {
		list = Html.div();

		var shortCodes = module.ShortCodes;

		if (shortCodes.length == 0) {
			empty = true;

			highlightModule(true);
		}
		else {
			empty = false;

			highlightModule(false);

			for (var i = 0; i < shortCodes.length; i++) {
				if (i > 0) {
					list.append(Html.div('listOptionsSeperator'));
				}
				var item = shortCodes[i];
				var view = new Administration.SmsShortCodeView(license, item, module, self);
				list.append(view.create());
			}
		}

		updateShortCodeHeader();

		updateList();

		tab.setContent(list);
	}

	function highlightModule(highlight) {
		moduleList.highlightModule(type, highlight);
	}

	function showNumberList(optionalToEdit) {
		list = Html.div();

		list = list;

		var longCodes = module.LongCodes;

		var editor = new Administration.LongCodeList(license, longCodes);

		tab.setTitleArea(Html.button('defaultButton', 'Buy A Number', function () {

			tab.setTitleArea(Html.div('contentTitle', 'ADDING A NUMBER'));

			var search = new Administration.LongCodeSearch(license, longCodes, self);

			tab.setContent(search.create());

			search.focus();
		}).kswid('longBuyNumber'));

		if (longCodes.length == 0) {
			tab.setContent(Html.div('large centered', 'No Numbers Added').kswid('longResult'));

			empty = true;

			highlightModule(true);
		} else {
			empty = false;

			highlightModule(false);

			tab.setContent(editor.create(optionalToEdit));
		}
	}

	function updateShortCodeHeader() {

		var available = module.AvailableShortCodes;

		if (!available || available.length == 0) {
			tab.setTitleArea('');
		}
		else {
			tab.setTitleArea(Html.button('defaultButton', 'Add Short Code', function () {

				tab.setTitleArea('');

				var editor = new Administration.SmsShortCodeEditor(license, null, module, self);

				if (list) {
					list.prepend(editor.create());
				}

				updateList();

			}).kswid('buttonAddShortCode'));
		}
	}

	function updateList() {
		if (!list)
			return;

		if (list.children().length == 0) {
			placeHolder = createShortCodePlaceHolder();
			list.append(placeHolder);
			highlightModule(true);
		}
		else if (placeHolder) {
			placeHolder.remove();
			placeHolder = null;
		}
	}

	function createShortCodePlaceHolder() {
		return Html.div('large centered', 'No Short Codes Added');
	}

	return self;
};

Administration.SmsShortCodeView = function (license, shortCode, module, moduleEditor) {
	var self = {
		create: create
	};

	var view = null;

	function create() {
		var list = Html.div();

		var keywordCount = 0;

		if (shortCode.Keywords) {
			keywordCount = shortCode.Keywords.length;
			for (var i = 0; i < keywordCount; i++) {
				var current = shortCode.Keywords[i];
				list.append(Html.div(null,
					Html.label('value', current.Value),
					' ',
					current.Website,
					' ',
					current.Format.toUpperCase()
				).kswid('keyword'));
			}
		}

		var buttons = Html.editAndDeleteButtons(function () {
			var editor = new Administration.SmsShortCodeEditor(license, shortCode, module, moduleEditor);
			view.swapWith(editor.create());
		}, function () {
			Callback.submit('Administration/DeleteShortCode', { licenseId: license.Id, shortCodeNumber: shortCode.Number }, function (data) {
				module.ShortCodes.remove(shortCode);
				view.remove();
				moduleEditor.addAvailableShortCode(data.Number);
				moduleEditor.updateShortCodeHeader();
				moduleEditor.updateList();
			});
		});

		view = Html.twoColumnContent('twoColumnModule',
			Html.div('itemTitle alternative', shortCode.Number).kswid('shortCodeNumber'),
			Html.div(null,
				Html.div('keywordArea',
					buttons,
					Html.div(null, 'Licensed Keywords: ', Html.label('value', keywordCount.toString())).kswid('licensedKeywords'),
					Html.spacer()
				),
				list,
				Html.breakDiv()
			)
		);

		buttons.showOnHover(view);

		return view;
	}

	return self;
};

Administration.SmsShortCodeEditor = function (license, shortCode, module, moduleEditor) {

	var self = {
		create: create
	};

	var	keywordCount = 0,
		editor,
		keyWordList,
		keywordTemplate,
		countLabel;

	function create() {
		editor = Html.div();
		
		ResourceProvider.getSmsModuleHtml(onHtml);

		return editor;
	}
	
	function onHtml(html) {
		keywordTemplate = html.find('#shortcodekeywordEditorTemplate').remove();
		
		var available = module.AvailableShortCodes;

		keyWordList = Html.div().kswid('divKeyWords');

		if (shortCode && shortCode.Keywords) {
			for (var i = 0; i < shortCode.Keywords.length; i++) {
				addKeyword(shortCode.Keywords[i]);
			}
		}

		countLabel = Html.label('value');

		updateCount();
		
		editor.append(Html.twoColumnContent('twoColumnModule',
			createShortCodeSelector(available),
			Html.div(null,
				Html.closeButton(function () { cancel(); }).kswid('buttonSMSClose'),
				Html.hidden('routeType', 'Keyword'),
				Html.div(null,
					Html.div('keywordArea',
						'Licensed Keywords*: ',
						countLabel,
						Html.anchorButton('iconAdd', '', function () {
							addKeyword();
							updateCount();
						}).kswid('buttonAddKey')
					),
					keyWordList,
					Html.saveAndCancelButtons(null, null, function () {
						if (shortCode) {
							Callback.submitContainer('Administration/UpdateShortCode', editor, { licenseId: license.Id, shortCodeNumber: shortCode.Number }, function (data) {
								//Update the short code
								for (var j = 0; j < module.ShortCodes.length; j++) {
									if (module.ShortCodes[j].Number == data.Number)
										module.ShortCodes[j] = data;

									break;
								}

								shortCode = data;

								var view = new Administration.SmsShortCodeView(license, data, module, moduleEditor);
								editor.swapWith(view.create());
							});
						}
						else {
							Callback.submitContainer('Administration/CreateShortCode', editor, { licenseId: license.Id }, function (data) {
								available.remove(data.Number);
								moduleEditor.updateShortCodeHeader();
								var view = new Administration.SmsShortCodeView(license, data, module, moduleEditor);
								editor.swapWith(view.create());
								moduleEditor.highlightModule(false);

								module.ShortCodes.push(data);
							});
						}
					},
					function () { cancel(); }
				)
			)
		)));
	}
	
	function updateCount() {
		countLabel.text(keywordCount);
	}

	function createShortCodeSelector(available) {
		if (available && available.length > 1 && !shortCode) {

			var options = [];
			for (var i = 0; i < available.length; i++) {
				options.push({ name: available[i], value: available[i] });
			}

			var ret = Html.dropDown('medium dropDown', 'number', options);
			if (shortCode) {
				ret.val(shortCode.Number);
			}

			return ret;
		}

		var number = '';

		if (shortCode && shortCode.Number) {
			number = shortCode.Number;
		} else if (available && available.length > 0) {
			number = available[0];
		}

		return Html.div(null, Html.div('itemTitle alternative', number), Html.hidden('number', number));
	}

	function cancel() {
		if (shortCode) {
			var view = new Administration.SmsShortCodeView(license, shortCode, module, moduleEditor);
			editor.swapWith(view.create());
		}
		else {
			if (shortCodeCount <= 0) {
				editor.swapWith(createEmptyShortCode());
			}
			else {
				editor.remove();
				moduleEditor.updateList();
			}
		}

		moduleEditor.updateShortCodeHeader();
	}

	function addKeyword(keyword) {
		var options = { 'Json': 'JSON', 'Xml': 'XML' };

		var keywordEditor = keywordTemplate.clone();

		var prefix = 'keywords.' + keywordCount + '.';

		var dropdown = Html.dropDown('small dropDown', prefix + 'format', options, keyword ? keyword.Format : '').kswid('selectKeyWordType');
		
		var formatPlaceholder = keywordEditor.find('#formatPlaceholder');

		formatPlaceholder.after(dropdown);
		formatPlaceholder.remove();
		
		keywordEditor.find('#keywordValue')
			.attr('name', prefix + 'value')
			.labelField('textboxLabel', 'Keyword...');
		
		keywordEditor.find('#keywordWebsite')
			.attr('name', prefix + 'website')
			.labelField('textboxLabel', 'Website...');

		if (keyword) {
			keywordEditor.find('#keywordValue').val(keyword.Value);
			keywordEditor.find('#keywordWebsite').val(keyword.Website);
		}
		
		keywordEditor.find('#delete').click(function () {
			keywordEditor.remove();
			keywordCount--;
			countLabel.text(keywordCount);
			touchKeywordList();
		});

		keyWordList.append(keywordEditor);
		
		keywordCount++;
	}
	
	function touchKeywordList() {
		var i = 0;
		keyWordList.children().each(function () {
			var keywordEditor = $(this);
			var prefix = 'keywords.' + i + '.';
			keywordEditor.find('#keywordValue').attr('name', prefix + 'value');
			keywordEditor.find('#keywordWebsite').attr('name', prefix + 'website');
			keywordEditor.find('select').attr('name', prefix + 'format');
			i++;
		});
	}


	return self;
};

Administration.LongCodeSearch = function(license, longCodes, editor) {
	var self = {
		constructor: constructor,
		create: create,
		focus: focus
	};

	var pageSize = 10;
	var container,
	    pagerContainer,
	    noMatch,
	    prompt,
	    longCodeBase,
	    resultContainer,
	    searchBox,
	    searchButton,
	    cancelButton,
	    availableLongCodes,
	    searchByNumber,
	    confirmTemplate,
		_pager;

	function constructor(licenseValue, longCodesValue, editorValue) {
		license = licenseValue;
		longCodes = longCodesValue;
		editor = editorValue;
	}

	function create() {
		container = Html.div();

		ResourceProvider.getSmsModuleHtml(function (html) {
			// parse objects
			var searchForm = html.find('#longCodeSearch');
			noMatch = searchForm.find('#noMatch');
			prompt = searchForm.find('#displayedResults');
			longCodeBase = searchForm.find('.buyLongCodeArea').remove();
			resultContainer = searchForm.find('#longCodeResults');
			searchBox = searchForm.find('#longCodeSearchBox');
			searchButton = searchForm.find('#longCodeSearchButton');
			cancelButton = searchForm.find('#cancelButton');
			pagerContainer = searchForm.find('#pagerContainer');
			searchByNumber = searchForm.find('#searchByNumber');
			confirmTemplate = html.find('#longCodeConfirm');

			// setup initial state
			clear();

			// setup callbacks
			searchButton.click(search);
			searchBox.enter(search);
			cancelButton.click(cancel);

			container.append(searchForm);

			searchBox.focus();

			_pager = new QueriedPager({
				itemsPerPage: 2, //USERS_PER_PAGE,
				maxVisiblePages: 4, //MAXIMUM_NUMBER_OF_VISIBLE_PAGES,
				pageNavigationCallback: function (page, itemsPerPage, offset) {
					
				}
			});
		});

		return container;
	}

	function clear() {
		pagerContainer.empty();
		resultContainer.empty();
		noMatch.hide();
		prompt.hide();
	}

	function showNoResultsFound() {
		noMatch.show();
		prompt.hide();
		resultContainer.empty();
	}

	function search() {
		clear();

		var filter = searchBox.val();

		var params = {
			licenseId: license.Id,
			offset: 0,
			count: 100,
			filter: filter,
			filterType: searchByNumber.is(':checked') ? 'Number' : 'Location'
		};

		Callback.submit('Administration/GetLongCodes', params, function(data) {
			showResults(data, filter);
		});
	}

	function showResults(results, filter) {

		if (results.Total == 0 || !results.Items) {
			availableLongCodes = null;
			showNoResultsFound();
			return;
		}

		prompt.empty();
		var promptValue;

		if (filter) {
			promptValue = 'Showing results for <span class="emphasized">' + filter + '</span> in the United States.';
		} else {
			promptValue = 'Showing results in the United States.';
		}

		prompt.append(promptValue);

		prompt.show();

		availableLongCodes = results.Items;

		if (availableLongCodes.length > pageSize) {
			var pager = new Pager(pageSize, function(offset, count) {
				showResultsPage(offset, count);
				pager.update(availableLongCodes.length);
			});
			pagerContainer.empty();
			pagerContainer.append(pager.create());
			pager.update(availableLongCodes.length);
		} else {
			showResultsPage(0, pageSize);
		}
	}

	function showResultsPage(offset, count) {
		resultContainer.empty();
		for (var i = 0; i < count; i++) {

			if (offset + i >= availableLongCodes.length)
				return;

			resultContainer.append(createLongCodeElement(availableLongCodes[offset + i]));
		}
	}

	function createLongCodeElement(longCode) {
		var element = longCodeBase.clone();
		element.find('.buylongCodeMainText').text(Helper.getFormattedPhoneNumber(longCode.Number));
		var matchedNumber = Helper.matchSearchWithResults(searchBox.val(), longCode.Number);
		var region = ' ';

		if (longCode.Region && longCode.State)
			region = longCode.Region + ', ' + longCode.State;
		else if (longCode.State)
			region = longCode.State;
		else if (longCode.Region)
			region = longCode.Region;

		element.find('.buyLongCodeRegionInfo').html(matchedNumber + '  ' + region);
		element.find('.buyLongCodeButton').click(function() {
			confirmPurchase(longCode);
		});
		return element;
	}

	function confirmPurchase(longCode) {
		var popup = new Popup();
		var confirmation = confirmTemplate.clone();

		confirmation.find('#number').text(Helper.getFormattedPhoneNumber(longCode.Number));
		confirmation.find('#cancelButton').click(function() {
			popup.close();
		});
		confirmation.find('#submitButton').click(function() {
			purchase(longCode);
			popup.close();
		});
		popup.show(confirmation);
	}

	function purchase(longCode) {
		Callback.submit('Administration/PurchaseLongCode', { licenseId: license.Id, number: longCode.Number }, function(data) {
			longCodes.unshift(data);
			editor.showNumberList(data);
		});
	}

	function focus() {
		if (searchBox)
			searchBox.focus();
	}

	function cancel() {
		editor.showNumberList();
	}

	return self;
};

Administration.LongCodeList = function (license, longCodes) {
	var template,
		container = null;

	function create(optionalToEdit) {
		container = Html.div();

		ResourceProvider.getSmsModuleHtml(function (html) {
			//parse objects

			template = {};
			template.viewer = html.find('#longCodeView').remove();
			template.editor = html.find('#longCodeEdit').remove();
			template.keywordViewer = template.viewer.find('#longCodeViewTemplate').remove();
			template.keywordEditor = template.editor.find('#longCodeKeywordTemplate').remove();
			//editDeleteButtons = viewTemplate.find('.itemModifyButtonsArea');
			template.catchAllEditor = template.editor.find('#longCodeCatchAllEditTemplate').remove();
			template.deleteDialog = html.find('#longCodeDeleteConfirm').remove();

			//Set up initial state
			if (longCodes && longCodes.length > 0) {
				showLongCodes(optionalToEdit);
			}

			//editDeleteButtons.showOnHover(container);

		});

		return container;
	};

	function showLongCodes(optionalToEdit) {
		container.empty();

		for (var i = 0; i < longCodes.length; i++) {
			var item = longCodes[i];
			if (optionalToEdit && item == optionalToEdit) {
				var itemEditor = new Administration.LongCodeEditor(license, item, longCodes, template);
				container.append(itemEditor.create());
			}
			else {
				var viewer = new Administration.LongCodeViewer(license, item, longCodes, template);
				container.append(viewer.create());
			}
		}
	};

	return {
		create: create
	};
};

Administration.LongCodeEditor = function (license, longCode, longCodeArray, template) {
	var element,
		detailContainer,
		keywordCount = 0,
		addKeywordArea,
		countLabel;

	function create() {

		keywordCount = longCode.Keywords ? longCode.Keywords.length : 0;

		element = template.editor.clone();

		element.find('.longCodeNumber').text(Helper.getFormattedPhoneNumber(longCode.Number));
		detailContainer = element.find('#longCodeEditDetail');

		var catchAllOption = element.find('.useLongCodeCatchAll');
		var keywordOption = element.find('.useLongCodeKeyword');
		addKeywordArea = element.find('#addKeywordArea');
		countLabel = element.find('#longCodeKeywordCount');
		var hiddenNumber = element.find('.hiddenLongCodeNumber');

		catchAllOption.attr('name', 'routeType:' + longCode.Number);
		keywordOption.attr('name', 'routeType:' + longCode.Number);
		hiddenNumber.attr('name', 'number:' + longCode.Number);

		hiddenNumber.val(longCode.Number);

		catchAllOption.click(function () {
			showCatchAllEditor(element, longCode);
		});

		keywordOption.click(function () {
			showKeywordListEditor(element, longCode);
		});

		switch (longCode.RouteType) {
			case 'Keyword':
				keywordOption.attr('checked', true);
				catchAllOption.attr('checked', false);
				showKeywordListEditor(element, longCode);
				break;
			case 'CatchAll':
				catchAllOption.attr('checked', true);
				keywordOption.attr('checked', false);
				showCatchAllEditor(element, longCode);
				break;
		}

		//Save/Cancel event bindings
		element.find('.editCancelButton').click(function () {
			//container.html(create());
			var viewer = new Administration.LongCodeViewer(license, longCode, longCodeArray, template);
			element.swapWith(viewer.create());
		});

		element.find('.editSaveButton').click(function () {
			Callback.submitContainer('Administration/UpdateLongCode', element, { licenseId: license.Id, longCodeNumber: longCode.Number }, function (data) {
				// update the long code
				longCodeArray.replace(longCode, data);
				longCode = data;

				var viewer = new Administration.LongCodeViewer(license, longCode, longCodeArray, template);
				element.swapWith(viewer.create());
			});
		});

		return element;
	}
	
	function touchKeywordList(keywordList) {
		var i = 0;
		keywordList.children().each(function () {
			var keywordEditor = $(this);
			var prefix = 'keywords.' + i + '.';

			keywordEditor.find('.longCodeKeywordEditKeyword').attr('name', prefix + 'value');
			keywordEditor.find('.longCodeKeywordEditWebsite').attr('name', prefix + 'website');
			keywordEditor.find('select').attr('name', prefix + 'format');
			i++;
		});
	}

	function showKeywordListEditor() {
		if (!longCode.Keywords)
			longCode.Keywords = [];

		detailContainer.empty();

		//Edit long code options and event bindings
		var addKeywordButton = addKeywordArea.find('#addKeywordButton');
		addKeywordButton.unbind();
		addKeywordButton.click(function () {
			createKeywordEditor(detailContainer, keywordCount, null);

			keywordCount++;

			countLabel.text(keywordCount);
		});

		addKeywordArea.show();

		//Creates edit templates for all existing keywords
		if (keywordCount > 0) {
			for (var i = 0; i < keywordCount; i++) {
				createKeywordEditor(detailContainer, i, longCode.Keywords[i]);
			}
		}

		countLabel.text(keywordCount);

		addKeywordArea.show();
	}

	function showCatchAllEditor() {
		addKeywordArea.hide();
		var catchAllEditor = template.catchAllEditor.clone();

		catchAllEditor.find('input[name="website"]')
			.val(longCode.Website)
			.labelField('textboxLabel', 'Website...');

		var options = { 'Json': 'JSON', 'Xml': 'XML' };

		var dropdown = catchAllEditor.find('.formatDropdown');

		dropdown.append(Html.dropDown('small dropDown', 'format', options, longCode.Format ? longCode.Format : '').kswid('selectKeyWordType'));

		element.find('#longCodeEditDetail').empty().append(catchAllEditor);
	}

	function createKeywordEditor(keywordList, index, keyword) {
		var options = { 'Json': 'JSON', 'Xml': 'XML' };

		var keywordTemplate = template.keywordEditor.clone();
		var dropdown = keywordTemplate.find('.formatDropdown');

		var longCodeKeywordEditInput = keywordTemplate.find('.longCodeKeywordEditKeyword');
		var longCodeWebsiteEditInput = keywordTemplate.find('.longCodeKeywordEditWebsite');

		longCodeKeywordEditInput.attr('name', 'keywords.' + index + '.value');
		longCodeWebsiteEditInput.attr('name', 'keywords.' + index + '.website');

		if (keyword) {
			longCodeKeywordEditInput.val(keyword.Value);
			longCodeWebsiteEditInput.val(keyword.Website);
		}

		var keywordFieldLabel = keywordTemplate.find('.keywordFieldLabel');
		var websiteFieldLabel = keywordTemplate.find('.websiteFieldLabel');

		keywordFieldLabel.append(Html.labeledField('', 'textAreaLabel', 'Keyword...', longCodeKeywordEditInput));
		websiteFieldLabel.append(Html.labeledField('', 'textAreaLabel', 'Website...', longCodeWebsiteEditInput));

		dropdown.append(Html.dropDown('small dropDown', 'keywords.' + index + '.format', options, keyword ? keyword.Format : '').kswid('selectKeyWordType'));

		keywordTemplate.find('#deleteLongCodeKeyword').click(function () {

			keywordTemplate.remove();

			keywordCount--;

			touchKeywordList(keywordList);

			countLabel.text(keywordCount);
		});

		keywordList.append(keywordTemplate);
	}

	return {
		create: create
	};
};

Administration.LongCodeViewer = function (license, longCode, longCodeArray, template) {
	var element;

	function create() {
		element = template.viewer.clone();
		var detailView = element.find('#longCodeDetail');
		var catchAllView = element.find('#catchAllDetail').remove();
		var keywordView = element.find('#keywordDetail').remove();

		element.find('.longCodeNumber').text(Helper.getFormattedPhoneNumber(longCode.Number));

		element.find('.iconEdit').click(function () {
			var itemEditor = new Administration.LongCodeEditor(license, longCode, longCodeArray, template);
			element.swapWith(itemEditor.create());
		});

		element.find('#deleteLongCode').click(function () {
			confirmLongCodeDelete(longCode);
		});

		if (longCode.RouteType == 'Keyword') {
			detailView.empty();
			detailView.append(keywordView);
			if (!longCode.Keywords || longCode.Keywords.length == 0) {
				element.find('.keywordCount').text('0');

				element.find('.longCodeKeywordInfo').empty();
			} else {
				element.find('.keywordCount').text(longCode.Keywords.length);
				var keywordList = element.find('#longCodeViewKeywordList');

				for (var j = 0; j < longCode.Keywords.length; j++) {
					var keyword = longCode.Keywords[j];

					var keywordElement = template.keywordViewer.clone();

					keywordElement.find('#keywordKeyword').text(keyword.Value);
					keywordElement.find('#keywordWebsite').text(keyword.Website);
					keywordElement.find('#keywordFormat').text(keyword.Format.toUpperCase());

					keywordList.append(keywordElement);
				}
			}
		} else {
			detailView.empty();
			catchAllView.find('#longCodeViewWebsite').text(longCode.Website);
			catchAllView.find('#longCodeViewFormat').text(longCode.Format.toUpperCase());
			detailView.append(catchAllView);
		}

		return element;
	}

	function confirmLongCodeDelete() {
		var popup = new Popup();
		var confirmation = template.deleteDialog.clone();

		confirmation.find('#number').text(Helper.getFormattedPhoneNumber(longCode.Number));
		confirmation.find('#cancelButton').click(function () {
			popup.close();
		});
		confirmation.find('#submitButton').click(function () {
			deleteLongCode(longCode);
			popup.close();
		});
		popup.show(confirmation);
	}

	function deleteLongCode() {
		Callback.submit('Administration/DeleteLongCode', { licenseId: license.Id, longCodeNumber: longCode.Number }, function () {
			longCodeArray.remove(longCode);

			element.remove();
		});
	}

	return {
		create: create
	};
};