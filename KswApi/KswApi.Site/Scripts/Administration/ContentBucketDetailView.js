﻿var Administration = Administration || {};

Administration.ContentBucketDetailView = function(list, bucket) {
	var self = {
		show: show
	};

	var popup, view;

	function show() {
		ResourceProvider.getContentBucketDetailView(onHtml);
	}
	
	function onHtml(html) {
		view = html.find('#view').remove();

		view.find('#title').text(bucket.Name);
		view.find('#slug').text(bucket.Slug);
		view.find('#typeImage').addClass('icon' + bucket.Type);
		view.find('#typeName').text(bucket.TypeDescription);
		view.find('#origin').text(bucket.OriginName);
		view.find('#description').text(bucket.Description);

		view.find('#features').empty().append(getFeatures);
		view.find('#editButton').click(edit);
		view.find('#closeButton').click(close);

		popup = new Popup();

		popup.show(view);
	}
	
	function getFeatures() {
		var array = [];

		addSegments(array);
		
		if (bucket.LegacyId) {
			separate(array);
			array.push($('<div class="left bold">Legacy Content ID#:&nbsp;</div>'));
			array.push($('<div class="left">').text(bucket.LegacyId).kswid('bucketLegacy'));
		}
		
		if (bucket.Constraints) {
			if (bucket.Constraints.Length) {
				separate(array);
				array.push($('<div class="left bold">Char Limit:&nbsp;</div>'));
				array.push($('<div class="left">').text(bucket.Constraints.Length).kswid('charLimit'));
			}
		}
		
		return array;
	}
	
	function separate(array) {
		if (array.length)
			array.push('<div class="left">&nbsp;&nbsp;|&nbsp;&nbsp;</div>');
	}
	
	function addSegments(array) {
		if (!bucket.Segments || bucket.Segments.length == 0)
			return;

		array.push($('<div class="left bold">Segments (' + bucket.Segments.length + '):&nbsp;</div>').kswid('bucketSegments'));

		var result = '';
		for (var i = 0; i < bucket.Segments.length; i++) {
			if (i != 0)
				result += ', ';
			result += bucket.Segments[i].Name;
		}

		array.push($('<div class="left">').text(result).kswid('bucketSegTitle'));
	}
	
	function edit() {
		var editor = new Administration.ContentBucketEditor(list, bucket);
		editor.show();
		close();
	}
	
	function close() {
		if (popup)
			popup.close();
	}

	return self;
};
