﻿/// <reference path="../Common/_commonReference.js" />

var Administration = Administration || {};

Administration.LicenseFlyout = function (client, container, editor) {
	this._client = client;
	this._container = container;
	this._editor = editor;
};

Administration.LicenseFlyout.show = function (clientId, clientName, container) {

	var selectContainer = $(container);

	var data = $(container).data('licenseFlyout');

	if (data) {
		data.hide();
		return;
	}

	if (Administration.LicenseFlyout._current) {
		Administration.LicenseFlyout._current.hide();
	}

	Administration.LicenseFlyout._current = new Administration.LicenseFlyout();

	Administration.LicenseFlyout._current.show(clientId, clientName, selectContainer);
};

Administration.LicenseFlyout.refreshCurrent = function () {
	if (Administration.LicenseFlyout._current) {
		Administration.LicenseFlyout._current.refresh();
	}
};

// class definition

Administration.LicenseFlyout._flyouts = { };

Administration.LicenseFlyout.prototype = {
	_client: null,
	_editor: null,
	_flyout: null,
	_container: null,
	_anchor: null,
	_searchBox: null,
	_licenseList: null,
	_searchValue: null,
	_clientName: null,
	_visible: false,
	_showing: false,
	toggle: function () {
		if (this._visible || this._showing) {
			this.hide();
		}
		else {
			this.show();
		}
	},
	show: function () {
		var self = this;
		var client = this._client;
		this._showing = true;
		
		if (Administration.LicenseFlyout._current && Administration.LicenseFlyout._current != this)
			Administration.LicenseFlyout._current.hide();

		if (this._flyout) {
			this.showFlyout(this._flyout);
			return;
		}

		Callback.submit('Administration/GetClientLicenseDetail', { clientId: client.Id }, function (data) {

			if (!self._showing)
				return;
			
			self._flyout = self.createFlyout(data);
			
			self.showFlyout(self._flyout);
		});
	},
	hide: function () {
		if (Administration.LicenseFlyout._current == this)
			Administration.LicenseFlyout._current = null;
		
		var self = this;
		this._showing = false;
		this._visible = false;
		if (self._flyout) {
			self._flyout.stop();

			self._flyout.hide('drop', { direction: 'left' }, 400, function () {
				self._flyout.detach();
			});
		}
	},
	getData: function () {
		var self = this;
		var client = this._client;
		Callback.submit('Administration/GetClientLicenseDetail', { clientId: client.Id }, function (data) {
			self.showFlyout(data);
		});
	},
	refresh: function () {
		this.search();
	},
	showFlyout: function (flyout) {
		var self = this;
		Administration.LicenseFlyout._current = this;
		flyout.stop();
		flyout.removeAttr('style');
		flyout.hide();
		this._container.append(flyout);
		flyout.show('drop', { direction: 'left' }, 400, function () {
			// ie hack to fix issue with body not expanding:
			$('body').css('height', '100%');
			self._searchBox.select();
		});
	},
	createFlyout: function (data) {
		var client = this._client;
		var self = this;

		return Html.div('flyout itemBox',
				Html.div('iconClose', ' ').click(function () { self.hide(); }),
				Html.div('flyoutTitle', data.ClientName + ' Licenses'),
				Html.spacer(2),
				Html.div('defaultButton centered', 'Add New License')
					.attr('data-kswid', 'buttonFlyLicAdd')
					.click(function () {
						Administration.Pages.LicenseEditor.showNew(client);
					}),
				Html.spacer(4),
				self.createSearch(),
				self.createLicenseList(data)
			)
			.attr('data-kswid', 'divClientFlyOut')
			.click(function(e) { e.stopPropagation(); } );
	},
	createSearch: function () {
		var self = this;
		
		self._searchBox = Html.textBox('textBox flyoutSearch')
			.attr('data-kswid', 'inputFlySearch')
			.keyup(function () {
				if (self._searchBox.val() != this._searchValue)
					self.search();
			});

		return Html.div(null,
				self._searchBox,
				Html.button('defaultButton searchButton', ' ',
					function () { self.search(); })
					.attr('data-kswid', 'buttonFlySearch'),
				Html.spacer(5)
			);
	},
	search: function () {
		var self = this;
		var client = this._client;
		this._searchValue = this._searchBox.val();
		Callback.submit('Administration/GetClientLicenseDetail',
			{ clientId: client.Id, search: this._searchValue },
			function (data) {
				//data.LicenseCount
				//data.ClientId
				//data.ClientName
				self._editor.setLicenseCount(data.LicenseCount);
				
				if (self._licenseList) {
					self._licenseList.hide('fade',
						{},
						50,
						function () {
							self._licenseList.remove();
							self._licenseList = self.createLicenseList(data);
							self._licenseList.hide();
							self._flyout.append(self._licenseList);
							self._licenseList.show('fade',
								{},
								50);
						});
				}
			});
	},
	createLicenseList: function (data) {
		var client = this._client;
		this._licenseList = Html.breakDiv();

		if (data.Licenses) {
			for (var i = 0; i < data.Licenses.length; i++) {
				var view = new Administration.LicenseFlyoutItem(client, data.Licenses[i], this);
				this._licenseList.append(view.create());
			}
		}

		return this._licenseList;
	}
	
};
