﻿var Administration = Administration || {};

Administration.ContentBucketEditor = function (list, bucket, clone) {
	var self = {
		show: show
	};

	var origin,
	    popup,
	    form,
	    nameBox,
	    slugSpan,
	    slugBox,
	    originContainer,
	    typeCheckboxes,
	    readOnlyCheckbox,
	    segmentList,
	    segmentTemplate,
	    segmentCountContainer,
		segmentCount = 0,
		deleteTemplate,
		copyrightSelector;

	function show() {
		ResourceProvider.getContentBucketEditor(onHtml);
	}

	function onHtml(html) {
		popup = new Popup();

		form = html.find('#form').remove();
		segmentTemplate = html.find('#segment').remove();
		deleteTemplate = html.find('#deleteConfirm').remove();

		form.find('#cancelButton').click(close);
		form.escape(close);

		form.find('#submitButton').click(submit);

		nameBox = form.find('#name');
		slugSpan = form.find('#slugSpan');
		slugBox = form.find('#slugBox');
		readOnlyCheckbox = form.find('#readOnly');
		segmentCountContainer = form.find('#segmentCount');
		segmentList = form.find('#segments');
		var typeBar = form.find('#typeBar');
		typeCheckboxes = typeBar.find('.defaultCheckBox');

		copyrightSelector = new Content.Controls.CopyrightSelector();
		form.find('#copyrightItemBox').append(copyrightSelector);

		if (clone) {
			initializeClone();
		}

		if (bucket) {
			initializeEdit(typeBar);
		}
		else {
			initializeNew();
		}

		

		readOnlyCheckbox.checkBox();

		form.find('#addSegment').click(function () { addSegment(); });

		typeCheckboxes.click(typeCheckboxClick);

		typeCheckboxes.each(function () {
			setupCheckbox($(this), typeBar);
		});

		showConstraints();

		originContainer = form.find('#originContainer');

		Callback.submit('Content/BucketOrigins', null, onOrigins);

		form.form();

		popup.show(form);

		segmentList.sortable({
			update: updateSegments
		});

		segmentList.disableSelection();

		nameBox.select();
	}

	function initializeNew() {
	    copyrightSelector.val(null);
	    
	    form.find('#editSlug').click(function () {
	        slugSpan.hide();
	        slugBox.show();
	        $(this).hide();
	    });

	    nameBox
	        .keypress(function(e) {
	            var cursor = Helper.getSelectionStart(e.target);
	            // ignore leading whitespace
	            if (e.which == 32 && cursor == 0) {
	                e.preventDefault();
	            }
	        })
	        .keyup(function() {
	            updateSlug(); // $(this).val());
	        });
	}

	function updateSegments() {
		var i = 0;
		segmentList.children().each(function () {
			var segment = $(this);
			var current = $(this).find('#segmentNumber');
			if (current.length == 0)
				return;
			var prefix = 'segments.' + i + '.';
			segment.find('#id').attr('name', prefix + 'id');
			segment.find('#segmentName').attr('name', prefix + 'name');
			segment.find('#slugValue').attr('name', prefix + 'slug');
			segment.find('#required').data('name', prefix + 'required');
			i++;
			current.text(i + '.');
		});
	}

	function initializeClone() {
		bucket = $.extend(true, {}, bucket);
		bucket.Name += ' - copy';
		bucket.Slug = Helper.getSlug(bucket.Slug + ' - copy');
	}

	function initializeEdit(typeBar) {
	    form.find('#editSlug').remove();
	    
		nameBox.val(bucket.Name);
		slugSpan.text(bucket.Slug);
		slugBox.val(bucket.Slug);
		if (bucket.ReadOnly)
			readOnlyCheckbox.addClass('checked');
		else
			readOnlyCheckbox.removeClass('checked');
		form.find('input[name="legacyId"]').val(bucket.LegacyId);

		typeCheckboxes.removeClass('checked');
		typeCheckboxes.filter('#type' + bucket.Type).addClass('checked');

		if (clone) {
			form.find('#title').text('Clone Bucket');
			form.find('#submitButton').text('Create Clone');
		}
		else {
			form.find('#title').text('Edit ' + bucket.Name);
			form.find('#submitButton').text('Commit Changes');
			var first = typeBar.children(':first');
			var last = typeBar.children(':last');
			var keep = typeBar.find('[for="type' + bucket.Type + '"]');
			typeBar.empty();
			typeBar.append(first, keep, last);
		}
		
		if (bucket.Constraints) {
			if (bucket.Constraints.Length) {
				form.find('#length').val(bucket.Constraints.Length);
			}
		}

		form.find('textarea[name="description"]').val(bucket.Description);
		if (bucket.Segments) {
			for (var i = 0; i < bucket.Segments.length; i++) {
				addSegment(bucket.Segments[i]);
			}
		}

		copyrightSelector.val(bucket.CopyrightId || null);
	}

	function close() {
		if (popup)
			popup.close();
	}

	function submit() {
		var data = Callback.fromContainer(form);

		var type = getType();

		data['type'] = type;
		data['readOnly'] = readOnlyCheckbox.hasClass('checked');
		data['originId'] = origin.val();
		data['copyrightId'] = copyrightSelector.val();

		switch (type) {
			case 'Text':
				data['constraints.length'] = form.find('#length').val();
				break;
		}

		segmentList.find('.defaultCheckBox').each(function () {
			var current = $(this);
			var name = current.data('name');
			if (name) {
				data[name] = current.hasClass('checked');
			}
		});

		if (bucket && !clone) {
			data['id'] = bucket.Id;
			Callback.submit('Content/UpdateBucket', data, onCreate);
		}
		else {
			Callback.submit('Content/CreateBucket', data, onCreate);
		}
	}

	function getType() {
		return typeCheckboxes.filter('.checked').attr('id').substring(4);
	}

	function onCreate() {
		list.refreshCurrentPage();
		popup.close();
	}

	function addSegment(data) {
		var segment = segmentTemplate.clone();
		var required = segment.find('#required');
		var name = segment.find('#segmentName');
		var slugValue = segment.find('#slugValue');
		var slugLabel = segment.find('#slug');
		var idValue = segment.find('#id');

		if (data) {
			name.val(data.Name);
			if (data.Required)
				required.addClass('checked');
			else
				required.removeClass('checked');
			slugLabel.text(data.Slug);
			slugValue.val(data.Slug);
			idValue.val(data.Id);
		}
		else {
			// only calculate slug when there is none
			name.keyup(function() {
				var slug = Helper.getSlug(name.val());
				slugValue.val(slug);
				slugLabel.text(slug);
			});
		}

		var prefix = 'segments.' + segmentCount + '.';

		idValue.attr('name', prefix + 'id');
		slugValue.attr('name', prefix + 'slug');
		name.attr('name', prefix + 'name');
		required.checkBox().data('name', prefix + 'required');

		segmentCount++;
		segmentCountContainer.text(segmentCount.toString());
		segment.find('#segmentNumber').text(segmentCount + '.');

		segment.find('#delete').click(function () { confirmDelete(segment); });

		segmentList.append(segment);
	}

	function confirmDelete(segment) {
		var deletePopup = new Popup();

		var confirmation = deleteTemplate.clone();
		var name = segment.find('#segmentName').val();

		confirmation.find('#name').text(name);
		confirmation.find('#cancelButton').click(function () {
			deletePopup.close();
		});
		confirmation.find('#submitButton').click(function () {
			segment.remove();
			updateSegments();
			segmentCount--;
			segmentCountContainer.text(segmentCount.toString());
			deletePopup.close();
		});
		deletePopup.show(confirmation);
	}

	function setupCheckbox(checkbox, typeBar) {
		var id = checkbox.attr('id');
		if (!id)
			return;
		typeBar.find('[for="' + id + '"]').click(function () { typeCheckboxClick.call(checkbox.get(0)); });
	}

	function typeCheckboxClick() {
		var checkbox = $(this);

		if (checkbox.hasClass('checked'))
			return;

		typeCheckboxes.removeClass('checked');

		checkbox.addClass('checked');

		showConstraints();
	}

	function showConstraints() {
		if (typeCheckboxes.filter('#typeText').hasClass('checked'))
			form.find('#lengthContainer').show();
		else
			form.find('#lengthContainer').hide();
	}

	function onOrigins(origins) {
		originContainer.empty();
		var options = {};
		for (var i = 0; i < origins.Items.length; i++)
			options[origins.Items[i].Id] = origins.Items[i].Name;

		origin = Html.dropDown('dropDown', null, options).kswid('bucketOrigin');
		originContainer.append(origin);
		if (bucket)
			origin.val(bucket.OriginId);
	}

	function updateSlug() {
		var value = Helper.getSlug(nameBox.val());
		
	    
		var params = {
		    slug: value
		};

		Service.get('Buckets/Slugs', params, function (data) {
		    slugSpan.text(data.Value);
		    slugBox.val(data.Value);
		});


	}

	return self;
}