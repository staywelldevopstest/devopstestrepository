﻿using System;

namespace KswApi.Site.Objects
{
	public class LicenseName
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
	}
}