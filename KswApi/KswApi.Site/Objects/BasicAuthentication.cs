﻿namespace KswApi.Site.Objects
{
	public class BasicAuthentication
	{
		public string ClientId { get; set; }
		public string ClientSecret { get; set; }
	}
}