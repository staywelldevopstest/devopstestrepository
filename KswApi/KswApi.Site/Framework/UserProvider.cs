﻿using System.Web;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Site.Constants;

namespace KswApi.Site.Framework
{
	public static class UserProvider
	{
		public static UserResponse GetCurrentUser()
		{
			HttpContext context = HttpContext.Current;
			
			UserResponse user;

			if (context != null)
			{
				user = context.Items[Constant.HTTP_CONTEXT_USER_KEY] as UserResponse;

				if (user != null)
					return user;
			}

			ApiClient client = ClientFactory.GetClient();

			if (!client.Authorization.IsAuthorized)
				return null;

			try
			{
				user = client.User.GetCurrentUser(true);
			}
			catch (ServiceException)
			{
				user = null;
			}

			if (context != null)
				context.Items[Constant.HTTP_CONTEXT_USER_KEY] = user;

			return user;
		}

	}
}