﻿using System.Collections.Generic;

namespace KswApi.Site.Framework
{
	public static class ListHelper
	{
		public static int MinLength(params List<string>[] lists)
		{
			int min = int.MaxValue;

			foreach (List<string> list in lists)
			{
				if (list.Count < min)
					min = list.Count;
			}

			return min != int.MaxValue ? min : 0;
		}
	}
}