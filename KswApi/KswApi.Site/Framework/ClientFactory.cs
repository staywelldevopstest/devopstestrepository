﻿using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Web.Mvc;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Site.Constants;

namespace KswApi.Site.Framework
{
	public static class ClientFactory
	{
		private const string OAUTH_AUTHORIZATION_ADDRESS = "OAuth/Authorize";

		public static ApiClient GetClient()
		{
			string serviceUri = ConfigurationManager.AppSettings[AppSettingKeys.SERVICE_URI];
			string clientSecret = ConfigurationManager.AppSettings[AppSettingKeys.CLIENT_SECRET];

			return new ApiClient(serviceUri, Constant.WEB_PORTAL_CLIENT_ID, clientSecret);
		}

		public static AuthorizationClient GetAuthorizationClient()
		{
			string authServerUri = ConfigurationManager.AppSettings[AppSettingKeys.AUTHORIZATION_SERVER_URI];
			string serviceUri = ConfigurationManager.AppSettings[AppSettingKeys.SERVICE_URI];
			string clientSecret = ConfigurationManager.AppSettings[AppSettingKeys.CLIENT_SECRET];

			return new AuthorizationClient(authServerUri, serviceUri, Constant.WEB_PORTAL_CLIENT_ID, clientSecret);
		}

		public static string GetAuthorizationRedirect()
		{
			string authServerUri = ConfigurationManager.AppSettings[AppSettingKeys.AUTHORIZATION_SERVER_URI];
			string uri = KswApi.Framework.UriHelper.Combine(authServerUri, OAUTH_AUTHORIZATION_ADDRESS);
			NameValueCollection parameters = new NameValueCollection
				{
					{"response_type", "code"},
					{"client_id", Constant.WEB_PORTAL_CLIENT_ID},
					{"redirect_uri", ConfigurationManager.AppSettings[AppSettingKeys.PORTAL_HOME_URI]}
				};

			return UriHelper.AddQuery(uri, parameters);
		}

		public static ActionResult Call(Action<ApiClient> function)
		{
			try
			{
				ApiClient service = GetClient();

				function(service);

				return new EmptyResult();
			}
			catch (ServiceException exception)
			{
				return new JsonActionResult(exception);
			}
		}

		public static ActionResult CallAuthorization(Action<AuthorizationClient> function)
		{
			try
			{
				AuthorizationClient service = GetAuthorizationClient();

				function(service);

				return new EmptyResult();
			}
			catch (ServiceException exception)
			{
				return new JsonActionResult(exception);
			}
		}

		public static ActionResult Call<TReturn>(Func<ApiClient, TReturn> function)
		{
			try
			{
				ApiClient service = GetClient();

				TReturn value = function(service);

				return new JsonActionResult(value);
			}
			catch (ServiceException exception)
			{
				return new JsonActionResult(exception);
			}
		}

		public static ActionResult Call(string fileName, Func<ApiClient, StreamResponse> function)
		{
			try
			{
				ApiClient service = GetClient();

				StreamResponse value = function(service);

				return new FileStreamResult(value, value.ContentType)
					       {
						       FileDownloadName = fileName
					       };
			}
			catch (ServiceException exception)
			{
				return new JsonActionResult(exception);
			}
		}

	}
}