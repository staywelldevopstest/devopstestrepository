﻿using System.Collections.Generic;
using System.Web;

namespace KswApi.Site.Framework
{
	public static class RequestExtensionMethods
	{
		public static List<string> GetList(this HttpRequestBase request, string prefix)
		{
			List<string> list = new List<string>();
			for (int i = 0; ; i++)
			{
				string value = request.Params[prefix + i];
				if (value == null)
					return list;
				list.Add(value);
			}
		}
	}
}