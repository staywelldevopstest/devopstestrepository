﻿// ReSharper disable InconsistentNaming

namespace KswApi.Site.Framework
{
	public class OAuthAuthorizationRequest
	{
		// These names are case-sensitive, to correspond to parameter names in OAuth
		public string response_type { get; set; }
		public string client_id { get; set; }
		public string redirect_uri { get; set; }
		public string scope { get; set; }
		public string state { get; set; }
	}
}