﻿using System;
using System.Net;
using System.Web.Mvc;
using KswApi.RestrictedInterface.Objects;
using KswApi.Site.Framework;
using KswApi.Site.Helpers;
using KswApi.Site.Models;
using KswApi.Interface.Exceptions;

namespace KswApi.Site.Controllers
{
	public class OAuthController : Controller
	{
		#region Public Methods
		[HttpGet]
		public ActionResult Authorize(OAuthAuthorizationRequest request)
		{
			AuthorizationClient authorizationClient = ClientFactory.GetAuthorizationClient();

			try
			{
				ApplicationValidationResponse response = authorizationClient.Authorization.GetApplicationValidation(request.response_type, request.client_id, request.redirect_uri, request.scope, request.state);

				LoginModel model = new LoginModel();

				if (!string.IsNullOrEmpty(response.Warning))
					model.Warning = response.Warning;

				model.client_id = request.client_id;
				model.redirect_uri = request.redirect_uri;
				model.response_type = request.response_type;

				return View("~/Views/Login/Index.cshtml", model);
			}
			catch (ServiceRedirectException exception)
			{
				return new RedirectResult(exception.RedirectUri);
			}
			catch (ServiceException exception)
			{
				return OAuthError(exception);
			}
		}

		[HttpPost]
		public ActionResult Authorize(LoginModel model)
		{
			if (!ModelState.IsValid)
			{
				return View("~/Views/Login/Index.cshtml", model);
			}

			AuthorizationGrantRequest request = OAuthHelper.AuthorizationGrantRequestFromLoginModel(model);

			AuthorizationClient authorizationClient = ClientFactory.GetAuthorizationClient();

			try
			{
				AuthorizationGrantResponse response = authorizationClient.Authorization.CreateAuthorizationGrant(request);

				if (response.IsValid)
					return new RedirectResult(response.RedirectUri);

				if (!string.IsNullOrEmpty(response.Error))
					model.Error = response.Error;

				return View("~/Views/Login/Index.cshtml", model);
			}
			catch (ServiceRedirectException exception)
			{
				return new RedirectResult(exception.RedirectUri);
			}
			catch (ServiceException exception)
			{
				return OAuthError(exception);
			}
			catch (WebException exception)
			{
				return OAuthError(exception);
			}
		}

		#endregion Public Methods

		#region Private Methods

		private ActionResult OAuthError(Exception exception)
		{
			return View("OAuthError", new ErrorData { ErrorMessage = exception.Message });
		}

		#endregion Private Methods
	}
}
