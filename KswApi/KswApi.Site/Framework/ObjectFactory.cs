﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using System.Web.Mvc;
using KswApi.Interface.Exceptions;

namespace KswApi.Site.Framework
{
	public static class ObjectFactory
	{
		public static T Get<T>(Controller controller)
		{
			return (T) Get(controller.Request, null, typeof(T));
		}

		public static T Get<T>(HttpRequestBase request)
		{
			return (T) Get(request, null, typeof(T));
		}

		private static object GetClass(HttpRequestBase request, string path, Type type)
		{
			ConstructorInfo constructor = type.GetConstructor(Type.EmptyTypes);

			if (constructor == null)
				throw new InvalidOperationException(string.Format("Type does not have a default constructor: {0}.", type.Name));

			object result = constructor.Invoke(null);

			PropertyInfo[] properties = type.GetProperties(BindingFlags.Instance | BindingFlags.Public);

			foreach (PropertyInfo property in properties)
			{
				// look for camel-case parameters
				string name = char.ToLowerInvariant(property.Name[0]) + property.Name.Substring(1);

				name = string.IsNullOrEmpty(path) ? name : path + '.' + name;

				object value = Get(request, name, property.PropertyType);

				if (value != null)
					property.SetValue(result, value, null);
			}

			return result;
		}

		private static object Get(HttpRequestBase request, string path, Type type)
		{
			TypeCode code = Type.GetTypeCode(type);
			Type propertyType = type;

			if (code == TypeCode.Object && propertyType.IsGenericType)
			{
				Type generic = propertyType.GetGenericTypeDefinition();
				if (generic == typeof(Nullable<>))
				{
					propertyType = propertyType.GetGenericArguments()[0];
					code = Type.GetTypeCode(propertyType);
				}
				else if (generic == typeof(List<>))
				{
					return GetList(request, path, propertyType);
				}
			}

			if (propertyType.IsClass && code != TypeCode.String)
			{
				if (!string.IsNullOrEmpty(path) && !request.Params.AllKeys.Any(item => item.StartsWith(path)))
					return null;

				return GetClass(request, path, type);
			}

			string value = request.Params[path];

			if (value == null)
				return null;

			if (propertyType.IsEnum)
			{
				FieldInfo field = propertyType.GetFields().FirstOrDefault(item => item.Name == value);

				if (field == null)
					return null;

				return field.GetRawConstantValue();
			}

			if (code != TypeCode.Object)
			{
				if (code != TypeCode.String && string.IsNullOrEmpty(value))
					return null;

				try
				{
					return Convert.ChangeType(value, propertyType);
				}
				catch (FormatException)
				{
					throw new ServiceException(HttpStatusCode.BadRequest, string.Format("Invalid value for: {0}.", path));
				}
			}

			if (propertyType == typeof(Guid))
			{
				Guid guid;
				if (Guid.TryParse(value, out guid))
					return guid;

				return null;
			}

			return null;
		}

		private static object GetList(HttpRequestBase request, string path, Type type)
		{
			IList result = null;
			Type listType = null;

			for (int i = 0; ; i++)
			{
				string currentPath = string.Format("{0}.{1}", path, i);

				if (!request.Params.AllKeys.Any(item => item.StartsWith(currentPath)))
					break;

				if (result == null)
				{
					ConstructorInfo constructor = type.GetConstructor(Type.EmptyTypes);

					if (constructor == null)
						throw new InvalidOperationException(string.Format("Type does not have a default constructor: {0}.", type.Name));

					result = (IList) constructor.Invoke(null);
					listType = type.GetGenericArguments()[0];
				}

				result.Add(Get(request, currentPath, listType));
			}

			return result;
		}
	}
}