﻿using System.Web;
using System.Web.Mvc;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace KswApi.Site.Framework
{
	public class JsonActionResult : ActionResult
	{
		private readonly object _result;

		public JsonActionResult(object result)
		{
			_result = result;
		}

		public JsonActionResult(Error result)
		{
			if (HttpContext.Current != null)
				HttpContext.Current.Response.StatusCode = (int)result.StatusCode;

			_result = new Error
			{
				StatusCode = (int)result.StatusCode,
				Details = result.Details
			};
		}

		public JsonActionResult(ServiceException exception)
		{
			if (HttpContext.Current != null)
				HttpContext.Current.Response.StatusCode = (int)exception.StatusCode;

			_result = new Error
				{
					StatusCode = (int) exception.StatusCode,
					Details = exception.Message
				};
		}

		public override void ExecuteResult(ControllerContext context)
		{
			HttpResponseBase response = context.HttpContext.Response;

			response.ContentType = "application/json";

			JsonSerializer serializer = new JsonSerializer();
			serializer.Formatting = Formatting.Indented;
			serializer.NullValueHandling = NullValueHandling.Ignore;
			serializer.Converters.Add(new StringEnumConverter());
			serializer.Serialize(response.Output, _result);
		}
	}
}