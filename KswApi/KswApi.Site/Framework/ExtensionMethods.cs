﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace KswApi.Site.Framework
{
    public static class ExtensionMethods 
    { 
        public static MvcHtmlString DropDownList(this HtmlHelper helper, string name, Dictionary<string, string> dictionary, object htmlAttributes) 
        { 
            var selectListItems = new SelectList(dictionary, "Key", "Value"); 
            return helper.DropDownList(name, selectListItems,htmlAttributes ); 
        }

        public static MvcHtmlString DropDownListFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, Dictionary<string, string> dictionary, object htmlAttributes)
        {
            var attrs = HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributes);
            var selectListItems = new SelectList(dictionary, "Key", "Value");

            return htmlHelper.DropDownListFor(expression, selectListItems, null /* optionLabel */, attrs);
        }
    }
}