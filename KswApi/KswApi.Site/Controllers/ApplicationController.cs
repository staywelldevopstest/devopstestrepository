﻿using System.Collections.Generic;
using System.Configuration;
using System.Web.Mvc;
using KswApi.Interface.Objects;
using KswApi.Site.Constants;
using KswApi.Site.Framework;
using KswApi.Site.Models;

namespace KswApi.Site.Controllers
{
	public class ApplicationController : Controller
	{
		public ActionResult Index(string code, string state)
		{
			ApiClient client = ClientFactory.GetClient();
			
			if (string.IsNullOrEmpty(code))
			{
				if (client.Authorization.IsAuthorized)
					return View();

				return View("Error", new ErrorData { ErrorMessage = "Authorization is not valid." });
			}

			string redirectUri = ConfigurationManager.AppSettings[AppSettingKeys.PORTAL_HOME_URI];

			AuthorizationResult result = client.Authorization.Authorize(code, redirectUri);
			
			return View("AuthorizationResult", result);
		}

		public ActionResult UserDetails()
		{
			ApiClient client = ClientFactory.GetClient();

			UserResponse user = client.User.GetCurrentUser(true);

			return View(user);
		}

		public ActionResult AllRights()
		{
			ApiClient client = ClientFactory.GetClient();

			RightList rights = client.Administration.GetRights();

			return View(rights);
		}

		public ActionResult AllRoles()
		{
			ApiClient client = ClientFactory.GetClient();

			RoleList roles = client.Administration.GetRoles();

			return View(roles);
		}

		[HttpGet]
		public ActionResult CreateRole()
		{
			return View();
		}

		[HttpPost]
		public ActionResult CreateRole(string name, string description, string rights)
		{
			ApiClient client = ClientFactory.GetClient();

			Role role = new Role
				{
					Name = name,
					Description = description,
					Rights = string.IsNullOrEmpty(rights) ? null : new List<string>(rights.Split(','))
				};

			role = client.Administration.CreateRole(role);

			return View("ViewRole", role);
		}

		[HttpGet]
		public ActionResult UpdateRole(string roleId)
		{
			ApiClient client = ClientFactory.GetClient();

			Role role = client.Administration.GetRole(roleId);

			return View(role);
		}

		[HttpPost]
		public ActionResult UpdateRole(string roleId, string name, string description, string rights)
		{
			ApiClient client = ClientFactory.GetClient();

			Role role = new Role
			{
				Name = name,
				Description = description,
				Rights = string.IsNullOrEmpty(rights) ? null : new List<string>(rights.Split(','))
			};

			role = client.Administration.UpdateRole(roleId, role);

			return View("ViewRole", role);
		}

		public ActionResult ViewRole(string roleId)
		{
			ApiClient client = ClientFactory.GetClient();

			Role role = client.Administration.GetRole(roleId);

			return View(role);
		}

		public ActionResult DeleteRole(string roleId)
		{
			ApiClient client = ClientFactory.GetClient();

			client.Administration.DeleteRole(roleId);

			return RedirectToAction("AllRoles");
		}
	}
}
