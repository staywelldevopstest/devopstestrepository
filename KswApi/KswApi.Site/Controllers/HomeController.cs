﻿using System.Configuration;
using System.Web.Mvc;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Site.Constants;
using KswApi.Site.Framework;
using KswApi.Site.Models;

namespace KswApi.Site.Controllers
{
	public class HomeController : Controller
	{
		public ActionResult Index(string code)
		{
			if (!string.IsNullOrEmpty(code))
				return HandleOauthAuthorization(code);

			ApiClient client = ClientFactory.GetClient();

			if (!client.Authorization.IsAuthorized)
				return Redirect(ClientFactory.GetAuthorizationRedirect());

			UserResponse user = UserProvider.GetCurrentUser();

			if (user == null)
				return Redirect(ClientFactory.GetAuthorizationRedirect());

			// make sure if there is a virtual application, we get a '/' at the end of the virtual application
			// so all content is rooted correctly
			string filePath = Request.FilePath;
			if (!string.IsNullOrEmpty(filePath) && !filePath.EndsWith("/"))
				return Redirect(filePath + "/");

			switch (user.Type)
			{
				case UserType.DomainUser:
					return View("AdministrationIndex");
				case UserType.ClientUser:
					return View("ClientUserIndex");
				default:
					return View("Error", new ErrorData { ErrorMessage = "Invalid user, please login again." });
			}
		}

		private ActionResult HandleOauthAuthorization(string code)
		{
			string redirectUri = ConfigurationManager.AppSettings[AppSettingKeys.PORTAL_HOME_URI];

			ApiClient client = ClientFactory.GetClient();

			AuthorizationResult result = client.Authorization.Authorize(code, redirectUri);
			if (!result.IsSuccessful)
				return View("Error", new ErrorData { ErrorMessage = "Could not authenticate." });

			// strip off the query string
			return RedirectToAction("Index");
		}
	}
}
