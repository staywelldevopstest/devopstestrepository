﻿using System.Web.Mvc;
using KswApi.Interface.Exceptions;
using KswApi.Site.Framework;
using KswApi.Site.Models;

namespace KswApi.Site.Controllers
{
	public class UserController : Controller
	{
		public ActionResult Password(string token)
		{
			if (string.IsNullOrEmpty(token))
			{
				return View(new PasswordModel
					     {
						     IsValid = false,
						     Error = "The password request is not valid. The correct URL should be contained in an e-mail message."
					     });
			}
			try
			{
				AuthorizationClient authorizationClient = ClientFactory.GetAuthorizationClient();

				authorizationClient.Authorization.ValidateClientUserPasswordToken(token);
			}
			catch (ServiceException exception)
			{
				return View(new PasswordModel { IsValid = false, Error = exception.Message });
			}

			return View(new PasswordModel { IsValid = true, Token = token });
		}
	}
}
