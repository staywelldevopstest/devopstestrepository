﻿using System.Configuration;
using System.IO;
using System.Net;
using System.Web;
using System.Web.Mvc;
using KswApi.Exceptions;
using KswApi.Framework;
using KswApi.Interface.Objects;
using KswApi.Objects;
using KswApi.Site.Constants;
using KswApi.Site.Framework;
using Constant = KswApi.Site.Constants.Constant;

namespace KswApi.Site.Controllers
{
	public class ServiceController : Controller
	{
		public ActionResult Index(string verb, string path)
		{
			CookieTokenStore store = new CookieTokenStore();

			AccessToken token = store.GetToken();

			try
			{
				Response.CacheControl = "no-cache";
				return CallService(verb, path, token);
			}
			catch (WebException exception)
			{
				return HandleException(verb, path, exception, token);
			}
		}

		private ActionResult HandleException(string verb, string path, WebException exception, AccessToken token)
		{
			HttpWebResponse response = exception.Response as HttpWebResponse;
			if (response == null)
			{
				HttpContext.Response.StatusCode = 500;
				return new JsonActionResult(new Error
				{
					Details = exception.Message,
					StatusCode = 500
				});
			}

			if (response.StatusCode != HttpStatusCode.Unauthorized)
			{
				HttpContext.Response.StatusCode = (int)response.StatusCode;
				return new FileStreamResult(response.GetResponseStream(), response.ContentType);
			}

			CookieTokenStore store = new CookieTokenStore();

			store.RemoveToken();

			if (token != null && !string.IsNullOrEmpty(token.RefreshToken))
			{
				token = RefreshToken(token);

				if (token == null)
				{
					HttpContext.Response.StatusCode = (int)response.StatusCode;
					return new FileStreamResult(response.GetResponseStream(), response.ContentType);
				}
			}

			if (token != null)
				store.SetToken(token);

			try
			{
				return CallService(verb, path, token);
			}
			catch (WebException)
			{
				HttpContext.Response.StatusCode = (int)response.StatusCode;
				return new FileStreamResult(response.GetResponseStream(), response.ContentType);
			}
		}

		private ActionResult CallService(string verb, string path, AccessToken token)
		{
			if (string.IsNullOrEmpty(verb))
				return new JsonActionResult(new Error
											{
												Details = "Invalid verb.",
												StatusCode = 404
											});

			verb = verb.ToUpper();

			string fullPath = ConfigurationManager.AppSettings[AppSettingKeys.SERVICE_URI] + '/' + path;

			HttpRequestBase incomingRequest = HttpContext.Request;

			if (incomingRequest.Url != null && !string.IsNullOrEmpty(incomingRequest.Url.Query))
				fullPath += incomingRequest.Url.Query;

			WebRequest outgoingRequest = WebRequest.Create(fullPath);

			outgoingRequest.Method = verb;
			outgoingRequest.ContentType = incomingRequest.ContentType;

			if (token != null)
				outgoingRequest.Headers.Add("Authorization", string.Format("Bearer {0}", token.Token));

			if (incomingRequest.ContentLength != 0)
			{
				incomingRequest.InputStream.Position = 0;
				Stream stream = outgoingRequest.GetRequestStream();
				incomingRequest.InputStream.CopyTo(stream);
				incomingRequest.InputStream.Flush();
			}

			WebResponse response = outgoingRequest.GetResponse();

			return new FileStreamResult(response.GetResponseStream(), response.ContentType);
		}

		private AccessToken RefreshToken(AccessToken token)
		{
			OAuthClient oAuthClient = new OAuthClient(ConfigurationManager.AppSettings[AppSettingKeys.SERVICE_URI]);
			try
			{
				return oAuthClient.RefreshUserToken(token.RefreshToken, Constant.WEB_PORTAL_CLIENT_ID,
											 ConfigurationManager.AppSettings[AppSettingKeys.CLIENT_SECRET]);
			}
			catch (OAuthException)
			{
				return null;
			}
		}
	}
}
