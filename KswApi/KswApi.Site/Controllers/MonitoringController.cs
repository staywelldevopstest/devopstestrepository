﻿using System;
using System.Web;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Site.Framework;
using System.Text;
using System.Web.Mvc;

namespace KswApi.Site.Controllers
{
    public class MonitoringController : Controller
    {
        //
        // GET: /Monitorning/
        public string Index()
        {
            ApiClient clientLibrary = ClientFactory.GetClient();

            MonitoringStatus status = null;

	        try
	        {
				status = clientLibrary.Monitoring.ServerStatus();
	        }
	        catch (ServiceException)
	        {
				// set everything to false
				status = new MonitoringStatus();
	        }
			

            status.PortalStatus = true;

            StringBuilder result = new StringBuilder();

            result.Append("<p>");
            result.Append("Cache Status: ");
            result.Append(status.CacheStatus ? "OK" : "ERROR");
            result.Append("</p>");

            result.Append("<p>");
            result.Append("Database Status: ");
            result.Append(status.DatabaseStatus ? "OK" : "ERROR");
            result.Append("</p>");

            result.Append("<p>");
            result.Append("Service Status: ");
            result.Append(status.ServiceStatus ? "OK" : "ERROR");
            result.Append("</p>");

            result.Append("<p>");
            result.Append("Portal Status: ");
            result.Append(status.PortalStatus ? "OK" : "ERROR");
            result.Append("</p>");

            result.Append("<p>");
            result.Append("User Logged In: ");
            result.Append(clientLibrary.Authorization.IsAuthorized.ToString());
            result.Append("</p>");

            return result.ToString();
        }

		public void Modules()
		{
			HttpApplication application = HttpContext.ApplicationInstance;
			
			HttpModuleCollection modules = application.Modules;

			Response.Write("Total Number Active HttpModules: " + modules.Count.ToString() + "<br/><br/>");

			Response.Write("<b>List of Active Modules</b>" + "<br/><br/>");

			foreach (string module in modules.AllKeys)
			{
				Response.Write(module + "</br>");
			}
		}
    }
}
