﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KswApi.Site.Controllers
{
	public class BackButtonTestController : Controller
	{
		//
		// GET: /BackButtonTest/

		public ActionResult Index()
		{
			return View();
		}

		[HttpGet]
		public ActionResult GetRedText()
		{
			return PartialView("RedText");
		}

		[HttpGet]
		public ActionResult GetBlueText()
		{
			return PartialView("BlueText");
		}

		[HttpGet]
		public ActionResult GetGreenText()
		{
			return PartialView("GreenText");
		}

		[HttpGet]
		public ActionResult GetChart()
		{
			return PartialView("Chart");
		}

	}
}
