﻿using System.Web.Mvc;
using KswApi.Site.Framework;

namespace KswApi.Site.Controllers
{
	public class LogoutController : Controller
	{
		//
		// GET: /Logout/

		public ActionResult Index()
		{
			ApiClient client = ClientFactory.GetClient();

			client.Authorization.Unauthorize();

			return View();
		}

	}
}
