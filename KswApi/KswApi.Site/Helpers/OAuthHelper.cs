﻿using KswApi.RestrictedInterface.Objects;
using KswApi.Site.Models;

namespace KswApi.Site.Helpers
{
	public static class OAuthHelper
	{
		public static AuthorizationGrantRequest AuthorizationGrantRequestFromLoginModel(LoginModel model)
		{
			return new AuthorizationGrantRequest
			{
				ResponseType = model.response_type,
				ApplicationId = model.client_id,
				RedirectUri = model.redirect_uri,
				UserName = model.user_name,
				Password = model.password,
				State = model.state
			};
		}
	}
}