﻿using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace KswApi.Site.Helpers
{
	public static class HtmlHelperExtensions
	{
		public static IHtmlString Json(this HtmlHelper helper, object obj)
		{
			JsonSerializerSettings settings = new JsonSerializerSettings();
			settings.Converters.Add(new StringEnumConverter()); 
			return helper.Raw(JsonConvert.SerializeObject(obj, settings));
		}
	}
}