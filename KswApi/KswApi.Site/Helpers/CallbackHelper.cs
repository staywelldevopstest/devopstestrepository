﻿using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace KswApi.Site.Helpers
{
	public static class CallbackHelper
	{
		public static void RenderCallback(this HtmlHelper htmlHelper, string actionName, string controllerName)
		{
			RenderCallback(htmlHelper, actionName, controllerName, new RouteValueDictionary());
		}

		public static void RenderCallback(this HtmlHelper htmlHelper, string actionName, string controllerName, object routeValues)
		{
			RenderCallback(htmlHelper, actionName, controllerName, new RouteValueDictionary(routeValues));
		}

		public static void RenderCallback(this HtmlHelper htmlHelper, string actionName, string controllerName, RouteValueDictionary routeValues)
		{
			string contentType = HttpContext.Current.Response.ContentType;

			routeValues["area"] = "Callback";

			htmlHelper.RenderAction(actionName, controllerName, routeValues);

			HttpContext.Current.Response.ContentType = contentType;
		}
	}
}