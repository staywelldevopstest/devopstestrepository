﻿namespace KswApi.Site.Constants
{
	public static class AppSettingKeys
	{
		public const string CLIENT_SECRET = "KswApiClientSecret";
		public const string AUTHORIZATION_SERVER_URI = "KswApiAuthorizationServer";
		public const string SERVICE_URI = "KswApiService";
		public const string PORTAL_HOME_URI = "KswApiPortalHomeUri";
	}

	public static class OAuthHeaderKeys
	{
		public const string CLIENT_ID = "client_id";
		public const string CLIENT_SECRET = "client_secret";
		public const string BASIC_AUTHENTICATION = "Authorization";
	}

    public static class MiscConstants
    {
        public const int NUM_DETAILS_PER_PAGE = 10;
    }
}