﻿namespace KswApi.Site.Constants
{
	public static class Constant
	{
		public const string WEB_PORTAL_CLIENT_ID = "29eeff76-85d0-4f70-94ed-a49072524692";
		public const string HTTP_CONTEXT_USER_KEY = "User";
		public const int SESSION_TIMEOUT_IN_MINUTES = 20;
	}
}