﻿namespace KswApi.Site.Models
{
	public class PasswordModel
	{
		public bool IsValid { get; set; }
		public string Error { get; set; }
		public string Token { get; set; }
	}
}