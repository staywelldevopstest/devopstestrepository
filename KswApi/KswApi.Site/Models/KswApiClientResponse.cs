﻿namespace KswApi.Site.Models
{
	public class KswApiClientResponse
	{
		public bool JsonReturnFormat { get; set; }
		public string ClientResponse { get; set; }
	}
}