﻿namespace KswApi.Site.Models
{
	public class AuthenticationData
	{
		public string UserName { get; set; }
		public string Password { get; set; }
	}
}