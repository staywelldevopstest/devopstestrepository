﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace KswApi.Site.Models
{
    [Serializable]
    public class ClientListModel
    {
        public IList<ClientModel> ClientList { get; set; }
        public bool ShowPaging { get; set; }
        public bool ShowPrevButton { get; set; }
        public bool ShowNextButton { get; set; }
        public int CurrentPage { get; set; }
        public int TotalPages { get; set; }
        public int TotalCount { get; set; }
        public int Offset { get; set; }
        public string SearchString { get; set; }
    }
}