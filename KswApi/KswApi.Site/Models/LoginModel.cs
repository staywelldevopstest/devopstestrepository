﻿using System.ComponentModel.DataAnnotations;
using KswApi.Site.Framework;

// Allowing inconsistent naming here, to allow our form fields
// to follow the same format that is expected for OAuth

// ReSharper disable InconsistentNaming

namespace KswApi.Site.Models
{
	public class LoginModel : OAuthAuthorizationRequest
	{
		public string Warning { get; set; }
		public string Error { get; set; }

		[Required(ErrorMessage = "User name is required")]
		public string user_name { get; set; }

		[Required(ErrorMessage = "Password is required")]
		public string password { get; set; }
	}
}
