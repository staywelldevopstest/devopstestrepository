﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KswApi.Site.Models
{
	[Serializable]
	public class LicenseModel
	{
        [Required]
        [MaxLength(100)]
        [DataType(DataType.Text)]
        public string Name { get; set; }

        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }
        
        public ClientModel Client { get; set; }
        public Dictionary<string, string> ParentLicenseList { get; set; }
		public Guid Id { get; set; }
		public Guid? ParentLicenseId { get; set; }
		public Guid ClientId { get; set; }
		public bool Active { get; set; }
		public DateTime CreatedDate { get; set; }
		public List<Guid> Applications { get; set; }
	}
}