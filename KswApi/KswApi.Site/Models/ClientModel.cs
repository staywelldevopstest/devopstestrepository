﻿using KswApi.Interface.Enums;
using KswApi.Site.Framework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace KswApi.Site.Models
{
    [Serializable]
    public class ClientModel
    {
        public Guid Id { get; set; }

        [Required]
        [MaxLength(100)]
        [DataType(DataType.Text)]
        public string ClientName { get; set; }

        [DataType(DataType.Url)]
        [MaxLength(1024)]
        public string ClientWebSite { get; set; }

        [DataType(DataType.Text)]
        public string ContactName { get; set; }

        [DataType(DataType.EmailAddress)]
        //[EmailAddress(ErrorMessage="Must be a valid email.")]
        public string ContactEmail { get; set; }

        [DataType(DataType.PhoneNumber)]
        public string ContactPhone { get; set; }

        [DataType(DataType.MultilineText)]
        public string Notes { get; set; }

        public DateTime DateAdded { get; set; }

        public ClientStatus Status { get; set; }

		public int LicenseCount { get; set; }
    }
}
