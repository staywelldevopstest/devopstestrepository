﻿using System;
using System.Web;
using System.Web.Mvc;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Site.Areas.Callback.Models;
using KswApi.Site.Framework;

namespace KswApi.Site.Areas.Callback.Controllers
{
	public class LoggingController : Controller
	{
		[HttpPost]
		public ActionResult Index(LogModel model)
		{
			ApiClient client = ClientFactory.GetClient();

			try
			{
				//"Type eq Notification and Id eq guid'asdf-wij-asdofi-asdf'
				string filter = GetFilterString(model); //string.IsNullOrEmpty(model.Type) ? null : "Type eq " + model.Type;
				
				//LogList list = client.Reporting.GetFilteredLogs(model.Offset, model.Count, model.IncludeWarnings, model.IncludeErrors, 
				LogList list = client.Logs.GetLogs(model.Offset, model.Count, filter, model.Sort);

				if (list.Items.Count == 0)
					return new JsonActionResult("No Results Found");

				list = HtmlEncode(list);

				return new JsonActionResult(list);
			}
			catch (ServiceException exception)
			{
				Error error = new Error
								  {
									  StatusCode = (int) exception.StatusCode,
									  Details = exception.Message
								  };
				
				Response.StatusCode = (int) exception.StatusCode;

				return new JsonActionResult(error);
			}
		}

		private LogList HtmlEncode(LogList list)
		{
			foreach (Log log in list.Items)
			{
				log.Body = HttpUtility.HtmlEncode(log.Body);
				log.Message = HttpUtility.HtmlEncode(log.Message);
				log.Path = HttpUtility.HtmlEncode(log.Path);
				log.StackTrace = HttpUtility.HtmlEncode(log.StackTrace);
				log.Title = HttpUtility.HtmlEncode(log.Title);
				log.Verb = HttpUtility.HtmlEncode(log.Verb);
			}

		    return list;
		}

		private string GetFilterString(LogModel model)
		{
			string result = "Type eq 2";

			string severityFilter = GetSeverityFilter(model);
			string dateRangeFilter = GetDateRangeFilter(model);
			string clientFilter = string.IsNullOrWhiteSpace(model.ClientFilter) || model.ClientFilter == Guid.Empty.ToString() ? string.Empty : "ClientId eq guid'" + model.ClientFilter + "'";
			string licenseFilter = string.IsNullOrWhiteSpace(model.ClientFilter) || model.LicenseFilter == Guid.Empty.ToString() ? string.Empty : "and LicenseId eq guid'" + model.LicenseFilter + "'";

			if (!string.IsNullOrWhiteSpace(severityFilter))
				result += " and (" + severityFilter + ") and " + dateRangeFilter;
			else
				result += " and " + dateRangeFilter;

			if (!string.IsNullOrWhiteSpace(clientFilter))
				result += " and " + clientFilter;

			if (!string.IsNullOrWhiteSpace(licenseFilter))
				result += " and " + licenseFilter;

			return result;
		}

		private string GetDateRangeFilter(LogModel model)
		{
			string dateRangeFilter;

			//Default the search to the last week
			if (model.DateRange == null)
				model.DateRange = DateTime.Now.AddDays(-7).ToShortDateString() + " - " + DateTime.Now.ToShortDateString();

			int hyphenIndex = model.DateRange.IndexOf("-", StringComparison.Ordinal);

			if (hyphenIndex > 0)
			{
				string fromDate = model.DateRange.Substring(0, hyphenIndex - 1).Trim() + " 12:00:00 AM";
				string toDate = model.DateRange.Substring(hyphenIndex + 1).Trim() + " 12:00:00 AM";

				dateRangeFilter = "(Date ge datetime'" + FormatDateTime(fromDate) + "' and Date lt datetime'" + FormatDateTime(toDate, 1) + "')";
			}
			else
			{
				string fromDate = model.DateRange.Trim() + " 12:00:00 AM";
				string toDate = model.DateRange.Trim() + " 12:00:00 AM";

				dateRangeFilter = "(Date ge datetime'" + FormatDateTime(fromDate) + "' and Date lt datetime'" + FormatDateTime(toDate, 1) + "')";
			}

			return dateRangeFilter;
		}

		private string FormatDateTime(string dateTime)
		{
			DateTime date = DateTime.Parse(dateTime);

			return date.ToString("s");
		}

		private string FormatDateTime(string dateTime, int addDay)
		{
			DateTime date = DateTime.Parse(dateTime);

			date = date.AddDays(addDay);

			return date.ToString("s");
		}

		private string GetSeverityFilter(LogModel model)
		{
			string result = model.IncludeErrors ? "SubType eq 2" : string.Empty;
			result += model.IncludeWarnings ? string.IsNullOrWhiteSpace(result) ? "SubType eq 3" : " or SubType eq 3" : string.Empty;
			result += model.IncludeInfo ? string.IsNullOrWhiteSpace(result) ? "SubType eq 4" : " or SubType eq 4" : string.Empty;

			if (string.IsNullOrWhiteSpace(result))
				result = "SubType eq 0";

			return result;
		}
	}
}
