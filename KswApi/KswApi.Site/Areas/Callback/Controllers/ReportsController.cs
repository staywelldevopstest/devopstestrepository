﻿using System.IO;
using KswApi.Interface.Enums;
using KswApi.Site.Framework;
using System.Web.Mvc;

namespace KswApi.Site.Areas.Callback.Controllers
{
	public class ReportsController : Controller
	{
		//
		// GET: /Callback/Reports/

		public ActionResult GetSmsBillingReport(int month, int year)
		{
			Month selectedMonth = (Month) month;

			return ClientFactory.Call(service => service.Reports.GetSmsBillingReportData(selectedMonth, year));
		}

		public ActionResult GetCsvExport(int month, int year)
		{
			Month selectedMonth = (Month) month;

			string reportName = "SmsBillingReport" + selectedMonth.ToString() + year.ToString() + ".csv";

			return ClientFactory.Call(reportName, service => service.Reports.GetSmsBillingReportCsvExport(selectedMonth, year));
		}
	}
}
