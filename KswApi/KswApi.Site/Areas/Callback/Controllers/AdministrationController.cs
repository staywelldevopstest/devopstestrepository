﻿using System.Net;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.Content;
using KswApi.Interface.Objects.SmsGateway;
using KswApi.Site.Framework;
using KswApi.Site.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace KswApi.Site.Areas.Callback.Controllers
{
	public class AdministrationController : Controller
	{
		public AdministrationController()
		{
			ValidateRequest = false;
		}

		#region Public MessageHandlers

		#region Client Methods

		public ActionResult GetClients(string search, int offset, int count)
		{
			return ClientFactory.Call(service => service.Administration.GetClients(offset, count, search, null));
		}

		public ActionResult GetCurrentUser()
		{
			return ClientFactory.Call(service => service.User.GetCurrentUser(true));
		}

		public ActionResult GetClient(string clientId)
		{
			return ClientFactory.Call(service => service.Administration.GetClient(clientId));
		}

		#endregion Client Methods

		#region License Methods

		public ActionResult GetClientLicenseDetail(string clientId, string search)
		{
			return ClientFactory.Call(service => service.Administration.GetClientLicenseDetail(clientId, search));
		}

		public ActionResult GetClientLicenseNames(string clientId)
		{
			ApiClient service = ClientFactory.GetClient();

			LicenseList list = service.Administration.GetLicenses(0, 200, null, clientId, "Name");

			if (list != null && list.Items != null && list.Items.Count > 0)
			{
				return new JsonActionResult(list.Items.Select(item => new LicenseName { Id = item.Id, Name = item.Name }).ToList());
			}

			return new JsonActionResult(new List<LicenseName>());
		}

		public ActionResult GetParentLicenses(string clientId, string licenseId)
		{
			return ClientFactory.Call(service => service.Administration.GetAvailableParentLicenses(clientId, licenseId, "Name"));
		}

		public ActionResult CreateLicense(string clientId, string name, string parentLicenseId, string notes)
		{
			Guid guid = Guid.Parse(clientId);
			Guid? parent = null;

			if (!string.IsNullOrEmpty(parentLicenseId))
				parent = Guid.Parse(parentLicenseId);

			License license = new License
				{
					ClientId = guid,
					Name = name,
					Notes = notes,
					ParentLicenseId = parent
				};

			return ClientFactory.Call(service => service.Administration.CreateLicense(license));
		}

		public ActionResult UpdateLicense(string clientId, string licenseId, string name, string parentLicenseId, string notes)
		{
			Guid guid = Guid.Parse(clientId);

			Guid? parent = null;

			if (!string.IsNullOrEmpty(parentLicenseId))
				parent = Guid.Parse(parentLicenseId);
			License license = new License
			{
				ClientId = guid,
				Name = name,
				Notes = notes,
				ParentLicenseId = parent
			};

			return ClientFactory.Call(service => service.Administration.UpdateLicense(licenseId, license));
		}

		public ActionResult GetLicense(string licenseId)
		{
			return ClientFactory.Call(service => service.Administration.GetLicense(licenseId));
		}

		#endregion License Methods

		#region Application Methods

		public ActionResult GetApplications(string licenseId)
		{
			return ClientFactory.Call(service => service.Administration.GetLicenseApplications(licenseId));
		}

		public ActionResult DeleteLicense(string licenseId)
		{
			return ClientFactory.Call(service => service.Administration.DeleteLicense(licenseId));
		}

		public ActionResult CreateApplication(string licenseId)
		{
			return ClientFactory.Call(service => service.Administration.CreateLicenseApplication(licenseId, ObjectFactory.Get<ApplicationRequest>(this)));
		}

		public ActionResult UpdateApplication(string licenseId, string applicationId)
		{
			var app = ObjectFactory.Get<ApplicationRequest>(this);
			return ClientFactory.Call(service => service.Administration.UpdateLicenseApplication(licenseId, applicationId, app));
		}

		public ActionResult DeleteApplication(string licenseId, string applicationId)
		{
			return ClientFactory.Call(service => service.Administration.DeleteLicenseApplication(licenseId, applicationId));
		}

		public ActionResult GetApplication(string licenseId, string applicationId)
		{
			return ClientFactory.Call(service => service.Administration.GetLicenseApplication(licenseId, applicationId));
		}

		#endregion Application Methods

		#region Module Methods

		public ActionResult GetModules(string licenseId)
		{
			return ClientFactory.Call(service => service.Administration.GetLicenseModules(licenseId));
		}

		public ActionResult AddModule(string licenseId, string moduleType)
		{
			return ClientFactory.Call(service => service.Administration.AddLicenseModule(licenseId, moduleType));
		}

		public ActionResult RemoveModule(string licenseId, string moduleType)
		{
			return ClientFactory.Call(service => service.Administration.RemoveLicenseModule(licenseId, moduleType));
		}

		#endregion Module Methods

		#region Copyright Methods

		public ActionResult GetCopyrights(string query, int offset, int count, bool includeBucketCount)
		{
			return ClientFactory.Call(service => service.Administration.GetCopyrights(offset, count, query, includeBucketCount));
		}

		public ActionResult CreateCopyright(string content)
		{
			if (content == null)
			{
				content = string.Empty;
			}

			CopyrightRequest request = new CopyrightRequest
										   {
											   Value = content
										   };

			return ClientFactory.Call(service => service.Administration.CreateCopyright(request));
		}

		public ActionResult UpdateCopyright(string copyrightId, string newContent)
		{

			CopyrightRequest request = new CopyrightRequest
										   {
											   Value = newContent
										   };

			return ClientFactory.Call(service => service.Administration.UpdateCopyright(copyrightId, request));

		}


		public ActionResult DeleteCopyright(string copyrightId)
		{
			return ClientFactory.Call(service => service.Administration.DeleteCopyright(copyrightId));
		}

		#endregion

		public ActionResult GetBucketsAssignedToLicense(string licenseId)
		{
			return ClientFactory.Call(service => service.Administration.GetLicenseBuckets(licenseId));
		}

		public ActionResult UpdateBucketsAssignedToLicense(string licenseId, string[] bucketList)
		{
			if (bucketList == null)
			{
				bucketList = new string[0];
			}

			ContentBucketIdListRequest bucketIds = new ContentBucketIdListRequest
													   {
														   Items = bucketList.ToList()
													   };
			return ClientFactory.Call(service => service.Administration.UpdateLicenseBuckets(licenseId, bucketIds));
		}

		public ActionResult GetSmsGatewayModule(string licenseId)
		{
			return ClientFactory.Call(service => service.SmsGatewayAdministration.GetSmsGateway(licenseId));
		}

		public ActionResult CreateShortCode(string licenseId)
		{
			ShortCode shortCode = ObjectFactory.Get<ShortCode>(this);
			return ClientFactory.Call(service => service.SmsGatewayAdministration.CreateShortCode(licenseId, shortCode));
		}

		public ActionResult UpdateShortCode(string licenseId, string shortCodeNumber)
		{
			ShortCode shortCode = ObjectFactory.Get<ShortCode>(this);
			return ClientFactory.Call(service => service.SmsGatewayAdministration.UpdateShortCode(licenseId, shortCodeNumber, shortCode));
		}

		public ActionResult DeleteShortCode(string licenseId, string shortCodeNumber)
		{
			return ClientFactory.Call(service => service.SmsGatewayAdministration.DeleteShortCode(licenseId, shortCodeNumber));
		}

		public ActionResult GetLongCodes(string licenseId, int offset, int count, string filter, SmsLongCodeFilterType filterType)
		{
			return ClientFactory.Call(service => service.SmsGatewayAdministration.SearchLongCodes(licenseId, offset, count, filter, filterType));
		}

		public ActionResult UpdateLongCode(string licenseId, string longCodeNumber)
		{
			return ClientFactory.Call(service => service.SmsGatewayAdministration.UpdateLongCode(licenseId, longCodeNumber, ObjectFactory.Get<LongCode>(this)));
		}

		public ActionResult PurchaseLongCode(string licenseId, string number)
		{
			LongCode longCode = new LongCode
									 {
										 Number = number,
										 RouteType = SmsNumberRouteType.CatchAll
									 };

			return ClientFactory.Call(service => service.SmsGatewayAdministration.PurchaseLongCode(licenseId, longCode));
		}

		public ActionResult DeleteLongCode(string licenseId, string longCodeNumber)
		{
			return ClientFactory.Call(service => service.SmsGatewayAdministration.DeleteLongCode(licenseId, longCodeNumber));
		}

		#region Client User Methods

		public ActionResult GetClientUsers(int offset, int count, string searchQuery, bool includeAdmin, bool includeUser, bool includeNone, string sort)
		{
			string filter = GetFilterQuery(includeAdmin, includeUser, includeNone);

			return ClientFactory.Call(service => service.Administration.GetClientUsers(offset, count, searchQuery, sort, filter));
		}

		public ActionResult CreateClientUser(string firstName, string lastName, string emailAddress, string phoneNumber,
											 string clientId, bool disabled, string permissionType)
		{
			if (string.IsNullOrWhiteSpace(clientId))
				clientId = Guid.Empty.ToString();

			ClientUserRequest request = new ClientUserRequest
			{
				ClientId = Guid.Parse(clientId),
				Disabled = disabled,
				EmailAddress = emailAddress,
				FirstName = firstName,
				LastName = lastName,
				PhoneNumber = phoneNumber,
				PermissionType = (ClientUserPermissionType)Enum.Parse(typeof(ClientUserPermissionType), permissionType)
			};

			return ClientFactory.Call(service => service.Administration.CreateClientUser(request));
		}

		public ActionResult UpdateClientUser(string id)
		{
			return ClientFactory.Call(service => service.Administration.UpdateClientUser(id, ObjectFactory.Get<ClientUserUpdateRequest>(Request)));
		}

		public ActionResult DeleteClientUser(string id)
		{
			return ClientFactory.Call(service => service.Administration.DeleteClientUser(id));
		}

		public ActionResult SetPassword()
		{
			return ClientFactory.CallAuthorization(service => service.Authorization.SetClientUserPassword(ObjectFactory.Get<PasswordRequest>(Request)));
		}

		public ActionResult ResetPassword(PasswordResetRequest request)
		{
			return ClientFactory.CallAuthorization(service => service.Authorization.ResetClientUserPassword(request));
		}

		public ActionResult UnlockClientUser(string id)
		{
			return ClientFactory.Call(service => service.Administration.UnlockClientUser(id));
		}

		#endregion

		#endregion Public MessageHandlers

		#region Private Helper Methods

		private LongCode ParseNumberData()
		{
			MessageFormat catchAllFormat = MessageFormat.Json;
			string catchAllWebsite = null;

			List<SmsKeyword> list = null;

			string number = Request.Params["number"];

			SmsNumberRouteType routeType;

			Enum.TryParse(Request.Params["routeType"], out routeType);

			if (routeType == SmsNumberRouteType.Keyword)
			{
				List<string> keywords = Request.GetList("keyword");
				List<string> websites = Request.GetList("website");
				List<string> formats = Request.GetList("format");

				int count = ListHelper.MinLength(keywords, websites, formats);

				list = new List<SmsKeyword>();

				for (int i = 0; i < count; i++)
				{
					MessageFormat format;

					Enum.TryParse(formats[i], true, out format);

					list.Add(new SmsKeyword
								 {
									 Value = keywords[i],
									 Website = websites[i],
									 Format = format
								 });
				}
			}
			else
			{
				Enum.TryParse(Request.Params["format"], out catchAllFormat);
				catchAllWebsite = Request.Params["website"];
			}

			return new LongCode
			{
				RouteType = routeType,
				Format = catchAllFormat,
				Website = catchAllWebsite,
				Number = number,
				Keywords = list
			};
		}

		private string GetFilterQuery(bool includeAdmin, bool includeUser, bool includeNone)
		{
			string result = includeAdmin ? "PermissionType eq " + (int)ClientUserPermissionType.Admin : "PermissionType ne " + (int)ClientUserPermissionType.Admin;

			result += includeUser
						  ? string.IsNullOrEmpty(result)
								? "PermissionType eq " + (int)ClientUserPermissionType.User
								: " or PermissionType eq " + (int)ClientUserPermissionType.User
						  : string.IsNullOrEmpty(result)
								? "PermissionType ne " + (int)ClientUserPermissionType.User
								: " and PermissionType ne " + (int)ClientUserPermissionType.User;

			result += includeNone
						  ? string.IsNullOrEmpty(result)
								? "PermissionType eq " + (int)ClientUserPermissionType.None
								: " or PermissionType eq " + (int)ClientUserPermissionType.None
						  : string.IsNullOrEmpty(result)
								? "PermissionType ne " + (int)ClientUserPermissionType.None
								: " and PermissionType ne " + (int)ClientUserPermissionType.None;

			return result;
		}

		#endregion Private Helper Methods
	}
}
