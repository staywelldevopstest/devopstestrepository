﻿using System.Web.Mvc;
using KswApi.Site.Framework;

namespace KswApi.Site.Areas.Callback.Controllers
{
	public class ReportingController : Controller
	{
		public ActionResult GetLog(string id)
		{
			return ClientFactory.Call(service => service.Logs.GetLog(id));
		}
	}
}