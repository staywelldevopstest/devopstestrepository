﻿using System;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects.Content;
using KswApi.Site.Framework;
using System.Web.Mvc;
using KswApi.Interface.Objects;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace KswApi.Site.Areas.Callback.Controllers
{
    public class ContentController : Controller
    {
        public ContentController()
        {
            ValidateRequest = false;
        }
		
		#region Language Methods

		public ActionResult GetLanguages(string bucketId, string id)
		{
			return ClientFactory.Call(service => service.Languages.GetLanguages(bucketId, id));
		}

		#endregion

		public ActionResult BucketOrigins()
        {
			return ClientFactory.Call(service => service.Origins.GetBucketOrigins());
        }

        public ActionResult CreateBucket()
        {
            return ClientFactory.Call(service => service.Buckets.CreateBucket(ObjectFactory.Get<ContentBucketCreateRequest>(this)));
        }

		public ActionResult Bucket(string id)
		{
			return ClientFactory.Call(service => service.Buckets.GetBucket(id));
		}

        public ActionResult Buckets()
        {
			return ClientFactory.Call(service => service.Buckets.SearchBuckets(ObjectFactory.Get<BucketSearchRequest>(this)));
        }

        public ActionResult UpdateBucket(string id)
        {
			return ClientFactory.Call(service => service.Buckets.UpdateBucket(id, ObjectFactory.Get<ContentBucketUpdateRequest>(this)));
        }

        public ActionResult DeleteBucket(string id)
        {
			return ClientFactory.Call(service => service.Buckets.DeleteBucket(id));
        }

		public ActionResult GetContent(string bucketId, string id, bool includeBody)
		{
            return ClientFactory.Call(service => service.Content.GetContent(bucketId, id, includeBody, false, null));
		}

		[ValidateInput(false)]
		public ActionResult CreateContent(string bucketId)
		{
			NewContentRequest request = ObjectFactory.Get<NewContentRequest>(this);

			return ClientFactory.Call(service => service.Content.CreateContent(bucketId, request));
		}

		public ActionResult UpdateContent(string bucketId, string id)
		{
			return ClientFactory.Call(service => service.Content.UpdateContent(bucketId, id, ObjectFactory.Get<ContentArticleRequest>(this)));
		}

		public ActionResult AddHistoryContent(string bucketId, string id)
		{
			return ClientFactory.Call(service => service.Content.AddHistoryContent(bucketId, id, ObjectFactory.Get<NewContentRequest>(this)));
		}

        public ActionResult GetTaxonomyTypes()
        {
            return ClientFactory.Call(service => service.TaxonomyType.SearchTaxonomyTypes());
        }

        public ActionResult SearchServiceLines(string path)
        {
            return ClientFactory.Call(service => service.Servicelines.SearchServicelines(path));
        }

		public ActionResult GetHistory(string bucketId, string id)
		{
			return ClientFactory.Call(service => service.Content.GetHistory(bucketId, id));
		}

		public ActionResult GetHistoryContent(string bucketId, string contentId, string id)
		{
			return ClientFactory.Call(service => service.Content.GetHistoryContent(bucketId, contentId, id, true));
		}

		public ActionResult Slugs(string bucketId, string title, string slug)
		{
			return ClientFactory.Call(service => service.Content.GetSlug(bucketId, slug, title));
		}

		public ActionResult UpdateImageDetail(string bucketId, string imageId)
		{
			ImageDetailRequest request = ObjectFactory.Get<ImageDetailRequest>(this);
			return ClientFactory.Call(service => service.Content.UpdateImageDetails(bucketId, imageId, request));
		}

		public void UploadImage(string bucketId, string imageId, HttpPostedFileBase file)
		{
			// have to set content type appropriately to force IE to work
			// plUpload uses an iFrame to download, and in IE, when it receives the json response type,
			// it prompts the user to "save" the json file, instead of passing it on to the javascript

			StreamRequest streamRequest = new StreamRequest(file.InputStream);
			streamRequest.ContentType = file.ContentType;

			ApiClient client = ClientFactory.GetClient();

			try
			{
				Response.ContentType = "text/html";
				ImageDetailResponse response = client.Content.CreateImage(bucketId, streamRequest);
				JsonSerializer serializer = new JsonSerializer();
				serializer.Formatting = Formatting.Indented;
				serializer.NullValueHandling = NullValueHandling.Ignore;
				serializer.Converters.Add(new StringEnumConverter());
				serializer.Serialize(Response.Output, response);
			}
			catch (ServiceException exception)
			{
				Response.ContentType = "text/html";

				Response.StatusCode = (int) exception.StatusCode;

				Error result = new Error
				{
					StatusCode = (int)exception.StatusCode,
					Details = exception.Message
				};

				JsonSerializer serializer = new JsonSerializer();
				serializer.Formatting = Formatting.Indented;
				serializer.NullValueHandling = NullValueHandling.Ignore;
				serializer.Converters.Add(new StringEnumConverter());
				serializer.Serialize(Response.Output, result);
			}
		}

		public ActionResult DownloadImage(string bucketId, string imageId)
		{
			return ClientFactory.Call("x", service => service.Content.GetImage(bucketId, imageId));
		}

		public ActionResult GetImageSlug(string bucketId, string id, string slug, string title)
		{
			return ClientFactory.Call(service => service.Content.GetImageSlug(bucketId, id, slug, title));
		}
    }
}
