﻿using KswApi.Interface.Objects;
using KswApi.Site.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace KswApi.Site.Areas.Callback.Controllers
{
	public class UserController : Controller
	{
		public ActionResult UpdateUserSettings()
		{
			UserSettingsRequest userSettings = ObjectFactory.Get<UserSettingsRequest>(this);
			return ClientFactory.Call(service => service.User.UpdateCurrentUserSettings(userSettings));
		}

		public ActionResult GetUserSettings()
		{
			return ClientFactory.Call(service => service.User.GetCurrentUserSettings());
		}

		public ActionResult GetUserLicenseDetailList()
		{
			return ClientFactory.Call(service => service.User.GetUserLicenseDetails());
		}

		public ActionResult ResetLicenseApplicationSecret(string licenseId, string applicationId)
		{
			return ClientFactory.Call(service => service.Administration.ResetLicenseApplicationSecret(licenseId, applicationId));
		}
	}
}
