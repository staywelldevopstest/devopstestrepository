﻿using System;

namespace KswApi.Site.Areas.Callback.Models
{
	public class LogModel
	{
		public int Offset { get; set; }
		public int Count { get; set; }
		public string Type { get; set; }
		public string Sort { get; set; }
        public bool IncludeWarnings { get; set; }
        public bool IncludeErrors { get; set; }
        public bool IncludeInfo { get; set; }
        public string DateRange { get; set; }
        //public DateTime FromDate { get; set; }
        //public DateTime ToDate { get; set; }
        public string ClientFilter { get; set; }
        public string LicenseFilter { get; set; }
        public string LogId { get; set; }
	}
}