﻿namespace KswApi.ImportFromUcrV1
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.Log = new System.Windows.Forms.RichTextBox();
			this.button1 = new System.Windows.Forms.Button();
			this.pBar = new System.Windows.Forms.ProgressBar();
			this.dailyNewsFeed = new System.Windows.Forms.CheckBox();
			this.kramesHealthsheets = new System.Windows.Forms.CheckBox();
			this.nciPatientSummaries = new System.Windows.Forms.CheckBox();
			this.wellnessLibrary = new System.Windows.Forms.CheckBox();
			this.marriamWebsterDictionary = new System.Windows.Forms.CheckBox();
			this.greystonAdult = new System.Windows.Forms.CheckBox();
			this.greystonCancerCenter = new System.Windows.Forms.CheckBox();
			this.harvardVideosV2 = new System.Windows.Forms.CheckBox();
			this.kramesStaywellVideosV2 = new System.Windows.Forms.CheckBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.serviceLocation = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.applicationSecret = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.applicationId = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// Log
			// 
			this.Log.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.Log.Location = new System.Drawing.Point(184, 41);
			this.Log.Name = "Log";
			this.Log.Size = new System.Drawing.Size(438, 372);
			this.Log.TabIndex = 1;
			this.Log.Text = "";
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(184, 12);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(57, 23);
			this.button1.TabIndex = 2;
			this.button1.Text = "Import";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.Button1Click);
			// 
			// pBar
			// 
			this.pBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.pBar.Location = new System.Drawing.Point(247, 12);
			this.pBar.Name = "pBar";
			this.pBar.Size = new System.Drawing.Size(375, 23);
			this.pBar.TabIndex = 3;
			// 
			// dailyNewsFeed
			// 
			this.dailyNewsFeed.AutoSize = true;
			this.dailyNewsFeed.Location = new System.Drawing.Point(6, 19);
			this.dailyNewsFeed.Name = "dailyNewsFeed";
			this.dailyNewsFeed.Size = new System.Drawing.Size(106, 17);
			this.dailyNewsFeed.TabIndex = 4;
			this.dailyNewsFeed.Text = "Daily News Feed";
			this.dailyNewsFeed.UseVisualStyleBackColor = true;
			// 
			// kramesHealthsheets
			// 
			this.kramesHealthsheets.AutoSize = true;
			this.kramesHealthsheets.Location = new System.Drawing.Point(6, 43);
			this.kramesHealthsheets.Name = "kramesHealthsheets";
			this.kramesHealthsheets.Size = new System.Drawing.Size(126, 17);
			this.kramesHealthsheets.TabIndex = 5;
			this.kramesHealthsheets.Text = "Krames Healthsheets";
			this.kramesHealthsheets.UseVisualStyleBackColor = true;
			// 
			// nciPatientSummaries
			// 
			this.nciPatientSummaries.AutoSize = true;
			this.nciPatientSummaries.Location = new System.Drawing.Point(6, 67);
			this.nciPatientSummaries.Name = "nciPatientSummaries";
			this.nciPatientSummaries.Size = new System.Drawing.Size(134, 17);
			this.nciPatientSummaries.TabIndex = 6;
			this.nciPatientSummaries.Text = "NCI Patient Summaries";
			this.nciPatientSummaries.UseVisualStyleBackColor = true;
			// 
			// wellnessLibrary
			// 
			this.wellnessLibrary.AutoSize = true;
			this.wellnessLibrary.Location = new System.Drawing.Point(6, 91);
			this.wellnessLibrary.Name = "wellnessLibrary";
			this.wellnessLibrary.Size = new System.Drawing.Size(103, 17);
			this.wellnessLibrary.TabIndex = 7;
			this.wellnessLibrary.Text = "Wellness Library";
			this.wellnessLibrary.UseVisualStyleBackColor = true;
			// 
			// marriamWebsterDictionary
			// 
			this.marriamWebsterDictionary.AutoSize = true;
			this.marriamWebsterDictionary.Location = new System.Drawing.Point(6, 115);
			this.marriamWebsterDictionary.Name = "marriamWebsterDictionary";
			this.marriamWebsterDictionary.Size = new System.Drawing.Size(156, 17);
			this.marriamWebsterDictionary.TabIndex = 8;
			this.marriamWebsterDictionary.Text = "Merriam-Webster Dictionary";
			this.marriamWebsterDictionary.UseVisualStyleBackColor = true;
			// 
			// greystonAdult
			// 
			this.greystonAdult.AutoSize = true;
			this.greystonAdult.Location = new System.Drawing.Point(6, 139);
			this.greystonAdult.Name = "greystonAdult";
			this.greystonAdult.Size = new System.Drawing.Size(101, 17);
			this.greystonAdult.TabIndex = 9;
			this.greystonAdult.Text = "Greystone Adult";
			this.greystonAdult.UseVisualStyleBackColor = true;
			// 
			// greystonCancerCenter
			// 
			this.greystonCancerCenter.AutoSize = true;
			this.greystonCancerCenter.Checked = true;
			this.greystonCancerCenter.CheckState = System.Windows.Forms.CheckState.Checked;
			this.greystonCancerCenter.Location = new System.Drawing.Point(6, 163);
			this.greystonCancerCenter.Name = "greystonCancerCenter";
			this.greystonCancerCenter.Size = new System.Drawing.Size(145, 17);
			this.greystonCancerCenter.TabIndex = 10;
			this.greystonCancerCenter.Text = "Greystone Cancer Center";
			this.greystonCancerCenter.UseVisualStyleBackColor = true;
			// 
			// harvardVideosV2
			// 
			this.harvardVideosV2.AutoSize = true;
			this.harvardVideosV2.Location = new System.Drawing.Point(6, 187);
			this.harvardVideosV2.Name = "harvardVideosV2";
			this.harvardVideosV2.Size = new System.Drawing.Size(115, 17);
			this.harvardVideosV2.TabIndex = 11;
			this.harvardVideosV2.Text = "Harvard Videos V2";
			this.harvardVideosV2.UseVisualStyleBackColor = true;
			// 
			// kramesStaywellVideosV2
			// 
			this.kramesStaywellVideosV2.AutoSize = true;
			this.kramesStaywellVideosV2.Location = new System.Drawing.Point(6, 211);
			this.kramesStaywellVideosV2.Name = "kramesStaywellVideosV2";
			this.kramesStaywellVideosV2.Size = new System.Drawing.Size(154, 17);
			this.kramesStaywellVideosV2.TabIndex = 12;
			this.kramesStaywellVideosV2.Text = "Krames Staywell Videos V2";
			this.kramesStaywellVideosV2.UseVisualStyleBackColor = true;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.dailyNewsFeed);
			this.groupBox1.Controls.Add(this.kramesStaywellVideosV2);
			this.groupBox1.Controls.Add(this.kramesHealthsheets);
			this.groupBox1.Controls.Add(this.harvardVideosV2);
			this.groupBox1.Controls.Add(this.nciPatientSummaries);
			this.groupBox1.Controls.Add(this.greystonCancerCenter);
			this.groupBox1.Controls.Add(this.wellnessLibrary);
			this.groupBox1.Controls.Add(this.greystonAdult);
			this.groupBox1.Controls.Add(this.marriamWebsterDictionary);
			this.groupBox1.Location = new System.Drawing.Point(12, 177);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(165, 236);
			this.groupBox1.TabIndex = 13;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Libraries";
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.serviceLocation);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.applicationSecret);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Controls.Add(this.applicationId);
			this.groupBox2.Controls.Add(this.label1);
			this.groupBox2.Location = new System.Drawing.Point(12, 12);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(165, 159);
			this.groupBox2.TabIndex = 14;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "KSW API Credentials";
			// 
			// serviceLocation
			// 
			this.serviceLocation.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.serviceLocation.Location = new System.Drawing.Point(6, 41);
			this.serviceLocation.Name = "serviceLocation";
			this.serviceLocation.Size = new System.Drawing.Size(151, 20);
			this.serviceLocation.TabIndex = 5;
			this.serviceLocation.Text = "http://localhost/kswapi";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(3, 24);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(87, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Service Location";
			// 
			// applicationSecret
			// 
			this.applicationSecret.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.applicationSecret.Location = new System.Drawing.Point(6, 129);
			this.applicationSecret.Name = "applicationSecret";
			this.applicationSecret.PasswordChar = '*';
			this.applicationSecret.Size = new System.Drawing.Size(150, 20);
			this.applicationSecret.TabIndex = 3;
			this.applicationSecret.Text = "QZhA9wysXV2R4RYT3iSjJssF6xU4Ra8fm2gGAl9TMqPRDPr4KCVX7XeBXrmaBkuB";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 113);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(93, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Application Secret";
			// 
			// applicationId
			// 
			this.applicationId.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.applicationId.Location = new System.Drawing.Point(6, 86);
			this.applicationId.Name = "applicationId";
			this.applicationId.Size = new System.Drawing.Size(151, 20);
			this.applicationId.TabIndex = 1;
			this.applicationId.Text = "297282f5-2c89-498e-8285-28ffbbf8832f";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(3, 69);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(73, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Application ID";
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(634, 425);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.Controls.Add(this.pBar);
			this.Controls.Add(this.button1);
			this.Controls.Add(this.Log);
			this.MinimumSize = new System.Drawing.Size(650, 270);
			this.Name = "MainForm";
			this.Text = "Import from UCR into KSW API";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.RichTextBox Log;
		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.ProgressBar pBar;
		private System.Windows.Forms.CheckBox dailyNewsFeed;
		private System.Windows.Forms.CheckBox kramesHealthsheets;
		private System.Windows.Forms.CheckBox nciPatientSummaries;
		private System.Windows.Forms.CheckBox wellnessLibrary;
		private System.Windows.Forms.CheckBox marriamWebsterDictionary;
		private System.Windows.Forms.CheckBox greystonAdult;
		private System.Windows.Forms.CheckBox greystonCancerCenter;
		private System.Windows.Forms.CheckBox harvardVideosV2;
		private System.Windows.Forms.CheckBox kramesStaywellVideosV2;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox applicationSecret;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox applicationId;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox serviceLocation;
		private System.Windows.Forms.Label label3;
	}
}

