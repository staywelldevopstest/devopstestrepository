﻿using System;
using System.Collections.Generic;
using System.Xml;

namespace KswApi.ImportFromUcrV1
{
	class XmlValueReader
	{
		private readonly XmlElement _document;
		private XmlNode _current;
		public XmlValueReader(XmlElement document)
		{
			_current = _document = document;
		}

		public string Value()
		{
			if (_current == null)
				return null;

			return _current.InnerText;
		}

		public string Value(string name)
		{
			if (_current == null)
				return null;

			XmlNode node = _current.SelectSingleNode(name);

			if (node == null)
				return null;

			return node.InnerText;
		}

		public List<string> Values(string name)
		{
			if (_current == null)
				return null;

			XmlNode node = _current.SelectSingleNode(name);

			if (node == null)
				return null;

			if (!node.HasChildNodes)
				return null;

			List<string> result = new List<string>();
			
			foreach (XmlNode child in node.ChildNodes)
				result.Add(child.InnerText);

			return result;
		}

		public List<TResult> Values<TResult>(string name, Func<string, TResult> converter)
		{
			if (_current == null)
				return null;

			XmlNode node = _current.SelectSingleNode(name);

			if (node == null)
				return null;

			if (!node.HasChildNodes)
				return null;

			List<TResult> result = new List<TResult>();

			foreach (XmlNode child in node.ChildNodes)
				result.Add(converter(child.InnerText));

			return result;
		}

		public string Xml()
		{
			if (_current == null)
				return null;

			return _current.InnerXml;
		}

		public string Xml(string name)
		{
			if (_current == null)
				return null;

			XmlNode node = _current.SelectSingleNode(name);

			if (node == null)
				return null;

			return node.InnerXml;
		}

		public bool Select(string name)
		{
			_current = _document.SelectSingleNode(name);
			return _current != null;
		}
	}
}
