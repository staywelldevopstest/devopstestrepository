﻿namespace KswApi.ImportFromUcrV1
{
	public class ImportOptions
	{
		public bool WellnessLibrary;
		public bool KramesHealthsheets;
		public bool DailyNewsFeed;
		public bool NciPatientSummaries;
		public bool GreystoneAdult;
		public bool GreystoneCancerCenter;
		public bool MerriamWebsterDictionary;
		public bool KramesStaywellVideosV2;
		public bool HarvardVideosV2;

		public string ServiceLocation { get; set; }
		public string ApplicationId { get; set; }
		public string ApplicationSecret { get; set; }
	}
}
