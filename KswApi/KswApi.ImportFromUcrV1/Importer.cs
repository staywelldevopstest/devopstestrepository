﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Xml;
using KswApi.Enums;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.Content;
using KswApi.Utility.Helpers;

namespace KswApi.ImportFromUcrV1
{
	public class Importer
	{
		private ApiClient _client;
		private readonly MainForm _form;
		private int _total;
		private int _count;
		private Thread _thread;
		readonly ManualResetEvent _stopping = new ManualResetEvent(false);

		public Importer(MainForm form)
		{
			_form = form;
		}

		public void Start(ImportOptions options)
		{
			if (_thread != null)
				return;

			_stopping.Reset();

			_client = new ApiClient(options.ServiceLocation, options.ApplicationId, options.ApplicationSecret, TokenStoreType.SingleApplication);

			_thread = new Thread(StartThread);
			_thread.Start(options);
		}

		public void Stop()
		{
			_stopping.Set();
		}

		private void StartThread(object obj)
		{
			try
			{
				ImportContent((ImportOptions)obj);

				if (_stopping.WaitOne(0, false))
				{
					_form.UpdateStatus("******** Stopped ********");
					_form.UpdateStatus("");
				}
				else
				{
					_form.UpdateStatus("******** Finished Successfully ********");
					_form.UpdateStatus("");
				}
			}
			catch (Exception exception)
			{
				_form.UpdateStatus("******** Failed with Exception ********");
				_form.UpdateStatus(exception.ToString());
				_form.UpdateStatus("");
			}

			_form.Finished();
		}

		private void ImportContent(ImportOptions options)
		{
			XmlDocument contentList = new XmlDocument();

			contentList.Load("http://external.ws.staywell.com/RichTest/Content.svc/ListAllContent");

			_total = contentList.SelectNodes("//ContentObject").Count;

			if (options.WellnessLibrary)
			{
				ContentBucketResponse bucket = GetBucket("Wellness Library", 1);
				XmlNodeList contentObjects = contentList.SelectNodes("//ContentObject[@ContentTypeId='1']");
				ProcessContentTypeList(contentObjects, bucket);
			}

			if (options.KramesHealthsheets)
			{
				ContentBucketResponse bucket = GetBucket("Krames Healthsheets", 3);
				XmlNodeList contentObjects = contentList.SelectNodes("//ContentObject[@ContentTypeId='3']");
				ProcessContentTypeList(contentObjects, bucket);
			}

			if (options.DailyNewsFeed)
			{
				ContentBucketResponse bucket = GetBucket("Daily News Feed", 6);
				XmlNodeList contentObjects = contentList.SelectNodes("//ContentObject[@ContentTypeId='6']");
				ProcessContentTypeList(contentObjects, bucket);
			}

			if (options.NciPatientSummaries)
			{
				ContentBucketResponse bucket = GetBucket("NCI Patient Summaries", 37);
				XmlNodeList contentObjects = contentList.SelectNodes("//ContentObject[@ContentTypeId='37']");
				ProcessContentTypeList(contentObjects, bucket);
			}

			if (options.GreystoneAdult)
			{
				ContentBucketResponse bucket = GetBucket("Greystone Adult", 85);
				XmlNodeList contentObjects = contentList.SelectNodes("//ContentObject[@ContentTypeId='85']");
				ProcessContentTypeList(contentObjects, bucket);
			}

			if (options.GreystoneCancerCenter)
			{
				ContentBucketResponse bucket = GetBucket("Greystone Cancer Center", 86);
				XmlNodeList contentObjects = contentList.SelectNodes("//ContentObject[@ContentTypeId='86']");
				ProcessContentTypeList(contentObjects, bucket);
			}

			if (options.MerriamWebsterDictionary)
			{
				ContentBucketResponse bucket = GetBucket("Merriam-Webster Dictionary", 120);
				XmlNodeList contentObjects = contentList.SelectNodes("//ContentObject[@ContentTypeId='120']");
				ProcessContentTypeList(contentObjects, bucket);
			}

			if (options.KramesStaywellVideosV2)
			{
				ContentBucketResponse bucket = GetBucket("Daily News Feed", 138);
				XmlNodeList contentObjects = contentList.SelectNodes("//ContentObject[@ContentTypeId='138']");
				ProcessContentTypeList(contentObjects, bucket);
			}

			if (options.HarvardVideosV2)
			{
				ContentBucketResponse bucket = GetBucket("Harvard Videos V2", 139);
				XmlNodeList contentObjects = contentList.SelectNodes("//ContentObject[@ContentTypeId='139']");
				ProcessContentTypeList(contentObjects, bucket);
			}
		}

		private ContentBucketResponse GetBucket(string name, int legacyId)
		{
			ContentBucketList list = _client.Buckets.SearchBuckets(new BucketSearchRequest { Offset = 0, Count = 1, LegacyId = legacyId });

			if (list.Items.Count > 0)
				return list.Items.First();

			ContentOriginList origins = _client.Origins.GetBucketOrigins();

			ContentBucketCreateRequest request = new ContentBucketCreateRequest
										   {
											   Name = name,
											   Slug = SlugHelper.CreateSlug(name),
											   LegacyId = legacyId,
											   Type = ContentType.Article,
											   OriginId = origins.Items.First().Id
										   };

			ContentBucketResponse response = _client.Buckets.CreateBucket(request);

			return response;
		}

		private void ProcessContentTypeList(XmlNodeList contentObjects, ContentBucketResponse bucket)
		{
			foreach (XmlElement contentObject in contentObjects)
			{
				if (_stopping.WaitOne(0, false))
					break;

				string contentId = contentObject.Attributes["ContentId"].Value;
				string contentTypeId = contentObject.Attributes["ContentTypeId"].Value;
				string objectType = contentObject.Attributes["ContentObjectType"].Value;
				string title = contentObject.ChildNodes[1].InnerText;
				_form.UpdateStatus(string.Format("Importing {0} of {1}: {2},{3} \"{4}\"", _count + 1, _total, contentTypeId, contentId, title));

				string url =
					string.Format(
						"{0}/{1}/Content.svc/GetContent?xmlRequest=<GetContent ContentTypeId=\"{2}\" ContentId=\"{3}\" IncludeBlocked=\"false\" GetOriginal=\"false\" />",
						"http://external.ws.staywell.com",
						"RichTest",
						contentTypeId,
						contentId
						);

				XmlDocument document = new XmlDocument();
				document.Load(url);
				ImportIntoKswApi(document.DocumentElement, objectType, bucket);
				_count++;
				_form.UpdateStatus(_count, _total);
			}
		}

		private void ImportIntoKswApi(XmlElement document, string objectType, ContentBucketResponse bucket)
		{
			if (objectType != "Document")
				throw new Exception(string.Format("UCR Type {0} not supported.", objectType));

			ImportDocumentIntoKswApi(document, bucket);
		}

		private void ImportDocumentIntoKswApi(XmlElement document, ContentBucketResponse bucket)
		{
			_form.UpdateStatus("Retrieved from UCR");

			XmlElement element = document["ContentObject"];

			if (element == null)
				return;

			XmlAttribute attribute = element.Attributes["ContentId"];
			if (attribute == null)
				return;

			string id = attribute.Value;

			string languageCode = GetKswLanguage(element["Language"].Attributes["Code"].Value);

			ContentSearchRequest searchRequest = new ContentSearchRequest
												 {
													 Count = 1,
													 Types = new List<SearchType> { SearchType.Content },
													 Buckets = new List<string> { bucket.Id.ToString() },
													 LegacyIds = new List<string> { id },
													 Languages = new List<string> { languageCode }
												 };


			ContentList result = _client.Content.SearchContent(searchRequest);

			ContentResponse content = result.Items.FirstOrDefault();

			//call Ksw Api method

			XmlValueReader reader = new XmlValueReader(element);

			if (content == null)
			{
				NewContentRequest request = new NewContentRequest();
				request.LanguageCode = languageCode;

				GetProperties(reader, request, bucket);
				SlugResponse slugResponse = _client.Content.GetSlug(bucket.Id.ToString(), request.Title, null);
				request.Slug = slugResponse.Value;
				request.LegacyId = id;
				_client.Content.CreateContent(bucket.Id.ToString(), request);
			}
			else
			{
				ContentArticleRequest request = new ContentArticleRequest();
				GetProperties(reader, request, bucket);
				request.LegacyId = id;
				_client.Content.UpdateContent(bucket.Id.ToString(), content.Id.ToString(), request);
			}

			_form.UpdateStatus("Saved to Ksw Api");
		}

		private void GetProperties(XmlValueReader reader, ContentArticleRequest request, ContentBucketResponse bucket)
		{
			request.Title = reader.Value("RegularTitle");
			request.InvertedTitle = reader.Value("InvertedTitle");
			if (request.Title == request.InvertedTitle)
				request.InvertedTitle = null;
			request.AlternateTitles = reader.Values("AdditionalTitles");
			request.AgeCategories = reader.Values("AgeGroups", GetKswAgeCategory);

			// if there are no age categories, specify all of them
			if (request.AgeCategories == null || request.AgeCategories.Count == 0)
				request.AgeCategories = new List<AgeCategory> { AgeCategory.Infant, AgeCategory.Child, AgeCategory.Teen, AgeCategory.Adult, AgeCategory.Senior };

			request.Authors = reader.Values("Authors");
			request.OnlineEditors = reader.Values("OnlineEditors");
			request.OnlineMedicalReviewers = reader.Values("OnlineMedicalReviewers");
			request.Blurb = reader.Xml("Blurb");
			request.Gender = GetKswGender(reader.Value("GenderCode"));
			string value = reader.Value("PostingDate");
			if (!string.IsNullOrEmpty(value))
				request.PostingDate = DateTime.Parse(value);
			value = reader.Xml("Content/body");
			if (value != null)
			{
				request.Segments = new List<ContentSegmentRequest>
					                   {
						                   new ContentSegmentRequest
						                   {
							                   IdOrSlug = bucket.Segments.First().Id.ToString(),
							                   Body = value
						                   }
					                   };
			}

			request.Publish = true;
		}

		private Gender GetKswGender(string value)
		{
			switch (value)
			{
				case "A":
					return Gender.All;
				case "M":
					return Gender.Male;
				case "F":
					return Gender.Female;
				default:
					throw new ArgumentException(string.Format("Invalid gender: {0}.", value));

			}
		}

		private AgeCategory GetKswAgeCategory(string value)
		{
			switch (value)
			{
				case "Infant (0 - 11 mo)":
					return AgeCategory.Infant;
				case "Childhood (11 mo - 12 yrs)":
					return AgeCategory.Child;
				case "Teen (12 - 18 yrs)":
					return AgeCategory.Teen;
				case "Adult (18+)":
					return AgeCategory.Adult;
				case "Senior":
					return AgeCategory.Senior;
				default:
					throw new ArgumentException(string.Format("Invalid age group: {0}.", value));
			}
		}

		private string GetKswLanguage(string value)
		{
			switch (value)
			{
				case "tgl":
					return "tl";
				case "aa":
					return "ar";
				case "po":
					return "pt";
				default:
					return value;
			}
		}
	}
}
