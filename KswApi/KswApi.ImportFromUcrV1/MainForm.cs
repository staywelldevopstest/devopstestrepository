﻿using System;
using System.Windows.Forms;
using System.Xml;

namespace KswApi.ImportFromUcrV1
{
	public partial class MainForm : Form
	{
		private Importer _importer;

		public MainForm()
		{
			InitializeComponent();
		}

		public void UpdateStatus(string status)
		{
			if (InvokeRequired)
			{
				Invoke(new Action<string>(UpdateStatus), status);
				return;
			}

			Log.AppendText(status);
			Log.AppendText(Environment.NewLine);
		}

		public void UpdateStatus(int position, int maximum)
		{
			if (InvokeRequired)
			{
				Invoke(new Action<int, int>(UpdateStatus), position, maximum);
				return;
			}

			pBar.Maximum = maximum;
			pBar.Value = position;
		}

		public void Finished()
		{
			if (InvokeRequired)
			{
				Invoke(new Action(Finished));
				return;
			}

			UpdateStatus(string.Format("Finished at {0}", DateTime.Now));

			button1.Text = "Import";

			_importer = null;

			pBar.Value = 0;
		}

		private void Button1Click(object sender, EventArgs e)
		{
			if (_importer == null)
				StartImport();
			else
				StopImport();
		}

		private void StartImport()
		{
			Log.Clear();
			pBar.Value = 0;

			button1.Text = "Cancel";

			UpdateStatus(string.Format("Starting at {0}", DateTime.Now));

			UpdateStatus("Loading content list from UCR...");

			ImportOptions options = new ImportOptions
			{
				ServiceLocation = serviceLocation.Text,
				ApplicationId = applicationId.Text,
				ApplicationSecret = applicationSecret.Text,
				WellnessLibrary = wellnessLibrary.Checked,
				KramesHealthsheets = kramesHealthsheets.Checked,
				DailyNewsFeed = dailyNewsFeed.Checked,
				NciPatientSummaries = nciPatientSummaries.Checked,
				GreystoneAdult = greystonAdult.Checked,
				GreystoneCancerCenter = greystonCancerCenter.Checked,
				MerriamWebsterDictionary = marriamWebsterDictionary.Checked,
				KramesStaywellVideosV2 = kramesStaywellVideosV2.Checked,
				HarvardVideosV2 = harvardVideosV2.Checked
			};

			_importer = new Importer(this);

			_importer.Start(options);
		}

		private void StopImport()
		{
			if (_importer == null)
				return;

			UpdateStatus("Stopping import...");

			_importer.Stop();
		}

		private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
		{
			if (_importer != null)
				StopImport();
		}
	}
}
