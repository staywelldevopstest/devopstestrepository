﻿using System;
using System.Collections;
using System.Configuration.Install;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using KswApi.AuxiliaryService.Framework;

namespace KswApi.AuxiliaryService
{
	internal static class Program
	{
		private static readonly string[] DebugParameters = new string[] { "debug", "-d" };
		private static readonly string[] InstallParameters = new string[] { "install", "-i" };
		private static readonly string[] UninstallParameters = new string[] { "uninstall", "-u" };
		private static readonly string[] StartParameters = new string[] { "start" };
		private static readonly string[] StopParameters = new string[] { "stop" };

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		private static void Main(string[] parameters)
		{
			if (parameters.Length == 0)
			{
				RunService();
			}
			else
			{
				ProcessParameters(parameters);
			}
		}

		private static void ProcessParameters(string[] parameters)
		{
			string command = parameters[0].ToLower();

			if (DebugParameters.Contains(command))
			{
				DebugService();
			}
			else if (InstallParameters.Contains(command))
			{
				InstallService();
			}
			else if (UninstallParameters.Contains(command))
			{
				UninstallService();
			}
			else if (StartParameters.Contains(command))
			{
				StartService();
			}
			else if (StopParameters.Contains(command))
			{
				StopService();
			}
			else
			{
				PrintUsage();
			}
		}

		private static void InstallService()
		{
			InstallContext context = new InstallContext(@"install.log", null);

			Assembly assembly = Assembly.GetExecutingAssembly();

			context.Parameters.Add("assemblypath", assembly.Location);

			ProjectInstaller installer = new ProjectInstaller
											 {
												 Context = context
											 };

			IDictionary state = new Hashtable();

			try
			{
				installer.Install(state);
				installer.Commit(state);
			}
			catch (Exception exception)
			{
				context.LogMessage(exception.Message);
				try
				{
					installer.Rollback(state);
				}
				catch (Exception exception2)
				{
					installer.Context.LogMessage(exception2.Message);
				}
			}
		}

		private static void UninstallService()
		{
			InstallContext context = new InstallContext(@"install.log", null);

			Assembly assembly = Assembly.GetExecutingAssembly();

			context.Parameters.Add("assemblypath", assembly.Location);

			ProjectInstaller installer = new ProjectInstaller
											 {
												 Context = context
											 };

			try
			{
				installer.Uninstall(null);
			}
			catch (Exception exception)
			{
				context.LogMessage(exception.Message);
			}
		}

		private static void StartService()
		{
			ServiceController controller = new ServiceController("KswApiAuxiliaryService");
			controller.Start();
			TimeSpan timeout = TimeSpan.FromMinutes(1);
			controller.WaitForStatus(ServiceControllerStatus.Running, timeout);
		}

		private static void StopService()
		{
			ServiceController controller = new ServiceController("KswApiAuxiliaryService");
			controller.Stop();
			TimeSpan timeout = TimeSpan.FromMinutes(1);
			controller.WaitForStatus(ServiceControllerStatus.Stopped, timeout);
		}

		private static void PrintUsage()
		{
			string name = Path.GetFileName(Process.GetCurrentProcess().MainModule.FileName);
			Console.Out.WriteLine();
			Console.Out.WriteLine("Invalid options");
			Console.Out.WriteLine();
			Console.Out.WriteLine("Usage:");
			Console.Out.WriteLine();
			Console.Out.WriteLine("  To debug the service:");
			foreach (string s in DebugParameters)
				Console.Out.WriteLine("    {0} {1}", name, s);
			Console.Out.WriteLine();

			Console.Out.WriteLine("  To install the service:");
			foreach (string s in InstallParameters)
				Console.Out.WriteLine("    {0} {1}", name, s);
			Console.Out.WriteLine();

			Console.Out.WriteLine("  To uninstall the service:");
			foreach (string s in UninstallParameters)
				Console.Out.WriteLine("    {0} {1}", name, s);
			Console.Out.WriteLine();

			Console.Out.WriteLine("  To start the service:");
			foreach (string s in StartParameters)
				Console.Out.WriteLine("    {0} {1}", name, s);
			Console.Out.WriteLine();

			Console.Out.WriteLine("  To stop the service:");
			foreach (string s in StopParameters)
				Console.Out.WriteLine("    {0} {1}", name, s);
			Console.Out.WriteLine();
		}


		private static void RunService()
		{
			ServiceBase[] servicesToRun = new ServiceBase[]
				                              {
					                              new KswApiAuxiliaryService()
				                              };
			ServiceBase.Run(servicesToRun);
		}

		private static void DebugService()
		{
			CommandLine commandLine = new CommandLine();
			commandLine.Run();
		}
	}
}
