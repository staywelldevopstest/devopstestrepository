﻿using System.ServiceProcess;
using KswApi.AuxiliaryService.Framework;

namespace KswApi.AuxiliaryService
{
	public partial class KswApiAuxiliaryService : ServiceBase
	{
		private static readonly TaskScheduler TaskScheduler = new TaskScheduler();

		public KswApiAuxiliaryService()
		{
			InitializeComponent();
		}

		protected override void OnStart(string[] args)
		{
			TaskScheduler.Start();
		}

		protected override void OnStop()
		{
			TaskScheduler.Stop();
		}
	}
}
