﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using KswApi.AuxiliaryService.Framework;

namespace KswApi.AuxiliaryService
{
	[RunInstaller(true)]
	public partial class ProjectInstaller : System.Configuration.Install.Installer
	{
		public ProjectInstaller()
		{
			InitializeComponent();
		}

		public override void Install(System.Collections.IDictionary stateSaver)
		{
			if (!EventLog.SourceExists(Constant.EVENT_LOG_SOURCE))
				EventLog.CreateEventSource(Constant.EVENT_LOG_SOURCE, Constant.EVENT_LOG_NAME);

			Console.WriteLine("Event log created");

			base.Install(stateSaver);
		}
	}
}
