﻿using System;
using System.Reflection;
using KswApi.Interface.Objects;

namespace KswApi.AuxiliaryService.Framework
{
	internal class ScheduledTask : IComparable<ScheduledTask>
	{
		public DateTime Time { get; set; }
		public ConstructorInfo Constructor { get; set; }
		public Type Type { get; set; }
		public Task Task { get; set; }

		public int CompareTo(ScheduledTask other)
		{
			return Time.CompareTo(other.Time);
		}
	}
}
