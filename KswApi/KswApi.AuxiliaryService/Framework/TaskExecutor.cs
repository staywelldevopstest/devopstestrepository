﻿using System;
using System.Configuration;
using System.Threading;
using KswApi.Enums;
using KswApi.Tasks;
using KswApi.Tasks.Interfaces;

namespace KswApi.AuxiliaryService.Framework
{
	internal class TaskExecutor : IDisposable
	{
		private readonly AutoResetEvent _executionEvent = new AutoResetEvent(false);
		private readonly ManualResetEvent _stop = new ManualResetEvent(false);
		private readonly Thread _thread;
		private readonly IExecutionTracker _tracker;

		public TaskExecutor(IExecutionTracker tracker)
		{
			_tracker = tracker;
			_thread = new Thread(Run);
			_thread.Start();
		}

		public ScheduledTask ScheduledTask { get; private set; }
		public ITask Task { get; set; }

		#region Public Properties

		public DateTime StartTime { get; private set; }

		#endregion

		#region Public Methods

		public void Execute(ScheduledTask task)
		{
			StartTime = DateTime.UtcNow;
			ScheduledTask = task;
			_executionEvent.Set();
		}

		public void Stop()
		{
			_stop.Set();

			ITask task = Task;

			if (task != null)
			{
				task.Stop();
			}

			_thread.Join();
		}

		#endregion

		#region Private Methods

		private void Run()
		{
			WaitHandle[] handles = new WaitHandle[] { _stop, _executionEvent };

			while (true)
			{
				if (_stop.WaitOne(0, false))
					break;

				int waitItem = WaitHandle.WaitAny(handles);

				if (waitItem == 0) // stop
					break;

				ITask task = null;

				ScheduledTask scheduledTask = ScheduledTask;

				if (scheduledTask != null)
				{
					task = (ITask) scheduledTask.Constructor.Invoke(null);

					Task = task;

					task.Task = scheduledTask.Task;
				}

				if (task != null)
				{
					if (scheduledTask.Task != null && scheduledTask.Task.Duration != default(TimeSpan))
						task.EndTime = DateTime.UtcNow + scheduledTask.Task.Duration;
					else
						task.EndTime = DateTime.UtcNow + TimeSpan.FromMinutes(Constant.MAXIMUM_TASK_TIME_IN_MINUTES);

					try
					{
						ApiClient client = ClientFactory.GetClient();

						task.Run(client, _tracker);
					}
					catch (Exception exception)
					{

					}
				}

				ScheduledTask = null;

				// report back to the scheduler that the task is finished
				_tracker.Finished(this);
			}
		}

		#endregion

		public void Dispose()
		{
			_stop.Set();

			Stop();

			_thread.Join(TimeSpan.FromSeconds(Constant.END_TASK_TIMEOUT_IN_SECONDS));
		}
	}
}
