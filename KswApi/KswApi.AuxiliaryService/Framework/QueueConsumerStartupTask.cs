﻿using System;
using KswApi.Tasks;
using KswApi.Tasks.Interfaces;

namespace KswApi.AuxiliaryService.Framework
{
	internal class QueueConsumerStartupTask : TaskBase
	{
		public override void Run(ApiClient client, IScheduler scheduler)
		{
			try
			{
				TaskScheduler taskScheduler = scheduler as TaskScheduler;

				if (taskScheduler != null)
					taskScheduler.StartQueueConsumer();

				return;
			}
			catch (Exception exception)
			{
				
			}

			scheduler.Schedule<QueueConsumerStartupTask>(TimeSpan.FromSeconds(Constant.RETRY_WAIT_IN_SECONDS));
		}
	}
}
