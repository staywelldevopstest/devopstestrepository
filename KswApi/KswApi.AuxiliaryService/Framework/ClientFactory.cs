﻿using System.Configuration;
using KswApi.Enums;

namespace KswApi.AuxiliaryService.Framework
{
	public static class ClientFactory
	{
		public static ApiClient GetClient()
		{
			string serviceUri = ConfigurationManager.AppSettings[AppSettingKey.SERVICE_URI];
			string clientSecret = ConfigurationManager.AppSettings[AppSettingKey.CLIENT_SECRET];

			return new ApiClient(serviceUri, Constant.AUXILIARY_APPLICATION_ID, clientSecret, TokenStoreType.SingleApplication);
		}
	}
}
