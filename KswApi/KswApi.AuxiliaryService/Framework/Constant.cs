﻿namespace KswApi.AuxiliaryService.Framework
{
	internal static class Constant
	{
		public const string INVALID_QUEUE_NAME = "The queue name is not configured.";
		public const int MAXIMUM_TASK_TIME_IN_MINUTES = 30;
		public const int DAYS_IN_A_WEEK = 7;
		public const string EVENT_LOG_SHUTDOWN_MESSAGE = "KSW API Auxiliary Service was shutdown";
		public const string EVENT_LOG_SUCCESS_MESSAGE = "KSW API Auxiliary Service started successfully";
		public const string EVENT_LOG_SOURCE = "KSW API Auxiliary Service";
		public const string EVENT_LOG_NAME = "KswApi";
		public const int HEARTBEAT_INTERVAL_IN_MILLISECONDS = 500;
		public const int END_TASK_TIMEOUT_IN_SECONDS = 120;
		public const int RETRY_WAIT_IN_SECONDS = 3;
		public const int MAXIMUM_THREAD_COUNT = 5000;
		public const string QUEUE_APP_SETTING_NAME = "KswApiQueue";
		public const string AUXILIARY_APPLICATION_ID = "29eeff76-85d0-4f70-94ed-a49072524692";
		public const int MINIMUM_THREAD_COUNT = 1;
	}
}
