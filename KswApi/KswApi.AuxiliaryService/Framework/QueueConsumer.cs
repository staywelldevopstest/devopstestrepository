﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.Messaging;
using System.Threading;
using KswApi.Common.Objects;
using KswApi.Interface.Enums;
using KswApi.Tasks.Interfaces;

namespace KswApi.AuxiliaryService.Framework
{
	internal class QueueConsumer
	{
		private readonly ManualResetEvent _stop = new ManualResetEvent(true); // initialize stopped
		private Thread _thread;
		private IScheduler _scheduler;

		public QueueConsumer(IScheduler scheduler)
		{
			_scheduler = scheduler;
		}

		public void Start()
		{
			if (!_stop.WaitOne(0))
				return; // already running

			_thread = new Thread(Run);

			_stop.Reset();

			_thread.Start();
		}

		public void Stop()
		{
			Thread thread = _thread;

			_thread = null;

			_stop.Set();

			if (thread != null)
				thread.Join();
		}

		private bool ProcessMessage(Message message)
		{
			try
			{
				message.Formatter = new BinaryMessageFormatter();

				QueuedTask task = message.Body as QueuedTask;
				if (task == null)
					return true;

				switch (task.Type)
				{
					case TaskType.Test:
						break;
					default:
						throw new ArgumentOutOfRangeException();
				}
			}
			catch (Exception exception)
			{
				// poison message
				Logger.LogLocal(exception.ToString(), EventLogEntryType.Error);
			}
			
			return true;
		}

		private void Run()
		{
			while (true)
			{
				if (_stop.WaitOne(0, false))
					return;

				using (MessageQueue queue = GetQueue())
				{
					if (queue == null) // GetQueue returns null only when we're stopping
						return;

					if (!ProcessQueue(queue))
						return;
				}
			}
		}

		private MessageQueue GetQueue()
		{
			string lastException = null;
			
			MessageQueue queue = null;

			while (true)
			{
				string queueName = ConfigurationManager.AppSettings[Constant.QUEUE_APP_SETTING_NAME];

				if (string.IsNullOrEmpty(queueName))
				{
					if (lastException != Constant.INVALID_QUEUE_NAME)
						Logger.LogLocal(Constant.INVALID_QUEUE_NAME, EventLogEntryType.Error);

					lastException = Constant.INVALID_QUEUE_NAME;
				}

				try
				{
					queue = new MessageQueue(queueName);
					
					// make sure we can access the queue
					queue.Peek(TimeSpan.Zero);

					if (!queue.Transactional)
						throw new InvalidOperationException("The queue must be transactional.");

					return queue;
				}
				catch (ArgumentException exception)
				{
					// don't bombard the event queue with repeat errors
					if (exception.Message != lastException)
						Logger.LogLocal(exception.ToString(), EventLogEntryType.Error);

					lastException = exception.Message;
				}
				catch (MessageQueueException exception)
				{
					if (exception.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout && queue != null)
					{
						// a timeout means we connected to the queue and timed out waiting, this should mean the queue is valid
						return queue;
					}

					// don't bombard the event queue with repeat errors
					if (exception.Message != lastException)
						Logger.LogLocal(exception.ToString(), EventLogEntryType.Error);

					lastException = exception.Message;
				}
				catch (Exception exception)
				{
					Logger.LogLocal(exception.ToString(), EventLogEntryType.Error);

					// unknown error occurred, quit attempting to connect
					return null;
				}

				// delay on error
				if (_stop.WaitOne(Constant.RETRY_WAIT_IN_SECONDS, false))
					return null;
			}

		}

		/// <summary>
		/// Returns false to signal stop,
		/// true means that processing should continue
		/// </summary>
		/// <param name="queue"></param>
		private bool ProcessQueue(MessageQueue queue)
		{
			while (true)
			{
				if (_stop.WaitOne(0, false))
					return false;

				Message message;

				MessageQueueTransaction transaction = new MessageQueueTransaction();

				try
				{
					IAsyncResult result = queue.BeginPeek();


					WaitHandle[] handles = new WaitHandle[] { _stop, result.AsyncWaitHandle };

					int waitItem = WaitHandle.WaitAny(handles);

					if (waitItem == 0)
						return false;

					queue.EndPeek(result);

					transaction.Begin();

					message = queue.Receive(TimeSpan.Zero, transaction);
				}
				catch (InvalidOperationException operationException)
				{
					Logger.LogLocal(operationException.ToString(), EventLogEntryType.Error);

					// problem receiving from the queue
					// we should return and attempt to re-create
					return true;
				}
				catch (MessageQueueException queueException)
				{
					// The IOTimeout may heppen when a message is put on the queue,
					// and another consumer grabs it before we receive it.
					// There's really no other way around this because
					// BeginReceive doesn't work with transactions
					if (queueException.MessageQueueErrorCode == MessageQueueErrorCode.IOTimeout)
						continue;

					Logger.LogLocal(queueException.ToString(), EventLogEntryType.Error);

					return true;
				}
				catch (Exception exception)
				{
					// all other exceptions: log and try to re-create the queue

					Logger.LogLocal(exception.ToString(), EventLogEntryType.Error);

					return true;
				}

				if (ProcessMessage(message))
				{
					try
					{
						transaction.Commit();
					}
					catch (Exception exception)
					{
						Logger.LogLocal(exception.ToString(), EventLogEntryType.Error);
					}
					
				}
				else
				{
					try
					{
						transaction.Abort();
					}
					catch (Exception exception)
					{
						Logger.LogLocal(exception.ToString(), EventLogEntryType.Error);
					}
				}
			}
		}
	}
}
