﻿namespace KswApi.AuxiliaryService.Framework
{
	public static class AppSettingKey
	{
		public const string CLIENT_SECRET = "KswApiAuxiliarySecret";
		public const string SERVICE_URI = "KswApiService";
	}
}
