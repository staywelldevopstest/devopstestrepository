﻿using KswApi.Tasks.Interfaces;

namespace KswApi.AuxiliaryService.Framework
{
	internal interface IExecutionTracker : IScheduler
	{
		void Finished(TaskExecutor executor);
	}
}
