﻿using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Tasks;
using KswApi.Tasks.Interfaces;
using System;
using System.Diagnostics;

namespace KswApi.AuxiliaryService.Framework
{
    class InitializationTask : TaskBase
    {
        public override void Run(ApiClient client, IScheduler scheduler)
        {
            try
            {
                ConfigureSettings(client, scheduler);

                // If initalization succeeded, start the queue reader
                scheduler.Schedule<QueueConsumerStartupTask>(TimeSpan.Zero);

                Logger.LogLocal(Constant.EVENT_LOG_SUCCESS_MESSAGE, EventLogEntryType.Information);

                return;
            }
            catch (Exception ex)
            {
                Logger.LogLocal(ex.Message, EventLogEntryType.Error);
            }

            // We failed initialization, so reschedule

            scheduler.Schedule<InitializationTask>(TimeSpan.FromSeconds(Constant.RETRY_WAIT_IN_SECONDS));
        }

        #region Private Methods

        private void ConfigureSettings(ApiClient client, IScheduler scheduler)
        {
            TaskServiceSettings settings = client.Tasks.GetServiceSettings();

            TaskScheduler taskScheduler = scheduler as TaskScheduler;

            if (taskScheduler != null)
                taskScheduler.SetThreadCount(settings.NumberOfThreads);

            foreach (Task task in settings.Tasks)
            {
                switch (task.Type)
                {
					case TaskType.Test:
		                break;
                    case TaskType.SmsSessionCleanup:
                        scheduler.Schedule<TrimSmsSessions>(task);
                        break;
                    case TaskType.AuthorizationGrantCleanup:
                        scheduler.Schedule<TrimAuthorizationGrants>(task);
                        break;
                    case TaskType.AccessTokenCleanup:
                        scheduler.Schedule<TrimAccessTokens>(task);
                        break;
                    default:
                        scheduler.Schedule<CommonTask>(task);
		                break;
                }
            }
        }

        #endregion
    }
}
