﻿using System.Diagnostics;

namespace KswApi.AuxiliaryService.Framework
{
	static class Logger
	{
		public static void LogLocal(string message, EventLogEntryType type)
		{
			try
			{
				EventLog eventLog = new EventLog
					                    {
						                    Source = Constant.EVENT_LOG_SOURCE,
						                    Log = Constant.EVENT_LOG_NAME
					                    };


				eventLog.WriteEntry(message, type);
			}
			catch // this is acceptable here, logging an exception shouldn't cause another exception
			{
				
			}
		}
	}
}
