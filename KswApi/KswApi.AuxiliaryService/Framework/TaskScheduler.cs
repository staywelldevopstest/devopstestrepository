﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using System.Threading;
using KswApi.Collections;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Tasks.Interfaces;

namespace KswApi.AuxiliaryService.Framework
{
	internal class TaskScheduler : IExecutionTracker
	{
		private Thread _thread;
		private readonly ManualResetEvent _stop = new ManualResetEvent(true); // initialize stopped
		private readonly PriorityQueue<ScheduledTask> _taskQueue = new PriorityQueue<ScheduledTask>();
		private readonly LinkedList<TaskExecutor> _tasksInProgress = new LinkedList<TaskExecutor>();
		private readonly Queue<TaskExecutor> _idleExecutors = new Queue<TaskExecutor>();
		private readonly Semaphore _executorSemaphore = new Semaphore(0, Constant.MAXIMUM_THREAD_COUNT + 1);
		private readonly QueueConsumer _queueConsumer;

		public TaskScheduler()
		{
			_queueConsumer = new QueueConsumer(this);
		}

		public int ThreadCount { get; private set; }

		public void Start()
		{
			if (!_stop.WaitOne(0))
				return; // already running

			SetThreadCount(Constant.MINIMUM_THREAD_COUNT);

			Schedule<InitializationTask>(DateTime.UtcNow);

			_thread = new Thread(Run);

			_stop.Reset();

			_thread.Start();
		}

		public void Stop()
		{
			Thread thread = _thread;

			_thread = null;

			_stop.Set();

			_queueConsumer.Stop();

			if (thread != null)
				thread.Join(TimeSpan.FromSeconds(Constant.END_TASK_TIMEOUT_IN_SECONDS));
		}

		public void Schedule<T>(DateTime time) where T : ITask
		{
			ConstructorInfo constructor = typeof (T).GetConstructor(Type.EmptyTypes);

			if (constructor == null)
				return;

			lock (_taskQueue)
			{
				_taskQueue.Push(new ScheduledTask
										{
											Constructor = constructor,
											Type = typeof(T),
											Time = time
										});
			}
		}

		public void Schedule<T>(TimeSpan interval) where T : ITask
		{
			ConstructorInfo constructor = typeof(T).GetConstructor(Type.EmptyTypes);

			if (constructor == null)
				return;

			lock (_taskQueue)
			{
				_taskQueue.Push(new ScheduledTask
				{
					Constructor = constructor,
					Type = typeof(T),
					Time = DateTime.UtcNow + interval
				});
			}
		}

		public void Schedule<T>(Task task) where T : ITask
		{
			Schedule(typeof (T), task);
		}

		public void SetThreadCount(int count)
		{
			if (count < Constant.MINIMUM_THREAD_COUNT)
				count = Constant.MINIMUM_THREAD_COUNT;

			if (count > Constant.MAXIMUM_THREAD_COUNT)
				count = Constant.MAXIMUM_THREAD_COUNT;

			int threadCount = 0;

			lock (_idleExecutors)
			{
				threadCount = ThreadCount;

				if (count == threadCount)
					return;

				ThreadCount = count;

				if (count > threadCount)
				{
					for (int i = threadCount; i < count; i++)
					{
						_idleExecutors.Enqueue(new TaskExecutor(this));
						_executorSemaphore.Release();
					}

					return;
				}
			}

			// reduce threads outside of locking block to prevent dead-locks
			// if we're stopping, quit trying to reduce

			WaitHandle[] handles = new WaitHandle[] {_stop, _executorSemaphore};

			for (int i = threadCount; i > count; i--)
			{
				int waitItem = WaitHandle.WaitAny(handles);

				if (waitItem == 0)
					break;

				TaskExecutor executor;

				lock (_idleExecutors)
				{
					executor = _idleExecutors.Dequeue();
				}

				executor.Dispose();
			}
		}

		#region Private Methods

		private void Run()
		{
			WaitHandle[] handles = new WaitHandle[] { _stop, _executorSemaphore };

			while (true)
			{
				//Console.WriteLine("Task Execution: {0}", DateTime.Now);
				if (_stop.WaitOne(0, false))
					break;

				int waitItem = WaitHandle.WaitAny(handles);

				if (waitItem == 0) // stop
					break;

				ScheduledTask task = GetScheduledTask();

				if (task == null)
				{
					_executorSemaphore.Release();

					// Delay to avoid thrashing
					if (_stop.WaitOne(TimeSpan.FromMilliseconds(Constant.HEARTBEAT_INTERVAL_IN_MILLISECONDS)))
						break;
				}
				else
				{
					TaskExecutor executor;

					// executor exists (because we're using the semaphore to track)
					lock (_idleExecutors)
					{
						executor = _idleExecutors.Dequeue();
					}

					lock (_tasksInProgress)
					{
						_tasksInProgress.AddLast(executor);
					}

					executor.Execute(task);
				}
			}

			Cleanup();

			Logger.LogLocal(Constant.EVENT_LOG_SHUTDOWN_MESSAGE, EventLogEntryType.Information);
		}

		private void Cleanup()
		{
			// If we're here, the stop event is signalled so no new tasks can be scheduled
			// signal all running tasks that they should stop
			
			lock (_tasksInProgress)
			{
				foreach (TaskExecutor executor in _tasksInProgress)
				{
					ITask task = executor.Task;
					if (task != null)
						task.Stop();
				}
			}

			// wait for running tasks to stop
			while (true)
			{
				TaskExecutor executor;

				lock (_tasksInProgress)
				{
					if (_tasksInProgress.Count == 0)
						break;

					executor = _tasksInProgress.First.Value;
				}

				executor.Stop();
			}

			// all executing tasks are stopped, all executors are idle
			// dispose idle executors
			while (_executorSemaphore.WaitOne(0, false))
			{
				TaskExecutor executor;

				lock (_idleExecutors)
				{
					executor = _idleExecutors.Dequeue();
				}

				executor.Dispose();
			}

			lock (_taskQueue)
			{
				_taskQueue.Clear();
			}
		}

		private ScheduledTask GetScheduledTask()
		{
			ScheduledTask scheduledTask;

			lock (_taskQueue)
			{
				if (!_taskQueue.TryPeek(out scheduledTask))
					return null;

				if (scheduledTask.Time > DateTime.UtcNow)
					return null;

				_taskQueue.Pop();
			}

			return scheduledTask;
		}

		private void Schedule(Type type, Task task)
		{
			ConstructorInfo constructor = type.GetConstructor(Type.EmptyTypes);

			if (constructor == null)
				return;

			DateTime time;

			switch (task.Recurrence)
			{
				case RecurringPatternType.Daily:
					if (task.DailyRecurrence == null)
						return;

					if (!TryGetNextOccurrence(task.DailyRecurrence, out time))
						return;

					break;

				case RecurringPatternType.Hourly:
					if (task.HourlyRecurrence == null)
						return;

					time = DateTime.UtcNow + TimeSpan.FromMinutes(task.HourlyRecurrence.IntervalInMinutes);

					break;
				default:
					return;
			}

			lock (_taskQueue)
			{
				_taskQueue.Push(new ScheduledTask
				{
					Task = task,
					Constructor = constructor,
					Type = type,
					Time = time
				});
			}
		}

		private bool TryGetNextOccurrence(DailyRecurringSettings dailyRecurringSettings, out DateTime time)
		{
			time = default(DateTime);

			if (dailyRecurringSettings == null || dailyRecurringSettings.DaysToRun == null)
				return false;

			DateTime now = DateTime.UtcNow;
			
			DateTime dailyTime = dailyRecurringSettings.StartTime.ToUniversalTime();
			
			DateTime tentativeTime = new DateTime(now.Year, now.Month, now.Day, dailyTime.Hour, dailyTime.Minute, dailyTime.Second, dailyTime.Millisecond);

			for (int i = 0; i < Constant.DAYS_IN_A_WEEK + 1; i++)
			{
				if (dailyRecurringSettings.DaysToRun.Contains(tentativeTime.DayOfWeek) && tentativeTime >= now)
				{
					time = tentativeTime;

					return true;
				}

				tentativeTime = tentativeTime.AddDays(1);
			}

			return false;
		}

		#endregion

		#region Interface IExecutionTracker
		
		public void Finished(TaskExecutor executor)
		{
			ScheduledTask scheduledTask = executor.ScheduledTask;

			if (scheduledTask != null)
			{
				Task task = scheduledTask.Task;

				// reschedule a recurring task
				if (task != null)
					Schedule(scheduledTask.Type, task);
			}

			lock (_idleExecutors)
			{
				_idleExecutors.Enqueue(executor);
			}

			lock (_tasksInProgress)
			{
				_tasksInProgress.Remove(executor);
			}

			_executorSemaphore.Release();
		}

		public void StartQueueConsumer()
		{
			_queueConsumer.Start();
		}

		#endregion
	}
}
