﻿using System;
using System.Configuration;

namespace KswApi.AuxiliaryService.Framework
{
	internal class CommandLine
	{
		private readonly TaskScheduler _taskScheduler = new TaskScheduler();

		public void Run()
		{
			StartScheduler();

			ShowCommands();
			while (true)
			{
				Console.Write(">");
				string command = Console.ReadLine();
				switch (command)
				{
					case "start":
						StartScheduler();
						break;
					case "stop":
						StopScheduler();
						break;
					case "quit":
					case "exit":
						return;
					case "threads":
						Console.WriteLine("Thread count: {0}", _taskScheduler.ThreadCount);
						break;
					case "help":
						ShowCommands();
						break;
					default:
						Console.WriteLine("Unknown command");
						break;
				}
			}
		}

		#region Private Methods

		private void StartScheduler()
		{
			_taskScheduler.Start();
			Console.WriteLine("Started");
		}

		private void StopScheduler()
		{
			_taskScheduler.Stop();
			Console.WriteLine("Stopped");
		}

		private void ShowCommands()
		{
			Console.WriteLine();
			Console.WriteLine("Commands:");
			Console.WriteLine("  start");
			Console.WriteLine("  stop");
			Console.WriteLine("  threads");
			Console.WriteLine("  quit");
			Console.WriteLine("  help");
			Console.WriteLine();
		}

		#endregion
	}
}
