﻿using System.Configuration;
using System.Messaging;
using KswApi.Common.Objects;
using KswApi.Interface.Enums;
using KswApi.Logic.Framework.Constants;
using KswApi.Logic.Framework.Interfaces;

namespace Kswapi.ExternalServices
{
	public class Msmq : ITaskQueue
	{
		public bool Test()
		{
			
			QueuedTask task = new QueuedTask
				                  {
									  Type = TaskType.Test
				                  };

			return Send(task);
		}

		#region Private Methods

		private bool Send(QueuedTask task)
		{
			Message message = new Message
				                  {
									  Body = task,
									  Formatter = new BinaryMessageFormatter()
				                  };

			MessageQueue queue = GetQueue();

			MessageQueueTransaction transaction = new MessageQueueTransaction();

			transaction.Begin();

			queue.Send(message, transaction);

			transaction.Commit();

			return true;
		}

		private MessageQueue GetQueue()
		{
			string queueName = ConfigurationManager.AppSettings[AppSettingKey.QUEUE_NAME];

			if (string.IsNullOrEmpty(queueName))
				throw new ConfigurationErrorsException(string.Format("The queue name is not set for {0}.", AppSettingKey.QUEUE_NAME));

			return new MessageQueue(queueName);
		}

		#endregion
	}
}
