﻿using System.Xml;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.SmsGateway;
using KswApi.Logic.Framework.Interfaces;
using KswApi.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.IO;
using System.Net;
using Formatting = Newtonsoft.Json.Formatting;

namespace Kswapi.ExternalServices
{
	public class KswApiSmsClient : ISmsMessageForwarder
	{
		public void SendMessage(MessageFormat format, string address, OutgoingMessage message)
		{
			switch (format)
			{
				case MessageFormat.Json:
					SendJson(address, message);
					break;
				case MessageFormat.Xml:
					SendXml(address, message);
					break;
				default:
					throw new ArgumentOutOfRangeException("format");
			}
		}

		private void SendJson(string address, OutgoingMessage message)
		{
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);

			request.Method = "POST";

			request.ContentType = "application/json";

			using (Stream stream = request.GetRequestStream())
			{

				JsonSerializer serializer = new JsonSerializer();

				serializer.Formatting = Formatting.Indented;
				serializer.NullValueHandling = NullValueHandling.Ignore;
				serializer.Converters.Add(new StringEnumConverter());

				TextWriter writer = new StreamWriter(stream);

				serializer.Serialize(writer, message);

				writer.Flush();
			}

			using (request.GetResponse())
			{
				
			}
		}

		private void SendXml(string address, OutgoingMessage message)
		{
			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(address);

			request.Method = "POST";

			request.ContentType = "application/xml";

			using (Stream stream = request.GetRequestStream())
			{
				CustomXmlSerializer serializer = new CustomXmlSerializer(typeof (OutgoingMessage));

				XmlWriterSettings xmlWriterSettings = new XmlWriterSettings {OmitXmlDeclaration = true};

				XmlWriter writer = XmlWriter.Create(stream, xmlWriterSettings);

				serializer.Serialize(writer, message);

				writer.Flush();
			}

			using (request.GetResponse())
			{
				
			}
		}
	}
}
