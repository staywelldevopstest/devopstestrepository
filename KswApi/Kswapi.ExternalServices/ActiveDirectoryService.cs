﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.Protocols;
using System.Linq;
using System.Net;
using KswApi.Common.Configuration;
using KswApi.Common.Objects;
using KswApi.Interface.Enums;
using KswApi.Logic.Framework.Interfaces;

namespace Kswapi.ExternalServices
{
	public class ActiveDirectoryService : IUserAuthenticator
	{
		#region Public Methods

		//Gets the user's AD details
		public UserAuthenticationDetails Authenticate(string userName, string password)
		{
			LdapDirectoryIdentifier identifier = new LdapDirectoryIdentifier(Settings.Current.ActiveDirectoryDomain);
			SearchResponse response;

			using (LdapConnection connection = new LdapConnection(identifier))
			{
				NetworkCredential credential = new NetworkCredential(userName, password);

				try
				{
					connection.Bind(credential);
				}
				catch (LdapException exception)
				{
					if (exception.ErrorCode == 49)
						return null;

					throw;
				}

				DirectoryRequest request = new SearchRequest("DC=medimedia,DC=com",
															 string.Format("(&(objectClass=user)(samaccountname={0}))",
																		   userName), SearchScope.Subtree, "objectGUID", "givenName", "sn", "mail", "memberOf");

				response = (SearchResponse)connection.SendRequest(request);
			}

			if (response == null)
				return null;

			if (response.Entries.Count != 1)
				return null;

			SearchResultEntry entry = response.Entries[0];

			string[] groups = (string[])entry.Attributes["memberOf"].GetValues(typeof(string));

			Guid? id = GetGuid(entry, "objectGUID");

			if (id == null)
				return null;

			List<string> roles = GetGroups(groups);

			return new UserAuthenticationDetails
				   {
					   Id = id.Value,
                       Username = userName,
					   EmailAddress = GetString(entry, "mail"),
					   FirstName = GetString(entry, "givenName"),
					   LastName = GetString(entry, "sn"),
					   MiscInfo = entry.DistinguishedName,
					   Roles = roles,
					   Type = UserType.DomainUser
				   };
		}

		private List<string> GetGroups(IEnumerable<string> groups)
		{
			List<string> result = new List<string>();
			foreach (string group in groups)
			{
				string[] split = group.Split(',', ';');

				if (split.Length < 2)
					continue;

				string[] kswapi = split[1].Split('=');
				if (kswapi.Length != 2 || kswapi[1].Trim() != "KSWAPI")
					continue;

				string[] name = split[0].Split('=');
				if (name.Length != 2)
					continue;

				result.Add(name[1].Trim());
			}

			return result;
		}

		private string GetString(SearchResultEntry entry, string name)
		{
			DirectoryAttribute attribute = entry.Attributes[name];
			if (attribute == null)
				return null;
			return (string)attribute.GetValues(typeof(string)).FirstOrDefault();
		}

		private Guid? GetGuid(SearchResultEntry entry, string name)
		{
			DirectoryAttribute attribute = entry.Attributes[name];

			if (attribute == null)
				return null;

			byte[] bytes = (byte[])attribute.GetValues(typeof(byte[])).FirstOrDefault();

			if (bytes == null || bytes.Length != 16)
				return null;

			return new Guid(bytes);
		}

		#endregion
	}
}
