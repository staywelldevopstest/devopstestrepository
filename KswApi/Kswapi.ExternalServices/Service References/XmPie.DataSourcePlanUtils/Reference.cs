﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.34003
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Kswapi.ExternalServices.XmPie.DataSourcePlanUtils {
    using System.Data;
    
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    [System.ServiceModel.ServiceContractAttribute(Namespace="XMPieWSAPI", ConfigurationName="XmPie.DataSourcePlanUtils.DataSourcePlanUtils_SSPSoap")]
    public interface DataSourcePlanUtils_SSPSoap {
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/TestDataSourceConnectivity", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        bool TestDataSourceConnectivity(string inUsername, string inPassword, string inDataSourceID);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/TestDataSourceConnectivity", ReplyAction="*")]
        System.Threading.Tasks.Task<bool> TestDataSourceConnectivityAsync(string inUsername, string inPassword, string inDataSourceID);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/TestDataSourceConnectivityByInfo", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        bool TestDataSourceConnectivityByInfo(string inUsername, string inPassword, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/TestDataSourceConnectivityByInfo", ReplyAction="*")]
        System.Threading.Tasks.Task<bool> TestDataSourceConnectivityByInfoAsync(string inUsername, string inPassword, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/GetRecipientsCount", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        int GetRecipientsCount(string inUsername, string inPassword, string inPlanID, string inDataSourceID, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.RecipientsInfo inRIInfo);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/GetRecipientsCount", ReplyAction="*")]
        System.Threading.Tasks.Task<int> GetRecipientsCountAsync(string inUsername, string inPassword, string inPlanID, string inDataSourceID, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.RecipientsInfo inRIInfo);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/GetRecipientsCountByInfo", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        int GetRecipientsCountByInfo(string inUsername, string inPassword, string inPlanID, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.RecipientsInfo inRIInfo);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/GetRecipientsCountByInfo", ReplyAction="*")]
        System.Threading.Tasks.Task<int> GetRecipientsCountByInfoAsync(string inUsername, string inPassword, string inPlanID, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.RecipientsInfo inRIInfo);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/GetCompatibleTables", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        string[] GetCompatibleTables(string inUsername, string inPassword, string inPlanID, string inDataSourceID, bool inTrivialPlan);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/GetCompatibleTables", ReplyAction="*")]
        System.Threading.Tasks.Task<string[]> GetCompatibleTablesAsync(string inUsername, string inPassword, string inPlanID, string inDataSourceID, bool inTrivialPlan);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/GetCompatibleTablesByInfo", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        string[] GetCompatibleTablesByInfo(string inUsername, string inPassword, string inPlanID, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo, bool inTrivialPlan);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/GetCompatibleTablesByInfo", ReplyAction="*")]
        System.Threading.Tasks.Task<string[]> GetCompatibleTablesByInfoAsync(string inUsername, string inPassword, string inPlanID, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo, bool inTrivialPlan);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/GetFirstCompatibleTable", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        string GetFirstCompatibleTable(string inUsername, string inPassword, string inPlanID, string inDataSourceID, bool inTrivialPlan);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/GetFirstCompatibleTable", ReplyAction="*")]
        System.Threading.Tasks.Task<string> GetFirstCompatibleTableAsync(string inUsername, string inPassword, string inPlanID, string inDataSourceID, bool inTrivialPlan);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/GetFirstCompatibleTableByInfo", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        string GetFirstCompatibleTableByInfo(string inUsername, string inPassword, string inPlanID, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo, bool inTrivialPlan);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/GetFirstCompatibleTableByInfo", ReplyAction="*")]
        System.Threading.Tasks.Task<string> GetFirstCompatibleTableByInfoAsync(string inUsername, string inPassword, string inPlanID, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo, bool inTrivialPlan);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/IsDataSourceCompatibleWithSchema", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        bool IsDataSourceCompatibleWithSchema(string inUsername, string inPassword, string inPlanID, string inSchemaName, string inDataSourceID);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/IsDataSourceCompatibleWithSchema", ReplyAction="*")]
        System.Threading.Tasks.Task<bool> IsDataSourceCompatibleWithSchemaAsync(string inUsername, string inPassword, string inPlanID, string inSchemaName, string inDataSourceID);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/IsDataSourceCompatibleWithSchemaByInfo", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        bool IsDataSourceCompatibleWithSchemaByInfo(string inUsername, string inPassword, string inPlanID, string inSchemaname, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/IsDataSourceCompatibleWithSchemaByInfo", ReplyAction="*")]
        System.Threading.Tasks.Task<bool> IsDataSourceCompatibleWithSchemaByInfoAsync(string inUsername, string inPassword, string inPlanID, string inSchemaname, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/GetDataSourceTypes", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataSet GetDataSourceTypes(string inUsername, string inPassword);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/GetDataSourceTypes", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataSet> GetDataSourceTypesAsync(string inUsername, string inPassword);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/GetDataSourceType", ReplyAction="*")]
        [System.ServiceModel.XmlSerializerFormatAttribute(SupportFaults=true)]
        System.Data.DataSet GetDataSourceType(string inUsername, string inPassword, string inDataSourceType);
        
        [System.ServiceModel.OperationContractAttribute(Action="XMPieWSAPI/GetDataSourceType", ReplyAction="*")]
        System.Threading.Tasks.Task<System.Data.DataSet> GetDataSourceTypeAsync(string inUsername, string inPassword, string inDataSourceType);
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="XMPieWSAPI")]
    public partial class Connection : object, System.ComponentModel.INotifyPropertyChanged {
        
        private string m_TypeField;
        
        private string m_ConnectionStringField;
        
        private string m_AdditionalInfoField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public string m_Type {
            get {
                return this.m_TypeField;
            }
            set {
                this.m_TypeField = value;
                this.RaisePropertyChanged("m_Type");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public string m_ConnectionString {
            get {
                return this.m_ConnectionStringField;
            }
            set {
                this.m_ConnectionStringField = value;
                this.RaisePropertyChanged("m_ConnectionString");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public string m_AdditionalInfo {
            get {
                return this.m_AdditionalInfoField;
            }
            set {
                this.m_AdditionalInfoField = value;
                this.RaisePropertyChanged("m_AdditionalInfo");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.Xml", "4.0.30319.33440")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace="XMPieWSAPI")]
    public partial class RecipientsInfo : object, System.ComponentModel.INotifyPropertyChanged {
        
        private int m_FromField;
        
        private int m_ToField;
        
        private int m_FilterTypeField;
        
        private string m_FilterField;
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=0)]
        public int m_From {
            get {
                return this.m_FromField;
            }
            set {
                this.m_FromField = value;
                this.RaisePropertyChanged("m_From");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=1)]
        public int m_To {
            get {
                return this.m_ToField;
            }
            set {
                this.m_ToField = value;
                this.RaisePropertyChanged("m_To");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=2)]
        public int m_FilterType {
            get {
                return this.m_FilterTypeField;
            }
            set {
                this.m_FilterTypeField = value;
                this.RaisePropertyChanged("m_FilterType");
            }
        }
        
        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Order=3)]
        public string m_Filter {
            get {
                return this.m_FilterField;
            }
            set {
                this.m_FilterField = value;
                this.RaisePropertyChanged("m_Filter");
            }
        }
        
        public event System.ComponentModel.PropertyChangedEventHandler PropertyChanged;
        
        protected void RaisePropertyChanged(string propertyName) {
            System.ComponentModel.PropertyChangedEventHandler propertyChanged = this.PropertyChanged;
            if ((propertyChanged != null)) {
                propertyChanged(this, new System.ComponentModel.PropertyChangedEventArgs(propertyName));
            }
        }
    }
    
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public interface DataSourcePlanUtils_SSPSoapChannel : Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.DataSourcePlanUtils_SSPSoap, System.ServiceModel.IClientChannel {
    }
    
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("System.ServiceModel", "4.0.0.0")]
    public partial class DataSourcePlanUtils_SSPSoapClient : System.ServiceModel.ClientBase<Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.DataSourcePlanUtils_SSPSoap>, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.DataSourcePlanUtils_SSPSoap {
        
        public DataSourcePlanUtils_SSPSoapClient() {
        }
        
        public DataSourcePlanUtils_SSPSoapClient(string endpointConfigurationName) : 
                base(endpointConfigurationName) {
        }
        
        public DataSourcePlanUtils_SSPSoapClient(string endpointConfigurationName, string remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DataSourcePlanUtils_SSPSoapClient(string endpointConfigurationName, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(endpointConfigurationName, remoteAddress) {
        }
        
        public DataSourcePlanUtils_SSPSoapClient(System.ServiceModel.Channels.Binding binding, System.ServiceModel.EndpointAddress remoteAddress) : 
                base(binding, remoteAddress) {
        }
        
        public bool TestDataSourceConnectivity(string inUsername, string inPassword, string inDataSourceID) {
            return base.Channel.TestDataSourceConnectivity(inUsername, inPassword, inDataSourceID);
        }
        
        public System.Threading.Tasks.Task<bool> TestDataSourceConnectivityAsync(string inUsername, string inPassword, string inDataSourceID) {
            return base.Channel.TestDataSourceConnectivityAsync(inUsername, inPassword, inDataSourceID);
        }
        
        public bool TestDataSourceConnectivityByInfo(string inUsername, string inPassword, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo) {
            return base.Channel.TestDataSourceConnectivityByInfo(inUsername, inPassword, inConnectionInfo);
        }
        
        public System.Threading.Tasks.Task<bool> TestDataSourceConnectivityByInfoAsync(string inUsername, string inPassword, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo) {
            return base.Channel.TestDataSourceConnectivityByInfoAsync(inUsername, inPassword, inConnectionInfo);
        }
        
        public int GetRecipientsCount(string inUsername, string inPassword, string inPlanID, string inDataSourceID, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.RecipientsInfo inRIInfo) {
            return base.Channel.GetRecipientsCount(inUsername, inPassword, inPlanID, inDataSourceID, inRIInfo);
        }
        
        public System.Threading.Tasks.Task<int> GetRecipientsCountAsync(string inUsername, string inPassword, string inPlanID, string inDataSourceID, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.RecipientsInfo inRIInfo) {
            return base.Channel.GetRecipientsCountAsync(inUsername, inPassword, inPlanID, inDataSourceID, inRIInfo);
        }
        
        public int GetRecipientsCountByInfo(string inUsername, string inPassword, string inPlanID, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.RecipientsInfo inRIInfo) {
            return base.Channel.GetRecipientsCountByInfo(inUsername, inPassword, inPlanID, inConnectionInfo, inRIInfo);
        }
        
        public System.Threading.Tasks.Task<int> GetRecipientsCountByInfoAsync(string inUsername, string inPassword, string inPlanID, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.RecipientsInfo inRIInfo) {
            return base.Channel.GetRecipientsCountByInfoAsync(inUsername, inPassword, inPlanID, inConnectionInfo, inRIInfo);
        }
        
        public string[] GetCompatibleTables(string inUsername, string inPassword, string inPlanID, string inDataSourceID, bool inTrivialPlan) {
            return base.Channel.GetCompatibleTables(inUsername, inPassword, inPlanID, inDataSourceID, inTrivialPlan);
        }
        
        public System.Threading.Tasks.Task<string[]> GetCompatibleTablesAsync(string inUsername, string inPassword, string inPlanID, string inDataSourceID, bool inTrivialPlan) {
            return base.Channel.GetCompatibleTablesAsync(inUsername, inPassword, inPlanID, inDataSourceID, inTrivialPlan);
        }
        
        public string[] GetCompatibleTablesByInfo(string inUsername, string inPassword, string inPlanID, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo, bool inTrivialPlan) {
            return base.Channel.GetCompatibleTablesByInfo(inUsername, inPassword, inPlanID, inConnectionInfo, inTrivialPlan);
        }
        
        public System.Threading.Tasks.Task<string[]> GetCompatibleTablesByInfoAsync(string inUsername, string inPassword, string inPlanID, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo, bool inTrivialPlan) {
            return base.Channel.GetCompatibleTablesByInfoAsync(inUsername, inPassword, inPlanID, inConnectionInfo, inTrivialPlan);
        }
        
        public string GetFirstCompatibleTable(string inUsername, string inPassword, string inPlanID, string inDataSourceID, bool inTrivialPlan) {
            return base.Channel.GetFirstCompatibleTable(inUsername, inPassword, inPlanID, inDataSourceID, inTrivialPlan);
        }
        
        public System.Threading.Tasks.Task<string> GetFirstCompatibleTableAsync(string inUsername, string inPassword, string inPlanID, string inDataSourceID, bool inTrivialPlan) {
            return base.Channel.GetFirstCompatibleTableAsync(inUsername, inPassword, inPlanID, inDataSourceID, inTrivialPlan);
        }
        
        public string GetFirstCompatibleTableByInfo(string inUsername, string inPassword, string inPlanID, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo, bool inTrivialPlan) {
            return base.Channel.GetFirstCompatibleTableByInfo(inUsername, inPassword, inPlanID, inConnectionInfo, inTrivialPlan);
        }
        
        public System.Threading.Tasks.Task<string> GetFirstCompatibleTableByInfoAsync(string inUsername, string inPassword, string inPlanID, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo, bool inTrivialPlan) {
            return base.Channel.GetFirstCompatibleTableByInfoAsync(inUsername, inPassword, inPlanID, inConnectionInfo, inTrivialPlan);
        }
        
        public bool IsDataSourceCompatibleWithSchema(string inUsername, string inPassword, string inPlanID, string inSchemaName, string inDataSourceID) {
            return base.Channel.IsDataSourceCompatibleWithSchema(inUsername, inPassword, inPlanID, inSchemaName, inDataSourceID);
        }
        
        public System.Threading.Tasks.Task<bool> IsDataSourceCompatibleWithSchemaAsync(string inUsername, string inPassword, string inPlanID, string inSchemaName, string inDataSourceID) {
            return base.Channel.IsDataSourceCompatibleWithSchemaAsync(inUsername, inPassword, inPlanID, inSchemaName, inDataSourceID);
        }
        
        public bool IsDataSourceCompatibleWithSchemaByInfo(string inUsername, string inPassword, string inPlanID, string inSchemaname, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo) {
            return base.Channel.IsDataSourceCompatibleWithSchemaByInfo(inUsername, inPassword, inPlanID, inSchemaname, inConnectionInfo);
        }
        
        public System.Threading.Tasks.Task<bool> IsDataSourceCompatibleWithSchemaByInfoAsync(string inUsername, string inPassword, string inPlanID, string inSchemaname, Kswapi.ExternalServices.XmPie.DataSourcePlanUtils.Connection inConnectionInfo) {
            return base.Channel.IsDataSourceCompatibleWithSchemaByInfoAsync(inUsername, inPassword, inPlanID, inSchemaname, inConnectionInfo);
        }
        
        public System.Data.DataSet GetDataSourceTypes(string inUsername, string inPassword) {
            return base.Channel.GetDataSourceTypes(inUsername, inPassword);
        }
        
        public System.Threading.Tasks.Task<System.Data.DataSet> GetDataSourceTypesAsync(string inUsername, string inPassword) {
            return base.Channel.GetDataSourceTypesAsync(inUsername, inPassword);
        }
        
        public System.Data.DataSet GetDataSourceType(string inUsername, string inPassword, string inDataSourceType) {
            return base.Channel.GetDataSourceType(inUsername, inPassword, inDataSourceType);
        }
        
        public System.Threading.Tasks.Task<System.Data.DataSet> GetDataSourceTypeAsync(string inUsername, string inPassword, string inDataSourceType) {
            return base.Channel.GetDataSourceTypeAsync(inUsername, inPassword, inDataSourceType);
        }
    }
}
