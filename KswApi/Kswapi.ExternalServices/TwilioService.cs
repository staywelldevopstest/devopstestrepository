﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using KswApi.Common.Configuration;
using KswApi.Common.Enums;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.SmsGateway;
using KswApi.Logic.Framework.Interfaces;
using System.Net;
using RestSharp;
using Twilio;
using ServiceException = KswApi.Interface.Exceptions.ServiceException;

namespace Kswapi.ExternalServices
{
	public class TwilioService : ISmsService
	{
		private const int MAXIMUM_TWILIO_PAGE_SIZE = 100;
		private const string US_COUNTRY_CODE = "US";
		private const string TWILIO_TEST_NUMBER = "5005550006";
		private const string TWILIO_COMMUNICATION_ERROR_MESSAGE = "Unable to communicate with SMS provider.";
		private const string TWILIO_DELETE_ERROR_MESSAGE = "Unable to delete the specified number from the SMS Provider.";

		#region Public Methods

		public void SendMessage(string to, string from, string body)
		{
			SendMessage(to, from, body, string.Empty);
		}

		public void SendMessage(string to, string from, string body, string statusCallBackUrl)
		{
			// twilio will not send an empty message

			if (string.IsNullOrEmpty(body))
				throw new ServiceException(HttpStatusCode.BadRequest, "Cannot send an empty message.");

			TwilioRestClient twilioClient = new TwilioRestClient(Settings.Current.TwilioAccountSid, Settings.Current.TwilioAuthToken);

			SMSMessage result = twilioClient.SendSmsMessage(from, to, body, statusCallBackUrl);

			if (result == null)
				throw new ServiceException(HttpStatusCode.InternalServerError, TWILIO_COMMUNICATION_ERROR_MESSAGE);
		}

		public IncomingMessage GetMessage(string messageSid)
		{
			TwilioRestClient twilioClient = new TwilioRestClient(Settings.Current.TwilioAccountSid, Settings.Current.TwilioAuthToken);

			SMSMessage twilioMessage = twilioClient.GetSmsMessage(messageSid);

			if (twilioMessage == null)
				throw new ServiceException(HttpStatusCode.InternalServerError, TWILIO_COMMUNICATION_ERROR_MESSAGE);

			if (twilioMessage.RestException != null)
				throw new ServiceException(HttpStatusCode.BadRequest, twilioMessage.RestException.Message);

			IncomingMessage message = new IncomingMessage { Body = twilioMessage.Body, From = twilioMessage.From, To = twilioMessage.To };

			return message;
		}

		public AvailableLongCodeList GetAvailableNumbers(int offset, int count, string filter, SmsLongCodeFilterType filterType)
		{
			TwilioRestClient twilioClient = new TwilioRestClient(Settings.Current.TwilioAccountSid, Settings.Current.TwilioAuthToken);

			RestRequest request = new RestRequest
									  {
										  Resource = "Accounts/{AccountSid}/AvailablePhoneNumbers/{IsoCountryCode}/Local.json"
									  };

			request.AddUrlSegment("IsoCountryCode", US_COUNTRY_CODE);

			if (!string.IsNullOrEmpty(filter))
			{
				switch (filterType)
				{
					case SmsLongCodeFilterType.Number:
						request.AddParameter(IsAreaCode(filter) ? "AreaCode" : "Contains", filter);
						break;
					case SmsLongCodeFilterType.Location:
						request.AddParameter(IsPostalCode(filter) ? "InPostalCode" : "InRegion", filter);
						break;
					default:
						throw new ArgumentOutOfRangeException("filterType");
				}
			}
				

			request.AddParameter("PageSize", MAXIMUM_TWILIO_PAGE_SIZE);

			if (count == 0)
				count = MAXIMUM_TWILIO_PAGE_SIZE;

			// Note: the Total, NextPageUri, and PreviousPageUri are not returned by Twilio, so this is the way we have to do things.

			AvailablePhoneNumberResult result = twilioClient.Execute<AvailablePhoneNumberResult>(request);
			
			if (result == null)
				throw new ServiceException(HttpStatusCode.InternalServerError, TWILIO_COMMUNICATION_ERROR_MESSAGE);

			if (result.RestException != null)
				throw new ServiceException(HttpStatusCode.BadRequest, result.RestException.Message);

			if (result.AvailablePhoneNumbers == null || result.AvailablePhoneNumbers.Count == 0)
				return new AvailableLongCodeList();

			List<AvailableLongCode> items = result.AvailablePhoneNumbers.Skip(offset).Take(count).Select(item => new AvailableLongCode
																				   {
																					   Number = item.PhoneNumber,
																					   Country = item.IsoCountry,
																					   Region = item.RateCenter,
																					   State = item.Region,
																					   PostalCode = item.PostalCode
																				   }).ToList();

			EnvironmentName environment = Settings.Current.Environment;

			if (environment == EnvironmentName.Debug || environment == EnvironmentName.EA)
			{
				// if the test credentials are available, prepend the magic numbers:
				// 15005550006 always returns success
				// 15005550001 is always invalid
				// 15005550000 is always unavailable
				items.Insert(0, new AvailableLongCode { Country = "US", Number = "5005550006", PostalCode = "00000", Region = "TEST_AVAILABLE" });
			}

			return new AvailableLongCodeList
					   {
						   Items = items,
						   Total = result.AvailablePhoneNumbers.Count,
						   Offset = offset
					   };

		}

		public void PurchaseLongCode(string number)
		{
			string accountSid, authToken;

			if (IsTestNumber(number))
			{
				// use test account credentials only for the test number
				accountSid = Settings.Current.TwilioTestAccountSid;
				authToken = Settings.Current.TwilioTestAuthToken;
			}
			else
			{
				accountSid = Settings.Current.TwilioAccountSid;
				authToken = Settings.Current.TwilioAuthToken;
			}

			number = ConvertToTwilioNumber(number);

			TwilioRestClient twilioClient = new TwilioRestClient(accountSid, authToken);

			PhoneNumberOptions options = new PhoneNumberOptions
											 {
												 PhoneNumber = number
											 };

			IncomingPhoneNumber returnedNumber = twilioClient.AddIncomingPhoneNumber(options);

			if (returnedNumber == null)
				throw new ServiceException(HttpStatusCode.InternalServerError, TWILIO_COMMUNICATION_ERROR_MESSAGE);

			if (returnedNumber.RestException != null)
				throw new ServiceException(HttpStatusCode.BadRequest, returnedNumber.RestException.Message);
		}

		public void ReleaseLongCode(string number)
		{
			// allow the twilio test number to be deleted
			// even using test credentials, twilio denies
			// deletion of the test number
			if (IsTestNumber(number))
				return;

			TwilioRestClient twilioClient = new TwilioRestClient(Settings.Current.TwilioAccountSid, Settings.Current.TwilioAuthToken);

			RestRequest request = new RestRequest
			{
				Resource = "Accounts/{AccountSid}/IncomingPhoneNumbers/.json?PhoneNumber={LongCodeNumber}"
			};

			request.AddParameter("LongCodeNumber", number);

			IncomingPhoneNumberResult result = twilioClient.ListIncomingPhoneNumbers(number, null, null, null);

			if (result == null)
				throw new ServiceException(HttpStatusCode.InternalServerError, TWILIO_COMMUNICATION_ERROR_MESSAGE);

			if (result.RestException != null)
				throw new ServiceException(HttpStatusCode.BadRequest, result.RestException.Message);

			if (result.IncomingPhoneNumbers == null || result.IncomingPhoneNumbers.Count != 1)
				throw new ServiceException(HttpStatusCode.InternalServerError, TWILIO_DELETE_ERROR_MESSAGE);

			IncomingPhoneNumber phoneNumberResult = result.IncomingPhoneNumbers[0];

			DeleteStatus deleteStatus = twilioClient.DeleteIncomingPhoneNumber(phoneNumberResult.Sid);

			if (deleteStatus != DeleteStatus.Success)
				throw new ServiceException(HttpStatusCode.InternalServerError, TWILIO_DELETE_ERROR_MESSAGE);
		}

		#endregion

		#region Private Methods

		private bool IsAreaCode(string text)
		{
			return Regex.IsMatch(text, "^[0-9]{3}$");
		}

		private bool IsPostalCode(string text)
		{
			return Regex.IsMatch(text, "[0-9-]");
		}

		private bool IsTestNumber(string number)
		{
			return number.EndsWith(TWILIO_TEST_NUMBER);
		}

		private string ConvertToTwilioNumber(string phoneNumber)
		{
			// 10 digit: standard us number
			if (phoneNumber.Length == 10)
				return "+1" + phoneNumber;
			
			// 11 or more digits:
			if (phoneNumber.Length >= 11)
				return '+' + phoneNumber;

			return phoneNumber;
		}

		#endregion
	}
}
