﻿using System.Linq;
using System.ServiceModel;
using KswApi.Common.Configuration;
using KswApi.ExternalServices.Framework.WcfServices;

namespace Kswapi.ExternalServices.Rendering.XmPie
{
	public static class XmPieServiceHelpers
	{
		public static Service<T> CreateXmPieService<T>(string serviceIdentifier) where T : IClientChannel
		{
			return new Service<T>(new ClientChannelFactory<T>(new BasicHttpBinding(),
				new EndpointAddress(string.Format("{0}/xmpiewsapi/{1}_SSP.asmx",
					Settings.Current.Renderers.First(r => r.Type == typeof(XmPieRenderingModeFulfiller)).Options["server"],
					serviceIdentifier))));
		}
	}
}