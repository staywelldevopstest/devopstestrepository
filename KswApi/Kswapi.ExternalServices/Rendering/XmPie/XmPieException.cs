﻿using System;

namespace Kswapi.ExternalServices.Rendering.XmPie
{
	public class XmPieException : Exception
	{
		public XmPieException(string message) : base(message) { }
	}
}