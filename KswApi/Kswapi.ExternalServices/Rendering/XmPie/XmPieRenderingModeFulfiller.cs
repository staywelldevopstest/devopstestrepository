﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using KswApi.Common.Configuration;
using KswApi.ExternalServices.Framework;
using KswApi.ExternalServices.Framework.Enums.Rendering.XmPie;
using KswApi.ExternalServices.Framework.Poco.Rendering.XmPie;
using KswApi.ExternalServices.Framework.WcfServices;
using Kswapi.ExternalServices.XmPie.Account;
using Kswapi.ExternalServices.XmPie.Campaign;
using Kswapi.ExternalServices.XmPie.DataSource;
using Kswapi.ExternalServices.XmPie.DataSourcePlanUtils;
using Kswapi.ExternalServices.XmPie.Document;
using Kswapi.ExternalServices.XmPie.Job;
using Kswapi.ExternalServices.XmPie.JobTicket;
using Kswapi.ExternalServices.XmPie.PlanUtils;
using Kswapi.ExternalServices.XmPie.Production;
using KswApi.Interface.Objects.Content;
using KswApi.Interface.Objects.Rendering;
using KswApi.Poco.Enums;
using RestSharp.Contrib;

namespace Kswapi.ExternalServices.Rendering.XmPie
{
	public class XmPieRenderingModeFulfiller : AbstractRenderingModeFulfiller
	{

		// TODO is this necessary?
		//  NATIVE_PDF_OPTIONS
		//  "XMPieQualityHigh"/"XMPieQualityProof"
		//  call jobTicketWs.SetDistillJobOptionName with PDF Conversion Settings file
		//parameters.Add("NATIVE_PDF_OPTIONS", "XMPieQualityProof");
		//if (!_jobTicketService.Use(s => s.SetDistillJobOptionName(userName, password, jobTicketId, "")).Body.SetDistillJobOptionNameResult))
		//	throw new XmPieException("SetDistillJobOptionName call failed.");

		#region Private Fields

		private readonly Service<Account_SSPSoapChannel> _accountService;
		private readonly Service<Campaign_SSPSoapChannel> _campaignService;
		private readonly Service<DataSource_SSPSoapChannel> _dataSourceService;
		private readonly Service<DataSourcePlanUtils_SSPSoapChannel> _dataSourcePlanUtilsService;
		private readonly Service<Document_SSPSoapChannel> _documentService;
		private readonly Service<Job_SSPSoapChannel> _jobService;
		private readonly Service<JobTicket_SSPSoapChannel> _jobTicketService;
		private readonly Service<PlanUtils_SSPSoapChannel> _planUtilsService;
		private readonly Service<Production_SSPSoapChannel> _productionService;
		private string _userName;
		private string _password;
		private string _sharePath;
		private string _shareUserName;
		private string _sharePassword;
		private string _physicalPath;
		private string _xmpieAccount;

		#endregion

		#region Initializers

		public XmPieRenderingModeFulfiller(
			Service<Account_SSPSoapChannel> accountService,
			Service<Campaign_SSPSoapChannel> campaignService,
			Service<DataSource_SSPSoapChannel> dataSourceService,
			Service<DataSourcePlanUtils_SSPSoapChannel> dataSourcePlanUtilsService,
			Service<Document_SSPSoapChannel> documentService,
			Service<Job_SSPSoapChannel> jobService,
			Service<JobTicket_SSPSoapChannel> jobTicketService,
			Service<PlanUtils_SSPSoapChannel> planUtilsService,
			Service<Production_SSPSoapChannel> productionService,
			Settings settings)
		{
			_accountService = accountService;
			_campaignService = campaignService;
			_dataSourceService = dataSourceService;
			_dataSourcePlanUtilsService = dataSourcePlanUtilsService;
			_documentService = documentService;
			_jobService = jobService;
			_jobTicketService = jobTicketService;
			_planUtilsService = planUtilsService;
			_productionService = productionService;
			ReadSettings(settings);
		}

		#endregion

		#region IRenderingModeFulfiller Members

		public override RenderResponse Render(RenderRequest request, KswApiTemplate kswApiTemplate,
			IList<ContentArticleResponse> allTemplates, IList<ContentArticleResponse> allContent)
		{
			// TODO transfer XMPie Campaign package / indesign file to uProduce server
			//string templateCampaignPackageId = xmPieServices.TransferFiles(UserName, Password);

			// translate marked up content into Adobe Tagged Text
			//TranslateContentIntoAdobeTaggedText();

			// produce job
			//byte[] result = _xmPieServices.GenerateProof(UserName, Password, templateCampaignPackageId, ProofType.Jpg);
			string jobId = SubmitJob(@"C:\DropFolderTemp\Sample.cpkg", @"C:\DropFolderTemp\DataSource.csv", ProofType.Pdf);

			// TODO delete files
			//xmPieServices.DeleteFiles(UserName, Password, templateCampaignPackageId);
			//throw new NotImplementedException();

			//byte[] result = GenerateProof("61", ProofType.Jpg);

			// TODO delete files
			//xmPieServices.DeleteFiles(UserName, Password, templateCampaignPackageId);

			return new RenderResponse();
		}

		#endregion

		#region Private Methods

		private string TranslateContentIntoTaggedText(string value)
		{
			string paragraphStyle = "";//Ador.Name.Trim;
			var richText = HttpUtility.HtmlDecode(value) + "\"";
			var taggedText = "\"<UNICODE-WIN>" + Environment.NewLine +
								  "<Version:5><FeatureSet:InDesign-Roman><ColorTable:=<PMS\\_BLACK:COLOR:CMYK:Spot:0,0.13,0.49,0.98>>" +
								  Environment.NewLine +
								  "<ParaStyle:" + paragraphStyle + ">" +
								  richText.Replace("<p>", "<ParaStyle:" + paragraphStyle + ">").
									  Replace("</p>", Environment.NewLine).
									  Replace("<br />", Environment.NewLine + "<ParaStyle:" + paragraphStyle + ">").
									  Replace("<b>", "<cTypeface:Bold>").
									  Replace("</b>", "<cTypeface:>").
									  Replace("<u>", "<cUnderline:1>").
									  Replace("</u>", "<cUnderline:>").
									  Replace("<i>", "<cTypeface:Italic>").
									  Replace("</i>", "<cTypeface:>").
									  Replace("&amp;", "&");
			richText += "\"";
			return taggedText;
		}

		private void CopyFileToXmPieShare(string sourcePathAndFileName)
		{
			//WindowsIdentity identity = WindowsIdentity.GetCurrent();
			//if (identity == null || identity.Name != @"NT AUTHORITY\NETWORK SERVICE")
			//	throw new XmPieException("Process is not running as NETWORK SERVICE!");

			// copy a local file to the XMPie server
			string destinationFileName = Path.GetFileName(sourcePathAndFileName);
			if (destinationFileName == null)
				throw new XmPieException("No source filename provided.");
			//string localTempPathAndFileName = Path.Combine(@"C:\DropFilesTemp\", destinationFileName);
			string destinationPathAndFileName = Path.Combine(_sharePath, destinationFileName);

			// copy files to the local temp folder

			// delete the file if it already exists
			//if (File.Exists(localTempPathAndFileName))
			//	File.Delete(localTempPathAndFileName);
			//File.Copy(sourcePathAndFileName, localTempPathAndFileName);

			// copy from the local temp folder to the remote folder

			using (new Impersonator(_shareUserName, null, _sharePassword))
			{
				// delete the file if it already exists
				if (File.Exists(destinationPathAndFileName))
					File.Delete(destinationPathAndFileName);
				File.Copy(sourcePathAndFileName, destinationPathAndFileName);
			}
		}

		private string ChangeDataSource(string campaignName, string dataSourceFileName)
		{
			//string pathAndFileName = @"C:\DropFolderTemp\Sample (2).mdb";
			//string pathAndFileName = @"C:\DropFolderTemp\DataSource.csv";
			//string campaignName = "Sample";

			string dataSourceName = Path.GetFileNameWithoutExtension(dataSourceFileName);

			string accountId = GetAccountId(_xmpieAccount);
			if (accountId == null)
				throw new XmPieException(string.Format("Unable to find XMPie account '{0}'.", _xmpieAccount));
			string campaignId = GetCampaignId(accountId, campaignName);
			if (campaignId == null)
				throw new XmPieException(string.Format("Unable to find XMPie campaign '{0}'.", campaignName));

			// delete any existing data source that is using this name
			string dataSourceId = GetDataSourceId(campaignId, dataSourceName);
			if (dataSourceId != "0")
				DeleteDataSource(campaignId, dataSourceId);

			// copy the data source file to the XMPie server and create the data source entity

			// MDB
			//CopyFileToXmPieShare(pathAndFileName);
			//string fileName = Path.Combine(_physicalPath, Path.GetFileName(pathAndFileName));
			//string fullUploadedFilePath = Path.Combine(_physicalPath, fileName);
			//string connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0}", fullUploadedFilePath);
			//dataSourceId = CreateNewDataSource(campaignId, DataSourceType.Mdb, dataSourceName, connectionString,
			//	string.Empty, _physicalPath, true);
				
			// TXT
			CopyFileToXmPieShare(dataSourceFileName);
			string fileName = Path.GetFileName(dataSourceFileName);
			string connectionString = string.Format("Provider=Microsoft.Jet.OLEDB.4.0;Data Source={0};Extended Properties=text;", _physicalPath);
			// these both appear to be necessary, true?
			string additionalInfo = string.Format("{0}@,@	", fileName);
			IDictionary<string, string> properties = new Dictionary<string, string> { { "dataSourceFileName", fileName } };
			properties = null;
			dataSourceId = CreateNewDataSource(campaignId, DataSourceType.Txt, dataSourceName, connectionString,
				additionalInfo, _physicalPath, true, properties);

			return dataSourceId;
		}

		// TODO rename this
		private JobStatus GetStatus2(string jobId)
		{
			// state
			int state = GetStatus(jobId);
			JobState jobState;
			if (!Enum.TryParse(state.ToString(), false, out jobState))
				throw new XmPieException(string.Format("Unexpected job state '{0}'.", state));

			// messages
			DataSet messagesDataSet = GetMessagesDataSet(jobId);
			// TODO return more of this information, not just the messages
			string[] messages = messagesDataSet.Tables[0].Rows.Cast<DataRow>().Select(c => (string)c["message"]).ToArray();

			return new JobStatus(jobState, messages);
		}

		private string SubmitJob(string campaignPackageFileName, string dataSourceFileName, ProofType proofType,
			IDictionary<string, string> parameters = null)
		{
			// create the campaign from the file copied to the server
			string campaignName = Path.GetFileNameWithoutExtension(campaignPackageFileName);

			string accountId = GetAccountId(_xmpieAccount);
			if (accountId == null)
				throw new XmPieException(string.Format("Unable to find XMPie account '{0}'.", _xmpieAccount));

			// TODO we can't always rely on the existing to be current...

			// find an existing campaign if one exists
			string campaignId = GetCampaignId(accountId, campaignName);
			if (campaignId == "0")
			{
				// copy the campaign file (template) to the XMPie server and create the associated campaign
				CopyFileToXmPieShare(campaignPackageFileName);
				string pathAndFileName = Path.Combine(_physicalPath, Path.GetFileName(campaignPackageFileName));
				campaignId = CreateNewCampaignFromCpkg(accountId, pathAndFileName, campaignName, true, false);
			}

			// get the list of documents on the server and take the first INDD document found
			IEnumerable<string> documents = GetDocuments(campaignId);
			string documentId = null;
			foreach (string documentId2 in documents)
			{
				IDictionary<string, string> documentProperties =
					GetDocumentProperties(documentId2, new[] {"documentType"});
				if (documentProperties["documentType"] == "INDD")
				{
					documentId = documentId2;
					break;
				}
			}

			if (documentId == null)
				throw new XmPieException(string.Format(
					"Couldn't locate an INDD document within campaign '{0}'.", campaignName));

			// Create a new job ticket
			string jobTicketId = CreateNewTicketForDocument(documentId);


	
			
			//// change the data set for this job, locate the plan ID, schema names, and data source connection string
			//string planId = GetPlan(campaignId);
			//IEnumerable<string> schemaNames = GetSchemaNames(planId);
			//string schemaName = schemaNames.Single();
			//// TODO handle multiple
			////schemaName = schemaNames.Single(n => n == "Foo"));
			//string dataSourceId = ChangeDataSource(campaignName, dataSourceFileName);
			//ExternalServices.XmPie.DataSource.Connection connection = GetConnectionInfo(dataSourceId);
			//if (!SetDataSource(jobTicketId, schemaName, connection))
			//	throw new XmPieException("SetDataSource call failed.");


			//// perform additional checks... not sure how helpful these are
			//if (!ValidatePlan(planId))
			//	throw new XmPieException("ValidatePlan call failed.");
			//if (!IsDataSourceCompatibleWithSchema(planId, schemaName, dataSourceId))
			//	throw new XmPieException("IsDataSourceCompatibleWithSchema call failed.");
			//if (!TestDataSourceConnectivity(dataSourceId))
			//	throw new XmPieException("TestDataSourceConnectivity call failed.");
			

			// set the job output type
			if (!SetOutputInfo(jobTicketId, proofType, Media.Print, parameters))
				throw new XmPieException("SetOutputInfo failed.");

			// set the job type
			if (!SetJobType(jobTicketId, JobType.Proof))
				throw new XmPieException("SetJobType failed.");

			// add a destination
			if (!AddDestinationById(jobTicketId, "13"))
				throw new XmPieException("AddDestinationById failed.");

			// submit the production job
			return "0";
			return SubmitJob(jobTicketId, Priority.Lowest);
		}

		private byte[] GenerateProof(string documentId, ProofType proofType, bool deleteJob)
		{
			string jobId = SubmitJob(@"C:\DropFolderTemp\Sample.cpkg", @"C:\DropFolderTemp\DataSource.csv", proofType);

			bool isFinished = false;
			while (!isFinished)
			{
				JobStatus jobStatus = GetStatus2(jobId);
				switch (jobStatus.State)
				{
					case JobState.Waiting:
					case JobState.InProgress:
						break;
					case JobState.Completed:
						isFinished = true;
						break;
					case JobState.Failed:
						throw new XmPieException(string.Format("Job Failed! JobID: {0}", jobId));
					case JobState.Aborting:
						throw new XmPieException(string.Format("Job Aborting! JobID: {0}", jobId));
					case JobState.Aborted:
						throw new XmPieException(string.Format("Job Aborted! JobID: {0}", jobId));
					case JobState.Deployed:
						isFinished = true;
						break;
					case JobState.Suspended:
						throw new XmPieException(string.Format("Job Suspended! JobID: {0}", jobId));
					default:
						throw new XmPieException(string.Format("Unexpected job state '{0}'.", jobStatus.State));
				}
				Thread.Sleep(500);
			}

			byte[] rval = GetOutputResultBinaryFileStream(jobId);

			// delete the job afterwards?
			if (deleteJob)
				DeleteJob(jobId);

			return rval;
		}

		// TODO rename this
		private string GetOutputResults2(string jobId)
		{
			// Get the status of the job
			JobStatus status = GetStatus2(jobId);
			switch (status.State)
			{
				case JobState.Completed:
					string[] outputResults = GetOutputResults(jobId);
					string outputPath = GetFolderPath(jobId);//.GetFolderPathResult;
					return outputPath + outputResults[0];
				case JobState.Failed:
					return "The process failed";
				case JobState.Aborted:
					return "The process has been aborted";
				default:
					return string.Format("Unexpected status value {0}.", status);
			}
		}

		private void ReadSettings(Settings settings)
		{
			if (settings.Renderers == null)
				throw new XmPieException("Unable to find Renderers configuration.");

			foreach (RenderingConfiguration renderingConfiguration in settings.Renderers)
			{
				if (renderingConfiguration.Type != null &&
					renderingConfiguration.Type.Name.Contains(typeof(XmPieRenderingModeFulfiller).Name))
				{
					_userName = ReadOptionsValue(renderingConfiguration.Options, "userName");
					_password = ReadOptionsValue(renderingConfiguration.Options, "password");
					_sharePath = ReadOptionsValue(renderingConfiguration.Options, "sharePath");
					_shareUserName = ReadOptionsValue(renderingConfiguration.Options, "shareUserName");
					_sharePassword = ReadOptionsValue(renderingConfiguration.Options, "sharePassword");
					_physicalPath = ReadOptionsValue(renderingConfiguration.Options, "physicalPath");
					_xmpieAccount = ReadOptionsValue(renderingConfiguration.Options, "xmpieAccount");
				}
			}

			if (_userName == null || _password == null)
				throw new XmPieException("Unable to find XMPie Renderer Fulfiller.");
		}

		#endregion

		#region XMPie Services Wrapper Methods

		private bool AddDestinationById(string jobTicketId, string destinationId)
		{
			AddDestinationByIDRequest request = new AddDestinationByIDRequest(new AddDestinationByIDRequestBody(
				_userName, _password, jobTicketId, destinationId, null, false));
			return _jobTicketService.Use(s => s.AddDestinationByID(request).Body.AddDestinationByIDResult);
		}

		private string CreateNewCampaignFromCpkg(string accountId, string cpkgPath, string campaignName,
			bool deleteSource, bool deleteEmptySourceFolder)
		{
			CreateNewFromCPKGRequest request = new CreateNewFromCPKGRequest(_userName, _password, accountId,
				cpkgPath, campaignName, deleteSource, deleteEmptySourceFolder, null);
			return _campaignService.Use(s => s.CreateNewFromCPKG(request).CreateNewFromCPKGResult);
		}

		private string CreateNewDataSource(string campaignId, DataSourceType dataSourceType, string dataSourceName,
			string connectionString, string additionalInfo, string sourceFolder, bool deleteSource,
			IDictionary<string, string> properties = null)
		{
			ExternalServices.XmPie.DataSource.Property[] inProperties = null;
			if (properties != null)
				inProperties = properties.Cast<string>().Select(key => new ExternalServices.XmPie.DataSource.Property
				{
					m_Name = key,
					m_Value = properties[key]
				}).ToArray();

			ExternalServices.XmPie.DataSource.CreateNewRequest createNewRequest =
				new ExternalServices.XmPie.DataSource.CreateNewRequest(_userName, _password, campaignId,
				dataSourceType.GetDescription(), dataSourceName, connectionString, additionalInfo, sourceFolder,
				deleteSource, inProperties);
			return _dataSourceService.Use(s => s.CreateNew(createNewRequest).CreateNewResult);
		}

		private string CreateNewTicketForDocument(string documentId)
		{
			CreateNewTicketForDocumentRequest request = new CreateNewTicketForDocumentRequest(
				new CreateNewTicketForDocumentRequestBody(_userName, _password, documentId, string.Empty, false));
			return _jobTicketService.Use(s => s.CreateNewTicketForDocument(request)
				.Body.CreateNewTicketForDocumentResult);
		}

		private bool DeleteJob(string jobId)
		{
			return _jobService.Use(s => s.Delete(_userName, _password, jobId));
		}

		private bool DeleteDataSource(string campaignId, string dataSourceId)
		{
			return _campaignService.Use(s => s.DeleteDataSources(_userName, _password, campaignId, new[] { dataSourceId }));
		}

		private string GetAccountId(string accountName)
		{
			return _accountService.Use(s => s.GetID(_userName, _password, accountName));
		}

		private string GetCampaignId(string accountId, string campaignName)
		{
			return _accountService.Use(s => s.GetCampaignID(_userName, _password, accountId, campaignName));
		}

		private ExternalServices.XmPie.DataSource.Connection GetConnectionInfo(string dataSourceId)
		{
			return _dataSourceService.Use(s => s.GetConnectionInfo(_userName, _password, dataSourceId));
		}

		private string GetDataSourceId(string campaignId, string dataSourceName)
		{
			return _dataSourceService.Use(s => s.GetID(_userName, _password, campaignId, dataSourceName));
		}

		private IDictionary<string, string> GetDocumentProperties(string documentId, IEnumerable<string> propertyNames)
		{
			ExternalServices.XmPie.Document.GetPropertiesRequest request =
				new ExternalServices.XmPie.Document.GetPropertiesRequest(
				_userName, _password, documentId, propertyNames.ToArray());
			ExternalServices.XmPie.Document.Property[] properties =
				_documentService.Use(s => s.GetProperties(request).GetPropertiesResult);
			IDictionary<string, string> dictionary = new Dictionary<string, string>();
			foreach (var property in properties)
				dictionary.Add(property.m_Name, property.m_Value);
			return dictionary;
		}

		private IEnumerable<string> GetDocuments(string campaignId)
		{
			return _campaignService.Use(s => s.GetDocuments(_userName, _password, campaignId));
		}

		private string GetFolderPath(string jobId)
		{
			return _jobService.Use(s => s.GetFolderPath(_userName, _password, jobId));
		}

		private DataSet GetMessagesDataSet(string jobId)
		{
			return _jobService.Use(s => s.GetMessagesDataSet(_userName, _password, jobId));
		}

		private string[] GetOutputResults(string jobId)
		{
			return _jobService.Use(s => s.GetOutputResults(_userName, _password, jobId));
		}

		private byte[] GetOutputResultBinaryFileStream(string jobId)
		{
			GetOutputResultBinaryFileStreamRequest request =
				new GetOutputResultBinaryFileStreamRequest(_userName, _password, jobId, 0);
			return _jobService.Use(s => s.GetOutputResultBinaryFileStream(
				request).GetOutputResultBinaryFileStreamResult);
		}

		private string GetPlan(string campaignId)
		{
			return _campaignService.Use(s => s.GetPlan(_userName, _password, campaignId));
		}

		private IEnumerable<string> GetSchemaNames(string planId)
		{
			GetSchemaNamesRequest request = new GetSchemaNamesRequest(
				new GetSchemaNamesRequestBody(_userName, _password, planId, false));
			return _planUtilsService.Use(s => s.GetSchemaNames(request).Body.GetSchemaNamesResult);
		}

		private int GetStatus(string jobId)
		{
			return _jobService.Use(s => s.GetStatus(_userName, _password, jobId));
		}

		private bool IsDataSourceCompatibleWithSchema(string planId, string schemaName, string dataSourceId)
		{
			return _dataSourcePlanUtilsService.Use(s => s.IsDataSourceCompatibleWithSchema(
				_userName, _password, planId, schemaName, dataSourceId));
		}

		private bool SetDataSource(string jobTicketId, string schemaName, ExternalServices.XmPie.DataSource.Connection connection)
		{
			ExternalServices.XmPie.JobTicket.Connection jobTicketConnection = new ExternalServices.XmPie.JobTicket.Connection
			{
				m_AdditionalInfo = connection.m_AdditionalInfo,
				m_ConnectionString = connection.m_ConnectionString,
				m_Type = connection.m_Type
			};
			return SetDataSource(jobTicketId, schemaName, jobTicketConnection);
		}

		private bool SetDataSource(string jobTicketId, string schemaName, ExternalServices.XmPie.JobTicket.Connection connection)
		{
			SetDataSourceRequest request = new SetDataSourceRequest(
				new SetDataSourceRequestBody(_userName, _password, jobTicketId, schemaName, connection));
			return _jobTicketService.Use(s => s.SetDataSource(request).Body.SetDataSourceResult);
		}

		private bool SetJobType(string jobTicketId, JobType jobType)
		{
			SetJobTypeRequest request = new SetJobTypeRequest(new SetJobTypeRequestBody(
				_userName, _password, jobTicketId, jobType.GetDescription()));
			return _jobTicketService.Use(s => s.SetJobType(request).Body.SetJobTypeResult);
		}

		private bool SetOutputInfo(string jobTicketId, ProofType proofType, Media media,
			IDictionary<string, string> parameters = null)
		{
			Parameter[] inParams = null;
			if (parameters != null)
				inParams = parameters.Cast<string>().Select(key => new Parameter
				{
					m_Name = key,
					m_Value = parameters[key]
				}).ToArray();

			SetOutputInfoRequest request = new SetOutputInfoRequest(
				new SetOutputInfoRequestBody(_userName, _password, jobTicketId, proofType.GetDescription(),
					(int)media, string.Empty, string.Empty, inParams));
			return _jobTicketService.Use(s => s.SetOutputInfo(request).Body.SetOutputInfoResult);
		}

		private string SubmitJob(string jobTicketId, Priority priority)
		{
			SubmitJobRequest request = new SubmitJobRequest(
				new SubmitJobRequestBody(_userName, _password, jobTicketId, priority.ToString(), string.Empty));
			return _productionService.Use(s => s.SubmitJob(request).Body.SubmitJobResult);
		}

		private bool TestDataSourceConnectivity(string dataSourceId)
		{
			return _dataSourcePlanUtilsService.Use(s => s.TestDataSourceConnectivity(
				_userName, _password, dataSourceId));
		}

		private bool ValidatePlan(string planId)
		{
			ValidatePlanRequest request = new ValidatePlanRequest(new ValidatePlanRequestBody(
				_userName, _password, planId));
			return _planUtilsService.Use(s => s.ValidatePlan(request).Body.ValidatePlanResult);
		}

		#endregion
	}
}