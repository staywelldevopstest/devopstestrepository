﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using KswApi.Common.Configuration;
using Kswapi.ExternalServices.Rendering.XmPie;
using KswApi.Interface.Objects.Content;
using KswApi.Interface.Rendering;
using KswApi.Interface.Objects.Rendering;
using Newtonsoft.Json;

namespace Kswapi.ExternalServices.Rendering
{
	public class DebugRenderingModeFulfiller : AbstractRenderingModeFulfiller
	{
		private string _outputPath;

		public DebugRenderingModeFulfiller()
		{
			ReadSettings(Settings.Current);
		}

		public override RenderResponse Render(RenderRequest request, KswApiTemplate kswApiTemplate,
			IList<ContentArticleResponse> allTemplates, IList<ContentArticleResponse> allContent)
		{
			File.WriteAllText(Path.Combine(_outputPath, string.Format("{0} - {1}.txt", request.RecipientIdentifier, typeof(RenderRequest).Name)), JsonConvert.SerializeObject(request, Formatting.Indented));
			File.WriteAllText(Path.Combine(_outputPath, string.Format("{0} - {1}.txt", request.RecipientIdentifier, typeof(KswApiTemplate).Name)), JsonConvert.SerializeObject(kswApiTemplate, Formatting.Indented));

			// output templates
			foreach (ContentArticleResponse contentArticleResponse in allTemplates)
				File.WriteAllText(Path.Combine(_outputPath, string.Format("{0} - {1}.txt", request.RecipientIdentifier, contentArticleResponse.Slug)),
					contentArticleResponse.Segments.Aggregate(string.Empty, (current, s) => current + s.Body));

			// output content
			foreach (ContentArticleResponse contentArticleResponse in allContent)
				File.WriteAllText(Path.Combine(_outputPath, string.Format("{0} - {1}.txt", request.RecipientIdentifier, contentArticleResponse.Slug)),
					contentArticleResponse.Segments.Aggregate(string.Empty, (current, s) => current + s.Body));

			return new RenderResponse();
		}

		private void ReadSettings(Settings settings)
		{
			if (settings.Renderers == null)
				throw new XmPieException("Unable to find Renderers configuration.");

			foreach (RenderingConfiguration renderingConfiguration in settings.Renderers)
			{
				if (renderingConfiguration.Type != null &&
					renderingConfiguration.Type.Name.Contains(typeof(DebugRenderingModeFulfiller).Name))
				{
					_outputPath = ReadOptionsValue(renderingConfiguration.Options, "outputPath");
				}
			}
		}
	}
}