﻿using System.Collections.Generic;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects.Content;
using KswApi.Interface.Objects.Rendering;
using KswApi.Interface.Rendering;

namespace Kswapi.ExternalServices.Rendering
{
	public abstract class AbstractRenderingModeFulfiller : IRenderingModeFulfiller
	{
		public abstract RenderResponse Render(RenderRequest request, KswApiTemplate kswApiTemplate, IList<ContentArticleResponse> allTemplates, IList<ContentArticleResponse> allContent);

		protected string ReadOptionsValue(IDictionary<string, string> dictionary, string key)
		{
			string rval;
			if (dictionary.TryGetValue(key, out rval))
				return rval;
			throw new RenderingException(string.Format("Unable to find option value with key '{0}'.", key));
		}
	}
}
