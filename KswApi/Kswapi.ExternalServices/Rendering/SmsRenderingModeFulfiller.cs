﻿using System.Collections.Generic;
using KswApi.Interface.Objects.Content;
using KswApi.Interface.Objects.Rendering;
using KswApi.Interface.Rendering;
using KswApi.Logic.Framework.Interfaces;

namespace Kswapi.ExternalServices.Rendering
{
	public class SmsRenderingModeFulfiller : IRenderingModeFulfiller
	{
		public RenderResponse Render(RenderRequest request, KswApiTemplate kswApiTemplate,
			IList<ContentArticleResponse> allTemplates, IList<ContentArticleResponse> allContent)
		{
			ISmsService service = new TwilioService();
			service.SendMessage(request.To, request.From, "Body");
			return new RenderResponse();
		}
	}
}
