﻿using System;
using KswApi.Ioc;
using KswApi.Logging.Framework;
using KswApi.Logging.Interfaces;
using KswApi.Logging.Objects;

// ReSharper disable EmptyGeneralCatchClause
namespace KswApi.Logging
{
	public static class Logger
	{
		// We allow exception swallowing for the logs only
		// because this is the error handling code for logging
		// exceptions, and we don't want to hide the initial exception
		// by throwing a log exception every call
		public static void Log(OperationLog operationLog)
		{
			try
			{
				ILog log = Dependency.Get<ILog>();

				log.Log(operationLog);
			}
			catch (Exception)
			{
				
			}
		}

		public static void Log(Exception exception, OperationLog operationLog)
		{
			try
			{
				ILog log = Dependency.Get<ILog>();

				log.Log(exception, operationLog);
			}
			catch (Exception)
			{
				
			}
		}

		public static void Log(Exception exception)
		{
			try
			{
				ILog log = Dependency.Get<ILog>();

				OperationLog operationLog = LogContext.Current;

				log.Log(exception, operationLog);
			}
			catch (Exception)
			{
				
			}
		}

		public static void Log(Severity severity, Exception exception)
		{
			try
			{
				ILog log = Dependency.Get<ILog>();

				OperationLog operationLog = LogContext.Current;

				log.Log(severity, exception, operationLog);
			}
			catch (Exception)
			{
				
			}
		}

		public static void Log(Severity severity, string title, string details)
		{
			try
			{
				ILog log = Dependency.Get<ILog>();

				OperationLog operationLog = LogContext.Current;

				log.Log(severity, title, details, operationLog);
			}
			catch (Exception)
			{

			}
		}
	}
}
