﻿using System.Collections.Concurrent;
using System.Threading;
using KswApi.Logging.Objects;

namespace KswApi.Logging.Framework
{
	public class LogContext
	{
		private static readonly ConcurrentDictionary<int, OperationLog> OperationLogs =
			new ConcurrentDictionary<int, OperationLog>();

		public static OperationLog Current
		{
			get
			{
				OperationLog log;

				OperationLogs.TryGetValue(Thread.CurrentThread.ManagedThreadId, out log);

				return log;
			}
		}

		public static void SetCurrent(OperationLog operationLog)
		{
			OperationLogs.TryAdd(Thread.CurrentThread.ManagedThreadId, operationLog);
		}

		public static void RemoveCurrent()
		{
			OperationLog log;

			OperationLogs.TryRemove(Thread.CurrentThread.ManagedThreadId, out log);
		}

		public static void ClearAll()
		{
			OperationLogs.Clear();
		}
}
}
