﻿namespace KswApi.Logging
{
	public enum Severity
	{
		Information,
		Security,
		Warning
	}
}
