﻿using System;
using KswApi.Logging.Objects;

namespace KswApi.Logging.Interfaces
{
	public interface ILog
	{
		void Log(OperationLog operationLog);
		void Log(Exception exception, OperationLog operationLog);
		void Log(Severity severity, Exception exception, OperationLog operationLog);
		void Log(Severity severity, string title, string details, OperationLog operationLog);
	}
}
