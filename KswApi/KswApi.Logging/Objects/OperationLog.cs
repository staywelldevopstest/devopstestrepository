﻿using System;
using System.Diagnostics;

namespace KswApi.Logging.Objects
{
	public class OperationLog
	{
		private readonly Stopwatch _stopWatch = new Stopwatch();

		public string ClientAddress { get; set; }
		public string Verb { get; set; }
		public DateTime Date { get; set; }
		public string Path { get; set; }
		public TimeSpan Duration { get { return _stopWatch.Elapsed; } }
		public string Body { get; set; }
		public Guid Client { get; set; }
		public string License { get; set; }
		public int ResponseCode { get; set; }

		public void Start()
		{
			_stopWatch.Start();
		}

		public void Stop()
		{
			_stopWatch.Stop();
		}
	}
}
