﻿using System;
using KswApi.Logging.Interfaces;
using KswApi.Logging.Objects;

namespace KswApi.Logging.Logs
{
	/// <summary>
	/// This class does nothing at all with logging messages
	/// </summary>
	public class NullLog : ILog
	{
		public void Log(OperationLog operationLog)
		{
			
		}

		public void Log(Exception exception, OperationLog operationLog)
		{

		}

		public void Log(Severity severity, Exception exception, OperationLog operationLog)
		{

		}

		public void Log(Severity severity, string title, string details, OperationLog operationLog)
		{
			
		}
	}
}
