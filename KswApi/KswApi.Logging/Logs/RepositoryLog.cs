﻿using System;
using KswApi.Interface.Objects;
using KswApi.Logging.Interfaces;
using KswApi.Logging.Objects;
using KswApi.Repositories;

namespace KswApi.Logging.Logs
{
	public class RepositoryLog : ILog
	{
		public void Log(OperationLog operationLog)
		{
			Log log = LogFromOperationLog(operationLog);
			LogRepository logRepository = new LogRepository();
			logRepository.Logs.Add(log);
			logRepository.Commit();
		}

		public void Log(Exception exception, OperationLog operationLog)
		{
			Log log = LogFromOperationLog(operationLog);
            log.Title = exception.Message;
			log.Type = LogItemType.Notification;
			log.SubType = LogItemSubType.Exception;
            log.Message = exception.ToString();
			log.StackTrace = exception.ToString();            
			
			LogRepository logRepository = new LogRepository();
			logRepository.Logs.Add(log);
			logRepository.Commit();
		}

		Log LogFromOperationLog(OperationLog operationLog)
		{
			if (operationLog == null)
				return new Log
					       {
							   Date = DateTime.UtcNow
					       };

			return new Log
			{
                Title = "Operation Log",
				Date = DateTime.UtcNow,
				Type = LogItemType.Operation,
				SubType = LogItemSubType.Operation,
				Duration = operationLog.Duration,
				Body = operationLog.Body,
				Verb = operationLog.Verb,
				Path = operationLog.Path,
				Client = operationLog.Client,
				License = operationLog.License,
				ResponseCode = operationLog.ResponseCode,
				ClientAddress = operationLog.ClientAddress
			};
		}

		private LogItemSubType SubTypeFromSeverity(Severity severity)
		{
			switch (severity)
			{
				case Severity.Information:
					return LogItemSubType.Information;
				case Severity.Security:
					return LogItemSubType.Security;
				case Severity.Warning:
					return LogItemSubType.Warning;
				default:
					throw new ArgumentOutOfRangeException("severity");
			}
		}

		public void Log(Severity severity, Exception exception, OperationLog operationLog)
		{
			Log log = LogFromOperationLog(operationLog);

            log.Title = string.Format("{0}: {1}", exception.Message, Enum.GetName(typeof(Severity), severity));
			log.Type = LogItemType.Notification;
			log.SubType = SubTypeFromSeverity(severity);
			log.Message = exception.ToString();

			LogRepository logRepository = new LogRepository();
			logRepository.Logs.Add(log);
			logRepository.Commit();
		}

		public void Log(Severity severity, string title, string details, OperationLog operationLog)
		{
			Log log = LogFromOperationLog(operationLog);

			log.Title = title;
			log.Type = LogItemType.Notification;
			log.SubType = SubTypeFromSeverity(severity);
			log.Message = details;

			LogRepository logRepository = new LogRepository();
			logRepository.Logs.Add(log);
			logRepository.Commit();
		}
	}
}
