﻿using System;
using KswApi.Logging.Interfaces;
using KswApi.Logging.Objects;

namespace KswApi.Logging.Logs
{
	/// <summary>
	/// This class allows multiple logs to be called for each logging operation
	/// </summary>
	public class CompositeLog : ILog
	{
		private readonly ILog[] _logs;

		public CompositeLog(params ILog[] logs)
		{
			_logs = logs;
		}

		public void Log(OperationLog operationLog)
		{
			foreach (ILog log in _logs)
				log.Log(operationLog);
		}

		public void Log(Exception exception, OperationLog operationLog)
		{
			foreach (ILog log in _logs)
				log.Log(exception, operationLog);
		}

		public void Log(Severity severity, Exception exception, OperationLog operationLog)
		{
			foreach (ILog log in _logs)
				log.Log(severity, exception, operationLog);
		}

		public void Log(Severity severity, string title, string details, OperationLog operationLog)
		{
			foreach (ILog log in _logs)
				log.Log(severity, title, details, operationLog);
		}
	}
}
