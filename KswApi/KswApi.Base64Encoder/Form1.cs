﻿using System;
using System.Text;
using System.Windows.Forms;

// ReSharper disable InconsistentNaming

namespace KswApi.Base64Encoder
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void encodeButton_Click(object sender, EventArgs e)
		{
			try
			{
				encodingBox.Text = Convert.ToBase64String(Encoding.UTF8.GetBytes(textBox.Text));
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void decodeButton_Click(object sender, EventArgs e)
		{
			try
			{
				textBox.Text = Encoding.UTF8.GetString(Convert.FromBase64String(encodingBox.Text));
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

	}
}
