﻿using System.IO;

namespace KswApi.Logic.Framework
{
	public class DuplicatingStream : Stream
	{
		private readonly MemoryStream _memoryStream = new MemoryStream();
		private readonly Stream _stream;

		public DuplicatingStream(Stream stream)
		{
			_stream = stream;
		}

		#region Overrides of Stream

		public override void Flush()
		{
			_stream.Flush();
			_memoryStream.Flush();
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			long value = _stream.Seek(offset, origin);
			_memoryStream.Seek(offset, origin);

			return value;
		}

		public override void SetLength(long value)
		{
			_stream.SetLength(value);
			_memoryStream.SetLength(value);
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			int actualCount = _stream.Read(buffer, offset, count);
			
			_memoryStream.Write(buffer, offset, actualCount);
			
			return actualCount;
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			_stream.Write(buffer, offset, count);
			_memoryStream.Write(buffer, offset, count);
		}

		public override bool CanRead
		{
			get { return _stream.CanRead; }
		}

		public override bool CanSeek
		{
			get { return _stream.CanSeek; }
		}

		public override bool CanWrite
		{
			get { return _stream.CanWrite; }
		}

		public override long Length
		{
			get { return _stream.Length; }
		}

		public override long Position
		{
			get { return _stream.Position; }
			set
			{
				_stream.Position = value;
				_memoryStream.Position = value;
			}
		}

		public Stream GetDuplicate()
		{
			return _memoryStream;
		}

		#endregion
	}
}
