﻿using System;
using System.Collections.Generic;
using KswApi.Interface.Objects;
using KswApi.Poco.Content;

namespace KswApi.Logic.Framework.Objects
{
	public class CollectionMethodParameters
	{
		//public List<CollectionStructure> StructureList { get; set; }
		//public List<Topic> TopicList { get; set; }
		//public List<Guid> ContentList { get; set; }


		public DateTime Now { get; set; }
		public List<Guid> Path { get; set; }
		public Guid CollectionId { get; set; }
	
		public Dictionary<ItemReference, Guid> ContentLookup { get; set; }

		public List<CollectionStructure> StructuresToAdd { get; set; }
		public List<Topic> TopicsToAdd { get; set; }
		public List<CollectionStructure> StructuresToUpdate { get; set; }
		public List<Topic> TopicsToUpdate { get; set; }
		public Dictionary<Guid, CollectionStructure> StructureLookup { get; set; }
		public HashSet<string> UsedTopicSlugs { get; set; }
		public Dictionary<string, Topic> ExistingTopicLookup { get; set; }
	}
}
