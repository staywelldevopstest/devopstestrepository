﻿using KswApi.Interface.Enums;
using System.Collections.Generic;

namespace KswApi.Logic.Framework.Objects
{
    public class ServiceModule
    {
        public string Name { get; set; }
        public List<string> DefaultScope { get; set; }
        public ModuleType Type { get; set; }
        public string Path { get; set; }
    }
}
