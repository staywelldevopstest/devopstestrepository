﻿namespace KswApi.Logic.Framework.Objects
{
	// ContentSlugPath should become ContentPath (someday)
	public struct ContentSlugPath
	{
		public string BucketSlug { get; set; }
		public string ContentSlug { get; set; }
	}
}
