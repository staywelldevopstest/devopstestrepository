﻿using Newtonsoft.Json;

namespace KswApi.Logic.Framework.Objects
{
	public class TermCountResponse
	{
		[JsonProperty(PropertyName = "term")]
		public object Term { get; set; }

		[JsonProperty(PropertyName = "count")]
		public int Count { get; set; }
	}
}
