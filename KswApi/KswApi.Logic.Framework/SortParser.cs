﻿using System;
using System.Linq;
using System.Net;
using System.Reflection;
using KswApi.Interface.Exceptions;
using KswApi.Repositories.Enums;
using KswApi.Repositories.Objects;

namespace KswApi.Logic.Framework
{
	public class SortParser<T>
	{
		public SortOption<T> Parse(string sortString)
		{
			PropertyInfo[] properties = typeof (T).GetProperties(BindingFlags.Public | BindingFlags.Instance);

			string[] split = sortString.Split(' ', '-');

			SortDirection direction = split.Length > 1
				? GetSortDirection(split[1])
				: SortDirection.Ascending;

			PropertyInfo property = properties.FirstOrDefault(item => item.Name.Equals(split[0], StringComparison.OrdinalIgnoreCase));

			if (property == null)
				throw new ServiceException(HttpStatusCode.BadRequest, "Invalid sort option.");

			return new SortOption<T>
				       {
					       Property = property,
					       Direction = direction
				       };
		}

		private SortDirection GetSortDirection(string direction)
		{
			switch (direction.ToUpper())
			{
				case "ASCENDING":
				case "ASC":
					return SortDirection.Ascending;
				case "DESCENDING":
				case "DESC":
					return SortDirection.Descending;
				default:
					throw new ParseException("Invalid sort direction.");
			}
		}
	}
}
