﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace KswApi.Logic.Framework.Extensions
{
	public static class DbSetExtensions
	{
		public static TEntity Get<TEntity>(this IQueryable<TEntity> source, Expression<Func<TEntity, bool>> expression) where TEntity : class
		{
			TEntity value = source.SingleOrDefault(expression);

			if (value == null)
				return null;

			BinaryExpression binaryExpression = expression.Body as BinaryExpression;

			if (binaryExpression == null)
				throw new ArgumentException("Expression must be an equivalence expression");

			MemberExpression memberExpression = binaryExpression.Left as MemberExpression;

			if (memberExpression == null)
				throw new ArgumentException("Property is not a valid property", "expression");

			return value;
		}

		/// <summary>
		/// Remove the entity from the database and also from the cache.
		/// Use this method instead of the normal IDbSet Remove method
		/// 
		/// The entity does not need to be attached to call this method
		/// (it can come from the cache)
		/// 
		/// Commit must be called on the DataContext after this method is called.
		/// </summary>
		/// <typeparam name="TEntity">The type of entity to remove</typeparam>
		/// <typeparam name="TProperty">The type of the attribute that the entity is keyed on in the cache</typeparam>
		/// <param name="source">The IDbSet</param>
		/// <param name="entity">The entity to remove (does not need to be attached to the IDbSet)</param>
		/// <param name="propertyExpression">Expression giving the property that the entity is cached on</param>
		public static void Remove<TEntity, TProperty>(this IDbSet<TEntity> source, TEntity entity, Expression<Func<TEntity, TProperty>> propertyExpression) where TEntity : class
		{
			source.Attach(entity);

			source.Remove(entity);
		}
	}
}
