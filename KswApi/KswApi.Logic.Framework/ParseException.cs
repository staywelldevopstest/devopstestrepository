﻿using System;

namespace KswApi.Logic.Framework
{
	public class ParseException : Exception
	{
		public ParseException() : base("Error parsing the expression.")
		{
			
		}

		public ParseException(string message)
			: base(message)
		{

		}
	}
}
