﻿using System;
using System.Collections.Specialized;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using KswApi.Logic.Framework.Constants;

namespace KswApi.Logic.Framework.Helpers
{
	public static class UriHelper
	{
		public static bool IsHttps(string uri)
		{
			return uri.StartsWith(Uri.UriSchemeHttps, StringComparison.InvariantCultureIgnoreCase);
		}

		public static bool AbsoluteUrisAreEqual(string uri1, string uri2)
		{
			uri1 = GetAbsolutUri(uri1);
			uri2 = GetAbsolutUri(uri2);

			return uri1.Equals(uri2, StringComparison.InvariantCultureIgnoreCase);
		}

		public static string Combine(string baseUri, params string[] segments)
		{
			StringBuilder builder = new StringBuilder();
			bool hasSeparator = true;
			if (!string.IsNullOrEmpty(baseUri))
			{
				builder.Append(baseUri);
				hasSeparator = baseUri.EndsWith("/");
			}

			foreach (string segment in segments)
			{
				if (!hasSeparator)
					builder.Append('/');
				if (!string.IsNullOrEmpty(segment))
				{
					builder.Append(segment);
					hasSeparator = segment.EndsWith("/");
				}
			}

			return builder.ToString();
		}

		public static bool UriStartsWith(string uri1, string uri2)
		{
			if (string.IsNullOrEmpty(uri1) || string.IsNullOrEmpty(uri2))
				return false;

			return StripScheme(uri1).StartsWith(StripScheme(uri2), StringComparison.OrdinalIgnoreCase);
		}

		public static bool IsValid(string uri)
		{
			if (string.IsNullOrEmpty(uri))
				return false;

			// match domain or ip address
			return Regex.IsMatch(uri,
								 @"^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})(\:[1-9][0-9]*)?([\/][\w &\.\-%]*)*(\?[\w #&\.\-=%+,]*)?(\#[\w #&\.\-=%+,]*)?$",
								 RegexOptions.IgnoreCase)
				   || IsValidIpUri(uri)
				   || Regex.IsMatch(uri,
								 @"^(https?:\/\/)?localhost(\:[1-9][0-9]*)?([\/][\w &\.\-%]*)*(\?[\w #&\.\-=%+,]*)?(\#[\w #&\.\-=%+,]*)?$",
								 RegexOptions.IgnoreCase);
		}

		public static bool IsValidIpUri(string uri)
		{
			Match match = Regex.Match(uri,
								 @"^(https?:\/\/)?(?<number>0|([1-9][0-9]{0,2}))(\.(?<number>0|([1-9][0-9]{0,2}))){3}(\:(?<port>[1-9][0-9]*))?([\/][\w &\.\-%]*)*(\?[\w #&\.\-=%+,]*)?(\#[\w #&\.\-=%+,]*)?$",
								 RegexOptions.IgnoreCase);

			if (!match.Success)
				return false;

			foreach (Capture capture in match.Groups["number"].Captures)
			{
				int value;
				if (!int.TryParse(capture.Value, out value))
					return false;
				if (value < 0 || value > Constant.MAXIMUM_IP_SEGMENT_VALUE)
					return false;
			}

			foreach (Capture capture in match.Groups["port"].Captures)
			{
				int value;
				if (!int.TryParse(capture.Value, out value))
					return false;
				if (value < 0 || value > Constant.MAXIMUM_TCP_PORT_VALUE)
					return false;
			}

			return true;
		}

		public static string GetAbsolutUri(string uri)
		{
			return StripQuery(StripScheme(uri));
		}

		public static string StripScheme(string uri)
		{
			int index = uri.IndexOf("://", StringComparison.Ordinal);
			if (index == -1)
				return uri;

			// the +3 is for the length of "://"
			return uri.Substring(index + 3);
		}

		public static string StripQuery(string uri)
		{
			int index = uri.IndexOf("?", StringComparison.Ordinal);
			if (index == -1)
				return uri;

			return uri.Substring(0, index);
		}

		public static string AddQuery(string baseUrl, NameValueCollection parameters)
		{
			if (parameters.Count == 0)
				return baseUrl;

			StringBuilder builder = new StringBuilder();

			builder.Append(baseUrl);

			bool first = false;

			if (!baseUrl.Contains("?"))
			{
				builder.Append('?');
				first = true;
			}

			foreach (string key in parameters.AllKeys)
			{
				if (first)
					first = false;
				else
					builder.Append('&');

				builder.Append(HttpUtility.UrlEncode(key));
				builder.Append('=');
				builder.Append(HttpUtility.UrlEncode(parameters[key]));
			}

			return builder.ToString();
		}
	}
}
