﻿using System.Text.RegularExpressions;

namespace KswApi.Logic.Framework.Helpers
{
	public static class EmailHelper
	{
		private const string EMAIL_PATTERN = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
				+ @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
				+ @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

		public static bool IsValid(string email)
		{
			return Regex.IsMatch(email, EMAIL_PATTERN, RegexOptions.IgnoreCase);
		}
	}
}
