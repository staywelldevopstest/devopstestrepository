﻿using System.Net;
using System.Text.RegularExpressions;
using KswApi.Interface.Exceptions;
using KswApi.Logic.Framework.Constants;

namespace KswApi.Logic.Framework.Helpers
{
	public static class ValidationHelper
	{
		public static void ValidateSearch(int offset, int count, int max = Constant.MAXIMUM_PAGE_SIZE)
		{
			if (offset < 0)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.OFFSET_LESS_THAN_ZERO);

			if (count < 0)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.OFFSET_LESS_THAN_ZERO);

			if (count > max)
				throw new ServiceException(HttpStatusCode.BadRequest, string.Format(ErrorMessage.Common.COUNT_TOO_HIGH, count));
		}

		public static bool IsValidSlug(string slug)
		{
			return !string.IsNullOrEmpty(slug) && Regex.IsMatch(slug, "^[a-z0-9_-]+$");
		}

		public static void ValidateSlug(string slug)
		{
			if (!IsValidSlug(slug))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_SLUG);
		}
	}
}
