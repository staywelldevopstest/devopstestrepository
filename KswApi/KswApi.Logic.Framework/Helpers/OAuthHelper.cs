﻿
using System;
using KswApi.Interface.Enums;
using KswApi.Logic.Framework.Enums;

namespace KswApi.Logic.Framework.Helpers
{
	public static class OAuthHelper
	{
		public static string GetErrorCode(OAuthErrorCode errorCode)
		{
			switch (errorCode)
			{
				case OAuthErrorCode.InvalidClient:
					return "invalid_client";
				case OAuthErrorCode.InvalidRequest:
					return "invalid_request";
				case OAuthErrorCode.InvalidGrant:
					return "invalid_grant";
				case OAuthErrorCode.UnsupportedResponseType:
					return "unsupported_response_type";
				case OAuthErrorCode.UnauthorizedClient:
					return "unauthorized_client";
				case OAuthErrorCode.AccessDenied:
					return "access_denied";
				case OAuthErrorCode.UnsupportedGrantType:
					return "unsupported_grant_type";
				case OAuthErrorCode.InvalidScope:
					return "invalid_scope";
				default:
					throw new ArgumentOutOfRangeException("reasonCode");
			}
		}

		public static string GetTokenType(OAuthTokenType tokenType)
		{
			switch (tokenType)
			{
				case OAuthTokenType.Bearer:
					return "bearer";
				default:
					throw new ArgumentOutOfRangeException("tokenType");
			}
		}
	}
}
