﻿using System.Linq;
using System.Text;

namespace KswApi.Logic.Framework.Helpers
{
    public static class PhoneNumberHelper
    {
        private const int MINIMUM_PHONE_NUMBER_LENGTH = 3; // Standard is 5, but some countries have 3-digit short codes
        private const int MAXIMUM_PHONE_NUMBER_LENGTH = 20; // Largest known international number

        public static string GetStandardNumber(string number)
        {
            if (string.IsNullOrEmpty(number))
                return number;

            if (number.Length <= 10)
                return number;

            if (number.Length == 11 && number.StartsWith("1"))
                return number.Substring(1);

            if (number.Length == 12 && number.StartsWith("+1"))
                return number.Substring(2);

            if (number.StartsWith("+"))
                return number.Substring(1);

            return number;
        }

        public static bool IsValidPhoneNumber(string number)
        {
            if (string.IsNullOrEmpty(number))
                return false;

            if (number.StartsWith("+"))
                number = number.Substring(1);

            if (number.Length < MINIMUM_PHONE_NUMBER_LENGTH)
                return false;

            if (number.Length > MAXIMUM_PHONE_NUMBER_LENGTH)
                return true;

            if (!number.All(char.IsDigit))
                return false;

            return true;
        }

        public static string RemoveDisplayCharacters(string number)
        {
            if (string.IsNullOrEmpty(number))
                return number;

            StringBuilder builder = new StringBuilder();

            foreach (char c in number.Where(c => char.IsDigit(c)))
            {
                builder.Append(c);
            }

            return builder.ToString();
        }

    }
}
