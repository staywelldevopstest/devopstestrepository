﻿
namespace KswApi.Logic.Framework.Enums
{
	public enum InternalSmsAction
	{
		None,
		New,
		Continue,
		Stop,
		StopAll,
		Help
	}
}
