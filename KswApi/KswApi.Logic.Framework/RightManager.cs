﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Collections.Concurrent;
using System.Text.RegularExpressions;

namespace KswApi.Logic.Framework
{
	public static class RightManager
	{
		private const string RIGHT_REGEX = @"[a-zA-Z0-9_\-]";

		private static readonly ConcurrentDictionary<string, string> Rights = new ConcurrentDictionary<string, string>();

		private static List<string> _sortedRights; 

		public static void AddRight(string name)
		{
			Regex regex = new Regex(RIGHT_REGEX);
			if (!regex.IsMatch(name))
				throw new ArgumentException(string.Format("Right name is invalid: {0}.", name));
			Rights.TryAdd(name, name);
		}

		public static List<string> GetRights()
		{
			List<string> sortedRights = _sortedRights;
			
			if (sortedRights != null)
				return new List<string>(sortedRights);

			sortedRights = Rights.Values.ToList();
			
			sortedRights.Sort();

			_sortedRights = sortedRights;

			return new List<string>(sortedRights);
		}

		public static bool HasRight(string name)
		{
			return Rights.ContainsKey(name);
		}

		public static void Clear()
		{
			Rights.Clear();
		}
	}
}
