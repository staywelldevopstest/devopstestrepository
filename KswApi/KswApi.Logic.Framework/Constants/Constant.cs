﻿using System;

namespace KswApi.Logic.Framework.Constants
{
	public static class Constant
	{
		public static class Configuration
		{
			public static class Ids
			{
				public const string THREE_M_HDD_CURRENT_VERSION = "3M HDD Current Version";
                public const string HIERARCHICAL_TAXONOMY_REFERENCE_FILES = "HierarchicalTaxonomyReferenceFiles";
			}
		}

		public static class Taxonomies
		{
			public static class ThreeMIds
			{
				public const int ICD_9 = 1257;
				public const int CPT = 17274;
				public const int LOINC = 223;
				public const int HCPCS = 78828;
				public const int SNOMED = 76781;
				public const int RXNORM = 76895;
				public const int ICD_10_CM = 77001;
				public const int ICD_10_PCS = 77004;
			}

			public static class Slugs
			{
				public const string ICD_9 = "icd-9";
				public const string CPT = "cpt";
				public const string HCPCS = "hcpcs";
				public const string ICD_10_CM = "icd-10-cm";
				public const string ICD_10_PCS = "icd-10-pcs";
				public const string LOINC = "loinc";
				public const string MESH = "mesh";
				public const string NDC = "ndc";
				public const string RXNORM = "rxnorm";
				public const string SNOMED = "snomed";
			}
		}

		public static class Administration
		{
			public const int ACCOUNT_LOCKOUT_DURATION_IN_MINUTES = 30;
			public const int LOGIN_ATTEMPT_TIMESPAN_IN_MINUTES = 30;
			public const int MAXIMUM_FAILED_LOGIN_ATTEMPT = 5;
			public const int MINIMUM_PASSWORD_LENGTH = 8;
		}

		public static class Copyright
		{
			public const string DEFAULT_COPYRIGHT_ID = "f5e284cf-2b20-4d3a-93c7-a1e401554c30";
			public const string DEFAULT_COPYRIGHT_TEXT = "© 2000-{{year}} Krames StayWell, 780 Township Line Road, Yardley, PA 19067. All rights reserved. This information is not intended as a substitute for professional medical care. Always follow your healthcare professional's instructions.";
		}

		public static class User
		{
			public const string USER_UPDATE_REQUEST_CANNOT_BE_NULL = "";
		}

		public static class Application
		{
			public const int MAXIMUM_NAME_LENGTH = 100;
			public const string APPLICATION_QUERY_STRING_NAME = "applicationId";
		}

		public static class Images
		{
			public const int MAXIMUM_WIDTH = 1200;
			public const int MAXIMUM_HEIGHT = 1200;
			public const int MAXIMUM_CACHED_COUNT = 15;
		}

		public static class Content
		{
			public const string KSW_BUCKET_ORIGIN_NAME = "KSW";
			public const string KSW_BUCKET_ORIGIN_ID = "982ba75d-c444-4c6e-8242-8c3baee2698e";
			public const int MAXIMUM_SEARCH_RESULTS = 100;
			public const string ENGLISH_LANGUAGE_CODE = "en";
			public const int AUTOSAVE_EXPIRATION_IN_DAYS = 14;
		}

		public static class Collections
		{
			public const int MAXIMUM_LICENSED_COLLECTION_COUNT = 5000;
		}

		public static class Passwords
		{
			public const int MINIMUM_SALT_LENGTH = 32;
			public const int MAXIMUM_SALT_LENGTH = 64;
		}

		public static class Tasks
		{
			public static class Ids
			{
				public const string THREE_M_HDD_UPDATE = "3M HDD Update";
			}

			public const int STEP_IN_SECONDS = 7;
			public const int SMS_SESSION_EXPIRATION_IN_MONTHS = 18;
			public const int SMS_SESSION_CLEAN_STEP_SIZE = 50;
			public const int AUTHORIZATION_GRANT_CLEAN_STEP_SIZE = 50;
			public const int AUTOSAVE_CLEAN_STEP_SIZE = 50;
		}

        public static class Indexer
        {
            public const string INVALID_TYPE = "Invalid value for type";
        }

		public const int IP_SEGMENT_COUNT = 4;
		public const int MAXIMUM_TCP_PORT_VALUE = 65535;
		public const int MAXIMUM_IP_SEGMENT_VALUE = 255;

		// HTTP Referer field IS mispelled in practice, this is correct (and ridiculous)
		public const string HTTP_REFERER_HEADER_NAME = "Referer";

		public const int MAXIMUM_TASK_RESULT = 100;
		public const int MAX_NUM_TASK_THREADS = 8;
		public const int HEART_BEAT_IN_MINUTES = 1;

		public const string SMS_SESSION_TASK_NAME = "SMS Session Cleanup";
		public const string AUTH_GRANT_TASK_NAME = "Authorization Grant Cleanup";
		public const string ACCESS_TOKEN_TASK_NAME = "Access Token Cleanup";

		public const int MAXIMUM_PAGE_SIZE = 200;
		public const char QUERY_OPTION_SEPARATOR = ',';
		public const string ROLE_SEPARATOR = ",";
		public const string RIGHT_SEPARATOR_STRING = ",";
		public const char RIGHT_SEPARATOR = ',';
		public const int AUTHORIZATION_GRANT_EXPIRATION = 3;
		public const string BASIC_AUTHENTICATION_SCHEME_NAME = "Basic";
		public const string BEARER_AUTHENTICATION_SCHEME_NAME = "Bearer";
		public const string BASIC_AUTHENTICATION_SCHEME_REALM = "StayWell Login";

		public const string WEB_PORTAL_APPLICATION_ID = "29eeff76-85d0-4f70-94ed-a49072524692";
		public const string AUXILIARY_APPLICATION_ID = "0056a032-0f89-490a-a36e-67bfeeba751c";

		public const string DEFAULT_WEB_PORTAL_CLIENT_SECRET = "MxSVFBGt0W5EP-EXhAHZstWidOFSyyy.yC.R3opR4dsHboGycTy9aANpShQuybGp";
		public const string DEFAULT_AUXILIARY_CLIENT_SECRET = "PbQf6ItYwCP0EqptsnB2BcCksBDAba3689lQdLOvRd0nD5aiiwpfVL5PdgxzwU6W";
		public const string WEB_PORTAL_APPLICATION_NAME = "API Administration Portal";
		public const string AUXILIARY_APPLICATION_NAME = "Auxiliary Service Application";

		public const int MAXIMUM_SMS_KEYWORD_LENGTH = 16;
		public const int MINIMUM_SMS_KEYWORD_LENGTH = 3;

		public const int MAXIMUM_SMS_BODY_LENGTH = 960;
		public const int MAXIMUM_SMS_MESSAGE_LENGTH = 160;

		public const string INITIAL_SHORT_CODE = "98517";

		public const string FEATURE_NOT_AVAILABLE = "Feature not available";

		public const string HELP_SIGNATURE = "Txt HELP for help.";

		public const string SMS_HELP_MESSAGE = "Krames StayWell (KSW): Providing relevant, timely, health edu.\nMsg&Data Rates May Apply. 1 msg/day.\nContact: admin.kramesstaywell.com/help. Reply STOP to cancel";

		public const string STOP_KEYWORD_MESSAGE = "You will receive no more messages from KSW: {0} program.";
		public const string STOP_CATCHALL_MESSAGE = "You will receive no more messages.";

		public const string NO_SUBSCRIPTIONS = "You are not currently subscribed to any KSW programs.\n\nYou will receive no more messages";
		public const string MSG_DATA_RATE_DISCLAIMER = "\n\nMsg&Data Rates May Apply";

	    
	}
}
