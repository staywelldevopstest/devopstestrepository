﻿namespace KswApi.Logic.Framework.Constants
{
    public static class OAuthGrantType
    {
        public const string AUTHORIZATION_CODE = "authorization_code";
        public const string CLIENT_CREDENTIALS = "client_credentials";
        public const string REFRESH_TOKEN = "refresh_token";
    }
}
