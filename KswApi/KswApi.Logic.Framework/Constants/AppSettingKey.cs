﻿namespace KswApi.Logic.Framework.Constants
{
	public static class AppSettingKey
	{
		public const string CACHE_PREFIX = "KswApiCachePrefix";
		public const string QUEUE_NAME = "KswApiQueue";
	}
}
