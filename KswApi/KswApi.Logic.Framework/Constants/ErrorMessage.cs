﻿namespace KswApi.Logic.Framework.Constants
{
	public static class ErrorMessage
	{
		public static class Common
		{
			public const string OFFSET_LESS_THAN_ZERO = "Offset must be zero or greater than zero.";
			public const string COUNT_LESS_THAN_ZERO = "Count must be zero or greater than zero.";
			public const string COUNT_TOO_HIGH = "Cannot specify more than {0} results at a time.";

			public const string MISSING_MESSAGE_BODY = "The request did not contain a valid body.";
			public const string ITEMS_NOT_SPECIFIED = "Items must be specified.";
			public const string NULL_ITEMS = "Items cannot be null.";

			public const string INVALID_IDENTIFIER = "Invalid identifier.";
			public const string NOT_SUPPORTED = "The operation is not supported.";
			public const string INSUFFICIENT_RIGHTS = "Insufficient rights to perform the operation.";
			public const string SLUG_REQUIRED = "Slug is required.";
			public const string SLUG_OR_ID_REQUIRED = "Slug or ID is required.";
			public const string MODIFYING_SLUG = "Slug cannot be modified after the being set.";
			public const string DUPLICATE_SLUG = "The specified slug is already in use.";
		}

		public static class License
		{
			public const string APPLICATION_NOT_FOUND = "Application was not found: {0}.";
			public const string INVALID_LICENSE = "The license is invalid.";
		}

		public static class Authorization
		{
			public const string UNAUTHORIZED = "Login is required.";
			public const string MISSING_JSONP_REFERER = "The operation requires a referrer to be present.";
			public const string JSONP_NOT_SUPPORTED = "The application does not allow access via JSONP.";
			public const string JSONP_APPLICATION_NOT_FOUND = "The application was not found.";
			public const string INVALID_JSONP_SITE = "The referring website does not have rights to access this resource.";
			public const string INVALID_LOGIN = "Invalid user name, invalid password, or disabled/locked account.";
			public const string OBSOLETE_AUTHORIZATION_GRANT = "The authorization grant is obsolete.";
			public const string INVALID_LOGIN_TYPE = "The current login is invalid for this request.";
		}

		public static class Passwords
		{
			public const string MISSING_TOKEN = "Password token must be specified.";
			public const string MISSING_PASSWORD = "A password must be specified.";
			public const string MISSING_PASSWORD_CONFIRMATION = "Password confirmation must be specified.";
			public const string PASSWORD_AND_CONFIRMATION_NOT_EQUAL = "The password and password confirmation must be the same.";
			public const string INVALID_TOKEN = "The password request is invalid, expired, or has already been used.";
			public const string DISABLED_USER = "The account has been disabled.";
			public const string PASSWORD_CHARACTER_REQUIREMENTS = "Passwords must contain an upper-case, lower-case, numeric, and punctuation character.";
			public const string PASSWORD_LENGTH_REQUIREMENTS = "Passwords must be at least {0} characters in length.";
			public const string PASSWORD_CONTAINING_NAME = "Passwords cannot contain any part of your name.";
			public const string PASSWORD_CONTAINING_EMAIL = "Passwords cannot contain any part of your username email address.";
		}

		public  static class Bucket
		{
			public const string BUCKET_REQUIRED = "Bucket is required.";
			public const string NO_BUCKET_PERMISSIONS = "Bucket is not in license.";
			public const string MODIFYING_SLUG = "Bucket slug cannot be modified after the bucket has been created.";
			public const string NOT_FOUND_PARAMETERIZED = "Bucket {0} not found.";
		}

		public static class Content
		{
			public const string MISSING_CONTENT_TITLE = "Content title is required.";
			public const string CONTENT_NOT_FOUND = "Content was not found.";
			public const string MASTER_NOT_FOUND = "Content master was not found.";
			public const string ORPHANED_CONTENT_NO_BUCKET = "Content has been orphaned, no valid bucket found.";
			public const string ORPHANED_CONTENT_NO_CORE = "Content has been orphaned, no valid core found.";
			public const string DUPLICATE_LANGUAGE = "The {0} language version of the content already exists.";
			public const string MASTER_IDS_DO_NOT_MATCH = "Core master ID does not match a version declaring itself to be master.";
			public const string MISSING_TYPE = "Content type is required.";
			public const string MODIFYING_TYPE = "Content type can only be set when the content is created.";
			public const string SEARCH_RESULT_COUNT_TOO_LARGE = "Cannot specify more than {0} search results at a time.";

			public const string NAME_REQUIRED = "Name is required.";
			public const string ORIGIN_REQUIRED = "Origin ID is required.";
			public const string BUCKET_NOT_FOUND = "Content bucket was not found.";
			public const string ORPHANED_BUCKET_NO_ORIGIN = "Bucket specifies an invalid origin.";
			public const string DUPLICATE_SLUG = "The specified slug is already in use.";
			public const string INVALID_SLUG = "Slugs may only contain letters, numbers, dashes, and underscores.";
			public const string BUCKET_TYPE_CANNOT_BE_CHANGED = "Bucket type may not be changed.";
			public const string ORIGIN_CANNOT_BE_CHANGED = "Bucket origin may not be changed.";
			public const string SIBLING_BUCKET_MUST_MATCH = "Bucket ID of new language version must match the bucket ID of the original.";
			public const string INVALID_ORIGIN = "The origin ID was not found.";
			public const string INVALID_BUCKET = "The bucket was not found.";
			public const string TYPE_REQUIRED = "Type is required.";
			public const string BUCKET_REQUIRED = "Content bucket is required.";
			public const string MISSING_REQUIRED_SEGMENT = "Content segment is required: {0}.";
			public const string DUPLICATE_SEGMENT = "Cannot include same segment multiple times.";
			public const string DUPLICATE_SEGMENT_ID = "Cannot include same segment ID multiple times.";
			public const string INVALID_SEGMENT = "Invalid segment specified.";
			public const string SEGMENT_REQUIRED = "All content segments must have a bucket segment ID assigned.";
			public const string SEGMENT_NAME_REQUIRED = "All content segments must have a name.";
			public const string SEGMENT_SLUG_REQUIRED = "All content segments must have a slug.";
			public const string SEGMENT_ID_CANNOT_BE_SPECIFIED_ON_CREATION = "Segment ID cannot be specified on bucket creation.";
			public const string SEGMENT_SLUG_CANNOT_BE_MODIFIED = "Segment slug cannot be modified.";
			public const string DUPLICATED_SEGMENT_SLUG = "Two segments have names that result in duplicate slugs.";
			public const string INVALID_SEGMENT_SLUG = "A segment has a name that results in an invalid slug.";

			public const string MODIFYING_CONTENT_BUCKET = "Content bucket cannot be modified after the content has been created.";
			public const string MODIFYING_CONTENT_SLUG = "Content slug cannot be modified after the content has been created.";
			public const string MODIFYING_CONTENT_LANGUAGE = "Content language cannot be modified after the content has been created.";

			public const string INVALID_GUNNING_FOG_READING_LEVEL = "Invalid Gunning-Fog reading level.";
			public const string INVALID_FLESCH_KINCAID_READING_LEVEL = "Invalid Flesch-Kincaid reading level.";
			public const string DUPLICATE_AGE_CATEGORY = "Cannot specify an age category multiple times.";
			public const string AGE_CATEGORY_REQUIRED = "At least one age category is required.";
			public const string GENDER_REQUIRED = "Gender is required.";
			public const string EMPTY_SEGMENT = "One of the segments was null.";
			public const string IDENTICAL_INVERTED_TITLE = "The title and inverted title cannot be the same.";
			public const string INVALID_ONLINE_ORIGINATING_SOURCE = "One or more online originating sources are invalid.";
			public const string INVALID_ONLINE_ORIGINATING_SOURCE_URI = "One or more online originating sources specifies an invalid URI.";
			public const string INVALID_RECOMMENDED_SITE = "One or more recommended sites are invalid.";
			public const string INVALID_RECOMMENDED_SITE_URI = "One or more recommended sites specifies an invalid URI.";
			public const string BODY_TOO_LONG = "The content body is greater than the bucket maximum of {0} characters.";
			public const string INVALID_COPYRIGHT_ID = "The specified copyright is invalid.";

			public const string ID_REQUIRED = "ID is required.";
			public const string CONTENT_ID_REQUIRED = "Content ID is required.";

			public const string EMPTY_LANGUAGE = "Language is required.";
			public const string UNRECOGNIZED_LANGUAGE = "Unrecognized language code: {0}.";

			public const string THIRD_PARTY_TAXONOMY_VALUES_CANNOT_BE_DELETED = "Third party taxonomy values cannot be deleted.";
			public const string INVALID_TAXONOMY_VALUE = "Taxonomy value is invalid.";
			public const string INVALID_TAXONOMY_TYPE = "Taxonomy type is invalid.";
			public const string TAXONOMIES_ONLY_CHANGED_ON_MASTER = "Taxonomies may only be changed on master content";

			public const string INVALID_TYPES = "Invalid types.";

			public const string MUST_PUBLISH_MASTER_FIRST = "The master version must be published first.";
			public const string INVALID_CORE_ID = "Invalid core ID.";

			public const string PARAMETERIZED_NOT_FOUND = "Content {0}/{1} was not found.";

			public const string INVALID_CUSTOM_ATTRIBUTE_NAME = "Custom attribute name is invalid.";
			public const string DUPLICATE_CUSTOM_ATTRIBUTE_NAME = "Cannot specify a custom attribute multiple times.";
			public const string CUSTOM_ATTRIBUTE_MUST_HAVE_VALUE = "Cannot specify a custom attribute without a value.";
			public const string INVALID_CUSTOM_ATTRIBUTE_VALUE = "Custom attribute value is invalid.";
		}

		public static class Images
		{
			public const string INVALID_IMAGE = "Invalid image file.";
			public const string IMAGE_NOT_FOUND = "Image was not found.";
			public const string IMAGE_FILE_NOT_FOUND = "Image file was not found.";
			public const string TITLE_IS_REQUIRED = "Title is required.";
			public const string SLUG_IS_REQUIRED = "Slug is required.";
			public const string INVALID_DIMENSIONS = "Invalid image dimensions.";
			public const string INVALID_REPLACEMENT = "Replacement image was not found.";
			public const string REPLACEMENT_REUSE = "Replacement image is already used.";
			public const string EMPTY_MESSAGE = "Request body was empty or invalid.";
		}

		public static class ContentModule
		{
			public const string NO_ACCESS_TO_BUCKET = "The license does not have access to the specified bucket.";
			public const string NO_CONTENT_MODULE = "The license does not contain the content module.";
			public const string NO_LICENSE_ID = "License Id must be provided and cannot be null or empty";
			public const string NO_BUCKET_ID = "Bucket Id must be provided and cannot be null or empty";

			public const string NOT_ASSOCIATED_WITH_LICENSE = "The license does not contain the Content Module.";
			public const string NO_BUCKET_ID_LIST_SPECIFIED = "No bucket list of ids was specified.";
			public const string DUPLICATE_BUCKET = "A bucket cannot be specified multiple times.";
		}

		public static class Administration
		{
			public const string MISSING_EMAIL = "Please specify an e-mail address.";
			public const string INVALID_EMAIL = "Please specify a valid e-mail address.";
			public const string CLIENT_USER_NOT_FOUND = "The user was not found.";
			public const string CLIENT_REQUIRED = "Client is required.";
			public const string INVALID_CLIENT_ID = "Invalid client ID.";
			public const string INVALID_JSONP_WEBSITE = "Invalid JSONP Website.";
		}

		public static class Copyrights
		{
			public const string COPYRIGHT_ID_MUST_BE_SPECIFIED = "Copyright id must be specified.";
			public const string COPYRIGHT_NOT_FOUND = "Copyright could not be found.";
			public const string COPYRIGHT_CANNOT_BE_DELETED = "Copyright cannot be deleted.";
			public const string COPYRIGHT_VALUE_CANNOT_BE_NULL_OR_EMPTY = "Cannot set value of copyright to null or empty.";
		}

		public static class User
		{
			public const string USER_UPDATE_REQUEST_CANNOT_BE_NULL = "User Update Request cannot be nul or empty.";
			public const string ANOTHER_USER_HAS_SPECIFIED_EMAIL = "Another user already has that email, please use a different email.";
			public const string FIRST_NAME_CANNOT_BE_NULL_OR_EMPTY = "First name cannot be null or empty.";
			public const string EMAIL_ADDRESS_CANNOT_BE_NULL_OR_EMPTY = "Email address cannot be null or empty.";
			public const string USER_ID_CANNOT_BE_NULL = "User Id Cannot be null.";
			public const string USER_NOT_FOUND = "User cannot be found.";

			public const string UNSUPPORTED_USER_TYPE = "User type not supported.";
		}

		public static class OAuth
		{
			public const string RESPONSE_TYPE_MUST_BE_CODE = "Response type must be \"code\".";
			public const string EXPIRED_ACCESS_TOKEN = "Access token has expired.";
			public const string ACCESS_TOKEN_REQUIRED = "Access token is required.";
			public const string MISSING_APPLICATION_ID = "Application ID is missing.";
			public const string ACCESS_TOKEN_NOT_FOUND = "Access token was not found.";
		}

		public static class TaxonomyType
		{
			public const string TAXONOMY_TYPE_NOT_FOUND = "Taxonomy Type was not found.";
		}

		public static class HierarchicalTaxonomy
		{
			public const string NOT_FOUND = "Hierarchical Taxonomy was not found.";
		}

		public static class Serviceline
		{
			public const string AUDIENCE_REQUIRED = "Audience is required.";
			public const string SERVICELINE_REQUIRED = "Serviceline is required.";
			public const string INVALID_SERVICE_LINE = "Invalid service line specified.";
			public const string INVALID_SERVICE_LINE_TYPE = "Invalid type specified for service line.";
		}

		public static class Collection
		{
			public const string COLLECTION_NOT_FOUND = "Collection was not found.";
			public const string MODIFYING_SLUG = "Collection slug cannot be modified after the collection has been created.";
			public const string NULL_COLLECTION = "Collection cannot be null.";
			public const string ONE_OR_MORE_COLLECTIONS_NOT_FOUND = "One or more collections were not found.";

			public const string INVALID_TYPE = "Collection member type {0} has not been implemented yet.";
			public const string UNRECOGNIZED_TYPE = "Unrecognized collection member type.";
			public const string NULL_COLLECTION_ITEM = "Collection items cannot be null.";
			public const string ID_ON_CREATE_TOPIC = "Slug and ID are auto-generated and cannot be set when creating a topic.";

			public const string MISSING_TOPIC_SLUG = "Topic slug cannot be null or empty on update.";

			public const string DUPLICATE_TOPIC = "Topic cannot be included multiple times in collection.";
			public const string TOPIC_NOT_FOUND = "One or more topics were not found.";
			public const string PARAMETERIZED_TOPIC_NOT_FOUND = "topic {0} was not found.";
			public const string TYPE_NOT_SUPPORTED = "Item type is not defined or is not yet supported.";
			public const string ITEM_TITLE_REQUIRED = "Item title is required.";

			public const string CONTENT_ID_REQUIRED = "Content ID cannot be null or empty for content added to collection.";
			public const string BUCKET_ID_REQUIRED = "Bucket ID  cannot be null or empty for content added to collection.";
			public const string NO_ITEM_REFERENCE = "Item reference must be specified for all collection items.";
		}
	}
}
