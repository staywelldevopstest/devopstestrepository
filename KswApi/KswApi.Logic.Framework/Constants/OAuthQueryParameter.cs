﻿namespace KswApi.Logic.Framework.Constants
{
	public static class OAuthQueryParameter
	{
		public const string ERROR = "error";
		public const string ERROR_DESCRIPTION = "error_description";
		public const string AUTHORIZATION_CODE = "code";
		public const string STATE = "state";
	}
}
