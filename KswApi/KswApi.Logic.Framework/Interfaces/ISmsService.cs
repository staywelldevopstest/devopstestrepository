﻿
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.SmsGateway;

namespace KswApi.Logic.Framework.Interfaces
{
	public interface ISmsService
	{
		void SendMessage(string to, string from, string body);
		void SendMessage(string to, string from, string body, string statusCallBackUrl);
		IncomingMessage GetMessage(string messageSid);
		AvailableLongCodeList GetAvailableNumbers(int offset, int count, string filter, SmsLongCodeFilterType filterType);
		void ReleaseLongCode(string number);
		void PurchaseLongCode(string number);
	}
}
