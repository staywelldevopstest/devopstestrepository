﻿using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.SmsGateway;

namespace KswApi.Logic.Framework.Interfaces
{
	public interface ISmsMessageForwarder
	{
		void SendMessage(MessageFormat format, string address, OutgoingMessage message);
	}
}
