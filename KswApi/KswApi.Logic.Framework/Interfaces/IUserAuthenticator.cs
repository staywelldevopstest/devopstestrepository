﻿using KswApi.Common.Objects;

namespace KswApi.Logic.Framework.Interfaces
{
	public interface IUserAuthenticator
	{
		UserAuthenticationDetails Authenticate(string userName, string password);
	}
}
