﻿
namespace KswApi.Logic.Framework.Interfaces
{
	public interface ITaskQueue
	{
		bool Test();
	}
}
