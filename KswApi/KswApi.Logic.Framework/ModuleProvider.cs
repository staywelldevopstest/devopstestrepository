﻿using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Logic.Framework.Objects;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace KswApi.Logic.Framework
{
	public static class ModuleProvider
	{
		private static Dictionary<ModuleType, ServiceModule> _modulesByType;
		private static IList<ServiceModule> _moduleList;

		public static void InitializeModules()
		{
			List<ServiceModule> modules = new List<ServiceModule>
								{
									new ServiceModule
									{ 
										Type = ModuleType.SmsGateway,
										Name = "SMS Gateway",
										DefaultScope = new List<string> { "SMS_Send" }
									},
									new ServiceModule
									{
										Type = ModuleType.Content,
										Name = "Content",
										DefaultScope = new List<string> {"Read_Content"}
									},
									new ServiceModule
									{
										Type = ModuleType.ManageContent,
										Name = "Content Creation",
										DefaultScope = new List<string> { "Read_Content", "Manage_Content" }
									}
								};

			_moduleList = modules.OrderBy(item => item.Name).ToList().AsReadOnly();
			_modulesByType = new Dictionary<ModuleType, ServiceModule>();

			foreach (ServiceModule module in modules)
			{
				_modulesByType[module.Type] = module;
			}
		}

		public static ServiceModule GetModule(ModuleType type)
		{
			ServiceModule module;
			if (!_modulesByType.TryGetValue(type, out module))
				throw new ServiceException(HttpStatusCode.BadRequest, "Module does not exist.");

			return module;
		}

		public static IList<ServiceModule> GetModules()
		{
			return _moduleList;
		}
	}
}
