﻿using System;
using System.Collections.Generic;
using System.Drawing.Imaging;
using KswApi.Interface.Objects;

namespace KswApi.Logic.Framework.Providers
{
	public static class ImageTypeProvider
	{
		private static readonly Dictionary<Guid, ImageFormatType> ImageTypeLookup = new Dictionary<Guid, ImageFormatType>();

		static ImageTypeProvider()
		{
			ImageTypeLookup.Add(ImageFormat.Jpeg.Guid, ImageFormatType.Jpg);
			ImageTypeLookup.Add(ImageFormat.Png.Guid, ImageFormatType.Png);
			ImageTypeLookup.Add(ImageFormat.Tiff.Guid, ImageFormatType.Tiff);
			ImageTypeLookup.Add(ImageFormat.Gif.Guid, ImageFormatType.Gif);
			ImageTypeLookup.Add(ImageFormat.Bmp.Guid, ImageFormatType.Bmp);
		}

		public static ImageFormatType GetType(ImageFormat format)
		{
			ImageFormatType result;
			ImageTypeLookup.TryGetValue(format.Guid, out result);
			return result;
		}

		public static ImageFormatType GetTypeFromExtension(string extension)
		{
			if (string.IsNullOrEmpty(extension))
				return ImageFormatType.None;

			switch (extension.ToLower())
			{
				case "jpg":
				case "jpeg":
					return  ImageFormatType.Jpg;
				case "png":
					return ImageFormatType.Png;
				case "gif":
					return ImageFormatType.Gif;
				case "tiff":
				case "tif":
					return ImageFormatType.Tiff;
				case "bmp":
					return ImageFormatType.Bmp;
				default:
					return ImageFormatType.None;
			}
		}

		public static ImageFormatType GetTypeFromMime(string mime)
		{
			if (string.IsNullOrEmpty(mime))
				return ImageFormatType.None;

			switch (mime)
			{
				case "image/jpeg":
				case "image/pjpeg":
					return ImageFormatType.Jpg;
				case "image/png":
					return ImageFormatType.Png;
				case "image/gif":
					return ImageFormatType.Gif;
				case "image/tiff":
					return ImageFormatType.Tiff;
				case "image/bmp":
					return ImageFormatType.Bmp;
				default:
					return ImageFormatType.None;
			}
		}

		public static string GetMime(ImageFormatType type)
		{
			switch (type)
			{
				case ImageFormatType.Jpg:
					return "image/jpeg";
				case ImageFormatType.Png:
					return "image/png";
				case ImageFormatType.Tiff:
					return "image/tiff";
				case ImageFormatType.Gif:
					return "image/gif";
				case ImageFormatType.Bmp:
					return "image/bmp";
				default:
					throw new ArgumentOutOfRangeException("type");
			}
		}

		public static ImageFormat GetFormat(ImageFormatType type)
		{
			switch (type)
			{
				case ImageFormatType.Jpg:
					return ImageFormat.Jpeg;
				case ImageFormatType.Png:
					return ImageFormat.Png;
				case ImageFormatType.Tiff:
					return ImageFormat.Tiff;
				case ImageFormatType.Gif:
					return ImageFormat.Gif;
				case ImageFormatType.Bmp:
					return ImageFormat.Bmp;
				default:
					throw new ArgumentOutOfRangeException("type");
			}
		}
	}
}
