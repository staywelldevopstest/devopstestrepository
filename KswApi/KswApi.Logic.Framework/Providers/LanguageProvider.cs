﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.Framework.Constants;

namespace KswApi.Logic.Framework.Providers
{
	public static class LanguageProvider
	{
		// We're using ISO 639-1 codes because they're generally more recongized.
		// Most people know that "en" means english, and don't think of "eng".
		// We're using ISO 639-3 codes for any langues that don't exist in ISO 639-1

		private static readonly List<string> Languages = new List<string> { "en", "es", "zh", "vi", "ru", "hy", "fa", "km", "ko", "hmn", "tl", "de", "fr", "it", "so", "ar", "pt", "pl", "ab", "aa", "af", "ak", "sq", "am", "an", "as", "av", "ae", "ay", "az", "bm", "ba", "eu", "be", "bn", "bi", "bs", "br", "bg", "my", "ca", "ch", "ce", "ny", "cv", "kw", "co", "cr", "hr", "cs", "da", "dv", "nl", "dz", "eo", "et", "ee", "fo", "fj", "fi", "ff", "gl", "ka", "el", "gn", "gu", "ht", "ha", "he", "hz", "hi", "ho", "hu", "ia", "id", "ie", "ga", "ig", "ik", "io", "is", "iu", "ja", "jv", "kl", "kn", "kr", "ks", "kk", "ki", "rw", "ky", "kv", "kg", "ku", "kj", "la", "lb", "lg", "li", "ln", "lo", "lt", "lu", "lv", "gv", "mk", "mg", "ms", "ml", "mt", "mi", "mr", "mh", "mn", "na", "nv", "nb", "nd", "ne", "ng", "nn", "no", "ii", "nr", "oc", "oj", "cu", "om", "or", "os", "pa", "pi", "ps", "qu", "rm", "rn", "ro", "sa", "sc", "sd", "se", "sm", "sg", "sr", "gd", "sn", "si", "sk", "sl", "st", "su", "sw", "ss", "sv", "ta", "te", "tg", "th", "ti", "bo", "tk", "tn", "to", "tr", "ts", "tt", "tw", "ty", "ug", "uk", "ur", "uz", "ve", "vo", "wa", "cy", "wo", "fy", "xh", "yi", "yo", "za", "zu" };

		public static List<Language> GetAll()
		{
			return Languages.Select(item => new Language
											 {
												 Code = item,
												 Name = GetName(item),
												 Direction = GetDirection(item)
											 }).ToList();
		}

		public static Language GetLanguage(string code, bool validate = false)
		{
			return new Language
				   {
					   Code = code,
					   Name = GetName(code),
					   Direction = GetDirection(code)
				   };
		}

		public static string GetStandardCode(string code, bool validate = false)
		{
			if (string.IsNullOrEmpty(code))
			{
				if (validate)
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.EMPTY_LANGUAGE);

				return code;
			}


			code = code.ToLower();

			switch (code)
			{
				#region ISO 639-1

				case "ab":
				case "abk":
				case "abks":
					return "ab";

				case "aa":
				case "aar":
				case "aars":
					return "aa";

				case "af":
				case "afr":
				case "afrs":
					return "af";

				case "ak":
				case "aka":
					return "ak";

				case "sq":
				case "alb":
				case "sqi":
					return "sq";

				case "am":
				case "amh":
					return "am";

				case "ar":
				case "ara":
					return "ar";

				case "an":
				case "arg":
					return "an";

				case "hy":
				case "arm":
				case "hye":
					return "hy";

				case "as":
				case "asm":
					return "as";

				case "av":
				case "ava":
					return "av";

				case "ae":
				case "ave":
					return "ae";

				case "ay":
				case "aym":
					return "ay";

				case "az":
				case "aze":
					return "az";

				case "bm":
				case "bam":
					return "bm";

				case "ba":
				case "bak":
					return "ba";

				case "eu":
				case "baq":
				case "eus":
					return "eu";

				case "be":
				case "bel":
					return "be";

				case "bn":
				case "ben":
					return "bn";

				case "bh":
				case "bih":
				case "bi":
				case "bis":
					return "bi";

				case "bs":
				case "bos":
				case "boss":
					return "bs";

				case "br":
				case "bre":
					return "br";

				case "bg":
				case "bul":
				case "buls":
					return "bg";

				case "my":
				case "bur":
				case "mya":
					return "my";

				case "ca":
				case "cat":
					return "ca";

				case "ch":
				case "cha":
					return "ch";

				case "ce":
				case "che":
					return "ce";

				case "ny":
				case "nya":
					return "ny";

				case "zh":
				case "chi":
				case "zho":
					return "zh";

				case "cv":
				case "chv":
					return "cv";

				case "kw":
				case "cor":
					return "kw";

				case "co":
				case "cos":
					return "co";

				case "cr":
				case "cre":
					return "cr";

				case "hr":
				case "hrv":
					return "hr";

				case "cs":
				case "cze":
				case "ces":
					return "cs";

				case "da":
				case "dan":
					return "da";

				case "dv":
				case "div":
					return "dv";

				case "nl":
				case "dut":
				case "nld":
					return "nl";

				case "dz":
				case "dzo":
					return "dz";

				case "en":
				case "eng":
				case "engs":
					return "en";

				case "eo":
				case "epo":
					return "eo";

				case "et":
				case "est":
					return "et";

				case "ee":
				case "ewe":
					return "ee";

				case "fo":
				case "fao":
					return "fo";

				case "fj":
				case "fij":
					return "fj";

				case "fi":
				case "fin":
					return "fi";

				case "fr":
				case "fre":
				case "fra":
				case "fras":
					return "fr";

				case "ff":
				case "ful":
					return "ff";

				case "gl":
				case "glg":
					return "gl";

				case "ka":
				case "geo":
				case "kat":
					return "ka";

				case "de":
				case "ger":
				case "deu":
				case "deus":
					return "de";

				case "el":
				case "gre":
				case "ell":
				case "ells":
					return "el";

				case "gn":
				case "grn":
					return "gn";

				case "gu":
				case "guj":
					return "gu";

				case "ht":
				case "hat":
					return "ht";

				case "ha":
				case "hau":
					return "ha";

				case "he":
				case "heb":
					return "he";

				case "hz":
				case "her":
					return "hz";

				case "hi":
				case "hin":
				case "hins":
					return "hi";

				case "ho":
				case "hmo":
					return "ho";

				case "hu":
				case "hun":
					return "hu";

				case "ia":
				case "ina":
					return "ia";

				case "id":
				case "ind":
					return "id";

				case "ie":
				case "ile":
					return "ie";

				case "ga":
				case "gle":
					return "ga";

				case "ig":
				case "ibo":
					return "ig";

				case "ik":
				case "ipk":
					return "ik";

				case "io":
				case "ido":
				case "idos":
					return "io";

				case "is":
				case "ice":
				case "isl":
					return "is";

				case "it":
				case "ita":
				case "itas":
					return "it";

				case "iu":
				case "iku":
					return "iu";

				case "ja":
				case "jpn":
					return "ja";

				case "jv":
				case "jav":
					return "jv";

				case "kl":
				case "kal":
					return "kl";

				case "kn":
				case "kan":
					return "kn";

				case "kr":
				case "kau":
					return "kr";

				case "ks":
				case "kas":
					return "ks";

				case "kk":
				case "kaz":
					return "kk";

				case "km":
				case "khm":
					return "km";

				case "ki":
				case "kik":
					return "ki";

				case "rw":
				case "kin":
					return "rw";

				case "ky":
				case "kir":
					return "ky";

				case "kv":
				case "kom":
					return "kv";

				case "kg":
				case "kon":
					return "kg";

				case "ko":
				case "kor":
					return "ko";

				case "ku":
				case "kur":
					return "ku";

				case "kj":
				case "kua":
					return "kj";

				case "la":
				case "lat":
				case "lats":
					return "la";

				case "lb":
				case "ltz":
					return "lb";

				case "lg":
				case "lug":
					return "lg";

				case "li":
				case "lim":
					return "li";

				case "ln":
				case "lin":
					return "ln";

				case "lo":
				case "lao":
					return "lo";

				case "lt":
				case "lit":
					return "lt";

				case "lu":
				case "lub":
					return "lu";

				case "lv":
				case "lav":
					return "lv";

				case "gv":
				case "glv":
					return "gv";

				case "mk":
				case "mac":
				case "mkd":
					return "mk";

				case "mg":
				case "mlg":
					return "mg";

				case "ms":
				case "may":
				case "msa":
					return "ms";

				case "ml":
				case "mal":
					return "ml";

				case "mt":
				case "mlt":
					return "mt";

				case "mi":
				case "mao":
				case "mri":
					return "mi";

				case "mr":
				case "mar":
					return "mr";

				case "mh":
				case "mah":
					return "mh";

				case "mn":
				case "mon":
					return "mn";

				case "na":
				case "nau":
					return "na";

				case "nv":
				case "nav":
					return "nv";

				case "nb":
				case "nob":
					return "nb";

				case "nd":
				case "nde":
					return "nd";

				case "ne":
				case "nep":
					return "ne";

				case "ng":
				case "ndo":
					return "ng";

				case "nn":
				case "nno":
					return "nn";

				case "no":
				case "nor":
					return "no";

				case "ii":
				case "iii":
					return "ii";

				case "nr":
				case "nbl":
					return "nr";

				case "oc":
				case "oci":
					return "oc";

				case "oj":
				case "oji":
					return "oj";

				case "cu":
				case "chu":
					return "cu";

				case "om":
				case "orm":
					return "om";

				case "or":
				case "ori":
					return "or";

				case "os":
				case "oss":
					return "os";

				case "pa":
				case "pan":
					return "pa";

				case "pi":
				case "pli":
					return "pi";

				case "fa":
				case "per":
				case "fas":
					return "fa";

				case "pl":
				case "pol":
				case "pols":
					return "pl";

				case "ps":
				case "pus":
					return "ps";

				case "pt":
				case "por":
					return "pt";

				case "qu":
				case "que":
					return "qu";

				case "rm":
				case "roh":
					return "rm";

				case "rn":
				case "run":
					return "rn";

				case "ro":
				case "rum":
				case "ron":
					return "ro";

				case "ru":
				case "rus":
					return "ru";

				case "sa":
				case "san":
					return "sa";

				case "sc":
				case "srd":
					return "sc";

				case "sd":
				case "snd":
					return "sd";

				case "se":
				case "sme":
					return "se";

				case "sm":
				case "smo":
					return "sm";

				case "sg":
				case "sag":
					return "sg";

				case "sr":
				case "srp":
					return "sr";

				case "gd":
				case "gla":
					return "gd";

				case "sn":
				case "sna":
					return "sn";

				case "si":
				case "sin":
					return "si";

				case "sk":
				case "slo":
				case "slk":
					return "sk";

				case "sl":
				case "slv":
					return "sl";

				case "so":
				case "som":
					return "so";

				case "st":
				case "sot":
					return "st";

				case "es":
				case "spa":
					return "es";

				case "su":
				case "sun":
					return "su";

				case "sw":
				case "swa":
					return "sw";

				case "ss":
				case "ssw":
					return "ss";

				case "sv":
				case "swe":
					return "sv";

				case "ta":
				case "tam":
					return "ta";

				case "te":
				case "tel":
					return "te";

				case "tg":
				case "tgk":
					return "tg";

				case "th":
				case "tha":
					return "th";

				case "ti":
				case "tir":
					return "ti";

				case "bo":
				case "tib":
				case "bod":
					return "bo";

				case "tk":
				case "tuk":
					return "tk";

				case "tl":
				case "tgl":
					return "tl";

				case "tn":
				case "tsn":
					return "tn";

				case "to":
				case "ton":
					return "to";

				case "tr":
				case "tur":
					return "tr";

				case "ts":
				case "tso":
					return "ts";

				case "tt":
				case "tat":
					return "tt";

				case "tw":
				case "twi":
					return "tw";

				case "ty":
				case "tah":
					return "ty";

				case "ug":
				case "uig":
					return "ug";

				case "uk":
				case "ukr":
					return "uk";

				case "ur":
				case "urd":
					return "ur";

				case "uz":
				case "uzb":
					return "uz";

				case "ve":
				case "ven":
					return "ve";

				case "vi":
				case "vie":
					return "vi";

				case "vo":
				case "vol":
					return "vo";

				case "wa":
				case "wln":
					return "wa";

				case "cy":
				case "wel":
				case "cym":
					return "cy";

				case "wo":
				case "wol":
					return "wo";

				case "fy":
				case "fry":
					return "fy";

				case "xh":
				case "xho":
					return "xh";

				case "yi":
				case "yid":
					return "yi";

				case "yo":
				case "yor":
					return "yo";

				case "za":
				case "zha":
					return "za";

				case "zu":
				case "zul":
					return "zu";

				#endregion

				#region ISO 639-3 Extensions

				// Note: Only use ISO 639-3 Extensions where there is no 639-1 alternative
				case "hmn":
					return "hmn";

				#endregion

				default:
					if (validate)
						throw new ServiceException(HttpStatusCode.BadRequest, string.Format(ErrorMessage.Content.UNRECOGNIZED_LANGUAGE, code));

					return code;
			}
		}

		public static string GetName(string code, bool validate = false)
		{
			switch (code)
			{
				case "ab":
					return "Abkhaz";

				case "aa":
					return "Afar";

				case "af":
					return "Afrikaans";

				case "ak":
					return "Akan";

				case "sq":
					return "Albanian";

				case "am":
					return "Amharic";

				case "ar":
					return "Arabic";

				case "an":
					return "Aragonese";

				case "hy":
					return "Armenian";

				case "as":
					return "Assamese";

				case "av":
					return "Avaric";

				case "ae":
					return "Avestan";

				case "ay":
					return "Aymara";

				case "az":
					return "Azerbaijani";

				case "bm":
					return "Bambara";

				case "ba":
					return "Bashkir";

				case "eu":
					return "Basque";

				case "be":
					return "Belarusian";

				case "bn":
					return "Bengali";

				case "bh":
					return "Bihari";

				case "bi":
					return "Bislama";

				case "bs":
					return "Bosnian";

				case "br":
					return "Breton";

				case "bg":
					return "Bulgarian";

				case "my":
					return "Burmese";

				case "ca":
					return "Catalan";

				case "ch":
					return "Chamorro";

				case "ce":
					return "Chechen";

				case "ny":
					return "Chichewa";

				case "zh":
					return "Chinese";

				case "cv":
					return "Chuvash";

				case "kw":
					return "Cornish";

				case "co":
					return "Corsican";

				case "cr":
					return "Cree";

				case "hr":
					return "Croatian";

				case "cs":
					return "Czech";

				case "da":
					return "Danish";

				case "dv":
					return "Divehi";

				case "nl":
					return "Dutch";

				case "dz":
					return "Dzongkha";

				case "en":
					return "English";

				case "eo":
					return "Esperanto";

				case "et":
					return "Estonian";

				case "ee":
					return "Ewe";

				case "fo":
					return "Faroese";

				case "fj":
					return "Fijian";

				case "fi":
					return "Finnish";

				case "fr":
					return "French";

				case "ff":
					return "Fula";

				case "gl":
					return "Galician";

				case "ka":
					return "Georgian";

				case "de":
					return "German";

				case "el":
					return "Greek";

				case "gn":
					return "Guaraní";

				case "gu":
					return "Gujarati";

				case "ht":
					return "Haitian";

				case "ha":
					return "Hausa";

				case "he":
					return "Hebrew";

				case "hz":
					return "Herero";

				case "hi":
					return "Hindi";

				case "ho":
					return "Hiri Motu";

				case "hu":
					return "Hungarian";

				case "ia":
					return "Interlingua";

				case "id":
					return "Indonesian";

				case "ie":
					return "Interlingue";

				case "ga":
					return "Irish";

				case "ig":
					return "Igbo";

				case "ik":
					return "Inupiaq";

				case "io":
					return "Ido";

				case "is":
					return "Icelandic";

				case "it":
					return "Italian";

				case "iu":
					return "Inuktitut";

				case "ja":
					return "Japanese";

				case "jv":
					return "Javanese";

				case "kl":
					return "Kalaallisut";

				case "kn":
					return "Kannada";

				case "kr":
					return "Kanuri";

				case "ks":
					return "Kashmiri";

				case "kk":
					return "Kazakh";

				case "km":
					return "Khmer";

				case "ki":
					return "Kikuyu";

				case "rw":
					return "Kinyarwanda";

				case "ky":
					return "Kyrgyz";

				case "kv":
					return "Komi";

				case "kg":
					return "Kongo";

				case "ko":
					return "Korean";

				case "ku":
					return "Kurdish";

				case "kj":
					return "Kwanyama";

				case "la":
					return "Latin";

				case "lb":
					return "Luxembourgish";

				case "lg":
					return "Ganda";

				case "li":
					return "Limburgish";

				case "ln":
					return "Lingala";

				case "lo":
					return "Lao";

				case "lt":
					return "Lithuanian";

				case "lu":
					return "Luba-Katanga";

				case "lv":
					return "Latvian";

				case "gv":
					return "Manx";

				case "mk":
					return "Macedonian";

				case "mg":
					return "Malagasy";

				case "ms":
					return "Malay";

				case "ml":
					return "Malayalam";

				case "mt":
					return "Maltese";

				case "mi":
					return "Māori";

				case "mr":
					return "Marathi";

				case "mh":
					return "Marshallese";

				case "mn":
					return "Mongolian";

				case "na":
					return "Nauru";

				case "nv":
					return "Navajo";

				case "nb":
					return "Norwegian Bokmål";

				case "nd":
					return "North Ndebele";

				case "ne":
					return "Nepali";

				case "ng":
					return "Ndonga";

				case "nn":
					return "Norwegian Nynorsk";

				case "no":
					return "Norwegian";

				case "ii":
					return "Nuosu";

				case "nr":
					return "South Ndebele";

				case "oc":
					return "Occitan";

				case "oj":
					return "Ojibwe";

				case "cu":
					return "Old Church Slavonic";

				case "om":
					return "Oromo";

				case "or":
					return "Oriya";

				case "os":
					return "Ossetian";

				case "pa":
					return "Panjabi";

				case "pi":
					return "Pāli";

				case "fa":
					return "Farsi";

				case "pl":
					return "Polish";

				case "ps":
					return "Pashto";

				case "pt":
					return "Portuguese";

				case "qu":
					return "Quechua";

				case "rm":
					return "Romansh";

				case "rn":
					return "Kirundi";

				case "ro":
					return "Romanian";

				case "ru":
					return "Russian";

				case "sa":
					return "Sanskrit";

				case "sc":
					return "Sardinian";

				case "sd":
					return "Sindhi";

				case "se":
					return "Northern Sami";

				case "sm":
					return "Samoan";

				case "sg":
					return "Sango";

				case "sr":
					return "Serbian";

				case "gd":
					return "Scottish Gaelic";

				case "sn":
					return "Shona";

				case "si":
					return "Sinhala";

				case "sk":
					return "Slovak";

				case "sl":
					return "Slovene";

				case "so":
					return "Somali";

				case "st":
					return "Southern Sotho";

				case "es":
					return "Spanish";

				case "su":
					return "Sundanese";

				case "sw":
					return "Swahili";

				case "ss":
					return "Swati";

				case "sv":
					return "Swedish";

				case "ta":
					return "Tamil";

				case "te":
					return "Telugu";

				case "tg":
					return "Tajik";

				case "th":
					return "Thai";

				case "ti":
					return "Tigrinya";

				case "bo":
					return "Tibetan Standard";

				case "tk":
					return "Turkmen";

				case "tl":
					return "Tagalog";

				case "tn":
					return "Tswana";

				case "to":
					return "Tonga";

				case "tr":
					return "Turkish";

				case "ts":
					return "Tsonga";

				case "tt":
					return "Tatar";

				case "tw":
					return "Twi";

				case "ty":
					return "Tahitian";

				case "ug":
					return "Uighur";

				case "uk":
					return "Ukrainian";

				case "ur":
					return "Urdu";

				case "uz":
					return "Uzbek";

				case "ve":
					return "Venda";

				case "vi":
					return "Vietnamese";

				case "vo":
					return "Volapük";

				case "wa":
					return "Walloon";

				case "cy":
					return "Welsh";

				case "wo":
					return "Wolof";

				case "fy":
					return "Western Frisian";

				case "xh":
					return "Xhosa";

				case "yi":
					return "Yiddish";

				case "yo":
					return "Yoruba";

				case "za":
					return "Zhuang";

				case "zu":
					return "Zulu";

				case "hmn":
					return "Hmong";

				default:
					if (validate)
						throw new ServiceException(HttpStatusCode.BadRequest, string.Format(ErrorMessage.Content.UNRECOGNIZED_LANGUAGE, code));

					return null;
			}
		}

		public static LanguageDirection GetDirection(string code)
		{
			switch (code)
			{
				case "yi":
				case "fa":
				case "ar":
					return LanguageDirection.RightToLeft;
				default:
					return LanguageDirection.LeftToRight;
			}
		}

	}
}
