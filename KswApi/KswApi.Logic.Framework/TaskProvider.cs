﻿using System;
using KswApi.Common.Configuration;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace KswApi.Logic.Framework
{
	public static class TaskProvider
	{
		private static Dictionary<TaskType, Task> _tasksByType;
		private static List<Task> _taskList;

		public static void InitializeTasks()
		{
			_tasksByType = new Dictionary<TaskType, Task>();
			List<Task> tasks = GetTasksFromConfiguration();
			tasks.Sort((item1, item2) => string.CompareOrdinal(item1.ToString(), item2.ToString()));
			_taskList = tasks.OrderBy(item => item.Type.ToString()).ToList();

			foreach (Task module in tasks)
			{
				_tasksByType[module.Type] = module;
			}

		}

		public static Task GetTask(TaskType type)
		{
			Task task;

			if (!_tasksByType.TryGetValue(type, out task))
				throw new ServiceException(HttpStatusCode.BadRequest, "Task does not exist");

			return task;
		}

		public static List<Task> GetTasks()
		{
			return _taskList;
		}

		private static List<Task> GetTasksFromConfiguration()
		{
			List<Task> tasks = new List<Task>();
			foreach (TaskConfiguration task in Settings.Current.Tasks)
			{
				Task newTask;
				if (task.DailyOccurrence != null)
					newTask = GetDailyTask(task);
				else if (task.WeeklyOccurrence != null)
					newTask = GetWeeklyTask(task);
				else
					continue;

				tasks.Add(newTask);
				_tasksByType[task.Type] = newTask;
			}
			return tasks;
		}

		private static Task GetHourlyTask()
		{
			return null;
		}

		private static Task GetDailyTask(TaskConfiguration task)
		{
			return new Task
			{
				Active = task.Active,
				Type = task.Type,
				Duration = task.Duration,
				DailyRecurrence = new DailyRecurringSettings
				{
					DaysToRun = new List<DayOfWeek> { DayOfWeek.Saturday, DayOfWeek.Sunday, DayOfWeek.Monday, DayOfWeek.Tuesday, DayOfWeek.Wednesday, DayOfWeek.Thursday, DayOfWeek.Friday },
					StartTime = task.DailyOccurrence.Time
				}
			};
		}

		private static Task GetWeeklyTask(TaskConfiguration task)
		{
			return new Task
					   {
						   Active = task.Active,
						   Type = task.Type,
						   Duration = task.Duration,
						   DailyRecurrence = new DailyRecurringSettings
												 {
													 DaysToRun = task.WeeklyOccurrence.Days,
													 StartTime = task.WeeklyOccurrence.Time
												 }
					   };
		}
	}
}
