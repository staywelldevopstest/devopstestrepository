﻿using System;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace KswApi.MachineKeyGenerator
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			string validationKey = GenerateKey(128);
			string decryptionKey = GenerateKey(64);
			string tag = string.Format("<machineKey validationKey=\"{0}\" decryptionKey=\"{1}\" validation=\"SHA1\" decryption=\"AES\" />", validationKey, decryptionKey);
			textBox1.Text = tag;
		}

		private string GenerateKey(int len = 128)
		{
			byte[] buff = new byte[len / 2];
			RNGCryptoServiceProvider rng = new
									RNGCryptoServiceProvider();
			rng.GetBytes(buff);
			StringBuilder sb = new StringBuilder(len);
			for (int i = 0; i < buff.Length; i++)
				sb.Append(string.Format("{0:X2}", buff[i]));
			return sb.ToString();
		}
	}
}
