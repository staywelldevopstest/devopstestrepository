﻿using System;
using System.Net;
using KswApi.Logic.Framework.Helpers;
using OAuthErrorCode = KswApi.Interface.Enums.OAuthErrorCode;

namespace KswApi.Logic.Exceptions
{
	public class OAuthException : Exception
	{
		/// <summary>
		/// Private constructor. Use the static methods to create an OAuthException
		/// </summary>
		private OAuthException(string message)
			: base(message)
		{

		}

		public OAuthException(HttpStatusCode statusCode, OAuthErrorCode errorCode, string errorMessage, string state = null)
			: base(errorMessage)
		{
			StatusCode = statusCode;
			ErrorCode = errorCode;
			State = state;
		}

		#region Public Properties

		public HttpStatusCode StatusCode { get; private set; }
		public OAuthErrorCode ErrorCode { get; private set; }
		public string ErrorCodeString { get { return OAuthHelper.GetErrorCode(ErrorCode); } }
		public string State { get; private set; }

		#endregion Public Properties

	}
}
