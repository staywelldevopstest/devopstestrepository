﻿using System;
using KswApi.Interface.Enums;

namespace KswApi.Logic.Exceptions
{
	public class OAuthRedirectException : Exception
	{
		public OAuthRedirectException(string redirectUri, OAuthErrorCode errorCode, string errorMessage, string state = null)
		{

		}
	}
}
