﻿using KswApi.Repositories;

namespace KswApi.Logic
{
	public abstract class LogicBase
	{
		private Repository _repository;

		public Repository Repository
		{
			get { return _repository ?? (_repository = new Repository()); }
		}
	}
}
