﻿using KswApi.Caching;
using KswApi.Common.Interfaces;
using KswApi.Interface.Objects;
using KswApi.Ioc;
using KswApi.Poco.Tasks;
using KswApi.Repositories;
using System;

namespace KswApi.Logic
{
    public class TestLogic
    {
	    private const int BUFFER_SIZE = 1024;

        public PingData GetPingData()
        {
            return new PingData
                    {
                        CurrentDateAndTime = DateTime.UtcNow.ToString("o")
                    };
        }

        public CacheTestData GetCacheTestData()
        {
            TestData testData = ManualCache.Get<TestData>(item => item.Key == "CacheTest");

            if (testData != null)
            {
                return new CacheTestData
                {
                    Data = testData.Data,
                    Source = "Cache"
                };
            }

            TestRepository repository = new TestRepository();

            testData = repository.TestData.GetTestData();

            if (testData == null)
                return new CacheTestData();

            testData.Key = "CacheTest";

            // 2 minutes is the specified cache expiration time from the user story
            // this is a magic number, but due to the fact that this service is temporary
            // it's not put in the constants class
            ManualCache.Set(testData, item => item.Key, TimeSpan.FromMinutes(2));

            return new CacheTestData
            {
                Data = testData.Data,
                Source = "Database"
            };
        }

        public StatusResponse ClearCache()
        {
            ICache cache = Dependency.Get<ICache>();

            cache.Clear();

            return new StatusResponse
                       {
                           Status = "The cache was cleared."
                       };
        }

        public void CreateError()
        {
            throw new Exception("An error has occurred, intentionally.");
        }

		public void Stream(StreamResponse response, StreamRequest request)
		{
			byte[] buffer = new byte[BUFFER_SIZE];
			response.ContentType = request.ContentType;
			response.ContentLength = request.ContentLength;
			while (true)
			{
				int count = request.Read(buffer, 0, BUFFER_SIZE);
				if (count <= 0)
					break;
				response.Write(buffer, 0, count);
			}

			response.Flush();
		}
    }
}
