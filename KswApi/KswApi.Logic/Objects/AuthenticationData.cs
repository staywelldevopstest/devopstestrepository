﻿namespace KswApi.Logic.Objects
{
	public class AuthenticationData
	{
		public string Username { get; set; }
		public string Password { get; set; }
		public string Token { get; set; }
	}
}
