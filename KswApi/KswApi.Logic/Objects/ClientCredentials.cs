﻿namespace KswApi.Logic.Objects
{
	public class ClientCredentials
	{
		public string ClientId { get; set; }
		public string ClientSecret { get; set; }
	}
}
