﻿using System;
using KswApi.Interface.Objects;

namespace KswApi.Logic
{
	public class RootLogic
	{
		public ServiceInformation GetServiceInformation()
		{
			return new ServiceInformation
				       {
					       Copyright =
						       string.Format("Copyright © {0} Krames StayWell except where otherwise noted.", DateTime.UtcNow.Year)
				       };
		}
	}
}
