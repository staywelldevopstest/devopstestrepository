﻿using System;
using System.Collections.Generic;
using System.Linq;
using KswApi.Interface.Enums;
using KswApi.Poco;
using KswApi.Poco.Enums;
using KswApi.Repositories;

namespace KswApi.Logic
{
    public class ConfigurationValueLogic
    {
        // Retrieve from config file instead...
        private List<ConfigurationValue<List<ReferenceFile>>> _referenceFiles = new List<ConfigurationValue<List<ReferenceFile>>>
                                                                                    {
                                                                                        new ConfigurationValue<List<ReferenceFile>>
                                                                                            {
                                                                                                Data = new List<ReferenceFile>
                                                                                                           {
                                                                                                               new ReferenceFile
                                                                                                                   {
                                                                                                                       Filepath = "~/Files/Servicelines.csv",
                                                                                                                       FileModifiedDate = DateTime.MinValue,
                                                                                                                       Properties =
                                                                                                                           new Dictionary<string, object>
                                                                                                                               {
                                                                                                                                   {
                                                                                                                                       typeof (
                                                                                                                                       HierarchicalTaxonomyType)
                                                                                                                                       .Name
                                                                                                                                       ,
                                                                                                                                       HierarchicalTaxonomyType.
                                                                                                                                       Serviceline
                                                                                                                                   }
                                                                                                                               }
                                                                                                                   }
                                                                                                           }
                                                                                                ,
                                                                                                //Type = ConfigurationValueType.HierarchicalTaxonomyReferenceFile,
                                                                                                Id = "HierarchicalTaxonomyReferenceFiles"
                                                                                            }

                                                                                    };
            

        public void EnsureConfigurationValues()
        {
            foreach(var file in _referenceFiles)
            {
                EnsureConfigurationValue(file);
            }
        }

        private void EnsureConfigurationValue<T>(ConfigurationValue<T> configValue)
        {
            Repository repository = new Repository();

            if (repository.Configuration.ConfigurationValues.Get<T>(configValue.Id) == null)
            {
                repository.Configuration.ConfigurationValues.Add(configValue);
            }
        }
    }
}
