﻿using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Ioc;
using KswApi.Logging.Interfaces;
using KswApi.Logic.Framework.Constants;
using KswApi.Poco.Logging;
using KswApi.Repositories;
using KswApi.Repositories.Enums;
using Linq2Rest.Parser;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Net;

namespace KswApi.Logic
{
    public class LogLogic
    {
        #region Public Methods
        public LogList GetLogs(int offset, int count, string sort, string filter, string clientId)
        {
            if (count > Constant.MAXIMUM_PAGE_SIZE)
                throw new ServiceException(HttpStatusCode.BadRequest, "Count is greater than the maximum page size.");

            LogRepository logRepository = new LogRepository();

            // Commented because we do not filter by client yet, but we will so that's why it is still here.
            //if (!string.IsNullOrEmpty(clientId))
            //{
            //    Repository repository = new Repository();

            //    Guid clientGuid;

            //    if (!Guid.TryParse(clientId, out clientGuid))
            //        throw new ServiceException(HttpStatusCode.BadRequest, string.Format("Invalid client ID: {0}.", clientId));

            //    Client client = repository.Administration.Clients.GetById(clientGuid);

            //    if (client == null)
            //        throw new ServiceException(HttpStatusCode.BadRequest, string.Format("Invalid client ID: {0}.", clientId));
            //}

            int total;
            IEnumerable<Log> logs;

            LogSortOption sortOption = GetLogSortOption(sort);

            if (!string.IsNullOrEmpty(filter))
            {
                Expression<Func<Log, bool>> expression = ParseFilter(filter);

                total = logRepository.Logs.Count(expression);
                logs = logRepository.Logs.Get(offset, count, expression, sortOption);
            }
            else
            {
                total = logRepository.Logs.Count();
                logs = logRepository.Logs.Get(offset, count, sortOption);
            }

            return new LogList
                     {
                         Total = total,
                         Offset = offset,
                         Items = new List<Log>(logs),
                         SortedBy = GetLogSortValue(sortOption)
                     };
        }

        private Expression<Func<Log, bool>> ParseFilter(string filter)
        {
            FilterExpressionFactory factory = new FilterExpressionFactory();

            try
            {
                return factory.Create<Log>(filter);
            }
            catch (InvalidOperationException exception)
            {
                throw new ServiceException(HttpStatusCode.BadRequest, string.Format("The filter string is invalid. {0}", exception.Message));
            }
        }

        public Log GetLog(string logId)
        {
            LogRepository logRepository = new LogRepository();

            if (string.IsNullOrEmpty(logId))
                throw new ServiceException(HttpStatusCode.BadRequest, "No log ID was specified.");

            Guid guid;
            if (!Guid.TryParse(logId, out guid))
                throw new ServiceException(HttpStatusCode.BadRequest, string.Format("The log ID is invalid: {0}.", logId));

            Log log = logRepository.Logs.Get(guid);

            if (log == null)
                throw new ServiceException(HttpStatusCode.NotFound, string.Format("The specified log was not found: {0}.", logId));

            return log;
        }

        public bool IsLogInitialized()
        {
            try
            {
                ILog log = Dependency.Get<ILog>();

                return log != null;
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion Public Methods

        #region Private Methods

        private LogSortOption GetLogSortOption(string sort)
        {
            if (string.IsNullOrEmpty(sort))
                return LogSortOption.DateDescending;

            LogSortOption sortOption;

            if (!Enum.TryParse(sort.Replace("-", ""), true, out sortOption))
                throw new ServiceException(HttpStatusCode.BadRequest, string.Format("Invalid sort option: {0}.", sort));

            return sortOption;
        }

        private string GetLogSortValue(LogSortOption sortOption)
        {
            switch (sortOption)
            {
                case LogSortOption.DateDescending:
                    return "Date-Descending";
                case LogSortOption.DateAscending:
                    return "Date-Ascending";
                default:
                    throw new ArgumentOutOfRangeException("sortOption");
            }
        }

        #endregion Private Methods
    }
}
