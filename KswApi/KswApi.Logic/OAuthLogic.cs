﻿using KswApi.Common;
using KswApi.Common.Configuration;
using KswApi.Common.Objects;
using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Ioc;
using KswApi.Logic.Administration;
using KswApi.Logic.Exceptions;
using KswApi.Logic.Framework.Constants;
using KswApi.Logic.Framework.Enums;
using KswApi.Logic.Framework.Helpers;
using KswApi.Logic.Framework.Interfaces;
using KswApi.Poco.Administration;
using KswApi.Repositories;
using KswApi.Repositories.Objects;
using KswApi.RestrictedInterface.Objects;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using OAuthErrorCode = KswApi.Interface.Enums.OAuthErrorCode;

// ReSharper disable UnusedParameter.Local

namespace KswApi.Logic
{
	public class OAuthLogic : LogicBase
	{
		#region Public Methods

		public ApplicationValidationResponse GetApplicationValidation(string responseType, string applicationId, string redirectUri, string scope, string state)
		{
			ApplicationValidationRequest request = new ApplicationValidationRequest
				{
					ApplicationId = applicationId,
					RedirectUri = redirectUri,
					ResponseType = responseType,
					Scope = scope,
					State = state
				};

			ValidateClientParameters(request, out redirectUri);

			if (!UriHelper.IsHttps(request.RedirectUri))
				return new ApplicationValidationResponse
				{
					Warning = "The client endpoint is not over a secure connection which can lead to security breaches.  Please continue with caution.",
				};

			return new ApplicationValidationResponse();
		}

		public AuthorizationGrantResponse CreateAuthorizationGrant(AuthorizationGrantRequest request)
		{
			if (request == null)
				throw new ServiceException(HttpStatusCode.BadRequest, "No request object in the message.");

			string redirectUri;

			ValidateClientParameters(request, out redirectUri);

			IUserAuthenticator authenticator = Dependency.Get<IUserAuthenticator>();

			UserType userType = EmailHelper.IsValid(request.UserName) ? UserType.ClientUser : UserType.DomainUser;

			UserAuthenticationDetails details = null;

			if (userType == UserType.ClientUser)
			{
				// client user login
				ClientUserLogic clientUserLogic = new ClientUserLogic();

				details = clientUserLogic.Authenticate(request.UserName, request.Password);
			}
			else
			{
				details = authenticator.Authenticate(request.UserName, request.Password);
			}

			if (details == null)
			{
				return new AuthorizationGrantResponse
					   {
						   Error = ErrorMessage.Authorization.INVALID_LOGIN
					   };
			}

            if(userType == UserType.DomainUser)
            {
                DomainUserLogic logic = new DomainUserLogic();
                logic.Upsert(details);
            }

			AuthorizationGrant grant = new AuthorizationGrant
			{
				Code = TokenGenerator.CreateToken(),
				Expiration = DateTime.UtcNow.AddMinutes(Constant.AUTHORIZATION_GRANT_EXPIRATION),
				RedirectUri = request.RedirectUri,
				State = request.State,
				UserDetails = details
			};

			// save the grant to the database
			Repository repository = new Repository();

			repository.Authorization.AuthorizationGrants.Add(grant);

			repository.Commit();

			return new AuthorizationGrantResponse
				{
					IsValid = true,
					RedirectUri = UriHelper.AddQuery(redirectUri, new NameValueCollection
																				 {
																					 {"code", grant.Code},
																					 {"state", request.State}
																				 })
				};
		}

		public OAuthTokenResponse CreateAccessToken(OAuthTokenRequest request, NameValueCollection headers, string address)
		{
			if (request == null)
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidRequest, "The request is empty");

			if (string.IsNullOrEmpty(request.grant_type))
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidRequest, "The grant type is missing.");

			string authorizationHeader = GetAuthorizationHeader(headers);

			if (!string.IsNullOrEmpty(authorizationHeader))
			{
				if (!string.IsNullOrEmpty(request.client_id) || !string.IsNullOrEmpty(request.client_secret))
					throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidRequest, "The request contains both basic authentication and body parameter authentication.");

				SetAuthenticationFromHeader(request, authorizationHeader);
			}

			switch (request.grant_type)
			{
				case OAuthGrantType.AUTHORIZATION_CODE:
					return GetAuthorizationGrantAccessToken(request);
				case OAuthGrantType.CLIENT_CREDENTIALS:
					return GetApplicationCredentialsGrantAccessToken(request, address);
				case OAuthGrantType.REFRESH_TOKEN:
					return RefreshAccessToken(request);
				default:
					throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.UnsupportedGrantType, "The grant type is invalid.");
			}
		}

		public ValidationResponse ValidateAccessToken(string token)
		{
			if (string.IsNullOrEmpty(token))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.OAuth.ACCESS_TOKEN_REQUIRED);

			ValidationResponse response = new ValidationResponse();

			Repository repository = new Repository();

			AccessToken accessToken = repository.Authorization.AccessTokens.GetByToken(token);

			if (accessToken != null && DateTime.UtcNow <= accessToken.Expiration)
			{
				response.Valid = true;
				response.State = accessToken.State;
				response.Rights = accessToken.Rights;
				response.ApplicationId = accessToken.ApplicationId;

				UserDetails details = accessToken.UserDetails;

				if (details != null)
				{
					response.User = new UserResponse
									{
										EmailAddress = details.EmailAddress,
										FirstName = details.FirstName,
										LastName = details.LastName,
										FullName = details.FirstName + ' ' + details.LastName,
										Type = details.Type
									};
				}
			}

			return response;
		}

		public void AuthorizeOperation(string sourceAddress, AccessToken accessToken, Application client, AllowAttribute allowed)
		{
			if (allowed == null || allowed.ClientType == ClientType.Public || allowed.ClientType == ClientType.Any)
				return;

			if (accessToken == null)
				throw new ServiceException(HttpStatusCode.Unauthorized, ErrorMessage.Authorization.UNAUTHORIZED);

			if (DateTime.UtcNow > accessToken.Expiration)
			{
				Repository.Authorization.AccessTokens.Delete(accessToken);

				throw new ServiceException(HttpStatusCode.Unauthorized, ErrorMessage.OAuth.EXPIRED_ACCESS_TOKEN);
			}

			if (!string.IsNullOrEmpty(allowed.Rights))
			{
				if (accessToken.Rights == null)
					throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.Common.INSUFFICIENT_RIGHTS);

				IEnumerable<string> requiredRights = allowed.Rights.Split(new char[] { ',' }).Select(x => x.Trim());
				IEnumerable<string> intersectingRights = accessToken.Rights.Intersect(requiredRights);

				if (!intersectingRights.Any())
				{
					throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.Common.INSUFFICIENT_RIGHTS);
				}
			}

			if (client == null)
				throw new ServiceException(HttpStatusCode.Forbidden, "The access token referenced an invalid client.");

			// If this method is an authentication client method, check the client's ip
			if (allowed.ClientType != ClientType.Authentication) return;

			if (accessToken.TokenType != AccessTokenType.AuthorizationClient)
				throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.Common.INSUFFICIENT_RIGHTS);

			if (client.Addresses == null)
				throw new ServiceException(HttpStatusCode.Forbidden, "The client does not have any allowed addresses.");

			if (string.IsNullOrEmpty(sourceAddress))
				throw new InvalidOperationException("The client address could not be determined.");

			if (client.Addresses.All(item => item != sourceAddress))
				throw new ServiceException(HttpStatusCode.Forbidden, "The request originated from a blocked address.");
		}

		public AccessToken GetAccessTokenFromHeaders(NameValueCollection headers)
		{
			string authorizationHeader = GetAuthorizationHeader(headers);

			if (string.IsNullOrEmpty(authorizationHeader))
				return null;

			// Bearer auth should be "Bearer AccessToken"
			string[] split = authorizationHeader.Split(' ');

			string scheme = split[0];

			if (scheme == Constant.BASIC_AUTHENTICATION_SCHEME_NAME)
				return null; // allow basic to pass through for oauth token endpoint

			if (scheme != Constant.BEARER_AUTHENTICATION_SCHEME_NAME)
				throw new ServiceException(HttpStatusCode.Unauthorized, string.Format("Unknown authorization scheme in the authorization header."));

			if (split.Length != 2)
				throw new ServiceException(HttpStatusCode.Unauthorized, "Unrecognized authorization header format.");

			Repository repository = new Repository();

			AccessToken accessToken = repository.Authorization.AccessTokens.GetByToken(split[1]);

			if (accessToken == null)
				throw new ServiceException(HttpStatusCode.Unauthorized, "Invalid or expired access token.");

			return accessToken;
		}

		public Application GetApplicationByAccessToken(AccessToken token)
		{
			Repository repository = new Repository();

			return repository.Administration.Applications.GetById(token.ApplicationId);
		}

		public KeepAliveResponse KeepAlive(int idleMilliseconds, AccessToken accessToken)
		{
			if (idleMilliseconds < 0)
				idleMilliseconds = 0;

			TimeSpan timeout = Settings.Current.WebPortal.Timeout;
			TimeSpan idle = TimeSpan.FromMilliseconds(idleMilliseconds);

			DateTime now = DateTime.UtcNow;

			Repository repository = new Repository();

			TimeSpan timeLeft = timeout - idle;

			DateTime expiration = now + timeLeft;

			bool updateAccessToken = false;

			if (accessToken.WebPortalTimeout != null && accessToken.WebPortalTimeout.Value > expiration)
			{
				timeLeft = accessToken.WebPortalTimeout.Value - now;
				expiration = accessToken.WebPortalTimeout.Value;
			}
			else
			{
				updateAccessToken = true;
			}

			if (expiration < now)
			{
				// we should be logged out, log us out

				repository.Authorization.AccessTokens.Delete(accessToken.Id);
				if (accessToken.RefreshTokenId != null)
					repository.Authorization.RefreshTokens.Delete(accessToken.RefreshTokenId.Value);

				throw new ServiceException(HttpStatusCode.Unauthorized, ErrorMessage.Authorization.UNAUTHORIZED);
			}

			if (updateAccessToken)
			{
				// update the access token with the new timeout value
				accessToken.WebPortalTimeout = expiration;
				repository.Authorization.AccessTokens.Update(accessToken);
			}

			return new KeepAliveResponse { TimeoutInMilliseconds = (int) timeLeft.TotalMilliseconds };
		}

		#endregion Public Methods

		#region Private Methods

		private List<string> GetApplicationScope(Application application, string requestedScope)
		{
			if (!application.LicenseId.HasValue)
				return new List<string>();

			LicenseLogic licenseLogic = new LicenseLogic();

			License license = licenseLogic.GetLicenseByApplication(application.Id.ToString());

			if (license == null)
				throw new ServiceException(HttpStatusCode.NotFound, "License not found");

			ModuleLogic moduleLogic = new ModuleLogic();

			//If no modules no scope
			if (license.Modules == null || license.Modules.Count == 0)
				return new List<string>();

			List<string> defaultScope = moduleLogic.GetModuleRights(license.Modules);

			// if no scope, gets default scope
			if (string.IsNullOrEmpty(requestedScope))
				return defaultScope;

			List<string> requestedScopeList = requestedScope.Split(' ').ToList();


			List<string> newScope = defaultScope.Where(defaultScopeItem => requestedScopeList.Any(requestedScopeItem => requestedScopeItem == defaultScopeItem)).ToList();

			// if it's invalid scope, return the default scope
			if (newScope.Count == 0)
				return defaultScope;

			// if it's valid, return the new scope
			return newScope;
		}

		private void ValidateClientParameters(ApplicationValidationRequest request, out string redirectUri)
		{
			IEnumerable<string> scopeCollection;
			if (!string.IsNullOrEmpty(request.Scope) && !GetScope(request.Scope, out scopeCollection))
				throw new ServiceException(HttpStatusCode.BadRequest, "The scope is invalid.");

			if (!string.IsNullOrEmpty(request.RedirectUri) && request.RedirectUri.Contains("#"))
				throw new ServiceException(HttpStatusCode.BadRequest, "The redirect URI may not contain a hash tag.");

			Guid applicationGuid;
			if (!Guid.TryParse(request.ApplicationId, out applicationGuid))
				throw new ServiceException(HttpStatusCode.BadRequest, "The application ID is invalid.");

			Repository repository = new Repository();

			Application application = repository.Administration.Applications.GetById(applicationGuid);

			if (application == null)
				throw new ServiceException(HttpStatusCode.BadRequest, "The application ID is invalid.");

			if (string.IsNullOrEmpty(request.RedirectUri) && string.IsNullOrEmpty(application.RedirectUri))
				throw new ServiceException(HttpStatusCode.BadRequest, "The redirect URI is missing.");

			if (!string.IsNullOrEmpty(request.RedirectUri) && !string.IsNullOrEmpty(application.RedirectUri) && !UriHelper.AbsoluteUrisAreEqual(request.RedirectUri, application.RedirectUri))
				throw new ServiceException(HttpStatusCode.BadRequest, "The redirect URI is incorrect.");

			redirectUri = string.IsNullOrEmpty(request.RedirectUri) ? application.RedirectUri : request.RedirectUri;

			// unrecognized response type, this validation error should redirect back to the response uri
			if (request.ResponseType != "code")
				throw new ServiceRedirectException(HttpStatusCode.BadRequest,
					UriHelper.AddQuery(redirectUri,
							   new NameValueCollection
								   {
									   {"error", OAuthHelper.GetErrorCode(OAuthErrorCode.UnsupportedResponseType)},
									   {"error_description", ErrorMessage.OAuth.RESPONSE_TYPE_MUST_BE_CODE}
								   }),
					ErrorMessage.OAuth.RESPONSE_TYPE_MUST_BE_CODE);
		}

		private List<string> GetRightsForRoles(IList<string> roles)
		{
			Repository repository = new Repository();

			if (roles == null || roles.Count == 0)
				return null;

			List<string> rights = null;

			List<string> rightList = new List<string>();

			foreach (string roleName in roles)
			{
				if (string.IsNullOrEmpty(roleName))
					continue;

				Role role = repository.Administration.Roles.GetByName(roleName);

				if (role == null || role.Rights == null)
					continue;

				foreach (string right in role.Rights)
				{
					if (!rightList.Contains(right))
						rightList.Add(right);
				}
			}

			if (rightList.Count > 0)
				rights = rightList;

			return rights;
		}

		private OAuthTokenResponse GetAuthorizationGrantAccessToken(OAuthTokenRequest request)
		{
			Guid clientGuid;
			if (!Guid.TryParse(request.client_id, out clientGuid))
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidClient, "The application ID is invalid.");

			if (string.IsNullOrEmpty(request.code))
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidRequest, "The authorization code is missing.");

			Repository repository = new Repository();

			Application client = repository.Administration.Applications.GetById(clientGuid);

			if (client == null)
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidClient, "The application ID is invalid.");

			if (client.Secret != request.client_secret)
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidClient, "The client credentials are invalid.");

			AuthorizationGrant authorizationGrant = repository.Authorization.AuthorizationGrants.GetByCode(request.code);
			if (authorizationGrant == null)
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidGrant, "The authorization code is invalid.");

			if (!string.IsNullOrEmpty(authorizationGrant.RedirectUri) && !authorizationGrant.RedirectUri.Equals(request.redirect_uri, StringComparison.OrdinalIgnoreCase))
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidRequest, "The redirect URI is not the same as that included in the authorization request.");

			if (DateTime.UtcNow > authorizationGrant.Expiration)
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidGrant, "The authorization code is expired.");

			if (authorizationGrant.UserDetails == null)
				throw new ServiceException(HttpStatusCode.InternalServerError, ErrorMessage.Authorization.OBSOLETE_AUTHORIZATION_GRANT);

			List<string> rights = GetRightsForRoles(authorizationGrant.UserDetails.Roles);

			AccessToken accessToken = GenerateAccessToken(client, repository, authorizationGrant.UserDetails, AccessTokenType.InternalUser, rights);

			RefreshToken refreshToken = RefreshToken.FromAccessToken(accessToken, TokenGenerator.CreateToken(), DateTime.UtcNow.Add(Settings.Current.RefreshToken.Expiration));

			accessToken.RefreshTokenId = refreshToken.Id;
			refreshToken.RefreshTokenId = accessToken.Id;

			repository.Authorization.AccessTokens.Add(accessToken);

			repository.Authorization.RefreshTokens.Add(refreshToken);

			repository.Authorization.AuthorizationGrants.Delete(authorizationGrant);

			repository.Commit();

			return new OAuthTokenResponse
				{
					access_token = accessToken.Token,
					token_type = OAuthHelper.GetTokenType(OAuthTokenType.Bearer),
					expires_in = Convert.ToInt32(Settings.Current.AccessToken.Expiration.TotalSeconds),
					refresh_token = refreshToken.Token,
					scope = authorizationGrant.UserDetails.Roles != null && authorizationGrant.UserDetails.Roles.Count > 0 ? string.Join(Constant.ROLE_SEPARATOR, authorizationGrant.UserDetails.Roles) : null,
				};
		}

		private OAuthTokenResponse GetApplicationCredentialsGrantAccessToken(OAuthTokenRequest request, string address)
		{
			IEnumerable<string> scopeCollection;
			if (!string.IsNullOrEmpty(request.scope) && !GetScope(request.scope, out scopeCollection))
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidScope, "The scope is invalid.");

			Guid applicationGuid;
			if (!Guid.TryParse(request.client_id, out applicationGuid))
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidClient, "The application ID is invalid.");

			Repository repository = new Repository();

			Application application = repository.Administration.Applications.GetById(applicationGuid);

			if (application == null)
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidClient, "The application ID is invalid.");

			ValidateAddress(application, address);

			if (application.Secret != request.client_secret)
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidClient, "The application credentials are invalid.");

			List<string> validatedScope = GetApplicationScope(application, request.scope);

			AccessTokenType type = application.Type == ApplicationType.AdministrationPortal
									   ? AccessTokenType.AuthorizationClient
									   : AccessTokenType.Client;

			AccessToken accessToken = GenerateAccessToken(application, repository, null, type, validatedScope);

			accessToken.State = request.state;

			repository.Authorization.AccessTokens.Add(accessToken);

			repository.Commit();

			return new OAuthTokenResponse
				{
					access_token = accessToken.Token,
					token_type = OAuthHelper.GetTokenType(OAuthTokenType.Bearer),
					expires_in = Convert.ToInt32(Settings.Current.AccessToken.Expiration.TotalSeconds),
					scope = RightListToScopeString(validatedScope)
				};
		}

		private void ValidateAddress(Application application, string address)
		{
			if (application.Addresses == null || application.Addresses.Count == 0)
				return;

			if (string.IsNullOrEmpty(address))
				throw new InvalidOperationException("The request is not from a valid address.");

			if (application.Addresses.Any(item => item == address))
				return;

			throw new ServiceException(HttpStatusCode.Forbidden, "The application address is incorrect.");
		}

		private string RightListToScopeString(IEnumerable<string> rights)
		{
			if (rights == null)
				return string.Empty;

			return string.Join(" ", rights);
		}

		private AccessToken GenerateAccessToken(Application client, Repository repository, UserDetails userDetails, AccessTokenType type, List<string> rights = null)
		{
			DateTime expirationTime = DateTime.UtcNow.Add(Settings.Current.AccessToken.Expiration);

			AccessToken accessToken = new AccessToken
			{
				Id = Guid.NewGuid(),
				ApplicationId = client.Id,
				LicenseId = client.LicenseId,
				Token = TokenGenerator.CreateToken(),
				TokenType = type,
				Expiration = expirationTime,
				UserDetails = userDetails,
				Rights = rights
			};

			return accessToken;
		}

		private bool GetScope(string scope, out IEnumerable<string> scopeCollection)
		{
			// check for invalid scope characters

			// from the spec:
			// scope       = scope-token *( SP scope-token )
			// scope-token = 1*( %x21 / %x23-5B / %x5D-7E )

			if (!scope.All(character =>
				(character >= '#' && character <= '[')
				|| (character >= ']' && character <= '~')
				|| character == '!'
				|| character == ' '))
			{
				scopeCollection = null;
				return false;
			}

			scopeCollection = scope.Split(' ');

			return true;
		}

		private string GetAuthorizationHeader(NameValueCollection headers)
		{
			if (headers == null)
				return null;

			string authorizationHeaderKey =
				headers.AllKeys.FirstOrDefault(
					item => string.Compare(item, "Authorization", StringComparison.OrdinalIgnoreCase) == 0);

			if (string.IsNullOrEmpty(authorizationHeaderKey))
				return null;

			return headers[authorizationHeaderKey];
		}

		private OAuthTokenResponse RefreshAccessToken(OAuthTokenRequest request)
		{
			if (string.IsNullOrEmpty(request.refresh_token))
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidRequest, "Missing refresh token value.");

			Repository repository = new Repository();

			RefreshToken oldRefreshToken = repository.Authorization.RefreshTokens.GetByToken(request.refresh_token);

			if (oldRefreshToken == null)
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidGrant, "The refresh token is invalid.");

			if (DateTime.UtcNow > oldRefreshToken.Expiration)
			{
				Repository.Authorization.RefreshTokens.Delete(oldRefreshToken);
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidGrant, "The refresh token has expired.");
			}

			repository.Authorization.RefreshTokens.Delete(oldRefreshToken);

			// also remove the token being refreshed
			if (oldRefreshToken.RefreshTokenId != null)
				repository.Authorization.AccessTokens.Delete(oldRefreshToken.RefreshTokenId.Value);

			AccessToken newAccessToken = oldRefreshToken.ToAccessToken(TokenGenerator.CreateToken(), DateTime.UtcNow.Add(Settings.Current.AccessToken.Expiration));

			RefreshToken newRefreshToken = oldRefreshToken.ToRefreshToken(TokenGenerator.CreateToken(), DateTime.UtcNow.Add(Settings.Current.RefreshToken.Expiration));

			newAccessToken.RefreshTokenId = newRefreshToken.Id;
			newRefreshToken.RefreshTokenId = newAccessToken.Id;

			repository.Authorization.AccessTokens.Add(newAccessToken);

			repository.Authorization.RefreshTokens.Add(newRefreshToken);

			return new OAuthTokenResponse
				{
					access_token = newAccessToken.Token,
					token_type = OAuthHelper.GetTokenType(OAuthTokenType.Bearer),
					expires_in = Convert.ToInt32(Settings.Current.AccessToken.Expiration.TotalSeconds),
					refresh_token = newRefreshToken.Token,
					scope = RightListToScopeString(newAccessToken.Rights)
				};
		}

		private void SetAuthenticationFromHeader(OAuthTokenRequest request, string header)
		{
			if (string.IsNullOrEmpty(header))
				throw new OAuthException(HttpStatusCode.Unauthorized, OAuthErrorCode.InvalidRequest, "The authorization header is empty.");

			string[] parts = header.Split(' ');

			if (parts[0] != Constant.BASIC_AUTHENTICATION_SCHEME_NAME)
				throw new OAuthException(HttpStatusCode.Unauthorized, OAuthErrorCode.InvalidRequest, string.Format("Unrecognized authentication scheme: {0}.", parts[0]));

			string credentials;
			byte[] credentialBytes;
			try
			{
				credentialBytes = Convert.FromBase64String(parts[1]);
			}
			catch (FormatException)
			{
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidRequest, "The authentication header is not encoded correctly.");
			}

			try
			{
				credentials = Encoding.UTF8.GetString(credentialBytes);
			}
			catch (ArgumentException)
			{
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidRequest, "The authentication header is not encoded correctly.");
			}

			string[] splitCredentials = credentials.Split(':');
			if (splitCredentials.Length != 2)
				throw new OAuthException(HttpStatusCode.BadRequest, OAuthErrorCode.InvalidRequest, "The authentication header is not encoded correctly.");

			request.client_id = splitCredentials[0];
			request.client_secret = splitCredentials[1];
		}

		#endregion Private Methods
	}
}
