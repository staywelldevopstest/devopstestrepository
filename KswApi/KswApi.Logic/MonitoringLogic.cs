﻿
using System;
using KswApi.Caching;
using KswApi.Interface.Objects;
using KswApi.Ioc;
using KswApi.Logic.Framework.Interfaces;
using KswApi.Poco.Tasks;
using KswApi.Repositories.Objects;

namespace KswApi.Logic
{
    public class MonitoringLogic
    {
        public MonitoringStatus CheckStatus()
        {
            MonitoringStatus status = new MonitoringStatus();

            //Manually hit database
            try
            {
                LogLogic logLogic = new LogLogic();

                LogList logs = logLogic.GetLogs(0, 1, string.Empty, string.Empty, string.Empty);

                if (logs.Items.Count > 0)
                    status.DatabaseStatus = true;
            }
            catch
            {
                status.DatabaseStatus = false;
            }

            //Manually hit cache
            try
            {
                TestData testData = new TestData { Data = "Cache test data", Id = Guid.NewGuid(), Key = "CacheTest1" };

                ManualCache.Set(testData, item => item.Key, TimeSpan.FromMinutes(1));

                TestData cachedTestData = ManualCache.Get<TestData>(item => item.Key == "CacheTest1");

                if (cachedTestData != null && cachedTestData.Data == testData.Data && cachedTestData.Key == testData.Key && cachedTestData.Id == testData.Id)
                    status.CacheStatus = true;
            }
            catch
            {
                status.CacheStatus = false;
            }

			// hit the queue
	        try
	        {
		        ITaskQueue queue = Dependency.Get<ITaskQueue>();

				if (queue.Test())
					status.QueueStatus = true;
	        }
	        catch
	        {
		        
	        }

            //if we have even gotten here they the service is up and running :)
            status.ServiceStatus = true;

            return status;
        }
    }
}
