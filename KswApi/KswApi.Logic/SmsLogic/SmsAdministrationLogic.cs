﻿using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.SmsGateway;
using KswApi.Ioc;
using KswApi.Logic.Administration;
using KswApi.Logic.Framework;
using KswApi.Logic.Framework.Constants;
using KswApi.Logic.Framework.Helpers;
using KswApi.Logic.Framework.Interfaces;
using KswApi.Logic.Framework.Objects;
using KswApi.Poco.Sms;
using KswApi.Repositories;
using KswApi.Repositories.Enums;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace KswApi.Logic.SmsLogic
{
    public class SmsAdministrationLogic
    {
        public void EnsureShortCodes()
        {
            Repository repository = new Repository();

            SmsSharedNumber number = repository.SmsGateway.AvailableNumbers.GetByNumber(Constant.INITIAL_SHORT_CODE);

            if (number == null)
            {
                number = new SmsSharedNumber
                {
                    Number = Constant.INITIAL_SHORT_CODE,
                    Type = SmsNumberType.ShortCode
                };

                repository.SmsGateway.AvailableNumbers.Add(number);
            }
        }

        public SmsGatewayModule GetModule(string licenseId)
        {
            Repository repository = new Repository();

            LicenseLogic licenseLogic = new LicenseLogic();

            License license = licenseLogic.GetLicense(licenseId);

            CheckLicenseForModule(license);

            IEnumerable<SmsNumberData> numbers = repository.SmsGateway.Numbers.GetByLicenseId(license.Id);

            List<SmsSharedNumber> available = repository.SmsGateway.AvailableNumbers.Get(item => item.Type == SmsNumberType.ShortCode)
                .ToList();

            List<ShortCode> shortCodes = new List<ShortCode>();
            List<LongCode> longCodes = new List<LongCode>();

            foreach (SmsNumberData number in numbers)
            {
                // remove from available
                available.RemoveAll(item => item.Number == number.Number);

                List<SmsKeyword> keywords = null;

                if (number.RouteType != SmsNumberRouteType.CatchAll)
                {
                    keywords = new List<SmsKeyword>();

                    if (number.Keywords != null)
                    {
                        foreach (SmsNumberKeywordItem keywordItem in number.Keywords)
                        {
                            SmsKeywordData keywordData = repository.SmsGateway.Keywords.GetById(keywordItem.KeywordId);
                            if (keywordData == null)
                                continue;
                            keywords.Add(new SmsKeyword
                                             {
                                                 Format = keywordData.Format,
                                                 Value = keywordData.Keyword,
                                                 Website = keywordData.Website
                                             });
                        }
                    }
                }

                if (number.Type == SmsNumberType.ShortCode)
                {
                    shortCodes.Add(new ShortCode
                                       {
                                           Number = number.Number,
                                           Keywords = keywords
                                       });
                }
                else
                {
                    longCodes.Add(new LongCode
                                             {
                                                 Number = number.Number,
                                                 Keywords = keywords,
                                                 RouteType = number.RouteType,
                                                 Website = number.Website,
                                                 Format = number.Format
                                             });
                }
            }

            shortCodes.Sort((first, second) => string.Compare(first.Number, second.Number, StringComparison.OrdinalIgnoreCase));
            longCodes.Sort((first, second) => string.Compare(first.Number, second.Number, StringComparison.OrdinalIgnoreCase));

            List<string> availableShortCodes = available.Select(item => item.Number).ToList();

            availableShortCodes.Sort();

            return new SmsGatewayModule
            {
                ShortCodes = shortCodes,
                LongCodes = longCodes,
                AvailableShortCodes = availableShortCodes
            };
        }

        public ShortCode GetShortCode(string licenseId, string number)
        {
            SmsNumberData shortCode = GetNumber(licenseId, number, SmsNumberType.ShortCode);

            return new ShortCode
            {
                Number = number,
                Keywords = GetKeywordsForNumber(shortCode)
            };
        }

        public ShortCode CreateShortCode(string licenseId, ShortCode shortCode)
        {
            ValidateShortCode(shortCode);

            Repository repository = new Repository();

            LicenseLogic licenseLogic = new LicenseLogic();

            License license = licenseLogic.GetLicense(licenseId);

            CheckLicenseForModule(license);

            // currently, a "catch all" short code does not exist

            // make sure the number is an available number
            if (repository.SmsGateway.AvailableNumbers.GetByNumber(shortCode.Number) == null)
                throw new ServiceException(HttpStatusCode.BadRequest, "The specified number is not available.");

            // make sure the number isn't in use by this license
            if (repository.SmsGateway.Numbers.GetByNumberAndLicenseId(shortCode.Number, license.Id) != null)
                throw new ServiceException(HttpStatusCode.BadRequest, "The specified number is already in use by this license.");

            if (shortCode.Keywords != null)
            {
                ValidateKeywords(shortCode.Keywords);

                ValidateShortcodeKeywordUniqueness(shortCode.Number, shortCode.Keywords);
            }

            SmsNumberData numberData = new SmsNumberData
            {
                Id = Guid.NewGuid(),
                LicenseId = license.Id,
                Number = shortCode.Number,
                Type = SmsNumberType.ShortCode,
                RouteType = SmsNumberRouteType.Keyword
            };

            numberData.Keywords = AddKeywords(license, shortCode.Keywords, numberData.Id, numberData.Number);

            repository.SmsGateway.Numbers.Add(numberData);

            return shortCode;
        }

        public ShortCode UpdateShortCode(string licenseId, string shortCodeId, ShortCode shortCode)
        {
            ValidateShortCode(shortCode);

            if (shortCode == null)
                throw new ServiceException(HttpStatusCode.BadRequest, "No short code specified in the message body.");

            Repository repository = new Repository();

            LicenseLogic licenseLogic = new LicenseLogic();

            License license = licenseLogic.GetLicense(licenseId);

            if (string.IsNullOrEmpty(shortCodeId))
                throw new ServiceException(HttpStatusCode.BadRequest, "No short code was specified.");

            CheckLicenseForModule(license);

            ValidateKeywords(shortCode.Keywords);

            SmsNumberData existingNumber = repository.SmsGateway.Numbers.GetByNumberAndLicenseId(shortCodeId, license.Id);

            if (existingNumber == null)
                throw new ServiceException(HttpStatusCode.BadRequest, "Short code does not exist.");

            if (shortCode.Number == null)
                shortCode.Number = existingNumber.Number;

            if (shortCode.Number != existingNumber.Number)
                throw new ServiceException(HttpStatusCode.BadRequest, "Modifying the short code number is not currently supported. Delete and re-add the short code to modify it.");

            if (existingNumber.Type != SmsNumberType.ShortCode)
                throw new ServiceException(HttpStatusCode.BadRequest, "The number is not a short code.");

            ValidateShortcodeKeywordUniqueness(shortCode.Number, shortCode.Keywords, existingNumber.Keywords);

            UpdateKeywords(existingNumber, license, shortCode.Keywords);

            repository.SmsGateway.Numbers.Update(existingNumber);

            return shortCode;
        }

        public ShortCode DeleteShortCode(string licenseId, string shortCodeId)
        {
            SmsNumberData numberData = GetNumber(licenseId, shortCodeId, SmsNumberType.ShortCode);

            List<SmsKeyword> keywords = DeleteKeywords(numberData, true);

            Repository repository = new Repository();
            repository.SmsGateway.Numbers.Delete(numberData);

            return new ShortCode
                       {
                           Number = numberData.Number,
                           Keywords = keywords
                       };
        }

        // called by module logic, not by the service
        public void DeleteModuleForLicense(License license)
        {
            Repository repository = new Repository();

            IEnumerable<SmsNumberData> numbers = repository.SmsGateway.Numbers.GetByLicenseId(license.Id);

            foreach (SmsNumberData number in numbers)
            {
                DeleteKeywords(number, false);

                repository.SmsGateway.Numbers.Delete(number);
            }
        }

        public AvailableLongCodeList SearchLongCodes(string licenseId, int offset, int count, string filter, SmsLongCodeFilterType filterType)
        {
            LicenseLogic licenseLogic = new LicenseLogic();

            License license = licenseLogic.GetLicense(licenseId);

            CheckLicenseForModule(license);

            ISmsService service = Dependency.Get<ISmsService>();

            AvailableLongCodeList longCodeList = service.GetAvailableNumbers(offset, count, filter, filterType);

            UpdateLongCodeList(longCodeList.Items);

            return longCodeList;
        }

        private void UpdateLongCodeList(IEnumerable<AvailableLongCode> longCodeList)
        {
            if (longCodeList == null)
                return;

            foreach (AvailableLongCode longCode in longCodeList)
                longCode.Number = PhoneNumberHelper.GetStandardNumber(longCode.Number);
        }

        public LongCode GetLongCode(string licenseId, string number)
        {
            SmsNumberData longCode = GetNumber(licenseId, number, SmsNumberType.LongCode);

            return new LongCode
            {
                Number = number,
                Keywords = GetKeywordsForNumber(longCode),
                Format = longCode.Format,
                RouteType = longCode.RouteType,
                Website = longCode.Website
            };
        }

        public LongCode PurchaseLongCode(string licenseId, LongCode longCode)
        {
            ValidateLongCode(longCode);

            if (string.IsNullOrEmpty(longCode.Number))
                throw new ServiceException(HttpStatusCode.BadRequest, "Long code number must be specified.");

            LicenseLogic licenseLogic = new LicenseLogic();

            License license = licenseLogic.GetLicense(licenseId);

            CheckLicenseForModule(license);

            Repository repository = new Repository();
            // make sure we don't have a duplicate (rare case)

            if (longCode.RouteType == SmsNumberRouteType.Keyword)
            {
                ValidateKeywords(longCode.Keywords);
            }
            else
            {
                longCode.Keywords = null;
            }

            if (repository.SmsGateway.Numbers.GetByNumberAndLicenseId(longCode.Number, license.Id) != null)
                throw new ServiceException(HttpStatusCode.BadRequest, "Long code already exists.");

            ISmsService service = Dependency.Get<ISmsService>();

            service.PurchaseLongCode(longCode.Number);

            SmsNumberData numberData = new SmsNumberData
            {
                Id = Guid.NewGuid(),
                LicenseId = license.Id,
                Number = longCode.Number,
                Type = SmsNumberType.LongCode,
                RouteType = longCode.RouteType,
                Website = longCode.Website,
                Format = longCode.Format
            };

            if (longCode.RouteType == SmsNumberRouteType.Keyword)
                numberData.Keywords = AddKeywords(license, longCode.Keywords, numberData.Id, numberData.Number);

            repository.SmsGateway.Numbers.Add(numberData);

            NumberDataLogUpdated(numberData);

            return longCode;
        }

        public LongCode UpdateLongCode(string licenseId, string longCodeNumber, LongCode longCode)
        {
            if (string.IsNullOrEmpty(longCodeNumber))
                throw new ServiceException(HttpStatusCode.NotFound, "No number was specified.");

            ValidateLongCode(longCode);

            Repository repository = new Repository();

            LicenseLogic licenseLogic = new LicenseLogic();

            License license = licenseLogic.GetLicense(licenseId);

            CheckLicenseForModule(license);

            SmsNumberData existingNumber = repository.SmsGateway.Numbers.GetByNumberAndLicenseId(longCodeNumber, license.Id);

            if (existingNumber == null || existingNumber.Type != SmsNumberType.LongCode)
                throw new ServiceException(HttpStatusCode.BadRequest, "Number does not exist.");

            if (longCode.Number == null)
                longCode.Number = existingNumber.Number;

            if (longCode.Number != existingNumber.Number)
                throw new ServiceException(HttpStatusCode.BadRequest, "Modifying the long code number is not supported.");

            existingNumber.RouteType = longCode.RouteType;
            existingNumber.Website = longCode.Website;
            existingNumber.Format = longCode.Format;

            // the existing number route type must be set before updating the keywords
            UpdateKeywords(existingNumber, license, longCode.Keywords);

            repository.SmsGateway.Numbers.Update(existingNumber);

            return longCode;
        }

        public LongCode DeleteLongCode(string licenseId, string longCodeNumber)
        {
            SmsNumberData numberData = GetNumber(licenseId, longCodeNumber, SmsNumberType.LongCode);

            Repository repository = new Repository();
            repository.SmsGateway.Numbers.Delete(numberData);

            List<SmsKeyword> keywords = DeleteKeywords(numberData, true);

            return new LongCode
            {
                Number = numberData.Number,
                Keywords = keywords,
                Format = numberData.Format,
                RouteType = numberData.RouteType,
                Website = numberData.Website
            };
        }

        #region Private Helper Methods

        private List<SmsKeyword> DeleteKeywords(SmsNumberData number, bool returnKeywords)
        {
            if (number == null)
                throw new ServiceException(HttpStatusCode.NotFound, "Number was not found.");

            if (number.Type == SmsNumberType.LongCode)
            {
                ISmsService service = Dependency.Get<ISmsService>();
                service.ReleaseLongCode(number.Number);

                NumberDataLogDeleted(number);
            }

            Repository repository = new Repository();

            List<SmsKeywordData> existingKeywords = repository.SmsGateway.Keywords.GetByNumberId(number.Id).ToList();

            foreach (SmsKeywordData keyword in existingKeywords)
            {
                KeywordDataLogDeleted(keyword, 0);
                repository.SmsGateway.Keywords.Delete(keyword);

                IEnumerable<SmsSession> sessions = repository.SmsGateway.Sessions.GetByServiceNumberAndKeywordAndState(number.Number, keyword.Keyword, SmsSessionState.Active);

                foreach (SmsSession session in sessions)
                {
                    SmsSubscriber subscriber = repository.SmsGateway.Subscribers.GetBySessionId(session.Id);
                    if (subscriber != null)
                    {
                        subscriber.Sessions.RemoveAll(item => item.SessionId == session.Id);

                        if (subscriber.Sessions.Count == 0)
                            repository.SmsGateway.Subscribers.Delete(subscriber);
                        else
                            repository.SmsGateway.Subscribers.Update(subscriber);
                    }

                    session.State = SmsSessionState.Deleted;
                    repository.SmsGateway.Sessions.Update(session);
                }
            }

            if (number.RouteType == SmsNumberRouteType.CatchAll)
            {
                IEnumerable<SmsSession> sessions = repository.SmsGateway.Sessions.GetByServiceNumberAndKeywordAndState(number.Number, null, SmsSessionState.Active);
                foreach (SmsSession session in sessions)
                {
                    SmsSubscriber subscriber = repository.SmsGateway.Subscribers.GetBySessionId(session.Id);
                    if (subscriber != null)
                    {
                        subscriber.Sessions.RemoveAll(item => item.SessionId == session.Id);

                        if (subscriber.Sessions.Count == 0)
                            repository.SmsGateway.Subscribers.Delete(subscriber);
                        else
                            repository.SmsGateway.Subscribers.Update(subscriber);
                    }

                    session.State = SmsSessionState.Deleted;
                    repository.SmsGateway.Sessions.Update(session);
                }
            }

            if (!returnKeywords)
                return null;

            List<SmsKeyword> keywords = null;
            if (number.RouteType == SmsNumberRouteType.Keyword)
            {
                keywords = new List<SmsKeyword>();

                foreach (SmsNumberKeywordItem keyword in number.Keywords)
                {
                    SmsKeywordData existingKeyword = existingKeywords.FirstOrDefault(item => item.Id == keyword.KeywordId);

                    if (existingKeyword == null)
                        continue;

                    keywords.Add(new SmsKeyword
                    {
                        Value = existingKeyword.Keyword,
                        Format = existingKeyword.Format,
                        Website = existingKeyword.Website
                    });
                }
            }

            return keywords;
        }

        private SmsKeywordData SmsKeywordDataFromSmsKeyword(SmsKeyword keyword, License license, Guid numberId, string numberValue)
        {
            return new SmsKeywordData
                {
                    Format = keyword.Format,
                    Keyword = keyword.Value,
                    LicenseId = license.Id,
                    Number = numberValue,
                    NumberId = numberId,
                    Website = keyword.Website
                };
        }

        private void ValidateShortCode(ShortCode shortCode)
        {
            if (shortCode == null)
                throw new ServiceException(HttpStatusCode.BadRequest, "No short code is specified in the message body.");


            if (!PhoneNumberHelper.IsValidPhoneNumber(shortCode.Number))
                throw new ServiceException(HttpStatusCode.BadRequest, "The number value is invalid.");

            if (shortCode.Keywords == null)
                shortCode.Keywords = new List<SmsKeyword>();

            ValidateKeywords(shortCode.Keywords);
        }

        private void ValidateLongCode(LongCode longCode)
        {
            if (longCode == null)
                throw new ServiceException(HttpStatusCode.BadRequest, "No long code is specified in the message body.");

            if (!PhoneNumberHelper.IsValidPhoneNumber(longCode.Number))
                throw new ServiceException(HttpStatusCode.BadRequest, "The longCode value is invalid.");

            longCode.Number = PhoneNumberHelper.GetStandardNumber(longCode.Number);

            if (longCode.RouteType == SmsNumberRouteType.Unknown)
                throw new ServiceException(HttpStatusCode.BadRequest, "Route type is required.");

            if (longCode.RouteType == SmsNumberRouteType.Keyword)
            {
                if (!string.IsNullOrEmpty(longCode.Website))
                    throw new ServiceException(HttpStatusCode.BadRequest, "Cannot specify website for a keyword number.");

                if (longCode.Keywords == null)
                    longCode.Keywords = new List<SmsKeyword>();
            }
            else
            {
                if (longCode.RouteType == SmsNumberRouteType.CatchAll && longCode.Keywords != null)
                    throw new ServiceException(HttpStatusCode.BadRequest, "Cannot specify keywords for a catch all number.");

                longCode.Website = ValidateWebsite(longCode.Website);
            }

            ValidateKeywords(longCode.Keywords);
        }

        private void ValidateKeywords(List<SmsKeyword> keywords)
        {
            if (keywords == null || keywords.Count == 0)
                return;

            List<string> uniqueKeywords = new List<string>();

            foreach (SmsKeyword keyword in keywords)
            {
                ValidateKeyword(keyword);

                if (string.IsNullOrEmpty(keyword.Value))
                    continue;

                if (uniqueKeywords.Contains(keyword.Value))
                    throw new ServiceException(HttpStatusCode.BadRequest, string.Format("Cannot add duplicate keyword: {0}.", keyword.Value));

                uniqueKeywords.Add(keyword.Value);
            }
        }

        private void ValidateShortcodeKeywordUniqueness(string number, IEnumerable<SmsKeyword> keywords, List<SmsNumberKeywordItem> existingKeywords = null)
        {
            Repository repository = null;

            foreach (SmsKeyword keyword in keywords)
            {
                if (string.IsNullOrEmpty(keyword.Value))
                    continue;

                if (existingKeywords != null && existingKeywords.Any(item => item.Keyword == keyword.Value))
                    continue;

                if (repository == null)
                    repository = new Repository();

                if (repository.SmsGateway.Keywords.GetByNumberAndKeyword(number, keyword.Value) != null)
                    throw new ServiceException(HttpStatusCode.BadRequest, string.Format("Keyword is already in use: {0}.", keyword.Value));
            }
        }

        private void ValidateKeyword(SmsKeyword keyword)
        {
            if (keyword == null)
                throw new ServiceException(HttpStatusCode.BadRequest, "The keyword format is invalid.");

            if (keyword.Value == null)
                keyword.Value = string.Empty;

            string keywordValue = keyword.Value;

            if (!string.IsNullOrEmpty(keywordValue) && (keywordValue.Length < Constant.MINIMUM_SMS_KEYWORD_LENGTH || keywordValue.Length > Constant.MAXIMUM_SMS_KEYWORD_LENGTH))
                throw new ServiceException(HttpStatusCode.BadRequest, string.Format("Keyword must be between {0} and {1} characters in length.", Constant.MINIMUM_SMS_KEYWORD_LENGTH, Constant.MAXIMUM_SMS_KEYWORD_LENGTH));

            if (!keywordValue.All(char.IsLetterOrDigit))
                throw new ServiceException(HttpStatusCode.BadRequest, "Only letters and numbers may be used in keywords.");

            keyword.Website = ValidateWebsite(keyword.Website);

            keyword.Value = keywordValue.ToUpper();
        }

        private string ValidateWebsite(string website)
        {
            if (string.IsNullOrEmpty(website))
                return website;

            Uri uri;
            if (!Uri.TryCreate(website, UriKind.Absolute, out uri))
            {
                website = "https://" + website;

                if (!Uri.TryCreate(website, UriKind.Absolute, out uri))
                    throw new ServiceException(HttpStatusCode.BadRequest, "The website specified is not a valid URI.");
            }

            return website;
        }

        private List<SmsKeyword> GetKeywordsForNumber(SmsNumberData number)
        {
            if (number.RouteType != SmsNumberRouteType.Keyword)
                return null;

            Repository repository = new Repository();

            List<SmsKeywordData> existingKeywords = repository.SmsGateway.Keywords.GetByNumberId(number.Id).ToList();

            List<SmsKeyword> keywords = new List<SmsKeyword>();

            foreach (SmsNumberKeywordItem keyword in number.Keywords)
            {
                SmsKeywordData existingKeyword = existingKeywords.FirstOrDefault(item => item.Id == keyword.KeywordId);
                if (existingKeyword == null)
                    continue;
                keywords.Add(new SmsKeyword
                {
                    Value = existingKeyword.Keyword,
                    Format = existingKeyword.Format,
                    Website = existingKeyword.Website
                });
            }

            return keywords;
        }

        private SmsNumberData GetSmsNumberByNumber(License license, string number, SmsNumberType type)
        {
            Repository repository = new Repository();

            if (string.IsNullOrEmpty(number))
                throw new ServiceException(HttpStatusCode.BadRequest, "No number specified.");

            SmsNumberData value = repository.SmsGateway.Numbers.GetByNumberAndLicenseId(number, license.Id);

            if (value == null || license.Id != value.LicenseId || value.Type != type)
                throw new ServiceException(HttpStatusCode.NotFound, "Number was not found.");

            return value;
        }

        private void CheckLicenseForModule(License license)
        {
            ServiceModule module = ModuleProvider.GetModule(ModuleType.SmsGateway);
            if (license.Modules == null || !license.Modules.Any(item => item == module.Type))
                throw new ServiceException(HttpStatusCode.BadRequest, "The license does not contain the SMS Gateway.");
        }

        private SmsNumberData GetNumber(string licenseId, string number, SmsNumberType type)
        {
            LicenseLogic licenseLogic = new LicenseLogic();

            License license = licenseLogic.GetLicense(licenseId);

            CheckLicenseForModule(license);

            SmsNumberData numberData = GetSmsNumberByNumber(license, number, type);

            if (numberData == null)
                throw new ServiceException(HttpStatusCode.NotFound, "The number was not found.");

            return numberData;
        }

        private List<SmsNumberKeywordItem> AddKeywords(License license, List<SmsKeyword> keywords, Guid numberId, string numberValue)
        {
            List<SmsNumberKeywordItem> keywordResults = new List<SmsNumberKeywordItem>();

            Repository repository = new Repository();

            if (keywords != null)
            {
                int keywordCount = keywords.Count;

                foreach (SmsKeyword keyword in keywords)
                {
                    SmsKeywordData keywordData = SmsKeywordDataFromSmsKeyword(keyword, license, numberId, numberValue);

                    repository.SmsGateway.Keywords.Add(keywordData);
                    KeywordDataLogCreated(keywordData, keywordCount);

                    keywordResults.Add(new SmsNumberKeywordItem { Keyword = keyword.Value, KeywordId = keywordData.Id });
                }
            }

            return keywordResults;
        }

        private bool KeywordsAreEqual(SmsKeyword keyword, SmsKeywordData keywordData)
        {
            return keyword.Value == keywordData.Keyword
                   && keyword.Format == keywordData.Format
                   && keyword.Website == keywordData.Website;
        }

        private void UpdateKeywords(SmsNumberData existingNumber, License license, List<SmsKeyword> keywords)
        {
            Repository repository = new Repository();

            List<SmsKeywordData> existingKeywords = repository.SmsGateway.Keywords.GetByNumberId(existingNumber.Id).ToList();

            int keywordCount = keywords == null ? 0 : keywords.Count;

            if (existingNumber.RouteType != SmsNumberRouteType.Keyword)
            {
                keywordCount = 0;
                existingNumber.Keywords = null;
            }
            else
            {
                if (existingNumber.Keywords == null)
                    existingNumber.Keywords = new List<SmsNumberKeywordItem>();

                for (int i = 0; i < keywordCount; i++)
                {
                    SmsKeyword keyword = keywords[i];

                    if (i >= existingNumber.Keywords.Count)
                    {
                        // new keyword
                        SmsKeywordData keywordData = SmsKeywordDataFromSmsKeyword(keyword, license, existingNumber.Id, existingNumber.Number);

                        repository.SmsGateway.Keywords.Add(keywordData);
                        KeywordDataLogCreated(keywordData, keywordCount);

                        existingNumber.Keywords.Add(new SmsNumberKeywordItem { Keyword = keyword.Value, KeywordId = keywordData.Id });
                    }
                    else
                    {
                        // existing keyword to update
                        SmsKeywordData existingKeyword =
                            existingKeywords.FirstOrDefault(item => item.Id == existingNumber.Keywords[i].KeywordId);

                        // update if it's different
                        if (existingKeyword != null)
                        {
                            existingKeywords.Remove(existingKeyword);

                            if (!KeywordsAreEqual(keyword, existingKeyword))
                            {
                                existingNumber.Keywords[i].Keyword = keyword.Value;
                                existingKeyword.Keyword = keyword.Value;
                                existingKeyword.Format = keyword.Format;
                                existingKeyword.Website = keyword.Website;

                                repository.SmsGateway.Keywords.Update(existingKeyword);

                                KeywordDataLogUpdated(existingKeyword);
                            }
                        }
                    }
                }

                // trim the keywords on the existing number, if necessary
                if (keywordCount < existingNumber.Keywords.Count)
                    existingNumber.Keywords.RemoveRange(keywordCount, existingNumber.Keywords.Count - keywordCount);
            }

            // delete removed keywords/orphans
            foreach (SmsKeywordData keywordData in existingKeywords)
            {
                KeywordDataLogDeleted(keywordData, keywordCount);

                repository.SmsGateway.Keywords.Delete(keywordData);
            }
        }

        private void KeywordDataLogCreated(SmsKeywordData newKeyword, int keywordCount)
        {
            Repository repository = new Repository();

            repository.SmsGateway.KeywordDataLogs.Add(new SmsKeywordDataLog(newKeyword));

            //Commenting this code out as we ran into an issue, but still want to keep it around for a bit.
            //  Issue: You can't decrement the count if one is deleted, but you also don't get an updated count for the next month unless someone edits
            //  a keyword to cause this code to run.  We will do the count at the time the report data is requested.
            //SmsNumberData number = repository.SmsGateway.Numbers.GetById(newKeyword.NumberId);

            ////We only want to update the report data if it is a short code keyword
            //if (number.Type == SmsNumberType.LongCode) return;

            //ReportLogic reportLogic = new ReportLogic();

            //Month month = (Month)Enum.Parse(typeof(Month), CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.UtcNow.Month));

            //int? shortCodeKeywordCount = keywordCount;

			//reportLogic.UpdateLicenseBillingReportData(newKeyword.LicenseId, month, DateTime.UtcNow.Year, null, null,
            //                                           shortCodeKeywordCount, null);
        }

        private void KeywordDataLogUpdated(SmsKeywordData existingKeyword)
        {
            Repository repository = new Repository();

            // existing SmsKeywordDataLog To Update
            SmsKeywordDataLog keywordDataLog =
                repository.SmsGateway.KeywordDataLogs.GetByKeywordId(existingKeyword.Id);

            //This shouldn't really ever happen, but to not throw an error when we deploy this for the first time we are doing it.
            if (keywordDataLog == null)
            {
                keywordDataLog = new SmsKeywordDataLog(existingKeyword)
                    {
                        UpdatedDate = DateTime.UtcNow,
                        Keyword = existingKeyword.Keyword
                    };

                repository.SmsGateway.KeywordDataLogs.Add(keywordDataLog);
            }
            else
            {
                keywordDataLog.UpdatedDate = DateTime.UtcNow;
                keywordDataLog.Keyword = existingKeyword.Keyword;

                repository.SmsGateway.KeywordDataLogs.Update(keywordDataLog);
            }
        }

        private void KeywordDataLogDeleted(SmsKeywordData existingKeyword, int keywordCount)
        {
            Repository repository = new Repository();

            // existing SmsKeywordDataLog To Update
            SmsKeywordDataLog keywordDataLog =
                repository.SmsGateway.KeywordDataLogs.GetByKeywordId(existingKeyword.Id);

            //This shouldn't really ever happen, but to not throw an error when we deploy this for the first time we are doing it.
            if (keywordDataLog == null)
            {
                keywordDataLog = new SmsKeywordDataLog(existingKeyword)
                    {
                        DeletedDate = DateTime.UtcNow,
                        Keyword = existingKeyword.Keyword
                    };

                repository.SmsGateway.KeywordDataLogs.Add(keywordDataLog);
            }
            else
            {
                keywordDataLog.DeletedDate = DateTime.UtcNow;

                repository.SmsGateway.KeywordDataLogs.Update(keywordDataLog);
            }

            //Commenting this code out as we ran into an issue, but still want to keep it around for a bit.
            //  Issue: You can't decrement the count if one is deleted, but you also don't get an updated count for the next month unless someone edits
            //  a keyword to cause this code to run.  We will do the count at the time the report data is requested.

            //SmsNumberData number = repository.SmsGateway.Numbers.GetById(existingKeyword.NumberId);

            ////We only want to update the report data if it is a short code keyword
            //if (number.Type == SmsNumberType.LongCode) return;

            //ReportLogic reportLogic = new ReportLogic();

			//Month month = (Month)Enum.Parse(typeof(Month), CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.UtcNow.Month));

            //int? shortCodeKeywordCount = keywordCount;

			//reportLogic.UpdateLicenseBillingReportData(existingKeyword.LicenseId, month, DateTime.UtcNow.Year, null, null,
            //                                           shortCodeKeywordCount, null);

        }

        private void NumberDataLogUpdated(SmsNumberData number)
        {
            Repository repository = new Repository();

            SmsNumberDataLog numberDataLog = repository.SmsGateway.NumberDataLogs.Get(number.Id);

            if (numberDataLog != null) return;

            numberDataLog = new SmsNumberDataLog(number);

            repository.SmsGateway.NumberDataLogs.Add(numberDataLog);
        }

        private void NumberDataLogDeleted(SmsNumberData number)
        {
            Repository repository = new Repository();

            SmsNumberDataLog numberDataLog = repository.SmsGateway.NumberDataLogs.Get(number.Id);

            if (numberDataLog == null) return;

			numberDataLog.DeletedDate = DateTime.UtcNow;

            repository.SmsGateway.NumberDataLogs.Update(numberDataLog);
        }


        #endregion Private Helper Methods
    }
}

