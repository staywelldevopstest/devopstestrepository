﻿using KswApi.Common.Configuration;
using KswApi.Common.Objects;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects.SmsGateway;
using KswApi.Ioc;
using KswApi.Logging;
using KswApi.Logic.Framework.Constants;
using KswApi.Logic.Framework.Enums;
using KswApi.Logic.Framework.Helpers;
using KswApi.Logic.Framework.Interfaces;
using KswApi.Logic.Report;
using KswApi.Poco.Sms;
using KswApi.Repositories;
using KswApi.Repositories.Enums;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;

namespace KswApi.Logic.SmsLogic
{
    public class SmsMessageLogic
    {
        //These should always be upper case as they should not be case sensitive
        private static readonly List<string> StopCommands = new List<string> { "END", "CANCEL", "UNSUBSCRIBE", "QUIT", "STOP" };
        private static readonly List<string> StopAllCommands = new List<string> { "ALL" };
        private static readonly List<string> HelpCommands = new List<string> { "HELP", "HLP" };

        #region Public Methods

        /// <summary>
        /// Method to send a message
        /// </summary>
        /// <param name="message">Message to Send</param>
        /// <param name="accessToken">The access token for the current request</param>
        /// <returns></returns>
        public IncomingMessage SendSmsMessage(IncomingMessage message, AccessToken accessToken)
        {
            if (message == null)
                throw new ServiceException(HttpStatusCode.BadRequest, "No message was specified in the message body.");

            if (string.IsNullOrEmpty(message.Body))
                throw new ServiceException(HttpStatusCode.BadRequest, "Message must have a body.");

            if (message.Body.Length > Constant.MAXIMUM_SMS_BODY_LENGTH)
                throw new ServiceException(HttpStatusCode.BadRequest, string.Format("Message body cannot exceed {0} characters.", Constant.MAXIMUM_SMS_BODY_LENGTH));

            //If any of these are null or empty string (and at this point session id was not supplied) we throw an exception
            if (string.IsNullOrWhiteSpace(message.From) && string.IsNullOrWhiteSpace(message.To))
                throw new ServiceException(HttpStatusCode.BadRequest, "From or To must be specified.");

            return SendMessageUsingNumbers(message, accessToken);
        }

        /// <summary>
        /// Method that Twilio calls when a message is received on their end for us.
        /// </summary>
        /// <param name="smsSid">Twilio TWIML Application SID</param>
        /// <param name="accountSid">Twilio Account SID</param>
        /// <param name="sourceNumber">From Number</param>
        /// <param name="destinationNumber">To Number</param>
        /// <param name="body">Message Body</param>
        /// <returns></returns>
        public void ProcessSmsMessage(string smsSid, string accountSid, string sourceNumber, string destinationNumber, string body)
        {
            string keyword1, keyword2;

            // ignore invalid requests
            if (!PhoneNumberHelper.IsValidPhoneNumber(sourceNumber) || !PhoneNumberHelper.IsValidPhoneNumber(destinationNumber))
                return;

            sourceNumber = PhoneNumberHelper.GetStandardNumber(sourceNumber);

            destinationNumber = PhoneNumberHelper.GetStandardNumber(destinationNumber);

            Repository repository = new Repository();

            SmsSubscriber subscriber = repository.SmsGateway.Subscribers.GetByNumbers(sourceNumber, destinationNumber);

            if (TryGetKeywordsFromMessageBody(body, out keyword1, out keyword2))
            {
                if (TryActionResponseMessage(subscriber, keyword1, body))
                    return;

                // look for a message action
                if (TryActionMessage(subscriber, sourceNumber, destinationNumber, body, keyword1, keyword2))
                    return;

                // look for a keyword in the message
                if (TryKeywordMessage(sourceNumber, destinationNumber, body, keyword1))
                    return;
            }

            // look for a catch-all number
            if (TryCatchAllMessage(subscriber, sourceNumber, destinationNumber, body))
                return;

            // if the most recent session doesn't handle it, we just ignore the message
            TryMostRecentSessionMessage(subscriber, sourceNumber, destinationNumber, body);
        }

        #endregion Public Methods

        #region Private Methods

        private IncomingMessage SendMessageUsingNumbers(IncomingMessage message, AccessToken accessToken)
        {
            if (accessToken == null || accessToken.LicenseId == null)
                throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.Common.INSUFFICIENT_RIGHTS);

            if (!PhoneNumberHelper.IsValidPhoneNumber(message.From))
                throw new ServiceException(HttpStatusCode.BadRequest, "The 'From' number is invalid.");

            if (!PhoneNumberHelper.IsValidPhoneNumber(message.To))
                throw new ServiceException(HttpStatusCode.BadRequest, "The 'To' number is invalid.");

            message.From = PhoneNumberHelper.GetStandardNumber(message.From);
            message.To = PhoneNumberHelper.GetStandardNumber(message.To);

            Repository repository = new Repository();

            SmsSession session;

            string number, keyword;
            Guid licenseId;

            //if there is no keyword then it is possible the client has a catch all
            if (string.IsNullOrEmpty(message.Keyword))
            {
                // see if this is a catch all
                SmsNumberData catchAll = repository.SmsGateway.Numbers.GetOneByNumber(message.From);

                if (catchAll == null || catchAll.RouteType != SmsNumberRouteType.CatchAll)
                    throw new ServiceException(HttpStatusCode.BadRequest, "You must have a catch-all in order to not supply a keyword");

                if (accessToken.LicenseId != catchAll.LicenseId)
                    throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.Common.INSUFFICIENT_RIGHTS);

                session = repository.SmsGateway.Sessions.GetByNumbersAndKeywordAndState(message.To, message.From, null, SmsSessionState.Active).FirstOrDefault();

                number = catchAll.Number;

                licenseId = catchAll.LicenseId;

                keyword = null;
            }
            else
            {
                SmsKeywordData keywordData = repository.SmsGateway.Keywords.GetByNumberAndKeyword(message.From, message.Keyword.ToUpper());

                if (keywordData == null)
                    throw new ServiceException(HttpStatusCode.BadRequest, "From and Keyword must match a keyword configuration for the module.");

                if (accessToken.LicenseId != keywordData.LicenseId)
                    throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.Common.INSUFFICIENT_RIGHTS);

                number = keywordData.Number;

                keyword = keywordData.Keyword;

                licenseId = keywordData.LicenseId;

                session = repository.SmsGateway.Sessions.GetByNumbersAndKeywordAndState(message.To, message.From, message.Keyword.ToUpper(), SmsSessionState.Active).FirstOrDefault();
            }

            // if the session exists and if it is active, update it
            // otherwise create it.  We want to create a new session is they are stopped or inactive or suspended.
            if (session != null && session.State == SmsSessionState.Active)
            {
                session.LastRequestDate = DateTime.UtcNow;

                repository.SmsGateway.Sessions.Update(session);
            }
            else
            {
                DateTime now = DateTime.UtcNow;

                session = new SmsSession
                {
                    DateAdded = now,
                    LicenseId = licenseId,
                    State = SmsSessionState.Active,
                    ServiceNumber = number,
                    SubscriberNumber = message.To,
                    Keyword = keyword,
                    LastRequestDate = now,
                    Origin = SmsSessionOrigin.Client,
                };

                repository.SmsGateway.Sessions.Add(session);
            }

            //If the client is trying to send a message to a user who has opt-ed out, throw exception
            if (session.State == SmsSessionState.Suspended || session.State == SmsSessionState.Stopped)
                throw new ServiceException(HttpStatusCode.Unauthorized, "Unable to send a message to a subscriber who has opt-ed out of the program.");

            UpdateSubscriberSession(session);

            SendSmsMessage(session.SubscriberNumber, session.ServiceNumber, message.Body, accessToken.LicenseId, message.Keyword);

            //Send the message and start a session if session is null
            return message;
        }

        private bool TryActionResponseMessage(SmsSubscriber subscriber, string keyword1, string body)
        {
            if (subscriber == null)
                return false;

            if (subscriber.State == SmsSubscriberState.Normal)
                return false;

            // Currently this only happens the multiple stop response
            if (subscriber.State != SmsSubscriberState.AwaitingStopResponse)
                throw new ArgumentException("Invalid value for SmsSubscriberState.");

            subscriber.State = SmsSubscriberState.Normal;

            if (subscriber.StateExpiration == null || DateTime.UtcNow <= subscriber.StateExpiration.Value)
            {
                subscriber.StateExpiration = null;

                // If it's a keyword, stop that keyword,
                // otherwise reset the subscriber state
                // and process as normal
                if (TryStopWithKeywordMessage(subscriber, subscriber.SubscriberNumber, subscriber.ServiceNumber, keyword1, body))
                    return true;
            }

            Repository repository = new Repository();

            subscriber.StateExpiration = null;

            repository.SmsGateway.Subscribers.Update(subscriber);

            return false;
        }

        /// <summary>
        /// This method will perform whatever actions are required for a Stop/Stop All and Help action
        /// </summary>
        /// <param name="subscriber">The subscriber</param>
        /// <param name="sourceNumber">This is the user's number</param>
        /// <param name="destinationNumber">The client's number (short/long code)</param>
        /// <param name="body">Message Body</param>
        /// <param name="keyword1">First keyword</param>
        /// <param name="keyword2">Second keyword</param>
        /// <returns></returns>
        private bool TryActionMessage(SmsSubscriber subscriber, string sourceNumber, string destinationNumber, string body, string keyword1, string keyword2)
        {
            InternalSmsAction action = GetAction(keyword1, keyword2);

            switch (action)
            {
                case InternalSmsAction.None:
                case InternalSmsAction.New:
                case InternalSmsAction.Continue:
                    return false;
                case InternalSmsAction.Stop:
                    ProcessStopMessage(subscriber, sourceNumber, destinationNumber, keyword2, body);
                    break;
                case InternalSmsAction.StopAll:
                    ProcessStopAllMessage(subscriber, sourceNumber, destinationNumber, body);
                    break;
                case InternalSmsAction.Help:
                    ProcessHelpMessage(sourceNumber, destinationNumber, body);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return true;
        }

        private void ProcessStopAllMessage(SmsSubscriber subscriber, string sourceNumber, string destinationNumber, string body)
        {
            //Get existing sessions to check for multiple subscriptions
            //If we don't have any subscriptions (or active sessions) we need to return a message stating such
            if (subscriber == null || subscriber.Sessions.Count == 0)
            {
                SendSmsMessage(sourceNumber, destinationNumber, Constant.NO_SUBSCRIPTIONS + "\n\n" + Constant.HELP_SIGNATURE, null, null);

                return;
            }

            List<string> keywords = new List<string>();
            List<SmsKeywordData> keywordDataList = new List<SmsKeywordData>();

            Repository repository = new Repository();

            repository.SmsGateway.Subscribers.Delete(subscriber);

            List<Guid> sessionLicenseIdList = new List<Guid>();

            foreach (SmsSession session in subscriber.Sessions.Select(sessionItem => repository.SmsGateway.Sessions.GetById(sessionItem.SessionId)).Where(session => session != null))
            {
                session.State = SmsSessionState.Stopped;

                session.LastRequestDate = DateTime.UtcNow;

                repository.SmsGateway.Sessions.Update(session);

                sessionLicenseIdList.Add(session.LicenseId);

                if (!string.IsNullOrEmpty(session.Keyword))
                {
                    keywords.Add(session.Keyword);

                    SmsKeywordData keywordData = repository.SmsGateway.Keywords.GetByNumberAndKeyword(destinationNumber, session.Keyword);

                    if (keywordData == null || string.IsNullOrEmpty(keywordData.Website))
                        continue;

                    keywordDataList.Add(keywordData);

                    ForwardSmsMessage(SmsMessageAction.Stop, keywordData.Format, keywordData.Website, session, "STOP");
                }
                else
                {
                    // this is a catch-all session

                    SmsNumberData catchAll = repository.SmsGateway.Numbers.GetOneByNumber(destinationNumber);

                    if (catchAll == null || string.IsNullOrEmpty(catchAll.Website))
                        continue;

                    ForwardSmsMessage(SmsMessageAction.Stop, catchAll.Format, catchAll.Website, session, "STOP");
                }
            }

            LogSmsMessage(sessionLicenseIdList[0], subscriber.ServiceNumber, subscriber.SubscriberNumber, body, null, MessageType.Request);

            if (keywords.Count == 0)
            {
                SendSmsMessage(sourceNumber, destinationNumber, Constant.STOP_CATCHALL_MESSAGE + "\n\n" + Constant.HELP_SIGNATURE, null, null);
            }
            else
            {
                Guid? licenseId = null;

                if (keywordDataList.Count > 0 && keywordDataList.All(item => true))
                    licenseId = keywordDataList.First().LicenseId;

                SendSmsMessage(sourceNumber, destinationNumber, string.Format(Constant.STOP_KEYWORD_MESSAGE, string.Join(", ", keywords)) + "\n\n" +
                               Constant.HELP_SIGNATURE, licenseId, null);
            }
        }

        private void ProcessStopMessage(SmsSubscriber subscriber, string sourceNumber, string destinationNumber, string keyword2, string body)
        {
            Repository repository = new Repository();

            string noSubHelpMessage = Constant.NO_SUBSCRIPTIONS + "\n\n" + Constant.HELP_SIGNATURE;

            //If we don't have any subscriptions (or active sessions) we need to return a message stating such
            if (subscriber == null || subscriber.Sessions.Count == 0)
            {
                SendSmsMessage(sourceNumber, destinationNumber, noSubHelpMessage, null, keyword2);

                return;
            }

            if (TryStopWithKeywordMessage(subscriber, sourceNumber, destinationNumber, keyword2, body))
                return;

            // multiple program subscription
            if (subscriber.Sessions.Count > 1)
            {
                ProcessMultipleProgramStop(subscriber, sourceNumber, destinationNumber, body);

                return;
            }

            // process a single stop message
            SmsSubscriberSessionItem sessionItem = subscriber.Sessions.First();

            SmsSession session = repository.SmsGateway.Sessions.GetById(sessionItem.SessionId);

            if (session == null || session.State != SmsSessionState.Active)
            {
                SendSmsMessage(sourceNumber, destinationNumber, noSubHelpMessage, null, null);
                return;
            }

            session.State = SmsSessionState.Stopped;

            session.LastRequestDate = DateTime.UtcNow;

            repository.SmsGateway.Sessions.Update(session);

            repository.SmsGateway.Subscribers.Delete(subscriber);

            if (session.Keyword != null)
            {
                SmsKeywordData keywordData = repository.SmsGateway.Keywords.GetByNumberAndKeyword(destinationNumber, session.Keyword);

                if (keywordData != null && !string.IsNullOrEmpty(keywordData.Website))
                    ForwardSmsMessage(SmsMessageAction.Stop, keywordData.Format, keywordData.Website, session, "STOP");

                string messageBody = string.Format(Constant.STOP_KEYWORD_MESSAGE, session.Keyword) + "\n\n" + Constant.HELP_SIGNATURE;

                //Log Message to SmsMessageLog
                if (keywordData == null)
                    SendSmsMessage(sourceNumber, destinationNumber, messageBody, null, null);
                else
                    SendSmsMessage(sourceNumber, destinationNumber, messageBody, keywordData.LicenseId, keywordData.Keyword);
            }
            else
            {
                // this is a catch-all session
                SmsNumberData catchAll = repository.SmsGateway.Numbers.GetOneByNumber(destinationNumber);

                if (catchAll != null && !string.IsNullOrEmpty(catchAll.Website))
                    ForwardSmsMessage(SmsMessageAction.Stop, catchAll.Format, catchAll.Website, session, "STOP");

                SendSmsMessage(sourceNumber, destinationNumber, Constant.STOP_CATCHALL_MESSAGE + "\n\n" + Constant.HELP_SIGNATURE, null, null);
            }

            LogSmsMessage(session, body, MessageType.Request);
        }

        private void ProcessMultipleProgramStop(SmsSubscriber subscriber, string sourceNumber, string destinationNumber, string body)
        {
            IEnumerable<string> keywords = subscriber.Sessions
                .Select(sessionItem => sessionItem.Keyword)
                .OrderBy(keyword => keyword);
            string keywordString = string.Join("\n", keywords);

            if (string.IsNullOrEmpty(keywordString))
                return;
            string stopString = string.Format("KSW: Which program to stop?\n{0}\nSTOP ALL", keywordString);

            subscriber.State = SmsSubscriberState.AwaitingStopResponse;

            subscriber.StateExpiration = DateTime.UtcNow + Settings.Current.SmsGateway.StopCommand.Expiration;

            Repository repository = new Repository();

            repository.SmsGateway.Subscribers.Update(subscriber);

            List<Guid> sessionIds = subscriber.Sessions.Select(item => item.SessionId).ToList();

            SmsSession session = repository.SmsGateway.Sessions.GetById(sessionIds[0]);

            LogSmsMessage(session, body, MessageType.Request);

            SendSmsMessage(sourceNumber, destinationNumber, stopString, session.LicenseId, null);
        }

        private bool TryStopWithKeywordMessage(SmsSubscriber subscriber, string sourceNumber, string destinationNumber, string keyword, string body)
        {
            if (string.IsNullOrEmpty(keyword))
                return false;

            Repository repository = new Repository();

            SmsKeywordData keywordData = repository.SmsGateway.Keywords.GetByNumberAndKeyword(destinationNumber, keyword);

            if (keywordData == null || string.IsNullOrEmpty(keywordData.Website))
                return false;

            //Check to see if there is a session with the keyword
            SmsSession session = repository.SmsGateway.Sessions.GetByNumbersAndKeywordAndState(sourceNumber, destinationNumber, keyword, SmsSessionState.Active).FirstOrDefault();

            if (session == null)
                return false;

            session.State = SmsSessionState.Stopped;

            session.LastRequestDate = DateTime.UtcNow;
            repository.SmsGateway.Sessions.Update(session);

            subscriber.Sessions.RemoveAll(sessionItem => sessionItem.SessionId == session.Id);

            repository.SmsGateway.Subscribers.Update(subscriber);

            ForwardSmsMessage(SmsMessageAction.Stop, keywordData.Format, keywordData.Website, session, "STOP");

            //We need to log the request.  We are logging the response in the SendSmsMessage function
            LogSmsMessage(session, body, MessageType.Request);

            SendSmsMessage(sourceNumber, destinationNumber, string.Format(Constant.STOP_KEYWORD_MESSAGE, keyword) + "\n\n" +
                                 Constant.HELP_SIGNATURE, keywordData.LicenseId, keywordData.Keyword);

            return true;
        }

        private void ProcessHelpMessage(string sourceNumber, string destinationNumber, string body)
        {
            Repository repository = new Repository();

            SmsSession session = repository.SmsGateway.Sessions.GetByNumbersAndState(sourceNumber, destinationNumber, SmsSessionState.Active).OrderByDescending(item => item.LastRequestDate).FirstOrDefault();

            LogSmsMessage(session, body, MessageType.Request);

            SendSmsMessage(sourceNumber, destinationNumber, Constant.SMS_HELP_MESSAGE, null, null);
        }

        private bool TryCatchAllMessage(SmsSubscriber subscriber, string sourceNumber, string destinationNumber, string body)
        {
            Repository repository = new Repository();

            // look to see if there's a catch all on this number
            SmsNumberData catchAll = repository.SmsGateway.Numbers.GetOneByNumber(destinationNumber);

            if (catchAll == null || string.IsNullOrEmpty(catchAll.Website))
                return false;

            SmsMessageAction messageAction = SmsMessageAction.Continue;

            // see if there's an active session
            SmsSession session = repository.SmsGateway.Sessions.GetByNumbersAndKeywordAndState(sourceNumber, destinationNumber, null, SmsSessionState.Active).FirstOrDefault();

            if (session != null)
            {
                session.State = SmsSessionState.Active;

                session.LastRequestDate = DateTime.UtcNow;

                repository.SmsGateway.Sessions.Update(session);
            }
            else // otherwise start a new session
            {
                DateTime now = DateTime.UtcNow;

                session = new SmsSession
                {
                    SubscriberNumber = sourceNumber,
                    LicenseId = catchAll.LicenseId,
                    ServiceNumber = destinationNumber,
                    DateAdded = now,
                    LastRequestDate = now,
                    State = SmsSessionState.Active,
                    Keyword = null,
                    Origin = SmsSessionOrigin.Subscriber
                };

                repository.SmsGateway.Sessions.Add(session);

                messageAction = SmsMessageAction.New;

                if (subscriber != null)
                    repository.SmsGateway.Subscribers.Delete(subscriber);

                subscriber = new SmsSubscriber
                                 {
                                     SubscriberNumber = sourceNumber,
                                     ServiceNumber = destinationNumber,
                                     Sessions = new List<SmsSubscriberSessionItem> { new SmsSubscriberSessionItem { Keyword = null, SessionId = session.Id } },
                                     State = SmsSubscriberState.Normal
                                 };

                repository.SmsGateway.Subscribers.Add(subscriber);
            }

            if (!string.IsNullOrEmpty(catchAll.Website))
                ForwardSmsMessage(messageAction, catchAll.Format, catchAll.Website, session, body);

            LogSmsMessage(session, body, MessageType.Request);

            return true;
        }

        /// <summary>
        /// Look for a keyword in the message, and process it if it's found.
        /// </summary>
        /// <returns></returns>
        private bool TryKeywordMessage(string sourceNumber, string destinationNumber, string body, string keyword)
        {
            Repository repository = new Repository();

            SmsKeywordData keywordData = repository.SmsGateway.Keywords.GetByNumberAndKeyword(destinationNumber, keyword);

            if (keywordData == null || string.IsNullOrEmpty(keywordData.Website))
                return false;

            SmsSubscriber subscriber = repository.SmsGateway.Subscribers.GetByNumbers(sourceNumber, destinationNumber);

            //New logic: for standards compliance, create a new session
            IEnumerable<SmsSession> activeSessions = repository.SmsGateway.Sessions.GetByNumbersAndKeywordAndState(sourceNumber, destinationNumber, keyword, SmsSessionState.Active);

            // inactivate any active sessions on the keyword
            foreach (SmsSession activeSession in activeSessions)
            {
                // remove from the subscriber
                if (subscriber != null)
                    subscriber.Sessions.RemoveAll(item => item.SessionId == activeSession.Id);

                // mark as inactive
                activeSession.State = SmsSessionState.Inactive;

                activeSession.LastRequestDate = DateTime.UtcNow;

                repository.SmsGateway.Sessions.Update(activeSession);
            }

            // always start a new session on a keyword message
            DateTime now = DateTime.UtcNow;

            SmsSession session = new SmsSession
                          {
                              LicenseId = keywordData.LicenseId,
                              SubscriberNumber = sourceNumber,
                              ServiceNumber = keywordData.Number,
                              DateAdded = now,
                              LastRequestDate = now,
                              State = SmsSessionState.Active,
                              Keyword = keywordData.Keyword,
                              Origin = SmsSessionOrigin.Subscriber
                          };

            repository.SmsGateway.Sessions.Add(session);

            if (subscriber == null)
            {
                subscriber = GetNewSubscriber(sourceNumber, destinationNumber);
                subscriber.Sessions.Add(new SmsSubscriberSessionItem { SessionId = session.Id, Keyword = keyword });
                repository.SmsGateway.Subscribers.Add(subscriber);
            }
            else
            {
                subscriber.Sessions.Insert(0, new SmsSubscriberSessionItem { SessionId = session.Id, Keyword = keyword });
                repository.SmsGateway.Subscribers.Update(subscriber);
            }

            //We need to log the request.  We are logging the response in the SendSmsMessage function
            LogSmsMessage(session, body, MessageType.Request);

            // trim the keyword and whitespace following the keyword from the message
            body = body.Substring(keyword.Length);
            body = body.TrimStart();

            ForwardSmsMessage(SmsMessageAction.New, keywordData.Format, keywordData.Website, session, body);

            return true;
        }

        /// <summary>
        /// Send the message to the most recent session
        /// </summary>
        /// <returns></returns>
        private bool TryMostRecentSessionMessage(SmsSubscriber subscriber, string sourceNumber, string destinationNumber, string body)
        {
            Repository repository = new Repository();

            // send the message to the most recent session

            if (subscriber == null || subscriber.Sessions.Count == 0)
                return false;

            // send to the first active session
            foreach (SmsSubscriberSessionItem sessionItem in subscriber.Sessions)
            {
                SmsSession session = repository.SmsGateway.Sessions.GetById(sessionItem.SessionId);

                if (session == null || session.State != SmsSessionState.Active)
                    continue;

                string website;
                MessageFormat format;

                if (session.Keyword != null)
                {
                    SmsKeywordData keywordData = repository.SmsGateway.Keywords.GetByNumberAndKeyword(destinationNumber, session.Keyword);

                    if (keywordData == null || string.IsNullOrEmpty(keywordData.Website))
                        continue;

                    format = keywordData.Format;
                    website = keywordData.Website;
                }
                else
                {
                    // no keyword, look for a catch all
                    SmsNumberData catchAll = repository.SmsGateway.Numbers.GetOneByNumber(destinationNumber);

                    if (catchAll == null || string.IsNullOrEmpty(catchAll.Website))
                        continue;

                    format = catchAll.Format;
                    website = catchAll.Website;
                }

                session.State = SmsSessionState.Active;

                session.LastRequestDate = DateTime.UtcNow;

                repository.SmsGateway.Sessions.Update(session);

                ForwardSmsMessage(SmsMessageAction.Continue, format, website, session, body);

                //We need to log the request.  We are logging the response in the SendSmsMessage function
                LogSmsMessage(session, body, MessageType.Request);

                return true;
            }

            return false;
        }

        private SmsSubscriber GetNewSubscriber(string subscriberNumber, string serviceNumber)
        {
            return new SmsSubscriber
            {
                SubscriberNumber = subscriberNumber,
                ServiceNumber = serviceNumber,
                Sessions = new List<SmsSubscriberSessionItem>()
            };
        }

        /// <summary>
        /// Sets the desired session (session) to be the most recent
        /// </summary>
        /// <param name="session">Session to use for this request</param>
        private void UpdateSubscriberSession(SmsSession session)
        {
            Repository repository = new Repository();

            SmsSubscriber subscriber = repository.SmsGateway.Subscribers.GetByNumbers(session.SubscriberNumber, session.ServiceNumber);

            if (subscriber == null)
            {
                subscriber = new SmsSubscriber
                                 {
                                     SubscriberNumber = session.SubscriberNumber,
                                     ServiceNumber = session.ServiceNumber,
                                     Sessions = new List<SmsSubscriberSessionItem> { new SmsSubscriberSessionItem { Keyword = session.Keyword, SessionId = session.Id } }
                                 };

                repository.SmsGateway.Subscribers.Add(subscriber);
            }
            else
            {
                if (subscriber.Sessions.Count == 0 || subscriber.Sessions[0].SessionId != session.Id)
                {
                    if (session.Keyword != null)
                        subscriber.Sessions.RemoveAll(sessionItem => sessionItem.SessionId == session.Id);
                    else
                        subscriber.Sessions.Clear();

                    subscriber.Sessions.Insert(0, new SmsSubscriberSessionItem { Keyword = session.Keyword, SessionId = session.Id });

                    repository.SmsGateway.Subscribers.Update(subscriber);
                }
            }
        }

        private void SendSmsMessage(string to, string from, string body, Guid? licenseId, string keyword)
        {
            ISmsService service = Dependency.Get<ISmsService>();

            if (body.Length <= Constant.MAXIMUM_SMS_MESSAGE_LENGTH)
            {
                service.SendMessage(to, from, body);
                LogSmsMessage(licenseId, from, to, body, keyword, MessageType.Response);

                return;
            }

            for (int currentIndex = 0, length = body.Length; length > 0; currentIndex += Constant.MAXIMUM_SMS_MESSAGE_LENGTH, length -= Constant.MAXIMUM_SMS_MESSAGE_LENGTH)
            {
                service.SendMessage(to, from, body.Substring(currentIndex, Math.Min(length, Constant.MAXIMUM_SMS_MESSAGE_LENGTH)));
                LogSmsMessage(licenseId, from, to, body.Substring(currentIndex, Math.Min(length, Constant.MAXIMUM_SMS_MESSAGE_LENGTH)), keyword, MessageType.Response);
            }
        }

        private void ForwardSmsMessage(SmsMessageAction action, MessageFormat format, string website, SmsSession session, string body)
        {
            OutgoingMessage message = new OutgoingMessage
            {
                Action = action,
                Body = body,
                From = session.SubscriberNumber,
                Keyword = session.Keyword,
                To = session.ServiceNumber
            };

            SendServiceMessage(format, website, message);
        }

        private InternalSmsAction GetAction(string keyword1, string keyword2)
        {
            if (string.IsNullOrEmpty(keyword1))
                return InternalSmsAction.Continue;

            //Need to find a commmand word
            //If it is the first word, ignore all other words unless the next word is all
            if (StopAllCommands.Any(action => keyword1.Equals(action, StringComparison.Ordinal)))
                return InternalSmsAction.StopAll;

            if (StopCommands.Any(action => keyword1.Equals(action, StringComparison.Ordinal)))
            {
                if (string.IsNullOrEmpty(keyword2))
                    return InternalSmsAction.Stop;

                if (StopAllCommands.Any(action => action.Equals(keyword2)))
                    return InternalSmsAction.StopAll;

                return InternalSmsAction.Stop;
            }

            if (HelpCommands.Any(action => keyword1.Equals(action, StringComparison.Ordinal)))
                return InternalSmsAction.Help;

            return InternalSmsAction.None;
        }

        private void SendServiceMessage(MessageFormat format, string website, OutgoingMessage message)
        {
            ISmsMessageForwarder sender = Dependency.Get<ISmsMessageForwarder>();

            try
            {
                sender.SendMessage(format, website, message);
            }
            catch (Exception ex)
            {
                // log the exception, but don't fail
                Logger.Log(ex);
            }

        }

        private bool TryGetKeywordsFromMessageBody(string body, out string keyword1, out string keyword2)
        {
            keyword1 = null;
            keyword2 = null;

            if (string.IsNullOrEmpty(body))
                return false;

            int firstSpace = body.IndexOf(' ');

            string keyword = firstSpace >= 0 ? body.Substring(0, firstSpace) : body;

            if (keyword.Length > Constant.MAXIMUM_SMS_KEYWORD_LENGTH || keyword.Length < Constant.MINIMUM_SMS_KEYWORD_LENGTH)
                return false;

            keyword1 = keyword.ToUpper();

            if (firstSpace < 0)
                return true;

            int secondSpace = body.IndexOf(' ', firstSpace + 1);

            keyword = secondSpace > 0 ? body.Substring(firstSpace + 1, secondSpace - firstSpace - 1) : body.Substring(firstSpace + 1);

            if (!string.IsNullOrEmpty(keyword))
                keyword2 = keyword.ToUpper();

            return true;
        }

        private static void LogSmsMessage(Guid? licenseId, string serviceNumber, string subscriberNumber, string body, string keyword, MessageType messageType)
        {
            Repository repository = new Repository();

            if (!licenseId.HasValue)
            {
                SmsSession session = repository.SmsGateway.Sessions.GetByNumbersAndState(subscriberNumber, serviceNumber,
                                                                                         SmsSessionState.Active).OrderByDescending(item => item.LastRequestDate).FirstOrDefault();

                if (session == null)
                    return;

                licenseId = session.LicenseId;
            }

            SmsMessageLog messageLog = new SmsMessageLog
            {
                LicenseId = licenseId.Value,
                ServiceNumber = serviceNumber,
                SubscriberNumber = subscriberNumber,
                Body = body,
				DateOfMessage = DateTime.UtcNow,
                Keyword = keyword,
                Type = messageType
            };

            repository.SmsGateway.MessageLogs.Add(messageLog);

            ReportLogic reportLogic = new ReportLogic();

			Month month = (Month)Enum.Parse(typeof(Month), CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.UtcNow.Month));

            int? outgoingMessage = null;
            int? incomingMessage = null;

            if (messageType == MessageType.Request)
                incomingMessage = 1;
            else
                outgoingMessage = 1;

			reportLogic.UpdateLicenseBillingReportData(licenseId.Value, month, DateTime.UtcNow.Year, outgoingMessage, incomingMessage);
        }

        private void LogSmsMessage(SmsSession session, string body, MessageType messageType)
        {
            if (session == null)
                return;

            SmsMessageLog messageLog = new SmsMessageLog
            {
                LicenseId = session.LicenseId,
                ServiceNumber = session.ServiceNumber,
                SubscriberNumber = session.SubscriberNumber,
                Body = body,
				DateOfMessage = DateTime.UtcNow,
                Keyword = session.Keyword,
                Type = messageType
            };

            Repository repository = new Repository();

            repository.SmsGateway.MessageLogs.Add(messageLog);

            ReportLogic reportLogic = new ReportLogic();

			Month month = (Month)Enum.Parse(typeof(Month), CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(DateTime.UtcNow.Month));

            int? outgoingMessage = null;
            int? incomingMessage = null;

            if (messageType == MessageType.Request)
                incomingMessage = 1;
            else
                outgoingMessage = 1;

			reportLogic.UpdateLicenseBillingReportData(session.LicenseId, month, DateTime.UtcNow.Year, outgoingMessage, incomingMessage);
        }

        #endregion Private Methods
    }
}
