﻿using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.Administration;
using KswApi.Logic.Framework;
using KswApi.Logic.Framework.Helpers;
using KswApi.Logic.Framework.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using KswApi.Logic.Framework.Constants;
using KswApi.Poco.Content;

namespace KswApi.Logic.ContentLogic
{
	public class ContentModuleLogic : LogicBase
	{
		#region Public Methods

		public ContentModule GetModule(string licenseId)
		{
			Validate(licenseId);

			Guid licenseIdParsed = Guid.Parse(licenseId);

			ContentModule module = Repository.Content.ContentModule.GetByLicenseId(licenseIdParsed);
			if (module == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.ContentModule.NO_CONTENT_MODULE);
			return module;
		}

		public UnpagedContentBucketList GetBucketsAssignedToLicense(string licenseId)
		{
			ContentModule contentModule = GetModule(licenseId);

			if (contentModule.BucketIds == null || contentModule.BucketIds.Count == 0)
			{
				return new UnpagedContentBucketList();
			}

			return CreateBucketList(contentModule.BucketIds.Select(x => x.ToString()));
		}

		public IEnumerable<ContentModule> GetByBucketId(string bucketId)
		{
			if (string.IsNullOrEmpty(bucketId))
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.ContentModule.NO_BUCKET_ID);
			}

			Guid bucketIdParsed = Guid.Parse(bucketId);


			return Repository.Content.ContentModule.GetByBucketId(bucketIdParsed);
		}

		public ContentModule CreateContentModule(string licenseId)
		{
			if (string.IsNullOrEmpty(licenseId))
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.ContentModule.NO_LICENSE_ID);
			}

			ContentModule contentModule = new ContentModule();
			contentModule.LicenseId = Guid.Parse(licenseId);

			Repository.Content.ContentModule.Add(contentModule);

			return contentModule;
		}

		public UnpagedContentBucketList UpdateBucketsAssignedToLicense(string licenseId, ContentBucketIdListRequest bucketListIds)
		{
			License license = Validate(licenseId);

			if (bucketListIds == null)
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.ContentModule.NO_BUCKET_ID_LIST_SPECIFIED);
			}

			if (bucketListIds.Items == null)
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.ITEMS_NOT_SPECIFIED);
			}


			ContentModule contentModule = Repository.Content.ContentModule.GetByLicenseId(license.Id);

			List<Guid> bucketIds = new List<Guid>();

			foreach (string bucketId in bucketListIds.Items)
			{
				Guid guid;
				if (!Guid.TryParse(bucketId, out guid))
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.BUCKET_NOT_FOUND);

				bucketIds.Add(guid);
			}

			if (bucketIds.Count > 0)
			{
				if (bucketIds.GroupBy(item => item).Any(item => item.Count() > 1))
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.ContentModule.DUPLICATE_BUCKET);

				int countActive = Repository.Content.Buckets.CountByIdsAndStatus(bucketIds, ObjectStatus.Active);
				if (countActive != bucketIds.Count)
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.BUCKET_NOT_FOUND);
			}

			if (contentModule == null)
			{
				contentModule = new ContentModule
				{
					LicenseId = license.Id,
					BucketIds = bucketIds
				};

				Repository.Content.ContentModule.Add(contentModule);
			}
			else
			{
				contentModule.BucketIds = bucketIds;
				Repository.Content.ContentModule.Update(contentModule);
			}

			return CreateBucketList(bucketListIds.Items);
		}

		public CollectionListResponse GetCollectionsInLicense(string licenseId, CollectionSearchRequest request)
		{
			if (request == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.MISSING_MESSAGE_BODY);

			if (request.Offset == null)
				request.Offset = 0;

			if (request.Count == null)
				request.Count = Constant.Collections.MAXIMUM_LICENSED_COLLECTION_COUNT; 

			// allow any number of results
			ValidationHelper.ValidateSearch(request.Offset.Value, 0);

			License license = Validate(licenseId);

			CollectionLogic collectionLogic = new CollectionLogic();

			return collectionLogic.SearchCollections(request, license.Id, null);
		}

		public UnpagedCollectionListResponse AddCollectionsToLicense(string licenseId, CollectionIdListRequest collectionList)
		{
			License license = Validate(licenseId);

			List<Guid> ids = new List<Guid>();
			List<string> slugs = new List<string>();

			foreach (string value in collectionList.Items)
			{
				if (string.IsNullOrEmpty(value))
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.NULL_ITEMS);
				Guid guid;
				if (Guid.TryParse(value, out guid))
					ids.Add(guid);
				else
					slugs.Add(value);
			}

			List<Topic> topics = null;

			if (ids.Count > 0)
			{
				List<Topic> topicsById = Repository.Content.Topics.GetByIds(ids).ToList();

				if (topicsById.Count < ids.Count)
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.ONE_OR_MORE_COLLECTIONS_NOT_FOUND);

				topics = topicsById;
			}

			if (slugs.Count > 0)
			{
				List<Topic> topicsBySlug = Repository.Content.Topics.GetBySlugs(slugs).ToList();
				if (topicsBySlug.Count < slugs.Count)
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.ONE_OR_MORE_COLLECTIONS_NOT_FOUND);

				ids.AddRange(topicsBySlug.Select(item => item.Id));

				if (topics == null)
					topics = topicsBySlug;
				else
					topics.AddRange(topicsBySlug);
			}

			if (topics == null)
				return new UnpagedCollectionListResponse { Items = new List<CollectionResponse>() };

			if (topics.Any(item => item.Type != TopicType.Root))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.ONE_OR_MORE_COLLECTIONS_NOT_FOUND);

			IEnumerable<LicensedCollection> licensedCollections = Repository.Content.LicensedCollections.GetByLicenseIdAndCollectionIds(license.Id, ids);

			Dictionary<Guid, LicensedCollection> licensedCollectionLookup = licensedCollections.ToDictionary(item => item.CollectionId);

			foreach (Guid collectionId in ids)
			{
				LicensedCollection licensedCollection;
				if (!licensedCollectionLookup.TryGetValue(collectionId, out licensedCollection))
				{
					Repository.Content.LicensedCollections.Add(new LicensedCollection
															   {
																   LicenseId = license.Id,
																   CollectionId = collectionId
															   });

					Repository.Content.Collections.IncrementLicenseCount(collectionId, 1);
				}
			}

			IEnumerable<Collection> collections = Repository.Content.Collections.GetByIds(ids);

			CollectionLogic collectionLogic = new CollectionLogic();

			return new UnpagedCollectionListResponse
				   {
					   Items = collectionLogic.GetResponses(ids, collections, topics, license.Id)
				   };
		}

		public CollectionResponse RemoveCollectionFromLicense(string licenseId, string collectionId)
		{
			if (string.IsNullOrEmpty(collectionId))
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Collection.COLLECTION_NOT_FOUND);

			License license = Validate(licenseId);

			Topic topic;

			Guid id;

			if (Guid.TryParse(collectionId, out id))
			{
				topic = Repository.Content.Topics.GetById(id);
			}
			else
			{
				topic = Repository.Content.Topics.GetBySlug(collectionId);
			}

			if (topic == null || topic.Type != TopicType.Root)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Collection.COLLECTION_NOT_FOUND);

			Collection collection = Repository.Content.Collections.GetById(topic.Id);

			if (collection == null)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Collection.COLLECTION_NOT_FOUND);

			Repository.Content.LicensedCollections.DeleteByLicenseIdAndCollectionId(license.Id, collection.Id);

			Repository.Content.Collections.IncrementLicenseCount(topic.Id, -1);

			CollectionLogic collectionLogic = new CollectionLogic();

			return collectionLogic.ResponseFromCollection(collection, topic, license.Id);
		}

		public void DeleteModuleForLicense(License license)
		{
			if (license == null)
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.ContentModule.NO_LICENSE_ID);
			}

			ContentModule contentModule = Repository.Content.ContentModule.GetByLicenseId(license.Id);
			if (contentModule != null)
			{
				Repository.Content.ContentModule.Delete(contentModule);
			}
		}

		#endregion

		#region Private Methods

		private License Validate(string licenseId)
		{
			if (string.IsNullOrEmpty(licenseId))
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.ContentModule.NO_LICENSE_ID);
			}

			LicenseLogic licenseLogic = new LicenseLogic();
			License license = licenseLogic.GetLicense(licenseId);

			ServiceModule module = ModuleProvider.GetModule(ModuleType.Content);
			if (license.Modules == null || !license.Modules.Any(item => item == module.Type))
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.ContentModule.NO_CONTENT_MODULE);
			}

			return license;
		}

		private UnpagedContentBucketList CreateBucketList(IEnumerable<string> bucketIds)
		{
			UnpagedContentBucketList bucketList = new UnpagedContentBucketList();
			bucketList.Items = new List<ContentBucketResponse>();

			BucketLogic bucketLogic = new BucketLogic();
			foreach (string bucketId in bucketIds)
			{
				ContentBucketResponse bucketResponse = bucketLogic.GetBucket(bucketId);

				bucketList.Items.Add(bucketResponse);
			}

			return bucketList;
		}

		#endregion
	}
}
