﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using KswApi.Common.Configuration;
using KswApi.Common.Objects;
using KswApi.Indexing;
using KswApi.Indexing.Filters;
using KswApi.Indexing.Objects;
using KswApi.Indexing.Queries;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.Content;
using KswApi.Ioc;
using KswApi.Logic.Framework;
using KswApi.Logic.Framework.Constants;
using KswApi.Logic.Framework.Helpers;
using KswApi.Logic.Framework.Objects;
using KswApi.Logic.Framework.Providers;
using KswApi.Poco.Content;
using KswApi.Repositories;
using KswApi.Repositories.Enums;
using KswApi.Repositories.Objects;
using KswApi.Utility.Helpers;

namespace KswApi.Logic.ContentLogic
{
	public class ContentLogic : LogicBase
	{
		#region Public Methods

		public ContentList SearchContent(ContentSearchRequest request, Application application)
		{
			if (request == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.MISSING_MESSAGE_BODY);

			if (request.Count > Constant.Content.MAXIMUM_SEARCH_RESULTS)
				throw new ServiceException(HttpStatusCode.BadRequest, string.Format(ErrorMessage.Content.SEARCH_RESULT_COUNT_TOO_LARGE, Constant.Content.MAXIMUM_SEARCH_RESULTS));

			IIndex index = Dependency.Get<IIndex>();

			List<SearchFilter> filters = new List<SearchFilter>();

			// filter by buckets

			UpdateAllowedBuckets(request, application);

			if (request.Buckets != null)
			{
				if (request.Buckets.Count == 0)
					return GetEmptyContentList();

				filters.Add(new MultiTermFilter("bucket-id", request.Buckets.Select(item => (object)item).ToList()));
			}

			if (application != null && application.LicenseId != null)
			{
				License license = Repository.Administration.Licenses.GetById(application.LicenseId.Value);

				if (license == null)
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.License.INVALID_LICENSE);

				if (!license.Modules.Any(item => item == ModuleType.ManageContent))
				{
					CheckLicenseForModule(license);

					List<LicensedCollection> collections = Repository.Content.LicensedCollections.GetByLicenseId(license.Id).ToList();
					if (collections.Count == 0)
						return GetEmptyContentList();

					List<object> collectionIds = collections.Select(item => (object)item.CollectionId).ToList();

					filters.Add(new MultiTermFilter("collection-id", collectionIds));
				}
			}

			// filter by starts with
			if (!string.IsNullOrEmpty(request.TitleStartsWith))
			{
				if (request.IncludeAlternateTitles.HasValue && request.IncludeAlternateTitles.Value)
				{
					filters.Add(new OrFilter(
						new PrefixFilter("exact-title", request.TitleStartsWith.ToLower()),
						new PrefixFilter("exact-alternate-title", request.TitleStartsWith.ToLower())));
				}
				else
				{
					filters.Add(new PrefixFilter("exact-title", request.TitleStartsWith.ToLower()));
				}
			}

			// filter by language
			if (request.Languages != null)
			{
				if (request.Languages.Count == 0)
					return GetEmptyContentList();

				filters.Add(new MultiTermFilter("language", request.Languages.Select(item => (object)LanguageProvider.GetStandardCode(item)).ToList()));
			}

			// filter by legacy id
			if (request.LegacyIds != null && request.LegacyIds.Count > 0)
			{
				List<string> legacyIds = request.LegacyIds.Where(item => !string.IsNullOrWhiteSpace(item)).ToList();
				if (legacyIds.Count > 0)
					filters.Add(new TermFilter("legacy-id", legacyIds));
			}

			if (request.Formats != null && request.Formats.Count > 0)
			{
				filters.Add(new MultiTermFilter("format", request.Formats.Select(item => (object)item.ToString()).ToList()));
			}

			SearchFilter filter = null;

			if (filters.Count > 0)
			{
				filter = filters.Count == 1 ? filters[0] : new AndFilter(filters);
			}

			SearchQuery searchQuery = filter == null
										  ? SearchQuery.FromString(request.Query)
										  : new FilteredQuery(SearchQuery.FromString(request.Query), filter);

			IndexType[] types = GetIndexTypes(request);

			bool imagesIncluded = types.Contains(IndexType.Image);
			bool published = types.Contains(IndexType.Published);

			List<string> facets = imagesIncluded ? new List<string> { "format" } : null;

			List<SearchSort> sort = string.IsNullOrEmpty(request.Query)
										? new List<SearchSort> { new SearchSort("exact-title", "asc", "_last") }
										: null;

			SearchResponse list = index.Search(request.Offset, request.Count, searchQuery, null, facets, sort, types);

			TypeCount typeCount = null;

			if (imagesIncluded && list.Facets != null)
			{
				FacetResponse facetResponse = list.Facets["format"];

				typeCount = new TypeCount
				{
					Missing = facetResponse.Missing,
					Other = facetResponse.Other,
					Total = facetResponse.Total,
					Types = new Dictionary<ImageFormatType, int>()
				};

				foreach (TermCountResponse facet in facetResponse.Terms)
				{
					ImageFormatType type;
					if (Enum.TryParse(facet.Term as string, true, out type))
					{
						typeCount.Types[type] = facet.Count;
					}
				}
			}

			return new ContentList
					   {
						   Items = GetSearchResponse(list, published),
						   SortedBy = new SortOption("Relevance", SortDirection.Ascending).ToString(),
						   Offset = request.Offset,
						   Total = list.Hits.Total,
						   TypeCounts = typeCount
					   };
		}

		public ContentArticleResponse GetContent(string bucketIdOrSlug, string contentIdOrSlug, bool includeBody, AccessToken accessToken, bool draft, DateTime? time)
		{
			if (!time.HasValue)
				return GetContent(bucketIdOrSlug, contentIdOrSlug, includeBody, accessToken, draft);

			ContentRevisionType[] revisionTypes = draft
					? new[] { ContentRevisionType.Draft, ContentRevisionType.Created }
					: new[] { ContentRevisionType.Published };

			ContentHistory historyItem = GetHistoryByRevisionTypeOnOrBeforeDate(bucketIdOrSlug, contentIdOrSlug, revisionTypes, time.Value);
			if (historyItem == null)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);

			// Should this require access token?
			return GetHistoryContent(bucketIdOrSlug, contentIdOrSlug, historyItem.Id, includeBody);
		}

		public ContentArticleResponse GetContent(string bucketIdOrSlug, string contentId, bool includeBody, AccessToken accessToken, bool draft)
		{
			Repository repository = null;

			ContentModule contentModule = null;

			License license = null;

			if (accessToken != null && accessToken.LicenseId != null)
			{
				repository = new Repository();

				license = repository.Administration.Licenses.GetById(accessToken.LicenseId.Value);

				if (license == null)
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.License.INVALID_LICENSE);

				CheckLicenseForModule(license);

				contentModule = repository.Content.ContentModule.GetByLicenseId(accessToken.LicenseId.Value);

				if (contentModule == null)
					throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);
			}

			ContentVersion version = draft
										 ? GetContentVersion(bucketIdOrSlug, contentId)
										 : GetPublishedVersion(bucketIdOrSlug, contentId);

			if (contentModule != null)
			{
				if (contentModule.BucketIds == null)
					throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);

				List<Guid> allowedBuckets = contentModule.BucketIds;

				if (!allowedBuckets.Contains(version.BucketId))
					throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);

				List<LicensedCollection> collections = Repository.Content.LicensedCollections.GetByLicenseId(license.Id).ToList();
				if (collections.Count == 0)
					throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);

				List<Guid> collectionIds = collections.Select(item => item.CollectionId).ToList();

				if (!Repository.Content.CollectionStructures.ExistsByItemIdAndCollectionId(version.Id, collectionIds))
					throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);
			}

			if (repository == null)
				repository = new Repository();

			ContentCore core = repository.Content.Cores.GetById(version.CoreId);

			if (core == null)
				throw new ServiceException(HttpStatusCode.InternalServerError, ErrorMessage.Content.ORPHANED_CONTENT_NO_CORE);

			ContentBucket bucket = repository.Content.Buckets.GetById(version.BucketId);

			if (bucket == null)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.ORPHANED_CONTENT_NO_BUCKET);

			ContentArticleResponse response = GetResponse<ContentArticleResponse>(core, version, bucket, repository.Content.Origins.GetById(bucket.OriginId));

			response.Copyright = GetCopyright(core.CopyrightId, bucket);

			if (includeBody)
				response.Segments = GetSegments(version.Segments, bucket, license);

			return response;
		}

		public ContentMetadataResponse CreateContent(string bucketIdOrSlug, NewContentRequest request)
		{
			// validate the content
			ValidateRequest(request);

			request.Title = request.Title.Trim();

			if (request.MasterId == null)
				ValidateMasterRequest(request);

			Repository repository = new Repository();

			// creation-specific validation
			if (request.LanguageCode == null)
				request.LanguageCode = Constant.Content.ENGLISH_LANGUAGE_CODE;
			else
				request.LanguageCode = LanguageProvider.GetStandardCode(request.LanguageCode, true);

			if (string.IsNullOrEmpty(request.Slug))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.SLUG_REQUIRED);

			request.Slug = request.Slug.ToLower();

			ValidationHelper.ValidateSlug(request.Slug);

			BucketLogic bucketLogic = new BucketLogic();

			ContentBucket bucket = bucketLogic.InternalGetBucket(bucketIdOrSlug);

			ValidateSegments(request, bucket);

			// check slug uniqueness
			ContentVersion duplicate = repository.Content.Versions.GetByPath(bucket.Slug, request.Slug);

			if (duplicate != null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.DUPLICATE_SLUG);

			DateTime now = DateTime.UtcNow;

			Guid id = Guid.NewGuid();

			ContentVersion version = new ContentVersion
											  {
												  Title = request.Title,
												  InvertedTitle = request.InvertedTitle,
												  Blurb = request.Blurb,
												  AlternateTitles = request.AlternateTitles,
												  Id = id,
												  BucketId = bucket.Id,
												  DateAdded = now,
												  DateModified = now,
												  Status = ObjectStatus.Active,
												  Language = request.LanguageCode,
												  Slug = request.Slug,
												  LegacyId = request.LegacyId,
												  Paths = new List<ContentPath>
				                                          {
					                                          new ContentPath {BucketIdOrSlug = bucket.Slug, ContentIdOrSlug = request.Slug},
					                                          new ContentPath {BucketIdOrSlug = bucket.Id.ToString(), ContentIdOrSlug = request.Slug},
					                                          new ContentPath {BucketIdOrSlug = bucket.Slug, ContentIdOrSlug = id.ToString()},
					                                          new ContentPath {BucketIdOrSlug = bucket.Id.ToString(),ContentIdOrSlug = id.ToString()}
				                                          },
												  Published = request.Publish,
												  DateUpdated = now
											  };

			if (request.Publish)
				version.DatePublished = now;

			// add a sibling version if applicable
			if (request.MasterId != null)
				return SaveNewSibling(request, version, bucket);

			// otherwise save a master version
			return SaveNewMaster(request, version, bucket);
		}

		public ContentMetadataResponse UpdateContent(string bucketId, string contentId, ContentArticleRequest request)
		{
			// validate the content

			ValidateRequest(request);

			request.Title = request.Title.Trim();

			ContentVersion version = GetContentVersion(bucketId, contentId);

			if (version.IsMaster)
				ValidateMasterRequest(request);

			ContentBucket bucket = GetBucket(version.BucketId);

			ValidateSegments(request, bucket);

			Repository repository = new Repository();

			ContentCore core = repository.Content.Cores.GetById(version.CoreId);

			if (core == null)
				throw new ServiceException(HttpStatusCode.InternalServerError, ErrorMessage.Content.ORPHANED_CONTENT_NO_CORE);

			DateTime now = DateTime.UtcNow;

			version.LegacyId = request.LegacyId;
			version.Title = request.Title;
			version.InvertedTitle = request.InvertedTitle;
			version.Blurb = request.Blurb;
			version.AlternateTitles = request.AlternateTitles;
			version.DateModified = now;
			version.DateUpdated = now;
			version.Published = version.Published || request.Publish;

			if (request.Publish)
				version.DatePublished = now;

			// (we no longer delete old segments because they're used in history)

			if (version.IsMaster)
				return SaveUpdatedMaster(request, core, version, bucket);

			return SaveUpdatedSibling(request, core, version, bucket);
		}

		public ContentResponse DeleteContent(string bucketIdOrSlug, string contentIdOrSlug, bool? publishedOnly)
		{
			if (publishedOnly.HasValue && publishedOnly.Value)
				return DeletePublished(bucketIdOrSlug, contentIdOrSlug);

			return DeleteDraftAndPublished(bucketIdOrSlug, contentIdOrSlug);
		}

		public SlugResponse GetSlug(string bucketIdOrSlug, string title, string slug)
		{
			if (string.IsNullOrEmpty(slug) && string.IsNullOrWhiteSpace(title))
				return CreateSlugResponse(string.Empty);

			if (string.IsNullOrEmpty(slug))
				slug = SlugHelper.CreateSlug(title.Trim());
			else
			{
				slug = slug.ToLower();
				ValidationHelper.ValidateSlug(slug);
			}

			Repository repository = new Repository();

			ContentVersion version = repository.Content.Versions.GetByPath(bucketIdOrSlug, slug);

			if (version == null)
				return CreateSlugResponse(slug);

			// find next available slug
			int min = 1, current = min, max = min;

			while (true)
			{
				string candidate = slug + '-' + current;
				// logarithmic search
				version = repository.Content.Versions.GetByPath(bucketIdOrSlug, candidate);
				if (version == null)
				{
					if (min >= current)
						return CreateSlugResponse(candidate);

					max = current;
					current = (max + min) / 2;
				}
				else
				{
					min = current + 1;

					if (current >= max)
						current = current * 2;
					else
						current = (min + max) / 2;
				}
			}
		}

		public LanguageList GetLanguages(string bucketIdOrSlug, string contentIdOrSlug)
		{
			if (string.IsNullOrEmpty(contentIdOrSlug))
			{
				return new LanguageList
					   {
						   Items = LanguageProvider.GetAll()
					   };
			}

			ContentVersion version = GetContentVersion(bucketIdOrSlug, contentIdOrSlug);

			Repository repository = new Repository();
			ContentCore core = repository.Content.Cores.GetById(version.CoreId);

			if (core == null)
				throw new ServiceException(HttpStatusCode.InternalServerError, ErrorMessage.Content.ORPHANED_CONTENT_NO_CORE);

			return new LanguageList
				   {
					   Items =
						   LanguageProvider.GetAll()
						   .Where(item => !core.Versions.Any(coreVersion => coreVersion.Language == item.Code))
						   .ToList()
				   };
		}

		public void Reindex()
		{
			Repository repository = new Repository();
			ContentIndexer indexer = new ContentIndexer();

			IEnumerable<ContentVersion> versions = repository.Content.Versions.GetAll();

			foreach (ContentVersion version in versions)
			{
				Reindex(IndexType.Content, indexer, repository, version);
			}

			IEnumerable<ContentPublished> publishedVersions = repository.Content.Published.GetAll();

			foreach (ContentPublished version in publishedVersions)
			{
				Reindex(IndexType.Published, indexer, repository, version);
			}
		}

		#region History Methods

		public ContentHistoryItemResponse AddAutosaveHistoryContent(string bucketIdOrSlug, string contentIdOrSlug, ContentArticleRequest request)
		{
			if (request == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.MISSING_MESSAGE_BODY);

			ContentVersion oldVersion = GetContentVersion(bucketIdOrSlug, contentIdOrSlug);

			if (oldVersion == null)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);

			ContentBucket bucket = GetBucket(oldVersion.BucketId);

			ValidateSegments(request, bucket);

			DateTime now = DateTime.UtcNow;

			oldVersion.Segments = SaveBody(bucket, request.Segments, oldVersion.Id);

			// save the history

			ContentCore core = null;

			oldVersion.Title = request.Title;
			oldVersion.InvertedTitle = request.InvertedTitle;
			oldVersion.AlternateTitles = request.AlternateTitles;
			oldVersion.Blurb = request.Blurb;

			if (oldVersion.IsMaster)
			{
				Repository repository = new Repository();

				core = repository.Content.Cores.GetById(oldVersion.CoreId);

				if (core == null)
					throw new ApplicationException(ErrorMessage.Content.ORPHANED_CONTENT_NO_CORE);

				SetProperties(core, request);

				// set this to null, versions are re-populated on the history content requests
				core.Versions = null;
			}

			ContentHistory history = AddHistory(oldVersion, core, now, ContentRevisionType.Autosave);

			ContentHistoryItemResponse response = new ContentHistoryItemResponse
											 {
												 Id = history.Id,
												 ContentId = oldVersion.Id,
												 RevisionType = history.RevisionType,
												 Time = history.Time
											 };

			return response;
		}

		public ContentHistory GetHistoryByRevisionTypeOnOrBeforeDate(string bucketIdOrSlug, string contentIdOrSlug, ContentRevisionType[] revisionTypes, DateTime time)
		{
			Repository repository = new Repository();
			ContentVersion version = GetContentVersion(bucketIdOrSlug, contentIdOrSlug);
			ContentHistory history = repository.Content.History.GetByContentIdRevisionTypeAndOnOrBeforeDate(version.Id, revisionTypes, time);
			return history;
		}

		public ContentHistoryResponse GetHistory(string bucketIdOrSlug, string contentIdOrSlug)
		{
			Repository repository = new Repository();

			ContentVersion version = GetContentVersion(bucketIdOrSlug, contentIdOrSlug);

			IEnumerable<ContentHistory> history = repository.Content.History.GetByContentId(version.Id);

			return new ContentHistoryResponse
				   {
					   Items = history.Select(item => new ContentHistoryItemResponse
													  {
														  Id = item.Id,
														  ContentId = version.Id,
														  RevisionType = item.RevisionType,
														  Time = item.Time
													  })
						.ToList()
				   };
		}

		public ContentArticleResponse GetHistoryContent(string bucketIdOrSlug, string contentIdOrSlug, string id, bool? includeBody)
		{
			if (string.IsNullOrEmpty(id))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.ID_REQUIRED);

			Guid guid;
			if (!Guid.TryParse(id, out guid))
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);

			return GetHistoryContent(bucketIdOrSlug, contentIdOrSlug, guid, includeBody);
		}

		public ContentArticleResponse GetHistoryContent(string bucketIdOrSlug, string contentIdOrSlug, Guid id, bool? includeBody)
		{
			if (string.IsNullOrEmpty(bucketIdOrSlug) || string.IsNullOrEmpty(contentIdOrSlug))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.CONTENT_ID_REQUIRED);

			Repository repository = new Repository();

			ContentHistory history = repository.Content.History.GetById(id);

			if (history == null)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);

			ContentRevision revision = repository.Content.Revisions.GetById(history.RevisionId);
			if (revision == null)
				throw new ApplicationException("A history item has no corresponding revision.");

			if (revision.Version == null)
				throw new ApplicationException("A revision has no version.");

			ContentVersion version = revision.Version;

			// check the bucket id and content id
			if (!version.Paths.Any(item => item.BucketIdOrSlug == bucketIdOrSlug && item.ContentIdOrSlug == contentIdOrSlug))
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);

			ContentCore currentCore = repository.Content.Cores.GetById(version.CoreId);

			version.Published = repository.Content.Published.Exists(version.Id);

			if (currentCore == null)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);

			ContentBucket bucket = repository.Content.Buckets.GetById(version.BucketId);
			if (bucket == null)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.ORPHANED_CONTENT_NO_BUCKET);

			// make the language versions match current languages
			ContentCore core = revision.Core ?? currentCore;
			core.Versions = currentCore.Versions;

			ContentArticleResponse response = GetResponse<ContentArticleResponse>(core, version, bucket,
																	repository.Content.Origins.GetById(bucket.OriginId));

			if (!includeBody.HasValue || includeBody.Value)
				response.Segments = GetSegments(version.Segments, bucket, null, true);

			return response;
		}

		#endregion

		#region Taxonomy Methods

		public void UpdateTaxonomies(string bucketIdOrSlug, string contentIdOrSlug, TaxonomyValueSource source, List<ContentTaxonomyListRequest> taxonomiesRequest)
		{
			Repository repository = new Repository();
			ContentVersion version = GetContentVersion(bucketIdOrSlug, contentIdOrSlug);
			ContentCore core = repository.Content.Cores.GetById(version.CoreId);

			if (!version.IsMaster)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.TAXONOMIES_ONLY_CHANGED_ON_MASTER);

			if (core == null)
				throw new ServiceException(HttpStatusCode.InternalServerError, ErrorMessage.Content.ORPHANED_CONTENT_NO_CORE);

			core.Taxonomies.Values.ToList().ForEach(taxonomy => taxonomy.RemoveAll(taxonomyValue => taxonomyValue.Source == source));

			taxonomiesRequest.ForEach(x => core.Taxonomies[x.Slug].AddRange(x.Items.Select(y => new TaxonomyValue
																					{
																						Value = y.Value.Trim(),
																						Source = y.Source
																					}
																				)));

			ValidateTaxonomies(core.Taxonomies);
			ValidateCustomAttributes(core.CustomAttributes);
			repository.Content.Cores.Update(core);
		}

		public TaxonomyListResponse SearchTaxonomies(string bucketIdOrSlug, string contentIdOrSlug)
		{
			Repository repository = new Repository();
			ContentVersion version = GetContentVersion(bucketIdOrSlug, contentIdOrSlug);
			ContentCore core = repository.Content.Cores.GetById(version.CoreId);

			if (core == null)
				throw new ServiceException(HttpStatusCode.InternalServerError, ErrorMessage.Content.ORPHANED_CONTENT_NO_CORE);

			return new TaxonomyListResponse
				   {
					   Items = TaxonomiesResponseFromCore(core)
				   };
		}

		#endregion

		#endregion

		#region Internal Methods

		internal void IndexContent(Guid versionId, ContentIndexer indexer = null)
		{
			IEnumerable<CollectionStructure> structures = Repository.Content.CollectionStructures.GetByItemId(versionId);

			List<Guid> collections = structures.Select(item => item.CollectionId).Distinct().ToList();

			ContentVersion version = Repository.Content.Versions.GetById(versionId);

			if (indexer == null)
				indexer = new ContentIndexer();

			if (version != null)
				IndexContent(version, collections, indexer, IndexType.Content);

			version = Repository.Content.Published.GetById(versionId);

			if (version != null)
				IndexContent(version, collections, indexer, IndexType.Published);
		}

		private void IndexContent(ContentVersion version, List<Guid> collections, ContentIndexer indexer, IndexType type)
		{
			if (version != null && version.Status == ObjectStatus.Active)
			{
				ContentCore core = Repository.Content.Cores.GetById(version.CoreId);
				if (core == null)
					return;

				ContentBucket bucket = Repository.Content.Buckets.GetById(version.BucketId);
				if (bucket == null)
					return;

				List<ContentSegmentRequest> segments = new List<ContentSegmentRequest>();
				if (version.Segments != null)
				{
					foreach (ContentSegmentItem segmentItem in version.Segments)
					{
						ContentSegment segment = Repository.Content.Segments.GetById(segmentItem.Id);
						if (segment == null)
							continue;
						segments.Add(new ContentSegmentRequest
						{
							IdOrSlug = segmentItem.Id.ToString(),
							Body = segment.Body,
							CustomName = segmentItem.CustomName
						});
					}
				}

				List<HierarchicalTaxonomy> servicelines = HierarchicalTaxonomiesFromCore(core);

				if (indexer == null)
					indexer = new ContentIndexer();

				IndexableContent content = new IndexableContent
										   {
											   Version = version,
											   Bucket = bucket,
											   Collections = collections,
											   Core = core,
											   Segments = segments,
											   ServiceLines = servicelines
										   };

				indexer.Add(content, type);
			}
		}

		#endregion

		#region Private Methods

		private ContentResponse DeletePublished(string bucketIdOrSlug, string contentIdOrSlug)
		{
			ContentPublished version = GetPublishedVersion(bucketIdOrSlug, contentIdOrSlug);

			return DeletePublishedArticleContent(version);
		}

		private ContentResponse DeleteDraftAndPublished(string bucketIdOrSlug, string contentIdOrSlug)
		{
			ContentVersion version = GetContentVersion(bucketIdOrSlug, contentIdOrSlug, false);

			if (version != null)
				return DeleteArticleContent(version);

			ImageLogic imageLogic = new ImageLogic();

			ImageDetail detail = imageLogic.InternalGetImage(bucketIdOrSlug, contentIdOrSlug, false);

			if (detail != null)
				return imageLogic.DeleteImage(detail);

			throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);
		}

		private ContentResponse DeleteArticleContent(ContentVersion version)
		{
			Repository repository = new Repository();

			ContentCore core = repository.Content.Cores.GetById(version.CoreId);

			if (core == null)
				throw new ServiceException(HttpStatusCode.InternalServerError, ErrorMessage.Content.ORPHANED_CONTENT_NO_CORE);

			ContentIndexer indexer = new ContentIndexer();

			// we can mark the content as deleted but don't have to mark every segment as deleted
			// because all calls to the segments will require accessing the version

			ContentBucket bucket = repository.Content.Buckets.GetById(version.BucketId);

			ContentOrigin origin = bucket != null ? repository.Content.Origins.GetById(bucket.OriginId) : null;

			List<ContentVersionReference> originalVersions = core.Versions;

			if (version.IsMaster)
			{
				foreach (ContentVersionReference current in core.Versions)
				{
					ContentVersion otherVersion = repository.Content.Versions.GetById(current.Id);
					otherVersion.Status = ObjectStatus.Deleted;
					repository.Content.Versions.Update(otherVersion);
					indexer.Delete(current.Id, IndexType.Content);

					if (otherVersion.Published)
					{
						repository.Content.Published.DeleteById(current.Id);
						indexer.Delete(current.Id, IndexType.Published);
					}
				}

				core.Versions = new List<ContentVersionReference>();
			}
			else
			{
				core.Versions = core.Versions.Where(item => item.Id != version.Id).ToList();
				version.Status = ObjectStatus.Deleted;
				repository.Content.Versions.Update(version);
				indexer.Delete(version.Id, IndexType.Content);

				if (version.Published)
				{
					repository.Content.Published.DeleteById(version.Id);
					indexer.Delete(version.Id, IndexType.Published);
				}
			}

			repository.Content.Cores.Update(core);

			// replace the versions on the core for the response so the response looks like the original

			core.Versions = originalVersions;

			return GetArticleResponse(core, version, bucket, origin);
		}

		private ContentResponse DeletePublishedArticleContent(ContentPublished version)
		{
			Repository repository = new Repository();

			ContentCore core = repository.Content.Cores.GetById(version.CoreId);

			if (core == null)
				throw new ServiceException(HttpStatusCode.InternalServerError, ErrorMessage.Content.ORPHANED_CONTENT_NO_CORE);

			ContentIndexer indexer = new ContentIndexer();

			// we can mark the content as deleted but don't have to mark every segment as deleted
			// because all calls to the segments will require accessing the version

			ContentBucket bucket = repository.Content.Buckets.GetById(version.BucketId);

			ContentOrigin origin = bucket != null ? repository.Content.Origins.GetById(bucket.OriginId) : null;

			List<ContentVersionReference> originalVersions = core.Versions;

			if (version.IsMaster)
			{
				foreach (ContentVersionReference current in core.Versions)
				{
					ContentPublished otherVersion = repository.Content.Published.GetById(current.Id);
					otherVersion.Status = ObjectStatus.Deleted;
					repository.Content.Published.Update(otherVersion);
					indexer.Delete(current.Id, IndexType.Published);
				}

				core.Versions = new List<ContentVersionReference>();
			}
			else
			{
				core.Versions = core.Versions.Where(item => item.Id != version.Id).ToList();
				version.Status = ObjectStatus.Deleted;
				repository.Content.Published.Update(version);
				indexer.Delete(version.Id, IndexType.Published);
			}

			repository.Content.Cores.Update(core);

			// set the draft to unpublished
			ContentVersion draft = repository.Content.Versions.GetById(version.Id);
			draft.Published = false;
			draft.DatePublished = null;
			repository.Content.Versions.Update(draft);

			// replace the versions on the core for the response so the response looks like the original

			core.Versions = originalVersions;

			return GetArticleResponse(core, version, bucket, origin);
		}

		private ContentHistory AddHistory(ContentVersion version, ContentCore core, DateTime time, ContentRevisionType type)
		{
			Repository repository = new Repository();

			ContentRevision revision = new ContentRevision
			{
				Id = Guid.NewGuid(),
				Version = version
			};

			if (version.IsMaster)
				revision.Core = core;

			repository.Content.Revisions.Add(revision);

			ContentHistory history = new ContentHistory
			{
				ContentId = version.Id,
				RevisionId = revision.Id,
				Time = time,
				RevisionType = type
			};

			repository.Content.History.Add(history);

			return history;
		}

		private List<ContentSegmentResponse> GetSegments(List<ContentSegmentItem> segmentItems, ContentBucket bucket, License license, bool includeOrphanedSegments = false)
		{
			if (segmentItems == null || segmentItems.Count == 0)
				return null;

			List<ContentSegmentResponse> segments = new List<ContentSegmentResponse>();

			Repository repository = new Repository();

			string matchExpression = null, replaceExpression = null;
			if (license != null)
			{
				matchExpression = Settings.Current.WebPortalBaseUri + "/Service/Get(/Content)(/[a-z0-9-]+/Images/[a-z0-9-]+(\\.[0-9]+x[0-9]+)?(\\.[a-z]{3,4})?)";
				replaceExpression = Settings.Current.ServiceUri + "$1/" + license.Id + "$2";
			}

			foreach (ContentSegmentItem segmentItem in segmentItems)
			{
				ContentSegmentResponse segmentResponse = new ContentSegmentResponse
				{
					Id = segmentItem.SegmentId
				};

				if (bucket.Segments != null && segmentItem.SegmentId != null)
				{
					ContentBucketSegment bucketSegment = bucket.Segments.FirstOrDefault(item => item.Id == segmentItem.SegmentId);

					if (bucketSegment == null)
					{
						// if the bucket segment has been deleted, transparently delete it here from the
						// content as well
						if (!includeOrphanedSegments)
							continue;

						segmentResponse.Id = null;
					}
					else
					{
						segmentResponse.Name = bucketSegment.Name;
						segmentResponse.Slug = bucketSegment.Slug;
					}

					segmentResponse.CustomName = segmentItem.CustomName;
				}

				ContentSegment segment = repository.Content.Segments.GetById(segmentItem.Id);

				if (segment != null && segment.Body != null)
				{
					if (matchExpression == null)
					{
						segmentResponse.Body = segment.Body;
					}
					else
					{
						// modify image url's for publicly available images:
						Regex regex = new Regex(matchExpression, RegexOptions.IgnoreCase);
						segmentResponse.Body = regex.Replace(segment.Body, replaceExpression);
					}
				}

				segments.Add(segmentResponse);
			}

			return segments;
		}

		private string GetCopyright(Guid? copyrightId, ContentBucket bucket)
		{
			Repository repository = new Repository();

			Copyright copyright;
			string copyrightValue = string.Empty;

			if (copyrightId.HasValue)
			{
				copyright = repository.Administration.Copyrights.GetCopyright(copyrightId.Value);

				if (copyright != null)
				{
					copyrightValue = copyright.Value;
				}
			}
			else if (bucket.CopyrightId.HasValue)
			{
				copyright = repository.Administration.Copyrights.GetCopyright(bucket.CopyrightId.Value);
				if (copyright != null)
				{
					copyrightValue = copyright.Value;
				}
			}
			else
			{
				copyright = repository.Administration.Copyrights.GetCopyright(new Guid(Constant.Copyright.DEFAULT_COPYRIGHT_ID));
				if (copyright != null)
				{
					copyrightValue = copyright.Value;
				}
			}

			if (!string.IsNullOrEmpty(copyrightValue))
			{
				copyrightValue = copyrightValue.Replace("{{year}}", DateTime.Now.Year.ToString());
			}

			return copyrightValue;
		}

		private SlugResponse CreateSlugResponse(string slug)
		{
			return new SlugResponse
				   {
					   Value = slug
				   };
		}

		private ContentVersion GetContentVersion(string bucketIdOrSlug, string idOrSlug, bool validate = true)
		{
			if (string.IsNullOrEmpty(bucketIdOrSlug) || string.IsNullOrEmpty(idOrSlug))
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);

			bucketIdOrSlug = bucketIdOrSlug.ToLower();
			idOrSlug = idOrSlug.ToLower();

			Repository repository = new Repository();

			ContentVersion version = repository.Content.Versions.GetByPath(bucketIdOrSlug, idOrSlug);

			if (version == null && validate)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);

			return version;
		}

		private ContentPublished GetPublishedVersion(string bucketIdOrSlug, string idOrSlug, bool validate = true)
		{
			if (string.IsNullOrEmpty(bucketIdOrSlug) || string.IsNullOrEmpty(idOrSlug))
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);

			bucketIdOrSlug = bucketIdOrSlug.ToLower();
			idOrSlug = idOrSlug.ToLower();

			Repository repository = new Repository();

			ContentPublished version = repository.Content.Published.GetByPath(bucketIdOrSlug, idOrSlug);

			if (version == null && validate)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);

			return version;
		}

		private ContentBucket GetBucket(Guid bucketId)
		{
			Repository repository = new Repository();
			ContentBucket bucket = repository.Content.Buckets.GetById(bucketId);

			if (bucket == null || bucket.Status != ObjectStatus.Active)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_BUCKET);

			return bucket;
		}

		private List<ContentResponse> GetSearchResponse(SearchResponse searchResponse, bool published)
		{
			List<ContentResponse> response = new List<ContentResponse>();
			if (searchResponse.Hits.Items.Count == 0)
				return response;

			IndexType contentType = published ? IndexType.Published : IndexType.Content;
			List<Guid> articleIds = searchResponse.Hits.Items.Where(item => item.Type == contentType).Select(item => item.Id).ToList();
			List<Guid> imageIds = searchResponse.Hits.Items.Where(item => item.Type == IndexType.Image).Select(item => item.Id).ToList();

			Dictionary<Guid, ContentVersion> contentSorter = null;
			Dictionary<Guid, ImageDetail> imageSorter = null;
			Dictionary<Guid, ContentCore> coreLookup = null;

			Repository repository = new Repository();

			if (articleIds.Count > 0)
			{
				List<ContentVersion> unsortedArticles = published
					? repository.Content.Published.GetByIds(articleIds).Select(item => (ContentVersion)item).ToList()
					: repository.Content.Versions.GetByIds(articleIds).ToList();

				contentSorter = unsortedArticles.ToDictionary(item => item.Id);

				List<Guid> coreIds = unsortedArticles.Select(item => item.CoreId).Distinct().ToList();

				if (coreIds.Count > 0)
				{
					IEnumerable<ContentCore> cores = repository.Content.Cores.GetByIds(coreIds);
					coreLookup = cores.ToDictionary(item => item.Id);
				}
			}

			if (imageIds.Count > 0)
			{
				IEnumerable<ImageDetail> unsortedImages = repository.Content.ImageDetails.GetByIds(imageIds);
				imageSorter = unsortedImages.ToDictionary(item => item.Id);
			}

			Dictionary<Guid, ContentBucket> buckets = new Dictionary<Guid, ContentBucket>();
			Dictionary<Guid, ContentOrigin> origins = new Dictionary<Guid, ContentOrigin>();

			ImageLogic imageLogic = new ImageLogic();

			// reorder by correct sort order and add all details
			foreach (SearchItemResponse sortedItem in searchResponse.Hits.Items)
			{
				if (sortedItem.Type == contentType)
				{
					ContentVersion version;

					// contentSorter guaranteed not to be null if there's an item with SearchType.Content
					// ReSharper disable PossibleNullReferenceException

					if (!contentSorter.TryGetValue(sortedItem.Id, out version))
						continue;

					// ReSharper restore PossibleNullReferenceException

					ContentBucket bucket;
					if (!buckets.TryGetValue(version.BucketId, out bucket))
					{
						bucket = repository.Content.Buckets.GetById(version.BucketId);

						if (bucket == null)
							continue;

						buckets[bucket.Id] = bucket;
					}

					ContentCore core = null;
					if (coreLookup != null)
						coreLookup.TryGetValue(version.CoreId, out core);

					ContentOrigin origin;

					if (!origins.TryGetValue(bucket.OriginId, out origin))
					{
						origin = repository.Content.Origins.GetById(bucket.OriginId);
						origins[bucket.OriginId] = origin;
					}

					response.Add(GetArticleResponse(core, version, bucket, origin));
				}
				else if (sortedItem.Type == IndexType.Image)
				{
					ImageDetail detail;

					// imageSorter guaranteed not to be null if there's an item with SearchType.Image
					// ReSharper disable PossibleNullReferenceException

					if (!imageSorter.TryGetValue(sortedItem.Id, out detail))
						continue;

					// ReSharper restore PossibleNullReferenceException

					ContentBucket bucket;

					if (!buckets.TryGetValue(detail.BucketId, out bucket))
					{
						bucket = repository.Content.Buckets.GetById(detail.BucketId);

						if (bucket == null)
							continue;

						buckets[bucket.Id] = bucket;
					}

					ContentOrigin origin;

					if (!origins.TryGetValue(bucket.OriginId, out origin))
					{
						origin = repository.Content.Origins.GetById(bucket.OriginId);
						origins[bucket.OriginId] = origin;
					}


					response.Add(imageLogic.GetContentResponse(detail, bucket, origin));
				}
			}

			return response;
		}

		internal List<HierarchicalTaxonomyResponse> GetServicelinesResponseFromCore(ContentCore core)
		{
			// TODO: method badly in need of cleanup
			// TODO: store HT as serviceline object in content?
			// store full path in HT?

			IEnumerable<HierarchicalTaxonomyRequest> items = core.Servicelines;

			if (items == null)
				return null;

			Repository repository = new Repository();
			var response = new List<HierarchicalTaxonomyResponse>();
			foreach (HierarchicalTaxonomyRequest item in items)
			{
				if (string.IsNullOrEmpty(item.Slug))
				{
					if (string.IsNullOrEmpty(item.Path))
						continue;

					// ugly:
					string[] splitPath = item.Path.Split('/');
					if (splitPath.Length < 2 || string.IsNullOrEmpty(splitPath[1]))
						continue;

					string audience = splitPath[0];
					string serviceline = splitPath[1];

					HierarchicalTaxonomy htItem = repository.Content.HierarchicalTaxonomy.GetHierarchicalTaxonomyBySlugAndPath(HierarchicalTaxonomyType.Serviceline, serviceline, audience);
					response.Add(new HierarchicalTaxonomyResponse
									 {
										 Path = item.Path,
										 Slug = null,
										 PathValues = htItem.PathValues,
										 Type = HierarchicalTaxonomyType.Serviceline
									 });
				}
				else
				{
					if (string.IsNullOrEmpty(item.Path))
						continue;

					HierarchicalTaxonomy htItem = repository.Content.HierarchicalTaxonomy.GetHierarchicalTaxonomyBySlugAndPath(HierarchicalTaxonomyType.Serviceline, item.Slug, item.Path);

					if (htItem == null)
						continue;

					response.Add(new HierarchicalTaxonomyResponse
									 {
										 Path = htItem.Path,
										 Slug = htItem.Slug,
										 PathValues = htItem.PathValues,
										 Value = htItem.Value,
										 Type = htItem.Type
									 });
				}
			}
			return response;
		}

		private List<CustomAttribute> GetCustomAttributesFromCore(ContentCore core)
		{
			Dictionary<string, List<string>> items = core.CustomAttributes;
			if (items == null) return null;

			List<CustomAttribute> list =
				(items.Where(item => item.Value != null && item.Value.Count > 0).Select(item =>
					new CustomAttribute
						{
							Name = item.Key,
							CustomAttributeValues =
								item.Value.Select(s => new CustomAttributeContent { Values = s }).ToList(),
						})).ToList();

			return list;
		}

		private T GetResponse<T>(ContentCore core, ContentVersion version, ContentBucket bucket, ContentOrigin origin) where T : ContentMetadataResponse, new()
		{
			T response = new T
					   {
						   Id = version.Id,
						   Slug = version.Slug,
						   Title = version.Title,
						   InvertedTitle = version.InvertedTitle,
						   Blurb = version.Blurb,
						   AlternateTitles = version.AlternateTitles,
						   Language = LanguageProvider.GetLanguage(version.Language ?? Constant.Content.ENGLISH_LANGUAGE_CODE),
						   DateAdded = version.DateAdded,
						   DateModified = version.DateModified,
						   Master = version.IsMaster,
						   Bucket = new ContentBucketReference
									{
										Id = version.BucketId,
									},
						   LegacyId = version.LegacyId,
						   Published = version.Published,
						   DateUpdated = version.DateUpdated
					   };

			if (core != null)
			{
				response.AgeCategories = core.AgeCategories;
				response.Authors = core.Authors;
				response.Gender = core.Gender;
				response.OnlineEditors = core.OnlineEditors;
				response.OnlineMedicalReviewers = core.OnlineMedicalReviewers;
				response.OnlineOriginatingSources = core.OnlineOriginatingSources;
				response.PrintOriginatingSources = core.PrintOriginatingSources;
				response.RecommendedSites = core.RecommendedSites;
				response.LastReviewedDate = core.LastReviewedDate;
				response.PostingDate = core.PostingDate;
				response.GunningFogReadingLevel = core.GunningFogReadingLevel;
				response.FleschKincaidReadingLevel = core.FleschKincaidReadingLevel;
				response.CopyrightId = core.CopyrightId;
				response.MasterId = core.MasterId;
				response.Versions = core.Versions == null ?
					new List<ContentVersionItem>() :
					core.Versions.Select(item => new ContentVersionItem
																 {
																	 Id = item.Id,
																	 Slug = item.Slug,
																	 Master = item.Id == core.MasterId,
																	 Language = LanguageProvider.GetLanguage(item.Language)
																 }).ToList();

				response.Taxonomies = TaxonomiesResponseFromCore(core);
				response.Servicelines = GetServicelinesResponseFromCore(core);
				response.CustomAttributes = GetCustomAttributesFromCore(core);
			}

			if (bucket != null)
			{
				response.Bucket.Id = bucket.Id;
				response.Bucket.Name = bucket.Name;
				response.Bucket.Slug = bucket.Slug;
				response.Type = bucket.Type;
			}

			if (origin != null)
			{
				response.OriginName = origin.Name;
			}

			return response;
		}

		private static List<ContentTaxonomyList> TaxonomiesResponseFromCore(ContentCore core)
		{
			Repository repository = new Repository();

			return core.Taxonomies == null
					   ? null
					   : core.Taxonomies.Select(kvp => new ContentTaxonomyList
														   {
															   Slug = kvp.Key,
															   Name = repository.Content.TaxonomyTypes.GetTaxonomyTypes().Single(x => x.Slug.Equals(kvp.Key, StringComparison.OrdinalIgnoreCase)).Name,
															   Items = kvp.Value.Select(taxonomy => new ContentTaxonomyValue()
																								   {
																									   Value = taxonomy.Value,
																									   Source =
																										   taxonomy.Source
																								   }
																   )
																   .ToList()
														   }
							 ).ToList();
		}

		private ContentResponse GetArticleResponse(ContentCore core, ContentVersion version, ContentBucket bucket, ContentOrigin origin)
		{
			ContentResponse response = new ContentResponse
			{
				Id = version.Id,
				Slug = version.Slug,
				Title = version.Title,
				InvertedTitle = version.InvertedTitle,
				Blurb = version.Blurb,
				AlternateTitles = version.AlternateTitles,
				Language = LanguageProvider.GetLanguage(version.Language ?? Constant.Content.ENGLISH_LANGUAGE_CODE),
				DateAdded = version.DateAdded,
				DateModified = version.DateModified,
				Master = version.IsMaster,
				LegacyId = version.LegacyId,
				Bucket = new ContentBucketReference
				{
					Id = version.BucketId,
				},
				Published = version.Published,
				DateUpdated = version.DateUpdated
			};

			if (core != null)
			{
				response.AgeCategories = core.AgeCategories;
				response.Authors = core.Authors;
				response.Gender = core.Gender;
				response.OnlineEditors = core.OnlineEditors;
				response.OnlineMedicalReviewers = core.OnlineMedicalReviewers;
				response.OnlineOriginatingSources = core.OnlineOriginatingSources;
				response.PrintOriginatingSources = core.PrintOriginatingSources;
				response.RecommendedSites = core.RecommendedSites;
				response.LastReviewedDate = core.LastReviewedDate;
				response.PostingDate = core.PostingDate;
				response.GunningFogReadingLevel = core.GunningFogReadingLevel;
				response.FleschKincaidReadingLevel = core.FleschKincaidReadingLevel;
				response.CopyrightId = core.CopyrightId;
				response.MasterId = core.MasterId;
				response.Versions = core.Versions == null ?
					new List<ContentVersionItem>() :
					core.Versions.Select(item => new ContentVersionItem
					{
						Id = item.Id,
						Slug = item.Slug,
						Master = item.Id == core.MasterId,
						Language = LanguageProvider.GetLanguage(item.Language)
					}).ToList();
				response.Taxonomies = TaxonomiesResponseFromCore(core);
				response.Servicelines = GetServicelinesResponseFromCore(core);
				response.CustomAttributes = GetCustomAttributesFromCore(core);
			}

			if (bucket != null)
			{
				response.Bucket.Id = bucket.Id;
				response.Bucket.Name = bucket.Name;
				response.Bucket.Slug = bucket.Slug;
				response.Type = bucket.Type;
			}

			if (origin != null)
			{
				response.OriginName = origin.Name;
			}

			return response;
		}

		private void ValidateRequest(ContentArticleRequest request)
		{
			if (request == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.MISSING_MESSAGE_BODY);

			if (string.IsNullOrWhiteSpace(request.Title))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.MISSING_CONTENT_TITLE);

			request.Title = request.Title.Trim();

			if (string.Equals(request.Title, request.InvertedTitle, StringComparison.OrdinalIgnoreCase))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.IDENTICAL_INVERTED_TITLE);
		}

		private void ValidateMasterRequest(ContentArticleRequest request)
		{
			// gender is required
			if (request.Gender == Gender.None)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.GENDER_REQUIRED);

			ValidateAgeCategories(request);
			ValidateGunningFogReadingLevel(request);
			ValidateFleschKincaidReadingLevel(request);
			ValidateOnlineOriginatingSource(request);
			ValidateRecommendedSites(request);
			ValidateCopyright(request);
		}

		private void ValidateCopyright(ContentArticleRequest request)
		{
			if (!request.CopyrightId.HasValue)
				return;

			Repository repository = new Repository();
			Copyright copyright = repository.Administration.Copyrights.GetCopyright(request.CopyrightId.Value);
			if (copyright == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_COPYRIGHT_ID);
		}

		private void ValidateAgeCategories(ContentArticleRequest request)
		{
			if (request.AgeCategories == null || request.AgeCategories.Count == 0)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.AGE_CATEGORY_REQUIRED);

			if (request.AgeCategories.GroupBy(item => item).Any(item => item.Count() > 1))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.DUPLICATE_AGE_CATEGORY);
		}

		private void ValidateOnlineOriginatingSource(ContentArticleRequest request)
		{
			if (request.OnlineOriginatingSources == null)
				return;

			if (request.OnlineOriginatingSources.Count == 0)
			{
				request.OnlineOriginatingSources = null;
				return;
			}

			if (request.OnlineOriginatingSources.Any(item => item == null))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_ONLINE_ORIGINATING_SOURCE);

			if (request.OnlineOriginatingSources.Any(item => !string.IsNullOrEmpty(item.Uri) && !UriHelper.IsValid(item.Uri)))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_ONLINE_ORIGINATING_SOURCE_URI);
		}

		private void ValidateRecommendedSites(ContentArticleRequest request)
		{
			if (request.RecommendedSites == null)
				return;

			if (request.RecommendedSites.Count == 0)
			{
				request.RecommendedSites = null;
				return;
			}

			if (request.RecommendedSites.Any(item => item == null))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_RECOMMENDED_SITE);

			if (request.RecommendedSites.Any(item => !string.IsNullOrEmpty(item.Uri) && !UriHelper.IsValid(item.Uri)))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_RECOMMENDED_SITE_URI);
		}

		private void ValidateGunningFogReadingLevel(ContentArticleRequest request)
		{
			switch (request.GunningFogReadingLevel)
			{
				case "":
					request.GunningFogReadingLevel = null;
					break;
				case null:
				case "4-5":
				case "6-7":
				case "8-13":
					break;
				default:
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_GUNNING_FOG_READING_LEVEL);
			}
		}

		private void ValidateFleschKincaidReadingLevel(ContentArticleRequest request)
		{
			switch (request.FleschKincaidReadingLevel)
			{
				case "":
					request.FleschKincaidReadingLevel = null;
					break;
				case null:
					break;
				case "0-30":
				case "60-70":
				case "90-100":
					break;
				default:
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_FLESCH_KINCAID_READING_LEVEL);
			}
		}

		private void ValidateSegments(ContentArticleRequest request, ContentBucket bucket)
		{
			if (request.Segments != null)
			{
				if (request.Segments.Any(item => item == null))
					throw new ServiceException(HttpStatusCode.BadRequest, string.Format(ErrorMessage.Content.INVALID_SEGMENT));

				List<ContentSegmentRequest> nonCustomSegments = request.Segments.Where(item => !string.IsNullOrEmpty(item.IdOrSlug)).ToList();

				// error for duplicates
				if (nonCustomSegments.GroupBy(item => item.IdOrSlug).Any(item => item.Count() > 1))
					throw new ServiceException(HttpStatusCode.BadRequest, string.Format(ErrorMessage.Content.DUPLICATE_SEGMENT));

				// error for unknown buckets
				if (nonCustomSegments.Count > 0 && (bucket.Segments == null || bucket.Segments.Count == 0))
					throw new ServiceException(HttpStatusCode.BadRequest, string.Format(ErrorMessage.Content.INVALID_SEGMENT));

				Guid id;

				if (!nonCustomSegments.All(item => bucket.Segments.Any(bucketSegment => Guid.TryParse(item.IdOrSlug, out id) ? id == bucketSegment.Id : bucketSegment.Slug == item.IdOrSlug)))
					throw new ServiceException(HttpStatusCode.BadRequest, string.Format(ErrorMessage.Content.INVALID_SEGMENT));

				if (bucket.Type == ContentType.Text && bucket.Constraints != null && bucket.Constraints.Length != null)
				{
					int totalLength = request.Segments.Where(segment => segment.Body != null).Sum(segment => segment.Body.Length);
					if (totalLength > bucket.Constraints.Length)
						throw new ServiceException(HttpStatusCode.BadRequest, string.Format(ErrorMessage.Content.BODY_TOO_LONG, bucket.Constraints.Length));
				}
			}

			if (bucket.Segments != null && bucket.Segments.Count > 0)
			{
				foreach (ContentBucketSegment segment in bucket.Segments)
				{
					if (segment.Required)
					{
						Guid id;
						if (request.Segments == null || !request.Segments.Any(item => Guid.TryParse(item.IdOrSlug, out id) ? id == segment.Id : item.IdOrSlug == segment.Slug))
							throw new ServiceException(HttpStatusCode.BadRequest, string.Format(ErrorMessage.Content.MISSING_REQUIRED_SEGMENT, segment.Name));
					}
				}
			}
		}

		private void ValidateUpdatedTaxonomies(Dictionary<string, List<TaxonomyValue>> oldTaxonomies, Dictionary<string, List<TaxonomyValue>> newTaxonomies)
		{
			ValidateTaxonomies(newTaxonomies);

			if (oldTaxonomies == null)
				return;

			var flatOldCoreTaxonomies = oldTaxonomies.SelectMany(kvp => kvp.Value.Select(x =>
																				 new
																				 {
																					 Slug = kvp.Key,
																					 x.Value,
																					 x.Source
																				 }
																	)).ToList();

			var flatNewCoreTaxonomies = newTaxonomies == null
											? flatOldCoreTaxonomies.Take(0).ToList() // Empty list of the anonymous type defined above
											: newTaxonomies.SelectMany(kvp => kvp.Value.Select(x =>
																				 new
																				 {
																					 Slug = kvp.Key,
																					 x.Value,
																					 x.Source
																				 }
																	)).ToList();
			var test = flatOldCoreTaxonomies.Except(flatNewCoreTaxonomies).Any(taxonomyValue => taxonomyValue.Source != TaxonomyValueSource.Ksw);
			if (flatOldCoreTaxonomies.Except(flatNewCoreTaxonomies).Any(taxonomyValue => taxonomyValue.Source != TaxonomyValueSource.Ksw))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.THIRD_PARTY_TAXONOMY_VALUES_CANNOT_BE_DELETED);
		}

		private void ValidateTaxonomies(Dictionary<string, List<TaxonomyValue>> taxonomies)
		{
			if (taxonomies == null)
				return;

			Repository repository = new Repository();

			var v = repository.Content.TaxonomyTypes.GetTaxonomyTypes();
			// valid TaxonomyType slugs
			if (taxonomies.Keys.Any(taxonomyTypeSlug => !repository.Content.TaxonomyTypes.GetTaxonomyTypes()
				.Any(taxonomyType => taxonomyType.Slug.Equals(taxonomyTypeSlug, StringComparison.OrdinalIgnoreCase))))
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_TAXONOMY_TYPE);
			}

			// taxonomy value satisfies regex
			if (taxonomies.Keys.Any(taxonomyTypeSlug => (taxonomies[taxonomyTypeSlug]
						 .Any
						 (
							taxonomyValue => !Regex.IsMatch(taxonomyValue.Value, repository.Content.TaxonomyTypes.GetTaxonomyTypeBySlug(taxonomyTypeSlug).Regex, RegexOptions.IgnoreCase)
						 )
				)))
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_TAXONOMY_VALUE);
			}
		}

		private void ValidateCustomAttributes(Dictionary<string, List<string>> customAttributes)
		{
			if (customAttributes == null)
				return;

			if (customAttributes.Any(a => !Regex.IsMatch(a.Key, @"^[A-Za-z0-9]+$")))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_CUSTOM_ATTRIBUTE_NAME);

			if (customAttributes.Any(a => a.Value.Any(string.IsNullOrWhiteSpace)))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_CUSTOM_ATTRIBUTE_VALUE);
		}

		private List<ContentSegmentItem> SaveBody(ContentBucket bucket, List<ContentSegmentRequest> segments, Guid contentId)
		{
			switch (bucket.Type)
			{
				case ContentType.Article:
				case ContentType.Text:
					return SaveSegments(bucket, segments, contentId);
				default:
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.NOT_SUPPORTED);
			}
		}

		private List<ContentSegmentItem> SaveSegments(ContentBucket bucket, List<ContentSegmentRequest> segments, Guid contentId)
		{
			if (segments == null || segments.Count == 0)
				return null;

			Repository repository = new Repository();
			List<ContentSegmentItem> segmentItems = new List<ContentSegmentItem>();

			foreach (ContentSegmentRequest segmentRequest in segments)
			{
				ContentSegment segment = new ContentSegment
				{
					Body = segmentRequest.Body,
					ContentId = contentId
				};

				repository.Content.Segments.Add(segment);

				Guid? segmentId = null;
				if (!string.IsNullOrEmpty(segmentRequest.IdOrSlug))
				{
					Guid id;
					if (Guid.TryParse(segmentRequest.IdOrSlug, out id))
					{
						segmentId = id;
					}
					else
					{
						segmentId = bucket.Segments.First(item => item.Slug == segmentRequest.IdOrSlug).Id;
					}

				}

				segmentItems.Add(new ContentSegmentItem
				{
					Id = segment.Id,
					SegmentId = segmentId,
					CustomName = segmentRequest.CustomName
				});
			}

			return segmentItems;
		}

		private void CheckLicenseForModule(License license)
		{
			ServiceModule module = ModuleProvider.GetModule(ModuleType.Content);
			if (license.Modules == null || !license.Modules.Any(item => item == module.Type))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.ContentModule.NO_CONTENT_MODULE);
		}

		private ContentList GetEmptyContentList()
		{
			return new ContentList
				   {
					   Items = new List<ContentResponse>(),
					   Offset = 0,
					   SortedBy = new SortOption("Relevance", SortDirection.Ascending).ToString(),
					   Total = 0
				   };
		}

		private void UpdateAllowedBuckets(ContentSearchRequest request, Application application)
		{
			Repository repository = new Repository();

			if (application == null || application.LicenseId == null)
				return;

			License license = repository.Administration.Licenses.GetById(application.LicenseId.Value);

			if (license == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.License.INVALID_LICENSE);

			if (license.Modules.Any(item => item == ModuleType.ManageContent))
				return;

			CheckLicenseForModule(license);

			ContentModule contentModule = repository.Content.ContentModule.GetByLicenseId(application.LicenseId.Value);

			if (contentModule == null)
			{
				request.Buckets = new List<string>();
				return;
			}

			if (contentModule.BucketIds == null || contentModule.BucketIds.Count == 0)
			{
				request.Buckets = new List<string>();
				return;
			}

			List<Guid> allowedBuckets = contentModule.BucketIds;

			if (request.Buckets != null)
				allowedBuckets = allowedBuckets.Where(item => request.Buckets.Contains(item.ToString())).ToList();

			request.Buckets = allowedBuckets.Select(item => item.ToString()).ToList();
		}

		private IndexType[] GetIndexTypes(ContentSearchRequest request)
		{
			if (request.Types == null || request.Types.Count == 0)
			{
				return request.IncludeDrafts
						   ? new[] { IndexType.Content }
						   : new[] { IndexType.Published };
			}

			return request.Types.Distinct().Select(item =>
												{
													switch (item)
													{
														case SearchType.Content:
															return request.IncludeDrafts ? IndexType.Content : IndexType.Published;
														case SearchType.Image:
															return IndexType.Image;
														default:
															throw new ServiceException(HttpStatusCode.BadRequest,
																					   ErrorMessage.Content.INVALID_TYPES);
													}
												}).ToArray();
		}

		#region New Content Methods

		private ContentMetadataResponse SaveNewSibling(NewContentRequest request, ContentVersion version, ContentBucket bucket)
		{
			Repository repository = new Repository();

			ContentVersion master = GetContentVersion(bucket.Slug, request.MasterId);

			if (!master.IsMaster)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.MASTER_NOT_FOUND);

			ContentCore core = repository.Content.Cores.GetById(master.CoreId);

			if (core == null)
				throw new ServiceException(HttpStatusCode.InternalServerError, ErrorMessage.Content.ORPHANED_CONTENT_NO_CORE);

			if (core.Versions.Any(item => item.Language == request.LanguageCode))
				throw new ServiceException(HttpStatusCode.BadRequest, string.Format(ErrorMessage.Content.DUPLICATE_LANGUAGE, LanguageProvider.GetName(request.LanguageCode)));

			if (master.BucketId != bucket.Id)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.SIBLING_BUCKET_MUST_MATCH);

			// validate that we can publish
			ContentPublished publishedMaster = null;

			if (request.Publish)
			{
				publishedMaster = repository.Content.Published.GetById(master.Id);

				if (publishedMaster == null)
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.MUST_PUBLISH_MASTER_FIRST);
			}

			core.Versions.Add(new ContentVersionReference
			{
				Id = version.Id,
				Slug = version.Slug,
				Language = request.LanguageCode
			});

			repository.Content.Cores.Update(core);

			version.CoreId = core.Id;

			version.Segments = SaveBody(bucket, request.Segments, version.Id);

			repository.Content.Versions.Add(version);

			ContentIndexer indexer = new ContentIndexer();

			List<HierarchicalTaxonomy> servicelines = HierarchicalTaxonomiesFromCore(core);
			indexer.Add(new IndexableContent
				{
					Version = version,
					Core = core,
					Bucket = bucket,
					Collections = null,
					Segments = request.Segments,
					ServiceLines = servicelines
				},
				IndexType.Content);

			// add to history
			AddHistory(version, core, version.DateAdded, ContentRevisionType.Created);

			if (request.Publish)
				PublishNewSibling(request, version, core, bucket, publishedMaster);

			return GetResponse<ContentMetadataResponse>(core, version, bucket, repository.Content.Origins.GetById(bucket.OriginId));
		}

		private void PublishNewSibling(NewContentRequest request, ContentVersion version, ContentCore core, ContentBucket bucket, ContentPublished master)
		{
			Repository repository = new Repository();

			ContentCore publishedCore = repository.Content.Cores.GetById(master.CoreId);

			if (publishedCore == null)
				throw new InvalidOperationException(ErrorMessage.Content.ORPHANED_CONTENT_NO_CORE);

			// only update the version references

			publishedCore.Versions.Add(new ContentVersionReference
			{
				Id = version.Id,
				Slug = version.Slug,
				Language = request.LanguageCode
			});

			repository.Content.Cores.Update(publishedCore);

			ContentPublished publishedVersion = new ContentPublished();

			SetProperties(publishedVersion, version);

			publishedVersion.CoreId = publishedCore.Id;
			publishedVersion.DatePublished = DateTime.UtcNow;

			repository.Content.Published.Add(publishedVersion);

			ContentIndexer indexer = new ContentIndexer();

			List<HierarchicalTaxonomy> servicelines = HierarchicalTaxonomiesFromCore(core);
			indexer.Add(new IndexableContent
				{
					Version = version,
					Core = core,
					Bucket = bucket,
					Collections = null,
					Segments = request.Segments,
					ServiceLines = servicelines
				}, IndexType.Published);

			AddHistory(version, core, publishedVersion.DatePublished.Value, ContentRevisionType.Published);
		}

		private ContentMetadataResponse SaveNewMaster(NewContentRequest request, ContentVersion version, ContentBucket bucket)
		{
			ContentCore core = new ContentCore
							   {
								   Id = Guid.NewGuid(),
							   };

			SetProperties(core, request);

			ValidateTaxonomies(core.Taxonomies);
			ValidateCustomAttributes(core.CustomAttributes);
			List<HierarchicalTaxonomy> servicelines = ValidateServicelines(core);

			core.MasterId = version.Id;

			core.Versions = new List<ContentVersionReference>
			                {
				                new ContentVersionReference
				                {
					                Id = version.Id,
					                Slug = version.Slug,
					                Language = request.LanguageCode
				                }
			                };

			version.CoreId = core.Id;

			version.IsMaster = true;

			if (!string.IsNullOrEmpty(version.LegacyId))
			{
				ThreeMLogic threeMLogic = new ThreeMLogic();

				threeMLogic.UpdateContent(version, core, bucket);
			}

			Repository repository = new Repository();

			repository.Content.Cores.Add(core);

			version.Segments = SaveBody(bucket, request.Segments, version.Id);

			repository.Content.Versions.Add(version);

			ContentIndexer indexer = new ContentIndexer();

			indexer.Add(new IndexableContent
				{
					Version = version,
					Core = core,
					Bucket = bucket,
					Collections = null,
					Segments = request.Segments,
					ServiceLines = servicelines
				}, IndexType.Content);

			// add to history
			AddHistory(version, core, version.DateAdded, ContentRevisionType.Created);

			if (request.Publish)
				PublishNewMaster(request, version, core, bucket);

			return GetResponse<ContentMetadataResponse>(core, version, bucket, repository.Content.Origins.GetById(bucket.OriginId));
		}

		private void PublishNewMaster(NewContentRequest request, ContentVersion version, ContentCore core, ContentBucket bucket)
		{
			ContentCore publishedCore = new ContentCore();

			SetProperties(publishedCore, core);

			publishedCore.Id = Guid.NewGuid();

			Repository repository = new Repository();

			repository.Content.Cores.Add(publishedCore);

			ContentPublished publishedVersion = new ContentPublished();

			SetProperties(publishedVersion, version);

			publishedVersion.CoreId = publishedCore.Id;
			publishedVersion.DatePublished = DateTime.UtcNow;

			repository.Content.Published.Add(publishedVersion);

			ContentIndexer indexer = new ContentIndexer();

			List<HierarchicalTaxonomy> publishedServicelines = HierarchicalTaxonomiesFromCore(publishedCore);
			indexer.Add(new IndexableContent
				{
					Version = publishedVersion,
					Core = publishedCore,
					Bucket = bucket,
					Collections = null,
					Segments = request.Segments,
					ServiceLines = publishedServicelines
				}, IndexType.Published);

			AddHistory(version, core, publishedVersion.DatePublished.Value, ContentRevisionType.Published);
		}

		#endregion

		#region Update Content Methods

		private ContentMetadataResponse SaveUpdatedMaster(ContentArticleRequest request, ContentCore core, ContentVersion version, ContentBucket bucket)
		{
			if (core.MasterId != version.Id)
				throw new ServiceException(HttpStatusCode.InternalServerError, ErrorMessage.Content.MASTER_IDS_DO_NOT_MATCH);

			version.Segments = SaveBody(bucket, request.Segments, version.Id);

			ValidateUpdatedTaxonomies(core.Taxonomies, TaxonomiesFromRequest(request));
			ValidateCustomAttributes(CustomAttributesFromRequest(request));

			SetProperties(core, request);

			List<HierarchicalTaxonomy> servicelines = ValidateServicelines(core);

			if (!string.IsNullOrEmpty(version.LegacyId))
			{
				ThreeMLogic threeMLogic = new ThreeMLogic();

				threeMLogic.UpdateContent(version, core, bucket);
			}

			Repository repository = new Repository();

			// create a core if none exists (reverse compatibility)
			if (core.Id == Guid.Empty)
			{
				core.Id = Guid.NewGuid();
				repository.Content.Cores.Add(core);
			}
			else
			{
				repository.Content.Cores.Update(core);
			}

			repository.Content.Versions.Update(version);

			ContentIndexer indexer = new ContentIndexer();

			indexer.Update(new IndexableContent
				{
					Version = version,
					Core = core,
					Bucket = bucket,
					Collections = null,
					Segments = request.Segments,
					ServiceLines = servicelines
				}, IndexType.Content);

			// update the updated date on all subordinate language versions
			foreach (ContentVersionReference reference in core.Versions)
			{
				ContentVersion subordinate = repository.Content.Versions.GetById(reference.Id);
				if (subordinate == null)
					continue;

				subordinate.DateUpdated = version.DateUpdated;
				repository.Content.Versions.Update(subordinate);
			}

			if (request.Publish)
				PublishUpdatedMaster(request, version, core, bucket);

			// save to history
			AddHistory(version, core, version.DateModified, request.Publish ? ContentRevisionType.Published : ContentRevisionType.Draft);

			return GetResponse<ContentMetadataResponse>(core, version, bucket, repository.Content.Origins.GetById(bucket.OriginId));
		}

		private void PublishUpdatedMaster(ContentArticleRequest request, ContentVersion version, ContentCore core, ContentBucket bucket)
		{
			Repository repository = new Repository();
			ContentIndexer indexer = new ContentIndexer();

			ContentPublished published = repository.Content.Published.GetById(version.Id);

			if (published == null)
			{
				ContentCore publishedCore = new ContentCore
				{
					Id = Guid.NewGuid()
				};

				SetProperties(publishedCore, core, false);

				publishedCore.Versions = new List<ContentVersionReference>
				                         {
					                         new ContentVersionReference
					                         {
						                         Id = version.Id,
												 Language = version.Language,
												 Slug = version.Slug
					                         }
				                         };

				repository.Content.Cores.Add(publishedCore);

				published = new ContentPublished();

				SetProperties(published, version);

				published.CoreId = publishedCore.Id;
				published.DatePublished = version.DateModified;

				repository.Content.Published.Add(published);

				List<HierarchicalTaxonomy> publishedServicelines = HierarchicalTaxonomiesFromCore(publishedCore);
				indexer.Add(new IndexableContent
					{
						Version = published,
						Core = publishedCore,
						Bucket = bucket,
						Collections = null,
						Segments = request.Segments,
						ServiceLines = publishedServicelines
					}, IndexType.Published);
			}
			else
			{
				ContentCore publishedCore = repository.Content.Cores.GetById(published.CoreId);

				if (publishedCore == null)
					throw new InvalidOperationException(ErrorMessage.Content.ORPHANED_CONTENT_NO_CORE);

				SetProperties(publishedCore, core, false);

				repository.Content.Cores.Update(publishedCore);

				SetProperties(published, version);

				published.CoreId = publishedCore.Id;
				published.DatePublished = version.DateModified;

				repository.Content.Published.Update(published);

				List<HierarchicalTaxonomy> publishedServicelines = HierarchicalTaxonomiesFromCore(publishedCore);
				indexer.Update(new IndexableContent
					{
						Version = published,
						Core = publishedCore,
						Bucket = bucket,
						Collections = null,
						Segments = request.Segments,
						ServiceLines = publishedServicelines
					}, IndexType.Published);

				// update the updated date on all subordinate language versions
				foreach (ContentVersionReference reference in publishedCore.Versions)
				{
					ContentPublished subordinate = repository.Content.Published.GetById(reference.Id);
					if (subordinate == null)
						continue;

					subordinate.DateUpdated = version.DateUpdated;
					repository.Content.Published.Update(subordinate);
				}

			}
		}

		private ContentMetadataResponse SaveUpdatedSibling(ContentArticleRequest request, ContentCore core, ContentVersion version, ContentBucket bucket)
		{
			Repository repository = new Repository();

			ContentPublished publishedMaster = null;

			// validate that we can publish
			if (request.Publish)
			{
				publishedMaster = repository.Content.Published.GetById(core.MasterId);

				if (publishedMaster == null)
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.MUST_PUBLISH_MASTER_FIRST);
			}

			version.Segments = SaveBody(bucket, request.Segments, version.Id);

			repository.Content.Versions.Update(version);

			ContentIndexer indexer = new ContentIndexer();

			List<HierarchicalTaxonomy> servicelines = HierarchicalTaxonomiesFromCore(core);
			indexer.Update(new IndexableContent
				{
					Version = version,
					Core = core,
					Bucket = bucket,
					Collections = null,
					Segments = request.Segments,
					ServiceLines = servicelines
				}, IndexType.Content);

			if (request.Publish)
				PublishUpdatedSibling(request, version, core, bucket, publishedMaster);

			// save to history
			AddHistory(version, core, version.DateModified, request.Publish ? ContentRevisionType.Published : ContentRevisionType.Draft);

			return GetResponse<ContentMetadataResponse>(core, version, bucket, repository.Content.Origins.GetById(bucket.OriginId));
		}

		private void PublishUpdatedSibling(ContentArticleRequest request, ContentVersion version, ContentCore core, ContentBucket bucket, ContentPublished publishedMaster)
		{
			Repository repository = new Repository();
			ContentIndexer indexer = new ContentIndexer();

			ContentCore publishedCore = repository.Content.Cores.GetById(publishedMaster.CoreId);

			if (publishedCore == null)
				throw new ApplicationException(ErrorMessage.Content.ORPHANED_CONTENT_NO_CORE);

			ContentPublished published = repository.Content.Published.GetById(version.Id);

			if (published == null)
			{
				published = new ContentPublished();

				SetProperties(published, version);

				published.CoreId = publishedCore.Id;
				published.DatePublished = version.DateModified;

				repository.Content.Published.Add(published);

				List<HierarchicalTaxonomy> publishedServicelines = HierarchicalTaxonomiesFromCore(publishedCore);
				indexer.Add(new IndexableContent
					{
						Version = published,
						Core = publishedCore,
						Bucket = bucket,
						Collections = null,
						Segments = request.Segments,
						ServiceLines = publishedServicelines
					}, IndexType.Published);
			}
			else
			{
				SetProperties(published, version);

				published.CoreId = publishedCore.Id;
				published.DatePublished = version.DateModified;

				repository.Content.Published.Update(published);

				List<HierarchicalTaxonomy> publishedServicelines = HierarchicalTaxonomiesFromCore(publishedCore);
				indexer.Add(new IndexableContent
				{
					Version = published,
					Core = publishedCore,
					Bucket = bucket,
					Collections = null,
					Segments = request.Segments,
					ServiceLines = publishedServicelines
				}, IndexType.Published);
			}
		}

		#endregion

		#region Taxonomy Methods

		private List<HierarchicalTaxonomy> HierarchicalTaxonomiesFromCore(ContentCore core)
		{
			if (core == null || core.Servicelines == null)
				return null;

			//List<HierarchicalTaxonomyItem> items = core.Servicelines.Select(line => new HierarchicalTaxonomyItem { Path = line.Path, Slug = line.Slug, Type = line.Type }).ToList();
			//List<HierarchicalTaxonomyItem> items = core.Servicelines.Select(line => new HierarchicalTaxonomyItem { Path = line.Path, Slug = line.Slug, Type = line.Type }).ToList();

			List<string> paths = core.Servicelines.Select(HierarchicalTaxonomyLogic.GetFullPath).ToList();
			return Repository.Content.HierarchicalTaxonomy.GetHierarchicalTaxonomiesByFullPaths(paths).ToList();
		}

		private List<HierarchicalTaxonomy> ValidateServicelines(ContentCore core)
		{
			if (core == null || core.Servicelines == null)
				return null;

			if (core.Servicelines.Any(line => line.Type != HierarchicalTaxonomyType.Serviceline))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Serviceline.INVALID_SERVICE_LINE_TYPE);

			List<HierarchicalTaxonomy> servicelines = HierarchicalTaxonomiesFromCore(core);

			var missing = core.Servicelines.Any(x => servicelines.All(line => line.FullPath != HierarchicalTaxonomyLogic.GetFullPath(x)));

			if (missing)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Serviceline.INVALID_SERVICE_LINE);

			return servicelines;
		}

		private static Dictionary<string, List<TaxonomyValue>> TaxonomiesFromRequest(ContentArticleRequest request)
		{
			return request.Taxonomies == null
					   ? null
					   : request.Taxonomies.Where(x => x.Items != null)
					   .ToDictionary(taxonomy => taxonomy.Slug,
														 taxonomy =>
														 taxonomy.Items.Select(value => new TaxonomyValue
														 {
															 Value = value.Value.Trim(),
															 Source = value.Source == TaxonomyValueSource.None
																		? TaxonomyValueSource.Ksw
																		: value.Source
														 }).ToList());
		}

		private static Dictionary<string, List<string>> CustomAttributesFromRequest(ContentArticleRequest request)
		{
			if (request.CustomAttributes == null) return null;

			// need to perform some validation here for problems that could cause the dictionary conversion to fail
			if (request.CustomAttributes.GroupBy(a => a.Name).Any(g => g.Count() > 1))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.DUPLICATE_CUSTOM_ATTRIBUTE_NAME);

			if (request.CustomAttributes.Any(attr => attr.CustomAttributeValues == null
				|| attr.CustomAttributeValues.Count == 0))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.CUSTOM_ATTRIBUTE_MUST_HAVE_VALUE);

			Dictionary<string, List<string>> dictionary = request.CustomAttributes == null ? null
				: request.CustomAttributes.Where(attr => attr.CustomAttributeValues != null).ToDictionary(
				attr => attr.Name, attr => attr.CustomAttributeValues.Select(value => value.Values.Trim()).ToList());

			return dictionary;
		}

		#endregion

		#region Reindexing Methods

		private void Reindex(IndexType type, ContentIndexer indexer, Repository repository, ContentVersion version)
		{
			if (version.Status != ObjectStatus.Active)
				return;

			ContentCore core = repository.Content.Cores.GetById(version.CoreId);
			if (core == null)
				return;

			ContentBucket bucket = repository.Content.Buckets.GetById(version.BucketId);
			if (bucket == null)
				return;

			List<ContentSegmentRequest> segments = new List<ContentSegmentRequest>();
			if (version.Segments != null)
			{
				foreach (ContentSegmentItem segmentItem in version.Segments)
				{
					ContentSegment segment = repository.Content.Segments.GetById(segmentItem.Id);
					if (segment == null)
						continue;
					segments.Add(new ContentSegmentRequest
					{
						IdOrSlug = segmentItem.Id.ToString(),
						Body = segment.Body,
						CustomName = segmentItem.CustomName
					});
				}
			}

			List<HierarchicalTaxonomy> servicelines = HierarchicalTaxonomiesFromCore(core);
			indexer.Add(new IndexableContent
				{
					Version = version,
					Core = core,
					Bucket = bucket,
					Collections = null,
					Segments = segments,
					ServiceLines = servicelines
				}, type);
		}


		#endregion

		#region Setters

		private void SetProperties(ContentVersion destination, ContentVersion source)
		{
			destination.Id = source.Id;
			destination.Slug = source.Slug;
			destination.Status = source.Status;
			destination.LegacyId = source.LegacyId;
			destination.Title = source.Title;
			destination.InvertedTitle = source.InvertedTitle;
			destination.Blurb = source.Blurb;
			destination.AlternateTitles = source.AlternateTitles;
			destination.BucketId = source.BucketId;
			destination.Paths = source.Paths;
			destination.DateAdded = source.DateAdded;
			destination.DateModified = source.DateModified;
			destination.DateUpdated = source.DateUpdated;
			destination.DatePublished = source.DatePublished;
			destination.IsMaster = source.IsMaster;
			destination.Language = source.Language;
			destination.Segments = source.Segments;
			destination.CoreId = source.CoreId;
			destination.Published = source.Published;
		}

		private void SetProperties(ContentCore destination, ContentCore source, bool setId = true)
		{
			if (setId)
				destination.Id = source.Id;
			destination.MasterId = source.Id;
			destination.AgeCategories = source.AgeCategories;
			destination.Authors = source.Authors;
			destination.CopyrightId = source.CopyrightId;
			destination.FleschKincaidReadingLevel = source.FleschKincaidReadingLevel;
			destination.Gender = source.Gender;
			destination.GunningFogReadingLevel = source.GunningFogReadingLevel;
			destination.LastReviewedDate = source.LastReviewedDate;
			destination.OnlineEditors = source.OnlineEditors;
			destination.OnlineMedicalReviewers = source.OnlineMedicalReviewers;
			destination.OnlineOriginatingSources = source.OnlineOriginatingSources;
			destination.PostingDate = source.PostingDate;
			destination.PrintOriginatingSources = source.PrintOriginatingSources;
			destination.RecommendedSites = source.RecommendedSites;
			destination.Servicelines = source.Servicelines;
			destination.Taxonomies = source.Taxonomies;
			destination.CustomAttributes = source.CustomAttributes;
			destination.Versions = source.Versions;
		}

		private void SetProperties(ContentCore destination, ContentArticleRequest request)
		{
			destination.AgeCategories = request.AgeCategories;
			destination.Authors = request.Authors;
			destination.Gender = request.Gender;
			destination.OnlineEditors = request.OnlineEditors;
			destination.OnlineMedicalReviewers = request.OnlineMedicalReviewers;
			destination.OnlineOriginatingSources = request.OnlineOriginatingSources;
			destination.PrintOriginatingSources = request.PrintOriginatingSources;
			destination.RecommendedSites = request.RecommendedSites;
			destination.LastReviewedDate = request.LastReviewedDate;
			destination.PostingDate = request.PostingDate;
			destination.GunningFogReadingLevel = request.GunningFogReadingLevel;
			destination.FleschKincaidReadingLevel = request.FleschKincaidReadingLevel;
			destination.CopyrightId = request.CopyrightId;
			destination.Taxonomies = TaxonomiesFromRequest(request);
			destination.Servicelines = request.Servicelines;
			destination.CustomAttributes = CustomAttributesFromRequest(request);
		}

		#endregion

		#endregion
	}
}
