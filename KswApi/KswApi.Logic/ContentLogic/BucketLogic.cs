﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.Framework.Constants;
using KswApi.Logic.Framework.Helpers;
using KswApi.Poco.Content;
using KswApi.Repositories;
using KswApi.Utility.Helpers;

namespace KswApi.Logic.ContentLogic
{
    public class BucketLogic : LogicBase
    {
        #region Public Methods

        public ContentBucketResponse CreateBucket(ContentBucketCreateRequest bucket)
        {
            ValidateBucket(bucket);

            bucket.Name = bucket.Name.Trim();

            bucket.Slug = bucket.Slug.ToLower();

            ValidationHelper.ValidateSlug(bucket.Slug);

            Repository repository = new Repository();

            if (repository.Content.Buckets.GetByStatusAndSlug(ObjectStatus.Active, bucket.Slug) != null)
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.DUPLICATE_SLUG);

            ContentOrigin origin = repository.Content.Origins.GetById(bucket.OriginId);

            if (origin == null)
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_ORIGIN);

            ContentBucket newBucket = BucketFromRequest(bucket);

            newBucket.Status = ObjectStatus.Active;
            newBucket.DateAdded = DateTime.UtcNow;

            repository.Content.Buckets.Add(newBucket);

            return ResponseFromBucket(newBucket, origin);
        }

        public ContentBucketResponse GetBucket(string idOrSlug)
        {
            ContentBucket bucket = InternalGetBucket(idOrSlug);
            return ResponseFromBucket(bucket, GetOrigin(bucket.OriginId));
        }

		public ContentBucketList SearchBuckets(BucketSearchRequest request)
        {
			if (request == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.MISSING_MESSAGE_BODY);

			ValidationHelper.ValidateSearch(request.Offset, request.Count);

            Repository repository = new Repository();

			if (request.Type != null &&
				request.Type.Contains(ContentType.Article) && request.Type.Contains(ContentType.Text) && request.Type.Contains(ContentType.Image) && request.Type.Contains(ContentType.Video))
            {
                // if all types are specified, don't filter
				request.Type = null;
            }

			if (string.IsNullOrEmpty(request.Query) && string.IsNullOrEmpty(request.Origin) && request.LegacyId == null && request.ReadOnly == null && request.Type == null &&
				string.IsNullOrEmpty(request.CopyrightId))
            {
                // no parameters
                return new ContentBucketList
                {
                    Items = GetBucketResponseList(repository.Content.Buckets.GetByStatus(ObjectStatus.Active, request.Offset, request.Count)),
					Offset = request.Offset,
                    Total = repository.Content.Buckets.CountByStatus(ObjectStatus.Active)
                };
            }

            Guid guid = default(Guid);

			if (!string.IsNullOrEmpty(request.Origin))
				Guid.TryParse(request.Origin, out guid);

			if (!string.IsNullOrEmpty(request.Query))
				request.Query = request.Query.Trim().ToLower();

            Guid copyrightIdParsed;
            Guid? copyrightIdParsedExistent = null;
			if (Guid.TryParse(request.CopyrightId, out copyrightIdParsed))
            {
                copyrightIdParsedExistent = copyrightIdParsed;
            }

            ContentBucketSearch search = new ContentBucketSearch
            {
				LegacyId = request.LegacyId,
                Origin = guid == default(Guid) ? null : (Guid?)guid,
				Query = request.Query,
				ReadOnly = request.ReadOnly,
				Type = request.Type,
                CopyrightId = copyrightIdParsedExistent
            };

            return new ContentBucketList
            {
				Items = GetBucketResponseList(repository.Content.Buckets.GetByStatusAndSearch(ObjectStatus.Active, search, request.Offset, request.Count)),
				Offset = request.Offset,
                Total = repository.Content.Buckets.CountByStatusAndSearch(ObjectStatus.Active, search)
            };
        }

        public SlugResponse GetSlug(string slug, string title)
        {
            // Checks against slugs in Topics.
            // In collection logic since only topics that represent collections have their slug specified

            if (string.IsNullOrEmpty(slug) && string.IsNullOrWhiteSpace(title))
                return CreateSlugResponse(string.Empty);

            if (string.IsNullOrEmpty(slug))
                slug = SlugHelper.CreateSlug(title.Trim());
            else
            {
                slug = slug.ToLower();
                ValidationHelper.ValidateSlug(slug);
            }

            ContentBucket bucket = Repository.Content.Buckets.GetByStatusAndSlug(ObjectStatus.Active, slug);

            if (bucket == null)
                return CreateSlugResponse(slug);

            // find next available slug
            int min = 1, current = min, max = min;

            while (true)
            {
                string candidate = slug + '-' + current;
                // logarithmic search
                bucket = Repository.Content.Buckets.GetByStatusAndSlug(ObjectStatus.Active, candidate);
                if (bucket == null)
                {
                    if (min >= current)
                        return CreateSlugResponse(candidate);

                    max = current;
                    current = (max + min) / 2;
                }
                else
                {
                    min = current + 1;

                    if (current >= max)
                        current = current * 2;
                    else
                        current = (min + max) / 2;
                }
            }
        }

        private SlugResponse CreateSlugResponse(string slug)
        {
            return new SlugResponse
            {
                Value = slug
            };
        }

        public ContentBucketResponse UpdateBucket(string idOrSlug, ContentBucketUpdateRequest bucket)
        {
            ValidateBucket(bucket, true);

            bucket.Name = bucket.Name.Trim();

            Repository repository = new Repository();

            ContentBucket existingBucket = InternalGetBucket(idOrSlug);

            //if (!string.IsNullOrEmpty(bucket.Slug) && bucket.Slug != existingBucket.Slug)
            //    throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Bucket.MODIFYING_SLUG);

            if (bucket.Type != ContentType.None && bucket.Type != existingBucket.Type)
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.BUCKET_TYPE_CANNOT_BE_CHANGED);

            if (bucket.OriginId != existingBucket.OriginId)
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.ORIGIN_CANNOT_BE_CHANGED);

            existingBucket.ReadOnly = bucket.ReadOnly;
            existingBucket.Segments = GetSegments(bucket.Segments, existingBucket.Segments);
            existingBucket.Name = bucket.Name;
            existingBucket.CaseInsensitiveName = bucket.Name.ToLower();
            existingBucket.Type = bucket.Type;
            existingBucket.LegacyId = bucket.LegacyId;
            existingBucket.Constraints = bucket.Constraints;
            existingBucket.OriginId = bucket.OriginId;
            existingBucket.Description = bucket.Description;
            existingBucket.CopyrightId = bucket.CopyrightId;

            repository.Content.Buckets.Update(existingBucket);

            return ResponseFromBucket(existingBucket, GetOrigin(existingBucket.OriginId));
        }

        public ContentBucketResponse DeleteBucket(string idOrSlug)
        {
            ContentBucket existingBucket = InternalGetBucket(idOrSlug);

            Repository repository = new Repository();

            existingBucket.Status = ObjectStatus.Deleted;

            repository.Content.Buckets.Update(existingBucket);

            Guid existingBucketId = existingBucket.Id;

            switch (existingBucket.Type)
            {
                case ContentType.Article:
                case ContentType.Text:
                    DeleteArticles(existingBucketId);
                    break;
                case ContentType.Image:
                    DeleteImages(existingBucketId);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            ContentModuleLogic contentModuleLogic = new ContentModuleLogic();
            IEnumerable<ContentModule> affectedContentModules = contentModuleLogic.GetByBucketId(existingBucketId.ToString());

            foreach (var affectedContentModule in affectedContentModules)
            {
                affectedContentModule.BucketIds.Remove(existingBucketId);
                repository.Content.ContentModule.Update(affectedContentModule);
            }

            return ResponseFromBucket(existingBucket, GetOrigin(existingBucket.OriginId));
        }

        private void DeleteArticles(Guid bucketId)
        {
            Repository repository = new Repository();

            ContentLogic contentLogic = new ContentLogic();
            IEnumerable<ContentVersion> associatedContent = repository.Content.Versions.GetByBucketId(bucketId);

            foreach (ContentVersion contentMetadata in associatedContent)
            {
                // only delete masters:
                // deleting the master cascades deletes
                if (contentMetadata.IsMaster && contentMetadata.Status == ObjectStatus.Active)
                    contentLogic.DeleteContent(bucketId.ToString(), contentMetadata.Id.ToString(), false);
            }
        }

        private void DeleteImages(Guid bucketId)
        {
            Repository repository = new Repository();

            ImageLogic imageLogic = new ImageLogic();

            IEnumerable<ImageDetail> associatedContent = repository.Content.ImageDetails.GetByBucketId(bucketId);

            foreach (var detail in associatedContent)
            {
                if (detail.Status == ObjectStatus.Active)
                    imageLogic.DeleteImage(detail);
            }
        }

        public ContentOriginList GetBucketOrigins()
        {
            Repository repository = new Repository();

            return new ContentOriginList
            {
                Items =
                    repository.Content.Origins.GetAll()
                        .OrderBy(item => item.Type == ContentOriginType.Ksw ? 0 : 1)
                        .ThenBy(item => item.CaseInsensitiveName)
                        .Select(item =>
                            new ContentOriginResponse
                            {
                                Id = item.Id,
                                Name = item.Name
                            }).ToList()
            };
        }

        /// <summary>
        /// This is called by the service initialization
        /// </summary>
        public void EnsureKswOrigin()
        {
            Guid guid = new Guid(Constant.Content.KSW_BUCKET_ORIGIN_ID);

            Repository repository = new Repository();
            ContentOrigin origin = repository.Content.Origins.GetById(guid);

            if (origin == null)
            {
                origin = new ContentOrigin
                {
                    Id = guid,
                    Name = Constant.Content.KSW_BUCKET_ORIGIN_NAME,
                    CaseInsensitiveName = Constant.Content.KSW_BUCKET_ORIGIN_NAME.ToLower(),
                    Type = ContentOriginType.Ksw
                };
                repository.Content.Origins.Add(origin);
                return;
            }

            if (origin.Name == Constant.Content.KSW_BUCKET_ORIGIN_NAME && origin.CaseInsensitiveName == Constant.Content.KSW_BUCKET_ORIGIN_NAME.ToLower() &&
                origin.Type == ContentOriginType.Ksw)
                return;

            origin.Name = Constant.Content.KSW_BUCKET_ORIGIN_NAME;
            origin.CaseInsensitiveName = Constant.Content.KSW_BUCKET_ORIGIN_NAME.ToLower();
            origin.Type = ContentOriginType.Ksw;

            repository.Content.Origins.Update(origin);
        }

        #endregion

        #region Private Methods

        public ContentBucket InternalGetBucket(string idOrSlug)
        {
            Repository repository = new Repository();

            Guid guid;

            ContentBucket bucket;
            if (Guid.TryParse(idOrSlug, out guid))
            {
                bucket = repository.Content.Buckets.GetById(guid);

                if (bucket != null)
                {
                    if (bucket.Status != ObjectStatus.Active)
                        throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.BUCKET_NOT_FOUND);

                    return bucket;
                }
            }

            idOrSlug = idOrSlug.ToLower();

            bucket = repository.Content.Buckets.GetByStatusAndSlug(ObjectStatus.Active, idOrSlug);

            if (bucket == null)
                throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.BUCKET_NOT_FOUND);

            return bucket;
        }

        private void ValidateBucket(ContentBucketUpdateRequest bucket, bool allowUpdate = false)
        {
            if (bucket == null)
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.MISSING_MESSAGE_BODY);

            if (bucket is ContentBucketCreateRequest)
            {
                if (string.IsNullOrEmpty((bucket as ContentBucketCreateRequest).Slug))
                    throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.SLUG_REQUIRED);
            }

            switch (bucket.Type)
            {
                case ContentType.Article:
                case ContentType.Text:
                    ValidateSegments(bucket.Segments, allowUpdate);
                    break;
                case ContentType.Image:
                case ContentType.Video:
                    break;
                default:
                    throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.TYPE_REQUIRED);
            }

            if (string.IsNullOrEmpty(bucket.Name))
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.NAME_REQUIRED);

            if (bucket.OriginId == Guid.Empty)
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.ORIGIN_REQUIRED);
        }

        private void ValidateSegments(List<ContentBucketSegmentRequest> segments, bool allowUpdate = false)
        {
            if (segments == null)
                return;

            if (segments.Any(item => item == null))
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.EMPTY_SEGMENT);

            if (segments.Any(item => string.IsNullOrEmpty(item.Name)))
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.SEGMENT_NAME_REQUIRED);

            if (segments.Where(item => item.Id.HasValue && item.Id != default(Guid)).GroupBy(item => item.Id).Any(item => item.Count() > 1))
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.DUPLICATE_SEGMENT_ID);

            if (allowUpdate)
            {
                // set slugs to lower case
                segments.ForEach(item => item.Slug = !string.IsNullOrEmpty(item.Slug) ? item.Slug.ToLower() : null);

                if (segments.Where(item => item.Slug != null).Any(item => !ValidationHelper.IsValidSlug(item.Slug)))
                    throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_SEGMENT_SLUG);
            }
            else
            {
                if (segments.Any(item => item.Slug == null))
                    throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.SEGMENT_SLUG_REQUIRED);

                // set slugs to lower case
                segments.ForEach(item => item.Slug = item.Slug.ToLower());

                if (segments.Any(item => !ValidationHelper.IsValidSlug(item.Slug)))
                    throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_SEGMENT_SLUG);
            }
        }

        private ContentBucket BucketFromRequest(ContentBucketCreateRequest bucket, List<ContentBucketSegment> existingSegments = null)
        {
            return new ContentBucket
                       {
                           ReadOnly = bucket.ReadOnly,
                           Segments = GetSegments(bucket.Segments, existingSegments),
                           Slug = bucket.Slug,
                           Name = bucket.Name,
                           CaseInsensitiveName = bucket.Name.ToLower(),
                           Type = bucket.Type,
                           LegacyId = bucket.LegacyId,
                           Constraints = bucket.Constraints,
                           OriginId = bucket.OriginId,
                           Description = bucket.Description,
                           CopyrightId = bucket.CopyrightId
                       };
        }

        private void UpdateSegmentSlugs(List<ContentBucketSegmentRequest> segments, List<ContentBucketSegment> existingSegments)
        {
            foreach (ContentBucketSegmentRequest segment in segments)
            {
                if (segment.Id == null)
                {
                    if (segment.Slug == null)
                        throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.SEGMENT_SLUG_REQUIRED);

                    continue;
                }

                ContentBucketSegment existingSegment = existingSegments.FirstOrDefault(item => item.Id == segment.Id.Value);

                if (existingSegment == null)
                {
                    if (segment.Slug == null)
                        throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.SEGMENT_SLUG_REQUIRED);

                    continue;
                }

                if (segment.Slug == null)
                {
                    segment.Slug = existingSegment.Slug;
                    continue;
                }

                if (segment.Slug != existingSegment.Slug)
                {
                    throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.SEGMENT_SLUG_CANNOT_BE_MODIFIED);
                }
            }

            // revalidate for duplicate slugs, because a new slug could
            // be the same as an existing slug
            if (segments.GroupBy(item => item.Slug).Any(group => group.Count() > 1))
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.DUPLICATED_SEGMENT_SLUG);
        }

        private List<ContentBucketSegment> GetSegments(List<ContentBucketSegmentRequest> segments, List<ContentBucketSegment> existingSegments = null)
        {
            if (segments == null)
            {
                segments = new List<ContentBucketSegmentRequest>
					           {
						           
								   new ContentBucketSegmentRequest
									   {
										   Id = Guid.NewGuid(),
										   Name = "Default",
										   Required = false,
										   Slug = "default"
									   }
					           };
            }

            if (existingSegments != null)
                UpdateSegmentSlugs(segments, existingSegments);

            List<ContentBucketSegment> result = segments.Select(item => new ContentBucketSegment
                                          {
                                              Id = item.Id.HasValue && item.Id != default(Guid) ? item.Id.Value : Guid.NewGuid(),
                                              Name = item.Name,
                                              Slug = item.Slug,
                                              Required = item.Required
                                          }).ToList();

            if (result.Any(item => string.IsNullOrEmpty(item.Slug)))
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.INVALID_SEGMENT_SLUG);

            if (result.GroupBy(item => item.Slug).Any(group => group.Count() > 1))
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.DUPLICATED_SEGMENT_SLUG);

            return result;
        }

        private ContentBucketResponse ResponseFromBucket(ContentBucket bucket, ContentOrigin origin)
        {
            return new ContentBucketResponse
            {
                Id = bucket.Id,
                ReadOnly = bucket.ReadOnly,
                Segments = bucket.Segments,
                Slug = bucket.Slug,
                Name = bucket.Name,
                Type = bucket.Type,
                LegacyId = bucket.LegacyId,
                Constraints = bucket.Constraints,
                OriginId = bucket.OriginId,
                OriginName = origin.Name,
                OriginType = origin.Type,
                Status = bucket.Status,
                TypeDescription = GetTypeDescription(bucket.Type),
                Description = bucket.Description,
                DateAdded = bucket.DateAdded,
                CopyrightId = bucket.CopyrightId,

            };
        }

        private string GetTypeDescription(ContentType type)
        {
            switch (type)
            {
                case ContentType.Article:
                    return "Article (XHTML)";
                case ContentType.Text:
                    return "Plain Text";
                case ContentType.Image:
                case ContentType.Video:
                    return type.ToString();
                default:
                    throw new ArgumentOutOfRangeException("type");
            }
        }

        private ContentOrigin GetOrigin(Guid originId)
        {
            if (originId.ToString() == Constant.Content.KSW_BUCKET_ORIGIN_ID)
                return GetKswOrigin();

            Repository repository = new Repository();

            ContentOrigin origin = repository.Content.Origins.GetById(originId);

            if (origin == null)
                throw new InvalidOperationException(string.Format("Content origin is missing: {0}.", originId));

            return origin;
        }

        private List<ContentBucketResponse> GetBucketResponseList(IEnumerable<ContentBucket> buckets)
        {
            Repository repository = null;
            ContentOrigin kswOrigin = GetKswOrigin();

            Dictionary<Guid, ContentOrigin> origins = new Dictionary<Guid, ContentOrigin>();

            List<ContentBucketResponse> response = new List<ContentBucketResponse>();

            foreach (ContentBucket bucket in buckets)
            {
                ContentOrigin origin = kswOrigin;

                if (origin.Id != bucket.OriginId)
                {
                    if (!origins.TryGetValue(bucket.OriginId, out origin))
                    {
                        if (repository == null)
                            repository = new Repository();
                        origin = repository.Content.Origins.GetById(bucket.OriginId);
                        if (origin == null)
                            throw new ServiceException(HttpStatusCode.InternalServerError, ErrorMessage.Content.ORPHANED_BUCKET_NO_ORIGIN);
                        origins[origin.Id] = origin;
                    }
                }

                response.Add(ResponseFromBucket(bucket, origin));
            }

            return response;
        }

        private ContentOrigin GetKswOrigin()
        {
            return new ContentOrigin
            {
                Id = new Guid(Constant.Content.KSW_BUCKET_ORIGIN_ID),
                Name = Constant.Content.KSW_BUCKET_ORIGIN_NAME,
                CaseInsensitiveName = Constant.Content.KSW_BUCKET_ORIGIN_NAME.ToLower(),
                Type = ContentOriginType.Ksw
            };
        }

        #endregion
    }
}
