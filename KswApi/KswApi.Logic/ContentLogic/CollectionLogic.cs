﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using KswApi.Common.Configuration;
using KswApi.Common.Objects;
using KswApi.Indexing;
using KswApi.Indexing.Filters;
using KswApi.Indexing.Objects;
using KswApi.Indexing.Queries;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.Content;
using KswApi.Ioc;
using KswApi.Logic.Administration;
using KswApi.Logic.Framework.Constants;
using KswApi.Logic.Framework.Objects;
using KswApi.Poco.Administration;
using KswApi.Poco.Content;
using KswApi.Logic.Framework.Helpers;
using KswApi.Utility.Helpers;

namespace KswApi.Logic.ContentLogic
{
	public class CollectionLogic : LogicBase
	{
		#region Public methods

		public CollectionListResponse SearchCollections(CollectionSearchRequest request, AccessToken token)
		{
			if (request == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.MISSING_MESSAGE_BODY);

			if (request.Offset == null)
				request.Offset = 0;
			if (request.Count == null)
				request.Count = 0;

			ValidationHelper.ValidateSearch(request.Offset.Value, request.Count.Value);

			List<SearchFilter> filters = new List<SearchFilter>();

			// filter by starts with
			if (!string.IsNullOrEmpty(request.TitleStartsWith))
				filters.Add(new PrefixFilter("exact-title", request.TitleStartsWith.ToLower()));

			Guid? licenseId = token != null ? token.LicenseId : null;

			return SearchCollections(request, licenseId, licenseId);
		}

        public CollectionResponse CreateCollection(CollectionCreateRequest request, AccessToken token)
		{
			// we're doing a lazy-update, specifying all objects in the helper methods,
			// and only updating after returning. This lets us do validation as we populate values

			if (request == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.NULL_COLLECTION);

			if (string.IsNullOrEmpty(request.Slug))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.SLUG_REQUIRED);

			request.Slug = request.Slug.ToLower();
			ValidationHelper.ValidateSlug(request.Slug);

			// todo: add this to create
			Topic collectionTopic = Repository.Content.Topics.GetBySlug(request.Slug);
			if (collectionTopic != null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.DUPLICATE_SLUG);

			// todo: add this to update collection
			//if (!isNew && collectionTopic == null)
			//	throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.COLLECTION_NOT_FOUND);

			Dictionary<ItemReference, Guid> contentLookup = null;

			DateTime now = DateTime.UtcNow;

			Topic newTopic = NewTopicFromRequest(request, now);

			Collection newCollection = NewCollectionFromRequest(newTopic.Id, request, token);

			List<Guid> path = new List<Guid> { newTopic.Id };

			List<Guid> children = null;

			CollectionMethodParameters parameters = new CollectionMethodParameters
			{
				Now = now,
				Path = path,
				ContentLookup = GetContentLookup(request, true),
				CollectionId = newTopic.Id,
				StructuresToAdd = new List<CollectionStructure>(),
				TopicsToAdd = new List<Topic> { newTopic },
				UsedTopicSlugs = new HashSet<string> { newTopic.Slug }
			};

			if (request.Items != null)
				children = GetCollectionItemsFromRequest(request.Items, parameters);

			CollectionStructure structure = new CollectionStructure
			{
				Id = newTopic.Id,
				Path = new List<Guid>(parameters.Path),
				Children = children,
				Type = CollectionItemType.Collection,
				CollectionId = newTopic.Id,
				ItemId = newTopic.Id
			};

			parameters.StructuresToAdd.Add(structure);

			Repository.Content.Collections.Add(newCollection);

			foreach (Topic topic in parameters.TopicsToAdd)
				Repository.Content.Topics.Add(topic);
			foreach (CollectionStructure collectionStructure in parameters.StructuresToAdd)
				Repository.Content.CollectionStructures.Add(collectionStructure);

			AddRevision(newCollection, newTopic, CollectionRevisionType.Created);

			// indexer
			CollectionIndexer indexer = new CollectionIndexer();
			indexer.Add(newCollection, newTopic, IndexType.Collection);

			List<Guid> contentIdsToUpdate = parameters.StructuresToAdd.Where(item => item.Type == CollectionItemType.Content).Select(item => item.ItemId).ToList();
			// update the content-collection map

			if (contentIdsToUpdate.Count > 0)
				UpdateContentCollectionAssignments(contentIdsToUpdate);

			CollectionResponse response = ResponseFromCollection(newCollection, newTopic, token.LicenseId);

			TopicLogic topicLogic = new TopicLogic();

			response.Items = topicLogic.GetTopicItems(newTopic, parameters.StructuresToAdd, parameters.TopicsToAdd, true);

			return response;
		}

		public CollectionResponse UpdateCollection(string idOrSlug, CollectionRequest request, AccessToken token)
		{
			// we're doing a lazy-update, specifying all objects in the helper methods,
			// and only updating after returning. This lets us do validation as we populate values

			if (string.IsNullOrEmpty(idOrSlug))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.SLUG_OR_ID_REQUIRED);

			if (request == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.NULL_COLLECTION);

			Topic existingTopic = GetRootTopic(idOrSlug);

			Collection existingCollection = Repository.Content.Collections.GetById(existingTopic.Id);

			if (existingCollection == null)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Collection.COLLECTION_NOT_FOUND);

			ValidateCollectionAccess(token, existingCollection, existingTopic);

			DateTime now = DateTime.UtcNow;

			List<CollectionStructure> allStructures = Repository.Content.CollectionStructures.GetByPathElement(existingCollection.Id).ToList();

			List<Topic> allTopics = Repository.Content.Topics.GetByIds(allStructures.Where(item => item.Type == CollectionItemType.Topic).Select(item => item.ItemId).ToList()).ToList();



			//ValidateUpdateCollectionRequest(collectionRequest, existingTopic, out contentLookup, out topicLookup);

			List<Guid> children = null;

			ContentLookupLogic lookupLogic = new ContentLookupLogic();

			CollectionMethodParameters parameters = new CollectionMethodParameters
			{
				Now = now,
				Path = new List<Guid> { existingTopic.Id },
				ContentLookup = GetContentLookup(request, false),
				CollectionId = existingCollection.Id,
				StructuresToAdd = new List<CollectionStructure>(),
				StructuresToUpdate = new List<CollectionStructure>(),
				TopicsToAdd = new List<Topic>(),
				TopicsToUpdate = new List<Topic> { existingTopic },
				StructureLookup = allStructures.ToDictionary(item => item.Id),
				UsedTopicSlugs = new HashSet<string> { existingTopic.Slug },
				ExistingTopicLookup = allTopics.Select(item => new KeyValuePair<string, Topic>(item.Id.ToString(), item))
					.Union(allTopics.Select(item => new KeyValuePair<string, Topic>(item.Slug, item))).ToDictionary(item => item.Key, item => item.Value)
			};

			if (request.Items != null)
				children = GetCollectionItemsFromRequest(request.Items, parameters);

			CollectionStructure structure = new CollectionStructure
			{
				Id = existingTopic.Id,
				Path = new List<Guid>(parameters.Path),
				Children = children,
				Type = CollectionItemType.Collection,
				CollectionId = existingTopic.Id,
				ItemId = existingTopic.Id
			};

			parameters.StructuresToUpdate.Add(structure);

			UpdateCollectionFromRequest(existingCollection, existingTopic, request, now);

			// make sure the license count is correct
			existingCollection.LicenseCount = Repository.Content.LicensedCollections.CountByCollectionId(existingCollection.Id);

			// save stuff
			Repository.Content.Collections.Update(existingCollection);

			foreach (Topic topic in parameters.TopicsToUpdate)
				Repository.Content.Topics.Update(topic);
			foreach (Topic topic in parameters.TopicsToAdd)
				Repository.Content.Topics.Add(topic);
			foreach (CollectionStructure collectionStructure in parameters.StructuresToUpdate)
				Repository.Content.CollectionStructures.Update(collectionStructure);
			foreach (CollectionStructure collectionStructure in parameters.StructuresToAdd)
				Repository.Content.CollectionStructures.Add(collectionStructure);

			// delete any unused topics and structures
			List<Guid> touchedContent = new List<Guid>();

			if (allStructures.Count > 0)
			{
				Dictionary<Guid, CollectionStructure> updatedStructures = parameters.StructuresToUpdate.ToDictionary(item => item.Id);
				Dictionary<Guid, Topic> updateTopics = parameters.TopicsToUpdate.ToDictionary(item => item.Id);

				List<CollectionStructure> deletedStructureValues = allStructures.Where(item => !updatedStructures.ContainsKey(item.Id)).ToList();
				touchedContent.AddRange(deletedStructureValues.Where(item => item.Type == CollectionItemType.Content).Select(item => item.ItemId));

				List<Guid> deletedStructures = deletedStructureValues.Select(item => item.Id).ToList();
				List<Guid> deletedTopics = allTopics.Where(item => !updateTopics.ContainsKey(item.Id)).Select(item => item.Id).ToList();

				if (deletedStructures.Count > 0)
					Repository.Content.CollectionStructures.DeleteByIds(deletedStructures);
				if (deletedTopics.Count > 0)
					Repository.Content.Topics.DeleteByIds(deletedTopics);
			}

			AddRevision(existingCollection, existingTopic, CollectionRevisionType.Saved);

			CollectionIndexer indexer = new CollectionIndexer();
			indexer.Update(existingCollection, existingTopic, IndexType.Collection);

			touchedContent.AddRange(parameters.StructuresToAdd.Where(item => item.Type == CollectionItemType.Content).Select(item => item.ItemId));

			if (touchedContent.Count > 0)
				UpdateContentCollectionAssignments(touchedContent);

			CollectionResponse response = ResponseFromCollection(existingCollection, existingTopic, token.LicenseId);

			TopicLogic topicLogic = new TopicLogic();

			parameters.StructuresToAdd.AddRange(parameters.StructuresToUpdate);
			parameters.TopicsToAdd.AddRange(parameters.TopicsToUpdate);

			response.Items = topicLogic.GetTopicItems(existingTopic, parameters.StructuresToAdd, parameters.TopicsToAdd, true);

			return response;
		}

		public CollectionResponse DeleteCollection(string idOrSlug, AccessToken token)
		{
			Topic existingTopic = GetRootTopic(idOrSlug);

			Collection existingCollection = Repository.Content.Collections.GetById(existingTopic.Id);

			if (existingCollection == null)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Collection.COLLECTION_NOT_FOUND);

			ValidateCollectionAccess(token, existingCollection, existingTopic);

			AddRevision(existingCollection, existingTopic, CollectionRevisionType.Deleted);
			Repository.Content.Collections.Delete(existingCollection);

			// indexer
			CollectionIndexer indexer = new CollectionIndexer();
			indexer.Delete(existingCollection.Id, IndexType.Collection);

			Repository.Content.LicensedCollections.DeleteByCollectionId(existingCollection.Id);

			CollectionResponse response = ResponseFromCollection(existingCollection, existingTopic, token.LicenseId);

			IEnumerable<CollectionStructure> structures = Repository.Content.CollectionStructures.GetByPathElement(existingCollection.Id);

			List<Guid> ids = structures.Select(item => item.Id).ToList();

			Repository.Content.CollectionStructures.DeleteByIds(ids);
			Repository.Content.Topics.DeleteByIds(ids);

			return response;
		}

		public CollectionResponse GetCollection(string idOrSlug, bool? includeChildren, bool? recursive, AccessToken token)
		{
			Topic topic = GetRootTopic(idOrSlug);

			Collection collection = Repository.Content.Collections.GetById(topic.Id);

			if (collection == null)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Collection.COLLECTION_NOT_FOUND);

			ValidateCollectionAccess(token, collection, topic);

			CollectionResponse response = ResponseFromCollection(collection, topic, token.LicenseId);

			if ((includeChildren.HasValue && includeChildren.Value) || (recursive.HasValue && recursive.Value))
			{
				TopicLogic topicLogic = new TopicLogic();
				response.Items = topicLogic.GetTopicItems(topic, recursive.HasValue && recursive.Value);
			}

			return response;
		}

		internal void UpdateContentCollectionAssignments(List<Guid> contentIds)
		{
			contentIds = contentIds.Distinct().ToList();

			ContentIndexer indexer = new ContentIndexer();

			List<CollectionStructure> structures = Repository.Content.CollectionStructures.GetByItemIds(contentIds).ToList();

			List<Guid> collectionIds = structures.Select(item => item.CollectionId).ToList();

			Dictionary<Guid, Topic> topicLookup = Repository.Content.Topics.GetByIds(collectionIds).ToDictionary(item => item.Id);

			foreach (IGrouping<Guid, CollectionStructure> group in structures.GroupBy(item => item.ItemId).ToList())
			{
				List<Guid> collections = group.Select(item => item.CollectionId).ToList();
				List<string> slugs = new List<string>();
				foreach (Guid collectionId in collections)
				{
					Topic topic;
					if (topicLookup.TryGetValue(collectionId, out topic))
						slugs.Add(topic.Slug);
				}

				indexer.UpdateCollectionAssignments(group.Key, collections, slugs, IndexType.Content);
				indexer.UpdateCollectionAssignments(group.Key, collections, slugs, IndexType.Published);
			}
		}

		public void Reindex()
		{
			CollectionIndexer indexer = new CollectionIndexer();

			IEnumerable<Collection> collections = Repository.Content.Collections.GetAll();

			foreach (Collection collection in collections)
			{
				Topic topic = Repository.Content.Topics.GetById(collection.Id);
				if (topic == null)
					continue;

				indexer.Add(collection, topic, IndexType.Collection);
			}
		}

		#endregion

		#region Internal Methods

		internal CollectionListResponse SearchCollections(CollectionSearchRequest request, Guid? contextLicenseId, Guid? currentLicenseId)
		{
			if (request == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.MISSING_MESSAGE_BODY);

			if (request.Offset == null)
				request.Offset = 0;
			if (request.Count == null)
				request.Count = 0;

			List<SearchFilter> filters = new List<SearchFilter>();

			if (contextLicenseId != null)
			{
				IEnumerable<LicensedCollection> licensedCollections = Repository.Content.LicensedCollections.GetByLicenseId(contextLicenseId.Value);

				List<Guid> collectionIds = licensedCollections.Select(item => item.CollectionId).ToList();

				if (collectionIds.Count == 0)
					return EmptyResponse(request.Offset.Value);

				filters.Add(new IdsFilter(collectionIds));
			}

			// filter by starts with
			if (!string.IsNullOrEmpty(request.TitleStartsWith))
				filters.Add(new PrefixFilter("exact-title", request.TitleStartsWith.ToLower()));

			SearchFilter filter = null;

			if (filters.Count > 0)
			{
				filter = filters.Count == 1 ? filters[0] : new AndFilter(filters);
			}

			SearchQuery searchQuery = filter == null
										  ? SearchQuery.FromString(request.Query)
										  : new FilteredQuery(SearchQuery.FromString(request.Query), filter);

			// request.Types should contain a single type: Collection
			if (request.Types == null || request.Types.Count == 0)
				request.Types = new List<SearchType> { SearchType.Collection };

			// In anticipation...
			List<string> facets = null;
			SearchFilter searchFilter = null;
			IIndex index = Dependency.Get<IIndex>();

			List<SearchSort> sort = string.IsNullOrEmpty(request.Query)
										? new List<SearchSort> { new SearchSort("exact-title", "asc", "_last") }
										: null;

			SearchResponse list = index.Search(request.Offset.Value, request.Count.Value, searchQuery, searchFilter, facets, sort, IndexType.Collection);

			return new CollectionListResponse
			{
				Items = GetSearchResponse(list, currentLicenseId),
				Offset = request.Offset.Value,
				Total = list.Hits.Total
			};
		}

		internal List<CollectionResponse> GetResponses(IEnumerable<Guid> order, IEnumerable<Collection> collections, IEnumerable<Topic> topics, Guid? licenseId)
		{
			Dictionary<Guid, Collection> collectionLookup = collections.ToDictionary(item => item.Id);
			Dictionary<Guid, Topic> topicLookup = topics.ToDictionary(item => item.Id);
			Dictionary<Guid, UserDetailsResponse> userLookup = new Dictionary<Guid, UserDetailsResponse>();

			List<CollectionResponse> response = new List<CollectionResponse>();

			UserLogic logic = new UserLogic();

			foreach (Guid guid in order)
			{
				Collection collection;
				Topic topic;

				if (!collectionLookup.TryGetValue(guid, out collection) || !topicLookup.TryGetValue(guid, out topic))
					continue;

				CollectionResponse collectionResponse = ResponseFromCollection(collection, topic, licenseId, false);

				response.Add(collectionResponse);

				if (collection.CreatedBy == null)
					continue;

				UserDetailsResponse userDetails;

				if (!userLookup.TryGetValue(collection.CreatedBy.Id, out userDetails))
				{
					userDetails = logic.DetailResponseFromUser(collection.CreatedBy);
					userLookup[collection.CreatedBy.Id] = userDetails;
				}


				collectionResponse.CreatedBy = userDetails;
			}

			return response;
		}

		internal CollectionResponse ResponseFromCollection(Collection collection, Topic topic, Guid? licenseId, bool setCreatedBy = true)
		{
			CollectionResponse response = new CollectionResponse
			{
				Id = collection.Id,
				Slug = topic.Slug,
				DateAdded = topic.DateAdded,
				Title = topic.Title,
				DateModified = topic.DateModified,
				Description = topic.Description,
				Expires = collection.Expires,
				ImageUri = LicensifyImageUri(topic.ImageUri, licenseId),
				LegacyId = collection.LegacyId,
				LicenseCount = collection.LicenseCount
			};

			if (setCreatedBy)
			{
				if (collection.CreatedBy != null)
				{
					UserLogic logic = new UserLogic();
					response.CreatedBy = logic.DetailResponseFromUser(collection.CreatedBy);
				}
			}

			return response;
		}

		#endregion

		#region Private methods

		private List<Guid> GetCollectionItemsFromRequest(List<CollectionItemRequest> items, CollectionMethodParameters parameters)
		{
			List<Guid> children = new List<Guid>();

			foreach (CollectionItemRequest item in items)
			{
				switch (item.Type)
				{
					case CollectionItemType.Topic:
						if (item.ItemReference == null || string.IsNullOrEmpty(item.ItemReference.IdOrSlug))
							children.Add(AddTopic(item, parameters));
						else
							children.Add(UpdateTopic(item, parameters));
						break;
					case CollectionItemType.Content:
						children.Add(AddContent(item, parameters));
						break;
				}
			}

			return children;
		}

		private Guid AddContent(CollectionItemRequest item, CollectionMethodParameters parameters)
		{
			CollectionStructure structure = new CollectionStructure
			{
				Id = Guid.NewGuid(),
				CollectionId = parameters.CollectionId,
				Type = CollectionItemType.Content,
				Disabled = item.Disabled,
				ItemId = parameters.ContentLookup[item.ItemReference],
				ParentId = parameters.Path.Last(),
				Path = new List<Guid>(parameters.Path),
				Flagged = item.Flagged,
				FlagComment = item.Flagged ? item.FlagComment : null
			};

			parameters.StructuresToAdd.Add(structure);

			return structure.Id;
		}

		private Guid AddTopic(CollectionItemRequest item, CollectionMethodParameters parameters)
		{
			string slug = GetTopicSlug(item.Title, parameters.UsedTopicSlugs);
			item.ItemReference = new ItemReference { IdOrSlug = slug };
			parameters.UsedTopicSlugs.Add(slug);

			Topic topic = new Topic
						  {
							  Id = Guid.NewGuid(),
							  Slug = item.ItemReference.IdOrSlug,
                              Title = item.Title.Trim(),
							  ImageUri = item.ImageUri,
							  Description = item.Description,
							  DateAdded = parameters.Now,
							  DateModified = parameters.Now
						  };

			parameters.TopicsToAdd.Add(topic);

			Guid parentId = parameters.Path.Last();

			parameters.Path.Add(topic.Id);

			List<Guid> children = null;

			if (item.Items != null && item.Items.Count > 0)
				children = GetCollectionItemsFromRequest(item.Items, parameters);

			CollectionStructure structure = new CollectionStructure
											{
												Id = topic.Id,
												ItemId = topic.Id,
												CollectionId = parameters.CollectionId,
												ParentId = parentId,
												Path = new List<Guid>(parameters.Path),
												Children = children,
												Type = CollectionItemType.Topic,
												Flagged = item.Flagged,
												FlagComment = item.Flagged ? item.FlagComment : null
											};

			parameters.StructuresToAdd.Add(structure);

			parameters.Path.RemoveAt(parameters.Path.Count - 1);

			return structure.Id;
		}

		private Guid UpdateTopic(CollectionItemRequest item, CollectionMethodParameters parameters)
		{
			Topic topic;

			if (!parameters.ExistingTopicLookup.TryGetValue(item.ItemReference.IdOrSlug, out topic))
				throw new ServiceException(HttpStatusCode.BadRequest, string.Format(ErrorMessage.Collection.PARAMETERIZED_TOPIC_NOT_FOUND, item.ItemReference.IdOrSlug));

			topic.Description = item.Description;
			topic.DateModified = parameters.Now;
			topic.ImageUri = item.ImageUri;

			parameters.TopicsToUpdate.Add(topic);

			Guid parentId = parameters.Path.Last();

			parameters.Path.Add(topic.Id);

			List<Guid> children = null;

			if (item.Items != null && item.Items.Count > 0)
				children = GetCollectionItemsFromRequest(item.Items, parameters);

			CollectionStructure structure = new CollectionStructure
			{
				Id = topic.Id,
				ItemId = topic.Id,
				CollectionId = parameters.CollectionId,
				ParentId = parentId,
				Path = new List<Guid>(parameters.Path),
				Children = children,
				Type = CollectionItemType.Topic,
				Flagged = item.Flagged,
				FlagComment = item.Flagged ? item.FlagComment : null
			};

			parameters.StructuresToUpdate.Add(structure);

			parameters.Path.RemoveAt(parameters.Path.Count - 1);

			return structure.Id;
		}

		private string GetTopicSlug(string title, HashSet<string> usedSlugs)
		{
			string slug = SlugHelper.CreateSlug(title);

			// find next available slug
			int min = 1, current = min, max = min;

			if (!Repository.Content.Topics.Exists(slug) && !usedSlugs.Contains(slug))
				return slug;

			while (true)
			{
				string candidate = slug + '-' + current;
				// logarithmic search
				if (!Repository.Content.Topics.Exists(candidate) && !usedSlugs.Contains(candidate))
				{
					if (min >= current)
						return candidate;

					max = current;
					current = (max + min) / 2;
				}
				else
				{
					min = current + 1;

					if (current >= max)
						current = current * 2;
					else
						current = (min + max) / 2;
				}
			}
		}

		private Dictionary<ContentSlugPath, ContentPublished> ValidateContentItems(List<ContentPath> contentPaths)
		{
			if (contentPaths == null || contentPaths.Count == 0)
				return new Dictionary<ContentSlugPath, ContentPublished>();

			foreach (ContentPath path in contentPaths)
			{
				if (string.IsNullOrEmpty(path.BucketIdOrSlug))
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.BUCKET_ID_REQUIRED);

				if (string.IsNullOrEmpty(path.ContentIdOrSlug))
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.CONTENT_ID_REQUIRED);
			}

			List<ContentBucket> buckets = Repository.Content.Buckets.GetBySlugs(contentPaths.Select(path => path.BucketIdOrSlug).Distinct()).ToList();

			Dictionary<string, ContentBucket> bucketLookupBySlug = buckets.ToDictionary(bucket => bucket.Slug);

			if (contentPaths.Any(path => !bucketLookupBySlug.ContainsKey(path.BucketIdOrSlug)))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.BUCKET_NOT_FOUND); // use collection specific message?

			List<IdAndSlug> contentIdentifiers = contentPaths.Select(path => new IdAndSlug
			{
				Id = bucketLookupBySlug[path.BucketIdOrSlug].Id,
				Slug = path.ContentIdOrSlug
			}).Distinct().ToList();

			// this is a little heavyweight. Published method that checked for validity might be preferred
			List<ContentPublished> contentItems = Repository.Content.Published.GetByBucketIdsAndSlugs(contentIdentifiers).ToList();

			bool contentNotFound = contentIdentifiers.Any(idAndSlug =>
				!contentItems.Any(content => content.BucketId == idAndSlug.Id && content.Slug.Equals(idAndSlug.Slug, StringComparison.OrdinalIgnoreCase)));

			if (contentNotFound)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.CONTENT_NOT_FOUND); // use collection specific message?


			Dictionary<Guid, ContentBucket> bucketLookupById = buckets.ToDictionary(bucket => bucket.Id);

			Dictionary<ContentSlugPath, ContentPublished> contentDictionary =
				contentItems.ToDictionary(content => new ContentSlugPath { ContentSlug = content.Slug, BucketSlug = bucketLookupById[content.BucketId].Slug });

			return contentDictionary;
		}




		private void UpdateCollectionFromRequest(Collection existingCollection, Topic existingTopic, CollectionRequest request, DateTime updateDate)
		{
			existingTopic.DateModified = updateDate;
            existingTopic.Title = request.Title.Trim();
			existingTopic.Description = request.Description;
			existingCollection.Expires = request.Expires;
			existingTopic.ImageUri = request.ImageUri;
		}

        private Topic NewTopicFromRequest(CollectionCreateRequest request, DateTime creationDate)
		{
			return new Topic
					   {
						   Id = Guid.NewGuid(),
						   Type = TopicType.Root,
						   Slug = request.Slug,
						   Title = request.Title,
						   Description = request.Description,
						   ImageUri = request.ImageUri,
						   DateAdded = creationDate,
						   DateModified = creationDate
					   };
		}

		private Collection NewCollectionFromRequest(Guid id, CollectionRequest request, AccessToken token)
		{
			Collection collection = new Collection
									{
										Id = id,
										Expires = request.Expires,
										LegacyId = request.LegacyId
									};

			if (token != null && token.UserDetails != null)
				collection.CreatedBy = new User { Id = token.UserDetails.Id, Type = token.UserDetails.Type };

			return collection;
		}

		private string LicensifyImageUri(string imageUri, Guid? licenseId)
		{
			if (string.IsNullOrEmpty(imageUri))
				return imageUri;

			if (!licenseId.HasValue)
				return imageUri;

			string matchExpression = Settings.Current.WebPortalBaseUri +
									 "/Service/Get(/Content)(/[a-z0-9-]+/Images/[a-z0-9-]+(\\.[0-9]+x[0-9]+)?(\\.[a-z]{3,4})?)";
			string replaceExpression = Settings.Current.ServiceUri + "$1/" + licenseId + "$2";
			Regex regex = new Regex(matchExpression, RegexOptions.IgnoreCase);
			return regex.Replace(imageUri, replaceExpression);
		}

		private void ValidateCollectionAccess(AccessToken token, Collection collection, Topic topic)
		{
			// return if no license (ie in the case of Portal app)
			if (!token.LicenseId.HasValue)
				return;

			//if (!collection.AssignedLicenses.Contains(token.LicenseId.Value))
			//{
			//	// TODO: implement assign license to collection
			//	throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Collection.COLLECTION_NOT_FOUND);
			//}
		}

		private void AddRevision(Collection collection, Topic topic, CollectionRevisionType revisionType)
		{
			CollectionRevision revision = new CollectionRevision
											  {
												  Collection = collection,
												  Topic = topic,
												  RevisionType = revisionType,
												  RevisionDate = DateTime.UtcNow
											  };

			Repository.Content.CollectionRevisions.Add(revision);
		}

		/// <summary>
		/// Throws service exceptions
		/// </summary>
		/// <param name="idOrSlug"></param>
		/// <returns></returns>
		private Topic GetRootTopic(string idOrSlug)
		{
			Guid guid;

			Topic topic = Guid.TryParse(idOrSlug, out guid) ? Repository.Content.Topics.GetById(guid) : Repository.Content.Topics.GetBySlug(idOrSlug.ToLower());

			if (topic == null)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Collection.COLLECTION_NOT_FOUND);

			if (topic.Type != TopicType.Root)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Collection.COLLECTION_NOT_FOUND);

			return topic;
		}

		private List<CollectionResponse> GetSearchResponse(SearchResponse searchResponse, Guid? licenseId)
		{
			List<CollectionResponse> response = new List<CollectionResponse>();
			if (searchResponse.Hits.Items.Count == 0)
				return response;

			// TODO: include licenseid in query
			List<Guid> collectionIds = searchResponse.Hits.Items.Where(item => item.Type == IndexType.Collection).Select(item => item.Id).ToList();

			if (collectionIds.Count == 0)
				return response;

			IEnumerable<Collection> collections = Repository.Content.Collections.GetByIds(collectionIds);
			IEnumerable<Topic> topics = Repository.Content.Topics.GetByIds(collectionIds);

			return GetResponses(collectionIds, collections, topics, licenseId);
		}

		#endregion

		// TODO: this should be a helper method/class
		public SlugResponse GetSlug(string slug, string title)
		{
			// Checks against slugs in Topics.
			// In collection logic since only topics that represent collections have their slug specified
            
            if (string.IsNullOrEmpty(slug) && string.IsNullOrWhiteSpace(title))
					return CreateSlugResponse(string.Empty);

            if (string.IsNullOrEmpty(slug))
                slug = SlugHelper.CreateSlug(title.Trim());
			else
			{
				slug = slug.ToLower();
				ValidationHelper.ValidateSlug(slug);
			}

			Topic topic = Repository.Content.Topics.GetBySlug(slug);

			if (topic == null)
				return CreateSlugResponse(slug);

			// find next available slug
			int min = 1, current = min, max = min;

			while (true)
			{
				string candidate = slug + '-' + current;
				// logarithmic search
				topic = Repository.Content.Topics.GetBySlug(candidate);
				if (topic == null)
				{
					if (min >= current)
						return CreateSlugResponse(candidate);

					max = current;
					current = (max + min) / 2;
				}
				else
				{
					min = current + 1;

					if (current >= max)
						current = current * 2;
					else
						current = (min + max) / 2;
				}
			}
		}

		private CollectionListResponse EmptyResponse(int offset)
		{
			return new CollectionListResponse
			{
				Items = new List<CollectionResponse>(),
				Offset = offset,
			};
		}

		private SlugResponse CreateSlugResponse(string slug)
		{
			return new SlugResponse
			{
				Value = slug
			};
		}

		#region New Stuff

		private Dictionary<ItemReference, Guid> GetContentLookup(CollectionRequest request, bool isNew)
		{
			if (request.Items == null || request.Items.Count == 0)
				return null;

			List<ItemReference> contentItems = GetAllContentReferences(request.Items, isNew);

			if (contentItems.Count == 0)
				return null;

			ContentLookupLogic lookupLogic = new ContentLookupLogic();

			return lookupLogic.GetContentLookup(contentItems);
		}

		private List<ItemReference> GetAllContentReferences(List<CollectionItemRequest> items, bool isNew)
		{
			HashSet<string> usedTopicSlugs = new HashSet<string>();
			List<ItemReference> result = new List<ItemReference>();

			Stack<List<CollectionItemRequest>> stack = new Stack<List<CollectionItemRequest>>();

			while (true)
			{
				foreach (CollectionItemRequest request in items)
				{
					if (request == null)
						throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.NULL_COLLECTION_ITEM);

					switch (request.Type)
					{
						case CollectionItemType.Topic:
							
							ValidateTopicRequest(request, isNew, usedTopicSlugs);

							if (request.Items == null)
								continue;

							if (request.Items.Count == 0)
							{
								request.Items = null;
								continue;
							}

							stack.Push(request.Items);

							break;
						case CollectionItemType.Content:
							if (request.ItemReference == null)
								throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.NO_ITEM_REFERENCE);
							if (string.IsNullOrEmpty(request.ItemReference.BucketIdOrSlug))
								throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.BUCKET_ID_REQUIRED);
							if (string.IsNullOrEmpty(request.ItemReference.IdOrSlug))
								throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.CONTENT_ID_REQUIRED);
							result.Add(new ItemReference { BucketIdOrSlug = request.ItemReference.BucketIdOrSlug, IdOrSlug = request.ItemReference.IdOrSlug });
							break;
						default:
							throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.INVALID_TYPE);
					}
				}

				if (stack.Count == 0)
					break;

				items = stack.Pop();
			}

			return result;
		}

		private void ValidateTopicRequest(CollectionItemRequest topicRequest, bool isNew, HashSet<string> usedTopicSlugs)
		{
			if (topicRequest == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.NULL_COLLECTION_ITEM);

			if (string.IsNullOrEmpty(topicRequest.Title))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.ITEM_TITLE_REQUIRED);

			if (isNew)
			{
				if (topicRequest.ItemReference != null && !string.IsNullOrEmpty(topicRequest.ItemReference.IdOrSlug))
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.ID_ON_CREATE_TOPIC);
			}
			else
			{
				if (topicRequest.ItemReference != null && !string.IsNullOrEmpty(topicRequest.ItemReference.IdOrSlug))
				{
					if (usedTopicSlugs.Contains(topicRequest.ItemReference.IdOrSlug))
						throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Collection.DUPLICATE_TOPIC);

					usedTopicSlugs.Add(topicRequest.ItemReference.IdOrSlug);
				}
			}
		}

		#endregion
	}

}
