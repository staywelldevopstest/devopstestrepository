﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logic.Framework.Constants;
using KswApi.Poco.Content;

namespace KswApi.Logic.ContentLogic
{
	public class ContentLookupLogic : LogicBase
	{
		public Dictionary<ItemReference, Guid> GetContentLookup(List<ItemReference> paths)
		{
			paths = paths.Distinct().ToList();

			List<Tuple<ItemReference, IdAndSlug>> byBucketIdAndSlug = GetWithBucketIds(paths);

			List<Tuple<ItemReference, Guid>> list = GetWithContentIds(byBucketIdAndSlug);

			return list.ToDictionary(item => item.Item1, item => item.Item2);
		}

		private List<Tuple<ItemReference, IdAndSlug>> GetWithBucketIds(List<ItemReference> paths)
		{
			Guid guid;

			List<Tuple<ItemReference, IdAndSlug>> result = new List<Tuple<ItemReference, IdAndSlug>>();

			List<ItemReference> slugs = new List<ItemReference>();

			foreach (ItemReference path in paths)
			{
				if (Guid.TryParse(path.BucketIdOrSlug, out guid))
					result.Add(new Tuple<ItemReference, IdAndSlug>(path, new IdAndSlug { Id = guid, Slug = path.IdOrSlug }));
				else
					slugs.Add(path);
			}

			if (slugs.Count == 0)
				return result;

			Dictionary<string, Guid> lookup = Repository.Content.Buckets.GetBySlugs(slugs.Select(item => item.BucketIdOrSlug).ToList())
				.ToDictionary(item => item.Slug, item => item.Id);

			foreach (ItemReference path in slugs)
			{
				if (!lookup.TryGetValue(path.BucketIdOrSlug, out guid))
					throw new ServiceException(HttpStatusCode.BadRequest, string.Format(ErrorMessage.Bucket.NOT_FOUND_PARAMETERIZED, path.BucketIdOrSlug));

				result.Add(new Tuple<ItemReference, IdAndSlug>(path, new IdAndSlug { Id = guid, Slug = path.IdOrSlug }));
			}

			return result;
		}

		private List<Tuple<ItemReference, Guid>> GetWithContentIds(List<Tuple<ItemReference, IdAndSlug>> paths)
		{
			Guid guid;
            List<Tuple<ItemReference, Guid, Guid>> contentById = new List<Tuple<ItemReference, Guid, Guid>>();
            List<Tuple<ItemReference, IdAndSlug>> contentBySlug = new List<Tuple<ItemReference, IdAndSlug>>();
			List<Tuple<ItemReference, Guid>> result = new List<Tuple<ItemReference, Guid>>();

			foreach (Tuple<ItemReference, IdAndSlug> path in paths)
			{
				if (Guid.TryParse(path.Item1.IdOrSlug, out guid))
				{
                    contentById.Add(new Tuple<ItemReference, Guid, Guid>(path.Item1, path.Item2.Id, guid));
				}
				else
				{
                    contentBySlug.Add(path);
				}
			}

			if (contentBySlug.Count > 0)
			{
				IEnumerable<ContentPublished> content = Repository.Content.Published.GetByBucketIdsAndSlugs(contentBySlug.Select(item => item.Item2).ToList());

				Dictionary<IdAndSlug, ContentPublished> contentLookup = content.ToDictionary(item => new IdAndSlug { Id = item.BucketId, Slug = item.Slug });

				foreach (Tuple<ItemReference, IdAndSlug> tuple in contentBySlug)
				{
					ContentPublished item;
					if (!contentLookup.TryGetValue(tuple.Item2, out item))
						throw new ServiceException(HttpStatusCode.BadRequest, string.Format(ErrorMessage.Content.PARAMETERIZED_NOT_FOUND, tuple.Item1.BucketIdOrSlug, tuple.Item1.IdOrSlug));

					result.Add(new Tuple<ItemReference, Guid>(tuple.Item1, item.Id));
				}
			}

			if (contentById.Count > 0)
			{
				IEnumerable<ContentPublished> content = Repository.Content.Published.GetByIds(contentById.Select(item => item.Item3).ToList());

				Dictionary<Guid, ContentPublished> contentLookup = content.ToDictionary(item => item.Id);

				foreach (Tuple<ItemReference, Guid, Guid> tuple in contentById)
				{
					ContentPublished item;
					if (!contentLookup.TryGetValue(tuple.Item3, out item) || item.BucketId != tuple.Item2)
						throw new ServiceException(HttpStatusCode.BadRequest, string.Format(ErrorMessage.Content.PARAMETERIZED_NOT_FOUND, tuple.Item1.BucketIdOrSlug, tuple.Item1.IdOrSlug));
					
					result.Add(new Tuple<ItemReference, Guid>(tuple.Item1, tuple.Item3));
				}
			}

			return result;
		}
	}
}
