﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Logging;
using KswApi.Logic.Framework.Constants;
using KswApi.Logic.Tasks;
using KswApi.Poco;
using KswApi.Poco.Content;
using KswApi.Poco.Tasks;
using KswApi.Poco.ThreeM;
using KswApi.Repositories;
using KswApi.Sql.Repositories.ThreeM;

namespace KswApi.Logic.ContentLogic
{
	public class ThreeMLogic
	{
		public TaskStepResponse Update()
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();

			TaskLogic taskLogic = new TaskLogic();

			ThreeMUpdateTask threeMTask;
			if (!taskLogic.TryAcquireTask(Constant.Tasks.Ids.THREE_M_HDD_UPDATE, out threeMTask))
				return new TaskStepResponse { Continue = false };

			if (threeMTask == null)
			{
				threeMTask = new ThreeMUpdateTask
							 {
								 State = ThreeMUpdateTaskState.Start
							 };

				taskLogic.UpdateTask(Constant.Tasks.Ids.THREE_M_HDD_UPDATE, threeMTask);
			}

			// figure out state
			while (true)
			{
				switch (threeMTask.State)
				{
					case ThreeMUpdateTaskState.None:
						threeMTask.State = ThreeMUpdateTaskState.Start;
						break;
					case ThreeMUpdateTaskState.Start:
						Logger.Log(Severity.Information, "3M HDD Update Started", "Started starting update with 3M HDD Taxonomies.");

						if (!ShouldUpdate())
						{
							taskLogic.FinishTask(Constant.Tasks.Ids.THREE_M_HDD_UPDATE);
							Logger.Log(Severity.Information, "3M HDD Update Finished (no new version)", "The content taxonomies are up to date and do not require any changes.");
							return new TaskStepResponse { Continue = false };
						}
						threeMTask.PreviousItem = 0;
						threeMTask.State = ThreeMUpdateTaskState.AddDocuments;

						break;
					case ThreeMUpdateTaskState.AddDocuments:
						AddDocuments(threeMTask, stopwatch, TimeSpan.FromSeconds(Constant.Tasks.STEP_IN_SECONDS));

						if (threeMTask.State == ThreeMUpdateTaskState.AddDocuments)
						{
							// add documents did not complete
							taskLogic.ReleaseTask(Constant.Tasks.Ids.THREE_M_HDD_UPDATE);
							return new TaskStepResponse { Continue = true };
						}

						break;
					case ThreeMUpdateTaskState.UpdateDocuments:
						UpdateDocuments(threeMTask, stopwatch, TimeSpan.FromSeconds(Constant.Tasks.STEP_IN_SECONDS));

						if (threeMTask.State == ThreeMUpdateTaskState.UpdateDocuments)
						{
							// update documents did not complete
							taskLogic.ReleaseTask(Constant.Tasks.Ids.THREE_M_HDD_UPDATE);
							return new TaskStepResponse { Continue = true };
						}

						break;
					case ThreeMUpdateTaskState.Finished:
						UpdateVersionToFinished();

						taskLogic.FinishTask(Constant.Tasks.Ids.THREE_M_HDD_UPDATE);

						Logger.Log(Severity.Information, "3M HDD Update Finished", "The content taxonomies have been udpated with the most recent 3M HDD changes.");

						return new TaskStepResponse { Continue = false };

					default:
						throw new ArgumentOutOfRangeException();
				}
			}
		}

		public void UpdateContent(ContentVersion version, ContentCore core, ContentBucket bucket)
		{
			if (string.IsNullOrEmpty(version.LegacyId) || bucket.LegacyId == null)
				return;

			Repository repository = new Repository();

			EncodedDocument encodedDocument;
			using (EncodedDocumentRepository documentRepository = new EncodedDocumentRepository())
			{
				encodedDocument = documentRepository.Get(bucket.LegacyId.Value, version.LegacyId);

				if (encodedDocument == null)
					return;
			}

			using (EncodedTaxonomyRepository taxonomyRepository = new EncodedTaxonomyRepository())
			{
				// get the taxonomies
				List<EncodedTaxonomyValue> values = taxonomyRepository.GetByConceptId(encodedDocument.ConceptId).ToList();

				Dictionary<string, List<TaxonomyValue>> newTaxonomies = GetProcessedTaxonomies(values);

				if (core.Taxonomies == null)
					core.Taxonomies = new Dictionary<string, List<TaxonomyValue>>();

				ApplyTaxonomies(newTaxonomies, core.Taxonomies);

				if (newTaxonomies.Count == 0)
					repository.Content.ThreeMItems.Delete(encodedDocument.Id);
				else
					repository.Content.ThreeMItems.Upsert(new ThreeMItem
														  {
															  Id = encodedDocument.ConceptId,
															  LegacyId = encodedDocument.DocumentId
														  });
			}
		}

		private bool ShouldUpdate()
		{
			HddVersion hddVersion;

			using (HddVersionRepository hddRepository = new HddVersionRepository())
			{
				hddVersion = hddRepository.GetCurrent();
			}

			if (hddVersion == null)
				return false;

			Repository repository = new Repository();

			ConfigurationValue<HddVersion> currentVersion = repository.Configuration.ConfigurationValues.Get<HddVersion>(Constant.Configuration.Ids.THREE_M_HDD_CURRENT_VERSION);

			if (currentVersion == null)
			{
				currentVersion = new ConfigurationValue<HddVersion>
				{
					Id = Constant.Configuration.Ids.THREE_M_HDD_CURRENT_VERSION,
					Data = new HddVersion
					{
						Date = DateTime.UtcNow,
						State = TaskState.Active,
						Version = hddVersion.Version
					}
				};

				repository.Configuration.ConfigurationValues.Add(currentVersion);

				return true;
			}


			if (currentVersion.Data.Version < hddVersion.Version || currentVersion.Data.State != TaskState.Finished)
			{
				currentVersion.Data.Version = hddVersion.Version;
				currentVersion.Data.State = TaskState.Active;
				currentVersion.Data.Date = DateTime.UtcNow;
				repository.Configuration.ConfigurationValues.Update(currentVersion);
				return true;
			}

			return false;
		}

		private void UpdateVersionToFinished()
		{
			Repository repository = new Repository();

			ConfigurationValue<HddVersion> currentVersion = repository.Configuration.ConfigurationValues.Get<HddVersion>(Constant.Configuration.Ids.THREE_M_HDD_CURRENT_VERSION);

			if (currentVersion == null)
				return;

			currentVersion.Data.State = TaskState.Finished;
			currentVersion.Data.Date = DateTime.UtcNow;

			repository.Configuration.ConfigurationValues.Update(currentVersion);
		}

		private void AddDocuments(ThreeMUpdateTask threeMTask, Stopwatch stopwatch, TimeSpan timeout)
		{
			Repository repository = new Repository();
			TaskLogic taskLogic = new TaskLogic();

			using (EncodedDocumentRepository encodedDocumentRepository = new EncodedDocumentRepository())
			{
				IEnumerable<EncodedDocument> encodedDocuments = encodedDocumentRepository.Get(threeMTask.PreviousItem);

				foreach (EncodedDocument document in encodedDocuments)
				{
					repository.Content.ThreeMItems.Upsert(new ThreeMItem
																	   {
																		   Id = document.ConceptId,
																		   LegacyId = document.DocumentId
																	   });

					if (stopwatch.Elapsed > timeout)
					{
						threeMTask.PreviousItem = document.ConceptId;
						taskLogic.UpdateTask(Constant.Tasks.Ids.THREE_M_HDD_UPDATE, threeMTask);
						return;
					}
				}
			}

			threeMTask.PreviousItem = 0;
			threeMTask.State = ThreeMUpdateTaskState.UpdateDocuments;

			taskLogic.UpdateTask(Constant.Tasks.Ids.THREE_M_HDD_UPDATE, threeMTask);
		}

		private void UpdateDocuments(ThreeMUpdateTask threeMTask, Stopwatch stopwatch, TimeSpan timeout)
		{
			TaskLogic taskLogic = new TaskLogic();

			Dictionary<string, Guid?> buckets = new Dictionary<string, Guid?>();

			Repository repository = new Repository();

			IEnumerable<ThreeMItem> items = repository.Content.ThreeMItems.Get(threeMTask.PreviousItem);

			using (EncodedTaxonomyRepository taxonomyRepository = new EncodedTaxonomyRepository())
			{
				foreach (ThreeMItem item in items)
				{
					if (stopwatch.Elapsed > timeout)
					{
						threeMTask.PreviousItem = item.Id;
						taskLogic.UpdateTask(Constant.Tasks.Ids.THREE_M_HDD_UPDATE, threeMTask);
						return;
					}

					if (string.IsNullOrEmpty(item.LegacyId))
						continue;

					// get the document
					string[] split = item.LegacyId.Split(',');

					if (split.Length != 2)
						continue;

					// get the taxonomies
					List<EncodedTaxonomyValue> values = taxonomyRepository.GetByConceptId(item.Id).ToList();

					Guid? bucket;

					if (!buckets.TryGetValue(split[0], out bucket))
					{
						int legacyId;
						if (!int.TryParse(split[0], out legacyId))
							continue;

						ContentBucket bucketObject = repository.Content.Buckets.GetByLegacyId(legacyId);
						if (bucketObject == null)
							continue;

						buckets[split[0]] = bucketObject.Id;
					}

					if (bucket == null)
						continue;

					string legacyIdString = split[1];

					Dictionary<string, List<TaxonomyValue>> newTaxonomies = GetProcessedTaxonomies(values);

					DateTime now = DateTime.UtcNow;

					UpdateDraft(bucket.Value, legacyIdString, newTaxonomies, now);

					UpdatePublished(bucket.Value, legacyIdString, newTaxonomies, now);

					if (newTaxonomies.Count == 0)
						repository.Content.ThreeMItems.Delete(item.Id);
				}
			}

			threeMTask.PreviousItem = 0;
			threeMTask.State = ThreeMUpdateTaskState.Finished;

			taskLogic.UpdateTask(Constant.Tasks.Ids.THREE_M_HDD_UPDATE, threeMTask);
		}

		private void UpdateDraft(Guid bucketId, string legacyId, Dictionary<string, List<TaxonomyValue>> newTaxonomies, DateTime now)
		{
			Repository repository = new Repository();

			ContentVersion version = repository.Content.Versions.GetByBucketAndLegacyId(bucketId, legacyId);

			if (version == null || !version.IsMaster)
				return;

			ContentCore core = repository.Content.Cores.GetById(version.CoreId);

			if (core == null)
				return;

			if (core.Taxonomies == null)
				core.Taxonomies = new Dictionary<string, List<TaxonomyValue>>();

			if (Compare(core.Taxonomies, newTaxonomies))
				return;

			ApplyTaxonomies(newTaxonomies, core.Taxonomies);

			repository.Content.Cores.Update(core);

			// mark master version as updated
			version.DateUpdated = now;
			repository.Content.Versions.Update(version);

			// mark dependent versions as updated
			foreach (ContentVersionReference reference in core.Versions)
			{
				if (reference.Id == version.Id)
					continue;

				ContentVersion otherVersion = repository.Content.Versions.GetById(reference.Id);
				if (otherVersion == null)
					continue;

				otherVersion.DateUpdated = now;
				repository.Content.Versions.Update(otherVersion);
			}
		}

		private void UpdatePublished(Guid bucketId, string legacyId, Dictionary<string, List<TaxonomyValue>> newTaxonomies, DateTime now)
		{
			Repository repository = new Repository();

			ContentPublished version = repository.Content.Published.GetByBucketAndLegacyId(bucketId, legacyId);

			if (version == null || !version.IsMaster)
				return;

			ContentCore core = repository.Content.Cores.GetById(version.CoreId);

			if (core == null)
				return;

			if (core.Taxonomies == null)
				core.Taxonomies = new Dictionary<string, List<TaxonomyValue>>();

			if (Compare(core.Taxonomies, newTaxonomies))
				return;

			ApplyTaxonomies(newTaxonomies, core.Taxonomies);

			repository.Content.Cores.Update(core);

			// mark master version as updated
			version.DateUpdated = now;
			repository.Content.Published.Update(version);

			// mark dependent versions as updated
			foreach (ContentVersionReference reference in core.Versions)
			{
				if (reference.Id == version.Id)
					continue;

				ContentPublished otherVersion = repository.Content.Published.GetById(reference.Id);
				if (otherVersion == null)
					continue;

				otherVersion.DateUpdated = now;
				repository.Content.Published.Update(otherVersion);
			}
		}

		private Dictionary<string, List<TaxonomyValue>> GetProcessedTaxonomies(List<EncodedTaxonomyValue> taxonomies)
		{
			Dictionary<string, List<TaxonomyValue>> newTaxonomies = new Dictionary<string, List<TaxonomyValue>>();

			foreach (EncodedTaxonomyValue taxonomy in taxonomies)
			{
				string slug = GetTaxonomySlug(taxonomy.Type);

				if (slug == null)
					continue;

				List<TaxonomyValue> list;

				if (!newTaxonomies.TryGetValue(slug, out list))
				{
					list = new List<TaxonomyValue>();
					newTaxonomies[slug] = list;
				}

				TaxonomyValue value = new TaxonomyValue
				{
					Source = TaxonomyValueSource.ThreeM,
					Value = taxonomy.Value
				};

				list.Add(value);
			}

			return newTaxonomies;
		}

		private bool Compare(Dictionary<string, List<TaxonomyValue>> dictionary1, Dictionary<string, List<TaxonomyValue>> dictionary2)
		{
			if (dictionary1 == null)
				return dictionary2 == null;
			if (dictionary2 == null)
				return false;

			if (dictionary1.Keys.Count != dictionary2.Keys.Count || dictionary1.Count != dictionary2.Count)
				return false;

			foreach (KeyValuePair<string, List<TaxonomyValue>> kvp1 in dictionary1)
			{
				List<TaxonomyValue> list2;
				if (!dictionary2.TryGetValue(kvp1.Key, out list2))
					return false;

				foreach (TaxonomyValue value in kvp1.Value)
				{
					if (!list2.Any(item => item.Source == value.Source && item.Value == value.Value))
						return false;
				}
			}

			return true;
		}

		private void ApplyTaxonomies(Dictionary<string, List<TaxonomyValue>> newTaxonomies, Dictionary<string, List<TaxonomyValue>> existingTaxonomies)
		{
			// remove existing 3m taxonomies
			foreach (KeyValuePair<string, List<TaxonomyValue>> taxonomy in existingTaxonomies)
				taxonomy.Value.RemoveAll(item => item.Source == TaxonomyValueSource.ThreeM);

			// merge taxonomies
			foreach (KeyValuePair<string, List<TaxonomyValue>> taxonomy in newTaxonomies)
			{
				List<TaxonomyValue> list;
				if (existingTaxonomies.TryGetValue(taxonomy.Key, out list))
					list.AddRange(taxonomy.Value);
				else
					existingTaxonomies[taxonomy.Key] = taxonomy.Value;
			}

			// remove any empty taxonomies
			List<KeyValuePair<string, List<TaxonomyValue>>> emptyTaxonomies = existingTaxonomies.Where(item => item.Value.Count == 0).ToList();
			emptyTaxonomies.ForEach(item => existingTaxonomies.Remove(item.Key));
		}

		private string GetTaxonomySlug(long type)
		{
			switch (type)
			{
				case Constant.Taxonomies.ThreeMIds.ICD_9:
					return Constant.Taxonomies.Slugs.ICD_9;
				case Constant.Taxonomies.ThreeMIds.CPT:
					return Constant.Taxonomies.Slugs.CPT;
				case Constant.Taxonomies.ThreeMIds.LOINC:
					return Constant.Taxonomies.Slugs.LOINC;
				case Constant.Taxonomies.ThreeMIds.HCPCS:
					return Constant.Taxonomies.Slugs.HCPCS;
				case Constant.Taxonomies.ThreeMIds.SNOMED:
					return Constant.Taxonomies.Slugs.SNOMED;
				case Constant.Taxonomies.ThreeMIds.RXNORM:
					return Constant.Taxonomies.Slugs.RXNORM;
				case Constant.Taxonomies.ThreeMIds.ICD_10_CM:
					return Constant.Taxonomies.Slugs.ICD_10_CM;
				case Constant.Taxonomies.ThreeMIds.ICD_10_PCS:
					return Constant.Taxonomies.Slugs.ICD_10_PCS;
				default:
					return null;
			}
		}
	}
}
