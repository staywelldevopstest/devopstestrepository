﻿using System.Collections.Generic;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Linq;
using System.Web;
using KswApi.Indexing;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logic.Administration;
using KswApi.Logic.Framework;
using KswApi.Poco.Content;
using KswApi.Repositories;
using System;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.Framework.Constants;
using KswApi.Logic.Framework.Objects;
using KswApi.Logic.Framework.Helpers;
using System.Drawing;
using KswApi.Logic.Framework.Providers;
using KswApi.Common.Configuration;
using KswApi.Utility.Helpers;

namespace KswApi.Logic.ContentLogic
{
	public class ImageLogic
	{
		public ImageDetailResponse Create(string bucketIdOrSlug, StreamRequest imageStream, HttpFileCollectionBase files)
		{
			Stream stream = imageStream;

			if (files != null && files.Count > 0)
			{
				string key = files.AllKeys.First();
				HttpPostedFileBase file = files[key];
				if (file != null)
				{
					stream = file.InputStream;
				}
			}

			if (stream == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Images.EMPTY_MESSAGE);

			BucketLogic bucketLogic = new BucketLogic();

			MemoryStream ms = new MemoryStream();

			stream.CopyTo(ms);

			ContentBucket bucket = bucketLogic.InternalGetBucket(bucketIdOrSlug);

			Image image;
			try
			{
				image = Image.FromStream(ms);
			}
			catch (ArgumentException)
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Images.INVALID_IMAGE);
			}

			ImageFormatType type = ImageTypeProvider.GetType(image.RawFormat);

			if (type == ImageFormatType.None)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Images.INVALID_IMAGE);

			Guid id = Guid.NewGuid();

			DateTime now = DateTime.UtcNow;

			ImageDetail metadata = new ImageDetail
								   {
									   Id = id,
									   BucketId = bucket.Id,
									   Status = ObjectStatus.Uploading,
									   Format = type,
									   DateAdded = now,
									   DateModified = now,
									   Paths = new List<ContentPath>
				                               {
					                               new ContentPath {BucketIdOrSlug = bucket.Slug, ContentIdOrSlug = id.ToString()},
					                               new ContentPath {BucketIdOrSlug = bucket.Id.ToString(), ContentIdOrSlug = id.ToString()}
				                               },
									   Width = image.Width,
									   Height = image.Height
								   };


			ms.Position = 0;

			Repository repository = new Repository();

			repository.Content.ImageStore.Save(metadata.Id, ms, ImageTypeProvider.GetMime(type), now);

			repository.Content.ImageDetails.Add(metadata);

			return GetResponse(metadata, bucket);
		}

		public StreamResponse GetLicenseImage(string licenseId, string bucketIdOrSlug, string imageIdOrSlug, StreamResponse streamResponse)
		{
			if (string.IsNullOrEmpty(licenseId))
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.License.INVALID_LICENSE);

			LicenseLogic licenseLogic = new LicenseLogic();
			License license = licenseLogic.GetLicense(licenseId);

			ServiceModule module = ModuleProvider.GetModule(ModuleType.Content);
			if (license.Modules == null || !license.Modules.Any(item => item == module.Type))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.ContentModule.NO_CONTENT_MODULE);

			Repository repository = new Repository();

			ContentModule contentModule = repository.Content.ContentModule.GetByLicenseId(license.Id);

			if (contentModule == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.ContentModule.NO_ACCESS_TO_BUCKET);

			BucketLogic bucketLogic = new BucketLogic();

			ContentBucket bucket = bucketLogic.InternalGetBucket(bucketIdOrSlug);

			if (contentModule.BucketIds == null || !contentModule.BucketIds.Contains(bucket.Id))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.ContentModule.NO_ACCESS_TO_BUCKET);

			return GetImage(bucketIdOrSlug, imageIdOrSlug, streamResponse);
		}

		public StreamResponse GetImage(string bucketIdOrSlug, string imageIdOrSlug, StreamResponse streamResponse)
		{
			Repository repository = new Repository();

			if (string.IsNullOrEmpty(imageIdOrSlug))
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Images.IMAGE_NOT_FOUND, ExceptionResultType.Image);

			string[] split = imageIdOrSlug.Split('.');

			if (split.Length > 3)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Images.IMAGE_NOT_FOUND);

			ImageDetail detail = InternalGetImage(bucketIdOrSlug, split[0], false);

			if (detail == null)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Images.IMAGE_NOT_FOUND, ExceptionResultType.Image);

			if (split.Length == 1)
			{
				StreamResponse response = repository.Content.ImageStore.Retrieve(detail.Id);
				if (response == null)
					throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Images.IMAGE_FILE_NOT_FOUND);

				response.ContentType = ImageTypeProvider.GetMime(detail.Format);

				return response;
			}

			string extension = split[split.Length - 1];

			ImageFormatType type = ImageTypeProvider.GetTypeFromExtension(extension);

			Image image;
			ImageFormat format;

			if ((type != ImageFormatType.None && split.Length == 2) || detail.Width == 0 || detail.Height == 0)
			{
				streamResponse.ContentType = ImageTypeProvider.GetMime(type);

				if (type == detail.Format)
				{
					streamResponse = repository.Content.ImageStore.Retrieve(detail.Id);

					if (streamResponse == null)
						throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Images.IMAGE_FILE_NOT_FOUND);

					return streamResponse;
				}

				StreamResponse originalImageStream = repository.Content.ImageStore.Retrieve(detail.Id);
				if (originalImageStream == null)
					throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Images.IMAGE_FILE_NOT_FOUND);

				image = Image.FromStream(originalImageStream);

				format = ImageTypeProvider.GetFormat(type);

				streamResponse.ContentType = ImageTypeProvider.GetMime(type);

				image.Save(streamResponse, format);

				return streamResponse;
			}

			if (type == ImageFormatType.None)
				type = detail.Format;

			Size size = GetSize(split[1]);

			if (size.Width <= 0 || size.Height <= 0)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Images.INVALID_DIMENSIONS);

			if (size.Width > Constant.Images.MAXIMUM_WIDTH || size.Height > Constant.Images.MAXIMUM_HEIGHT)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Images.INVALID_DIMENSIONS);

			if (type == detail.Format && size.Width == detail.Width && size.Height == detail.Height)
			{
				StreamResponse savedStreamResponse = repository.Content.ImageStore.Retrieve(detail.Id);
				if (savedStreamResponse == null)
					throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Images.IMAGE_FILE_NOT_FOUND);

				savedStreamResponse.ContentType = ImageTypeProvider.GetMime(type);

				return savedStreamResponse;
			}

			streamResponse.ContentType = ImageTypeProvider.GetMime(type);

			if (detail.CachedImages != null)
			{
				CachedImage cached =
					detail.CachedImages.FirstOrDefault(
						item => item.Type == type && item.Width == size.Width && item.Height == size.Height);

				if (cached != null)
				{
					streamResponse = repository.Content.ImageStore.Retrieve(cached.Id);

					if (streamResponse == null)
						throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Images.IMAGE_NOT_FOUND);

					return streamResponse;
				}
			}

			Size scaledSize = GetScaledSize(detail.Width, detail.Height, size);

			Stream imageStream = repository.Content.ImageStore.Retrieve(detail.Id);
			if (imageStream == null)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Images.IMAGE_NOT_FOUND);

			image = Image.FromStream(imageStream);

			Bitmap bmp = new Bitmap(scaledSize.Width, scaledSize.Height);

			Graphics graphics = Graphics.FromImage(bmp);

			graphics.InterpolationMode = InterpolationMode.HighQualityBicubic;
			graphics.SmoothingMode = SmoothingMode.HighQuality;
			graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;
			graphics.CompositingQuality = CompositingQuality.HighQuality;

			graphics.DrawImage(image, 0, 0, scaledSize.Width, scaledSize.Height);

			format = ImageTypeProvider.GetFormat(type);

			if (detail.CachedImages != null && detail.CachedImages.Count >= Constant.Images.MAXIMUM_CACHED_COUNT)
			{
				bmp.Save(streamResponse, format);
				return streamResponse;
			}

			DuplicatingStream duplicatingStream = new DuplicatingStream(streamResponse);

			bmp.Save(duplicatingStream, format);

			Stream duplicated = duplicatingStream.GetDuplicate();

			CachedImage newCachedImage = new CachedImage
										 {
											 Id = Guid.NewGuid(),
											 Width = size.Width,
											 Height = size.Height,
											 Type = type
										 };

			duplicated.Position = 0;

			repository.Content.ImageStore.Save(newCachedImage.Id, duplicated, streamResponse.ContentType, DateTime.UtcNow);

			if (detail.CachedImages == null)
				detail.CachedImages = new List<CachedImage>();

			detail.CachedImages.Add(newCachedImage);

			repository.Content.ImageDetails.Update(detail);

			return streamResponse;
		}

		public ImageDetail InternalGetImage(string bucketIdOrSlug, string imageIdOrSlug, bool validate = true)
		{
			if (string.IsNullOrEmpty(bucketIdOrSlug) || string.IsNullOrEmpty(imageIdOrSlug))
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.CONTENT_NOT_FOUND);

			bucketIdOrSlug = bucketIdOrSlug.ToLower();
			imageIdOrSlug = imageIdOrSlug.ToLower();

			Repository repository = new Repository();

			ImageDetail detail = repository.Content.ImageDetails.GetByPath(bucketIdOrSlug, imageIdOrSlug);

			if (detail == null && validate)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Images.IMAGE_NOT_FOUND);

			return detail;
		}

		public ContentResponse DeleteImage(ImageDetail detail)
		{
			Repository repository = new Repository();

			detail.Status = ObjectStatus.Deleted;

			repository.Content.ImageDetails.Update(detail);

			ImageIndexer indexer = new ImageIndexer();

			indexer.Delete(detail.Id);

			ContentBucket bucket = repository.Content.Buckets.GetById(detail.BucketId);

			ContentOrigin origin = bucket != null ? repository.Content.Origins.GetById(bucket.OriginId) : null;

			return GetContentResponse(detail, bucket, origin);
		}

		public ImageDetailResponse DeleteImage(string bucketIdOrSlug, string imageIdOrSlug)
		{
			ImageDetail detail = InternalGetImage(bucketIdOrSlug, imageIdOrSlug);

			Repository repository = new Repository();

			detail.Status = ObjectStatus.Deleted;

			repository.Content.ImageDetails.Update(detail);

			ImageIndexer indexer = new ImageIndexer();

			indexer.Delete(detail.Id);

			ContentBucket bucket = repository.Content.Buckets.GetById(detail.BucketId);

			ContentOrigin origin = bucket != null ? repository.Content.Origins.GetById(bucket.OriginId) : null;

			return GetResponse(detail, bucket, origin);
		}

		public ImageDetailResponse UpdateDetails(string bucketIdOrSlug, string imageIdOrSlug, ImageDetailRequest request)
		{
			if (request == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.MISSING_MESSAGE_BODY);

			if (string.IsNullOrEmpty(request.Title))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Images.TITLE_IS_REQUIRED);

		    request.Title = request.Title.Trim();

			ImageDetail detail = InternalGetImage(bucketIdOrSlug, imageIdOrSlug);

			switch (detail.Status)
			{
				case ObjectStatus.Uploading:
					return AddNewImageDetails(detail, request);
				case ObjectStatus.Active:
					return UpdateImageDetails(detail, request);
				default:
					throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Images.IMAGE_NOT_FOUND);
			}
		}

		public ImageDetailResponse GetImageDetails(string bucketIdOrSlug, string imageIdOrSlug)
		{
			ImageDetail detail = InternalGetImage(bucketIdOrSlug, imageIdOrSlug);

			return GetResponse(detail);
		}


		private ImageDetailResponse AddNewImageDetails(ImageDetail detail, ImageDetailRequest request)
		{
            // TODO: fix inconsistent validation

			if (string.IsNullOrEmpty(request.Slug))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.SLUG_REQUIRED);

			request.Slug = request.Slug.ToLower();

			ValidationHelper.ValidateSlug(request.Slug);

			// make sure slug isn't already used
			Repository repository = new Repository();

			ContentBucket bucket = repository.Content.Buckets.GetById(detail.BucketId);

			if (bucket == null)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.BUCKET_NOT_FOUND);

			ImageDetail existing = repository.Content.ImageDetails.GetByPath(bucket.Id.ToString(), request.Slug);

			if (existing != null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Content.DUPLICATE_SLUG);

			detail.Slug = request.Slug;
			detail.Status = ObjectStatus.Active;
			detail.Blurb = request.Blurb;
			detail.Title = request.Title;
			detail.InvertedTitle = request.InvertedTitle;
			detail.AlternateTitles = request.AlternateTitles;
			detail.LegacyId = request.LegacyId;
			detail.DateModified = DateTime.UtcNow;
			detail.PostingDate = request.PostingDate;
			detail.Tags = request.Tags;

			detail.Paths = new List<ContentPath>
			               {
				               new ContentPath {BucketIdOrSlug = bucket.Slug, ContentIdOrSlug = detail.Slug},
				               new ContentPath {BucketIdOrSlug = bucket.Slug, ContentIdOrSlug = detail.Id.ToString()},
				               new ContentPath {BucketIdOrSlug = bucket.Id.ToString(), ContentIdOrSlug = detail.Slug},
				               new ContentPath {BucketIdOrSlug = bucket.Id.ToString(), ContentIdOrSlug = detail.Id.ToString()}
			               };

			repository.Content.ImageDetails.Update(detail);

			ImageIndexer indexer = new ImageIndexer();

			indexer.Add(detail);

			return GetResponse(detail, bucket);
		}

		private ImageDetailResponse UpdateImageDetails(ImageDetail detail, ImageDetailRequest request)
		{
			if (!string.IsNullOrEmpty(request.Slug) && request.Slug != detail.Slug)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.MODIFYING_SLUG);

			Repository repository = new Repository();

			if (request.Replacement != null)
			{
				// replacing the image
				ImageDetail replacement = repository.Content.ImageDetails.GetById(request.Replacement.Value);

				if (replacement == null)
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Images.INVALID_REPLACEMENT);

				if (replacement.Status != ObjectStatus.Uploading)
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Images.REPLACEMENT_REUSE);

				if (detail.CachedImages != null)
				{
					foreach (CachedImage cachedImage in detail.CachedImages)
					{
						repository.Content.ImageStore.Delete(cachedImage.Id);
					}

					detail.CachedImages = null;
				}

				repository.Content.ImageStore.Delete(detail.Id);

				repository.Content.ImageStore.Rename(replacement.Id, detail.Id);

				repository.Content.ImageDetails.DeleteById(replacement.Id);
			}

			detail.Status = ObjectStatus.Active;
			detail.Blurb = request.Blurb;
			detail.Title = request.Title;
			detail.InvertedTitle = request.InvertedTitle;
			detail.AlternateTitles = request.AlternateTitles;
			detail.LegacyId = request.LegacyId;
			detail.DateModified = DateTime.UtcNow;
			detail.PostingDate = request.PostingDate;

			detail.Tags = request.Tags;

			repository.Content.ImageDetails.Update(detail);

			ImageIndexer indexer = new ImageIndexer();

			indexer.Update(detail);

			return GetResponse(detail);
		}

		public SlugResponse GetImageSlug(string bucketIdOrSlug, string idOrSlug, string slug, string title)
		{
			if (string.IsNullOrEmpty(idOrSlug) || string.IsNullOrEmpty(slug))
				return GetNewSlug(bucketIdOrSlug, slug, title);

			ImageDetail detail = InternalGetImage(bucketIdOrSlug, idOrSlug);

			if (slug == detail.Slug)
				return new SlugResponse { Value = slug };

			return GetNewSlug(bucketIdOrSlug, slug, title);
		}

		private ImageDetailResponse GetResponse(ImageDetail detail, ContentBucket bucket = null, ContentOrigin origin = null)
		{
			ImageDetailResponse response = new ImageDetailResponse
			{
				Id = detail.Id,
				Blurb = detail.Blurb,
				Slug = detail.Slug,
				Tags = detail.Tags,
				Title = detail.Title,
				InvertedTitle = detail.InvertedTitle,
				AlternateTitles = detail.AlternateTitles,
				Width = detail.Width,
				Format = detail.Format,
				Height = detail.Height,
				DateAdded = detail.DateAdded,
				DateModified = detail.DateModified,
				PostingDate = detail.PostingDate,
				Uri = Settings.Current.WebPortalBaseUri + "/Service/Get/Content/" + detail.Paths[0].BucketIdOrSlug + "/Images/" + detail.Paths[0].ContentIdOrSlug,
				LegacyId = detail.LegacyId
			};

			Repository repository = null;

			if (bucket == null)
			{
				repository = new Repository();

				bucket = repository.Content.Buckets.GetById(detail.BucketId);

				if (bucket == null)
					throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.BUCKET_NOT_FOUND);
			}

			if (origin == null)
			{
				if (repository == null)
					repository = new Repository();

				origin = repository.Content.Origins.GetById(bucket.OriginId);

				if (origin == null)
					throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.ORPHANED_BUCKET_NO_ORIGIN);
			}

			response.Bucket = new ContentBucketReference
			{
				Id = bucket.Id,
				Name = bucket.Name,
				Slug = bucket.Slug
			};

			return response;
		}

		public ContentResponse GetContentResponse(ImageDetail detail, ContentBucket bucket, ContentOrigin origin)
		{
			ContentResponse response = new ContentResponse
			{
				Id = detail.Id,
				Slug = detail.Slug,
				Title = detail.Title,
				InvertedTitle = detail.InvertedTitle,
				AlternateTitles = detail.AlternateTitles,
				Blurb = detail.Blurb,
				DateAdded = detail.DateAdded,
				DateModified = detail.DateModified,
				PostingDate = detail.PostingDate,
				Type = ContentType.Image,
				Format = detail.Format,
				Bucket = new ContentBucketReference
				{
					Id = detail.BucketId
				},
				Uri = Settings.Current.WebPortalBaseUri + "/Service/Get/Content/" + bucket.Slug + "/Images/" + detail.Slug,
				LegacyId = detail.LegacyId,
				Width = detail.Width,
				Height = detail.Height
			};

			response.Bucket.Id = bucket.Id;
			response.Bucket.Name = bucket.Name;
			response.Bucket.Slug = bucket.Slug;

			if (origin != null)
			{
				response.OriginName = origin.Name;
			}

			return response;
		}


		public void Reindex()
		{
			ImageIndexer indexer = new ImageIndexer();

			Repository repository = new Repository();

			IEnumerable<ImageDetail> images = repository.Content.ImageDetails.GetAll();

			foreach (ImageDetail image in images)
			{
				if (image.Status != ObjectStatus.Active)
					continue;

				indexer.Add(image);
			}
		}

		private Size GetScaledSize(int originalWidth, int originalHeight, Size size)
		{
			double ratioX = (double)size.Width / originalWidth;
			double ratioY = (double)size.Height / originalHeight;

			double ratio = ratioX < ratioY ? ratioX : ratioY;

			return new Size((int)(originalWidth * ratio), (int)(originalHeight * ratio));
		}

		private Size GetSize(string size)
		{
			switch (size.ToLower())
			{
				case "thumb":
				case "thumbnail":
					return new Size(50, 50);
				case "sm":
				case "small":
					return new Size(150, 150);
				case "med":
				case "medium":
					return new Size(300, 300);
				case "lg":
				case "large":
					return new Size(600, 600);
			}

			string[] split = size.Split('x');
			if (split.Length != 2)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Images.IMAGE_NOT_FOUND);

			int width, height;

			if (int.TryParse(split[0], out width) && int.TryParse(split[1], out height))
				return new Size(width, height);

			throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Images.IMAGE_NOT_FOUND);
		}

		private SlugResponse GetNewSlug(string bucketIdOrSlug, string slug, string title)
		{
			if (string.IsNullOrEmpty(bucketIdOrSlug))
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Content.BUCKET_REQUIRED);

			if (string.IsNullOrEmpty(slug) && string.IsNullOrWhiteSpace(title))
                return new SlugResponse { Value = string.Empty };

            if (string.IsNullOrEmpty(slug))
                slug = SlugHelper.CreateSlug(title.Trim());
            else
            {
                slug = slug.ToLower();
                ValidationHelper.ValidateSlug(slug);
            }

			bucketIdOrSlug = bucketIdOrSlug.ToLower();

			Repository repository = new Repository();

			ImageDetail detail = repository.Content.ImageDetails.GetByPath(bucketIdOrSlug, slug);

			if (detail == null)
				return new SlugResponse { Value = slug };

			// find next available slug
			int min = 1, current = min, max = min;

			while (true)
			{
				string candidate = slug + '-' + current;
				// logarithmic search
				detail = repository.Content.ImageDetails.GetByPath(bucketIdOrSlug, candidate);
				if (detail == null)
				{
					if (min >= current)
						return new SlugResponse { Value = candidate };

					max = current;
					current = (max + min) / 2;
				}
				else
				{
					min = current + 1;

					if (current >= max)
						current = current * 2;
					else
						current = (min + max) / 2;
				}
			}
		}
	}
}
