﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.Framework.Constants;
using KswApi.Poco.Content;
using KswApi.Repositories;

namespace KswApi.Logic.ContentLogic
{
    public class TaxonomyTypeLogic
    {
        #region Fields

        // TODO: Might be nice to drive this outside of class logic
        private List<TaxonomyType> _taxonomyTypesToEnsure = new List<TaxonomyType>
                                                        {
                                                            new TaxonomyType{Name = "ICD-9", Slug = Constant.Taxonomies.Slugs.ICD_9, Regex=@"^(V\d{2}(\.\d{1,2})?|\d{3}(\.\d{1,2})?|E\d{3}(\.\d)?)$"},
                                                            new TaxonomyType{Name = "CPT", Slug = Constant.Taxonomies.Slugs.CPT, Regex=@"^\d{4,4}[A-Z0-9]$"},
                                                            new TaxonomyType{Name = "HCPCS", Slug = Constant.Taxonomies.Slugs.HCPCS, Regex=@"^[A-Z]\d{4}$"},
                                                            new TaxonomyType{Name = "ICD-10 CM", Slug = Constant.Taxonomies.Slugs.ICD_10_CM, Regex=@"^[A-Z]\d[A-Z0-9](\.[A-Z0-9]{1,4})?$"},
                                                            new TaxonomyType{Name = "ICD-10 PCS", Slug = Constant.Taxonomies.Slugs.ICD_10_PCS, Regex=@"^[A-HJ-NP-Z0-9]{7}$"},
                                                            new TaxonomyType{Name = "LOINC", Slug = Constant.Taxonomies.Slugs.LOINC, Regex=@"^[0-9]+-[0-9]$"},
                                                            new TaxonomyType{Name = "MeSH", Slug = Constant.Taxonomies.Slugs.MESH, Regex=@"^([A-NVZ]\d{2}(\.\d{3})*)|([A-NVZ]\d{6})$"},
                                                            new TaxonomyType{Name = "NDC", Slug = Constant.Taxonomies.Slugs.NDC, Regex=@"^\d{4}-\d{4}-\d{2}$|^\d{5}-\d{3}-\d{2}$|^\d{5}-\d{4}-\d{1}$"},
                                                            new TaxonomyType{Name = "RXNORM", Slug = Constant.Taxonomies.Slugs.RXNORM, Regex=@"^.+$"},
                                                            new TaxonomyType{Name = "SnoMed", Slug = Constant.Taxonomies.Slugs.SNOMED, Regex=@"^\d{6,18}$"},
                                                        };
        #endregion

        #region Public Methods

        public void EnsureTaxonomyTypes()
        {
            var repository = new Repository();
            repository.Content.TaxonomyTypes.Clear();

            foreach (TaxonomyType type in this._taxonomyTypesToEnsure)
            {
                this.EnsureTaxonomyType(type);
            }
        }

        public TaxonomyTypeResponse GetTaxonomyType(string slug)
        {
            if (string.IsNullOrEmpty(slug))
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.SLUG_REQUIRED);
            
            Repository repository = new Repository();
            TaxonomyType taxonomyTypeBySlug = repository.Content.TaxonomyTypes.GetTaxonomyTypeBySlug(slug);
            
            if (taxonomyTypeBySlug == null)
                throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.TaxonomyType.TAXONOMY_TYPE_NOT_FOUND, ExceptionResultType.Object);

            return new TaxonomyTypeResponse {Name = taxonomyTypeBySlug.Name, Slug = taxonomyTypeBySlug.Slug};
        }

        public TaxonomyTypeList SearchTaxonomyTypes()
        {
            Repository repository = new Repository();

            return new TaxonomyTypeList
					   {
                           Items = new List<TaxonomyTypeResponse>(repository.Content.TaxonomyTypes.GetTaxonomyTypes().Select(ResponseFromTaxonomyType))
					   };
        }

        #endregion

        #region Private Methods

        private TaxonomyTypeResponse ResponseFromTaxonomyType(TaxonomyType taxonomyType)
        {
            return new TaxonomyTypeResponse
            {
                Name = taxonomyType.Name,
                Slug = taxonomyType.Slug
            };
        }

        private void EnsureTaxonomyType(TaxonomyType taxonomyType)
        {
            Repository repository = new Repository();

            if (!repository.Content.TaxonomyTypes.GetTaxonomyTypes().Any(x => x.Slug == taxonomyType.Slug))
            {
                repository.Content.TaxonomyTypes.Add(taxonomyType);
            }
        }

        #endregion

    }
}
