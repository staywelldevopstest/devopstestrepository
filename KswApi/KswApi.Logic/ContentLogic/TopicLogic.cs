﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using KswApi.Common.Objects;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.Framework.Constants;
using KswApi.Poco.Content;

namespace KswApi.Logic.ContentLogic
{
	public class TopicLogic : LogicBase
	{
		#region Public Methods

		public TopicResponse GetTopic(string idOrSlug, bool? includeChildren, bool? recursive, AccessToken token)
		{
			if (string.IsNullOrEmpty(idOrSlug))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.SLUG_OR_ID_REQUIRED);

			Guid guid;

			Topic topic = Guid.TryParse(idOrSlug, out guid) ? Repository.Content.Topics.GetById(guid) : Repository.Content.Topics.GetBySlug(idOrSlug.ToLower());

			if (topic == null)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Collection.TOPIC_NOT_FOUND);

			ValidateCollectionAccess(token, topic);

			TopicResponse response = ResponseFromTopic(topic);

			if ((includeChildren.HasValue && includeChildren.Value) || (recursive.HasValue && recursive.Value))
				response.Items = GetTopicItems(topic, recursive.HasValue && recursive.Value);

			return response;
		}

		// used internally by get collection calls
		public List<CollectionItemResponse> GetTopicItems(Topic topic, bool recursive)
		{
			List<CollectionStructure> structures = recursive
				? Repository.Content.CollectionStructures.GetByPathElement(topic.Id).ToList()
				: Repository.Content.CollectionStructures.GetWithChildrenById(topic.Id).ToList();

			Dictionary<Guid, CollectionStructure> structureLookup = structures.ToDictionary(item => item.Id);

			CollectionStructure current;
			if (!structureLookup.TryGetValue(topic.Id, out current))
				return null;

			List<Guid> topicIds = structures.Where(item => item.Type == CollectionItemType.Topic && item.Id != current.Id).Select(item => item.ItemId).ToList();

			List<Topic> topics = Repository.Content.Topics.GetByIds(topicIds).ToList();

			return GetTopicItems(topic, structureLookup, topics, recursive);
		}

		#region Internal Methods

		internal List<CollectionItemResponse> GetTopicItems(Topic topic, List<CollectionStructure> structures, List<Topic> topics, bool recursive)
		{
			Dictionary<Guid, CollectionStructure> structureLookup = structures.ToDictionary(item => item.Id);

			return GetTopicItems(topic, structureLookup, topics, recursive);
		}

		#endregion

		#endregion

		#region Private Methods

		private List<CollectionItemResponse> GetTopicItems(Topic topic, Dictionary<Guid, CollectionStructure> structures, List<Topic> topics, bool recursive)
		{
			CollectionStructure current;
			if (!structures.TryGetValue(topic.Id, out current))
				return null;

			Dictionary<Guid, Topic> topicLookup = topics.ToDictionary(item => item.Id);

			List<Guid> contentIds;

			if (recursive)
			{
				contentIds = structures.Values.Where(item => item.Type == CollectionItemType.Content).Select(item => item.ItemId).Distinct().ToList();
			}
			else
			{
				contentIds = current.Children != null
							   ? structures.Values.Where(item => item.Type == CollectionItemType.Content).Select(item => item.ItemId).Distinct().ToList()
							   : new List<Guid>();
			}

			if (contentIds.Count == 0)
			{
				return GetTopicItems(current, structures, topicLookup, new Dictionary<Guid, ContentPublished>(), new Dictionary<Guid, ContentCore>(), new Dictionary<Guid, ContentBucket>(), recursive);
			}

			List<ContentPublished> published = Repository.Content.Published.GetByIds(contentIds).ToList();
			Dictionary<Guid, ContentPublished> publishedDictionary = published.ToDictionary(item => item.Id);
			Dictionary<Guid, ContentCore> contentCores = Repository.Content.Cores.GetByIds(published.Select(version => version.CoreId).Distinct()).ToDictionary(item => item.Id);
			Dictionary<Guid, ContentBucket> buckets = Repository.Content.Buckets.GetByIds(published.Select(version => version.BucketId).Distinct()).ToDictionary(item => item.Id);

			return GetTopicItems(current, structures, topicLookup, publishedDictionary, contentCores, buckets, recursive);
		}

		private void ValidateCollectionAccess(AccessToken token, Topic topic)
		{

		}

		private List<CollectionItemResponse> GetTopicItems(CollectionStructure current, Dictionary<Guid, CollectionStructure> structures, Dictionary<Guid, Topic> topics, Dictionary<Guid, ContentPublished> publisheds, Dictionary<Guid, ContentCore> contentCores, Dictionary<Guid, ContentBucket> buckets, bool recursive)
		{
			if (current.Children == null || current.Children.Count == 0)
				return null;

			List<CollectionItemResponse> response = new List<CollectionItemResponse>(current.Children.Count);

			foreach (Guid childId in current.Children)
			{
				CollectionStructure child;
				if (!structures.TryGetValue(childId, out child))
					continue;

				switch (child.Type)
				{
					case CollectionItemType.Topic:
						Topic topic = topics[child.ItemId];

						CollectionStructure structure;

						response.Add(new CollectionItemResponse
						{
                            Id = topic.Id,
                            Slug = topic.Slug,
							Title = topic.Title,
							Description = topic.Description,
							Type = CollectionItemType.Topic,
							ImageUri = topic.ImageUri,
							Flagged = child.Flagged,
							FlagComment = child.FlagComment,
							Items = recursive && structures.TryGetValue(topic.Id, out structure) && structure.Children != null
								? GetTopicItems(child, structures, topics, publisheds, contentCores, buckets, true)
								: null
						});
						break;
					case CollectionItemType.Content:
						ContentPublished published = publisheds[child.ItemId];
						ContentCore core = contentCores[published.CoreId];
						ContentBucket bucket = buckets[published.BucketId];
						response.Add(GetContentItemResponse(child, published, core, bucket));
						break;
				}
			}

			return response;
		}

		private CollectionItemResponse GetContentItemResponse(CollectionStructure structure, ContentPublished version, ContentCore core, ContentBucket bucket)
		{
			ContentLogic contentLogic = new ContentLogic();
			return new CollectionItemResponse
					   {
						   Type = CollectionItemType.Content,
                           Id = version.Id,
                           Slug = version.Slug,
                           Bucket = new ContentBucketReference{Id = bucket.Id, Slug = bucket.Slug, Name = bucket.Name},
						   Title = version.Title,
						   Description = version.Blurb,
						   Disabled = structure.Disabled,
						   Flagged = structure.Flagged,
						   FlagComment = structure.FlagComment,
						   ServiceLines = contentLogic.GetServicelinesResponseFromCore(core),
						   Items = null
					   };
		}

		private static TopicResponse ResponseFromTopic(Topic topic)
		{
			return new TopicResponse
					   {
						   Id = topic.Id,
						   Slug = topic.Slug,
						   Title = topic.Title,
						   DateAdded = topic.DateAdded,
						   DateModified = topic.DateAdded,
						   Description = topic.Description
					   };
		}

		#endregion
	}
}
