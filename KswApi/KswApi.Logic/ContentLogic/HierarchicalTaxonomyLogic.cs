﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.Framework.Constants;
using KswApi.Poco;
using KswApi.Poco.Content;
using KswApi.Repositories;
using KswApi.Utility.Helpers;
using Microsoft.VisualBasic.FileIO;
using KswApi.Logging;

namespace KswApi.Logic.ContentLogic
{
    public class HierarchicalTaxonomyLogic
    {
        #region Public Methods

        public void EnsureHierarchicalTaxonomies()
        {
            Repository repository = new Repository();

            // process reference files
            var referenceFiles = repository.Configuration.ConfigurationValues.Get<List<ReferenceFile>>(Constant.Configuration.Ids.HIERARCHICAL_TAXONOMY_REFERENCE_FILES);
            
            foreach (ReferenceFile file in referenceFiles.Data)
            {
                string serverFilepath = HttpContext.Current.Server.MapPath(file.Filepath);
                if (!File.Exists(serverFilepath)) continue;

                DateTime lastWriteUtc = File.GetLastWriteTimeUtc(serverFilepath).ToUniversalTime();
                
                if (lastWriteUtc == file.FileModifiedDate.ToUniversalTime()) continue;

                string key = typeof(HierarchicalTaxonomyType).Name;
                if (!file.Properties.ContainsKey(key)) continue;

                HierarchicalTaxonomyType type = (HierarchicalTaxonomyType) file.Properties[key];

                // clear db
                repository.Content.HierarchicalTaxonomy.Clear(type);

                // load file
                var result = ParseFile(serverFilepath, type);
                if (!result) continue;

                file.LoadDate = DateTime.UtcNow;
                file.FileModifiedDate = lastWriteUtc;
            }
            repository.Configuration.ConfigurationValues.Update(referenceFiles);
        }

        public static string GetFullPath(HierarchicalTaxonomyRequest hierarchicalTaxonomy)
        {
            return GetFullPath(hierarchicalTaxonomy.Type, hierarchicalTaxonomy.Path, hierarchicalTaxonomy.Slug);
        }

        public static string GetFullPath(HierarchicalTaxonomyType type, string path, string slug)
        {
            return type.ToString().ToLower()
                   + (string.IsNullOrEmpty(path) ? "" : "/" + path.ToLower())
                   + (string.IsNullOrEmpty(slug) ? "" : "/" + slug.ToLower());
        }

        private bool ParseFile(string filepath, HierarchicalTaxonomyType type)
        {
            try
            {
                Repository repository = new Repository();

                // Parse file
                var rows = new List<string[]>();
                using (TextFieldParser parser = new TextFieldParser(filepath)
                                                    {
                                                        Delimiters = new []{ "," },
                                                        TextFieldType = FieldType.Delimited,
                                                        HasFieldsEnclosedInQuotes = true,

                                                    })
                {
                    var header = parser.ReadFields();

                    while (!parser.EndOfData)
                    {
                        rows.Add(parser.ReadFields());
                    }
                    parser.Dispose();
                }

                var hierarchicalTaxonomiesToEnsure = GetTaxonomyFromRows(rows, type);

                foreach (HierarchicalTaxonomy taxonomy in hierarchicalTaxonomiesToEnsure)
                {
                    repository.Content.HierarchicalTaxonomy.Add(taxonomy);
                }
                return true;
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                return false;
            }
        }

        private IEnumerable<HierarchicalTaxonomy> GetTaxonomyFromRows(List<string[]> rows, HierarchicalTaxonomyType type)
        {
            int maxDepth = rows.Max(x => x.Length);
            var items = GetTaxonomyFromRows(rows, 0, maxDepth, type);
            return items;
        }

        private IEnumerable<HierarchicalTaxonomy> GetTaxonomyFromRows(List<string[]> rows, int currentDepth, int maxDepth, HierarchicalTaxonomyType type)
        {
            var items = rows.Select(row => new
                                               {
                                                   Value = row[currentDepth],
                                                   PathValues = string.Join("/", row.Take(currentDepth + 1)),
                                                   Path = string.Join("/", row.Take(currentDepth).Select(SlugHelper.CreateSlug)),
                                               })
                .Distinct()
                .Select(x => new HierarchicalTaxonomy
                                 {
                                     Slug = SlugHelper.CreateSlug(x.Value),
                                     Path = x.Path.ToLower(),
                                     FullPath = GetFullPath(type, x.Path, SlugHelper.CreateSlug(x.Value)),
                                     Value = x.Value,
                                     PathValues = x.PathValues.Split('/').ToList(),
                                     Type = type
                                 }).ToList();

            if (++currentDepth < maxDepth)
                return items.Union(GetTaxonomyFromRows(rows, currentDepth, maxDepth, type));

            return items;
        }

        public HierarchicalTaxonomyResponse GetHierarchicalTaxonomy(HierarchicalTaxonomyType type, string slug, string path)
        {
            if (string.IsNullOrEmpty(slug))
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.SLUG_REQUIRED);

            Repository repository = new Repository();
            HierarchicalTaxonomy hierarchicalTaxonomy = repository.Content.HierarchicalTaxonomy.GetHierarchicalTaxonomyBySlugAndPath(type, slug, path ?? string.Empty);

            if (hierarchicalTaxonomy == null)
                throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.HierarchicalTaxonomy.NOT_FOUND);

            return ResponseFromHierarchicalTaxonomy(hierarchicalTaxonomy);
        }

        public HierarchicalTaxonomyList GetHierarchicalTaxonomiesByPath(HierarchicalTaxonomyType type, string path)
        {
            Repository repository = new Repository();

            return new HierarchicalTaxonomyList
                       {
                           Items =
                               new List<HierarchicalTaxonomyResponse>(repository.Content.HierarchicalTaxonomy.GetHierarchicalTaxonomiesByPath(type, path ?? string.Empty)
                                   .Select(ResponseFromHierarchicalTaxonomy))
                       };
        }

        #endregion

        #region Private Methods

        private HierarchicalTaxonomyResponse ResponseFromHierarchicalTaxonomy(HierarchicalTaxonomy hierarchicalTaxonomy)
        {
            return new HierarchicalTaxonomyResponse
            {
                Path = hierarchicalTaxonomy.Path,
                Slug = hierarchicalTaxonomy.Slug,
                Value = hierarchicalTaxonomy.Value,
                PathValues = hierarchicalTaxonomy.PathValues,
                Type = hierarchicalTaxonomy.Type
            };
        }

        #endregion

    }
}
