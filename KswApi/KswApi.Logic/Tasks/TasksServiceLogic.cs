﻿using KswApi.Common.Objects;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.ContentLogic;
using KswApi.Logic.Framework;
using KswApi.Logic.Framework.Constants;
using KswApi.Poco.Administration;
using KswApi.Poco.Content;
using KswApi.Poco.Sms;
using KswApi.Poco.Tasks;
using KswApi.Repositories;
using KswApi.Repositories.Enums;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;
using System.Net;

namespace KswApi.Logic.Tasks
{
	public class TasksServiceLogic
	{
		public TaskServiceSettings GetServiceSettings()
		{
			TaskServiceSettings settings = new TaskServiceSettings
			{
				NumberOfThreads = Constant.MAX_NUM_TASK_THREADS,
				HeartBeatInMinutes = Constant.HEART_BEAT_IN_MINUTES,
				Tasks = TaskProvider.GetTasks()
			};

			return settings;
		}

		public ServiceCheck ServiceCheck()
		{
			return new ServiceCheck { IsServiceUp = true };
		}

		public Task GetTaskSettings(TaskType taskType)
		{
			return TaskProvider.GetTask(taskType);
		}

		public TaskStepResponse DoTaskStep(TaskType type)
		{
			switch (type)
			{
				case TaskType.Test:
					return Test();
				case TaskType.SmsSessionCleanup:
					return DeleteSmsSessions();
				case TaskType.AuthorizationGrantCleanup:
					return DeleteAuthorizationGrants();
				case TaskType.AccessTokenCleanup:
					return DeleteAccessTokens();
				case TaskType.ContentAutosaveCleanup:
					return DeleteAutosaveHistory();
				case TaskType.ThreeMHddUpdate:
					return ThreeMHddUpdate();
				default:
					throw new ServiceException(HttpStatusCode.NotFound, "Task type was not found.");
			}
		}

		public void StartedTask(Task task)
		{
			if (task == null)
				throw new ServiceException(HttpStatusCode.BadRequest, "Invalid object");

			TaskStatusData statusData = new TaskStatusData
				{
					TaskName = task.Type.ToString(),
					TaskType = task.Type,
					StatusDate = DateTime.UtcNow,
					StatusUpdateText = "Started task: " + task.Type.ToString()
				};

			Repository repository = new Repository();

			repository.Tasks.TaskStatus.Add(statusData);
		}

		public void EndedTask(Task task)
		{
			if (task == null)
				throw new ServiceException(HttpStatusCode.BadRequest, "Invalid object");

			TaskStatusData statusData = new TaskStatusData
			{
				TaskName = task.Type.ToString(),
				TaskType = task.Type,
				StatusDate = DateTime.UtcNow,
				StatusUpdateText = "Ended task: " + task.Type.ToString()
			};

			Repository repository = new Repository();

			repository.Tasks.TaskStatus.Add(statusData);
		}

		public void LogTaskException(Task task, Exception ex)
		{
			if (task == null || ex == null)
				throw new ServiceException(HttpStatusCode.BadRequest, "Invalid object");

			TaskStatusData statusData = new TaskStatusData
			{
				TaskName = task.Type.ToString(),
				TaskType = task.Type,
				StatusDate = DateTime.UtcNow,
				StatusUpdateText = "Exception occured while running task: " + task.Type.ToString() + ".  Exception Data: " + ex.Message
			};

			Repository repository = new Repository();

			repository.Tasks.TaskStatus.Add(statusData);
		}

		#region Private Methods

		private TaskStepResponse Test()
		{
			return new TaskStepResponse
			{
				Continue = false
			};
		}

		private TaskStepResponse ThreeMHddUpdate()
		{
			ThreeMLogic logic = new ThreeMLogic();

			return logic.Update();
		}

		private TaskStepResponse DeleteSmsSessions()
		{
			Repository repository = new Repository();

			IEnumerable<SmsSession> sessions = repository.SmsGateway.Sessions.GetByLastRequest(DateTime.UtcNow.AddMonths(-Constant.Tasks.SMS_SESSION_EXPIRATION_IN_MONTHS), Constant.Tasks.SMS_SESSION_CLEAN_STEP_SIZE);

			int count = 0;

			foreach (SmsSession session in sessions)
			{
				SmsSubscriber subscriber = repository.SmsGateway.Subscribers.GetBySessionId(session.Id);

				if (subscriber != null)
				{
					subscriber.Sessions.RemoveAll(item => item.SessionId == session.Id);
					if (subscriber.Sessions.Count == 0)
						repository.SmsGateway.Subscribers.Delete(subscriber);
					else
						repository.SmsGateway.Subscribers.Update(subscriber);
				}

				session.State = SmsSessionState.Inactive;
				repository.SmsGateway.Sessions.Update(session);

				count++;
			}

			return new TaskStepResponse
			{
				Continue = count >= Constant.Tasks.SMS_SESSION_CLEAN_STEP_SIZE
			};
		}

		private TaskStepResponse DeleteAccessTokens()
		{
			Repository repository = new Repository();

			repository.Authorization.AccessTokens.DeleteByExpiration(DateTime.UtcNow);
			repository.Authorization.RefreshTokens.DeleteByExpiration(DateTime.UtcNow);

			return new TaskStepResponse
			{
				Continue = false
			};
		}

		private TaskStepResponse DeleteAutosaveHistory()
		{
			Repository repository = new Repository();

			IEnumerable<ContentHistory> histories = repository.Content.History.GetByTimeAndType(DateTime.UtcNow - TimeSpan.FromDays(Constant.Content.AUTOSAVE_EXPIRATION_IN_DAYS), ContentRevisionType.Autosave, Constant.Tasks.AUTOSAVE_CLEAN_STEP_SIZE);

			int count = 0;

			foreach (ContentHistory history in histories)
			{
				repository.Content.Revisions.DeleteById(history.RevisionId);
				repository.Content.History.DeleteById(history.Id);
				count++;
			}

			return new TaskStepResponse
			{
				Continue = count >= Constant.Tasks.AUTOSAVE_CLEAN_STEP_SIZE
			};
		}

		private TaskStepResponse DeleteAuthorizationGrants()
		{
			Repository repository = new Repository();

			IEnumerable<AuthorizationGrant> grants = repository.Authorization.AuthorizationGrants.GetByExpiration(DateTime.UtcNow, Constant.Tasks.AUTHORIZATION_GRANT_CLEAN_STEP_SIZE);

			int count = 0;

			foreach (AuthorizationGrant grant in grants)
			{
				repository.Authorization.AuthorizationGrants.Delete(grant);
				count++;
			}

			return new TaskStepResponse
			{
				Continue = count >= Constant.Tasks.AUTHORIZATION_GRANT_CLEAN_STEP_SIZE
			};
		}

		private TaskStepResponse OptimizeIndex()
		{
			return null;
		}

		#endregion
	}
}
