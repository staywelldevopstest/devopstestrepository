﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KswApi.Poco;
using KswApi.Poco.Tasks;
using KswApi.Repositories;

namespace KswApi.Logic.Tasks
{
	public class TaskLogic
	{
		public void Test()
		{

		}

		public void TriggerTasks()
		{

		}

		public bool StartTask(string id)
		{

			return false;
		}

		public bool TryAcquireTask(string id)
		{
			object obj;

			return TryAcquireTask(id, out obj);
		}

		public bool TryAcquireTask<TData>(string id, out TData data)
		{
			Repository repository = new Repository();

			ExecutingTask<TData> task = repository.Tasks.ExecutingTasks.GetById<TData>(id);

			if (task == null)
			{
				task = new ExecutingTask<TData>
				{
					Id = id,
					Started = DateTime.UtcNow,
					State = TaskState.Waiting
				};

				try
				{
					repository.Tasks.ExecutingTasks.Add(task);
				}
				catch (Exception)
				{
					// can't execute the task (likely to a duplicate key exception)
					// we should fail gracefully and assume the task is running elsewhere
					data = default(TData);
					return false;
				}
				
			}
			else if (task.State != TaskState.Waiting)
			{
				data = default(TData);
				return false;
			}

			if (!repository.Tasks.ExecutingTasks.Acquire<TData>(id))
			{
				data = default(TData);
				return false;
			}

			task = repository.Tasks.ExecutingTasks.GetById<TData>(id);

			data = task.Data;

			return true;
		}

		public void UpdateTask<TData>(string id, TData data)
		{
			Repository repository = new Repository();

			repository.Tasks.ExecutingTasks.Update(id, data);
		}

		public void ReleaseTask(string id)
		{
			Repository repository = new Repository();

			repository.Tasks.ExecutingTasks.Release(id);
		}

		public void FinishTask(string id)
		{
			Repository repository = new Repository();

			repository.Tasks.ExecutingTasks.Delete(id);
		}
	}
}
