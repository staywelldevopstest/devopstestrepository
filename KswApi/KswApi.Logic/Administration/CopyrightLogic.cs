﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logic.Framework.Constants;
using KswApi.Poco.Content;
using KswApi.Repositories;
using KswApi.Repositories.Objects;

namespace KswApi.Logic.Administration
{
	public class CopyrightLogic
	{
		#region Public Methods

		public void EnsureDefaultCopyright()
		{
			Guid guid = new Guid(Constant.Copyright.DEFAULT_COPYRIGHT_ID);

			Repository repository = new Repository();
			
			Copyright copyright = repository.Administration.Copyrights.GetCopyright(guid);

			if (copyright != null && copyright.Value == Constant.Copyright.DEFAULT_COPYRIGHT_TEXT)
				return;

			DateTime creationDate = DateTime.UtcNow;
			Copyright newCopyright = new Copyright
			{
				Id = guid,
				Value = Constant.Copyright.DEFAULT_COPYRIGHT_TEXT,
				DateAdded = creationDate,
				DateModified = creationDate
			};

			if (copyright == null)
			{
				repository.Administration.Copyrights.Add(newCopyright);
			}
			else
			{
				repository.Administration.Copyrights.Update(newCopyright);
			}
		}

		public CopyrightList GetCopyrightList(int offset, int count, string query, bool includeBucketCount)
		{
			if (count > Constant.MAXIMUM_PAGE_SIZE)
			{
				throw new ServiceException(HttpStatusCode.BadRequest, "Count is greater than the maximum page size.");
			}

			if (count < 0)
			{
				throw new ServiceException(HttpStatusCode.BadRequest, "Count is less than zero.");
			}

			if (offset < 0)
			{
				throw new ServiceException(HttpStatusCode.BadRequest, "Offset is less than zero.");
			}

			Repository repository = new Repository();

			CopyrightList result = new CopyrightList();
			Guid defaultCopyrightId = Guid.Parse(Constant.Copyright.DEFAULT_COPYRIGHT_ID);

			IEnumerable<Copyright> projectedCopyrights;
			if (string.IsNullOrWhiteSpace(query))
			{
				projectedCopyrights = repository.Administration.Copyrights.GetCopyrights(offset, count, GetCopyrightExclusions());
				result.Total = repository.Administration.Copyrights.Count(GetCopyrightExclusions());
			}
			else
			{
				projectedCopyrights = repository.Administration.Copyrights.GetCopyrights(offset, count, query, GetCopyrightExclusions());
				result.Total = repository.Administration.Copyrights.Count(query, GetCopyrightExclusions());
			}

			IEnumerable<CopyrightResponse> copyrightResponses;
			Copyright defaultCopyright = repository.Administration.Copyrights.GetCopyright(defaultCopyrightId) ?? new Copyright();

			if (includeBucketCount)
			{
				copyrightResponses = projectedCopyrights
					.Where(x => defaultCopyrightId != x.Id)
					.Select(GetResponseWithBucketCount);
				result.Default = GetResponseWithBucketCountForDefault(defaultCopyright);
			}
			else
			{
				copyrightResponses = projectedCopyrights
					.Where(x => defaultCopyrightId != x.Id)
					.Select(GetResponseWithoutBucketCount);
				result.Default = GetResponseWithoutBucketCount(defaultCopyright);
			}

			result.Items = copyrightResponses.OrderByDescending(x => x.DateModified).ToList();
			result.Offset = offset;
			return result;
		}

		public CopyrightResponse CreateCopyright(CopyrightRequest copyright)
		{
			ValidateCopyright(copyright);

			Repository repository = new Repository();

			DateTime creationDate = DateTime.UtcNow;
			Copyright newCopyright = new Copyright();
			Copy(copyright, newCopyright);
			newCopyright.DateAdded = creationDate;
			newCopyright.DateModified = creationDate;
			repository.Administration.Copyrights.Add(newCopyright);

			return GetResponseWithoutBucketCount(newCopyright);
		}

		public CopyrightResponse UpdateCopyright(string id, CopyrightRequest copyright)
		{
			if (string.IsNullOrEmpty(id))
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Copyrights.COPYRIGHT_ID_MUST_BE_SPECIFIED);
			}

			Guid copyrightId;
			//Taken out to allow for requested functionality
			//if (string.Equals(id, Constant.Administration.DEFAULT_COPYRIGHT_ID))
			//{
			//	throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Copyrights.COPYRIGHT_NOT_FOUND);
			//}

			if (string.Equals(id, "default", StringComparison.OrdinalIgnoreCase))
			{
				copyrightId = Guid.Parse(Constant.Copyright.DEFAULT_COPYRIGHT_ID);
			}
			else if (!Guid.TryParse(id, out copyrightId))
			{
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Copyrights.COPYRIGHT_NOT_FOUND);
			}

			ValidateCopyright(copyright);

			Repository repository = new Repository();
			Copyright storedCopyright = repository.Administration.Copyrights.GetCopyright(copyrightId);
			if (storedCopyright == null)
			{
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Copyrights.COPYRIGHT_NOT_FOUND);
			}

			Copy(copyright, storedCopyright);
			storedCopyright.DateModified = DateTime.UtcNow;
			repository.Administration.Copyrights.Update(storedCopyright);

			return GetResponseWithoutBucketCount(storedCopyright);
		}

		public CopyrightResponse DeleteCopyright(string id)
		{
			if (string.IsNullOrEmpty(id))
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Copyrights.COPYRIGHT_ID_MUST_BE_SPECIFIED);
			}

			if (string.Equals(id, "default", StringComparison.OrdinalIgnoreCase))
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Copyrights.COPYRIGHT_CANNOT_BE_DELETED);
			}

			if (string.Equals(id, Constant.Copyright.DEFAULT_COPYRIGHT_ID, StringComparison.OrdinalIgnoreCase))
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Copyrights.COPYRIGHT_CANNOT_BE_DELETED);
			}

			Guid copyrightId;
			if (!Guid.TryParse(id, out copyrightId))
			{
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Copyrights.COPYRIGHT_NOT_FOUND);
			}

			Repository repository = new Repository();
			Copyright storedCopyright = repository.Administration.Copyrights.GetCopyright(copyrightId);
			if (storedCopyright == null)
			{
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Copyrights.COPYRIGHT_NOT_FOUND);
			}

			IEnumerable<ContentCore> masters = repository.Content.Cores.GetByCopyrightId(copyrightId);
			foreach (var master in masters)
			{
				master.CopyrightId = null;
				repository.Content.Cores.Update(master);
			}

			IEnumerable<ContentBucket> contentBuckets = repository.Content.Buckets.GetByCopyrightIdForAllStatuses(copyrightId);
			foreach (var contentBucket in contentBuckets)
			{
				contentBucket.CopyrightId = null;
				repository.Content.Buckets.Update(contentBucket);
			}

			repository.Administration.Copyrights.Delete(copyrightId);

			return GetResponseWithoutBucketCount(storedCopyright);
		}

		#endregion

		#region Private Methods

		private List<Guid> GetCopyrightExclusions()
		{
			return new List<Guid> { new Guid(Constant.Copyright.DEFAULT_COPYRIGHT_ID) };
		}

		private void ValidateCopyright(CopyrightRequest copyright)
		{
			if (copyright == null)
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Copyrights.COPYRIGHT_VALUE_CANNOT_BE_NULL_OR_EMPTY);
			}

			if (string.IsNullOrEmpty(copyright.Value))
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Copyrights.COPYRIGHT_VALUE_CANNOT_BE_NULL_OR_EMPTY);
			}
		}

		private CopyrightResponse GetResponseWithoutBucketCount(Copyright copyright)
		{
			CopyrightResponse response = new CopyrightResponse
			{
				Value = copyright.Value,
				DateAdded = copyright.DateAdded,
				DateModified = copyright.DateModified,
				Id = copyright.Id
			};

			return response;
		}

		private CopyrightResponse GetResponseWithBucketCountForDefault(Copyright copyright)
		{
			Repository repository = new Repository();
			ContentBucketSearch bucketSearch = new ContentBucketSearch
											   {
												   DefaultCopyright = true
											   };

			int bucketCount = repository.Content.Buckets.CountByStatusAndSearch(ObjectStatus.Active, bucketSearch);
			CopyrightResponse response = new CopyrightResponse
			{
				Value = copyright.Value,
				DateAdded = copyright.DateAdded,
				DateModified = copyright.DateModified,
				Id = copyright.Id,
				BucketCount = bucketCount
			};

			return response;
		}

		private CopyrightResponse GetResponseWithBucketCount(Copyright copyright)
		{
			Repository repository = new Repository();
			ContentBucketSearch bucketSearch = new ContentBucketSearch
				{
					CopyrightId = copyright.Id
				};

			int bucketCount = repository.Content.Buckets.CountByStatusAndSearch(ObjectStatus.Active, bucketSearch);
			CopyrightResponse response = new CopyrightResponse
			{
				Value = copyright.Value,
				DateAdded = copyright.DateAdded,
				DateModified = copyright.DateModified,
				Id = copyright.Id,
				BucketCount = bucketCount
			};

			return response;
		}

		private void Copy(CopyrightRequest from, Copyright to)
		{
			to.Value = from.Value;
		}

		#endregion
	}
}
