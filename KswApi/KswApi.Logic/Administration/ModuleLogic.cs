﻿using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logic.ContentLogic;
using KswApi.Logic.Framework;
using KswApi.Logic.Framework.Objects;
using KswApi.Logic.SmsLogic;
using KswApi.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace KswApi.Logic.Administration
{
	public class ModuleLogic
	{
		public void InitializeModules()
		{
			ModuleProvider.InitializeModules();
		}

		public ServiceModule GetServiceModule(string moduleId)
		{
			ModuleType type;

			if (Enum.TryParse(moduleId, true, out type))
				return ModuleProvider.GetModule(type);

			throw new ServiceException(HttpStatusCode.BadRequest, "Invalid module ID.");
		}

		public List<string> GetModuleRights(IEnumerable<ModuleType> modules)
		{
			List<ServiceModule> serviceModules = modules.Select(ModuleProvider.GetModule).ToList();

			return serviceModules.SelectMany(item => item.DefaultScope).Distinct().ToList();
		}

		public ModuleNameList GetModules()
		{
			return new ModuleNameList
									  {
										  Items =
											  new List<ModuleName>(
											  ModuleProvider.GetModules().Select(
												  item => new ModuleName { Name = item.Name, Type = item.Type }))
									  };
		}

		public LicenseModuleList GetLicenseModules(string licenseId)
		{
			LicenseLogic licenseLogic = new LicenseLogic();

			License license = licenseLogic.GetLicense(licenseId);

			IList<ServiceModule> modules = ModuleProvider.GetModules();

			List<ModuleName> selected = new List<ModuleName>();
			List<ModuleName> available = new List<ModuleName>();

			if (license.Modules == null || license.Modules.Count == 0)
			{
				available.AddRange(modules.Select(item => new ModuleName { Name = item.Name, Type = item.Type }));
			}
			else
			{
				foreach (ServiceModule module in modules)
				{
					ModuleName toAdd = new ModuleName
										   {
											   Name = module.Name,
											   Type = module.Type
										   };

					if (license.Modules.Any(item => item == module.Type))
						selected.Add(toAdd);
					else
						available.Add(toAdd);
				}
			}

			return new LicenseModuleList
					   {
						   Items = selected,
						   Available = available
					   };
		}

		public ModuleName AddLicenseModule(string licenseId, string moduleType)
		{
			ServiceModule module = GetServiceModule(moduleType);

			LicenseLogic licenseLogic = new LicenseLogic();

			License license = licenseLogic.GetLicense(licenseId);

			if (license.Modules == null)
				license.Modules = new List<ModuleType>();

			if (license.Modules.Contains(module.Type))
				throw new ServiceException(HttpStatusCode.BadRequest, "The license already contains the specified module.");

			Repository repository = new Repository();

			// add the module to the db before updating the license, this ensures any failure
			// doesn't leave the license with the module added but no module object
			switch (module.Type)
			{
				case ModuleType.Content:
					ContentModuleLogic contentModuleLogic = new ContentModuleLogic();
					contentModuleLogic.CreateContentModule(licenseId);
					break;
			}

			license.Modules.Add(module.Type);

			repository.Administration.Licenses.Update(license);

			return new ModuleName { Name = module.Name, Type = module.Type };
		}

		public ModuleName RemoveLicenseModule(string licenseId, string moduleType)
		{
			ServiceModule module = GetServiceModule(moduleType);

			LicenseLogic licenseLogic = new LicenseLogic();

			License license = licenseLogic.GetLicense(licenseId);

			if (license.Modules == null || !license.Modules.Remove(module.Type))
				throw new ServiceException(HttpStatusCode.NotFound, "License did not contain the specified module.");

			Repository repository = new Repository();

			switch (module.Type)
			{
				case ModuleType.SmsGateway:
					SmsAdministrationLogic smsLogic = new SmsAdministrationLogic();
					smsLogic.DeleteModuleForLicense(license);
					break;
				case ModuleType.Content:
					ContentModuleLogic contentModuleLogic = new ContentModuleLogic();
					contentModuleLogic.DeleteModuleForLicense(license);
					break;
				case ModuleType.ManageContent:
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			repository.Administration.Licenses.Update(license);

			return new ModuleName { Name = module.Name, Type = module.Type };
		}
	}
}
