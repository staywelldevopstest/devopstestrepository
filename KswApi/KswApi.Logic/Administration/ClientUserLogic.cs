﻿using KswApi.Common.Objects;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.Framework;
using KswApi.Logic.Framework.Constants;
using KswApi.Logic.Framework.Helpers;
using KswApi.Poco.Enums;
using KswApi.Repositories;
using KswApi.Repositories.Enums;
using KswApi.Repositories.Objects;
using Linq2Rest.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;

namespace KswApi.Logic.Administration
{
	public class ClientUserLogic
	{
		public ClientUserList GetClientUsers(int offset, int count, string query, string orderBy, string filter)
		{
			ClientUserList result;

			if (count > Constant.MAXIMUM_PAGE_SIZE)
				throw new ServiceException(HttpStatusCode.BadRequest, "Count is greater than the maximum page size.");

			if (count < 0)
				throw new ServiceException(HttpStatusCode.BadRequest, "Count is less than zero.");

			if (offset < 0)
				throw new ServiceException(HttpStatusCode.BadRequest, "Offset is less than zero.");

			Repository repository = new Repository();

			SortOption<ClientUser> sortOption;

			if (string.IsNullOrEmpty(orderBy))
			{
				sortOption = SortOption<ClientUser>.FromProperty(item => item.LastName);
			}
			else
			{
				SortParser<ClientUser> sortParser = new SortParser<ClientUser>();

				sortOption = sortParser.Parse(orderBy);
			}

			Expression<Func<ClientUser, bool>> expression = ParseFilter(filter);

			if (string.IsNullOrWhiteSpace(query))
			{
				result = new ClientUserList
						 {
							 Items =
								 repository.Administration.ClientUsers.GetClientUsers(offset, count, sortOption, expression).Select(
									 GetResponse).ToList(),
							 Offset = offset,
							 Total = repository.Administration.ClientUsers.Count(expression),
							 SortedBy = sortOption.ToString()
						 };
			}
			else
			{
				result = new ClientUserList
						 {
							 Items =
								 repository.Administration.ClientUsers.GetClientUsers(offset, count, query, sortOption, expression).Select
								 (GetResponse).ToList(),
							 Offset = offset,
							 Total = repository.Administration.ClientUsers.Count(query, expression),
							 SortedBy = sortOption.ToString()
						 };
			}

			result = GetClientNames(result);

			return result;
		}

		public ClientUserResponse GetClientUser(string idOrEmail)
		{
			ClientUser user = InternalGetClientUser(idOrEmail);

			return GetResponse(user);
		}

		/// <summary>
		/// Used locally and by password logic
		/// </summary>
		/// <param name="idOrEmail"></param>
		/// <returns></returns>
		public ClientUser InternalGetClientUser(string idOrEmail)
		{
			Guid clientUserId;
			ClientUser user;

			if (string.IsNullOrEmpty(idOrEmail))
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Administration.CLIENT_USER_NOT_FOUND);

			if (Guid.TryParse(idOrEmail, out clientUserId))
			{
				Repository repository = new Repository();

				user = repository.Administration.ClientUsers.GetUser(clientUserId);
			}
			else
			{
				Repository repository = new Repository();

				user = repository.Administration.ClientUsers.GetUserByEmail(idOrEmail);
			}

			if (user == null)
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Administration.CLIENT_USER_NOT_FOUND);

			return user;
		}

		public ClientUserResponse GetClientUserByEmail(string emailAddress)
		{
			if (string.IsNullOrWhiteSpace(emailAddress))
				throw new ServiceException(HttpStatusCode.BadRequest, "Invalid email");

			Repository repository = new Repository();

			return GetResponse(repository.Administration.ClientUsers.GetUserByEmail(emailAddress));
		}

		public ClientUserResponse CreateClientUser(ClientUserRequest clientUser)
		{
			ValidateRequest(clientUser);

			Repository repository = new Repository();

			if (repository.Administration.ClientUsers.ExistsByEmail(clientUser.EmailAddress))
				throw new ServiceException(HttpStatusCode.BadRequest, "Invalid or duplicate email address");

			ClientUser newClientUser = new ClientUser();

			Copy(clientUser, newClientUser);

			newClientUser.Created = DateTime.UtcNow;

			repository.Administration.ClientUsers.Add(newClientUser);

			PasswordLogic passwordLogic = new PasswordLogic();
			passwordLogic.CreateClientUserPassword(newClientUser);

			return GetResponse(newClientUser);
		}

		public ClientUserResponse UpdateClientUser(string id, ClientUserUpdateRequest clientUser)
		{
			Guid clientUserId;

			Guid.TryParse(id, out clientUserId);

			if (clientUserId == Guid.Empty)
				throw new ServiceException(HttpStatusCode.BadRequest, "Invalid Id");

			ValidateRequest(clientUser);

			Repository repository = new Repository();

			ClientUser storedUser = repository.Administration.ClientUsers.GetUser(clientUserId);

			if (storedUser == null)
				throw new ServiceException(HttpStatusCode.BadRequest, "User not found");

			if (storedUser.EmailAddress.ToLower() != clientUser.EmailAddress.ToLower())
			{
				if (repository.Administration.ClientUsers.ExistsByEmail(clientUser.EmailAddress))
					throw new ServiceException(HttpStatusCode.BadRequest, "Invalid or duplicate email address");
			}

			Copy(clientUser, storedUser);

			if (clientUser.ResetPassword)
			{
				PasswordLogic passwordLogic = new PasswordLogic();

				PasswordResetRequest resetRequest = new PasswordResetRequest
													{
														Email = clientUser.EmailAddress
													};

				passwordLogic.ResetClientUserPassword(resetRequest);
			}

			repository.Administration.ClientUsers.Update(storedUser);

			if (clientUser.Disabled)
			{
				repository.Authorization.AccessTokens.DeleteByUserId(storedUser.Id);
				repository.Authorization.RefreshTokens.DeleteByUserId(storedUser.Id);
			}

			return GetResponse(storedUser);
		}

		public ClientUserResponse DeleteClientUser(string id)
		{
			Guid clientUserId;

			Guid.TryParse(id, out clientUserId);

			if (clientUserId == Guid.Empty)
				throw new ServiceException(HttpStatusCode.BadRequest, "Invalid Id");

			Repository repository = new Repository();

			ClientUser storedUser = repository.Administration.ClientUsers.GetUser(clientUserId);

			if (storedUser == null)
				throw new ServiceException(HttpStatusCode.BadRequest, "User not found");

			ClientUserState originalState = storedUser.State;

			storedUser.State = ClientUserState.Deleted;

			repository.Administration.ClientUsers.Update(storedUser);
			
			repository.Authorization.AccessTokens.DeleteByUserId(storedUser.Id);
			repository.Authorization.RefreshTokens.DeleteByUserId(storedUser.Id);

			// set the state back to what it was for the response
			storedUser.State = originalState;

			return GetResponse(storedUser);
		}

		public void DeleteByClientId(Guid clientId)
		{
			Repository repository = new Repository();

			IEnumerable<ClientUser> users = repository.Administration.ClientUsers.GetByClientId(clientId);

			foreach (ClientUser user in users)
			{
				repository.Administration.ClientUsers.Delete(user.Id);
			}
		}

		public ClientUserValidation ValidateClientUserEmail(string emailAddress)
		{
			if (string.IsNullOrWhiteSpace(emailAddress))
				throw new ServiceException(HttpStatusCode.BadRequest, "No email address was specified.");

			if (!EmailHelper.IsValid(emailAddress))
				throw new ServiceException(HttpStatusCode.BadRequest, "Invalid email address.");

			Repository repository = new Repository();

			return new ClientUserValidation
				   {
					   IsAvailable = !repository.Administration.ClientUsers.ExistsByEmail(emailAddress)
				   };
		}

		public UserAuthenticationDetails Authenticate(string emailAddress, string password)
		{
			if (string.IsNullOrEmpty(emailAddress) || string.IsNullOrEmpty(password))
				return null;

			emailAddress = emailAddress.ToLower();

			Repository repository = new Repository();

			ClientUser user = repository.Administration.ClientUsers.GetUserByEmail(emailAddress);

			if (user == null)
				return null;

			if (user.State != ClientUserState.Active)
				return null;

			DateTime now = DateTime.UtcNow;

			if (user.LockedTime.HasValue)
			{
				if (now < user.LockedTime.Value + TimeSpan.FromMinutes(Constant.Administration.ACCOUNT_LOCKOUT_DURATION_IN_MINUTES))
					return null;

				user.LockedTime = null;
				user.RecentFailedLogins = null;
			}

			LoginAttempt loginAttempt = new LoginAttempt
										{
											Time = now,
											UserId = user.Id,
											UserType = UserType.ClientUser
										};

			PasswordLogic passwordLogic = new PasswordLogic();

			bool loginSuccess = passwordLogic.ValidatePassword(password, user);

			if (loginSuccess)
			{
				loginAttempt.Result = LoginResult.Success;

				user.LastLogin = now;

				user.RecentFailedLogins = null;
			}
			else
			{
				loginAttempt.Result = LoginResult.Failure;

				if (user.RecentFailedLogins == null)
				{
					user.RecentFailedLogins = new List<DateTime>
					                          {
						                          now
					                          };
				}
				else
				{
					user.RecentFailedLogins.Add(now);

					if (user.RecentFailedLogins.Count >= Constant.Administration.MAXIMUM_FAILED_LOGIN_ATTEMPT)
						user.LockedTime = now;
				}
			}

			repository.Administration.ClientUsers.Update(user);

			repository.Administration.LoginAttempts.Add(loginAttempt);

			if (!loginSuccess)
				return null;

			return new UserAuthenticationDetails
			       {
					   Id = user.Id,
				       EmailAddress = user.EmailAddress,
					   FirstName = user.FirstName,
					   LastName = user.LastName,
					   Type = UserType.ClientUser
			       };
		}

		public void UnlockClientUser(string userIdOrEmail)
		{
			ClientUser user = InternalGetClientUser(userIdOrEmail);

			Repository repository = new Repository();
			user.LockedTime = null;
			user.RecentFailedLogins = null;
			repository.Administration.ClientUsers.Update(user);
		}

        public UserDetailsResponse UserDetailsResponseFromClientUser(Guid id)
        {
            Repository repository = new Repository();
            ClientUser user = repository.Administration.ClientUsers.GetUser(id);
            if (user == null)
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.User.USER_NOT_FOUND);

            return new UserDetailsResponse
            {
                Id = user.Id,
                Type = UserType.ClientUser,
                FullName = string.Join(" ", new[] { user.FirstName, user.LastName }).Trim()
            };
        }

		#region Private Methods

		private ClientUserResponse GetResponse(ClientUser user)
		{
			return new ClientUserResponse
			{
				ClientId = user.ClientId,
				Created = user.Created,
				Id = user.Id,
				Disabled = user.State == ClientUserState.Disabled,
				EmailAddress = user.EmailAddress,
				FirstName = user.FirstName,
				LastName = user.LastName,
				Licenses = user.Licenses,
				PermissionType = user.PermissionType,
				PhoneNumber = user.PhoneNumber,
				Locked = user.LockedTime != null && DateTime.UtcNow < user.LockedTime.Value + TimeSpan.FromMinutes(Constant.Administration.ACCOUNT_LOCKOUT_DURATION_IN_MINUTES),
				LastLogin = user.LastLogin
			};
		}

		private Expression<Func<ClientUser, bool>> ParseFilter(string filter)
		{
			FilterExpressionFactory factory = new FilterExpressionFactory();

			try
			{
				return factory.Create<ClientUser>(filter);
			}
			catch (InvalidOperationException exception)
			{
				throw new ServiceException(HttpStatusCode.BadRequest, string.Format("The filter string is invalid. {0}", exception.Message));
			}
		}

        // TODO: Use constants...
		private void ValidateRequest(ClientUserRequest request)
		{
			if (request == null)
				throw new ServiceException(HttpStatusCode.BadRequest, "Request body is not recognized.");

			if (string.IsNullOrWhiteSpace(request.FirstName) || string.IsNullOrWhiteSpace(request.LastName))
				throw new ServiceException(HttpStatusCode.BadRequest, "Invalid first or last name");

			if (string.IsNullOrWhiteSpace(request.EmailAddress))
				throw new ServiceException(HttpStatusCode.BadRequest, "Email address is required.");

			if (!EmailHelper.IsValid(request.EmailAddress))
				throw new ServiceException(HttpStatusCode.BadRequest, "Invalid email address.");

			if (request.ClientId == Guid.Empty)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Administration.CLIENT_REQUIRED);

			Repository repository = new Repository();
			if (repository.Administration.Clients.GetById(request.ClientId) == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Administration.INVALID_CLIENT_ID);
		}

		private void Copy(ClientUserRequest from, ClientUser to)
		{
			to.ClientId = from.ClientId;
			to.State = from.Disabled ? ClientUserState.Disabled : ClientUserState.Active;
			to.EmailAddress = from.EmailAddress;
			to.FirstName = from.FirstName;
			to.LastName = from.LastName;
			to.Licenses = from.Licenses;
			to.PermissionType = from.PermissionType;
			to.PhoneNumber = from.PhoneNumber;
		}

		private ClientUserList GetClientNames(ClientUserList result)
		{
			Dictionary<Guid, string> clients = new Dictionary<Guid, string>();

			Repository repository = new Repository();

			foreach (ClientUserResponse clientUser in result.Items)
			{
				string name;

				if (!clients.TryGetValue(clientUser.ClientId, out name))
				{
					Client client = repository.Administration.Clients.GetById(clientUser.ClientId);

					name = client != null ? client.ClientName : string.Empty;

					clients[clientUser.ClientId] = name;
				}

				clientUser.ClientName = name;
			}

			return result;
		}

		#endregion
	}
}
