﻿using System;
using System.Collections.Generic;
using System.Net;
using KswApi.Common.Configuration;
using KswApi.Common.Objects;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.Framework.Constants;
using KswApi.Poco.Administration;
using KswApi.Poco.Enums;
using KswApi.Repositories;
using KswApi.Repositories.Objects;

namespace KswApi.Logic.Administration
{
	public class UserLogic : LogicBase
	{
		public AuthenticatedUserResponse GetUserDetails(bool startTimer, AccessToken accessToken)
		{
			if (accessToken == null)
				throw new ServiceException(HttpStatusCode.Unauthorized, ErrorMessage.OAuth.ACCESS_TOKEN_REQUIRED);

			if (DateTime.UtcNow > accessToken.Expiration)
			{
				Repository.Authorization.AccessTokens.Delete(accessToken);

				throw new ServiceException(HttpStatusCode.Unauthorized, ErrorMessage.OAuth.EXPIRED_ACCESS_TOKEN);
			}

			UserDetails userDetails = accessToken.UserDetails;

			if (userDetails == null)
				throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.Authorization.INVALID_LOGIN_TYPE);

			TimeSpan timeout = Settings.Current.WebPortal.Timeout;

			if (startTimer)
			{
				accessToken.WebPortalTimeout = DateTime.UtcNow + timeout;

				Repository repository = new Repository();

				repository.Authorization.AccessTokens.Update(accessToken);
			}

			return new AuthenticatedUserResponse
				   {
					   EmailAddress = userDetails.EmailAddress,
					   FirstName = userDetails.FirstName,
					   LastName = userDetails.LastName,
					   FullName = userDetails.FirstName + ' ' + userDetails.LastName,
					   Rights = accessToken.Rights != null ? new List<string>(accessToken.Rights) : new List<string>(),
					   Type = userDetails.Type,
					   TimeoutInMilliseconds = startTimer
							? (int)timeout.TotalMilliseconds // convert to milliseconds
							: 0,
					   TimeoutCountdownInMilliseconds = startTimer ? (int)Settings.Current.WebPortal.TimeoutCountdown.TotalMilliseconds : 0,
					   TimeoutPollIntervalInMilliseconds = startTimer ? (int)Settings.Current.WebPortal.TimeoutPollInterval.TotalMilliseconds : 0
				   };
		}

		public UserSettingsResponse GetUserSettings(AccessToken accessToken)
		{
			if (accessToken == null)
				throw new ServiceException(HttpStatusCode.Unauthorized, ErrorMessage.Authorization.UNAUTHORIZED);

			if (accessToken.UserDetails == null)
				throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.Authorization.INVALID_LOGIN_TYPE);

			switch (accessToken.UserDetails.Type)
			{
				case UserType.ClientUser:
					return GetClientUserSettings(accessToken);
				default:
					throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.Authorization.INVALID_LOGIN_TYPE);
			}
		}

		public UserSettingsResponse UpdateUserSettings(AccessToken accessToken, UserSettingsRequest userSettings)
		{
			if (accessToken == null)
				throw new ServiceException(HttpStatusCode.Unauthorized, ErrorMessage.Authorization.UNAUTHORIZED);

			if (accessToken.UserDetails == null)
				throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.Authorization.INVALID_LOGIN_TYPE);

			switch (accessToken.UserDetails.Type)
			{
				case UserType.ClientUser:
					return UpdateClientUserSettings(accessToken, userSettings);
				default:
					throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.Authorization.INVALID_LOGIN_TYPE);
			}
		}

		public ClientLicenseDetail GetUserLicenseDetails(AccessToken accessToken)
		{
			if (accessToken == null)
				throw new ServiceException(HttpStatusCode.Unauthorized, ErrorMessage.Authorization.UNAUTHORIZED);

			if (accessToken.UserDetails == null || accessToken.UserDetails.Type != UserType.ClientUser)
				throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.Authorization.INVALID_LOGIN_TYPE);

			Repository repository = new Repository();
			ClientUser clientUser = repository.Administration.ClientUsers.GetUser(accessToken.UserDetails.Id);

			if (clientUser == null || clientUser.State != ClientUserState.Active)
				throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.Authorization.UNAUTHORIZED);

			LicenseLogic licenseLogic = new LicenseLogic();
			return licenseLogic.GetClientLicenseDetail(clientUser.ClientId.ToString(), null);
		}

		#region Private Methods

		private UserSettingsResponse GetClientUserSettings(AccessToken accessToken)
		{
			if (accessToken.UserDetails == null)
				throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.User.USER_NOT_FOUND);

			Repository repository = new Repository();
			ClientUser user = repository.Administration.ClientUsers.GetUser(accessToken.UserDetails.Id);

			if (user == null)
				throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.User.USER_NOT_FOUND);

			return new UserSettingsResponse
			{
				EmailAddress = user.EmailAddress,
				FirstName = user.FirstName,
				LastName = user.LastName,
				PhoneNumber = user.PhoneNumber,
				Notes = user.Notes
			};
		}

		private UserSettingsResponse UpdateClientUserSettings(AccessToken accessToken, UserSettingsRequest userSettings)
		{
			if (accessToken.UserDetails == null)
				throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.User.USER_NOT_FOUND);

			if (userSettings == null)
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.User.USER_UPDATE_REQUEST_CANNOT_BE_NULL);
			}

			if (string.IsNullOrWhiteSpace(userSettings.FirstName))
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.User.FIRST_NAME_CANNOT_BE_NULL_OR_EMPTY);
			}

			if (string.IsNullOrWhiteSpace(userSettings.EmailAddress))
			{
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.User.EMAIL_ADDRESS_CANNOT_BE_NULL_OR_EMPTY);
			}

			Repository repository = new Repository();

			ClientUser clientUser = repository.Administration.ClientUsers.GetUser(accessToken.UserDetails.Id);

			if (clientUser == null)
				throw new ServiceException(HttpStatusCode.Forbidden, ErrorMessage.User.USER_NOT_FOUND);

			ClientUser clientCheck = repository.Administration.ClientUsers.GetUserByEmail(userSettings.EmailAddress);

			if (clientCheck != null && clientCheck.Id != accessToken.UserDetails.Id)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.User.ANOTHER_USER_HAS_SPECIFIED_EMAIL);

			clientUser.FirstName = userSettings.FirstName;
			clientUser.LastName = userSettings.LastName;
			clientUser.EmailAddress = userSettings.EmailAddress;
			clientUser.Notes = userSettings.Notes;
			clientUser.PhoneNumber = userSettings.PhoneNumber;

			repository.Administration.ClientUsers.Update(clientUser);

			return new UserSettingsResponse
					   {
						   EmailAddress = clientUser.EmailAddress,
						   FirstName = clientUser.FirstName,
						   LastName = clientUser.LastName,
						   Notes = clientUser.Notes,
						   PhoneNumber = clientUser.PhoneNumber
					   };
		}

		#endregion

		public UserDetailsResponse DetailResponseFromUser(User user)
		{
			if (user == null)
				return null;

			switch (user.Type)
			{
				case UserType.ClientUser:
					ClientUserLogic clientUserLogic = new ClientUserLogic();
					return clientUserLogic.UserDetailsResponseFromClientUser(user.Id);
				case UserType.DomainUser:
					DomainUserLogic domainUserLogic = new DomainUserLogic();
					return domainUserLogic.UserDetailsResponseFromDomainUser(user.Id);
				default:
					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.User.UNSUPPORTED_USER_TYPE);
			}

		}
	}
}
