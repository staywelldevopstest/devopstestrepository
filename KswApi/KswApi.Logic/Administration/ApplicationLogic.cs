﻿using System.Collections.Specialized;
using KswApi.Common;
using KswApi.Common.Configuration;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logic.Framework;
using KswApi.Logic.Framework.Constants;
using KswApi.Logic.Framework.Helpers;
using KswApi.Repositories;
using KswApi.Repositories.Objects;
using Linq2Rest.Parser;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;

namespace KswApi.Logic.Administration
{
	public class ApplicationLogic
	{
		public Application GetApplication(string applicationId)
		{
			if (string.IsNullOrEmpty(applicationId))
				throw new ServiceException(HttpStatusCode.NotFound, "Application ID was not specified.");

			Guid guid;
			if (!Guid.TryParse(applicationId, out guid))
				throw new ServiceException(HttpStatusCode.NotFound, "Application ID is invalid.");

			Repository repository = new Repository();

			Application application = repository.Administration.Applications.GetById(guid);

			if (application == null)
				throw new ServiceException(HttpStatusCode.NotFound, "Application not found.");

			return application;
		}

		public Application GetApplicationFromParameters(NameValueCollection headers)
		{
			string headerName =
				headers.AllKeys.FirstOrDefault(
					item =>
					string.Equals(item, Constant.Application.APPLICATION_QUERY_STRING_NAME, StringComparison.OrdinalIgnoreCase));

			if (string.IsNullOrEmpty(headerName))
				return null;

			string headerValue = headers[headerName];

			Guid guid;
			if (!Guid.TryParse(headerValue, out guid))
				throw new ServiceException(HttpStatusCode.NotFound, "Application ID is invalid.");

			Repository repository = new Repository();

			Application application = repository.Administration.Applications.GetById(guid);

			if (application == null)
				throw new ServiceException(HttpStatusCode.NotFound, "Application not found.");

			return application;
		}

		public ApplicationList GetApplications(int offset, int count, string filter, string orderBy)
		{
			if (count > Constant.MAXIMUM_PAGE_SIZE)
				throw new ServiceException(HttpStatusCode.BadRequest, "Count is greater than the maximum page size.");

			if (offset < 0)
				throw new ServiceException(HttpStatusCode.BadRequest, "Offset is less than zero.");

			SortOption<Application> sortOption;

			if (string.IsNullOrEmpty(orderBy))
			{
				sortOption = SortOption<Application>.FromProperty(item => item.Name);
			}
			else
			{
				SortParser<Application> sortParser = new SortParser<Application>();

				sortOption = sortParser.Parse(orderBy);
			}

			Repository repository = new Repository();

			if (string.IsNullOrEmpty(filter))
			{
				return new ApplicationList
						   {
							   Items = repository.Administration.Applications.GetApplications(offset, count, sortOption).ToList(),
							   Offset = offset,
							   Total = repository.Administration.Applications.Count(),
							   SortedBy = sortOption.ToString()
						   };
			}

			Expression<Func<Application, bool>> expression = ParseApplicationFilter(filter);

			return new ApplicationList
			{
				Items = repository.Administration.Applications.GetApplications(offset, count, expression, sortOption).ToList(),
				Offset = offset,
				Total = repository.Administration.Applications.Count(expression),
				SortedBy = sortOption.ToString()
			};
		}

		public Application CreateApplication(Guid? licenseId, ApplicationRequest application)
		{
			ValidateApplication(application);

			Application newApplication = new Application
											 {
												 Type = application.Type,
												 LicenseId = licenseId,
												 Secret = TokenGenerator.CreateSecret(),
												 DateAdded = DateTime.UtcNow,
												 Description = application.Description,
												 Name = application.Name,
												 RedirectUri = application.RedirectUri,
												 Site = application.Site,
												 Addresses = application.Addresses,
												 EnableJsonp = application.EnableJsonp,
												 JsonpWebsites = application.JsonpWebsites
											 };

			Repository repository = new Repository();

			repository.Administration.Applications.Add(newApplication);

			return newApplication;
		}

		public Application UpdateApplication(Guid? licenseId, string applicationId, ApplicationRequest application)
		{
			Guid guid;
			if (!Guid.TryParse(applicationId, out guid))
				throw new ServiceException(HttpStatusCode.NotFound, "The application ID was not found.");

			ValidateApplication(application);

			Repository repository = new Repository();

			Application existingApplication = repository.Administration.Applications.GetById(guid);

			if (existingApplication == null)
				throw new ServiceException(HttpStatusCode.NotFound, string.Format("Application was not found: {0}.", applicationId));

			existingApplication.Description = application.Description;
			existingApplication.Name = application.Name;
			existingApplication.RedirectUri = application.RedirectUri;
			existingApplication.Site = application.Site;
			existingApplication.Type = application.Type;
			existingApplication.Addresses = application.Addresses;
			existingApplication.EnableJsonp = application.EnableJsonp;
			existingApplication.JsonpWebsites = application.JsonpWebsites;

			repository.Administration.Applications.Update(existingApplication);

			return existingApplication;
		}

		public Application ResetApplicationSecret(Guid? licenseId, string applicationId)
		{
			Guid guid;
			if (!Guid.TryParse(applicationId, out guid))
				throw new ServiceException(HttpStatusCode.NotFound, "The application ID was not found.");

			Repository repository = new Repository();

			Application existingApplication = repository.Administration.Applications.GetById(guid);

			if (existingApplication == null)
			{
				throw new ServiceException(HttpStatusCode.NotFound, string.Format("Application was not found: {0}.", applicationId));
			}

			existingApplication.Secret = TokenGenerator.CreateSecret();
			repository.Administration.Applications.Update(existingApplication);

			return existingApplication;
		}

		public Application DeleteApplication(string applicationId)
		{
			Guid guid;
			if (!Guid.TryParse(applicationId, out guid))
				throw new ServiceException(HttpStatusCode.NotFound, "The application ID was not found.");

			Repository repository = new Repository();

			Application application = repository.Administration.Applications.GetById(guid);

			if (application == null)
				throw new ServiceException(HttpStatusCode.NotFound, string.Format("Application was not found: {0}.", applicationId));

			repository.Administration.Applications.Delete(application);

			return application;
		}

		public void EnsurePortalApplication()
		{
			Guid guid = new Guid(Constant.WEB_PORTAL_APPLICATION_ID);

			Repository repository = new Repository();

			Application application = repository.Administration.Applications.GetById(guid);

			string secret = GetPortalApplicationSecret();

			List<string> ips = GetPortalIpAddresses();

			if (ApplicationIsCurrent(application, ApplicationType.AdministrationPortal, secret, Settings.Current.WebPortalHomeUri, ips))
				return;

			Application newApplication = new Application
				{
					Id = guid,
					Name = Constant.WEB_PORTAL_APPLICATION_NAME,
					Type = ApplicationType.AdministrationPortal,
					RedirectUri = Settings.Current.WebPortalHomeUri,
					Secret = secret,
					Addresses = ips
				};

			if (application == null)
				repository.Administration.Applications.Add(newApplication);
			else
				repository.Administration.Applications.Update(newApplication);
		}

		public void EnsureAuxiliaryApplication()
		{
			Guid guid = new Guid(Constant.AUXILIARY_APPLICATION_ID);

			Repository repository = new Repository();

			Application application = repository.Administration.Applications.GetById(guid);

			string secret = GetAuxiliaryApplicationSecret();

			List<string> ips = GetAuxiliaryIpAddresses();

			if (ApplicationIsCurrent(application, ApplicationType.AuxiliaryService, secret, Settings.Current.WebPortalHomeUri, ips))
				return;

			Application newApplication = new Application
			{
				Id = guid,
				Name = Constant.AUXILIARY_APPLICATION_NAME,
				Type = ApplicationType.AuxiliaryService,
				RedirectUri = Settings.Current.WebPortalHomeUri,
				Secret = secret,
				Addresses = ips
			};

			if (application == null)
				repository.Administration.Applications.Add(newApplication);
			else
				repository.Administration.Applications.Update(newApplication);
		}

		public void ValidateJsonpCallback(NameValueCollection headers, Application application)
		{
			string headerName = headers.AllKeys.FirstOrDefault(item => item == Constant.HTTP_REFERER_HEADER_NAME);

			if (string.IsNullOrEmpty(headerName))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Authorization.MISSING_JSONP_REFERER);

			string headerValue = headers[headerName];

			if (string.IsNullOrEmpty(headerValue))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Authorization.MISSING_JSONP_REFERER);

			if (application == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Authorization.JSONP_APPLICATION_NOT_FOUND);

			if (!application.EnableJsonp)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Authorization.JSONP_NOT_SUPPORTED);

			if (application.JsonpWebsites == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Authorization.INVALID_JSONP_SITE);

			if (!application.JsonpWebsites.Any(item => UriHelper.UriStartsWith(headerValue, item)))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Authorization.INVALID_JSONP_SITE);
		}

		#region Private Methods

		private void ValidateApplication(ApplicationRequest application)
		{
			if (application == null)
				throw new ServiceException(HttpStatusCode.BadRequest, "No application is specified in the message body.");

			switch (application.Type)
			{
				case ApplicationType.Public:
				case ApplicationType.Confidential:
					break;
				default:
					throw new ServiceException(HttpStatusCode.BadRequest, "Invalid application type.");
			}

			if (!application.EnableJsonp)
			{
				application.JsonpWebsites = null;
			}
			else if (application.JsonpWebsites != null)
			{
				for (int i = 0; i < application.JsonpWebsites.Count; i++)
				{
					string value = application.JsonpWebsites[i];

					if (string.IsNullOrEmpty(value) || UriHelper.IsValid(value))
						continue;

					throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Administration.INVALID_JSONP_WEBSITE);
				}
			}

			if (string.IsNullOrEmpty(application.Name))
				throw new ServiceException(HttpStatusCode.BadRequest, "Application name is required.");

			if (application.Name.Length > Constant.Application.MAXIMUM_NAME_LENGTH)
				throw new ServiceException(HttpStatusCode.BadRequest, "Application name cannot be longer than 100 characters.");

			Uri uri;
			if (!string.IsNullOrEmpty(application.RedirectUri) && !Uri.TryCreate(application.RedirectUri, UriKind.Absolute, out uri))
				throw new ServiceException(HttpStatusCode.BadRequest, "Redirect URI must be a valid absolute URI if specified.");
		}

		private Expression<Func<Application, bool>> ParseApplicationFilter(string filter)
		{
			FilterExpressionFactory factory = new FilterExpressionFactory();

			try
			{
				return factory.Create<Application>(filter);
			}
			catch (InvalidOperationException exception)
			{
				throw new ServiceException(HttpStatusCode.BadRequest, string.Format("The filter string is invalid. {0}", exception.Message));
			}
		}

		private List<string> GetPortalIpAddresses()
		{
			List<string> ips = new List<string>();

			string configuredIps = Settings.Current.WebPortalClientIps;

			if (string.IsNullOrEmpty(configuredIps))
				return ips;

			string[] split = configuredIps.Split(',');

			foreach (string ip in split)
			{
				string trimmed = ip.Trim();
				if (!string.IsNullOrEmpty(trimmed))
					ips.Add(trimmed);
			}

			return ips;
		}

		private string GetPortalApplicationSecret()
		{
			string secret = Settings.Current.WebPortalClientSecret;

			if (string.IsNullOrEmpty(secret))
				secret = Constant.DEFAULT_WEB_PORTAL_CLIENT_SECRET;

			return secret;
		}

		private List<string> GetAuxiliaryIpAddresses()
		{
			List<string> ips = new List<string>();

			string configuredIps = Settings.Current.WebPortalClientIps;

			if (string.IsNullOrEmpty(configuredIps))
				return ips;

			string[] split = configuredIps.Split(',');

			foreach (string ip in split)
			{
				string trimmed = ip.Trim();
				if (!string.IsNullOrEmpty(trimmed))
					ips.Add(trimmed);
			}

			return ips;
		}

		private string GetAuxiliaryApplicationSecret()
		{
			string secret = Settings.Current.AuxiliaryClientSecret;

			if (string.IsNullOrEmpty(secret))
				secret = Constant.DEFAULT_AUXILIARY_CLIENT_SECRET;

			return secret;
		}

		private bool ApplicationIsCurrent(Application application, ApplicationType type, string secret, string redirectUri, List<string> ips)
		{
			return application != null &&
				   application.Secret == secret &&
				   application.RedirectUri == redirectUri &&
				   application.Type == type &&
				   application.Addresses != null &&
				   ips.Count == application.Addresses.Count &&
				   ips.All(item => application.Addresses.Contains(item));
		}

		#endregion Private Methods
	}
}
