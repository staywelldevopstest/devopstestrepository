﻿using System.Collections.Generic;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logic.Framework;
using KswApi.Logic.Framework.Constants;
using KswApi.Logic.Framework.Helpers;
using KswApi.Repositories;
using KswApi.Repositories.Objects;
using System;
using System.Globalization;
using System.Linq;
using System.Net;

namespace KswApi.Logic.Administration
{
    public class ClientLogic
    {
        #region Public Methods

        public ClientList GetClients(int offset, int count, string query, string orderBy)
        {
            if (count > Constant.MAXIMUM_PAGE_SIZE)
                throw new ServiceException(HttpStatusCode.BadRequest, "Count is greater than the maximum page size.");

            if (count < 0)
                throw new ServiceException(HttpStatusCode.BadRequest, "Count is less than zero.");

            if (offset < 0)
                throw new ServiceException(HttpStatusCode.BadRequest, "Offset is less than zero.");

            Repository repository = new Repository();

            SortOption<Client> sortOption;

            if (string.IsNullOrEmpty(orderBy) || orderBy == "ClientName")
            {
                sortOption = SortOption<Client>.FromProperty(item => item.CaseInsensitiveName, item => item.ClientName);
            }
            else
            {
                SortParser<Client> sortParser = new SortParser<Client>();
				sortOption = sortParser.Parse(orderBy);
            }

            if (string.IsNullOrWhiteSpace(query))
            {
                return new ClientList
                {
                    Items = repository.Administration.Clients.GetByStatus(ClientStatus.Active, offset, count, sortOption).Select(CreateResponse).ToList(),
                    Offset = offset,
                    Total = repository.Administration.Clients.Count(ClientStatus.Active),
                    SortedBy = sortOption.ToString()
                };
            }

            return new ClientList
            {
				Items = repository.Administration.Clients.GetByQueryAndStatus(query, ClientStatus.Active, offset, count, sortOption).Select(CreateResponse).ToList(),
                Offset = offset,
                Total = repository.Administration.Clients.Count(query, ClientStatus.Active),
                SortedBy = sortOption.ToString()
            };
        }

		public ClientResponse GetClient(string clientId)
        {
            return CreateResponse(GetClientInternal(clientId));
        }

		public ClientResponse CreateClient(ClientRequest client)
        {
            ValidateInputClientData(client);

            Repository repository = new Repository();

            if (repository.Administration.Clients.Contains(client.ClientName, ClientStatus.Active))
                throw new ServiceException(HttpStatusCode.BadRequest, "The client name is a duplicate.");

            Client newClient = new Client
                                   {
                                       ClientName = client.ClientName,
                                       ClientWebSite = client.ClientWebSite,
                                       ContactEmail = client.ContactEmail,
                                       ContactName = client.ContactName,
                                       ContactPhone = client.ContactPhone,
                                       DateAdded = DateTime.UtcNow,
                                       Notes = client.Notes,
                                       Status = ClientStatus.Active,
									   CaseInsensitiveName = client.ClientName.ToLower()
                                   };

            repository.Administration.Clients.Add(newClient);

            return CreateResponse(newClient);
        }

		public ClientResponse UpdateClient(string clientId, ClientRequest client)
        {
            ValidateInputClientData(client);

            Repository repository = new Repository();

            if (string.IsNullOrEmpty(clientId))
                throw new ServiceException(HttpStatusCode.NotFound, "No client was specified.");

            Guid guid;
            if (!Guid.TryParse(clientId, out guid))
                throw new ServiceException(HttpStatusCode.BadRequest, "Invalid client ID.");

            Client existingClient = repository.Administration.Clients.GetById(guid);

            if (existingClient == null)
                throw new ServiceException(HttpStatusCode.NotFound, "Client doesn't exist.");

            if (existingClient.Status == ClientStatus.Deleted)
                throw new ServiceException(HttpStatusCode.BadRequest, "Client was deleted.");

            if (client.ClientName != existingClient.ClientName)
            {
                if (repository.Administration.Clients.Contains(client.ClientName, ClientStatus.Active))
                    throw new ServiceException(HttpStatusCode.BadRequest, "The client name is a duplicate.");
            }

            existingClient.ClientName = client.ClientName;
            existingClient.ClientWebSite = client.ClientWebSite;
            existingClient.ContactName = client.ContactName;
            existingClient.ContactEmail = client.ContactEmail;
            existingClient.ContactPhone = client.ContactPhone;
            existingClient.Notes = client.Notes;
	        existingClient.CaseInsensitiveName = client.ClientName.ToLower();

            repository.Administration.Clients.Update(existingClient);

            repository.Commit();

			return CreateResponse(existingClient);
        }

		public ClientResponse DeleteClient(string clientId)
        {
            if (string.IsNullOrWhiteSpace(clientId))
                throw new ServiceException(HttpStatusCode.BadRequest, "Invalid client Id");

            Repository repository = new Repository();

            Guid clientIdGuid;

            Guid.TryParse(clientId, out clientIdGuid);

            if (clientIdGuid == Guid.Empty)
                throw new ServiceException(HttpStatusCode.BadRequest, "Invalid client Id");

			Client client = GetClientInternal(clientId);

            if (client.Id == Guid.Empty)
                throw new ServiceException(HttpStatusCode.BadRequest, "Client doesn't exist");

			// Delete the client's users
			
			ClientUserLogic clientUserLogic = new ClientUserLogic();

			clientUserLogic.DeleteByClientId(client.Id);

            //Delete the client's licenses
            LicenseLogic licenseLogic = new LicenseLogic();

            if (client.Licenses != null)
            {
                foreach (Guid licenseId in client.Licenses)
                {
                    licenseLogic.DeleteClientLicense(licenseId, false);
                }
            }

            //Mark client as deleted as clients are soft deleted
            client.Status = ClientStatus.Deleted;

            repository.Administration.Clients.Update(client);

            repository.Commit();

			return CreateResponse(client);
        }

        private void ValidateInputClientData(ClientRequest client)
        {
            if (client == null)
                throw new ServiceException(HttpStatusCode.BadRequest, "The client was empty.");

            if (string.IsNullOrWhiteSpace(client.ClientName))
                throw new ServiceException(HttpStatusCode.BadRequest, "The client name was not specified.");

            if (!NameIsValid(client.ClientName))
                throw new ServiceException(HttpStatusCode.BadRequest, "The client name contains invalid characters.");

            if (client.ClientName.Length > 100)
                throw new ServiceException(HttpStatusCode.BadRequest, "The client name is too long.");

            if (!string.IsNullOrEmpty(client.ContactEmail) && !EmailHelper.IsValid(client.ContactEmail))
                throw new ServiceException(HttpStatusCode.BadRequest, "The client email is not valid.");

            if (!string.IsNullOrEmpty(client.ClientWebSite) && !UriHelper.IsValid(client.ClientWebSite))
                throw new ServiceException(HttpStatusCode.BadRequest, "The client website is not valid.");

        }

		private ClientResponse CreateResponse(Client client)
		{
			return new ClientResponse
				       {
					       ClientName = client.ClientName,
					       ClientWebSite = client.ClientWebSite,
					       ContactEmail = client.ContactEmail,
					       ContactName = client.ContactName,
					       ContactPhone = client.ContactPhone,
					       DateAdded = client.DateAdded,
					       Id = client.Id,
					       Licenses = client.Licenses,
					       Notes = client.Notes,
					       Status = client.Status
				       };
		}

		private Client GetClientInternal(string clientId)
        {
            if (string.IsNullOrWhiteSpace(clientId))
                throw new ServiceException(HttpStatusCode.BadRequest, "Invalid client Id");

            Guid clientIdGuid;

            Guid.TryParse(clientId, out clientIdGuid);

            if (clientIdGuid == Guid.Empty)
                throw new ServiceException(HttpStatusCode.BadRequest, "Invalid client Id");

            Repository repository = new Repository();

            Client result = repository.Administration.Clients.GetById(clientIdGuid);

            if (result == null)
                throw new ServiceException(HttpStatusCode.BadRequest, "Client doesn't exist");

			return result;
        }

        private bool NameIsValid(string name)
        {
            const string allowed = "01234567890abcdefghijklmnopqrstuvwxyz -.'_";
            return name.ToLower().All(c => allowed.Contains(c.ToString(CultureInfo.InvariantCulture)));
        }
        #endregion

    }
}
