﻿using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logic.Framework;
using KswApi.Logic.Framework.Constants;
using KswApi.Logic.SmsLogic;
using KswApi.Repositories;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

namespace KswApi.Logic.Administration
{
	public class LicenseLogic : LogicBase
	{
		public License GetLicense(string licenseId)
		{
			if (string.IsNullOrEmpty(licenseId))
				throw new ServiceException(HttpStatusCode.NotFound, "License ID was not specified.");

			Guid guid;
			if (!Guid.TryParse(licenseId, out guid))
				throw new ServiceException(HttpStatusCode.NotFound, "License ID is invalid.");

			License license = Repository.Administration.Licenses.GetById(guid);

			if (license == null)
				throw new ServiceException(HttpStatusCode.NotFound, "License not found.");

			return license;
		}

		public AvailableParentLicenseList GetAvailableParentLicenses(string clientId, string licenseId, string orderBy)
		{
			if (string.IsNullOrEmpty(clientId))
				throw new ServiceException(HttpStatusCode.BadRequest, "Client ID is required.");

			License license = null;
			if (!string.IsNullOrEmpty(licenseId))
			{
				LicenseLogic licenseLogic = new LicenseLogic();
				license = licenseLogic.GetLicense(licenseId);
			}

			ClientLogic clientLogic = new ClientLogic();
			ClientResponse client = clientLogic.GetClient(clientId);

			SortOption<License> sortOption;

			if (string.IsNullOrEmpty(orderBy))
			{
				sortOption = SortOption<License>.FromProperty(item => item.Name);
			}
			else
			{
				SortParser<License> sortParser = new SortParser<License>();

				sortOption = sortParser.Parse(orderBy);
			}

			

			return new AvailableParentLicenseList
			{
				Items = license == null ?
					Repository.Administration.Licenses.GetByClientIdAndStatus(client.Id, LicenseStatus.Active, sortOption).ToList() :
					Repository.Administration.Licenses.GetByClientIdAndStatus(client.Id, LicenseStatus.Active, sortOption).Where(item => item.Id != license.Id).ToList(),
			};
		}

		public LicenseList GetLicenses(int offset, int count, string query, string clientId, string orderBy)
		{
			if (count > Constant.MAXIMUM_PAGE_SIZE)
				throw new ServiceException(HttpStatusCode.BadRequest, "Count is greater than the maximum page size.");

			if (offset < 0)
				throw new ServiceException(HttpStatusCode.BadRequest, "Offset is less than zero.");

			ClientResponse client = null;
			if (!string.IsNullOrEmpty(clientId))
			{
				ClientLogic clientLogic = new ClientLogic();
				client = clientLogic.GetClient(clientId);
			}

			SortOption<License> sortOption;

			if (string.IsNullOrEmpty(orderBy))
			{
				sortOption = SortOption<License>.FromProperty(item => item.Name);
			}
			else
			{
				SortParser<License> sortParser = new SortParser<License>();

				sortOption = sortParser.Parse(orderBy);
			}

			

			List<License> items;
			int total;
			if (client != null)
			{
				if (!string.IsNullOrEmpty(query))
				{
					items = Repository.Administration.Licenses.GetByClientIdQueryAndStatus(client.Id, query, LicenseStatus.Active, offset, count, sortOption).ToList();
					total = Repository.Administration.Licenses.CountByClientIdQueryAndStatus(client.Id, query, LicenseStatus.Active);
				}
				else
				{
					items = Repository.Administration.Licenses.GetByClientIdAndStatus(client.Id, LicenseStatus.Active, sortOption).ToList();
					total = items.Count;
					items = items.Skip(offset).Take(count).ToList();
				}
			}
			else
			{
				if (!string.IsNullOrEmpty(query))
				{
					items = Repository.Administration.Licenses.GetByQueryAndStatus(query, LicenseStatus.Active, offset, count, sortOption).ToList();
					total = Repository.Administration.Licenses.CountByQueryAndStatus(query, LicenseStatus.Active);
				}
				else
				{
					items = Repository.Administration.Licenses.GetByStatus(LicenseStatus.Active, offset, count, sortOption).ToList();
					total = Repository.Administration.Licenses.CountByStatus(LicenseStatus.Active);
				}
			}

			return new LicenseList
			{
				Items = items,
				Offset = offset,
				Total = total,
				SortedBy = sortOption.ToString()
			};
		}

		public License GetLicenseByApplication(string applicationId)
		{
			if (string.IsNullOrWhiteSpace(applicationId))
				throw new ServiceException(HttpStatusCode.NotFound, "Application Id was not specified.");

			Guid applicationGuid;
			if (!Guid.TryParse(applicationId, out applicationGuid))
				throw new ServiceException(HttpStatusCode.BadRequest, "Application Id is invalid.");

			

			License license = Repository.Administration.Licenses.GetByApplicationId(applicationGuid);

			return license;
		}

		public License CreateLicense(LicenseRequest license)
		{
			ValidateLicense(license);

			

			Client client = Repository.Administration.Clients.GetById(license.ClientId);

			if (client == null)
				throw new ServiceException(HttpStatusCode.BadRequest, "Invalid client.");

			License parent = null;

			if (license.ParentLicenseId != null)
			{
				parent = Repository.Administration.Licenses.GetById(license.ParentLicenseId.Value);

				if (parent == null)
					throw new ServiceException(HttpStatusCode.BadRequest, "Invalid parent license.");

				if (parent.ParentLicenseId.HasValue)
					throw new ServiceException(HttpStatusCode.BadRequest, "A child license can not have children.");
			}

			License newLicense = new License
									 {
										 Name = license.Name,
										 Notes = license.Notes,
										 ClientId = license.ClientId,
										 ParentLicenseId = (license.ParentLicenseId == null || license.ParentLicenseId == Guid.Empty) ? null : license.ParentLicenseId,
										 CreatedDate = DateTime.UtcNow,
										 Status = LicenseStatus.Active
									 };

			Repository.Administration.Licenses.Add(newLicense);

			if (parent != null)
			{
				if (parent.Children == null)
					parent.Children = new List<Guid>();

				parent.Children.Add(newLicense.Id);

				Repository.Administration.Licenses.Update(parent);
			}

			if (client.Licenses == null)
				client.Licenses = new List<Guid>();

			client.Licenses.Add(newLicense.Id);

			Repository.Administration.Clients.Update(client);

			return newLicense;
		}

		public License UpdateLicense(string licenseId, LicenseRequest license)
		{
			Guid guid;
			if (!Guid.TryParse(licenseId, out guid))
				throw new ServiceException(HttpStatusCode.NotFound, "The license ID was not found.");

			ValidateLicense(license);

			

			License existingLicense = Repository.Administration.Licenses.GetById(guid);

			if (existingLicense == null)
				throw new ServiceException(HttpStatusCode.NotFound, string.Format("License was not found: {0}.", licenseId));

			if (license.ClientId != existingLicense.ClientId)
				throw new ServiceException(HttpStatusCode.BadRequest, "Cannot assign license to a different client.");

			Client client = Repository.Administration.Clients.GetById(license.ClientId);

			if (client == null)
				throw new ServiceException(HttpStatusCode.BadRequest, "Invalid client.");

			if (license.ParentLicenseId != null)
			{
				if (existingLicense.Children != null && existingLicense.Children.Count > 0)
					throw new ServiceException(HttpStatusCode.BadRequest, "A parent license can not have a parent.");

				License parent = Repository.Administration.Licenses.GetById(license.ParentLicenseId.Value);

				if (parent == null)
					throw new ServiceException(HttpStatusCode.BadRequest, "Invalid parent license.");

				if (parent.ParentLicenseId.HasValue)
					throw new ServiceException(HttpStatusCode.BadRequest, "A child license can not have children.");

				if (parent.Children == null)
					parent.Children = new List<Guid>();

				if (!parent.Children.Contains(existingLicense.Id))
				{
					parent.Children.Add(existingLicense.Id);
					Repository.Administration.Licenses.Update(parent);
				}
			}
			else if (existingLicense.ParentLicenseId != null)
			{
				// this license is changing from a child to having no parent
				// we have to update the parent

				License parent = Repository.Administration.Licenses.GetById(existingLicense.ParentLicenseId.Value);

				if (parent != null && parent.Children != null)
				{
					parent.Children.Remove(existingLicense.Id);

					if (parent.Children.Count == 0)
						parent.Children = null;

					Repository.Administration.Licenses.Update(parent);
				}
			}

			existingLicense.Name = license.Name;
			existingLicense.Notes = license.Notes;
			existingLicense.ParentLicenseId = (license.ParentLicenseId == null || license.ParentLicenseId == Guid.Empty)
												  ? null
												  : license.ParentLicenseId;

			Repository.Administration.Licenses.Update(existingLicense);

			return existingLicense;
		}

		public License DeleteLicense(string licenseId)
		{
			//NOTE: We are soft-deleting licenses (including child licenses)

			Guid guid;
			if (!Guid.TryParse(licenseId, out guid))
				throw new ServiceException(HttpStatusCode.NotFound, "The license ID was not found.");

			License license = DeleteClientLicense(guid);

			if (license == null)
				throw new ServiceException(HttpStatusCode.NotFound, "The license ID was not found.");

			return license;
		}

		/// <summary>
		/// This is used by client logic to cascade deletes, so it needs to be public
		/// </summary>
		/// <returns>
		/// License on success, null on failure
		/// </returns>
		public License DeleteClientLicense(Guid licenseId, bool updateClient = true)
		{
			License license = Repository.Administration.Licenses.GetById(licenseId);

			// don't throw an exception here, it can
			// put client deletion into a state where it can't be deleted
			if (license == null)
				return null;

			Client client = null;

			if (updateClient)
			{
				client = Repository.Administration.Clients.GetById(license.ClientId);

				if (client == null)
					throw new ServiceException(HttpStatusCode.BadRequest, "Invalid client.");
			}

			//Delete children licenses again they should be soft deleted
			if (license.Children != null)
			{
				foreach (Guid child in license.Children)
					DeleteChildLicense(child, client);

				//Set the children list to null
				license.Children = null;
			}

			//If it is a child license we need to get the parent license and remove itself from the ParentLicense.Children list.
			if (license.ParentLicenseId != null)
			{
				License parentLicense = Repository.Administration.Licenses.GetById(license.ParentLicenseId.Value);

				if (parentLicense.Children.Contains(licenseId))
				{
					parentLicense.Children.Remove(licenseId);

					Repository.Administration.Licenses.Update(parentLicense);
				}
			}

			//This deletes all the modules, keywords, numbers associated with the license
			DeleteLicenseExtras(license);

			//This is a soft delete
			license.Status = LicenseStatus.Inactive;
			license.Modules = null;

			Repository.Administration.Licenses.Update(license);

			if (updateClient)
			{
				//Remove them from the client as the license has a reference to the client

				if (client.Licenses != null)
					client.Licenses.Remove(license.Id);

				Repository.Administration.Clients.Update(client);
			}

			return license;
		}

		public ClientLicenseDetail GetClientLicenseDetail(string clientId, string query)
		{
			Guid clientGuid;

			if (string.IsNullOrEmpty(clientId))
				throw new ServiceException(HttpStatusCode.BadRequest, "No client was specified.");

			if (!Guid.TryParse(clientId, out clientGuid))
				throw new ServiceException(HttpStatusCode.BadRequest, "Invalid client ID.");

			

			Client client = Repository.Administration.Clients.GetById(clientGuid);

			if (client == null || client.Status != ClientStatus.Active)
				throw new ServiceException(HttpStatusCode.NotFound, "Client was not found.");

			SortOption<License> sortOption = SortOption<License>.FromProperty(item => item.Name);

			IEnumerable<License> licenses = Repository.Administration.Licenses.GetByClientIdAndStatus(clientGuid, LicenseStatus.Active, sortOption);

			return new ClientLicenseDetail
			{
				ClientId = clientGuid,
				ClientName = client.ClientName,
				Licenses = (string.IsNullOrWhiteSpace(query)
					? GetUnfilteredClientLicenseDetails(licenses)
					: GetFilteredClientLicenseDetails(licenses, query)),
				LicenseCount = Repository.Administration.Licenses.CountByClientIdAndStatus(clientGuid, LicenseStatus.Active)
			};
		}

		public Application CreateLicenseApplication(string licenseId, ApplicationRequest application)
		{
			License license = GetLicense(licenseId);

			ApplicationLogic applicationLogic = new ApplicationLogic();

			Application newApplication = applicationLogic.CreateApplication(license.Id, application);

			if (license.Applications == null)
				license.Applications = new List<Guid>();

			license.Applications.Add(newApplication.Id);

			

			Repository.Administration.Licenses.Update(license);

			return newApplication;
		}

		public ApplicationList GetLicenseApplications(string licenseId)
		{
			LicenseLogic licenseLogic = new LicenseLogic();
			License license = licenseLogic.GetLicense(licenseId);

			ApplicationList list = new ApplicationList();

			if (license.Applications == null || license.Applications.Count == 0)
				return list;

			list.Items = new List<Application>();

			

			List<Application> applications = new List<Application>();

			foreach (Guid guid in license.Applications)
			{
				Application application = Repository.Administration.Applications.GetById(guid);
				if (application != null)
					applications.Add(application);
			}

			list.Total = applications.Count;
			list.Offset = 0;
			list.SortedBy = SortOption<Application>.FromProperty(item => item.Name).ToString();
			list.Items = applications.OrderBy(item => item.Name).ToList();
			return list;
		}

		public Application GetLicenseApplication(string licenseId, string applicationId)
		{
			License license = GetLicense(licenseId);

			ApplicationLogic applicationLogic = new ApplicationLogic();

			Application application = applicationLogic.GetApplication(applicationId);

			if (license.Applications == null || !license.Applications.Contains(application.Id))
				throw new ServiceException(HttpStatusCode.NotFound, "The license does not contain the specified application.");

			return application;
		}

		public Application DeleteLicenseApplication(string licenseId, string applicationId)
		{
			Guid guid;
			if (!Guid.TryParse(applicationId, out guid))
				throw new ServiceException(HttpStatusCode.NotFound, "The application ID was not found.");

			Application application = Repository.Administration.Applications.GetById(guid);

			if (application == null)
				throw new ServiceException(HttpStatusCode.NotFound, string.Format("Application was not found: {0}.", applicationId));

			License license = GetLicense(licenseId);

			if (license.Applications != null)
				license.Applications.Remove(application.Id);

			Repository.Administration.Licenses.Update(license);

			Repository.Administration.Applications.Delete(application);

			return application;
		}

		public Application UpdateLicenseApplication(string licenseId, string applicationId, ApplicationRequest application)
		{
			License license = GetLicense(licenseId);

			ApplicationLogic applicationLogic = new ApplicationLogic();

			return applicationLogic.UpdateApplication(license.Id, applicationId, application);
		}

		public Application ResetLicenseApplicationSecret(string licenseId, string applicationId)
		{
			License license = GetLicense(licenseId);

			ApplicationLogic applicationLogic = new ApplicationLogic();

			return applicationLogic.ResetApplicationSecret(license.Id, applicationId);
		}

		#region Private Methods

		private List<LicenseDetail> GetUnfilteredClientLicenseDetails(IEnumerable<License> licenses)
		{
			List<License> children = new List<License>();

			SortedList<Guid, LicenseDetail> parents = new SortedList<Guid, LicenseDetail>();

			foreach (License license in licenses)
			{
				if (license.ParentLicenseId.HasValue)
					children.Add(license);
				else
				{
					parents[license.Id] = new LicenseDetail
					{
						Id = license.Id,
						Name = license.Name,
						Applications = GetApplicationDetails(license)
					};
				}
			}

			// sort child licenses by name
			children.Sort((first, second) => first.Name.CompareTo(second.Name));

			foreach (License child in children)
			{
				LicenseDetail parent;

				if (parents.TryGetValue(child.ParentLicenseId.Value, out parent))
				{
					if (parent.ChildLicenses == null)
						parent.ChildLicenses = new List<LicenseDetail>();

					parent.ChildLicenses.Add(new LicenseDetail
					{
						Id = child.Id,
						Name = child.Name,
						Applications = GetApplicationDetails(child)
					});
				}
			}

			return parents.Values.OrderBy(item => item.Name).ToList();
		}

		private List<ApplicationDetail> GetApplicationDetails(License license)
		{
			if (license.Applications == null || license.Applications.Count == 0)
				return null;

			List<ApplicationDetail> list = new List<ApplicationDetail>();

			foreach (Guid guid in license.Applications)
			{
				Application application = Repository.Administration.Applications.GetById(guid);
				if (application != null)
					list.Add(new ApplicationDetail
						{
							Id = application.Id,
							Name = application.Name,
							Secret = application.Secret
						});
			}

			return list;
		}

		private List<LicenseDetail> GetFilteredClientLicenseDetails(IEnumerable<License> licenses, string query)
		{
			Dictionary<Guid, License> parents = new Dictionary<Guid, License>();

			Dictionary<Guid, LicenseDetail> list = new Dictionary<Guid, LicenseDetail>();

			List<License> children = new List<License>();

			foreach (License license in licenses)
			{


				if (license.ParentLicenseId.HasValue)
				{
					if (license.Name.IndexOf(query, StringComparison.OrdinalIgnoreCase) >= 0)
						children.Add(license);
				}
				else
				{
					parents[license.Id] = license;
					if (license.Name.IndexOf(query, StringComparison.OrdinalIgnoreCase) >= 0)
						list[license.Id] = new LicenseDetail
							{
								Id = license.Id,
								Name = license.Name,
								Applications = GetApplicationDetails(license)
							};
				}
			}

			foreach (License child in children)
			{
				LicenseDetail detail;
				if (!list.TryGetValue(child.ParentLicenseId.Value, out detail))
				{
					License parent;
					if (!parents.TryGetValue(child.ParentLicenseId.Value, out parent))
						continue;

					detail = new LicenseDetail
						{
							Id = parent.Id,
							Name = parent.Name,
							Applications = GetApplicationDetails(parent)
						};

					if (detail.ChildLicenses == null)
						detail.ChildLicenses = new List<LicenseDetail>();

					detail.ChildLicenses.Add(new LicenseDetail
					{
						Id = child.Id,
						Name = child.Name,
						Applications = GetApplicationDetails(child)
					});

					list[parent.Id] = detail;
				}
			}

			return list.Values.OrderBy(item => item.Name).ToList();
		}

		private void ValidateLicense(LicenseRequest license)
		{
			if (license == null)
				throw new ServiceException(HttpStatusCode.BadRequest, "No license is specified in the message body.");

			if (string.IsNullOrEmpty(license.Name))
				throw new ServiceException(HttpStatusCode.BadRequest, "License name is required.");

			if (license.Name.Length > 100)
				throw new ServiceException(HttpStatusCode.BadRequest, "License name cannot be longer than 100 characters.");

			if (license.ClientId == default(Guid))
				throw new ServiceException(HttpStatusCode.BadRequest, "A license must be assigned to a client.");
		}

		// this recursively deletes all licenses
		private void DeleteChildLicense(Guid licenseGuid, Client client)
		{
			License license = Repository.Administration.Licenses.GetById(licenseGuid);

			//Instead of throwing an exception we are just going to get rid of this license to make sure we don't cause
			//  problems later on in the process.
			if (license != null)
			{
				//This is a soft delete
				license.Status = LicenseStatus.Inactive;
				license.Modules = null;

				//This deletes all the modules, keywords, numbers associated with the license
				DeleteLicenseExtras(license);

				Repository.Administration.Licenses.Update(license);
			}

			if (client != null && client.Licenses != null)
				client.Licenses.Remove(licenseGuid);
		}

		private void DeleteLicenseExtras(License license)
		{
			Repository.Administration.Applications.DeleteByLicenseId(license.Id);

			Repository.Content.LicensedCollections.DeleteByLicenseId(license.Id);

			if (license.Modules != null)
			{
				foreach (ModuleType moduleType in license.Modules)
				{
					//Remove the modules, keywords and numbers.  These are hard-deleted

					switch (moduleType)
					{
						case ModuleType.SmsGateway:
							SmsAdministrationLogic smsAdminLogic = new SmsAdministrationLogic();
							smsAdminLogic.DeleteModuleForLicense(license);
							break;
						case ModuleType.Content:
						case ModuleType.ManageContent:
							break;
						default:
							throw new ArgumentOutOfRangeException();
					}

				}
			}
		}

		#endregion Private Methods

	}
}
