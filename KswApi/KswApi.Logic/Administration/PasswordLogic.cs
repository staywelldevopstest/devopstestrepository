﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using KswApi.Common;
using KswApi.Common.Configuration;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logging;
using KswApi.Logic.Framework.Constants;
using KswApi.Logic.Framework.Helpers;
using KswApi.Poco.Enums;
using KswApi.Repositories;
using KswApi.Repositories.Objects;

namespace KswApi.Logic.Administration
{
	public class PasswordLogic
	{
		private static char[] _nameSeparatorCharacters = { ' ', '@', '.', '\'', '"', '-', '_', '&', '~', '/', ','};
		#region Public Methods

		public void ValidatePasswordToken(string token)
		{
			if (string.IsNullOrEmpty(token))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Passwords.INVALID_TOKEN);

			Repository repository = new Repository();

			PasswordToken passwordToken = repository.Administration.PasswordTokens.GetByToken(token);

			if (passwordToken == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Passwords.INVALID_TOKEN);

			ClientUser user = repository.Administration.ClientUsers.GetUser(passwordToken.UserId);

			ValidateUser(user);
		}

		public void SetClientUserPassword(PasswordRequest request)
		{
			ValidateRequest(request);

			Repository repository = new Repository();

			PasswordToken passwordToken = repository.Administration.PasswordTokens.GetByToken(request.Token);

			if (passwordToken == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Passwords.INVALID_TOKEN);

			ClientUser user = repository.Administration.ClientUsers.GetUser(passwordToken.UserId);

			ValidateUser(user);

			ValidateNewPassword(request.Password, user);

			Random random = new Random();

			int saltLength = random.Next(Constant.Passwords.MINIMUM_SALT_LENGTH, Constant.Passwords.MAXIMUM_SALT_LENGTH);

			byte[] salt = new byte[saltLength];

			RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
			rng.GetNonZeroBytes(salt);

			user.PasswordSalt = salt;

			user.PasswordHash = HashPassword(request.Password, salt);

			// clear locked status
			user.LockedTime = null;
			user.RecentFailedLogins = null;

			repository.Administration.ClientUsers.Update(user);

			repository.Administration.PasswordTokens.DeleteById(passwordToken.Id);
		}

		public bool ValidatePassword(string password, ClientUser user)
		{
			if (user.PasswordSalt == null || user.PasswordHash == null)
				return false;

			byte[] hash = HashPassword(password, user.PasswordSalt);

			return user.PasswordHash.SequenceEqual(hash);
		}

		public void CreateClientUserPassword(ClientUser user)
		{
			CreatePasswordRequest(user, Settings.Current.EmailMessages.SetPassword);
		}

		public void ResetClientUserPassword(PasswordResetRequest request)
		{
			if (request == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.MISSING_MESSAGE_BODY);

			if (string.IsNullOrEmpty(request.Email))
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Administration.MISSING_EMAIL);

			if (!EmailHelper.IsValid(request.Email))
				throw new ServiceException(HttpStatusCode.NotFound, ErrorMessage.Administration.INVALID_EMAIL);

			Repository repository = new Repository();

			ClientUser user = repository.Administration.ClientUsers.GetUserByEmail(request.Email);

			// this is intentional, per HIPAA we can't give an indication whether the email
			// address is valid or invalid
			if (user == null)
				return;

			if (user.State != ClientUserState.Active)
				return;

			CreatePasswordRequest(user, Settings.Current.EmailMessages.ResetPassword);
		}

		public void CreatePasswordRequest(ClientUser user, EmailMessageConfiguration message)
		{
			Repository repository = new Repository();

			PasswordToken token = new PasswordToken
			{
				Id = Guid.NewGuid(),
				Token = TokenGenerator.CreateToken(),
				DateAdded = DateTime.UtcNow,
				UserId = user.Id,
				UserType = UserType.ClientUser
			};

			IEnumerable<PasswordToken> tokens = repository.Administration.PasswordTokens.GetByUserId(user.Id);
			foreach (PasswordToken oldToken in tokens)
				repository.Administration.PasswordTokens.DeleteById(oldToken.Id);

			repository.Administration.PasswordTokens.Add(token);

			// send email takes a while: run async for now,
			// consider using the aux service

			System.Threading.Tasks.Task task = new System.Threading.Tasks.Task(() => SendEmail(user.EmailAddress, token, message));
			task.Start();
		}

		#endregion

		#region Private Methods

		private void ValidateUser(ClientUser user)
		{
			if (user == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Passwords.INVALID_TOKEN);

			if (user.State == ClientUserState.Disabled)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Passwords.DISABLED_USER);

			if (user.State != ClientUserState.Active)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Passwords.INVALID_TOKEN);
		}

		private byte[] HashPassword(string password, byte[] salt)
		{
			byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

			byte[] combinedPassword = new byte[passwordBytes.Length + salt.Length];

			Array.Copy(passwordBytes, combinedPassword, passwordBytes.Length);
			Array.Copy(salt, 0, combinedPassword, passwordBytes.Length, salt.Length);

			SHA256 sha = SHA256.Create();

			return sha.ComputeHash(combinedPassword);	
		}

		private void SendEmail(string emailAddress, PasswordToken token, EmailMessageConfiguration messageConfiguration)
		{
			string uri = UriHelper.Combine(Settings.Current.WebPortalBaseUri, "User/Password");
			uri = UriHelper.AddQuery(uri, new NameValueCollection { { "token", token.Token } });

			MailMessage message = new MailMessage
								  {
									  From = new MailAddress(messageConfiguration.From),
									  Subject = messageConfiguration.Subject,
									  Body = string.Format(messageConfiguration.Body, uri)
								  };

			message.To.Add(emailAddress);

			SmtpClient client = new SmtpClient
			{
				Host = Settings.Current.SmtpServer
			};

			client.SendCompleted += OnSmtpSendCompleted;

			client.SendAsync(message, new SmtpData { Client = client, Message = message });
		}

		private void OnSmtpSendCompleted(object sender, AsyncCompletedEventArgs asyncCompletedEventArgs)
		{
			if (asyncCompletedEventArgs.Error != null)
				Logger.Log(asyncCompletedEventArgs.Error);
			else if (asyncCompletedEventArgs.Cancelled)
				Logger.Log(new IOException("SMTP Send was cancelled."));

			SmtpData data = asyncCompletedEventArgs.UserState as SmtpData;

			if (data != null)
			{
				data.Client.Dispose();
				data.Message.Dispose();
			}
		}

		private void ValidateRequest(PasswordRequest request)
		{
			if (request == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.MISSING_MESSAGE_BODY);

			if (string.IsNullOrEmpty(request.Token))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Passwords.MISSING_TOKEN);

			if (string.IsNullOrEmpty(request.Password))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Passwords.MISSING_PASSWORD);

			if (string.IsNullOrEmpty(request.PasswordConfirmation))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Passwords.MISSING_PASSWORD_CONFIRMATION);

			if (!string.Equals(request.Password, request.PasswordConfirmation))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Passwords.PASSWORD_AND_CONFIRMATION_NOT_EQUAL);
		}

		private void ValidateNewPassword(string password, ClientUser user)
		{
			if (password.Length < Constant.Administration.MINIMUM_PASSWORD_LENGTH)
				throw new ServiceException(HttpStatusCode.BadRequest, string.Format(ErrorMessage.Passwords.PASSWORD_LENGTH_REQUIREMENTS, Constant.Administration.MINIMUM_PASSWORD_LENGTH));

			if (!password.Any(char.IsUpper))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Passwords.PASSWORD_CHARACTER_REQUIREMENTS);

			if (!password.Any(char.IsLower))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Passwords.PASSWORD_CHARACTER_REQUIREMENTS);

			if (!password.Any(char.IsDigit))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Passwords.PASSWORD_CHARACTER_REQUIREMENTS);

			// look for symbols
			if (password.All(char.IsLetterOrDigit))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Passwords.PASSWORD_CHARACTER_REQUIREMENTS);

			string[] split = user.EmailAddress.Split(_nameSeparatorCharacters);

			if (split.Any(item => !string.IsNullOrEmpty(item) && password.IndexOf(item, StringComparison.OrdinalIgnoreCase) >= 0))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Passwords.PASSWORD_CONTAINING_EMAIL);

			List<string> names = new List<string> {user.FirstName, user.LastName};
			names = names.Where(item => !string.IsNullOrEmpty(item)).SelectMany(item => item.Split(_nameSeparatorCharacters)).ToList();

			if (names.Any(item => !string.IsNullOrEmpty(item) && password.IndexOf(item, StringComparison.OrdinalIgnoreCase) >= 0))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Passwords.PASSWORD_CONTAINING_NAME);
		}

		private class SmtpData
		{
			public SmtpClient Client { get; set; }
			public MailMessage Message { get; set; }
		}

		#endregion
	}
}
