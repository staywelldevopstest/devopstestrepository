﻿using KswApi.Common.Configuration;
using KswApi.Common.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logic.Framework;
using KswApi.Repositories;
using System;
using System.Collections.Generic;
using System.Net;

namespace KswApi.Logic.Administration
{
	public class RoleLogic
	{
		#region Public Methods

		public RightList GetAllRights()
		{
			return new RightList
					   {
						   Items = RightManager.GetRights()
					   };
		}

		public RoleList GetAllRoles()
		{
			Repository repository = new Repository();

			return new RoleList
					   {
						   Items = new List<Role>(repository.Administration.Roles.GetAll())
					   };
		}

		public Role GetRole(string roleId)
		{
			if (string.IsNullOrEmpty(roleId))
				throw new ServiceException(HttpStatusCode.NotFound, "No role ID was specified");

			Repository repository = new Repository();

			Guid guid;

			Role role = Guid.TryParse(roleId, out guid)
				? repository.Administration.Roles.GetById(guid)
				: repository.Administration.Roles.GetByName(roleId);

			if (role == null)
				throw new ServiceException(HttpStatusCode.NotFound, "The specified role ID does not exist.");

			return role;
		}

		public Role CreateRole(RoleRequest role)
		{
			ValidateInputRole(role);

			Repository repository = new Repository();

			if (repository.Administration.Roles.Contains(role.Name))
				throw new ServiceException(HttpStatusCode.BadRequest, "The role name is a duplicate.");

			Role newRole = new Role
							   {
								   Name = role.Name,
								   Description = role.Description,
								   Rights = role.Rights
							   };

			repository.Administration.Roles.Add(newRole);

			repository.Commit();

			return newRole;
		}

		public Role UpdateRole(string roleId, RoleRequest role)
		{
			if (string.IsNullOrEmpty(roleId))
				throw new ServiceException(HttpStatusCode.NotFound, "No role ID was specified");

			ValidateInputRole(role);

			Role existingRole = GetRole(roleId);

			existingRole.Name = role.Name;
			existingRole.Description = role.Description;
			existingRole.Rights = role.Rights;

			Repository repository = new Repository();

			repository.Administration.Roles.Update(existingRole);

			repository.Commit();

			return existingRole;
		}

		public Role DeleteRole(string roleId)
		{
			if (string.IsNullOrWhiteSpace(roleId))
				throw new ServiceException(HttpStatusCode.NotFound, "No role ID was specified");


			Role role = GetRole(roleId);

			Repository repository = new Repository();

			repository.Administration.Roles.Delete(role);

			repository.Commit();

			return role;
		}

		public void EnsureRoles()
		{
			string prefix;

			switch (Settings.Current.Environment)
			{
				case EnvironmentName.EA:
					prefix = "EA";
					break;
				case EnvironmentName.Staging:
					prefix = "STG";
					break;
				case EnvironmentName.Production:
					prefix = "PRD";
					break;
				default:
					prefix = "DEV";
					break;
			}

			EnsureAdminRole(prefix);
			EnsureBrowserRole(prefix);
			EnsureContentManagerRole(prefix);
			EnsureIndexerRole(prefix);
			EnsureEditorRole(prefix);
		}

		#endregion

		#region Private Methods

		private void EnsureRole(string prefix, string name, string description, List<string> rights)
		{
			string roleName = prefix + '_' + name;

			Repository repository = new Repository();

			Role role = repository.Administration.Roles.GetByName(roleName);

			if (role != null)
				return;

			role = new Role
			{
				Name = roleName,
				Description = description,
				Rights = rights
			};

			repository.Administration.Roles.Add(role);
		}

		private void EnsureAdminRole(string prefix)
		{
			EnsureRole(prefix, "Admin", "Administration Role", new List<string>
	                                                           {
		                                                           "Read_Client",
		                                                           "Manage_Client",
		                                                           "Read_Roles",
		                                                           "Manage_Roles",
		                                                           "Read_License",
		                                                           "Manage_License",
		                                                           "Read_Logs",
		                                                           "Manage_Content",
		                                                           "Read_Content",
		                                                           "Read_Reports",
		                                                           "Manage_ClientUsers",
		                                                           "Read_ClientUsers",
		                                                           "Manage_Administration"

	                                                           });
		}

		private void EnsureBrowserRole(string prefix)
		{
			EnsureRole(prefix, "Browser", "Browser Role", new List<string>
			                                              {
				                                              "Read_Client",
				                                              "Read_Roles",
				                                              "Read_License",
				                                              "Read_Logs",
				                                              "Read_Content",
				                                              "Read_ClientUsers",
				                                              "Read_Reports"
			                                              });
		}

		private void EnsureContentManagerRole(string prefix)
		{
			EnsureRole(prefix, "ContentManager", "Content Manager Role", new List<string>
			                                                             {
				                                                             "Read_Roles",
				                                                             "Read_Logs",
				                                                             "Read_Reports",
				                                                             "Read_Content",
				                                                             "Manage_Content"
			                                                             });
		}

		private void EnsureEditorRole(string prefix)
		{
			EnsureRole(prefix, "Editor", "Content Manager Role", new List<string>
			                                                     {
				                                                     "Read_Client",
				                                                     "Manage_Client"
			                                                     });
		}

		private void EnsureIndexerRole(string prefix)
		{
			EnsureRole(prefix, "Editor", "Content Manager Role", new List<string>
					{
						"Read_Roles"
					});
		}

		private void ValidateInputRole(RoleRequest role)
		{
			if (role == null)
				throw new ServiceException(HttpStatusCode.BadRequest, "The role was empty.");

			if (string.IsNullOrEmpty(role.Name))
				throw new ServiceException(HttpStatusCode.BadRequest, "The role did not specify a name.");

			if (role.Rights != null)
			{
				List<string> rights = RightManager.GetRights();

				foreach (string right in role.Rights)
				{
					if (!rights.Contains(right))
						throw new ServiceException(HttpStatusCode.BadRequest, string.Format("The list of rights contains an invalid right: {0}.", right));
				}
			}
		}

		#endregion
	}
}
