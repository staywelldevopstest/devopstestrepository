﻿using System;
using System.Net;
using KswApi.Common.Objects;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.Framework.Constants;
using KswApi.Poco.Administration;
using KswApi.Repositories;

namespace KswApi.Logic.Administration
{
    internal class DomainUserLogic
    {
        internal void Upsert(UserDetails details)
        {
            if(details == null)
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.MISSING_MESSAGE_BODY);

            DomainUser user = new DomainUser
                                  {
                                      Id = details.Id,
                                      FirstName = details.FirstName,
                                      LastName = details.LastName,
                                      Username = details.EmailAddress
                                  };

            Repository repository = new Repository();
            if(repository.Administration.DomainUsers.GetById(details.Id) != null)
                repository.Administration.DomainUsers.Update(user);
            else
                repository.Administration.DomainUsers.Add(user);

        }

        internal UserDetailsResponse UserDetailsResponseFromDomainUser(Guid id)
        {
            Repository repository = new Repository();
            DomainUser user =  repository.Administration.DomainUsers.GetById(id);
            if (user == null)
                throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.User.USER_NOT_FOUND);

            return new UserDetailsResponse
                       {
                           Id = user.Id,
                           FullName = string.Join(" ", new[] { user.FirstName, user.LastName }).Trim(),
                           Type = UserType.DomainUser
                       };
        }
    }
}
