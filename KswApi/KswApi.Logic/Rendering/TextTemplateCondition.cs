﻿using System;
using System.Text.RegularExpressions;
using System.Web;
using KswApi.Interface.Exceptions;

namespace KswApi.Logic.Rendering
{
	internal class TextTemplateCondition
	{
		public string Value { get; private set; }
		public int Index { get; private set; }
		public int Length { get; private set; }
		public string Condition { get; private set; }
		public string FirstExpression { get; private set; }
		public string SecondExpression { get; private set; }

		public TextTemplateCondition(string value, int index, int length)
		{
			Value = value;
			Index = index;
			Length = length;
			FindSegments();
		}

		private void FindSegments()
		{
			Match match = Regex.Match(Value, @"{{if(?:&nbsp;|\s)+([^}]+)}}", RegexOptions.Compiled | RegexOptions.IgnoreCase);
			if (!match.Success)
				throw new RenderingException("Unable to find the 'if' condition.");

			int elseIndex = Value.IndexOf("{{else}}", StringComparison.OrdinalIgnoreCase);
			int endIfIndex = Value.IndexOf("{{/if}}", StringComparison.OrdinalIgnoreCase);

			// remove HTML characters in the condition
			Condition = HttpUtility.HtmlDecode(match.Groups[1].Value);

			if (elseIndex != -1)
			{
				FirstExpression = Value.Substring(match.Index + match.Length, elseIndex - (match.Index + match.Length));
				SecondExpression = Value.Substring(elseIndex + 8, endIfIndex - (elseIndex + 8));
			}
			else
			{
				FirstExpression = Value.Substring(match.Index + match.Length, endIfIndex - (match.Index + match.Length));
			}
		}
	}
}