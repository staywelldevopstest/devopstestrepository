﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using DynamicExpresso;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects.Rendering;

namespace KswApi.Logic.Rendering
{
	public class TextTemplate
	{
		private readonly Interpreter _interpreter = new Interpreter();
		private readonly IDictionary<string, Lambda> _lambdas = new Dictionary<string, Lambda>();

		public string Resolve(string input, IList<TemplateField> fields)
		{
			StringBuilder stringBuilder = new StringBuilder(input);
			foreach (TemplateField field in fields)
				stringBuilder.Replace("{{" + field.Name + "}}", field.Value.ToString());

			string rval = stringBuilder.ToString();

			rval = ResolveDecisionTemplates(rval, fields);

			// look for templatable fields that didn't have values provided
			MatchCollection matches = Regex.Matches(rval, @"{{(\w+)}}");
			if (matches.Count > 0)
			{
				string[] names = (from Match match in matches select match.Groups[1].Value).ToArray();
				throw new ServiceException(HttpStatusCode.BadRequest,
					string.Format("Value not provided for field(s): {0}.", string.Join(", ", names)));
			}

			return rval;
		}

		private string ResolveDecisionTemplates(string input, IList<TemplateField> fields)
		{
			// remove any surrounding whitespace around the templates
			string input2 = input;
			input2 = Regex.Replace(input2, @"}}(?:&nbsp;|\s)*{{", "}}{{", RegexOptions.Compiled);
			input2 = Regex.Replace(input2, @"({{if(?:&nbsp;|\s)[^}]+}})(?:&nbsp;|\s)*", "$1", RegexOptions.Compiled | RegexOptions.IgnoreCase);
			input2 = Regex.Replace(input2, @"(?:&nbsp;|\s)*({{else}})(?:&nbsp;|\s)*", "$1", RegexOptions.Compiled | RegexOptions.IgnoreCase);
			input2 = Regex.Replace(input2, @"(?:&nbsp;|\s)*({{/if}})", "$1", RegexOptions.Compiled | RegexOptions.IgnoreCase);
			StringBuilder output = new StringBuilder(input2);

			while (true)
			{
				TextTemplateCondition textTemplateCondition = ReturnFirstNonEmbeddedCondition(output.ToString());
				if (textTemplateCondition == null)
					return output.ToString();

				string key = textTemplateCondition.Value;

				// use an existing parsed lambda if one exists, otherwise create

				// convert enum values to string
				Parameter[] parameters = fields.Select(field => field.Type.IsEnum
					? new Parameter(field.Name, typeof(string), field.Value.ToString())
					: new Parameter(field.Name, field.Type, field.Value)).ToArray();

				Lambda lambda;
				if (!_lambdas.TryGetValue(key, out lambda))
				{
					// convert the template expression to a C#-style conditional operator
					string expression = string.Format("{0} ? {1} : {2}",
						textTemplateCondition.Condition,
						EncodeAsString(textTemplateCondition.FirstExpression),
						textTemplateCondition.SecondExpression != null
							? EncodeAsString(textTemplateCondition.SecondExpression)
							: "string.Empty");

					//Debug.WriteLine(expression);
					lambda = _interpreter.Parse(expression, parameters);
					_lambdas.Add(key, lambda);
				}

				string result = (string)lambda.Invoke(parameters);
				output.Replace(textTemplateCondition.Value, result, textTemplateCondition.Index, textTemplateCondition.Length);
			}
		}

		private static string EncodeAsString(string input)
		{
			return string.Format("\"{0}\"", input.Replace("\"", "\\\""));
		}

		private TextTemplateCondition ReturnFirstNonEmbeddedCondition(string input)
		{
			int startIndex = 0;

			while (true)
			{
				startIndex = input.IndexOf("{{if", startIndex, StringComparison.OrdinalIgnoreCase);
				if (startIndex == -1)
					return null;

				int endIndex = input.IndexOf("{{/if}}", startIndex + 4, StringComparison.OrdinalIgnoreCase);
				if (startIndex == -1)
					return null;
				endIndex += 7;

				string expression = input.Substring(startIndex, endIndex - startIndex);

				// test for an embedded expression
				int nextIfStatement = input.IndexOf("{{if", startIndex + 4, StringComparison.OrdinalIgnoreCase);
				if (nextIfStatement >= startIndex && nextIfStatement <= endIndex)
				{
					// if there is an embedded, move to the next if occurrence
					startIndex = nextIfStatement;
				}
				else
				{
					return new TextTemplateCondition(expression, startIndex, endIndex - startIndex);
				}
			}
		}
	}
}
