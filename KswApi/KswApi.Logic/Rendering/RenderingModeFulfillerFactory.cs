﻿using System;
using KswApi.Common.Configuration;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Rendering;
using KswApi.Ioc;

namespace KswApi.Logic.Rendering
{
	public class RenderingModeFulfillerFactory : IRenderingModeFulfillerFactory
	{
		public IRenderingModeFulfiller GetRenderer(RenderMode renderMode)
		{
			if (Settings.Current.Renderers == null)
				throw new RenderingException("No Renderer configurations found.");

			foreach (RenderingConfiguration renderingConfiguration in Settings.Current.Renderers)
				if (renderMode.ToString().Equals(renderingConfiguration.Mode, StringComparison.OrdinalIgnoreCase))
					return (IRenderingModeFulfiller)Dependency.Get(renderingConfiguration.Type);

			throw new RenderingException(string.Format("Unable to find an {0} for render mode '{1}'.",
				typeof(IRenderingModeFulfiller).Name, renderMode));
		}
	}
}