﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using KswApi.Common.Objects;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.Content;
using KswApi.Interface.Objects.Rendering;
using KswApi.Interface.Rendering;
using KswApi.Ioc;
using KswApi.Logic.Framework.Constants;

namespace KswApi.Logic.Rendering
{
	public class RenderingLogic
	{
		#region Public Methods

		public RenderResponse Render(RenderRequest request, AccessToken accessToken, StreamResponse streamResponse)
		{
			if (request == null)
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.MISSING_MESSAGE_BODY);

			if (!request.RenderMode.HasValue)
				throw new ServiceException(HttpStatusCode.BadRequest, "RenderMode must be specified.");

			IList<ItemReference> templateReferences = request.Template;
			IList<ItemReference> contentReferences = request.Content;

			if ((templateReferences == null || !templateReferences.Any()) &&
				(contentReferences == null || !contentReferences.Any()))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.Common.MISSING_MESSAGE_BODY);

			// customize/personalize
			KswApiTemplate kswApiTemplate = CreateKswApiTemplate(request);

			// get all the templates and content requested
			IList<ContentArticleResponse> allTemplates = GetKswApiContent(templateReferences, accessToken, kswApiTemplate);
			IList<ContentArticleResponse> allContent = GetKswApiContent(contentReferences, accessToken, kswApiTemplate);

			// send request to the mode fulfiller
			IRenderingModeFulfillerFactory renderingModeFulfillerFactory = Dependency.Get<IRenderingModeFulfillerFactory>();
			IRenderingModeFulfiller renderingModeFulfiller = renderingModeFulfillerFactory.GetRenderer(request.RenderMode.Value);
			return renderingModeFulfiller.Render(request, kswApiTemplate, allTemplates, allContent);
		}

		#endregion

		#region Private Methods

		private KswApiTemplate CreateKswApiTemplate(RenderRequest request)
		{
			KswApiTemplate kswApiTemplate = new KswApiTemplate();
			kswApiTemplate.KswApiTemplateType = KswApiTemplateType.Personalized;
			if (request.Template != null)
				kswApiTemplate.TemplateReference = request.Template.FirstOrDefault();

			kswApiTemplate.Fields = new List<TemplateField>();
			if (request.Age.HasValue)
				kswApiTemplate.Fields.Add(new TemplateField("Age", request.Age));
			if (request.EducationLevel.HasValue)
				kswApiTemplate.Fields.Add(new TemplateField("EducationLevel", request.EducationLevel));
			if (request.Ethnicity.HasValue)
				kswApiTemplate.Fields.Add(new TemplateField("Ethnicity", request.Ethnicity));
			if (!string.IsNullOrEmpty(request.FirstName))
				kswApiTemplate.Fields.Add(new TemplateField("FirstName", request.FirstName));
			if (!string.IsNullOrEmpty(request.From))
				kswApiTemplate.Fields.Add(new TemplateField("From", request.From));
			if (request.Gender.HasValue)
				kswApiTemplate.Fields.Add(new TemplateField("Gender", request.Gender));
			if (!string.IsNullOrEmpty(request.Keyword))
				kswApiTemplate.Fields.Add(new TemplateField("Keyword", request.Keyword));
			if (request.Language != null)
				kswApiTemplate.Fields.Add(new TemplateField("Language", request.Language));
			if (!string.IsNullOrEmpty(request.LastName))
				kswApiTemplate.Fields.Add(new TemplateField("LastName", request.LastName));
			if (request.PresenceOfChildren.HasValue)
				kswApiTemplate.Fields.Add(new TemplateField("PresenceOfChildren", request.PresenceOfChildren));
			if (!string.IsNullOrEmpty(request.ProviderName))
				kswApiTemplate.Fields.Add(new TemplateField("ProviderName", request.ProviderName));
			kswApiTemplate.Fields.Add(new TemplateField("RecipientIdentifier", request.RecipientIdentifier));
			if (!string.IsNullOrEmpty(request.Subject))
				kswApiTemplate.Fields.Add(new TemplateField("Subject", request.Subject));
			if (!string.IsNullOrEmpty(request.To))
				kswApiTemplate.Fields.Add(new TemplateField("To", request.To));
			return kswApiTemplate;
		}

		private IList<ContentArticleResponse> GetKswApiContent(IEnumerable<ItemReference> itemReferences, AccessToken accessToken,
			KswApiTemplate kswApiTemplate)
		{
			IList<ContentArticleResponse> list = new List<ContentArticleResponse>();
			if (itemReferences == null) return list;

			ContentLogic.ContentLogic contentLogic = new ContentLogic.ContentLogic();
			TextTemplate textTemplate = new TextTemplate();

			foreach (ItemReference itemReference in itemReferences)
			{
				if (itemReference == null) continue;

				ContentArticleResponse content = contentLogic.GetContent(itemReference.BucketIdOrSlug,
					itemReference.IdOrSlug, true, accessToken, true);
				list.Add(content);

				// resolve the templatable values in the content
				foreach (ContentSegmentResponse segment in content.Segments)
					segment.Body = textTemplate.Resolve(segment.Body, kswApiTemplate.Fields);
			}

			return list;
		}
 
		#endregion
	}
}
