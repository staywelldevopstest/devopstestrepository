﻿using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Poco.Sms;
using KswApi.Repositories;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;

namespace KswApi.Logic.Report
{
    public class ReportLogic
    {
        public SmsBillingReportDataResponse GetSmsBillingReportData(Month month, int year)
        {
            if (month == Month.None || year == 0)
                throw new ServiceException(HttpStatusCode.BadRequest, "The month or year have invalid valies or are not specified.");

            SmsBillingReportData result = new SmsBillingReportData();

            DateTime startDate = new DateTime(year, (int)month, 1);
            DateTime endDate = startDate.AddMonths(1).AddMilliseconds(-1);

            Repository repository = new Repository();

            result.Clients = repository.Administration.Clients.GetByDateAdded(endDate)
                .Select(item => new SmsBillingReportClientData { Name = item.ClientName, Id = item.Id, Status = item.Status, IncludeInReport = true })
                .OrderBy(item => item.Name)
                .ToList();

            foreach (SmsBillingReportClientData clientData in result.Clients)
            {
                clientData.Licenses =
                    repository.Administration.Licenses.GetByClientIdCreatedDateAndStatus(clientData.Id, endDate, LicenseStatus.Active)
                              .Select(item => new SmsBillingReportLicenseData { LicenseName = item.Name, Id = item.Id })
                              .OrderBy(item => item.LicenseName)
                              .ToList();

                if (clientData.Licenses.Count == 0)
                {
                    clientData.IncludeInReport = false;
                    continue;
                }

                foreach (SmsBillingReportLicenseData licenseData in clientData.Licenses)
                {
                    licenseData.Month = month;
                    licenseData.Year = year;
                    licenseData.LicenseId = licenseData.Id;

                    //Get the short code
                    SmsNumberData shortCode =
                        repository.SmsGateway.Numbers.GetByLicenseId(licenseData.Id).SingleOrDefault(item => item.Type == SmsNumberType.ShortCode);

                    //If we have a short code get the keyword count
                    if (shortCode != null)
                    {
                        licenseData.ShortCodeKeywordCount = repository.SmsGateway.KeywordDataLogs.Count(licenseData.Id, shortCode.Number, startDate, endDate);

                        //Get the long code count
                        licenseData.LongCodeCount = repository.SmsGateway.NumberDataLogs.Count(licenseData.Id, shortCode.Id, startDate, endDate);
                    }
                    else
                    {
                        licenseData.ShortCodeKeywordCount = 0;

                        //Get the long code count
                        licenseData.LongCodeCount =
                            repository.SmsGateway.NumberDataLogs.Count(licenseData.Id, startDate, endDate);
                    }

                    //Get the outgoing and incoming message counts from the BillingReportData
                    SmsBillingReportLicenseData reportData = repository.SmsGateway.BillingReportData.Get(
                        licenseData.LicenseId, month, year);

                    //If there are no counts then ignore
                    if (reportData == null)
                    {
                        licenseData.IncomingMessageCount = 0;
                        licenseData.OutgoingMessageCount = 0;

                        clientData.IncludeInReport = clientData.Status == ClientStatus.Active;
                        //This is to make the data coming back easy to debug.  Instead of going through all the active licenses, just list the ones that have activity
                        //clientData.IncludeInReport = false;
                    }
                    else
                    {
                        licenseData.IncomingMessageCount = reportData.IncomingMessageCount;
                        licenseData.OutgoingMessageCount = reportData.OutgoingMessageCount;
                    }
                }
            }

            result.Clients = result.Clients.Where(item => item.IncludeInReport).ToList();

            SmsBillingReportDataResponse returnResult = new SmsBillingReportDataResponse
                {
                    Clients = new List<SmsBillingReportClientDataResponse>()
                };

            //Convert to the repose object to remove Ids and everything else we don't need to return
            foreach (SmsBillingReportClientData clientData in result.Clients)
            {
                List<SmsBillingReportLicenseDataResponse> licenseDataResponses =
                    clientData.Licenses.Select(licenseData =>
                        new SmsBillingReportLicenseDataResponse
                            {
                                Name = licenseData.LicenseName,
                                ShortCodeKeywordCount = licenseData.ShortCodeKeywordCount,
                                LongCodeCount = licenseData.LongCodeCount,
                                IncomingMessageCount = licenseData.IncomingMessageCount,
                                OutgoingMessageCount = licenseData.OutgoingMessageCount
                            }).ToList();

				returnResult.Clients.Add(new SmsBillingReportClientDataResponse { Name = clientData.Name, Licenses = licenseDataResponses });
            }

            return returnResult;
        }

        public void GetSmsBillingReportCsvExport(StreamResponse streamResponse, Month month, int year)
        {
            if (month == Month.None || year == 0)
                throw new ServiceException(HttpStatusCode.BadRequest, "The month or year have invalid valies or are not specified.");

            SmsBillingReportDataResponse reportData = GetSmsBillingReportData(month, year);

            streamResponse.ContentType = "text/csv";

            StreamWriter csvData = new StreamWriter(streamResponse);

            csvData.WriteLine("SMS Gateway Billing Report");
            csvData.WriteLine();
            csvData.WriteLine("Client Name,License Name,Sms Short Code Keyword Count,Long Code Count,Outgoing Message Count,Incoming Message Count");
            csvData.WriteLine();

            foreach (SmsBillingReportClientDataResponse client in reportData.Clients)
            {
                csvData.WriteLine(client.Name);

                foreach (SmsBillingReportLicenseDataResponse license in client.Licenses)
                {
                    csvData.Write(",");
                    csvData.Write(license.Name);
                    csvData.Write(",");
                    csvData.Write(license.ShortCodeKeywordCount);
                    csvData.Write(",");
                    csvData.Write(license.LongCodeCount);
                    csvData.Write(",");
                    csvData.Write(license.OutgoingMessageCount);
                    csvData.Write(",");
                    csvData.WriteLine(license.IncomingMessageCount.ToString(CultureInfo.InvariantCulture));
                }
            }
            csvData.Flush();
        }

        /// <summary>
        /// Updates the billing report data object in the database.  You should pass nulls for data you are not updating.
        /// </summary>
        /// <param name="licenseId"></param>
        /// <param name="month"></param>
        /// <param name="year"></param>
        /// <param name="outgoingMessagesSent"></param>
        /// <param name="incomingMessage"></param>
        public void UpdateLicenseBillingReportData(Guid licenseId, Month month, int year, int? outgoingMessagesSent,
                                      int? incomingMessage)
        {
            Repository repository = new Repository();

            SmsBillingReportLicenseData licenseData = repository.SmsGateway.BillingReportData.Get(licenseId, month, year);

            if (licenseData == null)
            {
                licenseData = new SmsBillingReportLicenseData
                {
                    //NOTE: Commeting out ShortCodeKeywordCount and LongCodeCount because we cannot make sure these are correct at any point in time.
                    //  You can't decrement the count if one is deleted, but you also don't get an updated count for the next month unless someone edits
                    //  a keyword to cause this code to run.  We will do the count at the time the report data is requested.
                    LicenseId = licenseId,
                    Month = month,
                    Year = year,
                    OutgoingMessageCount = outgoingMessagesSent.HasValue ? outgoingMessagesSent.Value : 0,
                    IncomingMessageCount = incomingMessage.HasValue ? incomingMessage.Value : 0
                };

                repository.SmsGateway.BillingReportData.Add(licenseData);
            }
            else
            {
                licenseData.OutgoingMessageCount += outgoingMessagesSent.HasValue ? outgoingMessagesSent.Value : 0;
                licenseData.IncomingMessageCount += incomingMessage.HasValue ? incomingMessage.Value : 0;

                repository.SmsGateway.BillingReportData.Update(licenseData);
            }
        }
    }
}
