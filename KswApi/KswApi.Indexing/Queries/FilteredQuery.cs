﻿using System;
using KswApi.Indexing.Filters;
using Newtonsoft.Json;

namespace KswApi.Indexing.Queries
{
	public class FilteredQuery : SearchQuery
	{
		public FilteredQuery(SearchQuery query, SearchFilter filter)
		{
			if (query == null)
				throw new ArgumentNullException("query");

			if (filter == null)
				throw new ArgumentNullException("filter");

			Filtered = new Item(query, filter);
		}

		[JsonProperty(PropertyName = "filtered")]
		public Item Filtered { get; private set; }

		public class Item
		{
			internal Item(SearchQuery query, SearchFilter filter)
			{
				Query = query;
				Filter = filter;
			}

			[JsonProperty(PropertyName = "query")]
			public SearchQuery Query { get; private set; }

			[JsonProperty(PropertyName = "filter")]
			public SearchFilter Filter { get; private set; }
		}
	}
}
