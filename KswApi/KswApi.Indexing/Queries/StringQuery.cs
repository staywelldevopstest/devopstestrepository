﻿using Newtonsoft.Json;

namespace KswApi.Indexing.Queries
{
	public class StringQuery : SearchQuery
	{
		public StringQuery(string query, string defaultOperator = "OR")
		{
			QueryString = new Item(query, defaultOperator);
		}

		[JsonProperty(PropertyName = "query_string")]
		public Item QueryString { get; private set; }

		public class Item
		{
			internal Item(string query, string defaultOperator)
			{
				Query = query;
				DefaultOperator = defaultOperator;
			}

			[JsonProperty(PropertyName = "query")]
			public string Query { get; private set; }

			[JsonProperty(PropertyName = "default_operator")]
			public string DefaultOperator { get; private set; }

		}
	}
}
