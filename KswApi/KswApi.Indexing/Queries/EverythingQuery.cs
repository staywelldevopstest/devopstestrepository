﻿using Newtonsoft.Json;

namespace KswApi.Indexing.Queries
{
	public class EverythingQuery : SearchQuery
	{
		private readonly object _matchAll = new object();

		[JsonProperty(PropertyName = "match_all")]
		public object MatchAll { get { return _matchAll; } }
	}
}
