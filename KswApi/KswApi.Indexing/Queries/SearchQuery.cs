﻿namespace KswApi.Indexing.Queries
{
	public abstract class SearchQuery
	{
		// all queries need to be in this dll
		internal SearchQuery()
		{
		}

		public static SearchQuery FromString(string query)
		{
			return string.IsNullOrEmpty(query) ? (SearchQuery) new EverythingQuery() : new StringQuery(query);
		}
	}
}
