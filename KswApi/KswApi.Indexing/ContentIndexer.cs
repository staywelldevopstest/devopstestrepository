﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KswApi.Indexing.Mapping;
using KswApi.Interface.Objects.Content;
using KswApi.Ioc;
using KswApi.Logic.Framework.Constants;
using KswApi.Poco.Content;

namespace KswApi.Indexing
{
	public class ContentIndexer
	{
		public void Add(IndexableContent content, IndexType type)
		{
			if (type != IndexType.Content && type != IndexType.Published)
				throw new ArgumentException(Constant.Indexer.INVALID_TYPE);

			IIndex index = Dependency.Get<IIndex>();
			index.Add(content.Version.Id, GetFields(content), type);
		}

		public void UpdateCollectionAssignments(Guid id, List<Guid> collectionIds, List<string> collectionSlugs, IndexType type)
		{
			if (type != IndexType.Content && type != IndexType.Published)
				throw new ArgumentException(Constant.Indexer.INVALID_TYPE);

			IIndex index = Dependency.Get<IIndex>();

			Dictionary<string, object> fields = new Dictionary<string, object>();
			fields["collection-id"] = collectionIds;
			fields["collection"] = collectionSlugs;

			index.UpdatePartial(id, fields, type);
		}

		public void Update(IndexableContent content, IndexType type)
		{
			if (type != IndexType.Content && type != IndexType.Published)
				throw new ArgumentException(Constant.Indexer.INVALID_TYPE);

			IIndex index = Dependency.Get<IIndex>();
			index.UpdatePartial(content.Version.Id, GetFields(content), type);
		}

		public void Delete(Guid id, IndexType type)
		{
			if (type != IndexType.Content && type != IndexType.Published)
				throw new ArgumentException(Constant.Indexer.INVALID_TYPE);

			IIndex index = Dependency.Get<IIndex>();

			index.Delete(id, type);
		}

		public void Map()
		{
			Dictionary<string, FieldMap> map = new Dictionary<string, FieldMap>
											   {
												   {"title", new FieldMap {Type = "string", Index = "analyzed"}},
												   {"inverted-title", new FieldMap {Type = "string", Index = "analyzed"}},
												   {"alternate-title", new FieldMap {Type = "string", Index = "analyzed"}},
												   {"blurb", new FieldMap {Type = "string", Index = "analyzed"}},
												   {"body", new FieldMap {Type = "string", Index = "analyzed"}},
												   {"bucket", new FieldMap {Type = "string", Index = "analyzed"}},
												   {"bucket-name", new FieldMap {Type = "string", Index = "analyzed"}},
												   {"author", new FieldMap {Type = "string", Index = "analyzed"}},
												   {"online-editor", new FieldMap {Type = "string", Index = "analyzed"}},
												   {"online-medical-reviewer", new FieldMap {Type = "string", Index = "analyzed"}},
												   
												   {"type", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"legacy-id", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"collection", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"collection-id", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"gunning-fog-reading-level", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"flesch-kincaid-reading-level", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"gender", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"age-category", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"created-date", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"modified-date", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"updated-date", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"exact-title", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"exact-inverted-title", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"exact-alternate-title", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"bucket-id", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"language", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"slug", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"serviceline", new FieldMap{Type = "string", Index = "analyzed"}},
												   {"custom", new FieldMap{Type = "string", Index = "not_analyzed"}},
											   };

			IIndex index = Dependency.Get<IIndex>();

			index.Map(map, IndexType.Content);
			index.Map(map, IndexType.Published);
		}

		#region Private Methods

		private Dictionary<string, object> GetFields(IndexableContent content)
		{
			// explicit setting of null values is intentional, it ensures that our update call happens correctly

			Dictionary<string, object> result = new Dictionary<string, object>();

			// order is intentional here, we don't want a segment slug to override an existing version field
			if (content.Segments != null)
			{
				StringBuilder builder = new StringBuilder();
				foreach (ContentSegmentRequest segment in content.Segments)
				{
					if (content.Bucket.Segments != null)
					{
						Guid id;
						ContentBucketSegment bucketSegment = content.Bucket.Segments.FirstOrDefault(item => Guid.TryParse(segment.IdOrSlug, out id) ? id == item.Id : segment.IdOrSlug == item.Slug);

						if (bucketSegment != null && !string.IsNullOrEmpty(bucketSegment.Name))
							result[bucketSegment.Slug] = segment.Body;
					}

					builder.Append(segment.Body);

					// make sure beginning/end words are not merged
					builder.Append(' ');
				}

				result["body"] = builder.ToString();
			}
			else
			{
				result["body"] = null;
			}

			result["title"] = content.Version.Title;
			result["inverted-title"] = content.Version.InvertedTitle;
			result["alternate-title"] = content.Version.AlternateTitles;
			result["blurb"] = content.Version.Blurb;
			result["slug"] = content.Version.Slug;
			result["bucket"] = content.Bucket.Name;
			result["bucket-name"] = content.Bucket.Name;
			result["bucket-slug"] = content.Bucket.Slug;
			result["created-date"] = content.Version.DateAdded;
			result["modified-date"] = content.Version.DateModified;
			result["updated-date"] = content.Version.DateUpdated;
			result["legacy-id"] = content.Version.LegacyId;

			if (content.Version.Title != null)
				result["exact-title"] = content.Version.Title.ToLower();
			else
				result["exact-title"] = null;

			// add alternate titles
			List<string> alernateTitles = new List<string>();

			if (!string.IsNullOrEmpty(content.Version.InvertedTitle))
				alernateTitles.Add(content.Version.InvertedTitle.ToLower());
			if (content.Version.AlternateTitles != null)
				alernateTitles.AddRange(content.Version.AlternateTitles.Where(item => !string.IsNullOrEmpty(item)).Select(item => item.ToLower()));
			
			result["exact-alternate-title"] = alernateTitles;

			result["bucket-id"] = content.Bucket.Id;
			result["type"] = content.Bucket.Type;
			result["language"] = content.Version.Language;
			result["author"] = content.Core.Authors;
			result["online-editor"] = content.Core.OnlineEditors;
			result["online-medical-reviewer"] = content.Core.OnlineMedicalReviewers;
			result["gunning-fog-reading-level"] = content.Core.GunningFogReadingLevel;
			result["flesch-kincaid-reading-level"] = content.Core.FleschKincaidReadingLevel;
			result["gender"] = content.Core.Gender.ToString();

			if (content.Core.AgeCategories != null)
				result["age-category"] = content.Core.AgeCategories.Select(item => item.ToString()).ToArray();
			else
				result["age-category"] = null;

			if (content.Core.Taxonomies != null)
			{
				content.Core.Taxonomies.Keys.ToList().ForEach
					(
						slug =>
						result.Add(slug, content.Core.Taxonomies[slug].Select(taxonomyValue => taxonomyValue.Value))
					);
			}

			if (content.ServiceLines != null)
				result["serviceline"] = content.ServiceLines.Select(line => string.Join("/", line.PathValues)).ToArray();
			else
				result["serviceline"] = null;

			if (content.Core.CustomAttributes != null && content.Core.CustomAttributes.Count > 0)
			{
				List<string> values = (content.Core.CustomAttributes.SelectMany(
					attr => attr.Value, (attr, value) => attr.Key + '=' + value)).ToList();
				result["custom"] = values;
			}
			else
			{
				result["custom"] = null;
			}

			return result;
		}

		#endregion
	}
}
