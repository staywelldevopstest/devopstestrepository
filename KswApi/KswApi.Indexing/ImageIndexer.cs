﻿using System;
using System.Collections.Generic;
using KswApi.Indexing.Mapping;
using KswApi.Interface.Enums;
using KswApi.Ioc;
using KswApi.Poco.Content;

namespace KswApi.Indexing
{
	public class ImageIndexer
	{
		public void Add(ImageDetail image)
		{
			IIndex index = Dependency.Get<IIndex>();

			index.Add(image.Id, GetFields(image), IndexType.Image);
		}

		public void Update(ImageDetail image)
		{
			Add(image);
		}

		public void Delete(Guid id)
		{
			IIndex index = Dependency.Get<IIndex>();

			index.Delete(id, IndexType.Image);
		}

		public void Map()
		{
			Dictionary<string, FieldMap> map = new Dictionary<string, FieldMap>
			                                   {
												   {"title", new FieldMap {Type = "string", Index = "analyzed"}},
												   {"blurb", new FieldMap {Type = "string", Index = "analyzed"}},

												   {"type", new FieldMap {Type = "string", Index = "not_analyzed"}},
				                                   {"extension", new FieldMap {Type = "string", Index = "not_analyzed"}},
				                                   {"format", new FieldMap {Type = "string", Index = "not_analyzed"}},
				                                   {"slug", new FieldMap {Type = "string", Index = "not_analyzed"}},
				                                   {"bucket-id", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"exact-title", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"created-date", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"modified-date", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"updated-date", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"legacy-id", new FieldMap {Type = "string", Index = "not_analyzed"}},
			                                   };

			IIndex index = Dependency.Get<IIndex>();

			index.Map(map, IndexType.Image);
		}

		#region Private Methods
		private Dictionary<string, object> GetFields(ImageDetail image)
		{
			Dictionary<string, object> result = new Dictionary<string, object>();

			result["title"] = image.Title;
			result["blurb"] = image.Blurb;

			result["type"] = ContentType.Image;
			result["extension"] = image.Format.ToString().ToLower();
			result["format"] = image.Format.ToString();
			result["slug"] = image.Slug;
			result["bucket-id"] = image.BucketId;
			result["created-date"] = image.DateAdded;
			result["modified-date"] = image.DateModified;
			result["updated-date"] = image.DateModified;
			result["legacy-id"] = image.LegacyId;

			if (image.Title != null)
				result["exact-title"] = image.Title.ToLower();

			return result;
		}

		#endregion
	}
}
