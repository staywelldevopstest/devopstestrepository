﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace KswApi.Indexing.Filters
{
	public class PrefixFilter : SearchFilter
	{
		public PrefixFilter(string name, string value)
		{
			Prefix = new Dictionary<string, object> { { name, value } };
		}

		[JsonProperty(PropertyName = "prefix")]
		public Dictionary<string, object> Prefix { get; private set; }
	}
}
