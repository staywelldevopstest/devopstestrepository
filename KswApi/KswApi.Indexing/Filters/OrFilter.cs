﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace KswApi.Indexing.Filters
{
	public class OrFilter : SearchFilter
	{
				public OrFilter(params SearchFilter[] filters)
		{
			Or = new List<SearchFilter>(filters);
		}

		[JsonProperty(PropertyName = "or")]
		public List<SearchFilter> Or { get; private set; }
	}
}
