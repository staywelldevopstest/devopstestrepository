﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace KswApi.Indexing.Filters
{
	public class TermFilter : SearchFilter
	{
		public TermFilter(string name, object value)
		{
			Term = new Dictionary<string, object> { { name, value } };
		}

		[JsonProperty(PropertyName = "term")]
		public Dictionary<string, object> Term { get; private set; }
	}
}
