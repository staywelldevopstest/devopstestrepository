﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace KswApi.Indexing.Filters
{
	public class AndFilter : SearchFilter
	{
		public AndFilter(params SearchFilter[] filters)
		{
			And = new List<SearchFilter>(filters);
		}

		public AndFilter(IEnumerable<SearchFilter> filters)
		{
			And = new List<SearchFilter>(filters);
		}

		[JsonProperty(PropertyName = "and")]
		public List<SearchFilter> And { get; private set; }
	}
}
