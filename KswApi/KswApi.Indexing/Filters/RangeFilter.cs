﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace KswApi.Indexing.Filters
{
	public class RangeFilter : SearchFilter
	{
		public RangeFilter(string name, object from, object to, bool inclusiveFrom, bool inclusiveTo)
		{
			Range = new Dictionary<string, Item> { { name, new Item(from, to, inclusiveFrom, inclusiveTo) } };
		}

		[JsonProperty(PropertyName = "range")]
		public Dictionary<string, Item> Range { get; set; }

		public class Item
		{
			internal Item(object from, object to, bool inclusiveFrom, bool inclusiveTo)
			{
				From = from;
				To = to;
				IncludeLower = inclusiveFrom;
				IncludeUpper = inclusiveTo;
			}

			public object From { get; private set; }
			public object To { get; private set; }
			public bool IncludeLower { get; private set; }
			public bool IncludeUpper { get; private set; }
		}
	}
}
