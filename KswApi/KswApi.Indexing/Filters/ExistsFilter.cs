﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace KswApi.Indexing.Filters
{
	public class ExistsFilter : SearchFilter
	{
		public ExistsFilter(string name, object value)
		{
			Exists = new Dictionary<string, object> { { name, value } };
		}

		[JsonProperty(PropertyName = "exists")]
		public Dictionary<string, object> Exists { get; private set; }	}
}
