﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace KswApi.Indexing.Filters
{
	public class IdsFilter : SearchFilter
	{
		public IdsFilter(IEnumerable<Guid> ids)
		{
			Ids = new Item(ids);
		}

		[JsonProperty(PropertyName = "ids")]
		public Item Ids { get; private set; }

		public class Item
		{
			internal Item(IEnumerable<Guid> ids)
			{
				Values = ids.ToList();
			}

			[JsonProperty(PropertyName = "values")]
			public List<Guid> Values { get; private set; } 
		}
	}
}
