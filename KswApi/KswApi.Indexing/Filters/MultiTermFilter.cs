﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace KswApi.Indexing.Filters
{
	public class MultiTermFilter : SearchFilter
	{
		public MultiTermFilter(string name, List<object> values)
		{
			Term = new Dictionary<string, List<object>> { { name, values } };
		}

		[JsonProperty(PropertyName = "terms")]
		public Dictionary<string, List<object>> Term { get; private set; }
	}
}
