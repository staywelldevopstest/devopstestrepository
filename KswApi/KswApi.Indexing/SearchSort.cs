﻿using Newtonsoft.Json;

namespace KswApi.Indexing
{
	public class SearchSort
	{
		public SearchSort(string name, string order, string missing = null, string mode = null)
		{
			Name = name;
			Sort = new Item(order, missing, mode);
		}

		public string Name { get; private set; }
		public Item Sort { get; private set; }


		public class Item
		{
			internal Item(string order, string missing, string mode)
			{
				Order = order;
				Missing = missing;
				Mode = mode;
			}

			[JsonProperty(PropertyName = "order")]
			public string Order { get; private set; }

			[JsonProperty(PropertyName = "mode")]
			public string Mode { get; private set; }

			[JsonProperty(PropertyName = "missing")]
			public string Missing { get; private set; }
		}
	}
}
