﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace KswApi.Indexing.Objects
{
	public class SearchListResponse
	{
		[JsonProperty(PropertyName = "total")]
		public int Total { get; set; }

		[JsonProperty(PropertyName = "max_score")]
		public double? MaximumScore { get; set; }

		[JsonProperty(PropertyName = "hits")]
		public List<SearchItemResponse> Items { get; set; }
	}
}
