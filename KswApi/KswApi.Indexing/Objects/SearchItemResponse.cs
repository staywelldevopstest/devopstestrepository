﻿using System;
using KswApi.Poco.Content;
using Newtonsoft.Json;

namespace KswApi.Indexing.Objects
{
	public class SearchItemResponse
	{
		[JsonProperty(PropertyName = "_id")]
		public Guid Id { get; set; }

		[JsonProperty(PropertyName = "score")]
		public double Score { get; set; }

		[JsonProperty(PropertyName = "_type")]
		public IndexType Type { get; set; }
	}
}
