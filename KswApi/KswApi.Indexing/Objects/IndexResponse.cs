﻿using System;
using Newtonsoft.Json;

namespace KswApi.Indexing.Objects
{
	public class IndexResponse
	{
		[JsonProperty(PropertyName = "_id")]
		public Guid Id { get; set; }

		[JsonProperty(PropertyName = "version")]
		public int Version { get; set; }
	}
}
