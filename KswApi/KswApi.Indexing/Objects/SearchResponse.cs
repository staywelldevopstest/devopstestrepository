﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace KswApi.Indexing.Objects
{
	public class SearchResponse
	{
		[JsonProperty(PropertyName = "hits")]
		public SearchListResponse Hits { get; set; }

		[JsonProperty(PropertyName = "facets")]
		public Dictionary<string, FacetResponse> Facets { get; set; }
	}
}
