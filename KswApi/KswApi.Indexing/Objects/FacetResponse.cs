﻿using System.Collections.Generic;
using KswApi.Logic.Framework.Objects;
using Newtonsoft.Json;

namespace KswApi.Indexing.Objects
{
	public class FacetResponse
	{
		[JsonProperty(PropertyName = "terms")]
		public List<TermCountResponse> Terms { get; set; }

		[JsonProperty(PropertyName = "other")]
		public int Other { get; set; }

		[JsonProperty(PropertyName = "missing")]
		public int Missing { get; set; }

		[JsonProperty(PropertyName = "total")]
		public int Total { get; set; }
	}
}
