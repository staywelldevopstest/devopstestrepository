﻿using System;
using System.Collections.Generic;
using KswApi.Indexing.Filters;
using KswApi.Indexing.Mapping;
using KswApi.Indexing.Objects;
using KswApi.Indexing.Queries;
using KswApi.Poco.Content;

namespace KswApi.Indexing
{
	public interface IIndex
	{
		IndexResponse Add(Guid id, Dictionary<string, object> fields, IndexType type);
		IndexResponse Update(Guid id, Dictionary<string, object> fields, IndexType type);
		IndexResponse UpdatePartial(Guid id, Dictionary<string, object> fields, IndexType type);
		void Delete(Guid id, IndexType type);
		SearchResponse Search(int offset, int count, SearchQuery query, SearchFilter filter, List<string> facets, List<SearchSort> sort, params IndexType[] types);
		void Map(Dictionary<string, FieldMap> map, IndexType type);
	}
}
