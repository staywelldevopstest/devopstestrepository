﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using KswApi.Indexing.Mapping;
using KswApi.Ioc;
using KswApi.Logic.Framework.Constants;
using KswApi.Poco.Content;

namespace KswApi.Indexing
{
	// TODO: Create base (abstract?) indexer class
	public class CollectionIndexer
	{
		private readonly Dictionary<IndexType, Dictionary<string, FieldMap>> _fieldMaps = new Dictionary<IndexType, Dictionary<string, FieldMap>>();

		public IEnumerable<IndexType> AllowedIndexTypes
		{
			get { return _fieldMaps.Keys.ToList(); }
		}

		public CollectionIndexer()
		{
			InitFieldMaps();
		}

		private void InitFieldMaps()
		{
			// TODO: get from configValues?

			// TODO: create fieldName, property map to drive GetFields?
			Dictionary<string, FieldMap> map = new Dictionary<string, FieldMap>
			                                   {
												   {"title", new FieldMap {Type = "string", Index = "analyzed"}},
												   {"created-date", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"modified-date", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"exact-title", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"legacy-id", new FieldMap {Type = "string", Index = "not_analyzed"}},
												   {"slug", new FieldMap {Type = "string", Index = "not_analyzed"}}
			                                   };
			_fieldMaps[IndexType.Collection] = map;

		}

		public void Add(Collection collection, Topic topic, IndexType type)
		{
			ValidateIndexType(type);
			IIndex index = Dependency.Get<IIndex>();
			index.Add(collection.Id, GetFields(collection, topic), type);
		}

		public void Update(Collection collection, Topic topic, IndexType type)
		{
			ValidateIndexType(type);
			Add(collection, topic, type);
		}

		public void Delete(Guid id, IndexType type)
		{
			ValidateIndexType(type);
			IIndex index = Dependency.Get<IIndex>();
			index.Delete(id, type);
		}

		public void Map()
		{
			IIndex index = Dependency.Get<IIndex>();
			foreach (IndexType type in _fieldMaps.Keys)
			{
				index.Map(_fieldMaps[type], type);
			}
		}

		#region Private Methods

		private void ValidateIndexType(IndexType type)
		{
			if (!AllowedIndexTypes.Contains(type))
				throw new ArgumentException(Constant.Indexer.INVALID_TYPE);
		}

		private Dictionary<string, object> GetFields(Collection collection, Topic topic)
		{
			Dictionary<string, object> result = new Dictionary<string, object>();

			// use reflection instead?
			result["title"] = topic.Title;
			result["slug"] = topic.Slug;
			result["created-date"] = topic.DateAdded;
			result["modified-date"] = topic.DateModified;

			if (topic.Title != null)
				result["exact-title"] = topic.Title.ToLower();

			if (!string.IsNullOrEmpty(collection.LegacyId))
				result["legacy-id"] = collection.LegacyId;

			// TODO: move to test
			// Debug.Assert(!IsMissingFields(collection.GetType(),result.Keys.ToList()));

			return result;
		}

		private bool IsMissingFields(Type type, List<string> indexedFields)
		{
			string idProperty = PropertyOf(() => default(Collection).Id).Name;
			PropertyInfo[] properties = type.GetProperties();
			return properties.Any(property => !indexedFields.Contains(property.Name) && property.Name.Equals(idProperty, StringComparison.OrdinalIgnoreCase));
		}

		// TODO: move to helpers
		internal static PropertyInfo PropertyOf<T>(Expression<Func<T>> expression)
		{
			var body = (MemberExpression)expression.Body;
			return (PropertyInfo)body.Member;
		}

		#endregion
	}
}
