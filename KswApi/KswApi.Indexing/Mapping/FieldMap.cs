﻿using Newtonsoft.Json;

namespace KswApi.Indexing.Mapping
{
	public class FieldMap
	{
		[JsonProperty(PropertyName = "type")]
		public string Type { get; set; }

		[JsonProperty(PropertyName = "analyzer")]
		public string Analyzer { get; set; }

		[JsonProperty(PropertyName = "store")]
		public string Store { get; set; }

		[JsonProperty(PropertyName = "index")]
		public string Index { get; set; }

        [JsonProperty(PropertyName = "format")]
        public string Format { get; set; }
	}
}
