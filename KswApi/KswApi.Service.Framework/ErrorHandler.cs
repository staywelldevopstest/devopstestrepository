﻿using System;
using System.Net;
using System.Reflection;
using System.Runtime.Serialization;
using System.ServiceModel.Web;
using System.Web;
using KswApi.Common.Configuration;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logging;
using KswApi.Logging.Objects;
using KswApi.Logic.Exceptions;
using KswApi.Service.Framework.Helpers;
using MessageFormat = KswApi.Service.Framework.Enums.MessageFormat;

namespace KswApi.Service.Framework
{
	public class ErrorHandler
	{
		public void HandleException(Exception exception, Operation operation)
		{
			try
			{
				while (exception is TargetInvocationException && exception.InnerException != null)
					exception = exception.InnerException;

				HttpStatusCode code = GetStatusCode(exception);

				if (!Enum.IsDefined(typeof(HttpStatusCode), code))
					code = HttpStatusCode.InternalServerError;

				string errorDescription;

				// service exceptions denote specific error codes where
				// the message should always be returned to the caller
				if (exception is ServiceException)
				{
					errorDescription = (exception as ServiceException).Message;
				}
				else if (exception is OAuthException)
				{
					HandleOAuthException((OAuthException)exception, operation);
					return;
				}
				else if (exception is SerializationException)
				{
					errorDescription = "Unable to parse request.";
				}
				else if (Settings.Current.ShowFullExceptions)
				{
					errorDescription = exception.ToString();
				}
				else
				{
					errorDescription = HttpWorkerRequest.GetStatusDescription((int)code);
				}

				Error error = new Error(code, errorDescription);

				MessageFormat format = GetResponseFormat(operation, operation.Request);

				string jsonpCallback = null;

				if (format == MessageFormat.Jsonp)
				{
					jsonpCallback = JsonpHelper.GetCallback(operation.Request);
					if (!JsonpHelper.IsValidCallback(jsonpCallback))
						jsonpCallback = "throw";
				}

				operation.Response.StatusCode = (int)code;

				if (code == HttpStatusCode.Unauthorized)
				{
					// Add WWW-Authenticate header for unauthorized access `
					operation.Response.Headers.Add("WWW-Authenticate", "Bearer");
				}

				operation.OperationLog.ResponseCode = (int)code;

				Serializer serializer = new Serializer();

				serializer.SerializeResponse(format, error, typeof(Error), operation.Response, jsonpCallback);

				LogException(operation, exception);
			}
			catch (Exception)
			{
				// this is a catastrophic failure, we can't respond to the exception
				// we truncate the response stream and return an internal server error

				operation.Response.StatusCode = (int)HttpStatusCode.InternalServerError;
				operation.Response.StatusDescription = HttpWorkerRequest.GetStatusDescription((int)HttpStatusCode.InternalServerError);
			}
		}

		private MessageFormat GetResponseFormat(Operation operation, HttpRequest request)
		{
			if (operation.Format != MessageFormat.Unknown)
				return operation.Format;

			return GuessResponseFormat(request);
		}

		private MessageFormat GuessResponseFormat(HttpRequest request)
		{
			// for unknown requests, guess the response format for the error
			string path = request.CurrentExecutionFilePath;

			string[] split = path.Split('/');

			for (int segment = split.Length - 1; segment >= 0; segment--)
			{
				string current = split[segment];

				if (current.EndsWith(".jsonp", StringComparison.OrdinalIgnoreCase))
					return MessageFormat.Jsonp;

				if (current.EndsWith(".json", StringComparison.OrdinalIgnoreCase))
					return MessageFormat.Json;

				if (current.EndsWith(".xml", StringComparison.OrdinalIgnoreCase))
					return MessageFormat.Xml;

				if (current.EndsWith(".soap", StringComparison.OrdinalIgnoreCase))
					return MessageFormat.Soap;
			}

			// if we couldn't figure out the response format,
			// send a json body

			return MessageFormat.Json;
		}

		private HttpStatusCode GetStatusCode(Exception exception)
		{
			// if-chaining this exception instead of using a switch statement
			// to capture the derived exceptions (especially in the case of
			// WebFaultException)
			if (exception is ServiceException)
				return (exception as ServiceException).StatusCode;

			if (exception is HttpException)
				return (HttpStatusCode)(exception as HttpException).GetHttpCode();

			if (exception is WebFaultException)
				return (exception as WebFaultException).StatusCode;

			if (exception is SerializationException)
				return HttpStatusCode.BadRequest;

			return HttpStatusCode.InternalServerError;
		}

		private void HandleOAuthException(OAuthException exception, Operation operation)
		{
			operation.Response.StatusCode = (int)exception.StatusCode;

			OAuthError error = new OAuthError
			{
				error = exception.ErrorCodeString,
				error_description = exception.Message
			};

			MessageFormat format = GetResponseFormat(operation, operation.Request);

			Serializer serializer = new Serializer();

			serializer.SerializeResponse(format, error, typeof(OAuthError), operation.Response);

			operation.OperationLog.Stop();

			LogException(operation, exception, Severity.Security);

		}

		private void LogException(Operation operation, Exception exception, Severity severity)
		{
			AllowedLogging logging = operation != null && operation.Allowed != null
										 ? operation.Allowed.Logging
										 : AllowedLogging.Log;

			switch (logging)
			{
				case AllowedLogging.NeverLog:
					return;

				default:
					Logger.Log(severity, exception);
					break;
			}
		}

		private void LogException(Operation operation, Exception exception)
		{
			AllowedLogging logging = operation != null && operation.Allowed != null
										 ? operation.Allowed.Logging
										 : AllowedLogging.Log;

			OperationLog operationLog;
			if (operation != null && operation.OperationLog != null)
				operationLog = operation.OperationLog;
			else
				operationLog = new OperationLog();

			switch (logging)
			{
				case AllowedLogging.NeverLog:
					return;

				case AllowedLogging.LogWithoutParametersOrBody:
					// when the logging infrastucture is rehashed, put in the
					// capability to mask parameters in the uri
					operationLog.Body = null;
					break;

				case AllowedLogging.LogExceptionWithoutBody:
				case AllowedLogging.LogWithoutBody:
					operationLog.Body = null;
					break;
			}

			Logger.Log(exception, operationLog);
		}
	}
}
