﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Interface.Attributes;
using KswApi.Service.Framework.Enums;

namespace KswApi.Service.Framework
{
	public class OperationMapper
	{
		private const string DUMMY_URI = "http://tempuri.com";
		private const string GET = "GET";
		private const string POST = "POST";

		private readonly Dictionary<string, UriTemplateTable> _tables = new Dictionary<string, UriTemplateTable>();

		private class Operation
		{
			public MethodInfo MethodInfo { get; set; }
			public MessageFormat ResponseFormat { get; set; }
			public AllowAttribute Allowed { get; set; }
			public object Service { get; set; }
			public Dictionary<string, ParameterInfo> Parameters { get; set; }
		}

		public ServiceOperation GetOperation(string verb, string path)
		{
			UriTemplateTable table;
			if (!_tables.TryGetValue(verb, out table))
				return null;

			if (!path.StartsWith("/"))
				path = "/" + path;

			UriTemplateMatch match = table.MatchSingle(new Uri(DUMMY_URI + path));

			if (match == null)
				return null;

			Operation operation = (Operation)match.Data;

			return new ServiceOperation
					   {
						   MethodInfo = operation.MethodInfo,
						   Parameters = operation.Parameters,
						   Service = operation.Service,
						   Allowed = operation.Allowed,
						   ResponseFormat = operation.ResponseFormat,
						   Template = match
					   };
		}

		public void RegisterService(Type serviceType)
		{
			ConstructorInfo constructor = serviceType.GetConstructor(Type.EmptyTypes);

			if (constructor == null)
				return;

			object service = constructor.Invoke(null);

			RegisterOperations(serviceType, service);
		}

		private void RegisterOperations(Type type, object service)
		{
			object[] attributes = type.GetCustomAttributes(false);

			ServiceContractAttribute serviceContractAttribute = (ServiceContractAttribute)
				attributes.FirstOrDefault(item => item is ServiceContractAttribute);

			if (serviceContractAttribute == null)
			{
				Type[] interfaces = type.GetInterfaces();

				foreach (Type serviceInterface in interfaces)
					RegisterOperations(serviceInterface, service);

				return;
			}

			MethodInfo[] methods = type.GetMethods();

			string serviceName = serviceContractAttribute.Name;

			if (string.IsNullOrEmpty(serviceName))
				serviceName = GetServiceName(type);

			foreach (MethodInfo method in methods)
			{
				RegisterOperation(method, service, serviceName);
			}
		}

		private string GetServiceName(Type type)
		{
			if (!type.IsInterface)
				return type.Name;

			string name = type.Name;

			if (name.Length == 1 || !name.StartsWith("I"))
				return name;

			if (!char.IsUpper(name[1]))
				return name;

			return name.Substring(1);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="methodAttributes"></param>
		/// <param name="verb"></param>
		/// <returns>null if no path is specified in the attribute</returns>
		private string GetPathFromAttribute(object[] methodAttributes, out string verb)
		{
			verb = GET;

			WebGetAttribute webGet = (WebGetAttribute)methodAttributes.FirstOrDefault(item => item is WebGetAttribute);

			if (webGet != null && !string.IsNullOrEmpty(webGet.UriTemplate))
			{
				return webGet.UriTemplate;
			}

			WebInvokeAttribute webInvoke =
				(WebInvokeAttribute)methodAttributes.FirstOrDefault(item => item is WebInvokeAttribute);

			// allow empty uri template
			if (webInvoke != null)
			{
				verb = string.IsNullOrEmpty(webInvoke.Method) ? POST : webInvoke.Method;

				if (webInvoke.UriTemplate != null)
					return webInvoke.UriTemplate;
			}

			return null;
		}

		private string GetPath(MethodInfo method, object[] methodAttributes, string basePath, out string verb)
		{
			string path = GetPathFromAttribute(methodAttributes, out verb);

			// allow path to be an empty string, only check for null
			if (path == null)
				path = method.Name;

			if (path.StartsWith("/"))
				return path;

			if (path != string.Empty)
				path = '/' + path;

			if (string.IsNullOrEmpty(basePath))
				return path;

			if (!basePath.StartsWith("/"))
				basePath = '/' + basePath;

			return basePath + path;
		}

		private void RegisterOperation(MethodInfo method, object service, string basePath)
		{
			object[] attributes = method.GetCustomAttributes(false);

			string verb;

			AllowAttribute allowed = (AllowAttribute)attributes.SingleOrDefault(item => item is AllowAttribute);

			string uriTemplate = GetPath(method, attributes, basePath, out verb);

			if (typeof(Stream).IsAssignableFrom(method.ReturnType))
			{
				RegisterAlternativeOperation(method, uriTemplate, service, verb, allowed, MessageFormat.Stream);
			}
			else
			{
				// standard extensions
				RegisterAlternativeOperation(method, uriTemplate, service, verb, allowed, MessageFormat.Json);
				RegisterAlternativeOperation(method, uriTemplate, service, verb, allowed, MessageFormat.Json, ".json");
				RegisterAlternativeOperation(method, uriTemplate, service, verb, allowed, MessageFormat.Xml, ".xml");
				RegisterAlternativeOperation(method, uriTemplate, service, verb, allowed, MessageFormat.Soap, ".soap");
				if (verb == GET)
					RegisterAlternativeOperation(method, uriTemplate, service, verb, allowed, MessageFormat.Jsonp, ".jsonp");
			}
		}

		private void RegisterAlternativeOperation(MethodInfo method, string uriTemplate, object service, string verb, AllowAttribute allowed, MessageFormat format, string extension = null)
		{
			string legacyCandidate = RewriteLegacyUriTemplate(uriTemplate, extension);

			if (!string.IsNullOrEmpty(legacyCandidate))
				RegisterRewrittenOperation(method, legacyCandidate, service, verb, allowed, format);

			string newCandidate = RewriteNewUriTemplate(uriTemplate, extension);

			if (!string.IsNullOrEmpty(newCandidate) && newCandidate != legacyCandidate)
				RegisterRewrittenOperation(method, newCandidate, service, verb, allowed, format);
		}

		private void RegisterRewrittenOperation(MethodInfo method, string uriTemplate, object service, string verb, AllowAttribute allowed, MessageFormat format)
		{
			Operation operation = new Operation
			{
				MethodInfo = method,
				Service = service,
				ResponseFormat = format,
				Allowed = allowed,
				Parameters = new Dictionary<string, ParameterInfo>()
			};

			ParameterInfo[] parameters = method.GetParameters();

			foreach (ParameterInfo parameter in parameters)
			{
				MessageParameterAttribute attribute =
					(MessageParameterAttribute)(parameter.GetCustomAttributes(false).FirstOrDefault(item => item is MessageParameterAttribute));

				if (attribute != null && !string.IsNullOrEmpty(attribute.Name))
					operation.Parameters[attribute.Name.ToUpper()] = parameter;
				else
					operation.Parameters[parameter.Name.ToUpper()] = parameter;
			}


			UriTemplateTable table;

			if (!_tables.TryGetValue(verb, out table))
			{
				table = new UriTemplateTable(new Uri(DUMMY_URI));

				_tables[verb] = table;
			}

			UriTemplate template = new UriTemplate(uriTemplate);

			table.KeyValuePairs.Add(new KeyValuePair<UriTemplate, object>(template, operation));
		}

		private string RewriteLegacyUriTemplate(string uriTemplate, string extension)
		{
			if (string.IsNullOrEmpty(extension))
				return uriTemplate;

			string[] split = uriTemplate.Split('/');

			for (int segment = split.Length - 1; segment >= 0; segment--)
			{
				string current = split[segment];

				if (!string.IsNullOrEmpty(current) && !current.StartsWith("{"))
				{
					// last non-parameter segment

                    int queryStringIndex = split[segment].LastIndexOf('?');
                    if (queryStringIndex > 0)
                    {
                        split[segment] = split[segment].Insert(queryStringIndex, extension);
                    }
                    else
                    {
					split[segment] = current + extension;
                    }

					
					return string.Join("/", split);
				}
			}

			return null;
		}

		private string RewriteNewUriTemplate(string uriTemplate, string extension)
		{
			if (string.IsNullOrEmpty(extension))
				return null;

		    int queryStringIndex = uriTemplate.LastIndexOf('?');
            if(queryStringIndex > 0)
            {
                return uriTemplate.Insert(queryStringIndex, extension);
            }

			return uriTemplate + extension;
		}
	}
}