﻿using System.Collections.Specialized;
using System.Reflection;
using System.Web;
using KswApi.Common.Objects;
using KswApi.Interface.Attributes;
using KswApi.Interface.Objects;
using KswApi.Logging.Objects;
using KswApi.Service.Framework.Enums;

namespace KswApi.Service.Framework
{
	public class Operation
	{
		public static Operation Current
		{
			get { return CurrentOperation.Get(); }
		}

		public MethodInfo MethodInfo { get; set; }
		public AllowAttribute Allowed { get; set; }
		public AccessToken AccessToken { get; set; }
		public Application Application { get; set; }
		public string SourceAddress { get; set; }
		public StreamResponse StreamResponse { get; set; }
		public NameValueCollection Headers { get; set; }
		public MessageFormat Format { get; set; }
		public OperationLog OperationLog { get; set; }
		public HttpRequest Request { get; set; }
		public HttpResponse Response { get; set; }
	}
}
