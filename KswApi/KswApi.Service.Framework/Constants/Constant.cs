﻿namespace KswApi.Service.Framework.Constants
{
	internal static class Constant
	{
		public const string JSONP_CALLBACK_PARAMETER = "CALLBACK";
		public const string JSONP_VALIDATION_REGEX = @"^[a-zA-Z$_][a-zA-Z0-9$_]*(\.[a-zA-Z$_][a-zA-Z0-9$_]*)*$";
	}
}
