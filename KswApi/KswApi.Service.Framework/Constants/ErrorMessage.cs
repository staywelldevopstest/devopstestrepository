﻿namespace KswApi.Service.Framework.Constants
{
	static class ErrorMessage
	{
		public const string MISSING_JSONP_CALLBACK = "JSONP requires a callback parameter.";
	}
}
