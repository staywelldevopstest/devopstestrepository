﻿using System;
using System.Collections.Generic;
using System.Reflection;
using KswApi.Interface.Attributes;
using KswApi.Service.Framework.Enums;

namespace KswApi.Service.Framework
{
	public class ServiceOperation
	{
		public AllowAttribute Allowed { get; set; }
		public MessageFormat ResponseFormat { get; set; }
		public UriTemplateMatch Template { get; set; }
		public MethodInfo MethodInfo { get; set; }
		public object Service { get; set; }
		public Dictionary<string, ParameterInfo> Parameters { get; set; }
	}
}