﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.Serialization;
using System.Xml;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logging;
using KswApi.Logging.Framework;
using KswApi.Logging.Objects;
using KswApi.Logic;
using KswApi.Logic.Administration;
using KswApi.Service.Framework.Constants;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Web;
using KswApi.Service.Framework.Helpers;
using MessageFormat = KswApi.Service.Framework.Enums.MessageFormat;

namespace KswApi.Service.Framework
{
	public class OperationHandler
	{
		#region Private Fields

		private readonly OperationMapper _operationMap;
		private readonly Operation _operation;
		private readonly Serializer _serializer = new Serializer();

		#endregion

		#region Public Constructors

		public OperationHandler(HttpContext context, OperationMapper operationMap)
		{
			_operation = new Operation
			{
				Request = context.Request,
				Response = context.Response,
				OperationLog = new OperationLog(),
				Headers = context.Request.Headers
			};

			_operationMap = operationMap;
		}

		#endregion

		#region Public Methods

		public void Process()
		{
			_operation.OperationLog.Start();

			// prevent caching
			_operation.Response.CacheControl = "no-cache";
			_operation.Response.Headers.Remove("Server");

			// the log context allows log functionality
			// to add client id, etc to informational logs
			// without passing it through every business
			// logic method

			LogContext.SetCurrent(_operation.OperationLog);

			try
			{
				ExecuteOperation();
			}
			catch (Exception exception)
			{
				ErrorHandler handler = new ErrorHandler();

				handler.HandleException(exception, _operation);

				return;
			}
			finally
			{
				LogContext.RemoveCurrent();

				CurrentOperation.Remove();
			}

			_operation.OperationLog.Stop();

			if (ShouldLogOperation(_operation))
				Logger.Log(_operation.OperationLog);
		}

		#endregion

		#region Private Methods

		private bool ShouldLogOperation(Operation operation)
		{
			return operation.Allowed == null || operation.Allowed.Logging == AllowedLogging.Log ||
				   operation.Allowed.Logging == AllowedLogging.LogWithoutBody;
		}

		private void ExecuteOperation()
		{
			HttpRequest request = _operation.Request;

			_operation.SourceAddress = request.UserHostAddress;

			_operation.OperationLog.ClientAddress = _operation.SourceAddress;
			_operation.OperationLog.Path = request.Url.AbsoluteUri;
			_operation.OperationLog.Verb = request.HttpMethod;
			_operation.OperationLog.ResponseCode = (int)HttpStatusCode.OK;

			OAuthLogic authorizationLogic = new OAuthLogic();

			_operation.AccessToken = authorizationLogic.GetAccessTokenFromHeaders(request.Headers);

			ApplicationLogic applicationLogic = new ApplicationLogic();

			if (_operation.AccessToken != null)
				_operation.Application = authorizationLogic.GetApplicationByAccessToken(_operation.AccessToken);
			else // try to get by jsonp app id
				_operation.Application = applicationLogic.GetApplicationFromParameters(request.Params);

			if (_operation.Application != null)
				_operation.OperationLog.Client = _operation.Application.Id;

			CurrentOperation.Set(_operation);

			string verb = request.HttpMethod;

			string servicePath = GetServicePath();

			ServiceOperation serviceOperation = _operationMap.GetOperation(verb, servicePath);

			if (serviceOperation == null)
				throw new ServiceException(HttpStatusCode.NotFound, "The requested resource does not exist.");

			_operation.MethodInfo = serviceOperation.MethodInfo;
			_operation.Allowed = serviceOperation.Allowed;
			_operation.Format = serviceOperation.ResponseFormat;

			string callback = null;

			// get a case-insensitive set of properties
			NameValueCollection properties = new NameValueCollection();

			NameValueCollection requestParams = _operation.Request.Params;

			foreach (string key in requestParams)
			{
				foreach (string s in requestParams.GetValues(key))
					properties.Add(key.ToUpper(), s);
			}

			if (_operation.Format == MessageFormat.Jsonp)
			{
				callback = GetJsonpCallback();

				applicationLogic.ValidateJsonpCallback(_operation.Request.Headers, _operation.Application);
			}
			else
			{
				authorizationLogic.AuthorizeOperation(_operation.SourceAddress, _operation.AccessToken, _operation.Application,
													  _operation.Allowed);
			}

			object[] parameters = new object[serviceOperation.Parameters.Count];

			foreach (KeyValuePair<string, ParameterInfo> kvp in serviceOperation.Parameters)
			{
				Type type = kvp.Value.ParameterType;

				if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
					type = type.GetGenericArguments()[0];

				if (type.IsPrimitive || type == typeof(string) || type == typeof(Guid) || type.IsEnum || type.IsArray || type == typeof(DateTime))
				{
					string value = properties[kvp.Key];
					if (value == null)
						continue;

					object result = GetParameter(value, type, kvp.Value.Name);

					if (result != null)
						parameters[kvp.Value.Position] = result;
				}
				else if (type.IsClass && verb == "GET")
				{
					ConstructorInfo constructor = type.GetConstructor(Type.EmptyTypes);

					if (constructor == null)
						continue;

					object obj = constructor.Invoke(null);

					foreach (PropertyInfo propertyInfo in type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
					{
						if (!propertyInfo.CanWrite)
							continue;

						Type propertyType = propertyInfo.PropertyType;

						if (propertyType.IsGenericType && propertyType.GetGenericTypeDefinition() == typeof(Nullable<>))
							propertyType = propertyType.GetGenericArguments()[0];

						DataMemberAttribute attribute = propertyInfo.GetCustomAttribute<DataMemberAttribute>();
						string name = attribute != null && !string.IsNullOrEmpty(attribute.Name)
										  ? attribute.Name
										  : propertyInfo.Name;

						string value = properties[name.ToUpper()];

						if (value == null)
							continue;

						object result = GetParameter(value, propertyType, name);

						if (result != null)
							propertyInfo.SetValue(obj, result);
					}

					parameters[kvp.Value.Position] = obj;
				}
			}

			foreach (string key in serviceOperation.Template.BoundVariables)
			{
				ParameterInfo parameter;

				if (!serviceOperation.Parameters.TryGetValue(key, out parameter))
					continue;

				object result = GetParameter(serviceOperation.Template.BoundVariables[key], parameter.ParameterType, parameter.Name);

				if (result != null)
					parameters[parameter.Position] = result;
			}

			// parse the body, if it exists
			if (request.ContentLength > 0)
				EvaluateRequestBody(parameters, serviceOperation);

			Type returnType = serviceOperation.MethodInfo.ReturnType;

			if (returnType != typeof(StreamResponse))
			{
				object result = serviceOperation.MethodInfo.Invoke(serviceOperation.Service, parameters);

				if (returnType != typeof(void))
				{

					Serializer serializer = new Serializer();
					serializer.SerializeResponse(_operation.Format, result, serviceOperation.MethodInfo.ReturnType, _operation.Response,
												 callback);
				}
			}
			else
			{
				_operation.StreamResponse = new StreamResponse(_operation.Response);

				StreamResponse result = (StreamResponse)serviceOperation.MethodInfo.Invoke(serviceOperation.Service, parameters);

				// allow a null return: this would simply return a 200 ok with no body
				if (result == null)
					return;

				if (_operation.StreamResponse == result)
				{
					result.Flush();
					return;
				}

				if (_operation.StreamResponse.HasData)
					throw new InvalidOperationException("Method attempted to write to an operation stream response and return another stream response.");

				_operation.Response.ContentType = result.ContentType;
				result.CopyTo(_operation.Response.OutputStream);
			}
		}

		private string GetJsonpCallback()
		{
			if (_operation.Allowed == null || (_operation.Allowed.SpecialAccess & AllowedSpecialAccess.Jsonp) == AllowedSpecialAccess.None)
				throw new ServiceException(HttpStatusCode.BadRequest, "The specified operation does not support JSONP");

			string callbackName =
				_operation.Request.Params.AllKeys.FirstOrDefault(
					item => string.Equals(item, Constant.JSONP_CALLBACK_PARAMETER, StringComparison.OrdinalIgnoreCase));

			if (string.IsNullOrEmpty(callbackName))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.MISSING_JSONP_CALLBACK);

			string callback = _operation.Request.Params[callbackName];

			JsonpHelper.ValidateJsonpCallback(callback);

			return callback;
		}

		private void EvaluateRequestBody(object[] parameters, ServiceOperation operation)
		{
			ParameterInfo parameter = operation.Parameters.Values.FirstOrDefault(
				item => (item.ParameterType.IsClass && !(item.ParameterType == typeof(string))));

			bool logBody = operation.Allowed == null || operation.Allowed.Logging == AllowedLogging.Log || operation.Allowed.Logging == AllowedLogging.LogException;

			MessageFormat format = GetRequestFormat();

			if (format == MessageFormat.Unknown)
			{
				if (operation.Parameters.Values.All(item => item.ParameterType != typeof(StreamRequest)))
					throw new ServiceException(HttpStatusCode.BadRequest, string.Format("Invalid request format: {0}.", _operation.Request.ContentType));
				format = MessageFormat.Stream;
			}

			if (parameter == null)
			{
				StreamReader reader = new StreamReader(_operation.Request.InputStream);

				_operation.OperationLog.Body = logBody ? reader.ReadToEnd() : string.Empty;

				if (format != MessageFormat.Form)
					throw new ServiceException(HttpStatusCode.BadRequest, "The specified operation does not accept a body.");

				return;
			}

			object value;

			if (format == MessageFormat.Stream)
			{
				value = new StreamRequest(_operation.Request.InputStream)
				{
					ContentLength = _operation.Request.ContentLength,
					ContentType = _operation.Request.ContentType
				};
			}
			else
			{
				value = _serializer.DeserializeRequest(parameter.ParameterType,
													   format,
													   _operation.Request,
													   _operation.OperationLog);

				//The deseralization methods above will set the body, but if we don't want to log the body we need to clear it out here.
				_operation.OperationLog.Body = logBody ? _operation.OperationLog.Body : string.Empty;
			}

			parameters[parameter.Position] = value;
		}

		private MessageFormat GetRequestFormat()
		{
			string contentType = _operation.Request.ContentType;

			if (string.IsNullOrEmpty(contentType))
				return _operation.Format;

			string[] split = contentType.Split(';');

			switch (split[0])
			{
				case "application/json":
					return MessageFormat.Json;
				case "application/xml":
				case "text/xml":
					return MessageFormat.Xml;
				case "application/x-www-form-urlencoded":
					return MessageFormat.Form;
				default:
					return MessageFormat.Unknown;
			}
		}

		private object GetParameter(string value, Type type, string name)
		{
			if (type == typeof(string))
				return value;

			if (type.IsArray)
			{
				string[] split = value.Split(',');
				split = split.Select(item => item.Trim()).Where(item => !string.IsNullOrEmpty(item)).ToArray();

				if (split.Length == 0)
					return null;

				Type elementType = type.GetElementType();
				Array array = Array.CreateInstance(elementType, split.Length);

				int i = 0;
				foreach (string item in split)
				{
					array.SetValue(GetParameter(item, elementType, name), i);
					i++;
				}

				return array;
			}

			if (type.IsGenericType)
			{
				Type genericType = type.GetGenericTypeDefinition();

				if (genericType == typeof(Nullable<>))
				{
					if (string.IsNullOrEmpty(value))
						return null;

					type = Nullable.GetUnderlyingType(type);
				}
				else if (genericType == typeof(List<>))
				{
					string[] split = value.Split(',');
					split = split.Select(item => item.Trim()).Where(item => !string.IsNullOrEmpty(item)).ToArray();

					if (split.Length == 0)
						return null;

					Type elementType = type.GetGenericArguments()[0];

					ConstructorInfo constructor = type.GetConstructor(Type.EmptyTypes);

					if (constructor == null)
						return null;

					object list = constructor.Invoke(null);

					MethodInfo method = type.GetMethod("Add");

					foreach (string item in split)
					{
						method.Invoke(list, new[] { GetParameter(item, elementType, name) });
					}

					return list;
				}
			}


			if (type == typeof(Guid))
			{
				Guid guid;
				if (!Guid.TryParse(value, out guid))
					throw new ServiceException(HttpStatusCode.BadRequest, string.Format("The parameter value is not valid: {0}.", name));
				return guid;
			}

			if (type.IsPrimitive)
			{
				try
				{
					return Convert.ChangeType(value, type);
				}
				catch (InvalidCastException)
				{
					throw new ServiceException(HttpStatusCode.BadRequest, string.Format("The parameter value is not valid: {0}.", name));
				}
				catch (FormatException)
				{
					throw new ServiceException(HttpStatusCode.BadRequest, string.Format("The parameter value is not valid: {0}.", name));
				}
				catch (OverflowException)
				{
					throw new ServiceException(HttpStatusCode.BadRequest, string.Format("The parameter value is too large: {0}.", name));
				}
			}

			if (type.IsEnum)
			{
				try
				{
					object enumValue = Enum.Parse(type, value, true);
					if (!Enum.IsDefined(type, enumValue))
						throw new ServiceException(HttpStatusCode.BadRequest, string.Format("Unrecognized option for parameter: {0}.", name));
					return enumValue;
				}
				catch (Exception)
				{
					throw new ServiceException(HttpStatusCode.BadRequest, string.Format("Invalid value for parameter: {0}.", name));
				}
			}
            if (type == typeof(DateTime))
            {
                DateTime dateTime;
                if (!DateTime.TryParse(value, out dateTime))
                    throw new ServiceException(HttpStatusCode.BadRequest, string.Format("The parameter value is not valid: {0}.", name));
                return dateTime.ToUniversalTime();
            }

			if (typeof(ICollection).IsAssignableFrom(type))
			{
				// handle any collection classes here
				Type listInterface = type.GetInterfaces().FirstOrDefault(
						item => item.IsGenericType && item.GetGenericTypeDefinition() == typeof(IList<>));

				if (listInterface != null)
					return GetList(value, type, listInterface, name);
			}

			return null;
		}

		private object GetList(string value, Type type, Type enumerableInterface, string name)
		{
			Type elementType = enumerableInterface.GetGenericArguments()[0];

			ConstructorInfo constructor = type.GetConstructor(Type.EmptyTypes);

			if (constructor == null)
				return null;

			IList result = (IList)constructor.Invoke(null);

			string[] split = value.Split(',');
			split = split.Select(item => item.Trim()).Where(item => !string.IsNullOrEmpty(item)).ToArray();

			if (split.Length == 0)
				return null;

			foreach (string item in split)
			{
				result.Add(GetParameter(item, elementType, name));
			}

			return result;
		}

		private string GetServicePath()
		{
			string path = _operation.Request.CurrentExecutionFilePath;
			string applicationPath = _operation.Request.ApplicationPath;

			if (!string.IsNullOrEmpty(applicationPath))
				path = path.Substring(applicationPath.Length);

			return path;
		}

		#endregion
	}
}