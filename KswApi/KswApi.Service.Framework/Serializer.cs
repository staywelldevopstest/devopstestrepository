﻿using System;
using System.Net;
using System.Runtime.Serialization;
using System.Web;
using System.Web.Security.AntiXss;
using System.Xml;
using KswApi.Interface.Exceptions;
using KswApi.Logging.Objects;
using KswApi.Service.Framework.Enums;
using KswApi.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Formatting = Newtonsoft.Json.Formatting;

namespace KswApi.Service.Framework
{
	public class Serializer
	{
		#region Public Methods
		public void SerializeResponse(MessageFormat format, object result, Type type, HttpResponse response, string jsonpCallback = null)
		{
			switch (format)
			{
				case MessageFormat.Stream:
				case MessageFormat.Json:
					SerializeJson(result, response);
					break;
				case MessageFormat.Jsonp:
					SerializeJsonP(result, response, jsonpCallback);
					break;
				case MessageFormat.Xml:
					SerializeXml(result, type, response);
					break;
				case MessageFormat.Soap:
					break;
				default:
					throw new ArgumentOutOfRangeException(string.Format("Invalid response format: {0}", format));
			}
		}

		public object DeserializeRequest(Type type, MessageFormat format, HttpRequest request, OperationLog operationLog)
		{
			switch (format)
			{
				case MessageFormat.Json:
					return DeserializeJson(type, request, operationLog);
				case MessageFormat.Xml:
					return DeserializeXml(type, request, operationLog);
				case MessageFormat.Soap:
					return null;
				case MessageFormat.Form:
					return DeserializeForm(type, request);
				default:
					throw new ArgumentOutOfRangeException(string.Format("Unrecognized request format: {0}.", format));
			}
		}

		#endregion

		#region Private Methods

		#region Serialization

		private void SerializeJson(object result, HttpResponse response)
		{
			response.ContentType = "application/json";

			JsonSerializer serializer = new JsonSerializer
			{
				Formatting = Formatting.Indented,
				NullValueHandling = NullValueHandling.Ignore
			};

			serializer.Converters.Add(new StringEnumConverter());

			serializer.Serialize(response.Output, result);
		}

		private void SerializeJsonP(object result, HttpResponse response, string jsonpCallback)
		{
			response.ContentType = "application/javascript";

			JsonSerializer serializer = new JsonSerializer
			{
				Formatting = Formatting.Indented,
				NullValueHandling = NullValueHandling.Ignore
			};

			serializer.Converters.Add(new StringEnumConverter());

			// this is flagged by fortify as an issue, but we validate _jsonpCallback using a regex
			// before it is set, so we know it is a valid function name
			
			response.Output.Write(string.IsNullOrEmpty(jsonpCallback) ? "throw" : AntiXssEncoder.UrlEncode(jsonpCallback));

			response.Output.Write('(');
			serializer.Serialize(response.Output, result);
			response.Output.Write(");");
		}

		private void SerializeXml(object result, Type returnType, HttpResponse response)
		{
			response.ContentType = "application/xml";

			CustomXmlSerializer serializer = new CustomXmlSerializer(returnType);

			XmlWriterSettings xmlWriterSettings = new XmlWriterSettings { OmitXmlDeclaration = true };

			XmlWriter writer = XmlWriter.Create(response.Output, xmlWriterSettings);

			try
			{
				serializer.Serialize(writer, result);
			}
			catch (SerializationException exception)
			{
				// Serialization exceptions thrown by the custom xml serializer
				// have better detail on the issue involved, so we push that message
				// up using a ServiceException
				throw new ServiceException(HttpStatusCode.BadRequest, exception.Message);
			}

			writer.Flush();
		}

		#endregion

		#region Deserialization

		private object DeserializeJson(Type type, HttpRequest request, OperationLog operationLog)
		{
			JsonSerializer serializer = new JsonSerializer();

			LoggingTextReader reader = new LoggingTextReader(request.InputStream);

			object value;

			try
			{
				value = serializer.Deserialize(reader, type);
			}
			catch (JsonSerializationException)
			{
				operationLog.Body = reader.ToString();

				throw new ServiceException(HttpStatusCode.BadRequest, "The json in the request body is invalid.");
			}
			catch (JsonReaderException)
			{
				operationLog.Body = reader.ToString();

				throw new ServiceException(HttpStatusCode.BadRequest, "The json in the request body is invalid.");
			}

			operationLog.Body = reader.ToString();

			return value;
		}

		private object DeserializeForm(Type type, HttpRequest request)
		{
			FormSerializer serializer = new FormSerializer();

			return serializer.Deserialize(request.InputStream, type);
		}

		private object DeserializeXml(Type type, HttpRequest request, OperationLog operationLog)
		{
			CustomXmlSerializer serializer = new CustomXmlSerializer(type);

			LoggingTextReader reader = new LoggingTextReader(request.InputStream);

			XmlReader xmlReader = new XmlTextReader(reader);

			object value;

			try
			{
				value = serializer.Deserialize(xmlReader);

				operationLog.Body = reader.ToString();
			}
			catch (SerializationException exception)
			{
				operationLog.Body = reader.ToString();

				throw new ServiceException(HttpStatusCode.BadRequest, "The xml in the request body is invalid. " + exception.Message);
			}
			catch (InvalidOperationException)
			{
				operationLog.Body = reader.ToString();

				throw new ServiceException(HttpStatusCode.BadRequest, "The xml in the request body is invalid.");
			}

			return value;
		}

		#endregion

		#endregion

	}
}
