﻿namespace KswApi.Service.Framework.Enums
{
	public enum MessageFormat
	{
		Unknown,
		Json,
		Xml,
		Soap,
		Form,
		Jsonp,
		Stream
	}
}