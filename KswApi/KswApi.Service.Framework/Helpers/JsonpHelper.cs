﻿using System;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using KswApi.Interface.Exceptions;
using KswApi.Service.Framework.Constants;

namespace KswApi.Service.Framework.Helpers
{
	public static class JsonpHelper
	{
		public static string GetCallback(HttpRequest request, bool validate = false)
		{
			foreach (string param in request.Params)
			{
				if (string.Compare(param, Constant.JSONP_CALLBACK_PARAMETER, StringComparison.OrdinalIgnoreCase) == 0)
				{
					string callback = request.Params[param];
					if (validate)
						ValidateJsonpCallback(callback);
					return callback;
				}
			}

			if (validate)
				throw new ServiceException(HttpStatusCode.BadRequest, "JSONP requires a callback parameter.");

			return null;
		}

		public static void ValidateJsonpCallback(string callback)
		{
			if (string.IsNullOrEmpty(callback))
				throw new ServiceException(HttpStatusCode.BadRequest, ErrorMessage.MISSING_JSONP_CALLBACK);
			if (!Regex.IsMatch(callback, Constant.JSONP_VALIDATION_REGEX, RegexOptions.IgnoreCase))
				throw new ServiceException(HttpStatusCode.BadRequest, "Invalid JSONP callback parameter.");
		}

		public static bool IsValidCallback(string callback)
		{
			return !string.IsNullOrEmpty(callback) && Regex.IsMatch(callback, Constant.JSONP_VALIDATION_REGEX);
		}
	}
}
