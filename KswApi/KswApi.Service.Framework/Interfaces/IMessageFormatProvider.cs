﻿using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;

namespace KswApi.Service.Framework.Interfaces
{
	interface IMessageFormatProvider
	{
		IDispatchMessageFormatter GetReplyDispatchFormatter(OperationDescription operationDescription, ServiceEndpoint endpoint);
		IDispatchMessageFormatter GetRequestDispatchFormatter(OperationDescription operationDescription, ServiceEndpoint endpoint);
	}
}
