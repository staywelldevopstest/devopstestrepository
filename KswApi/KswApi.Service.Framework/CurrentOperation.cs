﻿using System.Collections.Concurrent;
using System.Threading;

namespace KswApi.Service.Framework
{
	internal class CurrentOperation
	{
		private static readonly ConcurrentDictionary<int, Operation> Operations = new ConcurrentDictionary<int, Operation>();

		public static void Set(Operation operation)
		{
			Operations.TryAdd(Thread.CurrentThread.ManagedThreadId, operation);
		}

		public static Operation Get()
		{
			Operation operation;
			
			Operations.TryGetValue(Thread.CurrentThread.ManagedThreadId, out operation);

			return operation;
		}

		public static void Remove()
		{
			Operation operation;
			Operations.TryRemove(Thread.CurrentThread.ManagedThreadId, out operation);
		}

		public static void ClearAll()
		{
			Operations.Clear();
		}
	}
}