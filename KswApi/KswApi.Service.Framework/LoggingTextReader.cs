﻿using System.IO;
using System.Text;

namespace KswApi.Service
{
	public class LoggingTextReader : StreamReader
	{
		private readonly StringBuilder _builder = new StringBuilder();

		public LoggingTextReader(Stream stream)
			: base(stream)
		{
			
		}

		public override int Read()
		{
			int value = base.Read();

			if (value > 0)
				_builder.Append((char) value);

			return value;
		}

		public override int Read(char[] buffer, int index, int count)
		{
			int read = base.Read(buffer, index, count);

			if (read > 0)
				_builder.Append(buffer, index, read);

			return read;
		}

		public override int ReadBlock(char[] buffer, int index, int count)
		{
			int read = base.ReadBlock(buffer, index, count);

			if (read > 0)
				_builder.Append(buffer, index, read);

			return read;
		}

		public override string ReadLine()
		{
			string line = base.ReadLine();

			_builder.Append(line);

			return line;
		}

		public override string ReadToEnd()
		{
			string value = base.ReadToEnd();

			_builder.Append(value);

			return value;
		}

		public override string ToString()
		{
			if (!EndOfStream)
				ReadToEnd();

			return _builder.ToString();
		}
	}

}