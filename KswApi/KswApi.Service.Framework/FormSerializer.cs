﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Reflection;
using System.Web;

namespace KswApi.Service.Framework
{
	public class FormSerializer
	{
		public object Deserialize(Stream stream, Type type)
		{
			ConstructorInfo constructor = type.GetConstructor(Type.EmptyTypes);
			if (constructor == null)
				return null;

			object value = constructor.Invoke(null);

			StreamReader reader = new StreamReader(stream);

			string data = reader.ReadToEnd();

			NameValueCollection parameters = HttpUtility.ParseQueryString(data);

			PropertyInfo[] properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

			foreach (PropertyInfo property in properties)
			{
				if (!property.CanRead || !property.CanWrite)
					continue;
				
				string parameterValue = parameters[property.Name];

				if (parameterValue != null)
				{
					object convertedValue = GetValue(parameterValue, property.PropertyType);
					if (convertedValue != null)
						property.SetValue(value, convertedValue, null);
				}
			}

			return value;
		}

		private object GetValue(string value, Type type)
		{
			if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
			{
				return GetValue(value, Nullable.GetUnderlyingType(type));
			}

			if (type == typeof(string))
				return value;

			if (type == typeof(Guid))
			{
				Guid guid;
				if (Guid.TryParse(value, out guid))
					return guid;
			}
			else if (type == typeof(DateTime))
			{
				DateTime dateTime;

				if (DateTime.TryParse(value, out dateTime))
					return dateTime.ToUniversalTime();
			}
			else
			{
				return Convert.ChangeType(value, type);
			}

			return null;
		}
	}
}
