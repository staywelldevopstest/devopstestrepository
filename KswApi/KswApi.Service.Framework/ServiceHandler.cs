﻿using System;
using System.Web;

namespace KswApi.Service.Framework
{
	public class ServiceHandler : IHttpHandler
	{
		private static readonly OperationMapper Map = new OperationMapper();

		public void StartService(Type serviceType)
		{
			Map.RegisterService(serviceType);
		}

		public void ProcessRequest(HttpContext context)
		{
			OperationHandler operationHandler = new OperationHandler(context, Map);
			
			operationHandler.Process();
		}

		public bool IsReusable
		{
			get { return true; }
		}
	}
}