﻿namespace KswApi.Common.Enums
{
	// ReSharper disable InconsistentNaming
	public enum EnvironmentName
	{
		None = 0,
		Debug = 1,
		EA = 2,
		Staging = 3,
		Production  = 4
	}
}
