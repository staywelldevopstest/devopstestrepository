﻿using System;

namespace KswApi.Common.Interfaces
{
	public interface ICache
	{
		void Clear();
		void Set<T>(string key, T value);
		void Set<T>(string key, T value, TimeSpan expiration);
		T Get<T>(string key);
		void Remove(string key);
	}
}
