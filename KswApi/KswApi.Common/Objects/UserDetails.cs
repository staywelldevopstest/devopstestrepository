﻿using System;
using KswApi.Interface.Enums;

namespace KswApi.Common.Objects
{
	public class UserDetails
	{
		public Guid Id { get; set; }
        public string Username { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string EmailAddress { get; set; }
		public string MiscInfo { get; set; }
		public UserType Type { get; set; }
	}
}
