﻿using System;
using KswApi.Interface.Enums;

namespace KswApi.Common.Objects
{
	[Serializable]
	public class QueuedTask
	{
		public TaskType Type { get; set; }
		public object Data { get; set; }
	}
}
