﻿using System;
using System.Collections.Generic;

namespace KswApi.Common.Objects
{
    [Serializable]
    public class AccessToken
    {
        public Guid Id { get; set; }
        public Guid ApplicationId { get; set; }
        public Guid? LicenseId { get; set; }
        public string Token { get; set; }
        public AccessTokenType TokenType { get; set; }
        public DateTime Expiration { get; set; }
        public List<string> Rights { get; set; }
        public string Scope { get; set; }
		public UserDetails UserDetails { get; set; }
		public string State { get; set; }
		public DateTime? WebPortalTimeout { get; set; }
		public Guid? RefreshTokenId { get; set; }
    }
}
