﻿using System.Collections.Generic;

namespace KswApi.Common.Objects
{
	public class UserAuthenticationDetails : UserDetails
	{
		public List<string> Roles { get; set; }
    }
}
