﻿namespace KswApi.Common.Objects
{
    public enum AccessTokenType
    {
        None = 0,
        AuthorizationClient = 1,
        Client = 2,
        InternalUser = 3
    }
}
