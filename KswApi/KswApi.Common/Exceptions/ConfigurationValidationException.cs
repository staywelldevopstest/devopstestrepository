﻿using System;

namespace KswApi.Common.Exceptions
{
	public class ConfigurationValidationException : Exception
	{
		public ConfigurationValidationException(Type uninitializedType)
			: base("Type was not initialized: " + uninitializedType.Name)
		{
		}
	}
}
