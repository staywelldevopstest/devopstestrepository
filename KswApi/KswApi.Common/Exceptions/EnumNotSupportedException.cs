﻿using System;

namespace KswApi.Common.Exceptions
{
	public class EnumNotSupportedException : Exception
	{
		#region Private Fields

		private const string MESSAGE = "Enum of type '{0}' and value of '{1}' is not supported in this context.";

		#endregion

		#region Initializers

		public EnumNotSupportedException(Enum value)
			: base(string.Format(MESSAGE, value.GetType().FullName, value)) { }

		public EnumNotSupportedException(Enum value, Exception innerException)
			: base(string.Format(MESSAGE, value.GetType().FullName, value), innerException) { }

		#endregion
	}
}
