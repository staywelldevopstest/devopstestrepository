﻿namespace KswApi.Common.Configuration
{
	public class EmailMessageGroupConfiguration
	{
		public EmailMessageConfiguration SetPassword { get; set; }

		public EmailMessageConfiguration ResetPassword { get; set; }
	}
}
