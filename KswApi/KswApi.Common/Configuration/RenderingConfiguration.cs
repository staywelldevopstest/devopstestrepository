﻿using System;
using System.Collections.Generic;

namespace KswApi.Common.Configuration
{
	public class RenderingConfiguration
	{
		public string Mode { get; set; }
		public Type Type { get; set; }
		public IDictionary<string, string> Options { get; set; }
	}
}
