﻿using System;

namespace KswApi.Common.Configuration
{
	public class StopCommandConfiguration
	{
		private const int DEFAULT_EXPIRATION_IN_DAYS = 5;

		public StopCommandConfiguration()
		{
			Expiration = TimeSpan.FromDays(DEFAULT_EXPIRATION_IN_DAYS);
		}

		public TimeSpan Expiration { get; set; }
	}
}
