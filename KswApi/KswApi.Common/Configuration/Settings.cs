﻿using System.Collections.Generic;
using KswApi.Common.Enums;
using KswApi.Ioc;

namespace KswApi.Common.Configuration
{
	public class Settings
	{
		public Settings()
		{
			AccessToken = new AccessTokenConfiguration();
			RefreshToken = new RefreshTokenConfiguration();
			WebPortal = new WebPortalConfiguration();
			Cache = new CacheConfiguration();
			Database = new DatabaseConfiguration();
			SmsGateway = new SmsGatewayConfiguration();
			Index = new IndexConfiguration();
			EmailMessages = new EmailMessageGroupConfiguration();
		}

		public static Settings Current
		{
			get { return Dependency.Get<Settings>(); } 
		}

		public EnvironmentName Environment { get; set; }
		public AccessTokenConfiguration AccessToken { get; private set; }
		public RefreshTokenConfiguration RefreshToken { get; private set; }
		public WebPortalConfiguration WebPortal { get; private set; }
		public CacheConfiguration Cache { get; private set; }
		public DatabaseConfiguration Database { get; private set; }
		public IndexConfiguration Index { get; private set; }
		public EmailMessageGroupConfiguration EmailMessages { get; private set; }

		//public string ServiceUri 

		// Web Portal
		public string WebPortalBaseUri { get; set; }
		public string WebPortalHomeUri { get; set; }
		public string WebPortalClientSecret { get; set; }
		public string WebPortalClientIps { get; set; }
		public string ServiceUri { get; set; }

		// Auxiliary
		public string AuxiliaryClientSecret { get; set; }
		public string AuxiliaryClientIps { get; set; }

		public string ActiveDirectoryDomain { get; set; }
		public string ActiveDirectoryUser { get; set; }
		public string ActiveDirectoryPassword { get; set; }
		public string TwilioAccountSid { get; set; }
		public string TwilioAuthToken { get; set; }
		public string TwilioTestAccountSid { get; set; }
		public string TwilioTestAuthToken { get; set; }
		public SmsGatewayConfiguration SmsGateway { get; private set; }
		public bool ShowFullExceptions { get; set; }

		public List<TaskConfiguration> Tasks { get; set; }
		public string SmtpServer { get; set; }

		public string ThreeMHddConnectionString { get; set; }

		public List<RenderingConfiguration> Renderers { get; set; }
	}
}
