﻿namespace KswApi.Common.Configuration
{
	public class EmailMessageConfiguration
	{
		public string From { get; set; }

		public string Subject { get; set; }

		public string Body { get; set; }
	}
}
