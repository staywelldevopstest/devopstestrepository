﻿namespace KswApi.Common.Configuration
{
	public class IndexConfiguration
	{
		public string Uri { get; set; }
		public string UserName { get; set; }
		public string Password { get; set; }
		public string Name { get; set; }
		public string Reindex { get; set; }
	}
}
