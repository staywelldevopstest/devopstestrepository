﻿using System;
using KswApi.Common.Constants;

namespace KswApi.Common.Configuration
{
	public class WebPortalConfiguration
	{
		public WebPortalConfiguration()
		{
			Timeout = TimeSpan.FromMinutes(Constant.WEB_PORTAL_TIMEOUT_IN_MINUTES);
		}

		public TimeSpan Timeout { get; set; }
		public TimeSpan TimeoutCountdown { get; set; }
		public TimeSpan TimeoutPollInterval { get; set; }
	}
}
