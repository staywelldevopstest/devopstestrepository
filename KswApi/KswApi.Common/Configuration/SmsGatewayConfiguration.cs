﻿namespace KswApi.Common.Configuration
{
	public class SmsGatewayConfiguration
	{
		public SmsGatewayConfiguration()
		{
			StopCommand = new StopCommandConfiguration();	
		}

		public StopCommandConfiguration StopCommand { get; private set; }
	}
}
