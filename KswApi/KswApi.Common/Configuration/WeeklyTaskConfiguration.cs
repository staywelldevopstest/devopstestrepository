﻿using System;
using System.Collections.Generic;

namespace KswApi.Common.Configuration
{
	public class WeeklyTaskConfiguration
	{
		public List<DayOfWeek> Days { get; set; }
		public DateTime Time { get; set; }
	}
}
