﻿using System;

namespace KswApi.Common.Configuration
{
	public class AccessTokenConfiguration
	{
		private const int DEFAULT_EXPIRATION_IN_MINUTES = 30;

		public AccessTokenConfiguration()
		{
			Expiration = TimeSpan.FromMinutes(DEFAULT_EXPIRATION_IN_MINUTES);
		}

		public TimeSpan Expiration { get; set; }
	}
}
