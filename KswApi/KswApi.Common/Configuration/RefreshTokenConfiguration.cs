﻿using System;

namespace KswApi.Common.Configuration
{
	public class RefreshTokenConfiguration
	{
		public RefreshTokenConfiguration()
		{
			// Default to 24 hours
			Expiration = TimeSpan.FromHours(24);
		}

		public TimeSpan Expiration { get; set; }
	}
}
