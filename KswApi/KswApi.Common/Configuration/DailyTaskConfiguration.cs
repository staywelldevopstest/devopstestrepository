﻿using System;

namespace KswApi.Common.Configuration
{
	public class DailyTaskConfiguration
	{
		public DateTime Time { get; set; }
	}
}
