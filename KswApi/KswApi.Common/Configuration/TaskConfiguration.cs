﻿using System;
using KswApi.Interface.Enums;

namespace KswApi.Common.Configuration
{
	public class TaskConfiguration
	{
		public TaskType Type { get; set; }
		public bool Active { get; set; }
		public WeeklyTaskConfiguration WeeklyOccurrence { get; set; }
		public DailyTaskConfiguration DailyOccurrence { get; set; }
		public TimeSpan Duration { get; set; }
	}
}
