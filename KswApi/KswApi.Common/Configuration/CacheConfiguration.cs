﻿namespace KswApi.Common.Configuration
{
	public class CacheConfiguration
	{
		public string Prefix { get; set; }
		public CacheType Type { get; set; }
		public string Name { get; set; }
	}
}
