﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace KswApi.Common.Extensions
{
	public static class ExpressionExtensions
	{
		public static PropertyInfo GetProperty<T, TProperty>(this Expression<Func<T, TProperty>> propertyExpression)
		{
			MemberExpression memberExpression = propertyExpression.Body as MemberExpression;

			if (memberExpression == null)
				throw new ArgumentException("Property is not a valid property", "propertyExpression");

			PropertyInfo propertyInfo = memberExpression.Member as PropertyInfo;

			if (propertyInfo == null)
				throw new ArgumentException("Property is not a valid property", "propertyExpression");

			return propertyInfo;
		}
	}
}
