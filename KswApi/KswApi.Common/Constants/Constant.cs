﻿namespace KswApi.Common.Constants
{
    class Constant
    {
        public const int TOKEN_LENGTH = 64;
        public const int CLIENT_SECRET_LENGTH = 64;
		public const int WEB_PORTAL_TIMEOUT_IN_MINUTES = 20;
    }
}
