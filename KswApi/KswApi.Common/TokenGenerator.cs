﻿using System;
using System.Security.Cryptography;
using KswApi.Common.Constants;

namespace KswApi.Common
{
	public static class TokenGenerator
	{
		public static string CreateToken()
		{
			Guid guid = Guid.NewGuid();

			byte[] guidBytes = guid.ToByteArray();

			byte[] salt = new byte[Constant.TOKEN_LENGTH - guidBytes.Length];

			RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();

			provider.GetBytes(salt);

			byte[] token = new byte[Constant.TOKEN_LENGTH];

			guidBytes.CopyTo(token, 0);
			Array.Copy(salt, 0, token, guidBytes.Length, Constant.TOKEN_LENGTH - guidBytes.Length);

			// convert to base 64, make it valid for a url
			return Convert.ToBase64String(token).Substring(0, Constant.TOKEN_LENGTH).Replace('/', 'A').Replace('+', 'B');
		}

		public static string CreateSecret()
		{
			byte[] token = new byte[Constant.CLIENT_SECRET_LENGTH];

			RNGCryptoServiceProvider provider = new RNGCryptoServiceProvider();

			provider.GetBytes(token);
			
			// convert to base 64, make it valid for a url
			return Convert.ToBase64String(token).Substring(0, Constant.CLIENT_SECRET_LENGTH).Replace('/', 'A').Replace('+', 'B');
		}
	}
}
