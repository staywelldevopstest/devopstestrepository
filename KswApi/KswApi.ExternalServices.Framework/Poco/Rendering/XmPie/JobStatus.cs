﻿using System.Collections.Generic;
using KswApi.ExternalServices.Framework.Enums.Rendering.XmPie;

namespace KswApi.ExternalServices.Framework.Poco.Rendering.XmPie
{
	public class JobStatus
	{
		public JobState State { get; private set; }
		public IEnumerable<string> Messages { get; private set; }

		public JobStatus(JobState state, IEnumerable<string> messages)
		{
			State = state;
			Messages = messages;
		}
	}
}