﻿using System.ServiceModel;

namespace KswApi.ExternalServices.Framework.WcfServices
{
	/// <summary>
	/// Interface for the ClientChannelFactory wrapper class.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public interface IClientChannelFactory<out T> where T : IClientChannel
	{
		void Open();
		void Close();
		void Abort();
		T CreateChannel();
	}
}