﻿using System.ServiceModel;

namespace KswApi.ExternalServices.Framework.WcfServices
{
	public delegate void UseServiceDelegate<in T>(T proxy);

	public delegate TResult UseServiceDelegate<out TResult, in T>(T proxy);

	/// <summary>
	/// A class desgined to encapsulate the requirement of using try/catch blocks with WCF services, allowing the usage
	/// to be closer to a using() statement, while not going as far as allowing the developer to use a using()
	/// statement (this is to discourage associating the two).
	///
	/// http://stackoverflow.com/questions/573872/what-is-the-best-workaround-for-the-wcf-client-using-block-issue
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class Service<T> where T : IClientChannel
	{
		private readonly IClientChannelFactory<T> _channelFactory;

		public Service(IClientChannelFactory<T> channelFactory)
		{
			_channelFactory = channelFactory;
		}

		public void Use(UseServiceDelegate<T> codeBlock)
		{
			IClientChannel proxy = _channelFactory.CreateChannel();
			proxy.Open();
			bool success = false;
			try
			{
				codeBlock((T)proxy);
				proxy.Close();
				success = true;
			}
			finally
			{
				if (!success)
					proxy.Abort();
			}
		}

		public TResult Use<TResult>(UseServiceDelegate<TResult, T> codeBlock)
		{
			IClientChannel proxy = _channelFactory.CreateChannel();
			proxy.Open();
			bool success = false;
			try
			{
				TResult rval = codeBlock((T)proxy);
				proxy.Close();
				success = true;
				return rval;
			}
			finally
			{
				if (!success)
					proxy.Abort();
			}
		}
	}
}