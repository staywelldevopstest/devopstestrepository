﻿using System.ServiceModel;
using System.ServiceModel.Channels;

namespace KswApi.ExternalServices.Framework.WcfServices
{
	/// <summary>
	/// A wrapper class for the .NET Framework's ChannelFactory&lt;&gt; class, to allow mocking ChannelFactory&lt;&gt;
	/// in unit tests.
	/// </summary>
	/// <typeparam name="TChannel"></typeparam>
	public class ClientChannelFactory<TChannel> : IClientChannelFactory<TChannel> where TChannel : IClientChannel
	{
		private readonly ChannelFactory<TChannel> _factory;

		public ClientChannelFactory()
		{
			_factory = new ChannelFactory<TChannel>();
		}

		public ClientChannelFactory(Binding binding, EndpointAddress endpointAddress)
		{
			_factory = new ChannelFactory<TChannel>(binding, endpointAddress);
		}

		public ClientChannelFactory(string endpointConfigurationName)
		{
			_factory = new ChannelFactory<TChannel>(endpointConfigurationName);
		}

		public ClientChannelFactory(string endpointConfigurationName, string remoteAddress)
		{
			_factory = new ChannelFactory<TChannel>(endpointConfigurationName, new EndpointAddress(remoteAddress));
		}

		public void Open()
		{
			_factory.Open();
		}

		public void Close()
		{
			_factory.Close();
		}

		public void Abort()
		{
			_factory.Abort();
		}

		public TChannel CreateChannel()
		{
			return _factory.CreateChannel();
		}
	}
}