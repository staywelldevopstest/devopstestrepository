﻿using System.ComponentModel;

namespace KswApi.ExternalServices.Framework.Enums.Rendering.XmPie
{
	public enum JobType
	{
		[Description("PROOF")]
		Proof,
		[Description("PRINT")]
		Print
	}
}