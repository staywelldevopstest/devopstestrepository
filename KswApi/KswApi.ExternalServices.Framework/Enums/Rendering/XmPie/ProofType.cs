﻿using System.ComponentModel;

namespace KswApi.ExternalServices.Framework.Enums.Rendering.XmPie
{
	public enum ProofType
	{
		[Description("JPG")]
		Jpg,
		[Description("PDF")]
		Pdf,
		[Description("PDFO")]
		PdfOptimized
	}
}