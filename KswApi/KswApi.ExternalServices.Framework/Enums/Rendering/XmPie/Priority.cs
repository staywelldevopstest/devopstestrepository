namespace KswApi.ExternalServices.Framework.Enums.Rendering.XmPie
{
	public enum Priority
	{
		Lowest,
		VeryLow,
		Low,
		Normal,
		AboveNormal,
		High,
		VeryHigh,
		Highest
	}
}