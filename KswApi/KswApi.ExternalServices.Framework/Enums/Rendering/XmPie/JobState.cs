namespace KswApi.ExternalServices.Framework.Enums.Rendering.XmPie
{
	public enum JobState
	{
		Waiting = 1,
		InProgress,
		Completed,
		Failed,
		Aborting,
		Aborted,
		Deployed,
		Suspended
	}
}