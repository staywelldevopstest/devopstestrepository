namespace KswApi.ExternalServices.Framework.Enums.Rendering.XmPie
{
	public enum Media
	{
		Print = 1,
		Web,
		Email
	}
}