using System.ComponentModel;

namespace KswApi.ExternalServices.Framework.Enums.Rendering.XmPie
{
	public enum DataSourceType
	{
		[Description("MDB")]
		Mdb,
		[Description("DBF")]
		Dbf,
		[Description("XLS")]
		Xls,
		[Description("TXT")]
		Txt,
		[Description("FOXP")]
		Foxp,
		[Description("XML")]
		Xml,
		[Description("CNT")]
		Cnt,
		[Description("MSQL")]
		Msql,
		[Description("ORA")]
		Ora,
		[Description("DB2")]
		Db2,
		[Description("INFX")]
		Infx,
		[Description("YSQL")]
		YSql,
		[Description("SYB")]
		Syb,
		[Description("CONN")]
		Conn,
		[Description("NONE")]
		None
	}
}