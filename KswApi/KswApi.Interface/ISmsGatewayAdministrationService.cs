﻿using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.SmsGateway;

namespace KswApi.Interface
{
	[ServiceContract(Name = "Administration", Namespace = "http://www.kramesstaywell.com")]
	public interface ISmsGatewayAdministrationService
	{
		#region Sms Gateway Module Methods

		[WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/SmsGateway", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_License")]
		SmsGatewayModule GetSmsGateway(string licenseId);

		[WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/SmsGateway/ShortCodes/{shortCodeNumber}", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_License")]
		ShortCode GetShortCode(string licenseId, string shortCodeNumber);

		[WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/SmsGateway/ShortCodes", Method = "POST")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
		ShortCode CreateShortCode(string licenseId, ShortCode shortCode);

		[WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/SmsGateway/ShortCodes/{shortCodeNumber}", Method = "PUT")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
		ShortCode UpdateShortCode(string licenseId, string shortCodeNumber, ShortCode shortCode);

		[WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/SmsGateway/ShortCodes/{shortCodeNumber}", Method = "DELETE")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
		ShortCode DeleteShortCode(string licenseId, string shortCodeNumber);

		[WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/SmsGateway/AvailableLongCodes", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
		AvailableLongCodeList SearchLongCodes(
			string licenseId,
			[MessageParameter(Name = "$skip")]int offset,
			[MessageParameter(Name = "$top")] int count,
			[MessageParameter(Name = "$filter")] string filter,
			[MessageParameter(Name = "$filterType")] SmsLongCodeFilterType filterType);

		[WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/SmsGateway/LongCodes/{longCodeNumber}", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_License")]
		LongCode GetLongCode(string licenseId, string longCodeNumber);

		[WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/SmsGateway/LongCodes", Method = "POST")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
		LongCode PurchaseLongCode(string licenseId, LongCode longCode);

		[WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/SmsGateway/LongCodes/{longCodeNumber}", Method = "PUT")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
		LongCode UpdateLongCode(string licenseId, string longCodeNumber, LongCode longCode);

		[WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/SmsGateway/LongCodes/{longCodeNumber}", Method = "DELETE")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
		LongCode DeleteLongCode(string licenseId, string longCodeNumber);

		#endregion Sms Gateway Module Methods
	}
}
