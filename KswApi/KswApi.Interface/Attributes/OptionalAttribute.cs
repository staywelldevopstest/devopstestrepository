﻿using System;

namespace KswApi.Interface.Attributes
{
	[AttributeUsage(AttributeTargets.Property, AllowMultiple = false)]
	public class OptionalAttribute : Attribute
	{
	}
}
