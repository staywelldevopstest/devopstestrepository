﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("LicenseDetails")]
	public class ClientLicenseDetail
	{
		public Guid ClientId { get; set; }
		public string ClientName { get; set; }

		[XmlArrayItem("License")]
		public List<LicenseDetail> Licenses { get; set; }
		public int LicenseCount { get; set; }
	}
}
