﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("PasswordReset")]
	public class PasswordResetRequest
	{
		public string Email { get; set; }
	}
}
