using System;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects
{
    public class Task
    {
        public bool Active { get; set; }
        public TaskType Type { get; set; }
		public TimeSpan Duration { get; set; }
        public RecurringPatternType Recurrence { get; set; }
        public DailyRecurringSettings DailyRecurrence { get; set; }
        public HourlyRecurrence HourlyRecurrence { get; set; }
    }
}
