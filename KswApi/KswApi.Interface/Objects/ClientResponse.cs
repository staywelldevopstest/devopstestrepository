﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects
{
	public class ClientResponse : ClientRequest
	{
		public Guid Id { get; set; }
		public ClientStatus Status { get; set; }
		public DateTime DateAdded { get; set; }

		[XmlArrayItem("License")]
		public List<Guid> Licenses { get; set; }
	}
}
