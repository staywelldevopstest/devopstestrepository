﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
    [Serializable]
    [XmlRoot(ElementName = "Licenses")]
    public class LicenseList : SortedResultList<License>
    {

    }
}
