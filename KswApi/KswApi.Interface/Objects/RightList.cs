﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[Serializable]
	[XmlRoot("Rights")]
	public class RightList : ResultList<string>
	{

	}
}
