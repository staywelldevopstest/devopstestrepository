﻿namespace KswApi.Interface.Objects
{
	public class StatusResponse
	{
		public string Status { get; set; }
	}
}
