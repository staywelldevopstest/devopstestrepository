﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.SmsGateway
{
    [XmlType("SmsMessage")]
    public class IncomingMessage
    {
        public string From { get; set; }
        public string To { get; set; }
        public string Keyword { get; set; }
        public string Body { get; set; }
    }
}
