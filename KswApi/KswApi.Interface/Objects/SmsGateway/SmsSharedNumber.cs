﻿using System;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects.SmsGateway
{
	public class SmsSharedNumber
	{
		public Guid Id { get; set; }
		public string Number { get; set; }
		public SmsNumberType Type { get; set; }
	}
}
