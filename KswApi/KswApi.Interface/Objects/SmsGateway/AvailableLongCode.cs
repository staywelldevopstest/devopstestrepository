﻿namespace KswApi.Interface.Objects.SmsGateway
{
	public class AvailableLongCode
	{
		public string Number { get; set; }
		public string Region { get; set; }
		public string State { get; set; }
		public string PostalCode { get; set; }
		public string Country { get; set; }
	}
}
