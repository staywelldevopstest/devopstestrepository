﻿using System.Collections.Generic;

namespace KswApi.Interface.Objects.SmsGateway
{
	public class ShortCodeList : ResultList<LongCode>
	{
		public List<SmsSharedNumber> Available { get; set; }
	}
}
