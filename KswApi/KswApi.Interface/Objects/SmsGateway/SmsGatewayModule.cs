﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects.SmsGateway
{
	public class SmsGatewayModule
	{
		[XmlArrayItem("ShortCode")]
		public List<ShortCode> ShortCodes { get; set; }

		[XmlArrayItem("LongCode")]
		public List<LongCode> LongCodes { get; set; }

		[XmlArrayItem("ShortCode")]
		public List<string> AvailableShortCodes { get; set; }
	}
}
