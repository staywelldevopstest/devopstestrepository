﻿namespace KswApi.Interface.Objects.SmsGateway
{
	public class SmsAvailableNumber
	{
		public string Number { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string PostalCode { get; set; }
		public string Country { get; set; }
	}
}
