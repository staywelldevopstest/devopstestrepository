﻿using System;
using System.Xml.Serialization;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects.SmsGateway
{
	[Serializable]
	[XmlType("Keyword")]
	public class SmsKeyword
	{
		public string Value { get; set; }
		public string Website { get; set; }
		public MessageFormat Format { get; set; }
	}
}
