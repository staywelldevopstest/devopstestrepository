﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects.SmsGateway
{
	[XmlRoot("ShortCode")]
	public class ShortCode
	{
		public string Number { get; set; }

		[XmlArrayItem("Keyword")]
		public List<SmsKeyword> Keywords { get; set; }
	}
}
