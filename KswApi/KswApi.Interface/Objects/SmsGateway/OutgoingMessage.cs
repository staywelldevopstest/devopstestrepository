﻿using System.Xml.Serialization;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects.SmsGateway
{
	[XmlType("SmsMessage")]
	public class OutgoingMessage : IncomingMessage
	{
		public SmsMessageAction Action { get; set; }
	}
}
