﻿using System.Xml.Serialization;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects.SmsGateway
{
	[XmlRoot("LongCode")]
	public class LongCode : ShortCode
	{
		public SmsNumberRouteType RouteType { get; set; }
		public MessageFormat Format { get; set; }
		public string Website { get; set; }
	}
}
