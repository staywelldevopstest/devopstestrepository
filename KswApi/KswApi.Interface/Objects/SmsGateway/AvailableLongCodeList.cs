﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects.SmsGateway
{
	[Serializable]
	[XmlRoot(ElementName = "AvailableLongCodes")]
	public class AvailableLongCodeList : SortedResultList<AvailableLongCode>
	{
	}
}
