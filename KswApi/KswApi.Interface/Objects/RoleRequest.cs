﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("Role")]
	public class RoleRequest
	{
		public string Name { get; set; }
		public string Description { get; set; }
		
		[XmlArrayItem("Right")]
		public List<string> Rights { get; set; }
	}
}
