﻿namespace KswApi.Interface.Objects
{
	public class ClientUserUpdateRequest : ClientUserRequest
	{
		public bool ResetPassword { get; set; }
	}
}
