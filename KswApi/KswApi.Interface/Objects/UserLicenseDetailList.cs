﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	public class UserLicenseDetailList
	{
		[XmlArrayItem("UserLicenseDetail")]
		public List<UserLicenseDetail> Licenses { get; set; }
	}
}
