
using System.Collections.Generic;

namespace KswApi.Interface.Objects
{
    public class TaskServiceSettings
    {
        public int NumberOfThreads { get; set; }
        public int HeartBeatInMinutes { get; set; }
        public List<Task> Tasks { get; set; }
    }
}
