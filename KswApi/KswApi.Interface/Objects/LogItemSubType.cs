﻿
namespace KswApi.Interface.Objects
{
    public enum LogItemSubType
    {
        Operation = 1,
        Exception = 2,
        Warning = 3,
        Information = 4,
        Security = 5
    }
}
