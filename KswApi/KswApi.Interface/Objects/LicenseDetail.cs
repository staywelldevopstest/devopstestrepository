﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	public class LicenseDetail
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public List<string> Modules { get; set; }

		[XmlArrayItem("ChildLicense")]
		public List<LicenseDetail> ChildLicenses { get; set; }

		[XmlArrayItem("Application")]
		public List<ApplicationDetail> Applications { get; set; }
	}
}
