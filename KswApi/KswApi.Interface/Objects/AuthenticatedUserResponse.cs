﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlType("User")]
	public class AuthenticatedUserResponse : UserResponse
	{
		[XmlArrayItem("Right")]
		public List<string> Rights { get; set; }

		public int TimeoutInMilliseconds { get; set; }
		public int TimeoutCountdownInMilliseconds { get; set; }
		public int TimeoutPollIntervalInMilliseconds { get; set; }
		
	}
}
