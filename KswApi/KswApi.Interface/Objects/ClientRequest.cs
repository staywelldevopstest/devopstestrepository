﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot(ElementName = "Client")]
	public class ClientRequest
	{
		public string ClientName { get; set; }
		public string ClientWebSite { get; set; }
		public string ContactName { get; set; }
		public string ContactEmail { get; set; }
		public string ContactPhone { get; set; }
		public string Notes { get; set; }
	}
}
