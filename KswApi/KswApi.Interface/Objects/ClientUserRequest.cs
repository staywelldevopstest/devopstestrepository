﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects
{
	[XmlRoot("ClientUser")]
	public class ClientUserRequest
	{
		public string EmailAddress { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string PhoneNumber { get; set; }
		public Guid ClientId { get; set; }
		public bool Disabled { get; set; }
		public ClientUserPermissionType PermissionType { get; set; }
		public List<ClientUserLicense> Licenses { get; set; }
	}
}
