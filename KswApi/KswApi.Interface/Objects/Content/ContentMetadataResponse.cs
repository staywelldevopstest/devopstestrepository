﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects.Content
{
	[XmlType("Content")]
	public class ContentMetadataResponse : ContentBase
	{
		public Guid Id { get; set; }

		public ContentBucketReference Bucket { get; set; }

		public string OriginName { get; set; }

		public DateTime DateAdded { get; set; }

		public DateTime DateModified { get; set; }

		public DateTime DateUpdated { get; set; }

		public ContentType Type { get; set; }

		public string Title { get; set; }

		public string InvertedTitle { get; set; }

		[XmlArrayItem("AlternateTitle")]
		public List<string> AlternateTitles { get; set; }

		public string Slug { get; set; }

		public string Blurb { get; set; }

		public Language Language { get; set; }

		public bool Master { get; set; }

		public Guid MasterId { get; set; }

		public string MasterSlug { get; set; }

		public List<ContentVersionItem> Versions { get; set; }

		public List<ContentTaxonomyList> Taxonomies { get; set; }

		public List<HierarchicalTaxonomyResponse> Servicelines { get; set; }

		public List<CustomAttribute> CustomAttributes { get; set; }

		public string LegacyId { get; set; }

		public bool Published { get; set; }
	}
}
