﻿using System;

namespace KswApi.Interface.Objects.Content
{
	public class PrintOriginatingSource
	{
		public string Title { get; set; }
		public DateTime Date { get; set; }
	}
}
