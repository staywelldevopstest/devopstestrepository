﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
    [XmlRoot("Collections")]
    public class CollectionListResponse : PagedResultList<CollectionResponse>
    {

    }
}

