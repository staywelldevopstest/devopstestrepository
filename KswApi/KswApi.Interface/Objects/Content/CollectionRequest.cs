﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	[XmlRoot("Collection")]
	public class CollectionRequest : ResultList<CollectionItemRequest>
    {
        public string Title { get; set; }
        public DateTime? Expires { get; set; }
        public string ImageUri { get; set; }
        public string Description { get; set; }
        public string LegacyId { get; set; }
    }
}
