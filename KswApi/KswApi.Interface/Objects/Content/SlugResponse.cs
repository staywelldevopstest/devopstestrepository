﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	[XmlRoot("Slug")]
	public class SlugResponse
	{
		public string Value { get; set; }
	}
}
