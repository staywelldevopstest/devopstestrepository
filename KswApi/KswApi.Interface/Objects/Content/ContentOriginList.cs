﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	[XmlRoot("Origins")]
	public class ContentOriginList : ResultList<ContentOriginResponse>
	{
	}
}
