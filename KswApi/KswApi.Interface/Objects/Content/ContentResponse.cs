﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects.Content
{
	[XmlType("Content")]
	public class ContentResponse
	{
		public Guid Id { get; set; }
		
		public DateTime DateAdded { get; set; }
		public DateTime DateModified { get; set; }
		public DateTime DateUpdated { get; set; }

		public string Title { get; set; }
		public string Slug { get; set; }
		public string Blurb { get; set; }
		public ContentType Type { get; set; }

		[XmlArrayItem("Tag")]
		public List<string> Tags { get; set; }

		// image-specific
		public string Uri { get; set; }
		public int? Width { get; set; }
		public int? Height { get; set; }
		public ImageFormatType? Format { get; set; }

		// article-specific
		[XmlArrayItem("Segment")]
		public List<ContentSegmentResponse> Segments { get; set; }
		public string Copyright { get; set; }

		public ContentBucketReference Bucket { get; set; }

		public string OriginName { get; set; }

		public string InvertedTitle { get; set; }

		[XmlArrayItem("AlternateTitle")]
		public List<string> AlternateTitles { get; set; }

		public Language Language { get; set; }

		public bool? Master { get; set; }

		public Guid? MasterId { get; set; }

		public string MasterSlug { get; set; }

		public List<ContentVersionItem> Versions { get; set; }

		[XmlArrayItem("AgeCategory")]
		public List<AgeCategory> AgeCategories { get; set; }

		public Gender? Gender { get; set; }

		public string GunningFogReadingLevel { get; set; }

		public string FleschKincaidReadingLevel { get; set; }

		[XmlArrayItem("OnlineOriginatingSource")]
		public List<OnlineOriginatingSource> OnlineOriginatingSources { get; set; }

		[XmlArrayItem("PrintOriginatingSource")]
		public List<PrintOriginatingSource> PrintOriginatingSources { get; set; }

		[XmlArrayItem("RecommendedSite")]
		public List<RecommendedSite> RecommendedSites { get; set; }

		[XmlArrayItem("OnlineEditor")]
		public List<string> OnlineEditors { get; set; }

		[XmlArrayItem("OnlineMedicalReviewer")]
		public List<string> OnlineMedicalReviewers { get; set; }

		public DateTime? LastReviewedDate { get; set; }

		public DateTime? PostingDate { get; set; }

		public DateTime? PublishedDate { get; set; }

		public string LegacyId { get; set; }

		[XmlArrayItem("Author")]
		public List<string> Authors { get; set; }

		public Guid? CopyrightId { get; set; }

		public bool Published { get; set; }
		
		[XmlArrayItem("Taxonomy")]
		public List<ContentTaxonomyList> Taxonomies { get; set; }

		public List<HierarchicalTaxonomyResponse> Servicelines { get; set; }

		public List<CustomAttribute> CustomAttributes { get; set; }
	}
}
