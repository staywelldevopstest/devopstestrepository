﻿using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects.Content
{
	public class Language
	{
		public string Code { get; set; }
		public string Name { get; set; }
		public LanguageDirection Direction { get; set; }
	}
}
