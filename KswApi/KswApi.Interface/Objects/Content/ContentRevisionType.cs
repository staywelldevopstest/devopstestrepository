﻿namespace KswApi.Interface.Objects.Content
{
	public enum ContentRevisionType
	{
		None,
		Draft,
		Published,
		Autosave,
		Created
	}
}
