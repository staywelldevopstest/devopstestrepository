﻿using KswApi.Interface.Objects.Content;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("Buckets")]
	public class UnpagedContentBucketList : ResultList<ContentBucketResponse>
	{

	}
}
