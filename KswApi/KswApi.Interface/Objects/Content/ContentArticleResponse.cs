﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	[XmlType("Content")]
	public class ContentArticleResponse : ContentMetadataResponse
	{
		[XmlArrayItem("Segment")]
		public List<ContentSegmentResponse> Segments { get; set; }
		public string Copyright { get; set; }
	}
}
