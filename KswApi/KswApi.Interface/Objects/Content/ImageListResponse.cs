﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	[XmlType("Images")]
	public class ImageListResponse : PagedResultList<ImageDetailResponse>
	{
		public TypeCount TypeCounts { get; set; }
	}
}
