﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
    [Serializable]
    [XmlRoot(ElementName = "Servicelines")]
    public class ServicelineList : ResultList<Serviceline>
    {

    }

}

