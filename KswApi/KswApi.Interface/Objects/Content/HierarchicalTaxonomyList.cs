﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
    [Serializable]
    [XmlRoot(ElementName = "HierarchicalTaxonomies")]
    public class HierarchicalTaxonomyList : ResultList<HierarchicalTaxonomyResponse>
    {

    }

}
