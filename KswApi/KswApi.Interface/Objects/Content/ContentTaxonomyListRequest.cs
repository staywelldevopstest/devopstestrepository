﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
    [XmlType("Taxonomy")]
	public class ContentTaxonomyListRequest : ResultList<ContentTaxonomyValue>
    {
        public string Slug { get; set; }
    }
}
