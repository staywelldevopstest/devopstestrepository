﻿using System;

namespace KswApi.Interface.Objects.Content
{
	public class ContentBucketSegmentRequest
	{
		public Guid? Id { get; set; }
		public string Name { get; set; }
		public string Slug { get; set; }
		public bool Required { get; set; }
	}
}
