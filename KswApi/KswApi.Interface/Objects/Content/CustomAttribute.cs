﻿using System.Collections.Generic;

namespace KswApi.Interface.Objects.Content
{
	public class CustomAttribute
	{
		public string Name { get; set; }
		public List<CustomAttributeContent> CustomAttributeValues { get; set; }
	}
}