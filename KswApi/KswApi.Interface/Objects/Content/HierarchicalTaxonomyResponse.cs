﻿using System.Collections.Generic;
using System.Xml.Serialization;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects.Content
{
    [XmlType("HierarchicalTaxonomy")]
    public class HierarchicalTaxonomyResponse
    {
        public string Slug { get; set; }
        public string Path { get; set; }
        public string Value { get; set; }
        public List<string> PathValues { get; set; }
        public HierarchicalTaxonomyType Type { get; set; }
    }
}
