﻿using System;
using System.Collections.Generic;

namespace KswApi.Interface.Objects.Content
{
	public class CollectionItemResponse : ResultList<CollectionItemResponse>
	{
		public CollectionItemType Type { get; set; }
		
        public Guid Id { get; set; }
        public string Slug { get; set; }
        public ContentBucketReference Bucket { get; set; }

		public string Title { get; set; }
		public string ImageUri { get; set; }
		public string Description { get; set; }
		public bool? Flagged { get; set; }

        public string FlagComment { get; set; }
        public bool? Disabled { get; set; }
        public List<HierarchicalTaxonomyResponse> ServiceLines { get; set; }
	}
}
