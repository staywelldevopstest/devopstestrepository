﻿namespace KswApi.Interface.Objects.Content
{
	public class Dimensions
	{
		public int Width { get; set; }
		public int Height { get; set; }
	}
}
