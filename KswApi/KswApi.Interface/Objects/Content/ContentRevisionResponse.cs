﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	[XmlType("Content")]
	public class ContentRevisionResponse : ContentMetadataResponse
	{
		public Guid RevisionId { get; set; }
		public ContentRevisionType RevisionType { get; set; }
	}
}
