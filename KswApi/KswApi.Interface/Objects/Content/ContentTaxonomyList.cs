﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
    [XmlType("Taxonomy")]
    public class ContentTaxonomyList : ResultList<ContentTaxonomyValue>
    {
        public string Slug { get; set; }
        public string Name { get; set; }
    }
}