﻿using System;

namespace KswApi.Interface.Objects.Content
{
    public class TopicRequest
    {
        public string Title { get; set; }
		public string Description { get; set; }

        public Guid ImageUrl { get; set; }
        public Guid ThumbnailUrl { get; set; }
    }
}
