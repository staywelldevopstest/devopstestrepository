﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	[XmlType("Content")]
	public class NewContentRequest : ContentArticleRequest
	{
		public string MasterId { get; set; }

		public string LanguageCode { get; set; }

		public string Slug { get; set; }
	}
}
