﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	[XmlType("Content")]
	public class ContentList : SortedResultList<ContentResponse>
	{
		public TypeCount TypeCounts { get; set; }
	}
}
