﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
    [XmlType("Serviceline")]
    public class Serviceline
    {
        public string Value { get; set; }
        public string Slug { get; set; }
        public string Path { get; set; }
    }
}
