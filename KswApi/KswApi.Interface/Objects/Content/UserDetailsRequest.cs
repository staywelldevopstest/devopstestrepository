﻿using System;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects.Content
{
    public class UserDetailsRequest
    {
        public Guid Id { get; set; }
        public UserType Type { get; set; }
    }
}
