﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	[XmlRoot("Taxonomies")]
	public class TaxonomyListResponse : ResultList<ContentTaxonomyList>
	{
	}
}
