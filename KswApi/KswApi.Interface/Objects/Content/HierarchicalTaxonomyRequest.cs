﻿using System.Xml.Serialization;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects.Content
{
    [XmlType("HierarchicalTaxonomyItem")]
    public class HierarchicalTaxonomyRequest
    {
        public string Slug { get; set; }
        public string Path { get; set; }
        public HierarchicalTaxonomyType Type { get; set; }
    }
}
