﻿using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects.Content
{
    public class ContentTaxonomyValue
    {
        public string Value { get; set; }
        public TaxonomyValueSource Source { get; set; }
    }
}
