﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	[XmlRoot("Buckets")]
	public class ContentBucketList : PagedResultList<ContentBucketResponse>
	{

	}
}
