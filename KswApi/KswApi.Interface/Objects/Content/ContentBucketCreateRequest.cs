﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
    [XmlRoot("Bucket")]
    public class ContentBucketCreateRequest : ContentBucketUpdateRequest
    {
        public string Slug { get; set; }
    }
}
