﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	[XmlType("Languages")]
	public class LanguageList : ResultList<Language>
	{
	}
}
