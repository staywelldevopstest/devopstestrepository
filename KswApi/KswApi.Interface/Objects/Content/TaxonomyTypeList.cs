﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
    [Serializable]
    [XmlRoot(ElementName = "TaxonomyTypes")]
    public class TaxonomyTypeList : ResultList<TaxonomyTypeResponse>
    {

    }
}
