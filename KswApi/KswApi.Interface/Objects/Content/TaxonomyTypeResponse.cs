﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
    [XmlType("TaxonomyType")]
    public class TaxonomyTypeResponse
    {
        public string Name { get; set; }
        public string Slug { get; set; }
    }
}
