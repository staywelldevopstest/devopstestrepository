﻿using System;

namespace KswApi.Interface.Objects.Content
{
	public class TopicResponse : ResultList<CollectionItemResponse>
	{
		public Guid Id { get; set; }
		public string Slug { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public DateTime DateAdded { get; set; }
		public DateTime DateModified { get; set; }
		//public ObjectStatus Status { get; set; }

		public Guid ImageId { get; set; }
		public Guid ThumbnailId { get; set; }
	}
}
