﻿using System;

namespace KswApi.Interface.Objects.Content
{
	public class ContentVersionItem
	{
		public Guid Id { get; set; }
		public string Slug { get; set; }
		public Language Language { get; set; }
		public bool Master { get; set; }
	}
}
