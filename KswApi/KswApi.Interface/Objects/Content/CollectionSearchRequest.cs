﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace KswApi.Interface.Objects.Content
{
    public class CollectionSearchRequest
    {
        // base search request:
        [DataMember(Name = "$skip")]
        public int? Offset { get; set; }

        [DataMember(Name = "$top")]
        public int? Count { get; set; }

        public string Query { get; set; }
        public string TitleStartsWith { get; set; }

        public List<SearchType> Types { get; set; }

        // collection specific
    }
}
