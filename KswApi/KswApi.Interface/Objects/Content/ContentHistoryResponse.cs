﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	[XmlType("History")]
	public class ContentHistoryResponse : ResultList<ContentHistoryItemResponse>
	{
	}
}
