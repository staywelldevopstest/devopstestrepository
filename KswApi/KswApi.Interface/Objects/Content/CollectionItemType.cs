﻿namespace KswApi.Interface.Objects.Content
{
	public enum CollectionItemType
	{
		None,
		Collection,
		Topic,
		Content,
		Sort
	}
}
