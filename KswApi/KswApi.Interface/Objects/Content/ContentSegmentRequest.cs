﻿using System;

namespace KswApi.Interface.Objects.Content
{
	public class ContentSegmentRequest
	{
		public string IdOrSlug { get; set; }
		public string CustomName { get; set; }
		public string Body { get; set; }
	}
}
