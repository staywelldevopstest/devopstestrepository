﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
    [XmlRoot("Collection")]
    public class CollectionCreateRequest : CollectionRequest
    {
        public string Slug { get; set; }
    }
}
