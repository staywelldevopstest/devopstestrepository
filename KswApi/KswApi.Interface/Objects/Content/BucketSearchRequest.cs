﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects.Content
{
	public class BucketSearchRequest
	{
		[DataMember(Name = "$skip")]
		public int Offset { get; set; }

		[DataMember(Name = "$top")]
		public int Count { get; set; }

		public List<ContentType> Type { get; set; }
		public string Origin { get; set; }
		public int? LegacyId { get; set; }
		public bool? ReadOnly { get; set; }
		public string Query { get; set; }
		public string CopyrightId { get; set; }
	}
}
