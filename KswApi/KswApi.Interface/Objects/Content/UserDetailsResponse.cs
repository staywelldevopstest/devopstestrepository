﻿using System;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects.Content
{
    public class UserDetailsResponse
    {
        public Guid Id { get; set; }
        public string FullName { get; set; }
        public UserType Type { get; set; }
    }
}
