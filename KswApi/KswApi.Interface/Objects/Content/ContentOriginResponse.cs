﻿using System;

namespace KswApi.Interface.Objects.Content
{
	public class ContentOriginResponse
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
	}
}
