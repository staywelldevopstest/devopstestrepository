﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	[XmlType("Content")]
	public class ContentArticleRequest : ContentBase
	{
		[XmlArrayItem("Segment")]
		public List<ContentSegmentRequest> Segments { get; set; }

		public string Title { get; set; }
		public string InvertedTitle { get; set; }

		[XmlArrayItem("AlternateTitle")]
		public List<string> AlternateTitles { get; set; }

		public string Blurb { get; set; }

		public string LegacyId { get; set; }

		public List<ContentTaxonomyListRequest> Taxonomies { get; set; }

		public List<HierarchicalTaxonomyRequest> Servicelines { get; set; } // Items of type Audience

		public List<CustomAttribute> CustomAttributes { get; set; }

		public bool Publish { get; set; }
	}
}
