﻿using System;

namespace KswApi.Interface.Objects.Content
{
	public class ContentSegmentResponse
	{
		public Guid? Id { get; set; }
		public string Name { get; set; }
		public string CustomName { get; set; }
		public string Slug { get; set; }
		public string Body { get; set; }
	}
}
