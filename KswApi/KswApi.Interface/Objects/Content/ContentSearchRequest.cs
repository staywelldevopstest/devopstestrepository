﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace KswApi.Interface.Objects.Content
{
	public class ContentSearchRequest
	{
		[DataMember(Name = "$skip")]
		public int Offset { get; set; }

		[DataMember(Name = "$top")]
		public int Count { get; set; }

		public string Query { get; set; }
		public string TitleStartsWith { get; set; }
		public bool? IncludeAlternateTitles { get; set; }
		public List<string> Buckets { get; set; }
		public List<string> Languages { get; set; }
		public List<SearchType> Types { get; set; }
		public string Sort { get; set; }
		public List<string> LegacyIds { get; set; }
		public List<ImageFormatType> Formats { get; set; }
		public bool IncludeDrafts { get; set; }
	}
}
