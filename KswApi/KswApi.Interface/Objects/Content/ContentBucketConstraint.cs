﻿namespace KswApi.Interface.Objects.Content
{
	public class ContentBucketConstraint
	{
		public int? Length { get; set; }
		public Dimensions Dimensions { get; set; }
	}
}
