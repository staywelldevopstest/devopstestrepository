﻿
namespace KswApi.Interface.Objects.Content
{
	public class CollectionItemRequest : ResultList<CollectionItemRequest>
	{
		public CollectionItemType Type { get; set; }
		public ItemReference ItemReference { get; set; }
        public string Title { get; set; }
		public string Description { get; set; }
		public bool Flagged { get; set; }
        public string FlagComment { get; set; }
        public bool? Disabled { get; set; }
		public string ImageUri { get; set; }
	}
}
