﻿using System;

namespace KswApi.Interface.Objects.Content
{
	public class OnlineOriginatingSource
	{
		public string Source { get; set; }
		public string Uri { get; set; }
		public string Title { get; set; }
		public DateTime Date { get; set; }
	}
}
