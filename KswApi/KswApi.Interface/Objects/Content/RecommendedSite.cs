﻿namespace KswApi.Interface.Objects.Content
{
	public class RecommendedSite
	{
		public string Uri { get; set; }
		public string Title { get; set; }
	}
}
