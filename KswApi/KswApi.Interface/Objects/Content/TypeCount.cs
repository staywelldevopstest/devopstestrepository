﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	public class TypeCount
	{
		[XmlArrayItem("Type")]
		public Dictionary<ImageFormatType, int> Types { get; set; }

		public int Other { get; set; }

		public int Missing { get; set; }

		public int Total { get; set; }
	}
}
