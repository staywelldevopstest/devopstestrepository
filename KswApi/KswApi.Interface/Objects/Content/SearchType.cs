﻿namespace KswApi.Interface.Objects.Content
{
	public enum SearchType
	{
		None,
		Content,
		Image,
        Collection,
        Bucket
	}
}
