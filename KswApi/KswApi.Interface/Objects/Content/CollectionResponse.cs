﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	[XmlRoot("Collection")]
	public class CollectionResponse : ResultList<CollectionItemResponse>
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
        public DateTime? DateAdded { get; set; }
        public DateTime? DateModified { get; set; }

        public UserDetailsResponse CreatedBy { get; set; }
        public DateTime? Expires { get; set; }
        public string ImageUri { get; set; }
        public string Description { get; set; }
        
        public string LegacyId { get; set; }
		public int LicenseCount { get; set; }
    }
}
