﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
    [XmlRoot("Bucket")]
    public class ContentBucketUpdateRequest : ContentBucketBase
    {
		[XmlArrayItem("Segment")]
		public List<ContentBucketSegmentRequest> Segments { get; set; }
	}
}


