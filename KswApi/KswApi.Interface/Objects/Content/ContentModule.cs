﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	public class ContentModule
	{
		[XmlArray("Buckets")]
		public List<Guid> BucketIds { get; set; }

		[XmlElement("LicenseId")]
		public Guid LicenseId { get; set; }

		[XmlElement("Id")]
		public Guid Id { get; set; }
	}
}
