﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects.Content
{
	[XmlRoot("Collections")]
	public class UnpagedCollectionListResponse : ResultList<CollectionResponse>
	{
	}
}
