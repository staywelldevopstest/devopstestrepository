﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("Modules")]
	public class LicenseModuleList : ResultList<ModuleName>
	{
		[XmlArrayItem("Item")]
		public List<ModuleName> Available { get; set; }  
	}
}
