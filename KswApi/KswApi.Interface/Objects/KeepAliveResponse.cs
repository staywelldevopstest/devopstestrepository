﻿namespace KswApi.Interface.Objects
{
	public class KeepAliveResponse
	{
		public int TimeoutInMilliseconds { get; set; }
	}
}
