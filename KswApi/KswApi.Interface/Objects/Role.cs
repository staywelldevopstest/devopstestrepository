﻿using System;

namespace KswApi.Interface.Objects
{
	[Serializable]
	public class Role : RoleRequest
	{
		public Guid Id { get; set; }
	}
}
