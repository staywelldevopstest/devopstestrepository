﻿using System;

namespace KswApi.Interface.Objects
{
    public class ClientUserResponse : ClientUserRequest
    {
        public Guid Id { get; set; }
        public DateTime Created { get; set; }
        public string ClientName { get; set; }
		public bool Locked { get; set; }
		public DateTime? LastLogin { get; set; }
    }
}
