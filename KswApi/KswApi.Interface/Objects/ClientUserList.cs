﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
    [Serializable]
    [XmlRoot(ElementName = "ClientUsers")]
	public class ClientUserList : SortedResultList<ClientUserResponse>
    {
    }
}
