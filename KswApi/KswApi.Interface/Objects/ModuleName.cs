﻿using System.Xml.Serialization;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects
{
	[XmlRoot("Module")]
	public class ModuleName
	{
		public string Name { get; set; }
		public ModuleType Type { get; set; }
	}
}
