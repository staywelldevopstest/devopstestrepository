﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlType("Settings")]
	public class UserSettingsRequest
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string EmailAddress { get; set; }
		public string PhoneNumber { get; set; }
		public string Notes { get; set; }
	}
}
