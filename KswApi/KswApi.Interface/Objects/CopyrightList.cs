﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[Serializable]
	[XmlRoot(ElementName = "Copyrights")]
	public class CopyrightList : SortedResultList<CopyrightResponse>
	{
		public CopyrightResponse Default { get; set; }
	}
}
