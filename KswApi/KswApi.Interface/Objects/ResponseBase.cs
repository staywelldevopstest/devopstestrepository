﻿namespace KswApi.Interface.Objects
{
	public class ResponseBase
	{
		public bool IsSuccessful { get; set; }
		public string ErrorDetails { get; set; }
	}
}
