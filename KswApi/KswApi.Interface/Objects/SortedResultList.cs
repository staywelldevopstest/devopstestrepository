﻿
namespace KswApi.Interface.Objects
{
	public class SortedResultList<T> : PagedResultList<T>
	{
		public string SortedBy { get; set; }
	}
}
