﻿namespace KswApi.Interface.Objects.Monitoring
{
	public class ServerModuleList : ResultList<string>
	{
		public int Total { get; set; }
	}
}
