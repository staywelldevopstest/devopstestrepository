﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("Ping")]
	public class PingData
	{
		public string CurrentDateAndTime { get; set; }
	}
}
