﻿using KswApi.Interface.Enums;
using System;

namespace KswApi.Interface.Objects
{
    public class ClientUserLicense
    {
        public Guid Id { get; set; }
        public LicensePermissionType PermissionType { get; set; }
    }
}
