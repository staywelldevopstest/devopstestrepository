﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace KswApi.Interface.Objects
{
	public class ImageDownloadResponse : StreamResponse
	{
		public ImageDownloadResponse(Stream stream) : base(stream)
		{
		}
	}
}
