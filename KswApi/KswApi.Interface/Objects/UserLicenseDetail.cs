﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KswApi.Interface.Objects
{
	public class UserLicenseDetail
	{
		public Guid UserId { get; set; }
		public Guid ClientId { get; set; }
		public LicenseDetail LicenseDetail { get; set; }
	}
}
