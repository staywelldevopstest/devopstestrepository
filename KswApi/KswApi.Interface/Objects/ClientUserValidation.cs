﻿
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("AvailableEmail")]
    public class ClientUserValidation
    {
        public bool IsAvailable { get; set; }
    }
}
