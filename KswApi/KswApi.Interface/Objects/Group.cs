﻿using System;

namespace KswApi.Interface.Objects
{
	[Serializable]
	public class Group
	{
		public string GroupName { get; set; }
		public string DistinguishedName { get; set; }
	}
}
