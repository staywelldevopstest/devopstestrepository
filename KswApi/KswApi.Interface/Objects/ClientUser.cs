﻿namespace KswApi.Interface.Objects
{
    public class ClientUser : ClientUserResponse
    {
		public string PasswordHash { get; set; }
    }
}
