﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KswApi.Interface.Objects
{
	public class ImageUploadResponse
	{
		public Guid ImageId { get; set; }
	}
}
