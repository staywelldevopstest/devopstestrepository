﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("Service")]
	public class ServiceInformation
	{
		public string Copyright { get; set; }
	}
}
