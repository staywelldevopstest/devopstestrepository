﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("Modules")]
	public class ModuleNameList : ResultList<ModuleName>
	{

	}
}
