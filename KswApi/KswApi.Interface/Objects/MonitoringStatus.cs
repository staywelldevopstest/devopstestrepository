﻿
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("ServerStatus")]
    public class MonitoringStatus
    {
        public bool DatabaseStatus { get; set; }
        public bool CacheStatus { get; set; }
        public bool ServiceStatus { get; set; }
        public bool PortalStatus { get; set; }
		public bool QueueStatus { get; set; }
    }
}
