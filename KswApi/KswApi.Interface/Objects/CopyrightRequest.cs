﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("Copyright")]
	public class CopyrightRequest
	{
		public string Value { get; set; }
	}
}
