﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("TaskStep")]
	public class TaskStepResponse
	{
		public bool Continue { get; set; }
	}
}
