﻿using System;

namespace KswApi.Interface.Objects
{
	public class Log
	{
		public Guid Id { get; set; }
        public string Title { get; set; }
		public LogItemType Type { get; set; }
		public LogItemSubType SubType { get; set; }
		public string Verb { get; set; }
		public string Path { get; set; }
		public string Body { get; set; }
		public Guid Client { get; set; }
		public string ClientAddress { get; set; }
		public string License { get; set; }
		public TimeSpan Duration { get; set; }
		public DateTime Date { get; set; }
		public string Message { get; set; }
		public string StackTrace { get; set; }
		public int ResponseCode { get; set; }
	}
}
