﻿using System;
using System.Xml.Serialization;
using KswApi.Interface.Objects.Content;

namespace KswApi.Interface.Objects
{
	[XmlType("Image")]
	public class ImageDetailResponse : ImageDetailRequest
	{
		public Guid Id { get; set; }
		
		public ContentBucketReference Bucket { get; set; }

		public string Uri { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
		public ImageFormatType Format { get; set; }
		public DateTime DateAdded { get; set; }
		public DateTime DateModified { get; set; }
	}
}
