﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
    [Serializable]
    [XmlRoot(ElementName = "Clients")]
	public class ClientList : SortedResultList<ClientResponse>
    {

    }
}
