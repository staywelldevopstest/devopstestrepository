﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("Applications")]
	public class ApplicationList : SortedResultList<Application>
	{
	}
}
