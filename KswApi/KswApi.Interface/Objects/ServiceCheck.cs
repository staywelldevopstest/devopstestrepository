﻿
namespace KswApi.Interface.Objects
{
    public class ServiceCheck
    {
        public bool IsServiceUp { get; set; }
    }
}
