﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[Serializable]
	[XmlRoot(ElementName = "Roles")]
	public class RoleList : ResultList<Role>
	{

	}
}
