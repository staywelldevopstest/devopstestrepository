﻿namespace KswApi.Interface.Objects
{
	public class PagedResultList<T> : ResultList<T>
	{
		public int Total { get; set; }
		public int Offset { get; set; }
	}
}
