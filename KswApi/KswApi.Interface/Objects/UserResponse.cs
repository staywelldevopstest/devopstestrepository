﻿using System.Collections.Generic;
using System.Xml.Serialization;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects
{
	[XmlType("User")]
	public class UserResponse
	{
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string FullName { get; set; }
		public string EmailAddress { get; set; }
		public UserType Type { get; set; }
	}
}
