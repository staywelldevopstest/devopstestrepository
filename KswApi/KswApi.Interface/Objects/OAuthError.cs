﻿// ReSharper disable InconsistentNaming

using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("error")]
	public class OAuthError
	{
		public string error { get; set; }
		public string error_description { get; set; }
	}
}
