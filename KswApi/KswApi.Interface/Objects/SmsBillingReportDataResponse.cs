﻿
using System.Collections.Generic;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlType("SmsBilling")]
    public class SmsBillingReportDataResponse
    {
		[XmlArrayItem("Client")]
        public List<SmsBillingReportClientDataResponse> Clients { get; set; }
    }

    public class SmsBillingReportClientDataResponse
    {
        public string Name { get; set; }

		[XmlArrayItem("License")]
        public List<SmsBillingReportLicenseDataResponse> Licenses { get; set; }
    }

    public class SmsBillingReportLicenseDataResponse
    {
        public string Name { get; set; }
        public int ShortCodeKeywordCount { get; set; }
        public int LongCodeCount { get; set; }
        public int IncomingMessageCount { get; set; }
        public int OutgoingMessageCount { get; set; }
    }
}
