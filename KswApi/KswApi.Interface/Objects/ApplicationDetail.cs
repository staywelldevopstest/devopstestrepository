﻿using System;

namespace KswApi.Interface.Objects
{
	public class ApplicationDetail
	{
		public string Name { get; set; }
		public Guid Id { get; set; }
		public string Secret { get; set; }
	}
}
