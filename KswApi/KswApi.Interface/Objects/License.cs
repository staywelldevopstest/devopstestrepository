﻿using KswApi.Interface.Enums;
using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
    public class License : LicenseRequest
    {
        public Guid Id { get; set; }
        public DateTime CreatedDate { get; set; }

		[XmlArrayItem("Application")]
        public List<Guid> Applications { get; set; }

		[XmlArrayItem("Module")]
        public List<ModuleType> Modules { get; set; }

		[XmlArrayItem("Child")]
        public List<Guid> Children { get; set; }
        public LicenseStatus Status { get; set; }
    }
}
