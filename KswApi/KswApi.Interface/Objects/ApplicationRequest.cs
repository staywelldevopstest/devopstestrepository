﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects
{
	[Serializable]
	[XmlRoot("Application")]
	public class ApplicationRequest
	{
		public string Name { get; set; }
		public string RedirectUri { get; set; }
		public string Site { get; set; }
		public string Description { get; set; }
		public ApplicationType Type { get; set; }
		public bool EnableJsonp { get; set; }

		[XmlArrayItem("JsonpWebsite")]
		public List<string> JsonpWebsites { get; set; }

		[XmlArrayItem("Address")]
		public List<string> Addresses { get; set; }
	}
}
