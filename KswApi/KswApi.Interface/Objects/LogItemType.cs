﻿namespace KswApi.Interface.Objects
{
	public enum LogItemType
	{
		Operation = 1,
		Notification = 2
	}
}
