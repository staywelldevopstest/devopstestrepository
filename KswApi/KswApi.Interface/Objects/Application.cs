﻿using System;

namespace KswApi.Interface.Objects
{
	[Serializable]
	public class Application : ApplicationRequest
	{
		public Guid Id { get; set; }
		public Guid? LicenseId { get; set; }
		public DateTime DateAdded { get; set; }
		public string Secret { get; set; }
	}
}
