﻿using System;

namespace KswApi.Interface.Objects.Rendering
{
	public class TemplateField
	{
		public string Id { get; set; }
		public string Name { get; set; }
		public string Realm { get; set; }
		public string Options { get; set; }
		public object Value { get; set; }
		public Type Type { get { return Value.GetType(); } }
		public object Default { get; set; }
		public bool Overridable { get; set; }

		public TemplateField(string name, object value)
		{
			if (name == null) throw new ArgumentNullException("name");
			if (value == null) throw new ArgumentNullException("value");
			Name = name;
			Value = value;
		}
	}
}