﻿using System.Collections.Generic;
using System.Xml.Serialization;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.Content;

namespace KswApi.Interface.Objects.Rendering
{
	[XmlRoot("Render")]
	public class RenderRequest
	{
		public List<ItemReference> Template { get; set; }
		public List<ItemReference> Content { get; set; }

		public RenderMode? RenderMode { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string ProviderName { get; set; }
		public string RecipientIdentifier { get; set; }
		public int? Age { get; set; }
		public Gender? Gender { get; set; }
		public Ethnicity? Ethnicity { get; set; }
		public Language Language { get; set; }
		public EducationLevel? EducationLevel { get; set; }
		public bool? PresenceOfChildren { get; set; }

		// All modes/channels
		public string From { get; set; }
		public string To { get; set; }

		// SMS only
		public string Keyword { get; set; }

		// Email only
		public string Subject { get; set; }
	}
}
