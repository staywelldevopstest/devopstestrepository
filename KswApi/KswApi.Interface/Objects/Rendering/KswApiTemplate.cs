﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace KswApi.Interface.Objects.Rendering
{
	[DataContract]
	public class KswApiTemplate
	{
		// TODO allow multiple templates..or not?
		public ItemReference TemplateReference { get; set; }

		[DataMember]
		public KswApiTemplateType KswApiTemplateType { get; set; }

		[DataMember]
		public IList<TemplateField> Fields { get; set; }
	}
}