﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("Password")]
	public class PasswordRequest
	{
		public string Token { get; set; }
		public string Password { get; set; }
		public string PasswordConfirmation { get; set; }
	}
}
