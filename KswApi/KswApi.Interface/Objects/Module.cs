﻿using System;
using KswApi.Interface.Enums;

namespace KswApi.Interface.Objects
{
	/// <summary>
	/// Base class for all modules
	/// </summary>
	public class Module
	{
		public Guid Id;
		public ModuleStatus Status { get; set; }
		public Guid LicenseId { get; set; }
		public Guid ServiceId { get; set; }
	}
}
