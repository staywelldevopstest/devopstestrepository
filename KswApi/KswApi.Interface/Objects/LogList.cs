﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[Serializable]
	[XmlRoot(ElementName = "Logs")]
	public class LogList : SortedResultList<Log>
	{
	}
}
