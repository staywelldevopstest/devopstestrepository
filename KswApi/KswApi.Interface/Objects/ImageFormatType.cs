﻿namespace KswApi.Interface.Objects
{
	public enum ImageFormatType
	{
		None,
		Jpg,
		Png,
		Tiff,
		Gif,
		Bmp
	}
}
