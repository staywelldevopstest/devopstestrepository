﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("Copyright")]
	public class CopyrightResponse : CopyrightRequest
	{
		public Guid? Id { get; set; }
		public int? BucketCount { get; set; }
		public DateTime DateAdded { get; set; }
		public DateTime DateModified { get; set; }
	}
}
