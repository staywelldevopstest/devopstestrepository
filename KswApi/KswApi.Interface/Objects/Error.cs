﻿using System.Net;
using System.Web;

namespace KswApi.Interface.Objects
{
	public class Error
	{
		public Error()
		{
		}

		public Error(HttpStatusCode code, string message)
		{
			StatusCode = (int)code;
			StatusDescription = HttpWorkerRequest.GetStatusDescription(StatusCode);
			Details = message;
		}

		public int StatusCode { get; set; }
		public string StatusDescription { get; set; }
		public string Details { get; set; }
		public string RedirectUri { get; set; }
	}
}