﻿using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("AvailableParentLicenses")]
	public class AvailableParentLicenseList : ResultList<License>
	{
	}
}
