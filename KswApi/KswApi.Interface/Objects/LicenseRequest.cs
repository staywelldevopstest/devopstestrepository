﻿using System;
using System.Xml.Serialization;

namespace KswApi.Interface.Objects
{
	[XmlRoot("License")]
	public class LicenseRequest
	{
		public string Name { get; set; }
		public Guid ClientId { get; set; }
		public Guid? ParentLicenseId { get; set; }
		public string Notes { get; set; }
	}
}
