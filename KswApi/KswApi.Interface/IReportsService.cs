﻿using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace KswApi.Interface
{
    [ServiceContract(Name = "Reports", Namespace = "http://www.kramesstaywell.com")]
    public interface IReportsService
    {
        [WebInvoke(UriTemplate = "SMSBilling", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_Reports")]
        SmsBillingReportDataResponse GetSmsBillingReportData(Month month, int year);

        [WebInvoke(UriTemplate = "SMSBillingCsvExport", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_Reports")]
        StreamResponse GetSmsBillingReportCsvExport(Month month, int year);
    }
}
