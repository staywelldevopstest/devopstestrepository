﻿namespace KswApi.Interface.Enums
{
	public enum LicenseAncestry
	{
		None,
		Child,
		Parent
	}
}
