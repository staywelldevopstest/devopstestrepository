﻿
namespace KswApi.Interface.Enums
{
    public enum TaxonomyValueSource
    {
        None = 0,
        Ksw = 1,
        ThirdParty = 2,
        Client = 3,
        ThreeM = 4
    }
}
