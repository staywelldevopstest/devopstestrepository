﻿namespace KswApi.Interface.Enums
{
	public enum LanguageDirection
	{
		None,
		LeftToRight,
		RightToLeft
	}
}
