
namespace KswApi.Interface.Enums
{
    public enum RecurringPatternType
    {
        Daily,
        Hourly
    }
}
