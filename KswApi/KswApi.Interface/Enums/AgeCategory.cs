﻿namespace KswApi.Interface.Enums
{
	public enum AgeCategory
	{
		None,
		Infant,
		Child,
		Teen,
		Adult,
		Senior
	}
}
