﻿namespace KswApi.Interface.Enums
{
	public enum SmsNumberRouteType
	{
		Unknown = 0,
		Keyword = 2,
		CatchAll = 3
	}
}
