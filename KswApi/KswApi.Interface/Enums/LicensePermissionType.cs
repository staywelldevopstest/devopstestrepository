﻿
namespace KswApi.Interface.Enums
{
    public enum LicensePermissionType
    {
        None,
        User,
        Developer
    }
}
