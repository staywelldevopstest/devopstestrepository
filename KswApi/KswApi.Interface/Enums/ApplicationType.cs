﻿namespace KswApi.Interface.Enums
{
    public enum ApplicationType
    {
        Confidential,
        Public,
        Product,
        Internal,
		AdministrationPortal,
		AuxiliaryService
    }
}
