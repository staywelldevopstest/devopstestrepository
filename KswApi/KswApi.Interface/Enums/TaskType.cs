
namespace KswApi.Interface.Enums
{
    public enum TaskType
    {
		None = 0,
		Test = 1,
        SmsSessionCleanup = 2,
        AuthorizationGrantCleanup = 3,
        AccessTokenCleanup = 4,
		ContentAutosaveCleanup = 5,
		ThreeMHddUpdate = 6
    }
}
