﻿
namespace KswApi.Interface.Enums
{
    public enum CollectionRevisionType
    {
        None,
        Created,
        Saved,
        Deleted,
        Autosave,
    }
}
