﻿namespace KswApi.Interface.Enums
{
	public enum EducationLevel
	{
		None,
		SomeHighSchool,
		College,
		SomeCollege,
		GraduateSchool
	}
}
