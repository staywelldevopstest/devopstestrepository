using System;

namespace KswApi.Interface.Enums
{
    public class HourlyRecurrence
    {
        public DateTime StartTime { get; set; }
        public int IntervalInMinutes { get; set; }
    }
}
