﻿namespace KswApi.Interface.Enums
{
	public enum ModuleStatus
	{
		Active,
		Inactive
	}
}
