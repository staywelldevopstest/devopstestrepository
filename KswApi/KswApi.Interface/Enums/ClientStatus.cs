﻿namespace KswApi.Interface.Enums
{
    public enum ClientStatus
    {
        Active,
        Disabled,
        Inactive,
        Deleted
    }
}
