﻿
namespace KswApi.Interface.Enums
{
	public enum SmsNumberType
	{
		ShortCode,
		LongCode
	}
}
