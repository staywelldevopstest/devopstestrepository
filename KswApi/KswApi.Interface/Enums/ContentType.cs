﻿namespace KswApi.Interface.Enums
{
	public enum ContentType
	{
		None = 0,
		Article = 1,
		Text = 2,
		Image = 3,
		Video = 4
	}
}
