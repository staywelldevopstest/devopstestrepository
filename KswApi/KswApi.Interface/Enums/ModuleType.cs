﻿
namespace KswApi.Interface.Enums
{
	public enum ModuleType
	{
		SmsGateway = 0,
		Content = 1,
		ManageContent = 2
	}
}
