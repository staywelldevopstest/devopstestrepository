﻿
namespace KswApi.Interface.Enums
{
    public enum ClientUserPermissionType
    {
        None = 0,
        User = 1,
        Admin = 2
    }
}
