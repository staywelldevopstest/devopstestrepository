using System;
using System.Collections.Generic;

namespace KswApi.Interface.Enums
{
    public class DailyRecurringSettings
    {
        public DateTime StartTime { get; set; }
        public List<DayOfWeek> DaysToRun { get; set; }
    }
}
