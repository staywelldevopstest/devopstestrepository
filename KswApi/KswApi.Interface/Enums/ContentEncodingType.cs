﻿namespace KswApi.Interface.Enums
{
	public enum ContentEncodingType
	{
		Unknown,
		Utf8,
		Jpeg,
		Png
	}
}
