﻿namespace KswApi.Interface.Enums
{
	public enum Gender
	{
		None,
		All,
		Female,
		Male
	}
}
