﻿namespace KswApi.Interface.Enums
{
	public enum OAuthErrorCode
	{
		InvalidClient,
		InvalidRequest,
		InvalidGrant,
		UnsupportedResponseType,
		UnauthorizedClient,
		AccessDenied,
		UnsupportedGrantType,
		InvalidScope,
		ServerError
	}
}
