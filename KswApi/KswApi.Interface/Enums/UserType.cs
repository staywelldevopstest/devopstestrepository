﻿namespace KswApi.Interface.Enums
{
	public enum UserType
	{
		None,
		DomainUser,
		ClientUser
	}
}
