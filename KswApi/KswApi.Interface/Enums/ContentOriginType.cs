﻿namespace KswApi.Interface.Enums
{
	public enum ContentOriginType
	{
		None,
		Ksw,
		Other
	}
}
