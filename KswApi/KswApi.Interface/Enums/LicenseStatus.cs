﻿
namespace KswApi.Interface.Enums
{
    public enum LicenseStatus
    {
        Active = 0,
        Inactive = 1
    }
}
