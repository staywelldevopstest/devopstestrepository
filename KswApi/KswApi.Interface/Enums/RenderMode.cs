﻿namespace KswApi.Interface.Enums
{
	public enum RenderMode
	{
		Sms,
		Email,
		Print,
		Portal,
		Debug
	}
}