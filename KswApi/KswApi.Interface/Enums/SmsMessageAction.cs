﻿namespace KswApi.Interface.Enums
{
    public enum SmsMessageAction
    {
        New,
        Continue,
        Stop,
        Help
    }
}
