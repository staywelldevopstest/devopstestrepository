﻿using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;

namespace KswApi.Interface
{
	[ServiceContract(Name = "OAuth", Namespace = "http://www.kramesstaywell.com")]
	public interface IOAuthService
	{
		[WebInvoke(UriTemplate = "TokenEndpoint", Method = "POST")]
		[OperationContract]
		[Allow(ClientType = ClientType.Public, Logging = AllowedLogging.LogWithoutBody)]
		OAuthTokenResponse CreateAccessToken(OAuthTokenRequest request);
	}
}
