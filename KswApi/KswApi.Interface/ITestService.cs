﻿using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace KswApi.Interface
{
    [ServiceContract(Name = "Test", Namespace = "http://www.kramesstaywell.com")]
    public interface ITestService
    {
        [WebGet]
        [OperationContract]
        [Allow(ClientType = ClientType.Public)]
        PingData Ping();

        [WebGet]
        [OperationContract]
        [Allow(ClientType = ClientType.Public)]
        void Error();

		[WebInvoke(Method = "POST")]
		[Allow(ClientType = ClientType.Public)]
	    StreamResponse Stream(StreamRequest request);
    }
}
