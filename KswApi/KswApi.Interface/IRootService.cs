﻿using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;

namespace KswApi.Interface
{
	[ServiceContract(Name = "/", Namespace = "http://www.kramesstaywell.com")]
	public interface IRootService
	{
		[WebInvoke(UriTemplate = "", Method = "GET")]
		[Allow(ClientType = ClientType.Public)]
		ServiceInformation GetServiceInformation();
	}
}
