﻿using System.Collections.Generic;
using KswApi.Interface.Objects.Content;
using KswApi.Interface.Objects.Rendering;

namespace KswApi.Interface.Rendering
{
	public interface IRenderingModeFulfiller
	{
		RenderResponse Render(RenderRequest request, KswApiTemplate kswApiTemplate,
			IList<ContentArticleResponse> allTemplates,
			IList<ContentArticleResponse> allContent);
	}
}