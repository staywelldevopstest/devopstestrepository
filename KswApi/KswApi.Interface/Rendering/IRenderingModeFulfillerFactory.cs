﻿using KswApi.Interface.Enums;

namespace KswApi.Interface.Rendering
{
	public interface IRenderingModeFulfillerFactory
	{
		IRenderingModeFulfiller GetRenderer(RenderMode renderMode);
	}
}