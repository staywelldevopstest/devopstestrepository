﻿using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;

namespace KswApi.Interface
{
	[ServiceContract(Name = "Security", Namespace = "http://www.kramesstaywell.com")]
	public interface ISecurityService
	{
		[WebInvoke(UriTemplate = "Validations", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Public)]
		ValidationResponse ValidateAccessToken(string token);
	}
}
