﻿using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.Rendering;

namespace KswApi.Interface
{
	[ServiceContract(Name = "Rendering", Namespace = "http://www.kramesstaywell.com")]
	public interface IRenderingService
	{
		[WebInvoke(UriTemplate = "", Method = "POST")]
		//[Allow(ClientType = ClientType.Internal, Rights = "Render_Content", SpecialAccess = AllowedSpecialAccess.Jsonp, Extensions = "txt,pdf")]
		[Allow(ClientType = ClientType.Internal)]
		RenderResponse Render(RenderRequest request);
	}
}
