﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.Content;
using KswApi.Interface.Objects;

namespace KswApi.Interface
{
	[ServiceContract(Name = "Content", Namespace = "http://www.kramesstaywell.com")]
	public interface IContentService
	{
		[WebInvoke(UriTemplate = "", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_Content", SpecialAccess = AllowedSpecialAccess.Jsonp)]
		ContentList SearchContent(ContentSearchRequest request);

	    [WebInvoke(UriTemplate = "{bucketIdOrSlug}/{idOrSlug}", Method = "GET")]
	    [OperationContract]
	    [Allow(ClientType = ClientType.Internal, Rights = "Read_Content", SpecialAccess = AllowedSpecialAccess.Jsonp)]
        ContentArticleResponse GetContent(string bucketIdOrSlug, string idOrSlug, bool includeBody, bool draft, DateTime? time = null);

		[WebInvoke(UriTemplate = "{bucketIdOrSlug}", Method = "POST")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_Content")]
		ContentMetadataResponse CreateContent(string bucketIdOrSlug, NewContentRequest content);

		[WebInvoke(UriTemplate = "{bucketIdOrSlug}/{idOrSlug}", Method = "PUT")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_Content")]
		ContentMetadataResponse UpdateContent(string bucketIdOrSlug, string idOrSlug, ContentArticleRequest content);

		[WebInvoke(UriTemplate = "{bucketIdOrSlug}/{idOrSlug}", Method = "DELETE")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_Content")]
		ContentResponse DeleteContent(string bucketIdOrSlug, string idOrSlug, bool? publishedOnly);

		[WebInvoke(UriTemplate = "{bucketIdOrSlug}/Slugs", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_Content", SpecialAccess = AllowedSpecialAccess.Jsonp)]
		SlugResponse GetSlug(string bucketIdOrSlug, string title, string slug);

		#region History

		[WebInvoke(UriTemplate = "{bucketIdOrSlug}/{contentIdOrSlug}/History", Method = "POST")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_Content")]
		ContentHistoryItemResponse AddHistoryContent(string bucketIdOrSlug, string contentIdOrSlug, NewContentRequest content);

		[WebInvoke(UriTemplate = "{bucketIdOrSlug}/{contentIdOrSlug}/History", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_Content")]
		ContentHistoryResponse GetHistory(string bucketIdOrSlug, string contentIdOrSlug);

		[WebInvoke(UriTemplate = "{bucketIdOrSlug}/{contentIdOrSlug}/History/{id}", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_Content")]
		ContentArticleResponse GetHistoryContent(string bucketIdOrSlug, string contentIdOrSlug, string id, bool? includeBody);

		#endregion

		#region Images

		[WebInvoke(UriTemplate = "{bucketIdOrSlug}/Images", Method = "POST")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_Content")]
		ImageDetailResponse CreateImage(string bucketIdOrSlug, StreamRequest body);

		// publicly accessible image using license id
		[WebInvoke(UriTemplate = "{licenseId}/{bucketIdOrSlug}/Images/{imageIdOrSlug}", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Public, Extensions = "jpg,jpeg,tiff,bmp,png,gif")]
		StreamResponse GetLicenseImage(string licenseId, string bucketIdOrSlug, string imageIdOrSlug);

		[WebInvoke(UriTemplate = "{bucketIdOrSlug}/Images/{imageIdOrSlug}", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_Content", Extensions = "jpg,jpeg,tiff,bmp,png,gif")]
		StreamResponse GetImage(string bucketIdOrSlug, string imageIdOrSlug);

		[WebInvoke(UriTemplate = "{bucketIdOrSlug}/Images/Details/{imageIdOrSlug}", Method = "PUT")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_Content")]
		ImageDetailResponse UpdateImageDetails(string bucketIdOrSlug, string imageIdOrSlug, ImageDetailRequest request);

		[WebInvoke(UriTemplate = "{bucketIdOrSlug}/Images/Details/{imageIdOrSlug}", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_Content")]
		ImageDetailResponse GetImageDetails(string bucketIdOrSlug, string imageIdOrSlug);

		[WebInvoke(UriTemplate = "{bucketIdOrSlug}/Images/Slugs/{idOrSlug}", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_Content", SpecialAccess = AllowedSpecialAccess.Jsonp)]
		SlugResponse GetImageSlug(string bucketIdOrSlug, string idOrSlug, string slug, string title);

		[WebInvoke(UriTemplate = "{bucketIdOrSlug}/Images/{imageIdOrSlug}", Method = "DELETE")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_Content")]
		ImageDetailResponse DeleteImage(string bucketIdOrSlug, string imageIdOrSlug);

		#endregion

        #region Taxonomies

        [WebInvoke(UriTemplate = "{bucketIdOrSlug}/{contentIdOrSlug}/Taxonomies", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_Content")]
		TaxonomyListResponse SearchTaxonomies(string bucketIdOrSlug, string contentIdOrSlug);

        #endregion
    }
}
