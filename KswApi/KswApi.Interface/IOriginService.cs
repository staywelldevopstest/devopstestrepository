﻿using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.Content;

namespace KswApi.Interface
{
	[ServiceContract(Name = "Origins", Namespace = "http://www.kramesstaywell.com")]
	public interface IOriginService
	{
		[WebInvoke(UriTemplate = "", Method = "GET")]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_Content", SpecialAccess = AllowedSpecialAccess.Jsonp)]
		ContentOriginList GetBucketOrigins();
	}
}
