﻿using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.Content;

namespace KswApi.Interface
{
	[ServiceContract(Name = "Languages", Namespace = "http://www.kramesstaywell.com")]
	public interface ILanguageService
	{
		[WebInvoke(UriTemplate = "", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_Content", SpecialAccess = AllowedSpecialAccess.Jsonp)]
		LanguageList GetLanguages(string bucketIdOrSlug, string contentIdOrSlug);
	}
}
