﻿using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.Content;

namespace KswApi.Interface
{
    [ServiceContract(Name = "Content", Namespace = "http://www.kramesstaywell.com")]
    public interface IServicelinesService
    {
        [WebGet(UriTemplate = "Servicelines")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_Content", SpecialAccess = AllowedSpecialAccess.Jsonp)]
        HierarchicalTaxonomyList SearchServicelines(string path);

        [WebGet(UriTemplate = "Servicelines/{slug}")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_Content", SpecialAccess = AllowedSpecialAccess.Jsonp)]
        HierarchicalTaxonomyResponse GetServicelines(string slug, string path);
    }
}
