﻿using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.SmsGateway;

namespace KswApi.Interface
{
	[ServiceContract(Name = "SmsGateway", Namespace = "http://www.kramesstaywell.com")]
	public interface ISmsGatewayService
	{
		[WebInvoke(UriTemplate = "Messages", Method = "POST")]
		[Allow(ClientType = ClientType.Internal, Rights = "SMS_Send")]
		IncomingMessage SendMessage(IncomingMessage message);
	}
}
