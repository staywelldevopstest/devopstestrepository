using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using System;
using System.Collections.Generic;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace KswApi.Interface
{
    [ServiceContract(Name = "Tasks", Namespace = "http://www.kramesstaywell.com")]
    public interface ITasksService
    {

        #region Settings and General Methods

        [WebInvoke(UriTemplate = "Settings", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Public)]
        TaskServiceSettings GetServiceSettings();

        //[WebInvoke(UriTemplate = "Settings/{settings}", Method = "PUT")]
        //[OperationContract]
        //[Allow(ClientType = ClientType.Public)]
        //TaskServiceSettings UpdateServiceSettings(TaskServiceSettings settings);

        [WebInvoke(UriTemplate = "TaskSettings", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Public)]
        Task GetTaskSettings(TaskType taskType);

        [WebInvoke(UriTemplate = "StartedTask", Method = "POST")]
        [OperationContract]
        [Allow(ClientType = ClientType.Public)]
        void StartedTask(Task task);

        [WebInvoke(UriTemplate = "FinishedTask", Method = "POST")]
        [OperationContract]
        [Allow(ClientType = ClientType.Public)]
        void FinishedTask(Task task);

        [WebInvoke(UriTemplate = "TaskExceptionLog", Method = "POST")]
        [OperationContract]
        [Allow(ClientType = ClientType.Public)]
        void LogTaskException(Task task, Exception ex);

        [WebInvoke(UriTemplate = "ServiceCheck", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Public)]
        ServiceCheck ServiceCheck();

        #endregion

        #region Task Specific Methods

		[WebInvoke(UriTemplate = "TaskStep/{type}", Method = "POST")]
		[OperationContract]
		[Allow(ClientType = ClientType.Public)]
		TaskStepResponse DoTaskStep(TaskType type);

        #endregion
    }
}
