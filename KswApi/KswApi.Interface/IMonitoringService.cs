﻿using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Interface.Objects.Monitoring;

namespace KswApi.Interface
{
    [ServiceContract(Name = "Monitoring", Namespace = "http://www.kramesstaywell.com")]
    public interface IMonitoringService
    {
        [WebGet]
        [OperationContract]
        [Allow(ClientType = ClientType.Public, Logging = AllowedLogging.NeverLog)]
        MonitoringStatus ServerStatus();

		[WebGet]
		[OperationContract]
		[Allow(ClientType = ClientType.Public, Logging = AllowedLogging.NeverLog)]
		ServerModuleList Modules();
	}
}
