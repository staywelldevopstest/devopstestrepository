﻿using System;

namespace KswApi.Interface.Exceptions
{
	public class RenderingException : Exception
	{
		public RenderingException(string message) : base(message) { }
	}
}