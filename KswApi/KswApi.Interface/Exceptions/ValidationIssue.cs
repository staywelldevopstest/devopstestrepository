﻿namespace KswApi.Interface.Exceptions
{
	public class ValidationIssue
	{
		public string Field { get; set; }
		public string Issue { get; set; }
	}
}
