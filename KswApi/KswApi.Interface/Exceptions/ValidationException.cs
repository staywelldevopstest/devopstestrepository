﻿using System;
using System.Collections.Generic;
using System.Net;

namespace KswApi.Interface.Exceptions
{
    [Serializable]
    public class ValidationException : ServiceException
    {
        public ValidationException(HttpStatusCode statusCode, string field, string issue)
            : base(statusCode, issue)
        {
            Issues = new List<ValidationIssue>
						 {
							 new ValidationIssue
								 {
									 Field = field,
									 Issue = issue
								 }
						 };
        }

        public List<ValidationIssue> Issues { get; private set; }
    }
}
