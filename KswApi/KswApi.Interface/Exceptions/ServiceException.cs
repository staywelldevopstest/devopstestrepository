﻿using System;
using System.Net;

namespace KswApi.Interface.Exceptions
{
	public class ServiceException : Exception
	{
		/// <summary>
		/// Serivce Exception 
		/// </summary>
		/// <param name="statusCode">HttpStatusCode </param>
		/// <param name="title">Exception Title</param>
		/// <param name="message">Exception Message</param>
		/// <param name="resultType">A result type to pass back (allows image/html/document error results)</param>
		public ServiceException(HttpStatusCode statusCode, string message, ExceptionResultType resultType = ExceptionResultType.Object)
			: base(message)
		{
			StatusCode = statusCode;
            Title = string.Format("Service Exception: {0}", Enum.GetName(typeof(HttpStatusCode), statusCode));
			ResultType = resultType;
		}

		public HttpStatusCode StatusCode { get; private set; }
        public string Title { get; private set; }
		public ExceptionResultType ResultType { get; private set; }
	}
}
