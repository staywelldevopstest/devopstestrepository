﻿using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;

namespace KswApi.Interface
{
	[ServiceContract(Name = "Logs", Namespace = "http://www.kramesstaywell.com")]
	public interface ILogService
	{
		[WebInvoke(UriTemplate = "", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_Logs")]
		LogList GetLogs([MessageParameter(Name = "$skip")] int offset, [MessageParameter(Name = "$top")] int count, [MessageParameter(Name = "$filter")] string filter, string sort);

		[WebInvoke(UriTemplate = "{logId}", Method = "GET")]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_Logs")]
		Log GetLog(string logId);
	}
}
