﻿using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.Content;

namespace KswApi.Interface
{
    [ServiceContract(Name = "Content", Namespace = "http://www.kramesstaywell.com")]
    public interface ITaxonomyTypeService
    {
        [WebGet(UriTemplate = "TaxonomyType")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_Content", SpecialAccess = AllowedSpecialAccess.Jsonp)]
        TaxonomyTypeList SearchTaxonomyTypes();

        [WebGet(UriTemplate = "TaxonomyType/{slug}")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_Content", SpecialAccess = AllowedSpecialAccess.Jsonp)]
        TaxonomyTypeResponse GetTaxonomyType(string slug);
    }
}
