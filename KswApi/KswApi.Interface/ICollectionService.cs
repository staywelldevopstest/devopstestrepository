﻿using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.Content;

namespace KswApi.Interface
{
    [ServiceContract(Name = "Collections", Namespace = "http://www.kramesstaywell.com")]
    public interface ICollectionService
    {
        [WebInvoke(UriTemplate = "", Method = "POST")]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_Content")]
        CollectionResponse CreateCollection(CollectionCreateRequest collection);

        [WebInvoke(UriTemplate = "", Method = "GET")]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_Content", SpecialAccess = AllowedSpecialAccess.Jsonp)]
        CollectionListResponse SearchCollections(CollectionSearchRequest request);

        [WebInvoke(UriTemplate = "{idOrSlug}", Method = "GET")]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_Content", SpecialAccess = AllowedSpecialAccess.Jsonp)]
        CollectionResponse GetCollection(string idOrSlug, bool? includeChildren, bool? recursive);

        [WebInvoke(UriTemplate = "{idOrSlug}", Method = "PUT")]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_Content")]
        CollectionResponse UpdateCollection(string idOrSlug, CollectionRequest collection);

        [WebInvoke(UriTemplate = "{idOrSlug}", Method = "DELETE")]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_Content")]
        CollectionResponse DeleteCollection(string idOrSlug);

        [WebInvoke(UriTemplate = "Slugs", Method = "GET")]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_Content", SpecialAccess = AllowedSpecialAccess.Jsonp)]
        SlugResponse GetSlug(string title, string slug);
    }
}
