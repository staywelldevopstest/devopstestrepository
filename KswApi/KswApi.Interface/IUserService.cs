﻿using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace KswApi.Interface
{
	[ServiceContract(Name = "User", Namespace = "http://www.kramesstaywell.com")]
	public interface IUserService
	{
		// This should be public: returns an unauthenticated user object
		// if the user is does not have a valid access token
		[WebInvoke(UriTemplate = "", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Any)]
		AuthenticatedUserResponse GetCurrentUser(bool startTime);

		[WebInvoke(UriTemplate = "Settings", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Any)]
		UserSettingsResponse GetCurrentUserSettings();

		[WebInvoke(UriTemplate = "Settings", Method = "PUT")]
		[OperationContract]
		[Allow(ClientType = ClientType.Any)]
		UserSettingsResponse UpdateCurrentUserSettings(UserSettingsRequest userSettings);

		[WebInvoke(UriTemplate = "Licenses", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Any)]
		ClientLicenseDetail GetUserLicenseDetails();
	}
}
