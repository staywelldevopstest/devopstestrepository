﻿using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.Content;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace KswApi.Interface
{
    [ServiceContract(Name = "Administration", Namespace = "http://www.kramesstaywell.com")]
    public interface IAdministrationService
    {
        #region Role Methods

        [WebInvoke(UriTemplate = "Roles", Method = "POST")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_Roles")]
        Role CreateRole(RoleRequest role);

        [WebGet(UriTemplate = "Roles")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_Roles")]
        RoleList GetRoles();

        [WebGet(UriTemplate = "Roles/{roleId}")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_Roles")]
        Role GetRole(string roleId);

        [WebInvoke(UriTemplate = "Roles/{roleId}", Method = "PUT")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_Roles")]
        Role UpdateRole(string roleId, RoleRequest role);

        [WebInvoke(UriTemplate = "Roles/{roleId}", Method = "DELETE")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_Roles")]
        Role DeleteRole(string roleId);

        [WebGet(UriTemplate = "Rights")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_Roles")]
        RightList GetRights();

        #endregion Role Methods

        #region Client Methods

        [WebInvoke(UriTemplate = "Clients", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_Client")]
        ClientList GetClients(
            [MessageParameter(Name = "$skip")]int offset,
            [MessageParameter(Name = "$top")]int count,
            string query,
            [MessageParameter(Name = "$orderby")] string sort);

        [WebInvoke(UriTemplate = "Clients/{clientId}", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_Client")]
        ClientResponse GetClient(string clientId);

        [WebInvoke(UriTemplate = "Clients", Method = "POST")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_Client")]
        ClientResponse CreateClient(ClientRequest client);

        [WebInvoke(UriTemplate = "Clients/{clientId}", Method = "PUT")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_Client")]
        ClientResponse UpdateClient(string clientId, ClientRequest client);

        [WebInvoke(UriTemplate = "Clients/{clientId}", Method = "DELETE")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_Client")]
        ClientResponse DeleteClient(string clientId);

        #endregion Client Methods

        #region License Methods

		[WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/Content/Buckets", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_License")]
		UnpagedContentBucketList GetLicenseBuckets(string licenseId);

		[WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/Content/Buckets", Method = "PUT")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
		UnpagedContentBucketList UpdateLicenseBuckets(string licenseId, ContentBucketIdListRequest bucketList);

		[WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/Content/Collections", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_License")]
		CollectionListResponse GetLicenseCollections(string licenseId, CollectionSearchRequest request);

		[WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/Content/Collections", Method = "POST")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
		UnpagedCollectionListResponse AddLicenseCollections(string licenseId, CollectionIdListRequest collectionList);

		[WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/Content/Collections/{collectionId}", Method = "DELETE")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
		CollectionResponse RemoveLicenseCollection(string licenseId, string collectionId);
		
		[WebInvoke(UriTemplate = "Licenses", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_License")]
        LicenseList GetLicenses([MessageParameter(Name = "$skip")] int offset,
            [MessageParameter(Name = "$top")] int count,
            string query,
            string clientId,
            [MessageParameter(Name = "$orderby")] string orderBy);

        [WebInvoke(UriTemplate = "Licenses/{licenseId}", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_License")]
        License GetLicense(string licenseId);

        [WebInvoke(UriTemplate = "Licenses", Method = "POST")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
        License CreateLicense(LicenseRequest license);

        [WebInvoke(UriTemplate = "Licenses/{licenseId}", Method = "PUT")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
        License UpdateLicense(string licenseId, LicenseRequest license);

        [WebInvoke(UriTemplate = "Licenses/{licenseId}", Method = "DELETE")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
        License DeleteLicense(string licenseId);

        [WebInvoke(UriTemplate = "Clients/{clientId}/LicenseDetails", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_License")]
        ClientLicenseDetail GetClientLicenseDetail(string clientId, string query);

        [WebInvoke(UriTemplate = "Clients/{clientId}/AvailableParentLicenses", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_License")]
        AvailableParentLicenseList GetAvailableParentLicenses(string clientId, string licenseId, [MessageParameter(Name = "$orderby")] string orderBy);

        [WebInvoke(UriTemplate = "Licenses/{licenseId}/Applications", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_License")]
        ApplicationList GetLicenseApplications(string licenseId);

        [WebInvoke(UriTemplate = "Licenses/{licenseId}/Applications/{applicationId}", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_License")]
        Application GetLicenseApplication(string licenseId, string applicationId);

        [WebInvoke(UriTemplate = "Licenses/{licenseId}/Applications", Method = "POST")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
        Application CreateLicenseApplication(string licenseId, ApplicationRequest application);

        [WebInvoke(UriTemplate = "Licenses/{licenseId}/Applications/{applicationId}", Method = "PUT")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
        Application UpdateLicenseApplication(string licenseId, string applicationId, ApplicationRequest application);

		[WebInvoke(UriTemplate = "Licenses/{licenseId}/Applications/{applicationId}/Secret", Method = "PUT")]
		[OperationContract]
		[Allow(ClientType = ClientType.Any, Rights = "Manage_License")]
		Application ResetLicenseApplicationSecret(string licenseId, string applicationId);

        [WebInvoke(UriTemplate = "Licenses/{licenseId}/Applications/{applicationId}", Method = "DELETE")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
        Application DeleteLicenseApplication(string licenseId, string applicationId);

        #endregion License Methods

        #region Module Methods

        [WebInvoke(UriTemplate = "Modules", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_License")]
        ModuleNameList GetModules();

        [WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_License")]
        LicenseModuleList GetLicenseModules(string licenseId);

        [WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/{moduleType}", Method = "POST")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
        ModuleName AddLicenseModule(string licenseId, string moduleType);

        [WebInvoke(UriTemplate = "Licenses/{licenseId}/Modules/{moduleType}", Method = "DELETE")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_License")]
        ModuleName RemoveLicenseModule(string licenseId, string moduleType);

        #endregion Module Methods

		#region Copyright Methods

		[WebGet(UriTemplate = "Copyrights")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_Administration,Read_Content")]
		CopyrightList GetCopyrights(int offset, int count, string query, bool includeBucketCount);

		[WebInvoke(UriTemplate = "Copyrights", Method = "POST")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_Administration")]
		CopyrightResponse CreateCopyright(CopyrightRequest copyright);

		[WebInvoke(UriTemplate = "Copyrights/{copyrightId}", Method = "PUT")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_Administration")]
		CopyrightResponse UpdateCopyright(string copyrightId, CopyrightRequest copyright);

		[WebInvoke(UriTemplate = "Copyrights/{copyrightId}", Method = "DELETE")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_Administration")]
		CopyrightResponse DeleteCopyright(string copyrightId);


		#endregion

		#region Client User Management Methods

		[WebInvoke(UriTemplate = "ClientUsers", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Read_ClientUsers")]
        ClientUserList GetClientUsers([MessageParameter(Name = "$skip")]int offset,
            [MessageParameter(Name = "$top")]int count,
            string query,
            [MessageParameter(Name = "$orderby")] string sort,
            string filter);

		[WebInvoke(UriTemplate = "ClientUsers/{userIdOrEmail}", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_ClientUsers")]
		ClientUserResponse GetClientUser(string userIdOrEmail);

        [WebInvoke(UriTemplate = "ClientUsers", Method = "POST")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_ClientUsers")]
        ClientUserResponse CreateClientUser(ClientUserRequest clientUser);

        [WebInvoke(UriTemplate = "ClientUsers/{clientUserId}", Method = "PUT")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_ClientUsers")]
		ClientUserResponse UpdateClientUser(string clientUserId, ClientUserUpdateRequest clientUser);

		[WebInvoke(UriTemplate = "ClientUsers/{userIdOrEmail}/Unlock", Method = "PUT")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Rights = "Manage_ClientUsers")]
		void UnlockClientUser(string userIdOrEmail);

        [WebInvoke(UriTemplate = "ClientUsers/{clientUserId}", Method = "DELETE")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_ClientUsers")]
        ClientUserResponse DeleteClientUser(string clientUserId);

        [WebInvoke(UriTemplate = "ClientUsers/AvailableEmails", Method = "GET")]
        [OperationContract]
        [Allow(ClientType = ClientType.Internal, Rights = "Manage_ClientUsers")]
        ClientUserValidation ValidateClientUserEmail(string emailAddress);

		#endregion
	}
}
