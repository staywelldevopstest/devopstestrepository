﻿using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.Content;

namespace KswApi.Interface
{
	[ServiceContract(Name = "Topics", Namespace = "http://www.kramesstaywell.com")]
	public interface ITopicService
	{
		[WebInvoke(UriTemplate = "{idOrSlug}", Method = "GET")]
		[Allow(ClientType = ClientType.Internal, Rights = "Read_Content", SpecialAccess = AllowedSpecialAccess.Jsonp)]
		TopicResponse GetTopic(string idOrSlug, bool? includeChildren, bool? recursive);
	}
}
