﻿using System;
using System.Runtime.Serialization;

namespace KswApi.Objects
{
	[DataContract]
	public class JsonAccessTokenResponse
	{
		// ReSharper disable InconsistentNaming
		[DataMember] 
		public string access_token { get; set; }
		
		[DataMember] 
		public string token_type { get; set; }
		
		[DataMember] 
		public int expires_in { get; set; }
		
		[DataMember] 
		public string refresh_token { get; set; }
		
		[DataMember] 
		public string error { get; set; }
		
		[DataMember] 
		public string error_description { get; set; }
		
		[DataMember] 
		public string error_uri { get; set; }

	// ReSharper restore InconsistentNaming

	}
}
