﻿using KswApi.Interface.Objects;
using KswApi.Objects;

namespace KswApi.Site.Models
{
	public class AuthorizationResult : ResponseBase
	{
		public AccessToken AccessToken { get; set; }
	}
}