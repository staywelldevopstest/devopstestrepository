﻿namespace KswApi.Enums
{
	internal enum HttpMethod
	{
		Unknown,
		Get,
		Post,
		Put,
		Delete
	}
}
