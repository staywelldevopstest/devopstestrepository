﻿namespace KswApi.Enums
{
	public enum TokenStoreType
	{
		Session,
		Cookie,
		SingleApplication,
		PerClient
	}
}
