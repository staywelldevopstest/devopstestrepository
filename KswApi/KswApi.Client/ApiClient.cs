﻿using System.Web;
using KswApi.Enums;
using KswApi.Framework;
using KswApi.Interface;
using KswApi.Interfaces;
using System.Runtime.Serialization;
using Authorization = KswApi.Framework.Authorization;

namespace KswApi
{
	public class ApiClient : ServiceClient
	{
		#region Constants

		#endregion Constants

		#region Static Fields

		#endregion Static Fields

		#region Private Fields

		private Authorization _authorization;
		private readonly string _applicationId;
		private readonly string _applicationSecret;
		private readonly string _serviceUri;

		#endregion Private Fields

		#region Static Constructor

		static ApiClient()
		{
		}

		#endregion Static Constructor

		#region Constructors

		public ApiClient()
			: base(Constant.DEFAULT_API_URI, null, null, null)
		{

		}

		public ApiClient(string serviceUri)
			: base(serviceUri, null, null, null)
		{

		}

		public ApiClient(string applicationId, string applicationSecret)
			: base(Constant.DEFAULT_API_URI, applicationId, applicationSecret, GetDefaultTokenStore())
		{
			_serviceUri = Constant.DEFAULT_API_URI;
			_applicationId = applicationId;
			_applicationSecret = applicationSecret;
		}

		public ApiClient(string applicationId, string applicationSecret, TokenStoreType type)
			: base(Constant.DEFAULT_API_URI, applicationId, applicationSecret, type)
		{
			_serviceUri = Constant.DEFAULT_API_URI;
			_applicationId = applicationId;
			_applicationSecret = applicationSecret;
		}

		public ApiClient(string serviceUri, string applicationId, string applicationSecret)
			: base(serviceUri, applicationId, applicationSecret, GetDefaultTokenStore())
		{
			_serviceUri = serviceUri;
			_applicationId = applicationId;
			_applicationSecret = applicationSecret;
		}

		public ApiClient(string serviceUri, string applicationId, string applicationSecret, TokenStoreType type)
			: base(serviceUri, applicationId, applicationSecret, type)
		{
			_serviceUri = serviceUri;
			_applicationId = applicationId;
			_applicationSecret = applicationSecret;
		}

		public ApiClient(string serviceUri, string applicationId, string applicationSecret, ITokenStore tokenStore)
			: base(serviceUri, applicationId, applicationSecret, tokenStore)
		{
			_serviceUri = serviceUri;
			_applicationId = applicationId;
			_applicationSecret = applicationSecret;
		}

		#endregion Constructors

		#region Public Properties

		[IgnoreDataMember]
		public Authorization Authorization { get { return (_authorization ?? (_authorization = new Authorization(_serviceUri, _applicationId, _applicationSecret, TokenStore))); } }

		public IAdministrationService Administration { get { return GetService<IAdministrationService>(); } }
		public ISmsGatewayAdministrationService SmsGatewayAdministration { get { return GetService<ISmsGatewayAdministrationService>(); } }
		public ILogService Logs { get { return GetService<ILogService>(); } }
		public IMonitoringService Monitoring { get { return GetService<IMonitoringService>(); } }
		public ISmsGatewayService SmsGateway { get { return GetService<ISmsGatewayService>(); } }
		public IContentService Content { get { return GetService<IContentService>(); } }
		public IBucketService Buckets { get { return GetService<IBucketService>(); } }
		public IOriginService Origins { get { return GetService<IOriginService>(); } }
		public ILanguageService Languages { get { return GetService<ILanguageService>(); } }
		public IReportsService Reports { get { return GetService<IReportsService>(); } }
		public ITasksService Tasks { get { return GetService<ITasksService>(); } }
		public IUserService User { get { return GetService<IUserService>(); } }
		public ITestService Test { get { return GetService<ITestService>(); } }
		public ISecurityService Security { get { return GetService<ISecurityService>(); } }
		public ITaxonomyTypeService TaxonomyType { get { return GetService<ITaxonomyTypeService>(); } }
		public IServicelinesService Servicelines { get { return GetService<IServicelinesService>(); } }
		public ICollectionService Collections { get { return GetService<ICollectionService>(); } }
		public ITopicService Topics { get { return GetService<ITopicService>(); } }
		public IRenderingService Rendering { get { return GetService<IRenderingService>(); } }

		#endregion Public Properties

		#region Private Methods

		private static TokenStoreType GetDefaultTokenStore()
		{
			if (HttpContext.Current != null)
				return TokenStoreType.Cookie;

			return TokenStoreType.PerClient;
		}

		#endregion
	}
}
