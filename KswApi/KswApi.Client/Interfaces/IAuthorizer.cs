﻿namespace KswApi.Interfaces
{
	public interface IAuthorizer
	{
		 string GetClientToken();
	}
}
