﻿using KswApi.Framework;

namespace KswApi.Interfaces
{
	interface IServiceChannel
	{
		object Invoke(OperationRequest operation);
	}
}
