﻿using KswApi.Objects;

namespace KswApi.Interfaces
{
	public interface ITokenStore
	{
		AccessToken GetToken();
		void SetToken(AccessToken value);
		void RemoveToken();
	}
}
