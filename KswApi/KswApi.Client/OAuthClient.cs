﻿using System;
using System.Collections.Specialized;
using KswApi.Framework;
using KswApi.Interface;
using KswApi.Interface.Objects;
using KswApi.Objects;

namespace KswApi
{
	public class OAuthClient
	{
		private readonly string _serviceUri;
		private readonly ServiceProxy<IOAuthService> _proxy;
		private readonly IOAuthService _service;

		public OAuthClient()
			: this(Constant.DEFAULT_API_URI)
		{
		}

		public OAuthClient(string serviceUri)
		{
			_serviceUri = serviceUri;

			_proxy = new ServiceProxy<IOAuthService>(serviceUri);

			_service = _proxy.GetService();
		}

		public AccessToken RefreshUserToken(string refreshToken, string applicationId, string applicationSecret)
		{
			OAuthTokenRequest request = new OAuthTokenRequest
			                            {
				                            grant_type = Constant.OAuthGrantTypes.REFRESH,
				                            refresh_token = refreshToken,
				                            client_id = applicationId,
				                            client_secret = applicationSecret
			                            };

			OAuthTokenResponse response = _service.CreateAccessToken(request);

			return GetResponse(response, AccessTokenType.User);
		}

		public AccessToken GetUserToken(string authorizationCode, string clientId, string clientSecret, string applicationUri)
		{
			OAuthTokenRequest request = new OAuthTokenRequest
			                            {
				                            grant_type = Constant.OAuthGrantTypes.USER,
				                            code = authorizationCode,
				                            client_id = clientId,
				                            client_secret = clientSecret,
											redirect_uri = applicationUri
			                            };

			OAuthTokenResponse response = _service.CreateAccessToken(request);

			return GetResponse(response, AccessTokenType.User);
		}

		public string GetUserLoginAddress(string applicationId, string applicationUri)
		{
			//string authServerUri = ConfigurationManager.AppSettings[AppSettingKeys.AUTHORIZATION_SERVER_URI];
			//string uri = KswApi.Framework.UriHelper.Combine(authServerUri, OAUTH_AUTHORIZATION_ADDRESS);
			//NameValueCollection parameters = new NameValueCollection
			//	{
			//		{"response_type", "code"},
			//		{"client_id", Constant.WEB_PORTAL_CLIENT_ID},
			//		{"redirect_uri", ConfigurationManager.AppSettings[AppSettingKeys.PORTAL_HOME_URI]}
			//	};

			//return UriHelper.AddQuery(uri, parameters);
			return null;
		}

		public AccessToken GetApplicationToken(string applicationId, string applicationSecret, string state = null)
		{
			OAuthTokenRequest request = new OAuthTokenRequest
			                            {
											grant_type = Constant.OAuthGrantTypes.APPLICATION,
				                            client_id = applicationId,
				                            client_secret = applicationSecret,
											state = state
			                            };

			OAuthTokenResponse response = _service.CreateAccessToken(request);

			return GetResponse(response, AccessTokenType.Application);
		}

		private AccessToken GetResponse(OAuthTokenResponse response, AccessTokenType type)
		{
			DateTime now = DateTime.UtcNow;

			return new AccessToken
			{
				Token = response.access_token,
				AuthorizationServer = _serviceUri,
				ExpiresIn = response.expires_in,
				CreationTime = now,
				RefreshToken = response.refresh_token,
				Type = type
			};
		}
	}
}
