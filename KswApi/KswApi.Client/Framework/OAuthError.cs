﻿using System.Runtime.Serialization;

// ReSharper disable InconsistentNaming

namespace KswApi.Framework
{
	[DataContract]
	class OAuthError
	{
		[DataMember]
		public string error { get; set; }

		[DataMember]
		public string error_description { get; set; }
	}
}
