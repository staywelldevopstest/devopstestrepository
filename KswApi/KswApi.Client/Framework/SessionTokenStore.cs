﻿using System;
using System.Web;
using KswApi.Interfaces;
using KswApi.Objects;

namespace KswApi.Framework
{
	class SessionTokenStore : ITokenStore
	{
		private const string SESSION_NAME_PREFIX = "KswApi.Token";

		public AccessToken GetToken()
		{
			if (HttpContext.Current == null)
				throw GetExceptionForInvalidContext();

			return HttpContext.Current.Session[SESSION_NAME_PREFIX] as AccessToken;
		}

		public void SetToken(AccessToken value)
		{
			if (HttpContext.Current == null)
				throw GetExceptionForInvalidContext();

			HttpContext.Current.Session[SESSION_NAME_PREFIX] = value;
		}

		public void RemoveToken()
		{
			if (HttpContext.Current == null)
				throw GetExceptionForInvalidContext();

			HttpContext.Current.Session.Remove(SESSION_NAME_PREFIX);
		}

		private Exception GetExceptionForInvalidContext()
		{
			return new InvalidOperationException("A client with a session token store can only be used with an active request context.");
		}
	}
}
