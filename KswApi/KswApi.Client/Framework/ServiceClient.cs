﻿using System;
using System.Collections.Concurrent;
using KswApi.Enums;
using KswApi.Interfaces;

namespace KswApi.Framework
{
	public abstract class ServiceClient
	{
		#region Private Fields

		private readonly string _serviceUri;
		private readonly ConcurrentDictionary<Type, object> _services = new ConcurrentDictionary<Type, object>();
		private readonly string _applicationId;
		private readonly string _applicationSecret;

		#endregion Private Fields

		#region Public Constructors

		protected ServiceClient(string serviceUri, string applicationId, string applicationSecret, TokenStoreType tokenStoreType)
		{
			_serviceUri = serviceUri;

			_applicationId = applicationId;
			_applicationSecret = applicationSecret;

			TokenStore = GetTokenStore(tokenStoreType);
		}

		protected ServiceClient(string serviceUri, string applicationId, string applicationSecret, ITokenStore tokenStore)
		{
			_serviceUri = serviceUri;
			_applicationId = applicationId;
			_applicationSecret = applicationSecret;

			TokenStore = tokenStore;
		}

		#endregion Public Constructors

		#region Protected Properties

		protected ITokenStore TokenStore { get; private set; }

		#endregion Protected Properties
		/// <summary>
		/// Lazy loads the service
		/// </summary>
		/// <typeparam name="TService"></typeparam>
		/// <returns></returns>
		public TService GetService<TService>()
		{
			return (TService)_services.GetOrAdd(typeof(TService), type => CreateService<TService>());
		}

		private TService CreateService<TService>()
		{
			ServiceProxy<TService> proxy = new ServiceProxy<TService>(_serviceUri, _applicationId, _applicationSecret, TokenStore);

			return (TService)proxy.GetTransparentProxy();
		}

		private ITokenStore GetTokenStore(TokenStoreType tokenStoreType)
		{
			switch (tokenStoreType)
			{
				case TokenStoreType.Session:
					return new SessionTokenStore();
				case TokenStoreType.Cookie:
					return new CookieTokenStore();
				case TokenStoreType.SingleApplication:
					return new SingleApplicationTokenStore();
				case TokenStoreType.PerClient:
					return new PerClientTokenStore();
				default:
					throw new ArgumentOutOfRangeException("tokenStoreType");
			}
		}
	}
}
