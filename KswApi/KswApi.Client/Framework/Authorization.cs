﻿using KswApi.Exceptions;
using KswApi.Interfaces;
using KswApi.Objects;
using KswApi.Site.Models;
using System;
using System.Web;

namespace KswApi.Framework
{
	public class Authorization
	{
		private const string MODULE = "OAuth";
		private const string AUTHORIZATION_METHOD = "Authorize";

		private readonly ITokenStore _tokenStore;
		private readonly string _serviceUri;
		private readonly string _clientId;
		private readonly string _clientSecret;

		internal Authorization(string serviceUri, string clientId, string clientSecret, ITokenStore tokenStore)
		{
			_serviceUri = serviceUri;
			_tokenStore = tokenStore;
			_clientId = clientId;
			_clientSecret = clientSecret;
		}

		public AuthorizationResult Authorize(string authorizationCode, string redirectUri)
		{
			OAuthClient client = new OAuthClient(_serviceUri);

			try
			{
				AccessToken accessToken = client.GetUserToken(authorizationCode, _clientId, _clientSecret, redirectUri);

				_tokenStore.SetToken(accessToken);

				return new AuthorizationResult
						   {
							   IsSuccessful = true,
							   AccessToken = accessToken
						   };
			}
			catch (OAuthException exception)
			{
				return new AuthorizationResult
						   {
							   IsSuccessful = false,
							   ErrorDetails = exception.ErrorDescription
						   };
			}
		}

		public void Unauthorize()
		{
			_tokenStore.RemoveToken();
		}

		public bool IsAuthorized
		{
			get
			{
				AccessToken token = _tokenStore.GetToken();
				if (token == null)
					return false;

				DateTime now = DateTime.UtcNow;

				if (now < token.CreationTime + TimeSpan.FromSeconds(token.ExpiresIn))
					return true;

				// if we have a refresh token
				if (!string.IsNullOrEmpty(token.RefreshToken) && now < token.CreationTime + TimeSpan.FromHours(Constant.REFRESH_TOKEN_EXPIRATION_IN_HOURS))
					return true;

				return false;
			}
		}
	}
}
