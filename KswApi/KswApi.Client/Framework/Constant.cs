﻿namespace KswApi.Framework
{
	public static class Constant
	{
		public const int REFRESH_TOKEN_EXPIRATION_IN_HOURS = 23; // Actual expiration is 24 hours, expire after 23 on the client side to be sure
		public const string DEFAULT_API_URI = "https://api.kramesstaywell.com";
		
		public static class OAuthGrantTypes
		{
			public const string REFRESH = "refresh_token";
			public const string USER = "authorization_code";
			public const string APPLICATION = "client_credentials";
		}
	}
}
