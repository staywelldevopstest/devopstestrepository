﻿using KswApi.Interfaces;
using KswApi.Objects;

namespace KswApi.Framework
{
	class PerClientTokenStore : ITokenStore
	{
		private AccessToken _token;

		public AccessToken GetToken()
		{
			return _token;
		}

		public void SetToken(AccessToken value)
		{
			_token = value;
		}

		public void RemoveToken()
		{
			_token = null;
		}
	}
}
