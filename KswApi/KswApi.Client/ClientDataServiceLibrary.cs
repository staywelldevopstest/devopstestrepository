﻿using System.Collections.Specialized;
using System.Globalization;
using KswApi.Enum;
using KswApi.Framework;
using KswApi.Interface;
using KswApi.Interface.Objects;

namespace KswApi
{
	public class ClientDataServiceLibrary : ClientBase, IClientDataManagementService
	{
		private const string MODULE = "ClientDataManagement";

		/// <summary>
		/// This is a public enum use to determine how the results will be formatted.
		/// </summary>
		private readonly ResultType _resultType;

		private readonly string _serviceUri;
		
		/// <summary>
		/// Default constructor.  Will default the Result Type to JSON
		/// </summary>
		public ClientDataServiceLibrary(string serviceUri)
		{
			_serviceUri = serviceUri;
			_resultType = ResultType.Json;
		}

		/// <summary>
		/// Constructor to set the Result Type (see ResultTypes enum)
		/// </summary>
		/// <param name="serviceUri">Specifiy alternate service URL</param>
		/// <param name="resultType">Specifiy whether to return either XML or JSON</param>
		public ClientDataServiceLibrary(string serviceUri, ResultType resultType)
		{
			_serviceUri = serviceUri;
			_resultType = resultType;
		}

		/// <summary>
		/// Gets a paged client list against the provided filter
		/// </summary>
		/// <param name="offset">Page number to get</param>
		/// <param name="count">Number of clients to return per page</param>
		/// <param name="filter">Filter string to filter list (client name only)</param>
		/// <returns></returns>
		public ClientDataList GetClients(int offset, int count, string filter)
		{
			NameValueCollection parameters = new NameValueCollection
				{{"offset", offset.ToString(CultureInfo.InvariantCulture)}, {"count", count.ToString(CultureInfo.InvariantCulture)}, {"filter", filter}};

			ClientDataList result = Get<ClientDataList>(_serviceUri, MODULE, _resultType == ResultType.Json ? "GetClients.json" : "GetClients.xml", parameters);

			return result;
		}

		/// <summary>
		///  Gets a paged client list
		/// </summary>
		/// <param name="offset">Page number to get</param>
		/// <param name="count">Number of clients to return per page</param>
		/// <returns></returns>
		public ClientDataList GetClients(int offset, int count)
		{
			NameValueCollection parameters = new NameValueCollection { { "offset", offset.ToString(CultureInfo.InvariantCulture) }, { "count", count.ToString(CultureInfo.InvariantCulture) } };

			ClientDataList result = Get<ClientDataList>(_serviceUri, MODULE, _resultType == ResultType.Json ? "GetClients.json" : "GetClients.xml", parameters);

			return result;
		}

		/// <summary>
		/// Creates a new client
		/// </summary>
		/// <param name="client">New client to create</param>
		/// <returns></returns>
		public ClientData CreateClient(ClientData client)
		{
			ClientData result = Post<ClientData>(_serviceUri, MODULE, _resultType == ResultType.Json ? "CreateClient.json" : "CreateClient.xml");

			return result;
		}
	}
}
