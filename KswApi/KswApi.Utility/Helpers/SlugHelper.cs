﻿using System.Collections.Generic;
using System.Text;

namespace KswApi.Utility.Helpers
{
	public static class SlugHelper
	{
		private static readonly Dictionary<char, char> Substitite = new Dictionary<char, char>
		                                                             {
			                                                             {'à', 'a'},
			                                                             {'è', 'e'},
			                                                             {'ì', 'i'},
			                                                             {'ò', 'o'},
			                                                             {'ù', 'u'},
			                                                             {'ä', 'a'},
			                                                             {'ë', 'e'},
			                                                             {'ï', 'i'},
			                                                             {'ö', 'o'},
			                                                             {'ü', 'u'},
			                                                             {'â', 'a'},
			                                                             {'ê', 'e'},
			                                                             {'î', 'i'},
			                                                             {'ô', 'o'},
			                                                             {'û', 'u'},
			                                                             {'á', 'a'},
			                                                             {'é', 'e'},
			                                                             {'í', 'i'},
			                                                             {'ó', 'o'},
			                                                             {'ú', 'u'},
			                                                             {'ã', 'a'},
			                                                             {'ñ', 'n'},
			                                                             {'õ', 'o'},
			                                                             {'š', 's'},
			                                                             {'ž', 'z'},
			                                                             {'ç', 'c'},
			                                                             {'å', 'a'},
			                                                             {'ø', 'o'}
		                                                             };
		public static string CreateSlug(string fromString)
		{
			StringBuilder builder = new StringBuilder();
			foreach (char c in fromString.Trim())
			{
				if (char.IsLetterOrDigit(c))
				{
					if (c < 128)
					{
						builder.Append(char.ToLower(c));
					}
					else
					{
						char substitute;
						if (Substitite.TryGetValue(c, out substitute))
							builder.Append(substitute);
					}
					continue;
				}

				switch (c)
				{
					case '&':
						builder.Append("and");
						break;
					case '$':
						builder.Append('s');
						break;
					case ' ':
						builder.Append('-');
						break;
					case '-':
						builder.Append('-');
						break;
				}
			}

			return builder.ToString();
		}
	}

}
