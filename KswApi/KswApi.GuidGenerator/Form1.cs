﻿using System;
using System.Windows.Forms;

namespace KswApi.GuidGenerator
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			textBox1.Text = Guid.NewGuid().ToString();
		}

		private void button1_Click(object sender, EventArgs e)
		{
			textBox1.Text = Guid.NewGuid().ToString();
		}
	}
}
