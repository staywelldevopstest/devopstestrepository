﻿using System.Xml.Serialization;

namespace KswApi.RestrictedInterface.Objects
{
	[XmlRoot("AuthorizationGrant")]
	public class AuthorizationGrantResponse
	{
		public bool IsValid { get; set; }
		public string RedirectUri { get; set; }
		public string Error { get; set; }
	}
}
