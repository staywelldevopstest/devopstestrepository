﻿namespace KswApi.RestrictedInterface.Objects
{
	public class Header
	{
		public string Name { get; set; }
		public string Value { get; set; }
	}
}
