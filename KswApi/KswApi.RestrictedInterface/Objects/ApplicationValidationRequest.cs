﻿using System.Xml.Serialization;

namespace KswApi.RestrictedInterface.Objects
{
	[XmlRoot("ApplicationValidation")]
    public class ApplicationValidationRequest
    {
        public string ResponseType { get; set; }
        public string ApplicationId { get; set; }
        public string RedirectUri { get; set; }
        public string Scope { get; set; }
        public string State { get; set; }
    }
}
