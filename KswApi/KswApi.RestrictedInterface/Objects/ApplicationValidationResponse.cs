﻿using System.Xml.Serialization;

namespace KswApi.RestrictedInterface.Objects
{
	[XmlRoot("ApplicationValidation")]
	public class ApplicationValidationResponse
	{
		public string Warning { get; set; }
	}
}
