﻿using System.Xml.Serialization;

namespace KswApi.RestrictedInterface.Objects
{
	[XmlRoot("AuthorizationGrant")]
    public class AuthorizationGrantRequest : ApplicationValidationRequest
    {
        public string UserName { get; set; }
        public string Password { get; set; }
    }
}
