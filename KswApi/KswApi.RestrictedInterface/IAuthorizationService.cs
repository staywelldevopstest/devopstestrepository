﻿using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.RestrictedInterface.Objects;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace KswApi.RestrictedInterface
{
    [ServiceContract(Name = "Authorization", Namespace = "http://www.kramesstaywell.com")]
    public interface IAuthorizationService
    {
        [WebInvoke(UriTemplate = "ApplicationValidations", Method = "GET")]
        [OperationContract]
		[Allow(ClientType = ClientType.Authentication)]
		ApplicationValidationResponse GetApplicationValidation(string responseType, string applicationId, string redirectUri, string scope, string state);

        [WebInvoke(UriTemplate = "AuthorizationGrants", Method = "POST")]
        [OperationContract]
        [Allow(ClientType = ClientType.Authentication, Logging = AllowedLogging.LogWithoutBody)]
		AuthorizationGrantResponse CreateAuthorizationGrant(AuthorizationGrantRequest request);

		[WebInvoke(UriTemplate = "Passwords", Method = "POST")]
		[OperationContract]
		[Allow(ClientType = ClientType.Authentication, Logging = AllowedLogging.LogWithoutBody)]
		void SetClientUserPassword(PasswordRequest request);

		[WebInvoke(UriTemplate = "Passwords/Tokens/{token}", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Authentication, Logging = AllowedLogging.LogWithoutBody)]
		void ValidateClientUserPasswordToken(string token);

		[WebInvoke(UriTemplate = "PasswordReset", Method = "POST")]
		[OperationContract]
		[Allow(ClientType = ClientType.Authentication, Logging = AllowedLogging.LogWithoutBody)]
		void ResetClientUserPassword(PasswordResetRequest request);

		[WebInvoke(UriTemplate = "KeepAlive", Method = "GET")]
		[OperationContract]
		[Allow(ClientType = ClientType.Internal, Logging = AllowedLogging.NeverLog)]
		KeepAliveResponse KeepAlive(int idleMilliseconds);
    }
}
