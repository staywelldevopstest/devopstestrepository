@echo off
cls
setlocal

set fortify_project=KSW API
set fortify_build_id=KSWAPI
set fortify_version=KSW API
set solution_name=KswApi.sln
set main_bin_path=KswApi.Service\bin
set notify_email_list=abennett@medimedia.com

echo Current directory: %cd%
echo Executing directory: %~dp0%

:: ignore the source path that was passed in, and use the path of this script as a starting point
pushd %~dp0%

:: navigate to the correct source path
:tryagain
::echo %cd%\%solution_name%
if not exist "%cd%\%solution_name%" (
	if "%cd:~-2%"==":\" (
		call :SendEmailAndExit "Could not find correct target_subfolder location."
		goto :end
	)
	cd ..
	goto :tryagain
)

set source_path=%cd%
set vs_solution_file=%source_path%\%solution_name%
echo Solution file found at: %vs_solution_file%
set main_bin_path=%source_path%\%main_bin_path%

if not exist "%source_path%" (
	echo source_path must be the parent to the '%target_subfolder%' folder.
	goto :end
)

set fortify_project_path=%source_path%\Security\%fortify_build_id%.fpr

:: Set paths
set vs2012_path=C:\Program Files (x86)\Microsoft Visual Studio 11.0
set fortify_path=C:\Program Files\HP_Fortify\HP_Fortify_SCA_and_Apps_3.90
set path=%fortify_path%\bin;%vs2012_path%\Common7\IDE;%vs2012_path%\Common7\Tools;%PATH%

:: Delete any existing Fortify scan and any existing bin/obj folders, or just the main project bin
if exist "%fortify_project_path%" (del "%fortify_project_path%")
if exist "%source_path%\Clean.bat" (pushd "%source_path%" & call Clean.bat & popd) else (rmdir /s /q "%main_bin_path%")

:: Run Fortify's sourceanalyzer on the project
sourceanalyzer -clean -b %fortify_build_id%
sourceanalyzer -Xmx1536m -b %fortify_build_id% devenv "%vs_solution_file%" /REBUILD Debug
sourceanalyzer -Xmx1536m -disable-language javascript -b %fortify_build_id% -scan -f "%fortify_project_path%" > "%source_path%\Security\sourceanalyzer_err.txt" 2>&1
type "%source_path%\Security\sourceanalyzer_err.txt"

if not exist "%fortify_project_path%" (
	call :SendEmailAndExit "Fortify project file was not created"
	goto :end
)

:: Check the last sourceanalyzer output for the message 'Build ID ... doesn't exist.', which indicates that the scan failed to run
for /f "tokens=*" %%a in ("%source_path%\Security\sourceanalyzer_err.txt") do (
	if "%%a"=="[error]: Build ID "%fortify_build_id%" doesn't exist." (
		call :SendEmailAndExit "%%a"
		goto :end
	)
)

:: Remove ExternalMetadata\externalmetadata.xml from the archive
if exist "C:\Program Files\7-Zip\7z.exe" ("C:\Program Files\7-Zip\7z.exe" -tzip d "%fortify_project_path%" ExternalMetadata\externalmetadata.xml -w "%source_path%\Security")

::call :InstallSslCertificate
call fortifyclient uploadFPR -url http://10.10.20.220:8080/f360 -f "%fortify_project_path%" -project "%fortify_project%" -version "%fortify_version%" -user Builder -password Aut0Builder! > "%source_path%\Security\fortifyclient_err.txt" 2>&1
type "%source_path%\Security\fortifyclient_err.txt"
:: Check the fortifyclient output for a message indicating that the scan failed to upload
for /f "tokens=*" %%a in ("%source_path%\Security\fortifyclient_err.txt") do (
	if "%%a"=="The HP Fortify SSC server could not be contacted.  Please check your network connection and try again.  If the error persists, please contact your system administrator." (
		call :SendEmailAndExit "%%a"
		goto :end
	)
)

goto :end

:InstallSslCertificate
set keytool_cmd=keytool -importcert -noprompt -trustcacerts -alias SSC -file "%source_path%\Security\KSW.cer" -keystore ..\lib\security\cacerts -storepass changeit
pushd %fortify_path%\jre64\bin
%keytool_cmd%
popd
pushd %fortify_path%\jre\bin
%keytool_cmd%
popd
goto :eof

:SendEmailAndExit
set message=The Fortify scan has failed. Reason: %1.
echo %message%
%SystemRoot%\system32\WindowsPowerShell\v1.0\PowerShell.exe -Command "$emailFrom = \"noreply@kramesstaywell.com\"; $emailTo = \"%notify_email_list%\"; $subject = \"Fortify scan failure for '%fortify_build_id%'\"; $body = \"%message%\"; $smtpServer = \"10.10.20.30\"; $smtp = new-object Net.Mail.SmtpClient($smtpServer); $smtp.Send($emailFrom, $emailTo, $subject, $body)"
exit /b 1
goto :eof

:end
endlocal
goto :eof