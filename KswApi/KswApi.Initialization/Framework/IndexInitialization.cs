﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using KswApi.Common.Configuration;
using KswApi.Common.Enums;
using KswApi.ElasticSearch;
using KswApi.Indexing;
using KswApi.Interface.Exceptions;
using KswApi.Ioc;
using KswApi.Logic.ContentLogic;
using KswApi.Poco.Content;

namespace KswApi.Initialization.Framework
{
	internal class IndexInitialization
	{
		public void InitializeIndex()
		{
			// if we're debugging, accept all certificates
			// this allows us to execute on dev machines without installing certs locally

			if (Settings.Current != null && (Settings.Current.Environment == EnvironmentName.Debug || Settings.Current.Environment == EnvironmentName.EA))
				ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

			ElasticSearchIndex index = new ElasticSearchIndex();

			Dependency.Set<IIndex>(index);

			if (string.IsNullOrEmpty(Settings.Current.Index.Reindex))
			{
				index.Initialize();

				MapIndexes();
			}
			else
			{
				index.Initialize();

				string[] split = Settings.Current.Index.Reindex.Split(',');

				IEnumerable<IndexType> types = split.Select(item =>
																{
																	IndexType type;
																	if (!Enum.TryParse(item.Trim(), out type))
																		throw new ServiceException(HttpStatusCode.BadRequest, string.Format("Invalid configuration value for reindex: {0}.", item));
																	return type;
																});

				foreach (IndexType type in types)
				{
					switch (type)
					{
						case IndexType.Content:
							index.DeleteType(IndexType.Content);
							index.DeleteType(IndexType.Published);
							ContentIndexer contentIndexer = new ContentIndexer();
							contentIndexer.Map();
							ContentLogic contentLogic = new ContentLogic();
							contentLogic.Reindex();
							break;
						case IndexType.Image:
							index.DeleteType(IndexType.Image);
							ImageIndexer imageIndexer = new ImageIndexer();
							imageIndexer.Map();
							ImageLogic imageLogic = new ImageLogic();
							imageLogic.Reindex();
							break;
						case IndexType.Collection:
							index.DeleteType(IndexType.Collection);
							CollectionIndexer collectionIndexer = new CollectionIndexer();
							collectionIndexer.Map();
							CollectionLogic collectionLogic = new CollectionLogic();
							collectionLogic.Reindex();
							break;
					}
				}

				MapIndexes();
			}
		}

		private void MapIndexes()
		{
			ContentIndexer contentIndexer = new ContentIndexer();
			contentIndexer.Map();

			ImageIndexer imageIndexer = new ImageIndexer();
			imageIndexer.Map();

			CollectionIndexer collectionIndexer = new CollectionIndexer();
			collectionIndexer.Map();
		}
	}
}
