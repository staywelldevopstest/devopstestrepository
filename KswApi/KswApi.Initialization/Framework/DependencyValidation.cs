﻿using System;
using KswApi.Common.Configuration;
using KswApi.Common.Exceptions;
using KswApi.Common.Interfaces;
using KswApi.Ioc;
using KswApi.Logging.Interfaces;
using KswApi.Logic.Framework.Interfaces;
using KswApi.Repositories.Interfaces;

namespace KswApi.Initialization.Framework
{
	class DependencyValidation
	{
		#region Public Methods

		public void ValidateDependencies()
		{
			ValidateType<IUserAuthenticator>();
			ValidateType<ICache>();
			ValidateType<ILog>();
			ValidateType<Settings>();
			ValidateType<IRepositoryFactory>();
		}

		public void DisposeDependencies()
		{
			DisposeIfDisposable<ICache>();
			DisposeIfDisposable<ILog>();
			DisposeIfDisposable<IUserAuthenticator>();
		}

		#endregion Public Methods

		#region Private Methods

		private void ValidateType<T>()
		{
			if (!Dependency.Has<T>())
				throw new ConfigurationValidationException(typeof(T));
		}

		private void DisposeIfDisposable<T>()
		{
			try
			{
				IDisposable disposable = Dependency.Get<T>() as IDisposable;
				if (disposable != null)
					disposable.Dispose();
			}
			catch (InvalidOperationException)
			{
				// allow dispose exceptions to continue
			}
		}

		#endregion Private Methods
	}
}
