﻿namespace KswApi.Initialization.Framework
{
    class Constant
    {
        public static class AppSettings
        {
            public const string NCACHE_NAME = "KswApiCache";
            public const string WEB_PORTAL_CLIENT_SECRET = "KswApiClientSecret";
            public const string WEB_PORTAL_HOME_URI = "KswApiPortalHomeUri";
			public const string WEB_PORTAL_BASE_URI = "KswApiPortalBaseUri";
            public const string WEB_PORTAL_IPS = "KswApiPortalIps";
			public const string AUXILIARY_CLIENT_SECRET = "KswApiAuxiliarySecret";
			public const string AUXILIARY_CLIENT_IPS = "KswApiAuxiliaryIps";
            public const string ACTIVE_DIRECTORY_DOMAIN = "KswApiActiveDirectoryDomain";
            public const string ACTIVE_DIRECTORY_USER = "KswApiActiveDirectoryUser";
            public const string ACTIVE_DIRECTORY_PASSWORD = "KswApiActiveDirectoryPassword";
            public const string TWILIO_ACCOUNT_SID = "KswApiTwilioAccountSid";
            public const string TWILIO_AUTH_TOKEN = "KswApiTwilioAuthToken";
            public const string TWILIO_TEST_ACCOUNT_SID = "KswApiTwilioTestAccountSid";
            public const string TWILIO_TEST_AUTH_TOKEN = "KswApiTwilioTestAuthToken";
            public const string ELASTICSEARCH_URI = "KswApiElasticSearchUri";
            public const string ELASTICSEARCH_USER = "KswApiElasticSearchUser";
            public const string ELASTICSEARCH_PASSWORD = "KswApiElasticSearchPassword";
	        public const string SMTP_SERVER = "SmtpServer";
        }

		public static class ConnectionStrings
		{
			public const string THREE_M_HDD = "ThreeMHdd";
		}
    }
}
