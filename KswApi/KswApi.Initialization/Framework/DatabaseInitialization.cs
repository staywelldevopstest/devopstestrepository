﻿using System;
using KswApi.Caching;
using KswApi.Caching.Caches;
using KswApi.Common.Configuration;
using KswApi.Common.Interfaces;
using KswApi.Data.Mongo;
using KswApi.Data.Mongo.BinaryStore;
using KswApi.Data.Mongo.Framework;
using KswApi.Ioc;
using KswApi.Logging;
using KswApi.Repositories;
using KswApi.Repositories.Interfaces;

namespace KswApi.Initialization.Framework
{
	internal class DatabaseInitialization
	{
		public void InitializeDatabase()
		{
			MongoInitialization initialization = new MongoInitialization();
			initialization.InitializeClientSettings();

            // setup the database uncached initially, then add caching
            // this lets us write cache errors to the log in the database
            IRepositoryFactory factory = SetupRepository();
            SetupCache(factory);

			SchemaUpdater updater = new SchemaUpdater();
			updater.UpdateSchema();

			SetupDatabaseIndexes();
		}

		private void SetupCache(IRepositoryFactory dataLayerRepositoryFactory)
		{
			switch (Settings.Current.Cache.Type)
			{
				case CacheType.None:
					SetupNoCaching();
					break;
				case CacheType.NCache:
					SetupNCache(dataLayerRepositoryFactory);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		private void SetupNoCaching()
		{
			ICache cache = new DummyCache();

			RegisterCache(cache);
		}

		private void SetupNCache(IRepositoryFactory dataLayerRepositoryFactory)
		{
			try
			{
				ICache cache = new NCache(Settings.Current.Cache.Name);
				// Both NCache and Redis are implemented, if we switch to redis, use the following.
				//cache = new RedisCache("10.10.20.160");

				RegisterCache(cache);

				IRepositoryFactory cachedRepositoryFactory = new RepositoryCacheFactory(dataLayerRepositoryFactory);

				Dependency.Set(cachedRepositoryFactory);
			}
			catch (Exception ex)
			{
				// if cache initialization fails, set up the dummy cache and log the issue

				Logger.Log(ex);

				SetupNoCaching();
			}
		}

		private IRepositoryFactory SetupRepository()
		{
			MongoRepositoryFactory factory = new MongoRepositoryFactory();

			Dependency.Set((IRepositoryFactory)factory);

			Dependency.Set((IBinaryStoreFactory)factory);

			SetupGridFs();

			return factory;
		}

		private void SetupDatabaseIndexes()
		{
			DatabaseIndexer indexer = new DatabaseIndexer();
			indexer.RegisterIndexes();
		}

		private void SetupGridFs()
		{
			Repository repository = new Repository();
			GridFsStore store = repository.Content.ImageStore as GridFsStore;

			if (store != null)
				store.EnsureIndexes();
		}

		private void RegisterCache(ICache cache)
		{
			Dependency.Set(cache);
		}
	}
}
