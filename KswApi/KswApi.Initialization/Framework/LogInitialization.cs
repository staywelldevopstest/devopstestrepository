﻿using KswApi.Ioc;
using KswApi.Logging.Interfaces;
using KswApi.Logging.Logs;

namespace KswApi.Initialization.Framework
{
	internal class LogInitialization
	{
		public void IntializeLog()
		{
			RegisterLog(new RepositoryLog());
		}

		private void RegisterLog(ILog log)
		{
			Dependency.Set(log);
		}
	}
}
