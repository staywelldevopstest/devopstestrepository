﻿using Kswapi.ExternalServices.Rendering.XmPie;
using Kswapi.ExternalServices.XmPie.Account;
using Kswapi.ExternalServices.XmPie.Campaign;
using Kswapi.ExternalServices.XmPie.DataSource;
using Kswapi.ExternalServices.XmPie.DataSourcePlanUtils;
using Kswapi.ExternalServices.XmPie.Document;
using Kswapi.ExternalServices.XmPie.Job;
using Kswapi.ExternalServices.XmPie.JobTicket;
using Kswapi.ExternalServices.XmPie.PlanUtils;
using Kswapi.ExternalServices.XmPie.Production;
using KswApi.Interface.Rendering;
using KswApi.Logic;
using KswApi.Logic.ContentLogic;
using Kswapi.ExternalServices;
using KswApi.Ioc;
using KswApi.Logic.Administration;
using KswApi.Logic.Enums;
using KswApi.Logic.Framework;
using KswApi.Logic.Framework.Interfaces;
using KswApi.Logic.Rendering;
using KswApi.Logic.SmsLogic;
using System;

namespace KswApi.Initialization.Framework
{
	internal class ModuleInitialization
	{
		public void InitializeModules()
		{
			InitializeConfiguration();

			InitializeQueue();

			InitializeModuleRights();

			InitializeModuleProvider();

			InitializeTaskProvider();

			InitializeAuthentication(UserAuthenticationMethod.ActiveDirectory);

			InitializeSmsServiceProvider();

			InitializeRenderingServiceProviders();

			InitializeRoles();

			InitializeApplications();

			InitializeContent();

			InitializeCopyrights();

			InitializeSmsGateway();
		}

		private void InitializeModuleRights()
		{
			RightManager.Clear();
		}

		private void InitializeModuleProvider()
		{
			ModuleProvider.InitializeModules();			
		}

		private void InitializeTaskProvider()
		{
			TaskProvider.InitializeTasks();
		}

		private void InitializeAuthentication(UserAuthenticationMethod method)
		{
			switch (method)
			{
				case UserAuthenticationMethod.ActiveDirectory:
					Dependency.Set<IUserAuthenticator>(new ActiveDirectoryService());

					break;
				default:
					throw new ArgumentOutOfRangeException("method");
			}
		}

		private void InitializeSmsServiceProvider()
		{
			Dependency.Set<ISmsService>(new TwilioService());
			Dependency.Set<ISmsMessageForwarder>(new KswApiSmsClient());
		}

		private void InitializeRenderingServiceProviders()
		{
			Dependency.Set<IRenderingModeFulfillerFactory, RenderingModeFulfillerFactory>();

			Dependency.Set(XmPieServiceHelpers.CreateXmPieService<Account_SSPSoapChannel>("Account"));
			Dependency.Set(XmPieServiceHelpers.CreateXmPieService<Campaign_SSPSoapChannel>("Campaign"));
			Dependency.Set(XmPieServiceHelpers.CreateXmPieService<DataSource_SSPSoapChannel>("DataSource"));
			Dependency.Set(XmPieServiceHelpers.CreateXmPieService<DataSourcePlanUtils_SSPSoapChannel>("DataSourcePlanUtils"));
			Dependency.Set(XmPieServiceHelpers.CreateXmPieService<Document_SSPSoapChannel>("Document"));
			Dependency.Set(XmPieServiceHelpers.CreateXmPieService<Job_SSPSoapChannel>("Job"));
			Dependency.Set(XmPieServiceHelpers.CreateXmPieService<JobTicket_SSPSoapChannel>("JobTicket"));
			Dependency.Set(XmPieServiceHelpers.CreateXmPieService<PlanUtils_SSPSoapChannel>("PlanUtils"));
			Dependency.Set(XmPieServiceHelpers.CreateXmPieService<Production_SSPSoapChannel>("Production"));
		}

		private void InitializeContent()
		{
			BucketLogic bucketLogic = new BucketLogic();
			bucketLogic.EnsureKswOrigin();

			TaxonomyTypeLogic taxonomyLogic = new TaxonomyTypeLogic();
			taxonomyLogic.EnsureTaxonomyTypes();

			HierarchicalTaxonomyLogic hierarchicalTaxonomyLogic = new HierarchicalTaxonomyLogic();
			hierarchicalTaxonomyLogic.EnsureHierarchicalTaxonomies();
		}

		private void InitializeConfiguration()
		{
			ConfigurationValueLogic logic = new ConfigurationValueLogic();
			logic.EnsureConfigurationValues();
		}

		private void InitializeRoles()
		{
			RoleLogic roleLogic = new RoleLogic();
			roleLogic.EnsureRoles();
		}

		private void InitializeApplications()
		{
			ApplicationLogic applicationLogic = new ApplicationLogic();
			applicationLogic.EnsurePortalApplication();
			applicationLogic.EnsureAuxiliaryApplication();
		}

		private void InitializeCopyrights()
		{
			CopyrightLogic copyrightLogic = new CopyrightLogic();
			copyrightLogic.EnsureDefaultCopyright();
		}

		private void InitializeQueue()
		{
			Dependency.Set<ITaskQueue>(new Msmq());
		}

		private void InitializeSmsGateway()
		{
			SmsAdministrationLogic smsLogic = new SmsAdministrationLogic();
			smsLogic.EnsureShortCodes();
		}
	}
}
