﻿using System.Collections.Generic;
using System.Linq;
using KswApi.Common.Configuration;
using KswApi.Common.Enums;
using KswApi.Initialization.Configuration;
using KswApi.Interface.Rendering;
using KswApi.Ioc;
using System;
using System.Configuration;
using System.Web.Configuration;

namespace KswApi.Initialization.Framework
{
	internal class SettingsInitialization
	{
		public void InitializeSettings()
		{
			Settings settings = ConfigurationFromWebXml();

			Dependency.Set(settings);
		}

		private void AddAdditionalSettings(Settings settings, KswApiConfigurationSection xmlConfig)
		{
			settings.Cache.Type = xmlConfig.Cache.Type;
			settings.Cache.Name = xmlConfig.Cache.Name;

			settings.Cache.Prefix = settings.Environment.ToString();

			if (string.IsNullOrEmpty(settings.Database.Name))
				settings.Database.Name = settings.Environment.ToString();
		}

		private Settings ConfigurationFromWebXml()
		{
			KswApiConfigurationSection xmlConfig = ConfigurationManager.GetSection("kswapi") as KswApiConfigurationSection;

			if (xmlConfig == null)
				throw new ConfigurationErrorsException("KswApi configuration section is not found.");

			Settings configuration = new Settings();

			EnvironmentName environment;

			configuration.Environment = !Enum.TryParse(xmlConfig.Environment, out environment)
				? EnvironmentName.Debug
				: environment;

			configuration.Database.Name = xmlConfig.Database.Name;

			configuration.AccessToken.Expiration = new TimeSpan(xmlConfig.AccessToken.Expiration.Hours, xmlConfig.AccessToken.Expiration.Minutes, xmlConfig.AccessToken.Expiration.Seconds);

			configuration.RefreshToken.Expiration = new TimeSpan(xmlConfig.RefreshToken.Expiration.Hours, xmlConfig.RefreshToken.Expiration.Minutes, xmlConfig.RefreshToken.Expiration.Seconds);

			configuration.WebPortal.Timeout = new TimeSpan(xmlConfig.WebPortal.Timeout.Hours, xmlConfig.WebPortal.Timeout.Minutes, xmlConfig.WebPortal.Timeout.Seconds);
			configuration.WebPortal.TimeoutCountdown = new TimeSpan(xmlConfig.WebPortal.TimeoutCountdown.Hours, xmlConfig.WebPortal.TimeoutCountdown.Minutes, xmlConfig.WebPortal.TimeoutCountdown.Seconds);
			configuration.WebPortal.TimeoutPollInterval = new TimeSpan(xmlConfig.WebPortal.TimeoutPollInterval.Hours, xmlConfig.WebPortal.TimeoutPollInterval.Minutes, xmlConfig.WebPortal.TimeoutPollInterval.Seconds);

			// Web Portal Settings
			configuration.WebPortalClientSecret = ConfigurationManager.AppSettings[Constant.AppSettings.WEB_PORTAL_CLIENT_SECRET];
			configuration.WebPortalHomeUri = ConfigurationManager.AppSettings[Constant.AppSettings.WEB_PORTAL_HOME_URI];
			configuration.WebPortalBaseUri = ConfigurationManager.AppSettings[Constant.AppSettings.WEB_PORTAL_BASE_URI];
			configuration.WebPortalClientIps = ConfigurationManager.AppSettings[Constant.AppSettings.WEB_PORTAL_IPS];

			// Auxiliary Settings
			configuration.AuxiliaryClientSecret = ConfigurationManager.AppSettings[Constant.AppSettings.AUXILIARY_CLIENT_SECRET];
			configuration.AuxiliaryClientIps = ConfigurationManager.AppSettings[Constant.AppSettings.AUXILIARY_CLIENT_IPS];

			configuration.ActiveDirectoryDomain = ConfigurationManager.AppSettings[Constant.AppSettings.ACTIVE_DIRECTORY_DOMAIN];
			configuration.ActiveDirectoryUser = ConfigurationManager.AppSettings[Constant.AppSettings.ACTIVE_DIRECTORY_USER];
			configuration.ActiveDirectoryPassword = ConfigurationManager.AppSettings[Constant.AppSettings.ACTIVE_DIRECTORY_PASSWORD];
			configuration.TwilioAccountSid = ConfigurationManager.AppSettings[Constant.AppSettings.TWILIO_ACCOUNT_SID];
			configuration.TwilioAuthToken = ConfigurationManager.AppSettings[Constant.AppSettings.TWILIO_AUTH_TOKEN];
			configuration.TwilioTestAccountSid = ConfigurationManager.AppSettings[Constant.AppSettings.TWILIO_TEST_ACCOUNT_SID];
			configuration.TwilioTestAuthToken = ConfigurationManager.AppSettings[Constant.AppSettings.TWILIO_TEST_AUTH_TOKEN];
			configuration.Index.Uri = ConfigurationManager.AppSettings[Constant.AppSettings.ELASTICSEARCH_URI];
			configuration.Index.UserName = ConfigurationManager.AppSettings[Constant.AppSettings.ELASTICSEARCH_USER];
			configuration.Index.Password = ConfigurationManager.AppSettings[Constant.AppSettings.ELASTICSEARCH_PASSWORD];
			configuration.Index.Name = xmlConfig.Index.Name;
			configuration.Index.Reindex = xmlConfig.Index.Reindex;
			configuration.ServiceUri = xmlConfig.Service.Uri;
			configuration.SmtpServer = ConfigurationManager.AppSettings[Constant.AppSettings.SMTP_SERVER];
			configuration.Cache.Name = xmlConfig.Cache.Name;

			configuration.SmsGateway.StopCommand.Expiration = new TimeSpan(xmlConfig.SmsGateway.StopCommand.Expiration.Hours, xmlConfig.SmsGateway.StopCommand.Expiration.Minutes, xmlConfig.SmsGateway.StopCommand.Expiration.Seconds);

			ConnectionStringSettings connectionString = ConfigurationManager.ConnectionStrings[Constant.ConnectionStrings.THREE_M_HDD];
			if (connectionString != null)
				configuration.ThreeMHddConnectionString = connectionString.ConnectionString;

			CustomErrorsSection customErrorsSection = (CustomErrorsSection)ConfigurationManager.GetSection("system.web/customErrors");
			configuration.ShowFullExceptions = customErrorsSection.Mode == CustomErrorsMode.Off;

			GetTaskSetting(configuration, xmlConfig);

			GetEmailSettings(configuration, xmlConfig);

			AddAdditionalSettings(configuration, xmlConfig);

			GetRendererSettings(configuration, xmlConfig);

			return configuration;
		}

		private void GetEmailSettings(Settings settings, KswApiConfigurationSection xmlConfig)
		{
			settings.EmailMessages.SetPassword = new EmailMessageConfiguration
												 {
													 From = xmlConfig.EmailMessages.SetPassword.From,
													 Subject = xmlConfig.EmailMessages.SetPassword.Subject,
													 Body = xmlConfig.EmailMessages.SetPassword.Body
												 };

			settings.EmailMessages.ResetPassword = new EmailMessageConfiguration
												   {
													   From = xmlConfig.EmailMessages.ResetPassword.From,
													   Subject = xmlConfig.EmailMessages.ResetPassword.Subject,
													   Body = xmlConfig.EmailMessages.ResetPassword.Body
												   };
		}

		private void GetTaskSetting(Settings settings, KswApiConfigurationSection xmlConfig)
		{
			settings.Tasks = new List<TaskConfiguration>();

			foreach (TaskConfigurationElement task in xmlConfig.Tasks)
			{
				TaskConfiguration configuration = new TaskConfiguration
				{
					Type = task.Type,
					Active = task.Active,
					Duration = TimeSpanFromExpiration(task.Duration)
				};

				if (task.DailyOccurrence != null)
				{
					configuration.DailyOccurrence = new DailyTaskConfiguration
					                                {
						                                Time = task.DailyOccurrence.Time,
					                                };
				}
				else if (task.WeeklyOccurrence != null)
				{
					if (string.IsNullOrEmpty(task.WeeklyOccurrence.Days))
						continue;

					configuration.WeeklyOccurrence = new WeeklyTaskConfiguration
														 {
															 Time = task.WeeklyOccurrence.Time,
															 Days =
																 task.WeeklyOccurrence.Days.Split(',').Select(
																	 item => (DayOfWeek)Enum.Parse(typeof(DayOfWeek), item.Trim())).ToList()
														 };
				}
				
				settings.Tasks.Add(configuration);
			}
		}

		private TimeSpan TimeSpanFromExpiration(ExpirationConfigurationElement expiration)
		{
			if (expiration == null)
				return default(TimeSpan);

			return new TimeSpan(expiration.Hours, expiration.Minutes, expiration.Seconds);
		}

		private void GetRendererSettings(Settings settings, KswApiConfigurationSection xmlConfig)
		{
			settings.Renderers = new List<RenderingConfiguration>();

			foreach (RenderingConfigurationElement rendering in xmlConfig.Renderers)
			{
				IDictionary<string, string> options = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
				foreach (string pairs in rendering.Options.Split(new[] {';'}))
				{
					string[] pair = pairs.Split(new[] {'='});
					options.Add(pair[0].Trim(), pair[1].Trim());
				}

				RenderingConfiguration configuration = new RenderingConfiguration
				{
					Mode = rendering.Mode,
					Type = Type.GetType(rendering.Type),
					Options = options
				};

				if (configuration.Type == null || !typeof(IRenderingModeFulfiller).IsAssignableFrom(configuration.Type))
					throw new ConfigurationErrorsException(string.Format(
						"Rendering configuration type '{0}' could not be resolved or does not implement {1}.",
						rendering.Type, typeof(IRenderingModeFulfiller).Name));

				settings.Renderers.Add(configuration);
			}
		}
	}
}
