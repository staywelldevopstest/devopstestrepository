﻿using System;
using System.Reflection;
using KswApi.Interface.Attributes;
using KswApi.Logic.Framework;
using KswApi.Service.Framework;
using KswApi.Services;

namespace KswApi.Initialization.Framework
{
	internal class ServiceInitialization
	{
		public void StartServices()
		{
			// this will start all services in the KswApi.Services library.

			ServiceHandler handler = new ServiceHandler();

			// get the service assembly, using the monitoring service
			Assembly assembly = Assembly.GetAssembly(typeof (MonitoringService));

			Type[] types = assembly.GetTypes();

			foreach (Type type in types)
			{
				AddServiceRights(type);

				handler.StartService(type);
			}
		}

		private void AddServiceRights(Type serviceType)
		{
			// get the rights
			Type[] interfaces = serviceType.GetInterfaces();

			foreach (Type type in interfaces)
			{
				MethodInfo[] methods = type.GetMethods();
				foreach (MethodInfo method in methods)
				{
					AllowAttribute[] attributes = (AllowAttribute[])Attribute.GetCustomAttributes(method, typeof(AllowAttribute), true);

					foreach (AllowAttribute attribute in attributes)
					{
						if (!string.IsNullOrEmpty(attribute.Rights))
							RightManager.AddRight(attribute.Rights);
					}
				}
			}
		}
	}
}
