﻿using System.Configuration;

namespace KswApi.Initialization.Configuration
{
	public class StopCommandConfigurationElement : ConfigurationElement
	{
		[ConfigurationProperty("expiration")]
		public ExpirationConfigurationElement Expiration
		{
			get
			{
				return this["expiration"] as ExpirationConfigurationElement;
			}
		}
	}
}