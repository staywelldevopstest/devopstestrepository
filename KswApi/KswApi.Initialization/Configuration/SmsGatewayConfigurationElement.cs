﻿using System.Configuration;

namespace KswApi.Initialization.Configuration
{
	public class SmsGatewayConfigurationElement : ConfigurationElement
	{
		[ConfigurationProperty("stopCommand")]
		public StopCommandConfigurationElement StopCommand
		{
			get
			{
				return this["stopCommand"] as StopCommandConfigurationElement;
			}
		}
	}
}