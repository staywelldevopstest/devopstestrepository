﻿using System.Configuration;

namespace KswApi.Initialization.Configuration
{
	public class WebPortalConfigurationElement : ConfigurationElement
	{
		[ConfigurationProperty("timeout")]
		public ExpirationConfigurationElement Timeout
		{
			get
			{
				return this["timeout"] as ExpirationConfigurationElement;
			}
		}

		[ConfigurationProperty("timeoutCountdown")]
		public ExpirationConfigurationElement TimeoutCountdown
		{
			get
			{
				return this["timeoutCountdown"] as ExpirationConfigurationElement;
			}
		}

		[ConfigurationProperty("timeoutPollInterval")]
		public ExpirationConfigurationElement TimeoutPollInterval
		{
			get
			{
				return this["timeoutPollInterval"] as ExpirationConfigurationElement;
			}
		}
	}
}
