﻿using System;
using System.Configuration;

namespace KswApi.Initialization.Configuration
{
	public class DailyTaskConfigurationElement : ConfigurationElement
	{
		[ConfigurationProperty("time", IsRequired = true)]
		public DateTime Time
		{
			get { return DateTime.Parse(this["time"].ToString()); }
		}
	}
}
