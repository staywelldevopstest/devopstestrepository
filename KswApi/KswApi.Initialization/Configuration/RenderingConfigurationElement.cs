using System.Configuration;

namespace KswApi.Initialization.Configuration
{
	public class RenderingConfigurationElement : ConfigurationElement
	{
		[ConfigurationProperty("mode", IsRequired = true)]
		public string Mode { get { return (string)this["mode"]; } }

		[ConfigurationProperty("type", IsRequired = true)]
		public string Type { get { return (string)this["type"]; } }

		[ConfigurationProperty("options", IsRequired = false)]
		public string Options { get { return (string)this["options"]; } }
	}
}