﻿using System.Configuration;
using System.Xml;

namespace KswApi.Initialization.Configuration
{
	public class EmailMessageConfigurationElement : ConfigurationElement
	{
		[ConfigurationProperty("from")]
		public string From { get; private set; }

		[ConfigurationProperty("subject")]
		public string Subject { get; private set; }

		[ConfigurationProperty("body")]
		public string Body { get; private set; }

		protected override void DeserializeElement(XmlReader reader, bool serializeCollectionKey)
		{
			while (reader.Read() && reader.NodeType != XmlNodeType.EndElement)
			{
				if (reader.NodeType == XmlNodeType.Element)
				{
					switch (reader.LocalName)
					{
						case "from":
							From = reader.ReadElementContentAsString();
							break;
						case "subject":
							Subject = reader.ReadElementContentAsString();
							break;
						case "body":
							Body = reader.ReadElementContentAsString();
							break;
						default:
							reader.Skip();
							break;
					}
				}
			}
		}
	}
}
