﻿using System.Configuration;

namespace KswApi.Initialization.Configuration
{
	public class EmailMessageGroupConfigurationElement : ConfigurationElement
	{
		[ConfigurationProperty("setPassword")]
		public EmailMessageConfigurationElement SetPassword { get { return this["setPassword"] as EmailMessageConfigurationElement; } }

		[ConfigurationProperty("resetPassword")]
		public EmailMessageConfigurationElement ResetPassword { get { return this["resetPassword"] as EmailMessageConfigurationElement; } }
	}
}
