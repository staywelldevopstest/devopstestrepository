﻿using System.Configuration;

namespace KswApi.Initialization.Configuration
{
	public class ExpirationConfigurationElement : ConfigurationElement
	{
		[ConfigurationProperty("days", IsRequired = false)]
		[IntegerValidator(MinValue = 0)]
		public int Days
		{
			get
			{
				return (int)this["days"];
			}
		}

		[ConfigurationProperty("hours", IsRequired = false)]
		[IntegerValidator(MinValue = 0)]
		public int Hours
		{
			get
			{
				return (int) this["hours"];
			}
		}

		[ConfigurationProperty("minutes", DefaultValue = 0, IsRequired = false)]
		[IntegerValidator(MinValue = 0)]
		public int Minutes
		{
			get
			{
				return (int)this["minutes"];
			}
		}

		[ConfigurationProperty("seconds", DefaultValue = 0, IsRequired = false)]
		[IntegerValidator(MinValue = 0)]
		public int Seconds
		{
			get
			{
				return (int)this["seconds"];
			}
		}

	}
}