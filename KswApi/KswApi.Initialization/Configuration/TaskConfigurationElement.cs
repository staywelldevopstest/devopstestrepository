﻿using System.Configuration;
using KswApi.Interface.Enums;

namespace KswApi.Initialization.Configuration
{
	public class TaskConfigurationElement : ConfigurationElement
	{
		[ConfigurationProperty("type", IsRequired = true)]
		public TaskType Type { get { return (TaskType) this["type"]; } }

		[ConfigurationProperty("active", DefaultValue = true)]
		public bool Active { get { return (bool) this["active"]; } }

		[ConfigurationProperty("weeklyOccurrence", IsRequired = false)]
		public WeeklyTaskConfigurationElement WeeklyOccurrence { get { return this["weeklyOccurrence"] as WeeklyTaskConfigurationElement; } }

		[ConfigurationProperty("dailyOccurrence", IsRequired = false)]
		public DailyTaskConfigurationElement DailyOccurrence { get { return this["dailyOccurrence"] as DailyTaskConfigurationElement; } }

		[ConfigurationProperty("duration")]
		public ExpirationConfigurationElement Duration
		{
			get
			{
				return this["duration"] as ExpirationConfigurationElement;
			}
		}
	}
}
