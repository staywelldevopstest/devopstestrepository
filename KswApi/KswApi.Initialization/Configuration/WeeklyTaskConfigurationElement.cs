﻿using System;
using System.Configuration;

namespace KswApi.Initialization.Configuration
{
	public class WeeklyTaskConfigurationElement : ConfigurationElement
	{
		[ConfigurationProperty("time", IsRequired = true)]
		public DateTime Time
		{
			get {  return DateTime.Parse(this["time"].ToString()); }
		}

		[ConfigurationProperty("day", IsRequired = true)]
		public string Days { get { return this["day"].ToString(); } }
	}
}
