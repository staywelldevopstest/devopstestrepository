﻿using System.Configuration;

namespace KswApi.Initialization.Configuration
{
	public class KswApiConfigurationSection : ConfigurationSection
	{
		[ConfigurationProperty("environment", DefaultValue = "Environment", IsRequired = true)]
		[StringValidator(MinLength = 2)]
		public string Environment
		{
			get { return this["environment"] as string; }
		}

		[ConfigurationProperty("emailMessages")]
		public EmailMessageGroupConfigurationElement EmailMessages { get { return this["emailMessages"] as EmailMessageGroupConfigurationElement; } }

		[ConfigurationProperty("accessToken")]
		public AccessTokenConfigurationElement AccessToken
		{
			get { return this["accessToken"] as AccessTokenConfigurationElement; }
		}

		[ConfigurationProperty("refreshToken")]
		public RefreshTokenConfigurationElement RefreshToken
		{
			get { return this["refreshToken"] as RefreshTokenConfigurationElement; }
		}

		[ConfigurationProperty("webPortal")]
		public WebPortalConfigurationElement WebPortal
		{
			get { return this["webPortal"] as WebPortalConfigurationElement; }
		}

		[ConfigurationProperty("cache", IsRequired = true)]
		public CacheConfigurationElement Cache
		{
			get { return this["cache"] as CacheConfigurationElement; }
		}

		[ConfigurationProperty("smsGateway")]
		public SmsGatewayConfigurationElement SmsGateway
		{
			get { return this["smsGateway"] as SmsGatewayConfigurationElement; }
		}

		[ConfigurationProperty("tasks", IsRequired = false)]
		public TaskConfigurationCollection Tasks
		{
			get { return this["tasks"] as TaskConfigurationCollection; }
		}

		[ConfigurationProperty("database", IsRequired = false)]
		public DatabaseConfigurationElement Database
		{
			get { return this["database"] as DatabaseConfigurationElement; }
		}

		[ConfigurationProperty("index", IsRequired = true)]
		public IndexConfigurationElement Index
		{
			get { return this["index"] as IndexConfigurationElement; }
		}

		[ConfigurationProperty("service", IsRequired = true)]
		public ServiceConfigurationElement Service
		{
			get { return this["service"] as ServiceConfigurationElement; }
		}

		[ConfigurationProperty("renderers", IsRequired = false)]
		public RenderingConfigurationCollection Renderers
		{
			get { return this["renderers"] as RenderingConfigurationCollection; }
		}
	}
}