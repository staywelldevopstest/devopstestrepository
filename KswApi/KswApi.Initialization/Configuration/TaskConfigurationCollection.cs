﻿using System.Configuration;

namespace KswApi.Initialization.Configuration
{
	[ConfigurationCollection(typeof(TaskConfigurationElement), CollectionType = ConfigurationElementCollectionType.BasicMapAlternate)]
	public class TaskConfigurationCollection : ConfigurationElementCollection
	{
		protected override string ElementName
		{
			get { return "task"; }
		}

		public override ConfigurationElementCollectionType CollectionType
		{
			get
			{
				return ConfigurationElementCollectionType.BasicMapAlternate;
			}
		}

		protected override bool IsElementName(string elementName)
		{
			return elementName == ElementName;
		}

		protected override ConfigurationElement CreateNewElement()
		{
			return new TaskConfigurationElement();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((TaskConfigurationElement) element).Type;
		}
	}
}
