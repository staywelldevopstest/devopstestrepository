﻿using System.Configuration;

namespace KswApi.Initialization.Configuration
{
	[ConfigurationCollection(typeof(RenderingConfigurationElement), CollectionType = ConfigurationElementCollectionType.BasicMapAlternate)]
	public class RenderingConfigurationCollection : ConfigurationElementCollection
	{
		protected override string ElementName
		{
			get { return "renderer"; }
		}

		public override ConfigurationElementCollectionType CollectionType
		{
			get { return ConfigurationElementCollectionType.BasicMapAlternate; }
		}

		protected override bool IsElementName(string elementName)
		{
			return elementName == ElementName;
		}

		protected override ConfigurationElement CreateNewElement()
		{
			return new RenderingConfigurationElement();
		}

		protected override object GetElementKey(ConfigurationElement element)
		{
			return ((RenderingConfigurationElement)element).Type;
		}
	}
}
