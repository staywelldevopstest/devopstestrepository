﻿using System.Configuration;

namespace KswApi.Initialization.Configuration
{
	public class DatabaseConfigurationElement : ConfigurationElement
	{
		[ConfigurationProperty("name")]
		public string Name
		{
			get
			{
				return (string) this["name"];
			}
		}
	}
}
