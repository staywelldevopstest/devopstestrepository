﻿using System.Configuration;

namespace KswApi.Initialization.Configuration
{
	public class ServiceConfigurationElement : ConfigurationElement
	{
		[ConfigurationProperty("uri", IsRequired = true)]
		public string Uri
		{
			get { return this["uri"] as string; }
		}

	}
}
