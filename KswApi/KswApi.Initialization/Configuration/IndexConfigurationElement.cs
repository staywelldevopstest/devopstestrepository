﻿using System.Configuration;

namespace KswApi.Initialization.Configuration
{
	public class IndexConfigurationElement : ConfigurationElement
	{
		[ConfigurationProperty("name", IsRequired = true)]
		public string Name
		{
			get
			{
				return (string) this["name"];
			}
		}

		[ConfigurationProperty("reindex", IsRequired = false, DefaultValue = "")]
		public string Reindex
		{
			get { return (string) this["reindex"]; }
		}
	}
}
