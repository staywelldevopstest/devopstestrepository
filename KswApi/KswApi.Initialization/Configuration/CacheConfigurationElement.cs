﻿using System.Configuration;
using KswApi.Common.Configuration;

namespace KswApi.Initialization.Configuration
{
	public class CacheConfigurationElement : ConfigurationElement
	{
		[ConfigurationProperty("type", IsRequired = true)]
		public CacheType Type { get { return (CacheType) this["type"]; } }

		[ConfigurationProperty("name")]
		public string Name { get { return this["name"] as string; } }
	}
}