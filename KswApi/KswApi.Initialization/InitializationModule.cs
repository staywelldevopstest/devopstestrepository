﻿using System.Net;
using KswApi.Initialization.Framework;
using KswApi.Logging;
using System;
using System.Threading;
using System.Web;
using KswApi.Logging.Objects;
using KswApi.Service.Framework;
using ModuleInitialization = KswApi.Initialization.Framework.ModuleInitialization;

namespace KswApi.Initialization
{
	public class InitializationModule : IHttpModule
	{
		private static readonly AutoResetEvent InitializeEvent = new AutoResetEvent(true);

		public void Dispose()
		{

		}

		public void Init(HttpApplication context)
		{
			try
			{
				// this takes care of multiple module creation that can happen due to thread pooling
				if (!InitializeEvent.WaitOne(0, false))
					return;

				InitializeErrorHandling(context);

				InitializeApplication();

				CheckDependencies();
			}
			catch (Exception exception)
			{
				// try to log the exception
				Logger.Log(exception);

				throw;
			}
		}

		private void InitializeApplication()
		{
			SettingsInitialization settingsInitialization = new SettingsInitialization();
			settingsInitialization.InitializeSettings();

			LogInitialization logInitialization = new LogInitialization();
			logInitialization.IntializeLog();

			DatabaseInitialization databaseInitialization = new DatabaseInitialization();
			databaseInitialization.InitializeDatabase();

			IndexInitialization indexInitialization = new IndexInitialization();
			indexInitialization.InitializeIndex();

			ModuleInitialization moduleInitialization = new ModuleInitialization();
			moduleInitialization.InitializeModules();

			ServiceInitialization serviceInitialization = new ServiceInitialization();
			serviceInitialization.StartServices();

			Logger.Log(Severity.Information, "Service Started", "The service completed startup and initialization.");
		}

		private void InitializeErrorHandling(HttpApplication context)
		{
			context.Error += OnError; 
		}

		private void OnError(object sender, EventArgs eventArgs)
		{
			HttpContext context = HttpContext.Current;
			if (context == null)
				return;
			Exception exception = context.Server.GetLastError();
			if (exception == null)
				return;

			ErrorHandler handler = new ErrorHandler();

			Operation operation = new Operation
								  {
									  Request = context.Request,
									  Response = context.Response,
									  OperationLog = new OperationLog()
								  };

			handler.HandleException(exception, operation);
		}

		private void CheckDependencies()
		{
			DependencyValidation dependencyValidation = new DependencyValidation();

			dependencyValidation.ValidateDependencies();
		}
	}
}
