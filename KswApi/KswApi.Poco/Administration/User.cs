﻿using System;
using KswApi.Interface.Enums;

namespace KswApi.Poco.Administration
{
    public class User
    {
        public Guid Id { get; set; }
        public UserType Type { get; set; }
    }
}
