﻿using System;
using KswApi.Interface.Enums;

namespace KswApi.Repositories.Objects
{
	public class PasswordToken
	{
		public Guid Id { get; set; }
		public string Token { get; set; }
		public DateTime DateAdded { get; set; }
		public UserType UserType { get; set; }
		public Guid UserId { get; set; }
	}
}
