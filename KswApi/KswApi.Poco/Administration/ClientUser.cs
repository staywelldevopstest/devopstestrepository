﻿using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Poco.Enums;
using System;
using System.Collections.Generic;

namespace KswApi.Repositories.Objects
{
    public class ClientUser
    {
        public Guid Id { get; set; }
        public Guid ClientId { get; set; }
        public string EmailAddress { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
		public string Notes { get; set; }
        public ClientUserState State { get; set; }
        public ClientUserPermissionType PermissionType { get; set; }
        public List<ClientUserLicense> Licenses { get; set; }
        public DateTime Created { get; set; }
		public byte[] PasswordSalt { get; set; }
        public byte[] PasswordHash { get; set; }
		public DateTime? LockedTime { get; set; }
		public DateTime? LastLogin { get; set; }
		public List<DateTime> RecentFailedLogins { get; set; }
		
		[Obsolete("This property is deprecated.  Please use State", false)]
        public bool? Disabled { get; set; }

        [Obsolete("This property is deprecated and not to be used", true)]
        public string ClientName { get; set; }
    }
}
