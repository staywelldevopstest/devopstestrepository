﻿using System;
using KswApi.Interface.Enums;
using KswApi.Repositories.Enums;

namespace KswApi.Repositories.Objects
{
	public class LoginAttempt
	{
		public Guid UserId { get; set; }
		public UserType UserType { get; set; }
		public DateTime Time { get; set; }
		public LoginResult Result { get; set; }
	}
}
