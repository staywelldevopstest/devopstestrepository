﻿using System;
using KswApi.Interface.Objects;

namespace KswApi.Repositories.Objects
{
	[Serializable]
	public class ClientAddress
	{
		public Guid Id { get; set; }
		public virtual Application Client { get; set; }
		public string Address { get; set; }
	}
}
