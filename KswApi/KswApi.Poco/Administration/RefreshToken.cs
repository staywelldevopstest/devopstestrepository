﻿using System;
using KswApi.Common.Objects;

namespace KswApi.Repositories.Objects
{
	public class RefreshToken : AccessToken
	{
		public AccessToken ToAccessToken(string newAccessToken, DateTime expiration)
		{
			return new AccessToken
					   {
						   Id = Guid.NewGuid(),
						   ApplicationId = ApplicationId,
						   Rights = Rights,
						   TokenType = TokenType,
						   Expiration = expiration,
						   Token = newAccessToken,
						   LicenseId = LicenseId,
						   Scope = Scope,
						   UserDetails = UserDetails
					   };
		}

		public RefreshToken ToRefreshToken(string newRefreshToken, DateTime expiration)
		{
			return new RefreshToken
				       {
						   Id = Guid.NewGuid(),
					       ApplicationId = ApplicationId,
					       Rights = Rights,
					       TokenType = TokenType,
					       Expiration = expiration,
					       Token = newRefreshToken,
						   LicenseId = LicenseId,
						   Scope = Scope,
						   UserDetails = UserDetails
				       };
		}

		public static RefreshToken FromAccessToken(AccessToken accessToken, string refreshToken, DateTime expiration)
		{
			return new RefreshToken
			{
				Id = Guid.NewGuid(),
				ApplicationId = accessToken.ApplicationId,
				Rights = accessToken.Rights,
				TokenType = accessToken.TokenType,
				Expiration = expiration,
				Token = refreshToken,
				LicenseId = accessToken.LicenseId,
				Scope = accessToken.Scope,
				UserDetails = accessToken.UserDetails
			};
		}

	}
}

