﻿using System;
using KswApi.Common.Objects;

namespace KswApi.Poco.Administration
{
	[Serializable]
	public class AuthorizationGrant
	{
		public Guid Id { get; set; }
		public string Code { get; set; }
		public DateTime Expiration { get; set; }
		public string State { get; set; }
		public string RedirectUri { get; set; }
		public UserAuthenticationDetails UserDetails { get; set; }
	}
}
