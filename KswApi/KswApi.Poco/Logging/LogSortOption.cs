﻿namespace KswApi.Poco.Logging
{
	public enum LogSortOption
	{
		DateDescending,
		DateAscending
	}
}
