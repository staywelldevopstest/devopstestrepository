﻿namespace KswApi.Poco.ThreeM
{
	public class EncodedTaxonomyValue
	{
		public long Id { get; set; }
		public string Value { get; set; }
		public string Description { get; set; }
		public long RelationshipType { get; set; }
		public long Type { get; set; }
	}
}
