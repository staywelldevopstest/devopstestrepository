﻿using System;
using KswApi.Poco.Tasks;

namespace KswApi.Poco.ThreeM
{
	public class HddVersion
	{
		public long Version { get; set; }
		public DateTime Date { get; set; }
		public TaskState State { get; set; }
	}
}
