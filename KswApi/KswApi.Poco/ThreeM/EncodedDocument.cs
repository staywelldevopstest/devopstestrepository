﻿namespace KswApi.Poco.ThreeM
{
	public class EncodedDocument
	{
		public long Id { get; set; }
		public string DocumentId { get; set; }
		public long ConceptId { get; set; }
	}
}
