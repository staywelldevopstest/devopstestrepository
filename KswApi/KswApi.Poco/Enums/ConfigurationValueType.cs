﻿
namespace KswApi.Poco.Enums
{
    public enum ConfigurationValueType
    {
        None = 0,
        HierarchicalTaxonomyReferenceFile = 1
    }
}
