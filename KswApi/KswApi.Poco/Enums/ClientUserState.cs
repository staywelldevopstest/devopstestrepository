﻿
namespace KswApi.Poco.Enums
{
    public enum ClientUserState
    {
        None,
        Active,
        Disabled,
        Deleted
    }
}
