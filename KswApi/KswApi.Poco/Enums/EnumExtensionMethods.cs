﻿using System;
using System.ComponentModel;
using System.Reflection;

namespace KswApi.Poco.Enums
{
	public static class EnumExtensionMethods
	{
		public static string GetDescription(this Enum value)
		{
			Type type = value.GetType();
			string name = Enum.GetName(type, value);
			if (name == null) return null;
			FieldInfo field = type.GetField(name);
			if (field == null) return null;
			DescriptionAttribute attr = Attribute.GetCustomAttribute(field,
				typeof(DescriptionAttribute)) as DescriptionAttribute;
			return attr != null ? attr.Description : null;
		}		
	}
}