﻿using KswApi.Poco.Enums;

namespace KswApi.Poco
{
	public class ConfigurationValue<TData>
	{
		public string Id { get; set; }
        //public ConfigurationValueType Type { get; set; }
		public TData Data { get; set; }
	}
}
