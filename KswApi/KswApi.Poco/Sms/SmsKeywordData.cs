﻿using System;
using KswApi.Interface.Enums;

namespace KswApi.Poco.Sms
{
	public class SmsKeywordData
	{
		public Guid Id { get; set; }
		public Guid LicenseId { get; set; }
		public Guid NumberId { get; set; }
		public string Number { get; set; }
		public string Keyword { get; set; }
		public string Website { get; set; }
		public MessageFormat Format { get; set; }
	}
}
