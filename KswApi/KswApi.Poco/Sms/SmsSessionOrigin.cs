﻿namespace KswApi.Poco.Sms
{
	public enum SmsSessionOrigin
	{
		Unknown = 0,
		Subscriber = 1,
		Client = 2
	}
}
