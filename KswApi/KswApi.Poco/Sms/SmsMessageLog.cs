﻿using System;
using KswApi.Repositories.Enums;

namespace KswApi.Poco.Sms
{
    public class SmsMessageLog
    {
        public Guid Id { get; set; }
        public Guid LogId { get; set; }
        public Guid LicenseId { get; set; }
        public string ServiceNumber { get; set; }
        public string SubscriberNumber { get; set; }
        public string Keyword { get; set; }
        public MessageType Type { get; set; }
        public DateTime DateOfMessage { get; set; }
        public string Body { get; set; }
    }
}
