﻿using System;
using System.Collections.Generic;
using KswApi.Interface.Enums;

namespace KswApi.Poco.Sms
{
	public class SmsNumberData
	{
		public Guid Id { get; set; }
		public Guid LicenseId { get; set; }
		public SmsNumberType Type { get; set; }
		public SmsNumberRouteType RouteType { get; set; }
		public string Number { get; set; }
		public MessageFormat Format { get; set; }
		public string Website { get; set; }
		public List<SmsNumberKeywordItem> Keywords { get; set; }
	}
}
