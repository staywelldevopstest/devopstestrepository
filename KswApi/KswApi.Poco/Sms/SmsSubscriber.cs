﻿using System;
using System.Collections.Generic;
using KswApi.Repositories.Enums;

namespace KswApi.Poco.Sms
{
	public class SmsSubscriber
	{
		public Guid Id { get; set; }
		public string SubscriberNumber { get; set; }
		public string ServiceNumber { get; set; }
		public List<SmsSubscriberSessionItem> Sessions { get; set; }
		public SmsSubscriberState State { get; set; }
		public DateTime? StateExpiration { get; set; }
	}
}
