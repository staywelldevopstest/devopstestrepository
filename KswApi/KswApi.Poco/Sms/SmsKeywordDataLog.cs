﻿using System;

namespace KswApi.Poco.Sms
{
    public class SmsKeywordDataLog
    {
        public Guid Id { get; set; }
        public Guid KeywordId { get; set; }
        public string Keyword { get; set; }
        public Guid NumberId { get; set; }
        public string Number { get; set; }
        public Guid LicenseId { get; set; }
        public DateTime CreateDate { get; set; }
        public DateTime? DeletedDate { get; set; }
        public DateTime? UpdatedDate { get; set; }

        public SmsKeywordDataLog()
        {
        }

        /// <summary>
        /// This creates a new object from the KeywordData Object.  It also sets the create date to now.  Updated and Deleted are set to null
        /// </summary>
        /// <param name="keywordData"></param>
        public SmsKeywordDataLog(SmsKeywordData keywordData)
        {
            KeywordId = keywordData.Id;
            Keyword = keywordData.Keyword;
            LicenseId = keywordData.LicenseId;
            NumberId = keywordData.NumberId;
            Number = keywordData.Number;
			CreateDate = DateTime.UtcNow;
            UpdatedDate = null;
            DeletedDate = null;
        }
    }
}
