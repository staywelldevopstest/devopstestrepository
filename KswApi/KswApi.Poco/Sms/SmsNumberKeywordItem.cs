﻿using System;

namespace KswApi.Poco.Sms
{
	public class SmsNumberKeywordItem
	{
		public string Keyword { get; set; }
		public Guid KeywordId { get; set; }
	}
}
