﻿using System;

namespace KswApi.Poco.Sms
{
	public class SmsSubscriberSessionItem
	{
		public string Keyword { get; set; }
		public Guid SessionId { get; set; }
	}
}
