﻿namespace KswApi.Poco.Sms
{
	public enum SmsSubscriberState
	{
		Normal,
		AwaitingStopResponse
	}
}
