﻿namespace KswApi.Poco.Sms
{
	public enum SmsSessionState
	{
		Active,
		Inactive,
		Stopped,
		Suspended,
		Deleted
	}
}
