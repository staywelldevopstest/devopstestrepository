﻿using System;

namespace KswApi.Poco.Sms
{
    public class SmsNumberDataLog
    {
        public Guid Id { get; set; }
        public Guid LicenseId { get; set; }
        public Guid NumberId { get; set; }
        public string Number { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime? DeletedDate { get; set; }

        public SmsNumberDataLog()
        {

        }

        public SmsNumberDataLog(SmsNumberData numberData)
        {
            LicenseId = numberData.LicenseId;
            NumberId = numberData.Id;
            Number = numberData.Number;
			CreatedDate = DateTime.UtcNow;
            DeletedDate = null;
        }
    }
}
