﻿using KswApi.Repositories.Enums;
using System;

namespace KswApi.Poco.Sms
{
    public class SmsSession
    {
        public Guid Id { get; set; }
        public string Keyword { get; set; }
        public string SubscriberNumber { get; set; }
        public string ServiceNumber { get; set; }
        public Guid LicenseId { get; set; }
        public SmsSessionState State { get; set; }
        public DateTime DateAdded { get; set; }
        public DateTime LastRequestDate { get; set; }
        public SmsSessionOrigin Origin { get; set; }
    }
}
