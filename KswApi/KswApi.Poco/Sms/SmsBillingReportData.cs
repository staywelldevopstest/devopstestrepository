﻿using KswApi.Interface.Enums;
using System;
using System.Collections.Generic;

namespace KswApi.Poco.Sms
{
    public class SmsBillingReportData
    {
        public List<SmsBillingReportClientData> Clients { get; set; }
    }

    public class SmsBillingReportClientData
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public ClientStatus Status { get; set; }
        public bool IncludeInReport { get; set; }
        public List<SmsBillingReportLicenseData> Licenses { get; set; }
    }

    public class SmsBillingReportLicenseData
    {
        public Guid Id { get; set; }
        public int Year { get; set; }
        public Month Month { get; set; }
        public string LicenseName { get; set; }
        public Guid LicenseId { get; set; }
        public int ShortCodeKeywordCount { get; set; }
        public int LongCodeCount { get; set; }
        public int IncomingMessageCount { get; set; }
        public int OutgoingMessageCount { get; set; }
    }
}
