﻿using System;
using KswApi.Poco.Tasks;

namespace KswApi.Poco
{
	public class Task
	{
		public Guid Id { get; set; }
		public DateTime Started { get; set; }
		public DateTime LastUpdate { get; set; }
		public DateTime? Finished { get; set; }
		public TaskState Status { get; set; }
	}
}
