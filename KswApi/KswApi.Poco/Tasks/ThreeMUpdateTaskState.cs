﻿namespace KswApi.Poco.Tasks
{
	public enum ThreeMUpdateTaskState
	{
		None,
		Start,
		AddDocuments,
		UpdateDocuments,
		Finished
	}
}
