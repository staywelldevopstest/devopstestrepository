﻿using System;

namespace KswApi.Poco.Tasks
{
	public class ExecutingTask<TData>
	{
		public string Id { get; set; }
		public Guid ConcurrencyToken { get; set; }
		public DateTime Started { get; set; }
		public DateTime? Finished { get; set; }
		public TaskState State { get; set; }
		public TData Data { get; set; }
	}
}
