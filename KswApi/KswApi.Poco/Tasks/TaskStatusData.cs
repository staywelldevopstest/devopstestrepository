﻿using KswApi.Interface.Enums;
using System;

namespace KswApi.Poco.Tasks
{
    public class TaskStatusData
    {
        public Guid Id { get; set; }
        public TaskType TaskType { get; set; }
        public string TaskName { get; set; }
        public DateTime StatusDate { get; set; }
        public string StatusUpdateText { get; set; }
    }
}
