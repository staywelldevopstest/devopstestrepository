﻿namespace KswApi.Poco.Tasks
{
	public class ThreeMUpdateTask
	{
		public ThreeMUpdateTaskState State { get; set; }
		public long PreviousItem { get; set; }
	}
}
