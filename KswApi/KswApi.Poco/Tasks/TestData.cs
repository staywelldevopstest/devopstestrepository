﻿using System;

namespace KswApi.Poco.Tasks
{
	[Serializable]
	public class TestData
	{
		public Guid Id { get; set; }
		public string Key { get; set; }
		public string Data { get; set; }
	}
}
