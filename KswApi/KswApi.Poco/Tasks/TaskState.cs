﻿namespace KswApi.Poco.Tasks
{
	public enum TaskState
	{
		None,
		Scheduled,
		Waiting,
		Active,
		Finished
	}
}
