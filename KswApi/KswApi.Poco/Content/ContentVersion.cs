﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;
using KswApi.Interface.Enums;

namespace KswApi.Poco.Content
{
	public class ContentVersion
	{
		public Guid Id { get; set; }

		public Guid CoreId { get; set; }

		public bool IsMaster { get; set; }
		
		public Guid BucketId { get; set; }
		
		public List<ContentSegmentItem> Segments { get; set; }
		
		public DateTime DateAdded { get; set; }
		
		public DateTime DateModified { get; set; }

		public DateTime? DatePublished { get; set; }

		public DateTime DateUpdated { get; set; }
		
		public ObjectStatus Status { get; set; }

		public string Title { get; set; }

		public string InvertedTitle { get; set; }

		[XmlArrayItem("AlternateTitle")]
		public List<string> AlternateTitles { get; set; }

		public string Slug { get; set; }

		public string Blurb { get; set; }

		public string Language { get; set; }

		public List<ContentPath> Paths { get; set; }

		public string LegacyId { get; set; }

		public bool Published { get; set; }
	}
}
