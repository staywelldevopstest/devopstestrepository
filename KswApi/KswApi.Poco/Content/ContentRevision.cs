﻿using System;

namespace KswApi.Poco.Content
{
	public class ContentRevision
	{
		public Guid Id { get; set; }
		public ContentVersion Version { get; set; }
		public ContentCore Core { get; set; }
	}
}
