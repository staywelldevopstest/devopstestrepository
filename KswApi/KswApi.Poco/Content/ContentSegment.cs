﻿using System;

namespace KswApi.Poco.Content
{
	public class ContentSegment
	{
		public Guid Id { get; set; }
		public Guid ContentId { get; set; }
		public string Body { get; set; }
	}
}
