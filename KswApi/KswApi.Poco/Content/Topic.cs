﻿using System;
using KswApi.Interface.Enums;

namespace KswApi.Poco.Content
{
    public class Topic
    {
        public Guid Id { get; set; }

		public string Slug { get; set; }
		
		public string Title { get; set; }
		public string Description { get; set; }

        public DateTime DateAdded { get; set; }
        public DateTime DateModified { get; set; }
        public ObjectStatus Status { get; set; }

		public string ImageUri { get; set; }

		public int ContentCount { get; set; }
		public int SubtopicCount { get; set; }
		public int DependencyCount { get; set; }
		public TopicType Type { get; set; }
    }
}
