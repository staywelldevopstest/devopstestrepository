﻿using System;
using System.Collections.Generic;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.Content;

namespace KswApi.Poco.Content
{
	public class ContentBucket
	{
		public Guid Id { get; set; }
		public string CaseInsensitiveName { get; set; }
		public ObjectStatus Status { get; set; }
		public List<ContentBucketSegment> Segments { get; set; }
		public DateTime DateAdded { get; set; }

        // previously inherited from  KswApi.Interface.Objects.Content.ContentBucketBase
        public string Slug { get; set; }
        public ContentType Type { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public bool ReadOnly { get; set; }
        public int? LegacyId { get; set; }
        public Guid OriginId { get; set; }
        public ContentBucketConstraint Constraints { get; set; }
        public Guid? CopyrightId { get; set; }
	}
}
