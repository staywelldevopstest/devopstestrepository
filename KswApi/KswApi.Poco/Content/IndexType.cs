﻿namespace KswApi.Poco.Content
{
	public enum IndexType
	{
		None,
		Content,
		Published,
		Image,
        Collection
	}
}
