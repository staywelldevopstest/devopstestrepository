﻿using System;

namespace KswApi.Poco.Content
{
	public class LicensedCollection
	{
		public Guid Id { get; set; }
		public Guid LicenseId { get; set; }
		public Guid CollectionId { get; set; }
	}
}
