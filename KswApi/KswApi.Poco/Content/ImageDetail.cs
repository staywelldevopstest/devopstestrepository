﻿using System;
using System.Collections.Generic;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;

namespace KswApi.Poco.Content
{
	public class ImageDetail
	{
		public Guid Id { get; set; }

		public Guid BucketId { get; set; }
		public string BucketSlug { get; set; }
		public string FileName { get; set; }
		public ImageFormatType Format { get; set; }
		public string Title { get; set; }

		public string InvertedTitle { get; set; }

		public List<string> AlternateTitles { get; set; }
		
		public DateTime? PostingDate { get; set; }

		public string Slug { get; set; }

		public DateTime DateAdded { get; set; }
		public DateTime DateModified { get; set; }

		public List<ContentPath> Paths { get; set; }

		public string Blurb { get; set; }
		public List<string> Tags { get; set; }

		public Dictionary<string, Guid> Versions { get; set; }

		public ObjectStatus Status { get; set; }

		public int Width { get; set; }
		public int Height { get; set; }

		public string LegacyId { get; set; }

		public List<CachedImage> CachedImages { get; set; }
	}
}
