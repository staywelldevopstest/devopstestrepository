﻿namespace KswApi.Poco.Content
{
	public class ImageConstraint
	{
		public int Width { get; set; }
		public int Height { get; set; }
		public string Extension { get; set; }
	}
}
