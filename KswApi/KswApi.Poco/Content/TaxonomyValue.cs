﻿using KswApi.Interface.Enums;

namespace KswApi.Poco.Content
{
    public class TaxonomyValue
    {
        public string Value { get; set; }
        public TaxonomyValueSource Source { get; set; }
    }
}
