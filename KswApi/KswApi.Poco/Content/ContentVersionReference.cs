﻿using System;

namespace KswApi.Poco.Content
{
	public class ContentVersionReference
	{
		public Guid Id { get; set; }
		public string Slug { get; set; }
		public string Language { get; set; }
	}
}
