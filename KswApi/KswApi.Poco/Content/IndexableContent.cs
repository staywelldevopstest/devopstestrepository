﻿using System;
using System.Collections.Generic;
using KswApi.Interface.Objects.Content;

namespace KswApi.Poco.Content
{
	public class IndexableContent
	{
		public ContentVersion Version { get; set; }
		public ContentCore Core { get; set; }
		public List<HierarchicalTaxonomy> ServiceLines { get; set; }
		public List<ContentSegmentRequest> Segments { get; set; }
		public ContentBucket Bucket { get; set; }
		public List<Guid> Collections { get; set; } 
	}
}
