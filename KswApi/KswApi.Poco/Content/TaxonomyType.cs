﻿using System;

namespace KswApi.Poco.Content
{
    public class TaxonomyType
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public string Slug { get; set; }
        public string Regex { get; set; }
    }
}
