﻿using System;
using KswApi.Poco.Administration;

namespace KswApi.Poco.Content
{
    public class Collection
    {
		public Guid Id { get; set; }
        public User CreatedBy { get; set; }
        public DateTime? Expires { get; set; }
        public string LegacyId { get; set; }
		public int LicenseCount { get; set; }
    }
}
