﻿namespace KswApi.Poco.Content
{
    public class ContentPath 
	{
		public string BucketIdOrSlug { get; set; }
		public string ContentIdOrSlug { get; set; }
	}
}
