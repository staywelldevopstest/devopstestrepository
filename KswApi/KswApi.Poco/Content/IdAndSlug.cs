﻿using System;

namespace KswApi.Poco.Content
{
	public class IdAndSlug
	{
		public Guid Id { get; set; }
		public string Slug { get; set; }

		protected bool Equals(IdAndSlug other)
		{
			return Id.Equals(other.Id) && string.Equals(Slug, other.Slug);
		}

		public override bool Equals(object obj)
		{
			if (ReferenceEquals(null, obj)) return false;
			if (ReferenceEquals(this, obj)) return true;
			if (obj.GetType() != this.GetType()) return false;
			return Equals((IdAndSlug) obj);
		}

		public override int GetHashCode()
		{
			unchecked
			{
				return (Id.GetHashCode()*397) ^ (Slug != null ? Slug.GetHashCode() : 0);
			}
		}

		public static bool operator ==(IdAndSlug left, IdAndSlug right)
		{
			return Equals(left, right);
		}

		public static bool operator !=(IdAndSlug left, IdAndSlug right)
		{
			return !Equals(left, right);
		}
	}
}
