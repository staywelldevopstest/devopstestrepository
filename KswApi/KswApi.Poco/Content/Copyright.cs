﻿using System;

namespace KswApi.Poco.Content
{
	public class Copyright
	{
		public Guid Id { get; set; }
		public string Value { get; set; }
		public DateTime DateAdded { get; set; }
		public DateTime DateModified { get; set; }
	}
}
