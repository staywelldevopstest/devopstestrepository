﻿using System;
using KswApi.Interface.Enums;

namespace KswApi.Poco.Content
{
	public class CollectionRevision
	{
		public Guid Id { get; set; }
        public CollectionRevisionType RevisionType { get; set; }
        public DateTime RevisionDate { get; set; }
        public Collection Collection { get; set; }
		public Topic Topic { get; set; }
	}
}
