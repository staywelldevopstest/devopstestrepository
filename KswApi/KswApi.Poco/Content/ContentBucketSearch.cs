﻿using System;
using System.Collections.Generic;
using KswApi.Interface.Enums;

namespace KswApi.Poco.Content
{
	public class ContentBucketSearch
	{
		public List<ContentType> Type { get; set; }
		public Guid? Origin { get; set; } 
		public int? LegacyId { get; set; }
		public bool? ReadOnly { get; set; }
		public string Query { get; set; }
		public Guid? CopyrightId { get; set; }
		public bool DefaultCopyright { get; set; }
	}
}
