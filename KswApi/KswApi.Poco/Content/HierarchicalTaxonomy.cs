﻿using System;
using System.Collections.Generic;
using KswApi.Interface.Enums;

namespace KswApi.Poco.Content
{
    public class HierarchicalTaxonomy
    {
        public Guid Id { get; set; }
        public string Slug { get; set; }
        public string Path { get; set; }
        public string FullPath { get; set; } // FullPath := Type/Path/Slug
        public string Value { get; set; }
        public List<string> PathValues { get; set; }
        public HierarchicalTaxonomyType Type { get; set; }
    }
}
