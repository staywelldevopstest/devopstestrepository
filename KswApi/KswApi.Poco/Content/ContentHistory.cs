﻿using System;
using KswApi.Interface.Objects.Content;

namespace KswApi.Poco.Content
{
	public class ContentHistory
	{
		public Guid Id { get; set; }
		public Guid ContentId { get; set; }
		
		public ContentRevisionType RevisionType { get; set; }
		public Guid RevisionId { get; set; }
		public DateTime Time { get; set; }
	}
}
