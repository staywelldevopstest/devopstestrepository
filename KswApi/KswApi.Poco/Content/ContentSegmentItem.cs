﻿using System;

namespace KswApi.Poco.Content
{
	public class ContentSegmentItem
	{
		public Guid Id { get; set; }
		public Guid? SegmentId { get; set; }
		public string CustomName { get; set; }
	}
}
