﻿using System;
using System.Collections.Generic;
using KswApi.Interface.Objects.Content;

namespace KswApi.Poco.Content
{
	public class CollectionStructure
	{
		public Guid Id { get; set; }
		public Guid CollectionId { get; set; }
		public Guid ItemId { get; set; }
		public Guid? ParentId { get; set; }
		public CollectionItemType Type { get; set; }
		public List<Guid> Path { get; set; }
		public List<Guid> Children { get; set; }
		public bool? Flagged { get; set; }

		public string FlagComment { get; set; }
		public bool? Disabled { get; set; }
	}
}
