﻿using System;
using System.Collections.Generic;
using KswApi.Interface.Objects.Content;

namespace KswApi.Poco.Content
{
	public class ContentCore : ContentBase
	{
		public Guid Id { get; set; }
		public Guid MasterId { get; set; }
		public List<ContentVersionReference> Versions { get; set; }
		public Dictionary<string, List<TaxonomyValue>> Taxonomies { get; set; } //string := Taxonomy Slug

		public List<HierarchicalTaxonomyRequest> Servicelines { get; set; } // Top nodes

		public Dictionary<string, List<string>> CustomAttributes { get; set; }
	}
}
