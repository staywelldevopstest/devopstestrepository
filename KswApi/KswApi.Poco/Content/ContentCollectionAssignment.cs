﻿using System;
using System.Collections.Generic;

namespace KswApi.Poco.Content
{
	public class ContentCollectionAssignment
	{
		public Guid Id { get; set; }
		public int Count { get; set; }
		public List<Guid> ContentIds { get; set; }
	}
}
