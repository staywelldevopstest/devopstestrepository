﻿using System;
using KswApi.Interface.Enums;

namespace KswApi.Poco.Content
{
	public class ContentOrigin
	{
		public Guid Id { get; set; }
		public string Name { get; set; }
		public string CaseInsensitiveName { get; set; }
		public ContentOriginType Type { get; set; }
	}
}
