﻿namespace KswApi.Poco.Content
{
	public enum TopicType
	{
		None,
		Root,
		Subtopic
	}
}
