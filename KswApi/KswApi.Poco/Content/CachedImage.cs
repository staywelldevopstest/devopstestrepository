﻿using System;
using KswApi.Interface.Objects;

namespace KswApi.Poco.Content
{
	public class CachedImage
	{
		public Guid Id { get; set; }
		public ImageFormatType Type { get; set; }
		public int Width { get; set; }
		public int Height { get; set; }
	}
}
