﻿using System;
using KswApi.Interface.Enums;

namespace KswApi.Poco.Content
{
    [Serializable]
    public class HierarchicalTaxonomyItem
    {
        public string Slug { get; set; }
        public string Path { get; set; }
        public HierarchicalTaxonomyType Type { get; set; }
    }
}
