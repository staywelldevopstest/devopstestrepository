﻿using System;
using System.Collections.Generic;
using System.Dynamic;

namespace KswApi.Poco
{
    public class ReferenceFile
    {
        public string Filepath { get; set; }
        public DateTime FileModifiedDate { get; set; }
        public DateTime LoadDate { get; set; }
        public Dictionary<string,object> Properties { get; set; }
    }
}
