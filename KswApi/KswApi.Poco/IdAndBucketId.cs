﻿using System;

namespace KswApi.Poco
{
	public class IdAndBucketId
	{
		public Guid Id { get; set; }
		public Guid BucketId { get; set; }
	}
}
