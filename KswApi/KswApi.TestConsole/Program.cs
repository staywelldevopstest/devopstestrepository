﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using KswApi.Common.Configuration;
using KswApi.Common.Enums;
using KswApi.ElasticSearch;
using KswApi.Ioc;
using KswApi.Logic.Framework.Objects;
using KswApi.Search.Enums;

namespace KswApi.TestConsole
{
	class Program
	{
		static void Main(string[] args)
		{
			Settings settings = new Settings();
			settings.Environment = EnvironmentName.EA;
			settings.Index.Uri = "https://10.10.20.158:9443";
			settings.Index.UserName = "kswapi";
			settings.Index.Password = "P@ssword1";

			Dependency.Set(settings);

			ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };

			//indexer.DeleteIndex();
		
			DeleteIndex();
			//Go();

		}

		static void Go()
		{
			ElasticSearchIndex indexer = new ElasticSearchIndex();
			indexer.Initialize();
			
			Add();
			Search();
		}

		static void Add()
		{
			ElasticSearchIndex indexer = new ElasticSearchIndex();

			Dictionary<string, object> content = new Dictionary<string, object>
			                                     {
				                                     {"title", "This is the title"},
													 {"body", "This is the body"}
			                                     };
			
			IndexResponse response = indexer.Add(Guid.NewGuid(), content, null, IndexType.Content);
		}

		static void Delete()
		{
			ElasticSearchIndex indexer = new ElasticSearchIndex();
			indexer.Delete(Guid.NewGuid(), IndexType.Content);
		}

		static void DeleteIndex()
		{
			ElasticSearchIndex indexer = new ElasticSearchIndex();
			//indexer.DeleteIndex();
		}

		static void Search()
		{
			ElasticSearchIndex indexer = new ElasticSearchIndex();
			Thread.Sleep(1000);
			//SearchResponse response = indexer.Search("\"big grape\"", 0, 100, IndexType.Content);
			SearchResponse response = indexer.Search("grape", 0, 100, null, null, IndexType.Content);

			if (response.Hits.Total == 0)
				return;
		}

	}
}
