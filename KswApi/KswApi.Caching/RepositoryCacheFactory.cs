﻿using KswApi.Repositories;
using KswApi.Repositories.Enums;
using KswApi.Repositories.Interfaces;

namespace KswApi.Caching
{
	public class RepositoryCacheFactory : IRepositoryFactory
	{
		private readonly IRepositoryFactory _dataLayerFactory;

		public RepositoryCacheFactory(IRepositoryFactory dataLayerFactory)
		{
			_dataLayerFactory = dataLayerFactory;
		}

		public IRepository GetRepository(RepositoryType type)
		{
			return new RepositoryCache(_dataLayerFactory.GetRepository(type));
		}
	}
}
