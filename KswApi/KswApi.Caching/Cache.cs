﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Linq.Expressions;
using KswApi.Common.Configuration;
using KswApi.Common.Interfaces;
using KswApi.Ioc;

namespace KswApi.Caching
{
	public class Cache
	{
		private const char PATH_SEPARATOR = '/';

		private class CacheItem
		{
			public bool Delete { get; set; }
			public Type Type { get; set; }
			public PropertyInfo Property { get; set; }
			public object Value { get; set; }
		}

		private readonly List<CacheItem> _items = new List<CacheItem>();

		public TEntity Get<TEntity>(Expression<Func<TEntity, bool>> expression) where TEntity : class
		{
			string cacheKey = GetCacheKeyForGet(expression);

			if (cacheKey == null)
				return null;

			ICache cache = Dependency.Get<ICache>();

			return cache.Get<TEntity>(cacheKey);
		}

		public TEntity Get<TEntity>() where TEntity : class
		{
			string cacheKey = GetCacheKeyForGet<TEntity>();

			if (cacheKey == null)
				return null;

			ICache cache = Dependency.Get<ICache>();

			return cache.Get<TEntity>(cacheKey);
		}

		public void Set<TEntity, TProperty>(TEntity entity, Expression<Func<TEntity, TProperty>> propertyExpression)
		{
			CacheItem item = GetPathForSet(entity, propertyExpression);
			
			_items.Add(item);
		}

		public void Remove<TEntity, TProperty>(TEntity entity, Expression<Func<TEntity, TProperty>> propertyExpression)
		{
			CacheItem item = GetPathForSet(entity, propertyExpression);
			
			item.Delete = true;
			
			_items.Add(item);
		}

		public void Commit()
		{
			ICache cache = Dependency.Get<ICache>();

			Settings configuration = Dependency.Get<Settings>();
			
			string prefix = string.IsNullOrEmpty(configuration.Cache.Prefix) ? string.Empty : configuration.Cache.Prefix + PATH_SEPARATOR;

			foreach (CacheItem item in _items)
			{
				object value = item.Property.GetValue(item.Value, null);

				// if the property is null, we can't set the value
				if (value == null)
					continue;

				string stringValue = value.ToString();
				
				// if it's an empty string, there is no value to key off
				if (string.IsNullOrEmpty(stringValue))
					continue;

				string key = prefix + item.Type.Name + PATH_SEPARATOR + item.Property.Name + PATH_SEPARATOR + stringValue;

				if (item.Value != null) // null means delete
					cache.Set(key, item.Value);
				else
					cache.Remove(key);
			}

			_items.Clear();
		}

		private static CacheItem GetPathForSet<TEntity>(TEntity entity, LambdaExpression propertyExpression)
		{
			MemberExpression memberExpression = propertyExpression.Body as MemberExpression;

			if (memberExpression == null)
				throw new ArgumentException("Property is not a valid property", "propertyExpression");

			PropertyInfo propertyInfo = memberExpression.Member as PropertyInfo;

			if (propertyInfo == null)
				throw new ArgumentException("Property is not a valid property", "propertyExpression");

			return new CacheItem {Type = typeof (TEntity), Property = propertyInfo, Value = entity};
		}

		private static string GetCacheKeyForGet<TEntity>(Expression<Func<TEntity, bool>> expression)
		{
			if (expression == null || expression.Body.NodeType != ExpressionType.Equal)
				throw new ArgumentException("Expression must be an equivalence expression");

			// At this point, we know the expression node type is ExpressionType.Equal,
			// which is always a binary expression.
			BinaryExpression binaryExpression = (BinaryExpression)expression.Body;

			MemberExpression leftExpression = binaryExpression.Left as MemberExpression;

			if (leftExpression == null)
				throw new ArgumentException("Left side of expression must be a property");

			PropertyInfo propertyInfo = leftExpression.Member as PropertyInfo;

			if (propertyInfo == null)
				throw new ArgumentException("Left side of expression must be a property");

			Expression convertedExpression = Expression.Convert(binaryExpression.Right, typeof(object));

			Func<object> function = Expression.Lambda<Func<object>>(convertedExpression).Compile();

			object value = function();

			if (value == null)
				return null;

			string stringValue = value.ToString();

			if (string.IsNullOrEmpty(stringValue))
				return null;

			Settings configuration = Dependency.Get<Settings>();

			return (configuration.Cache.Prefix ?? string.Empty) + typeof(TEntity).Name + PATH_SEPARATOR + propertyInfo.Name + PATH_SEPARATOR + stringValue;
		}

		private static string GetCacheKeyForGet<TEntity>()
		{
			Settings configuration = Dependency.Get<Settings>();

			return (configuration.Cache.Prefix ?? string.Empty) + typeof(TEntity).Name;
		}
	}
}
