﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using KswApi.Repositories;
using KswApi.Repositories.Interfaces;

namespace KswApi.Caching
{
	class RepositoryCache : IRepository
	{
		private readonly IRepository _repository;
		private Cache _cache;
		private readonly Dictionary<Type, object> _repositories = new Dictionary<Type, object>();

		public RepositoryCache(IRepository repository)
		{
			_repository = repository;
		}

		private static readonly ConcurrentDictionary<Type, ConstructorInfo> Types = new ConcurrentDictionary<Type, ConstructorInfo>();


		public T GetRepository<T>() where T : class
		{
			object repository;

			if (_repositories.TryGetValue(typeof(T), out repository))
				return repository as T;

			ConstructorInfo constructor;

			if (Types.TryGetValue(typeof(T), out constructor))
			{
				repository = constructor.Invoke(new object[] { _repository, _cache ?? (_cache = new Cache()) });

				_repositories.Add(typeof(T), repository);

				return repository as T;
			}

			// if no caching layer is setup for the repository, return the
			// repository of the data layer
			return _repository.GetRepository<T>();
		}

		public void Commit()
		{
			// commit to the repository first
			// this will update any id's on the objects, so that the cache
			// can use them to cache off
			_repository.Commit();

			if (_cache != null)
				_cache.Commit();
		}

		static RepositoryCache()
		{
			Assembly assembly = Assembly.GetExecutingAssembly();

			RepositoryBuilder builder = new RepositoryBuilder();

			foreach (Type type in builder.GetRepositoryTypes())
			{
				Type concreteType = assembly.GetTypes().SingleOrDefault(item => type.IsAssignableFrom(item));

				if (concreteType == null)
					continue;

				ConstructorInfo constructor = concreteType.GetConstructor(new[] { typeof(IRepository), typeof(Cache) });

				if (constructor == null)
					continue;

				Types[type] = constructor;
			}
		}
	}
}
