﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using KswApi.Common.Configuration;
using KswApi.Common.Interfaces;
using KswApi.Ioc;

namespace KswApi.Caching
{
	/// <summary>
	/// This class is left in the project to support the TestLogic,
	/// it should be removed when that class is removed.
	/// </summary>
	public class ManualCache
	{
		private const char PATH_SEPARATOR = '/';

		#region Public Static Methods
		public static void Set<TEntity, TProperty>(TEntity entity, Expression<Func<TEntity, TProperty>> propertyExpression)
		{
			string cacheKey = GetCacheKeyForSet(entity, propertyExpression);

			if (cacheKey == null)
				return;

			ICache cache = Dependency.Get<ICache>();

			cache.Set(cacheKey, entity);
		}

		public static void Set<TEntity, TProperty>(TEntity entity, Expression<Func<TEntity, TProperty>> propertyExpression, TimeSpan expiration)
		{
			string cacheKey = GetCacheKeyForSet(entity, propertyExpression);

			if (cacheKey == null)
				return;

			ICache cache = Dependency.Get<ICache>();

			cache.Set(cacheKey, entity, expiration);
		}

		public static void Set<TEntity>(TEntity entity, string keyName, string keyValue, TimeSpan expiration)
		{
			string cacheKey = typeof(TEntity).Name + PATH_SEPARATOR + keyName + PATH_SEPARATOR + keyValue;

			ICache cache = Dependency.Get<ICache>();

			cache.Set(cacheKey, entity, expiration);
		}

		public static void Set<TEntity>(TEntity entity, string keyName, string keyValue)
		{
			string cacheKey = typeof(TEntity).Name + PATH_SEPARATOR + keyName + PATH_SEPARATOR + keyValue;

			ICache cache = Dependency.Get<ICache>();

			cache.Set(cacheKey, entity);
		}

		public static TEntity Get<TEntity>(Expression<Func<TEntity, bool>> expression) where TEntity : class
		{
			string cacheKey = GetCacheKeyForGet(expression);

			if (cacheKey == null)
				return null;

			ICache cache = Dependency.Get<ICache>();

			return cache.Get<TEntity>(cacheKey);
		}

		public static TEntity Get<TEntity>() where TEntity : class
		{
			string cacheKey = GetCacheKeyForGet<TEntity>();

			if (cacheKey == null)
				return null;

			ICache cache = Dependency.Get<ICache>();

			return cache.Get<TEntity>(cacheKey);
		}

		public static void Remove<TEntity>(Expression<Func<TEntity, bool>> expression) where TEntity : class
		{
			string cacheKey = GetCacheKeyForGet(expression);

			if (cacheKey == null)
				return;

			ICache cache = Dependency.Get<ICache>();

			cache.Get<TEntity>(cacheKey);
		}

		public static void Remove<TEntity, TProperty>(TEntity entity, Expression<Func<TEntity, TProperty>> propertyExpression)
		{
			string cacheKey = GetCacheKeyForSet(entity, propertyExpression);

			if (cacheKey == null)
				return;

			ICache cache = Dependency.Get<ICache>();

			cache.Remove(cacheKey);
		}

		#endregion Public Static Methods

		#region Internal Methods

		private static void Set<TEntity>(TEntity entity, MemberExpression propertyExpression)
		{
			string cacheKey = GetCacheKeyForSet(entity, propertyExpression);

			if (cacheKey == null)
				return;

			ICache cache = Dependency.Get<ICache>();

			cache.Set(cacheKey, entity);
		}

		#endregion Internal Methods

		#region Private Static Methods
		private static string GetCacheKeyForSet<TEntity>(TEntity entity, MemberExpression propertyExpression)
		{
			PropertyInfo propertyInfo = propertyExpression.Member as PropertyInfo;

			if (propertyInfo == null)
				throw new ArgumentException("Property is not a valid property", "propertyExpression");

			object value = propertyInfo.GetValue(entity, null);

			// if the property is null, we can't set the value
			if (value == null)
				return null;

			string stringValue = value.ToString();

			// if it's an empty string, there is no value to key off
			if (string.IsNullOrEmpty(stringValue))
				return null;

			Settings ApplicationConfiguration = Dependency.Get<Settings>();

			return (ApplicationConfiguration.Cache.Prefix ?? string.Empty) + typeof(TEntity).Name + PATH_SEPARATOR + propertyInfo.Name + PATH_SEPARATOR + stringValue;
		}

		private static string GetCacheKeyForSet<TEntity, TProperty>(TEntity entity, Expression<Func<TEntity, TProperty>> propertyExpression)
		{
			MemberExpression memberExpression = propertyExpression.Body as MemberExpression;

			if (memberExpression == null)
				throw new ArgumentException("Property is not a valid property", "propertyExpression");

			return GetCacheKeyForSet(entity, memberExpression);
		}

		private static string GetCacheKeyForGet<TEntity>()
		{
			Settings ApplicationConfiguration = Dependency.Get<Settings>();

			return (ApplicationConfiguration.Cache.Prefix ?? string.Empty) + typeof(TEntity).Name;
		}

		private static string GetCacheKeyForGet<TEntity>(Expression<Func<TEntity, bool>> expression)
		{
			if (expression == null || expression.Body.NodeType != ExpressionType.Equal)
				throw new ArgumentException("Expression must be an equivalence expression");

			// At this point, we know the expression node type is ExpressionType.Equal,
			// which is always a binary expression.
			BinaryExpression binaryExpression = (BinaryExpression)expression.Body;

			MemberExpression leftExpression = binaryExpression.Left as MemberExpression;

			if (leftExpression == null)
				throw new ArgumentException("Left side of expression must be a property");

			PropertyInfo propertyInfo = leftExpression.Member as PropertyInfo;

			if (propertyInfo == null)
				throw new ArgumentException("Left side of expression must be a property");

			Expression convertedExpression = Expression.Convert(binaryExpression.Right, typeof(object));

			Func<object> function = Expression.Lambda<Func<object>>(convertedExpression).Compile();

			object value = function();

			if (value == null)
				return null;

			string stringValue = value.ToString();

			if (string.IsNullOrEmpty(stringValue))
				return null;

			Settings ApplicationConfiguration = Dependency.Get<Settings>();

			return (ApplicationConfiguration.Cache.Prefix ?? string.Empty) + typeof(TEntity).Name + PATH_SEPARATOR + propertyInfo.Name + PATH_SEPARATOR + stringValue;
		}

		#endregion Private Static Methods
	}
}
