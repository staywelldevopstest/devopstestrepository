﻿using KswApi.Repositories.Interfaces;

namespace KswApi.Caching.Repositories
{
	internal class CacheBase<TRepository> where TRepository : class
	{
		private readonly IRepository _repository;
		private readonly Cache _cache;
		
		public CacheBase(IRepository repository, Cache cache)
		{
			_repository = repository;
			_cache = cache;
		}

		public TRepository DataLayer
		{
			get { return _repository.GetRepository<TRepository>(); }
		}

		public Cache Cache { get { return _cache; } }
	}
}
