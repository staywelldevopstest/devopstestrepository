﻿namespace KswApi.ServiceDocumentor
{
	public class DocumentationSettings
	{
		public string BaseUri { get; set; }
		public bool ShowServiceName { get; set; }
		public bool ShowOperationName { get; set; }
		public bool ShowHttpMethod { get; set; }
		public bool ShowUri { get; set; }
		public bool ShowParameters { get; set; }
		public bool ShowXmlExample { get; set; }
		public bool ShowJsonExample { get; set; }
		public bool ShowHttpMethodAndUri { get; set; }
	}
}
