﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using KswApi.Interface;
using KswApi.Reflection;
using KswApi.Reflection.Objects;
using KswApi.RestrictedInterface;
using KswApi.ThirdPartyEndpoints.Interfaces;

namespace KswApi.ServiceDocumentor
{
	public partial class MainForm : Form
	{
		public MainForm()
		{
			InitializeComponent();

			InitializeTreeView();
		}

		private void createDocumentationButton_Click(object sender, EventArgs e)
		{
			richTextBox.Clear();

			TreeNode node = treeView.SelectedNode;

			if (node == null)
				return;

			DocumentationSettings settings = new DocumentationSettings
				                                 {
													 BaseUri = uriBaseTextbox.Text,
													 ShowHttpMethod = httpMethodCheckbox.Checked,
													 ShowJsonExample = jsonExampleCheckbox.Checked,
													 ShowXmlExample = xmlExampleCheckbox.Checked,
													 ShowOperationName = operationNameCheckbox.Checked,
													 ShowServiceName = serviceNameCheckbox.Checked,
													 ShowUri = uriCheckbox.Checked,
													 ShowParameters = parametersCheckbox.Checked,
													 ShowHttpMethodAndUri = httpMethodAndUri.Checked
				                                 };

			DocumentationBuilder builder = new DocumentationBuilder(settings);

			if (node.Tag is ServiceInfo)
				builder.DocumentService((ServiceInfo) node.Tag);
			else if (node.Tag is OperationInfo)
				builder.DocumentOperation((OperationInfo)node.Tag);

			richTextBox.Rtf = builder.ToRtf();
		}

		private void InitializeTreeView()
		{
			treeView.Nodes.Clear();

			ServiceInfo service = new ServiceInfo
				                      {
										  CodeName = "KSW API Services",
										  ServiceName = "",
										  Services = new List<ServiceInfo>(),
										  Operations = new List<OperationInfo>(),
										  UriSignature = ""
				                      };

			TreeNode root = new TreeNode
				                {
					                Text = service.CodeName,
					                Tag = service
				                };

			treeView.Nodes.Add(root);

			root.Nodes.Add(CreateNodes<IOAuthService>("OAuth End Point", service.Services));
			root.Nodes.Add(CreateNodes<IAuthorizationService>("Authorization Service", service.Services));
			root.Nodes.Add(CreateNodes<ApiClient>("API Service", service.Services));
			root.Nodes.Add(CreateNodes<ITestService>("Test Service", service.Services));
			root.Nodes.Add(CreateNodes<ITwilioEndpoint>("Twilio Endpoint", service.Services));
			root.Nodes.Add(CreateNodes<IRenderingService>("Rendering", service.Services));
		}

		TreeNode CreateNodes<T>(string name, List<ServiceInfo> services)
		{
			ServiceReflector reflector = new ServiceReflector();

			ServiceInfo service = reflector.ReflectService(typeof(T), name);

			services.Add(service);

			return CreateNodes(service, name);
		}

		TreeNode CreateNodes(ServiceInfo service, string name)
		{
			TreeNode node = new TreeNode(name);

			node.Tag = service;

			foreach (ServiceInfo childService in service.Services)
				node.Nodes.Add(CreateNodes(childService, childService.CodeName));

			foreach (OperationInfo operation in service.Operations)
				node.Nodes.Add(new TreeNode
				{
					Text = operation.MethodName,
					Tag = operation
				});

			return node;
		}
	}
}
