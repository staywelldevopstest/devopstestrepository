﻿namespace KswApi.ServiceDocumentor
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.treeView = new System.Windows.Forms.TreeView();
			this.createDocumentationButton = new System.Windows.Forms.Button();
			this.richTextBox = new System.Windows.Forms.RichTextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.uriBaseTextbox = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.jsonExampleCheckbox = new System.Windows.Forms.CheckBox();
			this.xmlExampleCheckbox = new System.Windows.Forms.CheckBox();
			this.operationNameCheckbox = new System.Windows.Forms.CheckBox();
			this.httpMethodCheckbox = new System.Windows.Forms.CheckBox();
			this.uriCheckbox = new System.Windows.Forms.CheckBox();
			this.serviceNameCheckbox = new System.Windows.Forms.CheckBox();
			this.parametersCheckbox = new System.Windows.Forms.CheckBox();
			this.httpMethodAndUri = new System.Windows.Forms.CheckBox();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.treeView);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.createDocumentationButton);
			this.splitContainer1.Panel2.Controls.Add(this.richTextBox);
			this.splitContainer1.Panel2.Controls.Add(this.groupBox1);
			this.splitContainer1.Size = new System.Drawing.Size(826, 651);
			this.splitContainer1.SplitterDistance = 275;
			this.splitContainer1.TabIndex = 0;
			// 
			// treeView
			// 
			this.treeView.Dock = System.Windows.Forms.DockStyle.Fill;
			this.treeView.HideSelection = false;
			this.treeView.Location = new System.Drawing.Point(0, 0);
			this.treeView.Name = "treeView";
			this.treeView.Size = new System.Drawing.Size(275, 651);
			this.treeView.TabIndex = 1;
			// 
			// createDocumentationButton
			// 
			this.createDocumentationButton.Location = new System.Drawing.Point(9, 144);
			this.createDocumentationButton.Name = "createDocumentationButton";
			this.createDocumentationButton.Size = new System.Drawing.Size(132, 23);
			this.createDocumentationButton.TabIndex = 4;
			this.createDocumentationButton.Text = "Create Documentation";
			this.createDocumentationButton.UseVisualStyleBackColor = true;
			this.createDocumentationButton.Click += new System.EventHandler(this.createDocumentationButton_Click);
			// 
			// richTextBox
			// 
			this.richTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.richTextBox.DetectUrls = false;
			this.richTextBox.Location = new System.Drawing.Point(9, 173);
			this.richTextBox.Name = "richTextBox";
			this.richTextBox.Size = new System.Drawing.Size(535, 475);
			this.richTextBox.TabIndex = 1;
			this.richTextBox.Text = "";
			this.richTextBox.WordWrap = false;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.httpMethodAndUri);
			this.groupBox1.Controls.Add(this.parametersCheckbox);
			this.groupBox1.Controls.Add(this.serviceNameCheckbox);
			this.groupBox1.Controls.Add(this.uriCheckbox);
			this.groupBox1.Controls.Add(this.httpMethodCheckbox);
			this.groupBox1.Controls.Add(this.operationNameCheckbox);
			this.groupBox1.Controls.Add(this.uriBaseTextbox);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Controls.Add(this.jsonExampleCheckbox);
			this.groupBox1.Controls.Add(this.xmlExampleCheckbox);
			this.groupBox1.Location = new System.Drawing.Point(9, 3);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(535, 135);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Settings";
			// 
			// uriBaseTextbox
			// 
			this.uriBaseTextbox.Location = new System.Drawing.Point(68, 13);
			this.uriBaseTextbox.Name = "uriBaseTextbox";
			this.uriBaseTextbox.Size = new System.Drawing.Size(214, 20);
			this.uriBaseTextbox.TabIndex = 5;
			this.uriBaseTextbox.Text = "http://api.kramesstaywell.com";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(56, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "URI Base:";
			// 
			// jsonExampleCheckbox
			// 
			this.jsonExampleCheckbox.AutoSize = true;
			this.jsonExampleCheckbox.Checked = true;
			this.jsonExampleCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.jsonExampleCheckbox.Location = new System.Drawing.Point(293, 62);
			this.jsonExampleCheckbox.Name = "jsonExampleCheckbox";
			this.jsonExampleCheckbox.Size = new System.Drawing.Size(97, 17);
			this.jsonExampleCheckbox.TabIndex = 3;
			this.jsonExampleCheckbox.Text = "JSON Example";
			this.jsonExampleCheckbox.UseVisualStyleBackColor = true;
			// 
			// xmlExampleCheckbox
			// 
			this.xmlExampleCheckbox.AutoSize = true;
			this.xmlExampleCheckbox.Checked = true;
			this.xmlExampleCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.xmlExampleCheckbox.Location = new System.Drawing.Point(293, 39);
			this.xmlExampleCheckbox.Name = "xmlExampleCheckbox";
			this.xmlExampleCheckbox.Size = new System.Drawing.Size(91, 17);
			this.xmlExampleCheckbox.TabIndex = 2;
			this.xmlExampleCheckbox.Text = "XML Example";
			this.xmlExampleCheckbox.UseVisualStyleBackColor = true;
			// 
			// operationNameCheckbox
			// 
			this.operationNameCheckbox.AutoSize = true;
			this.operationNameCheckbox.Checked = true;
			this.operationNameCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.operationNameCheckbox.Location = new System.Drawing.Point(9, 62);
			this.operationNameCheckbox.Name = "operationNameCheckbox";
			this.operationNameCheckbox.Size = new System.Drawing.Size(103, 17);
			this.operationNameCheckbox.TabIndex = 6;
			this.operationNameCheckbox.Text = "Operation Name";
			this.operationNameCheckbox.UseVisualStyleBackColor = true;
			// 
			// httpMethodCheckbox
			// 
			this.httpMethodCheckbox.AutoSize = true;
			this.httpMethodCheckbox.Checked = true;
			this.httpMethodCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.httpMethodCheckbox.Location = new System.Drawing.Point(167, 39);
			this.httpMethodCheckbox.Name = "httpMethodCheckbox";
			this.httpMethodCheckbox.Size = new System.Drawing.Size(94, 17);
			this.httpMethodCheckbox.TabIndex = 7;
			this.httpMethodCheckbox.Text = "HTTP Method";
			this.httpMethodCheckbox.UseVisualStyleBackColor = true;
			// 
			// uriCheckbox
			// 
			this.uriCheckbox.AutoSize = true;
			this.uriCheckbox.Checked = true;
			this.uriCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.uriCheckbox.Location = new System.Drawing.Point(167, 62);
			this.uriCheckbox.Name = "uriCheckbox";
			this.uriCheckbox.Size = new System.Drawing.Size(45, 17);
			this.uriCheckbox.TabIndex = 8;
			this.uriCheckbox.Text = "URI";
			this.uriCheckbox.UseVisualStyleBackColor = true;
			// 
			// serviceNameCheckbox
			// 
			this.serviceNameCheckbox.AutoSize = true;
			this.serviceNameCheckbox.Checked = true;
			this.serviceNameCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.serviceNameCheckbox.Location = new System.Drawing.Point(9, 39);
			this.serviceNameCheckbox.Name = "serviceNameCheckbox";
			this.serviceNameCheckbox.Size = new System.Drawing.Size(93, 17);
			this.serviceNameCheckbox.TabIndex = 9;
			this.serviceNameCheckbox.Text = "Service Name";
			this.serviceNameCheckbox.UseVisualStyleBackColor = true;
			// 
			// parametersCheckbox
			// 
			this.parametersCheckbox.AutoSize = true;
			this.parametersCheckbox.Checked = true;
			this.parametersCheckbox.CheckState = System.Windows.Forms.CheckState.Checked;
			this.parametersCheckbox.Location = new System.Drawing.Point(167, 85);
			this.parametersCheckbox.Name = "parametersCheckbox";
			this.parametersCheckbox.Size = new System.Drawing.Size(79, 17);
			this.parametersCheckbox.TabIndex = 10;
			this.parametersCheckbox.Text = "Parameters";
			this.parametersCheckbox.UseVisualStyleBackColor = true;
			// 
			// httpMethodAndUri
			// 
			this.httpMethodAndUri.AutoSize = true;
			this.httpMethodAndUri.Location = new System.Drawing.Point(9, 85);
			this.httpMethodAndUri.Name = "httpMethodAndUri";
			this.httpMethodAndUri.Size = new System.Drawing.Size(125, 17);
			this.httpMethodAndUri.TabIndex = 11;
			this.httpMethodAndUri.Text = "HTTP Method + URI";
			this.httpMethodAndUri.UseVisualStyleBackColor = true;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(826, 651);
			this.Controls.Add(this.splitContainer1);
			this.Name = "MainForm";
			this.Text = "Service Documentor";
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.CheckBox jsonExampleCheckbox;
		private System.Windows.Forms.CheckBox xmlExampleCheckbox;
		private System.Windows.Forms.RichTextBox richTextBox;
		private System.Windows.Forms.Button createDocumentationButton;
		private System.Windows.Forms.TreeView treeView;
		private System.Windows.Forms.TextBox uriBaseTextbox;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.CheckBox uriCheckbox;
		private System.Windows.Forms.CheckBox httpMethodCheckbox;
		private System.Windows.Forms.CheckBox operationNameCheckbox;
		private System.Windows.Forms.CheckBox serviceNameCheckbox;
		private System.Windows.Forms.CheckBox parametersCheckbox;
		private System.Windows.Forms.CheckBox httpMethodAndUri;
	}
}

