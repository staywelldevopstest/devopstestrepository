﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using KswApi.Examples;
using KswApi.Reflection;
using KswApi.Reflection.Objects;
using KswApi.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Formatting = Newtonsoft.Json.Formatting;

namespace KswApi.ServiceDocumentor
{
	public class DocumentationBuilder
	{
		private const int TAB_WIDTH = 180;
		private const int DEFAULT_FONT_SIZE = 16;
		private const int HEADING1_FONT_SIZE = 32;
		private const int HEADING2_FONT_SIZE = 24;
		private readonly DocumentationSettings _settings;
		private readonly StringBuilder _builder = new StringBuilder();
		private int _depth;

		public DocumentationBuilder(DocumentationSettings settings)
		{
			_settings = settings;
			_builder.Append(string.Format("{{\\rtf1\\ansi\\deftab{0}", TAB_WIDTH));
			_builder.Append(string.Format("\\fs{0} ", DEFAULT_FONT_SIZE));
		}

		public void DocumentService(ServiceInfo service)
		{
			Indent();

			if (_settings.ShowServiceName && !string.IsNullOrEmpty(service.ServiceName))
			{
				Heading1(service.ServiceName);

				WriteLine();
				_depth++;
			}

			foreach (ServiceInfo childService in service.Services)
				DocumentService(childService);

			foreach (OperationInfo operation in service.Operations)
				DocumentOperation(operation);

			if (_settings.ShowServiceName && !string.IsNullOrEmpty(service.ServiceName))
			{
				_depth--;
			}
		}

		public void DocumentOperation(OperationInfo operation)
		{
			Indent();

			if (_settings.ShowOperationName)
			{
				Heading2(operation.MethodName);
				WriteLine();
				_depth++;
				Indent();
			}

			if (_settings.ShowHttpMethodAndUri)
				WriteLine(operation.Verb + ' ' + GetBaseUri() + operation.UriSignature);

			if (_settings.ShowHttpMethod)
				WriteLine("HTTP Method", operation.Verb);

			if (_settings.ShowUri)
				WriteLine("URI", GetBaseUri() + operation.UriSignature);

			object requestObject = GetRequestObject(operation);
			object responseObject = GetResponseObject(operation);
			List<string> parameters = GetQueryStringParameters(operation);

			if (_settings.ShowParameters && parameters.Count > 0)
			{
				WriteLine("Parameters", string.Join(", ", parameters));
			}

			if (_settings.ShowJsonExample && requestObject != null)
			{
				Indent();
				Bold("JSON Request Example:");
				WriteLine();
				_depth++;
				Indent();
				WriteJson(requestObject);
				_depth--;
			}

			if (_settings.ShowJsonExample && responseObject != null)
			{
				Indent();
				Bold("JSON Response Example:");
				WriteLine();
				_depth++;
				Indent();
				WriteJson(responseObject);
				_depth--;
			}

			if (_settings.ShowXmlExample && requestObject != null)
			{
				Indent();
				Bold("XML Request Example:");
				WriteLine();
				_depth++;
				Indent();
				WriteXml(requestObject);
				_depth--;
			}

			if (_settings.ShowXmlExample && responseObject != null)
			{
				Indent();
				Bold("XML Response Example:");
				WriteLine();
				_depth++;
				Indent();
				WriteXml(responseObject);
				_depth--;
			}

			bool[] operationSettings = new bool[] { 
				_settings.ShowHttpMethod,
				_settings.ShowOperationName,
				_settings.ShowParameters,
				_settings.ShowUri
			};

			if (_settings.ShowXmlExample || _settings.ShowJsonExample
				|| operationSettings.Count(item => item) > 1)
			{
				WriteLine();
			}

			if (_settings.ShowOperationName)
			{
				_depth--;
			}
		}

		public void WriteJson(object obj)
		{
			_builder.Append(string.Format("\\pard\\li{0} ", TAB_WIDTH * _depth));
			StringWriter writer = new StringWriter();
			JsonTextWriter jsonWriter = new JsonTextWriter(writer);
			jsonWriter.IndentChar = '\t';
			jsonWriter.Indentation = 1;

			JsonSerializer serializer = new JsonSerializer();

			serializer.Formatting = Formatting.Indented;

			serializer.NullValueHandling = NullValueHandling.Ignore;
			serializer.Converters.Add(new StringEnumConverter());

			serializer.Serialize(jsonWriter, obj);

			writer.Flush();

			WriteLine(writer.ToString());
			WriteLine();
		}

		public void WriteXml(object obj)
		{
			_builder.Append(string.Format("\\pard\\li{0} ", TAB_WIDTH * _depth));
			CustomXmlSerializer serializer = new CustomXmlSerializer(obj.GetType());
			XmlWriterSettings settings = new XmlWriterSettings
											 {
												 OmitXmlDeclaration = true,
												 Indent = true,
												 IndentChars = "\t"
											 };

			StringWriter stringWriter = new StringWriter();
			XmlWriter writer = XmlWriter.Create(stringWriter, settings);
			serializer.Serialize(writer, obj);
			writer.Flush();

			WriteLine(stringWriter.ToString());
			WriteLine();
		}

		private object GetRequestObject(OperationInfo operation)
		{
			if (operation.Verb == "GET")
				return null;

			foreach (OperationParameterInfo parameter in operation.Parameters)
			{
				TypeCode code = Type.GetTypeCode(parameter.Type);

				if (code != TypeCode.Object)
					continue;

				if (parameter.Type == typeof(Guid) || (parameter.Type.IsGenericType && parameter.Type.GetGenericTypeDefinition() == typeof(Nullable<>)))
					continue;

				if (parameter.Type.IsArray)
					continue;

				return ExampleFactory.Get(parameter.Type);
			}

			return null;
		}

		private List<string> GetQueryStringParameters(OperationInfo operation)
		{
			List<string> list = new List<string>();

			foreach (OperationParameterInfo parameter in operation.Parameters)
			{
				TypeCode code = Type.GetTypeCode(parameter.Type);

				if (code == TypeCode.Object && parameter.Type != typeof(Guid) && !IsNullable(parameter.Type) && !parameter.Type.IsArray)
				{
					if (operation.Verb != "GET")
						continue;

					GetObjectQueryStringParameters(parameter.Type, list);

					continue;
				}
				
				list.Add(parameter.WebName);
			}
			
			return list;
		}

		private void GetObjectQueryStringParameters(Type type, List<string> queryStringParameters)
		{
			foreach (PropertyInfo propertyInfo in type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
			{
				if (!propertyInfo.CanRead || !propertyInfo.CanWrite)
					continue;

				Type propertyType = propertyInfo.PropertyType;

				if (IsNullable(propertyType))
					propertyType = Nullable.GetUnderlyingType(propertyType);

				if (propertyType.IsPrimitive || propertyType == typeof(string) || propertyType == typeof(Guid) || propertyType.IsEnum || propertyType.IsArray)
				{
					DataMemberAttribute attribute = propertyInfo.GetCustomAttribute<DataMemberAttribute>();
					string name = attribute != null && !string.IsNullOrEmpty(attribute.Name)
									  ? attribute.Name
									  : propertyInfo.Name;

					name = char.ToLower(name[0]) + name.Substring(1);
					queryStringParameters.Add(name);
				}
				else if (propertyType.IsClass && typeof(IList).IsAssignableFrom(propertyType))
				{
					DataMemberAttribute attribute = propertyInfo.GetCustomAttribute<DataMemberAttribute>();
					string name = attribute != null && !string.IsNullOrEmpty(attribute.Name)
									  ? attribute.Name
									  : propertyInfo.Name;

					name = char.ToLower(name[0]) + name.Substring(1);
					queryStringParameters.Add(name);
				}
			}
		}

		private bool IsNullable(Type type)
		{
			return type.IsGenericType && type.GetGenericTypeDefinition() == typeof (Nullable<>);
		}

		private object GetResponseObject(OperationInfo operation)
		{
			return ExampleFactory.Get(operation.Method.ReturnType);
		}

		public string ToRtf()
		{
			_builder.Append("}");
			return _builder.ToString();
		}

		private void Indent()
		{
			_builder.Append(string.Format("\\pard\\li{0} ", TAB_WIDTH * _depth));
		}

		private void Write(string text)
		{
			if (string.IsNullOrEmpty(text))
				return;

			foreach (char c in text)
			{
				switch (c)
				{
					case '\t':
						_builder.Append("\\tab");
						break;
					case '\n':
						_builder.Append("\\par");
						break;
					case '\\':
						_builder.Append("\\\\");
						break;
					case '{':
						_builder.Append("\\{");
						break;
					case '}':
						_builder.Append("\\}");
						break;
					case '~':
						_builder.Append("\\~");
						break;
					case '-':
						_builder.Append("\\endash ");
						break;
					case '_':
						_builder.Append("_");
						break;
					default:
						_builder.Append(c);
						break;
				}
			}
		}

		private void Heading1(string value)
		{
			_builder.Append(string.Format("\\fs{0}\\b ", HEADING1_FONT_SIZE));
			Write(value);
			_builder.Append(string.Format("\\fs{0}\\b0 ", DEFAULT_FONT_SIZE));
		}

		private void Heading2(string value)
		{
			_builder.Append(string.Format("\\fs{0}\\b\\ul ", HEADING2_FONT_SIZE));
			Write(value);
			_builder.Append(string.Format("\\fs{0}\\b0\\ul0 ", DEFAULT_FONT_SIZE));
		}

		private void Underline(string value)
		{
			_builder.Append("\\ul ");
			Write(value);
			_builder.Append("\\ul0 ");
		}

		private void Bold(string value)
		{
			_builder.Append("\\b ");
			Write(value);
			_builder.Append("\\b0 ");
		}

		private void WriteLine(string label, string value)
		{
			WriteLabel(label);
			Write(value);
			_builder.Append("\\par");
		}

		private void WriteLine(string value)
		{
			Write(value);
			_builder.Append("\\par");
		}

		private void WriteLine()
		{
			_builder.Append("\\par");
		}

		private string GetBaseUri()
		{
			if (string.IsNullOrEmpty(_settings.BaseUri))
				return "/";
			if (_settings.BaseUri.EndsWith("/"))
				return _settings.BaseUri;
			return _settings.BaseUri + "/";
		}

		private void WriteLabel(string label)
		{
			_builder.Append("\\b ");
			Write(label);
			_builder.Append(':');
			_builder.Append("\\b0 ");
			_builder.Append(" ");
		}

	}
}
