﻿using KswApi.Interface.Objects;
using KswApi.Objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
// ReSharper disable InconsistentNaming

namespace KswApi.Client.IntegrationTests
{
	[TestClass]
	public class SecurityTests
	{
		[TestMethod]
		public void ValidateAccessToken_ValidToken_ReturnsValid()
		{
			string serviceUri = "http://10.10.20.158/kswapi";

			// get an oauth access token
			OAuthClient oauthClient = new OAuthClient(serviceUri);

			// (this application name and secret are set up under "Integration Testing Client" in the admin portal on EA)
			AccessToken token = oauthClient.GetApplicationToken("36c7edfe-3dcb-458f-ba07-e9a3eb665d2a",
											"WNLFfrvOt7j1AyvnpQB5cLtIJiYGT1n2H7ABsMEPMThX6iQDrinjvkJSjgbQbuHp",
											"This is the state");


			Assert.IsNotNull(token);
			Assert.IsFalse(string.IsNullOrEmpty(token.Token));

			// validate the token received
			
			ApiClient client = new ApiClient(serviceUri);
			ValidationResponse response = client.Security.ValidateAccessToken(token.Token);

			Assert.IsNotNull(response);
			Assert.IsTrue(response.Valid);
			Assert.IsNull(response.User);
			Assert.IsNotNull(response.Rights);
			Assert.AreEqual("This is the state", response.State);
		}

		[TestMethod]
		public void ValidateAccessToken_UnknownToken_ReturnsInvalid()
		{
			string serviceUri = "http://10.10.20.158/kswapi";

			ApiClient client = new ApiClient(serviceUri);

			ValidationResponse response = client.Security.ValidateAccessToken("some unknown token");

			Assert.IsNotNull(response);
			Assert.IsFalse(response.Valid);
			Assert.IsNull(response.User);
			Assert.IsNull(response.Rights);
		}

	}
}
