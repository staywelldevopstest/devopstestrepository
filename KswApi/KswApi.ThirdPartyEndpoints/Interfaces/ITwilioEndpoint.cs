﻿using System.ServiceModel;
using System.ServiceModel.Web;
using KswApi.ThirdPartyEndpoints.Objects;

namespace KswApi.ThirdPartyEndpoints.Interfaces
{
	[ServiceContract(Name = "SmsGateway/Endpoints", Namespace = "http://www.kramesstaywell.com")]
	public interface ITwilioEndpoint
	{
		[WebInvoke(UriTemplate = "Twilio", Method = "POST")]
		TwilioResponse TwilioEndpoint(string smsSid, string accountSid, string @from, string to, string body);
	}
}
