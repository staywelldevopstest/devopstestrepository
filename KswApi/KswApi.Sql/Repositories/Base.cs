﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Resources;
using KswApi.Common.Configuration;
using Dapper;

namespace KswApi.Sql.Repositories
{
	public class Base<T> : IDisposable
	{
		private bool _disposed;
		private SqlConnection _connection;

		// ReSharper disable StaticFieldInGenericType
		private static readonly ConcurrentDictionary<string, string> CachedSql = new ConcurrentDictionary<string, string>();
		// ReSharper restore StaticFieldInGenericType

		internal IEnumerable<T> GetMultiple(string path, dynamic parameters = null)
		{
			string sql = GetSql(path);

			IEnumerable<T> reader = SqlMapper.Query<T>(Connection, sql, parameters);

			return reader;
		}

		internal T GetSingle(string path, dynamic parameters = null)
		{
			string sql = GetSql(path);

			return ((IEnumerable<T>) SqlMapper.Query<T>(Connection, sql, parameters)).FirstOrDefault();
		}

		#region Private Methods

		private SqlConnection Connection
		{
			get
			{
				if (_connection != null)
					return _connection;

				_connection = new SqlConnection(Settings.Current.ThreeMHddConnectionString);
				
				_connection.Open();

				return _connection;
			}
		}

	
		private string GetSql(string path)
		{
			return CachedSql.GetOrAdd(path, GetSqlFromResource);
		}

		private string GetSqlFromResource(string path)
		{
			Assembly assembly = Assembly.GetExecutingAssembly();
			path = "KswApi.Sql.Sql." + path.Replace('/', '.').Replace('\\', '.');
			using (Stream stream = assembly.GetManifestResourceStream(path))
			{
				if (stream == null)
					throw new MissingManifestResourceException("Cannot find sql file: " + path);
				StreamReader reader = new StreamReader(stream);
				return reader.ReadToEnd();
			}
		}

		#endregion

		#region Implementation of IDisposable

		public void Dispose()
		{
			if (_disposed)
				return;

			if (_connection != null)
			{
				_connection.Dispose();
			}

			_disposed = true;
		}



		#endregion
	}
}
