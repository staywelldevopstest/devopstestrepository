﻿using System.Collections.Generic;
using KswApi.Poco.ThreeM;

namespace KswApi.Sql.Repositories.ThreeM
{
	public class EncodedDocumentRepository : Base<EncodedDocument>
	{
		public IEnumerable<EncodedDocument> Get(long previousId)
		{
			return GetMultiple("ThreeM/EncodedDocuments.sql", new { PreviousId = previousId });
		}

		public EncodedDocument Get(int bucketId, string legacyId)
		{
			return GetSingle("ThreeM/EncodedDocumentByLegacyId.sql", new {DocumentId = string.Format("{0},{1}", bucketId, legacyId)});
		}
	}
}
