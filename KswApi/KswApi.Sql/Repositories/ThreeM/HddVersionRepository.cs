﻿using System.Linq;
using KswApi.Poco.ThreeM;

namespace KswApi.Sql.Repositories.ThreeM
{
	public class HddVersionRepository : Base<HddVersion>
	{
		public HddVersion GetCurrent()
		{
			return GetMultiple("ThreeM/HddVersion.sql").FirstOrDefault();
		}
	}
}
