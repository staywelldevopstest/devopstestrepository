﻿using System.Collections.Generic;
using KswApi.Poco.ThreeM;

namespace KswApi.Sql.Repositories.ThreeM
{
	public class EncodedTaxonomyRepository : Base<EncodedTaxonomyValue>
	{
		public IEnumerable<EncodedTaxonomyValue> GetByConceptId(long id)
		{
			return GetMultiple("ThreeM/EncodedTaxonomies.sql", new {Id = id});
		}
	}
}
