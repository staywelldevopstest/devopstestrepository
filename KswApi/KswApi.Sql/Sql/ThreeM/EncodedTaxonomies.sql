﻿select concept_relation.CONCEPT_RELATION_NCID as Id, rsform.REPRESENTATION as Value, concept.CONCEPT_DEFINITION as Description, concept_relation.RELATIONSHIP_NCID as RelationshipType, rsform_context.context_ncid as Type
	from concept_relation
	inner join rsform
		on concept_relation.CONCEPT_RELATION_NCID = @Id
		and concept_relation.CONCEPT_NCID = rsform.ncid
		and (concept_relation.RELATIONSHIP_NCID between 55155 and 55166
			or concept_relation.RELATIONSHIP_NCID in (55416, 55418))
	inner join rsform_context
		on rsform.RSFORM_ID = RSFORM_CONTEXT.RSFORM_ID
		and rsform_context.context_ncid in (1257, 17274, 223, 78828, 76781, 76895, 77001, 77004)
	inner join concept on concept.ncid = rsform.NCID
