﻿using System;

namespace KswApi.Search.Exceptions
{
	public class IndexException : Exception
	{
		public IndexException(string message) : base(message)
		{
			
		}
	}
}
