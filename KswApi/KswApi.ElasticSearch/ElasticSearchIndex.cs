﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using KswApi.Common.Configuration;
using KswApi.ElasticSearch.Objects;
using KswApi.Indexing;
using KswApi.Indexing.Filters;
using KswApi.Indexing.Mapping;
using KswApi.Indexing.Objects;
using KswApi.Indexing.Queries;
using KswApi.Interface.Exceptions;
using KswApi.Poco.Content;
using KswApi.Search.Constants;
using KswApi.Search.Exceptions;
using Newtonsoft.Json;

namespace KswApi.ElasticSearch
{
	public class ElasticSearchIndex : IIndex
	{
		#region Public Methods

		public void Initialize()
		{
			EnsureIndexes();
		}

		public void DeleteType(IndexType type)
		{
			string name = GetIndexName();

			//if (!TypeExists(name, type))
			//	return;

			DeleteType(name, type);
		}

		public void DeleteIndex()
		{
			string name = GetIndexName();

			if (!IndexExists(name))
				return;

			DeleteIndex(name);
		}

		public IndexResponse Add(Guid id, Dictionary<string, object> fields, IndexType type)
		{
			if (type == IndexType.None)
				throw new ArgumentException("type");

			string uri = string.Format("{0}/{1}/{2}", GetIndexName(), type, id);

			HttpWebRequest request = GetRequest(uri);

			request.Method = "PUT";

			JsonSerializer serializer = new JsonSerializer();

			try
			{
				using (Stream stream = request.GetRequestStream())
				{
					TextWriter writer = new StreamWriter(stream);

					serializer.Serialize(writer, fields);

					writer.Flush();
				}

				using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
				{
					using (Stream responseStream = response.GetResponseStream())
					{
						if (responseStream == null)
							throw new IndexException("Index communication error");

						IndexResponse result =
							(IndexResponse)serializer.Deserialize(new StreamReader(responseStream), typeof(IndexResponse));

						return result;
					}
				}
			}
			catch (Exception exception)
			{
				HandleException(exception);
				throw;
			}
		}

		public IndexResponse UpdatePartial(Guid id, Dictionary<string, object> fields, IndexType type)
		{
			if (type == IndexType.None)
				throw new ArgumentException("type");

			string uri = string.Format("{0}/{1}/{2}/_update", GetIndexName(), type, id);

			HttpWebRequest request = GetRequest(uri);

			request.Method = "POST";

			PartialUpdateRequest partialUpdateRequest = new PartialUpdateRequest
			                                            {
				                                            Document = fields
			                                            };

			JsonSerializer serializer = new JsonSerializer();
			
			// make sure null values are updated
			serializer.NullValueHandling = NullValueHandling.Include;

			try
			{
				using (Stream stream = request.GetRequestStream())
				{
					TextWriter writer = new StreamWriter(stream);

					serializer.Serialize(writer, partialUpdateRequest);

					writer.Flush();
				}

				using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
				{
					using (Stream responseStream = response.GetResponseStream())
					{
						if (responseStream == null)
							throw new IndexException("Index communication error");

						IndexResponse result =
							(IndexResponse)serializer.Deserialize(new StreamReader(responseStream), typeof(IndexResponse));

						return result;
					}
				}
			}
			catch (Exception exception)
			{
				HandleException(exception);
				throw;
			}
		}

		public IndexResponse Update(Guid id, Dictionary<string, object> fields, IndexType type)
		{
			return Add(id, fields, type);
		}

		public void Delete(Guid id, IndexType type)
		{
			if (type == IndexType.None)
				throw new ArgumentException("type");

			// refresh allows immediate search results with the deleted
			// value excluded. Consider changing this if it impacts performance
			// *this should be optimized for bulk deletes, only refresh after all documents
			// in a bulk update are deleted
			string uri = string.Format("{0}/{1}/{2}?refresh=true", GetIndexName(), type, id);

			HttpWebRequest request = GetRequest(uri);

			request.Method = "DELETE";

			try
			{
				using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
				{
				}
			}
			catch (Exception exception)
			{
				HandleException(exception);
				throw;
			}
		}

		public SearchResponse Search(int offset, int count, SearchQuery query, SearchFilter filter, List<string> facets, List<SearchSort> sort, params IndexType[] types)
		{
			if (types.Any(item => item == IndexType.None))
				throw new ArgumentException("types");

			string uri = string.Format("{0}/{1}/_search",
									   GetIndexName(),
									   string.Join(",", types));

			List<Dictionary<string, SearchSort.Item>> sortValue = null;

			if (sort != null && sort.Count > 0)
			{
				sortValue = sort.Select(item => new Dictionary<string, SearchSort.Item> { { item.Name, item.Sort } }).ToList();
			}

			SearchRequest searchRequest = new SearchRequest
										  {
											  From = offset,
											  Size = count,
											  Query = query,
											  Filter = filter,
											  Facets = GetFacets(facets),
											  Sort = sortValue
										  };

			HttpWebRequest request = GetRequest(uri);
			request.Method = "POST";

			try
			{
				JsonSerializer serializer = new JsonSerializer
				{
					Formatting = Formatting.None,
					NullValueHandling = NullValueHandling.Ignore
				};

				using (Stream stream = request.GetRequestStream())
				{
					StreamWriter writer = new StreamWriter(stream);

					StringWriter w = new StringWriter();
					serializer.Serialize(w, searchRequest);
					string s = w.ToString();
					serializer.Serialize(writer, searchRequest);

					writer.Flush();
				}

				using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
				{
					using (Stream stream = response.GetResponseStream())
					{
						StreamReader reader = new StreamReader(stream);

						SearchResponse result = (SearchResponse)serializer.Deserialize(reader, typeof(SearchResponse));
						return result;
					}
				}
			}
			catch (Exception exception)
			{
				HandleException(exception);
				throw;
			}
		}

		public void Map(Dictionary<string, FieldMap> map, IndexType type)
		{
			Dictionary<string, MappingRequest> mapping = new Dictionary<string, MappingRequest>
			                                             {
															 {
																 type.ToString(),
																 new MappingRequest
																 {
																	 Properties = map
																 }
															 }
			                                             };

			string uri = string.Format("{0}/{1}/_mapping", GetIndexName(), type);

			HttpWebRequest request = GetRequest(uri);

			request.Method = "PUT";

			JsonSerializer serializer = new JsonSerializer();

			using (Stream stream = request.GetRequestStream())
			{
				StreamWriter writer = new StreamWriter(stream);

				serializer.NullValueHandling = NullValueHandling.Ignore;

				serializer.Serialize(writer, mapping);

				writer.Flush();
			}

			try
			{
				using (request.GetResponse())
				{
					// 200 means eveything's ok, just return
				}
			}
			catch (Exception exception)
			{
				HandleException(exception);
				throw;
			}
		}

		#endregion

		private Dictionary<string, FacetRequest> GetFacets(List<string> facets)
		{
			if (facets == null || facets.Count == 0)
				return null;

			return facets.ToDictionary(item => item, item => new FacetRequest { Terms = new TermFacetRequest { Field = item } });
		}

		private void HandleException(Exception exception)
		{
			if (exception is WebException)
			{
				string message = null;

				WebException webException = (WebException)exception;
				HttpWebResponse response = (HttpWebResponse)webException.Response;

				if (response == null)
					throw new IndexException(exception.Message);

				using (Stream responseStream = response.GetResponseStream())
				{
					if (responseStream != null)
					{
						StreamReader reader = new StreamReader(responseStream);
						message = reader.ReadToEnd();
					}
				}

				if (string.IsNullOrEmpty(message))
					message = exception.Message;

				// we are assuming that this is an invalid query since ElasticSearch returns Internal Server Error for ParseExceptions
				throw new ServiceException(HttpStatusCode.BadRequest, "Invalid query.");
			}

			throw new IndexException(exception.Message);
		}

		public void DeleteIndex(string name)
		{
			string uri = string.Format("{0}", name);

			HttpWebRequest request = GetRequest(uri);

			request.Method = "DELETE";

			try
			{
				using (request.GetResponse())
				{
				}
			}
			catch (Exception exception)
			{
				HandleException(exception);
			}
		}

		private void DeleteType(string indexName, IndexType type)
		{
			string uri = string.Format("{0}/{1}", indexName, type);

			HttpWebRequest request = GetRequest(uri);

			request.Method = "DELETE";

			try
			{
				using (request.GetResponse())
				{
				}
			}
			catch (WebException exception)
			{
				if (exception.Response != null && ((HttpWebResponse)exception.Response).StatusCode == HttpStatusCode.NotFound)
					return;

				HandleException(exception);
				throw;
			}
			catch (Exception exception)
			{
				HandleException(exception);
			}
		}

		//private bool TypeExists(string indexName, IndexType type)
		//{
		//	string uri = string.Format("{0}/{1}", indexName, type);

		//	HttpWebRequest request = GetRequest(uri);

		//	request.Method = "HEAD";

		//	try
		//	{
		//		// 404 means the type doesn't exist
		//		using (request.GetResponse())
		//		{
		//			// 200 means the type exists, just return
		//			return true;
		//		}
		//	}
		//	catch (WebException exception)
		//	{
		//		if (exception.Response != null && ((HttpWebResponse)exception.Response).StatusCode == HttpStatusCode.NotFound)
		//			return false;

		//		HandleException(exception);
		//		throw;
		//	}
		//	catch (Exception exception)
		//	{
		//		HandleException(exception);
		//		throw;
		//	}	
		//}

		#region Private Methods

		private bool IndexExists(string indexName)
		{
			string uri = string.Format("{0}", indexName);

			HttpWebRequest request = GetRequest(uri);

			request.Method = "HEAD";

			try
			{
				// 404 means the index doesn't exist
				using (request.GetResponse())
				{
					// 200 means the index exists, just return
					return true;
				}
			}
			catch (WebException exception)
			{
				if (exception.Response != null && ((HttpWebResponse)exception.Response).StatusCode == HttpStatusCode.NotFound)
					return false;

				HandleException(exception);
				throw;
			}
			catch (Exception exception)
			{
				HandleException(exception);
				throw;
			}
		}

		private bool CreateIndex(string indexName)
		{
			string uri = string.Format("{0}/", GetIndexName());

			HttpWebRequest request = GetRequest(uri);

			request.Method = "POST";

			CreateIndexRequest settings = new CreateIndexRequest
			{
				Settings = new IndexSettingsRequest
				{
					ShardCount = 1,
					ReplicaCount = 1,
					Analysis = new AnalysisRequest()
					{
						Analyzer = new Dictionary<string, AnalyzerRequest>
							{
								{
									Constant.ANALYZER_NAME,
									new AnalyzerRequest
									{
										Type = "custom",
										Tokenizer = "standard",
										Filter = new List<string> {"standard", "lowercase", "stop"}
									}
								}
							}
					}
				}
			};

			try
			{
				using (Stream stream = request.GetRequestStream())
				{
					StreamWriter writer = new StreamWriter(stream);

					JsonSerializer serializer = new JsonSerializer();

					serializer.NullValueHandling = NullValueHandling.Ignore;

					serializer.Serialize(writer, settings);

					writer.Flush();
				}

				// 404 means the index doesn't exist
				using (request.GetResponse())
				{
					// 200 means the index exists, just return
					return true;
				}
			}
			catch (WebException exception)
			{
				// if we get a bad request, it's possible another instance created the index
				// check for the index, and continue if it exists
				if (exception.Response == null)
					return false;

				if (((HttpWebResponse)exception.Response).StatusCode == HttpStatusCode.BadRequest
					&& IndexExists(indexName))
					return false;

				HandleException(exception);
				throw;
			}
			catch (Exception exception)
			{
				HandleException(exception);
				throw;
			}
		}

		private HttpWebRequest GetRequest(string path = null)
		{
			string uri = Settings.Current.Index.Uri;

			if (!uri.EndsWith("/"))
				uri += '/';

			HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri + path);

			string authorizationString = string.Format("{0}:{1}", Settings.Current.Index.UserName, Settings.Current.Index.Password);
			string encodedAuthorization = Convert.ToBase64String(Encoding.UTF8.GetBytes(authorizationString));
			request.Headers.Add(HttpRequestHeader.Authorization, string.Format("Basic {0}", encodedAuthorization));

			return request;
		}

		private void EnsureIndexes()
		{
			if (!IndexExists(GetIndexName()))
				CreateIndex(GetIndexName());
		}

		private string GetIndexName()
		{
			return Settings.Current.Index.Name;
		}

		#endregion
	}
}
