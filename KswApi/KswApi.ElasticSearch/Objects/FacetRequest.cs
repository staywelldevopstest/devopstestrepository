﻿using Newtonsoft.Json;

namespace KswApi.ElasticSearch.Objects
{
	class FacetRequest
	{
		[JsonProperty(PropertyName = "terms")]
		public TermFacetRequest Terms { get; set; }
	}
}
