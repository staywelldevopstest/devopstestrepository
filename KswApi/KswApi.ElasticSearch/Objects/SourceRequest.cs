﻿using Newtonsoft.Json;

namespace KswApi.ElasticSearch.Objects
{
	class SourceRequest
	{
		[JsonProperty(PropertyName = "_source")]
		public SourceRequest Source { get; set; }

		[JsonProperty(PropertyName = "enabled")]
		public bool Enabled { get; set; }
	}
}
