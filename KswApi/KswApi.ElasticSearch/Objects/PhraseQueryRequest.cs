﻿using Newtonsoft.Json;

namespace KswApi.ElasticSearch.Objects
{
	class PhraseQueryRequest
	{
		[JsonProperty(PropertyName = "query")]
		public string Query { get; set; }

		[JsonProperty(PropertyName = "slop")]
		public int? Slop { get; set; }
	}
}
