﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace KswApi.ElasticSearch.Objects
{
	class MatchQueryRequest
	{
		[JsonProperty(PropertyName = "span_term")]
		public Dictionary<string, string> SpanTerm { get; set; } 
	}
}
