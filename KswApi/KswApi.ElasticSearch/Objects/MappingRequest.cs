﻿using System.Collections.Generic;
using KswApi.Indexing.Mapping;
using Newtonsoft.Json;

namespace KswApi.ElasticSearch.Objects
{
	class MappingRequest
	{
		[JsonProperty(PropertyName = "index_analyzer")]
		public string IndexAnalyzer { get; set; }

		[JsonProperty(PropertyName = "search_analyzer")]
		public string SearchAnalyzer { get; set; }

		[JsonProperty(PropertyName = "date_formats")]
		public List<string> DateFormats { get; set; }
	
		[JsonProperty(PropertyName = "_source")]
		public SourceRequest Source { get; set; }

		[JsonProperty(PropertyName = "properties")]
		public Dictionary<string, FieldMap> Properties { get; set; }
	}
}
