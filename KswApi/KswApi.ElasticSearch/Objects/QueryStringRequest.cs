﻿using Newtonsoft.Json;

namespace KswApi.ElasticSearch.Objects
{
	class QueryStringRequest
	{
		[JsonProperty(PropertyName = "query")]
		public string Query { get; set; }

		[JsonProperty(PropertyName = "analyzer")]
		public string Analyzer { get; set; }

		[JsonProperty(PropertyName = "lenient")]
		public bool Lenient { get; set; }
	}
}
