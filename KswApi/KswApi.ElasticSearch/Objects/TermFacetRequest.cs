﻿using Newtonsoft.Json;

namespace KswApi.ElasticSearch.Objects
{
	class TermFacetRequest
	{
		[JsonProperty(PropertyName = "field")]
		public string Field { get; set; }
	}
}
