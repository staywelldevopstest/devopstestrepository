﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace KswApi.ElasticSearch.Objects
{
	class AnalysisRequest
	{
		[JsonProperty(PropertyName = "filter")]
		public Dictionary<string, Dictionary<string, object>> Filter { get; set; }

		[JsonProperty(PropertyName = "analyzer")]
		public Dictionary<string, AnalyzerRequest> Analyzer { get; set; }
	}
}
