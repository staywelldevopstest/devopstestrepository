﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace KswApi.ElasticSearch.Objects
{
	class PartialUpdateRequest
	{
		[JsonProperty(PropertyName = "doc")]
		public Dictionary<string, object> Document { get; set; } 
	}
}
