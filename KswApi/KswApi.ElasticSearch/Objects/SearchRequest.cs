﻿using System.Collections.Generic;
using KswApi.Indexing;
using KswApi.Indexing.Filters;
using KswApi.Indexing.Queries;
using Newtonsoft.Json;

namespace KswApi.ElasticSearch.Objects
{
	class SearchRequest
	{
		[JsonProperty(PropertyName = "from")]
		public int From { get; set; }

		[JsonProperty(PropertyName = "size")]
		public int Size { get; set; }

		[JsonProperty(PropertyName = "query")]
		public SearchQuery Query { get; set; }

		[JsonProperty(PropertyName = "filter")]
		public SearchFilter Filter { get; set; }

		[JsonProperty(PropertyName = "facets")]
		public Dictionary<string, FacetRequest> Facets { get; set; }

		[JsonProperty(PropertyName = "sort")]
		public List<Dictionary<string, SearchSort.Item>> Sort { get; set; }
	}
}
