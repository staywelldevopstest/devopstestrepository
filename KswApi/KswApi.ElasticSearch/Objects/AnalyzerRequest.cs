﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace KswApi.ElasticSearch.Objects
{
	class AnalyzerRequest
	{
		[JsonProperty(PropertyName = "type")]
		public string Type { get; set; }

		[JsonProperty(PropertyName = "tokenizer")]
		public string Tokenizer { get; set; }

		[JsonProperty(PropertyName = "filter")]
		public List<string> Filter { get; set; }
	}
}
