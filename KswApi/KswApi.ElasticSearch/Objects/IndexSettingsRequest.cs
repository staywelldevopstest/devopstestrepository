﻿using Newtonsoft.Json;

namespace KswApi.ElasticSearch.Objects
{
	class IndexSettingsRequest
	{
		[JsonProperty(PropertyName = "number_of_shards")]
		public int ShardCount { get; set; }

		[JsonProperty(PropertyName = "number_of_replicas")]
		public int ReplicaCount { get; set; }

		[JsonProperty(PropertyName = "analysis")]
		public AnalysisRequest Analysis { get; set; }
	}
}
