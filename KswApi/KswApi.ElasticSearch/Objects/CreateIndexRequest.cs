﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Newtonsoft.Json;

namespace KswApi.ElasticSearch.Objects
{
	class CreateIndexRequest
	{
		[JsonProperty(PropertyName = "settings")]
		public IndexSettingsRequest Settings { get; set; }

		[JsonProperty(PropertyName = "mappings")]
		public Dictionary<string, MappingRequest> Mapping { get; set; }
	}
}
