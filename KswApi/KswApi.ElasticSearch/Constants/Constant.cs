﻿namespace KswApi.Search.Constants
{
	public static class Constant
	{
		public const string NGRAM_FILTER_NAME = "nGram_Filter";
		public const string NGRAM_ANALYZER_NAME = "nGram_Analayzer";
		public const string ANALYZER_NAME = "default_analyzer";
		public const string PHRASE_FILTER_NAME = "phraseFilter";
		public const string PHRASE_ANALYZER_NAME = "phraseFilter";
		public const int NGRAM_MINIMUM = 1;
		public const int NGRAM_MAXIMUM = 10;
		public const int SPAN_SEARCH_END = 1;
		public const string SEARCH_ALL_FIELD = "_all";
		public const int PHRASE_SEARCH_SLOP = 1;
	}
}
