﻿
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Logic.Report;
using KswApi.Poco.Sms;
using KswApi.Repositories.Objects;
using KswApi.Test.Framework.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;

namespace KswApi.Test
{
    [TestClass]
    public class ReportLogicTests
    {
        private const string CLIENT_NAME = "Test Client";
        private const string KEYWORD = "TestKeyword";
        private const string SHORTCODE = "12345";
        private const string LONGCODE = "8015551234";

        private const string CSV_STRING =
            "SMS Gateway Billing Report\r\n\r\nClient Name,License Name,Sms Short Code Keyword Count,Long Code Count,Outgoing Message Count,Incoming Message Count\r\n\r\nTest Client\r\n,Test Client 0,5,3,55,13\r\n";

        [TestMethod]
        public void ActiveClientWithEverything_Successful()
        {
            int outgoingMessages = 55;
            int incomingMessages = 13;
            int licenseCount = 1;
            int longCodeCount = 3;
            int numKeywords = 5;
			Month month = (Month)DateTime.UtcNow.Month;
			int year = DateTime.UtcNow.Year;

            FakeLayer fakeLayer = FakeLayer.Setup();

            GenerateClientReportRawData(ClientStatus.Active, CLIENT_NAME, licenseCount, true, longCodeCount, numKeywords, outgoingMessages, incomingMessages, fakeLayer, month, year);

            ReportLogic logic = new ReportLogic();

            SmsBillingReportDataResponse report = logic.GetSmsBillingReportData(month, year);

            Assert.IsTrue(report.Clients.Count == 1);
            Assert.IsTrue(report.Clients[0].Licenses.Count == licenseCount);
            Assert.IsTrue(report.Clients[0].Licenses[0].ShortCodeKeywordCount == numKeywords);
            Assert.IsTrue(report.Clients[0].Licenses[0].LongCodeCount == longCodeCount);
            Assert.IsTrue(report.Clients[0].Licenses[0].IncomingMessageCount == incomingMessages);
            Assert.IsTrue(report.Clients[0].Licenses[0].OutgoingMessageCount == outgoingMessages);
        }

        [TestMethod]
        public void ActiveClientNoLicenses_ReturnsNothing()
        {
			Month month = (Month)DateTime.UtcNow.Month;
			int year = DateTime.UtcNow.Year;

            FakeLayer fakeLayer = FakeLayer.Setup();

            GenerateClientReportRawData(ClientStatus.Active, CLIENT_NAME, 0, true, 0, 0, 0, 0, fakeLayer, month, year);

            ReportLogic logic = new ReportLogic();

            SmsBillingReportDataResponse report = logic.GetSmsBillingReportData(month, year);

            Assert.IsTrue(report.Clients.Count == 0);
        }

        [TestMethod]
        public void ActiveClient_NoShortCode_Successful()
        {
            int outgoingMessages = 55;
            int incomingMessages = 13;
            int licenseCount = 1;
            int longCodeCount = 3;
            int numKeywords = 5;
			Month month = (Month)DateTime.UtcNow.Month;
			int year = DateTime.UtcNow.Year;

            FakeLayer fakeLayer = FakeLayer.Setup();

            GenerateClientReportRawData(ClientStatus.Active, CLIENT_NAME, licenseCount, false, longCodeCount, numKeywords, outgoingMessages, incomingMessages, fakeLayer, month, year);

            ReportLogic logic = new ReportLogic();

            SmsBillingReportDataResponse report = logic.GetSmsBillingReportData(month, year);

            Assert.IsTrue(report.Clients.Count == 1);
            Assert.IsTrue(report.Clients[0].Licenses.Count == licenseCount);
            Assert.IsTrue(report.Clients[0].Licenses[0].ShortCodeKeywordCount == 0);
            Assert.IsTrue(report.Clients[0].Licenses[0].LongCodeCount == longCodeCount);
            Assert.IsTrue(report.Clients[0].Licenses[0].IncomingMessageCount == incomingMessages);
            Assert.IsTrue(report.Clients[0].Licenses[0].OutgoingMessageCount == outgoingMessages);
        }

        [TestMethod]
        public void InactiveClient_NoReturn()
        {
			Month month = (Month)DateTime.UtcNow.Month;
			int year = DateTime.UtcNow.Year;

            FakeLayer fakeLayer = FakeLayer.Setup();

            GenerateClientReportRawData(ClientStatus.Inactive, CLIENT_NAME, 0, false, 0, 0, 0, 0, fakeLayer, month, year);

            ReportLogic logic = new ReportLogic();

            SmsBillingReportDataResponse report = logic.GetSmsBillingReportData(month, year);

            Assert.IsTrue(report.Clients.Count == 0);
        }

        [TestMethod]
        public void InactiveClient_WithActivity_ReturnsData()
        {
            int outgoingMessages = 55;
            int incomingMessages = 13;
            int licenseCount = 1;
            int longCodeCount = 3;
            int numKeywords = 5;
			Month month = (Month)DateTime.UtcNow.Month;
			int year = DateTime.UtcNow.Year;

            FakeLayer fakeLayer = FakeLayer.Setup();

            GenerateClientReportRawData(ClientStatus.Inactive, CLIENT_NAME, licenseCount, true, longCodeCount, numKeywords, outgoingMessages, incomingMessages, fakeLayer, month, year);

            ReportLogic logic = new ReportLogic();

            SmsBillingReportDataResponse report = logic.GetSmsBillingReportData(month, year);

            Assert.IsTrue(report.Clients.Count == 1);
            Assert.IsTrue(report.Clients[0].Licenses.Count == licenseCount);
            Assert.IsTrue(report.Clients[0].Licenses[0].ShortCodeKeywordCount == numKeywords);
            Assert.IsTrue(report.Clients[0].Licenses[0].LongCodeCount == longCodeCount);
            Assert.IsTrue(report.Clients[0].Licenses[0].IncomingMessageCount == incomingMessages);
            Assert.IsTrue(report.Clients[0].Licenses[0].OutgoingMessageCount == outgoingMessages);
        }


        [TestMethod]
        public void ExportCsv_Successful()
        {
            int outgoingMessages = 55;
            int incomingMessages = 13;
            int licenseCount = 1;
            int longCodeCount = 3;
            int numKeywords = 5;
			Month month = (Month)DateTime.UtcNow.Month;
			int year = DateTime.UtcNow.Year;

            FakeLayer fakeLayer = FakeLayer.Setup();

            GenerateClientReportRawData(ClientStatus.Active, CLIENT_NAME, licenseCount, true, longCodeCount, numKeywords, outgoingMessages, incomingMessages, fakeLayer, month, year);

            ReportLogic logic = new ReportLogic();

            FakeStreamResponse stream = new FakeStreamResponse();

            logic.GetSmsBillingReportCsvExport(stream, month, year);

            Stream csvStream = stream;

            csvStream.Position = 0;

            StreamReader reader = new StreamReader(csvStream);

            string csvData = reader.ReadToEnd();

            Assert.IsFalse(string.IsNullOrWhiteSpace(csvData));
            Assert.IsTrue(csvData == CSV_STRING);
        }

        private void GenerateClientReportRawData(ClientStatus clientStatus, string clientName, int? licenseCount,
                                      bool hasShortCode, int longCodeCount,
                                      int numShortCodeKeywords, int outgoingMessageCount,
                                      int incomingMessageCount, FakeLayer fakeLayer,
                                      Month month, int year)
        {
            Client client = new Client { ClientName = clientName, Status = clientStatus, Id = Guid.NewGuid() };

            List<Guid> licenseIds = new List<Guid>();
            List<License> licenses = new List<License>();

            if (licenseCount > 0)
            {
                //Set up ShortCode and shortcode keywords
                for (int i = 0; i < licenseCount; i++)
                {
                    DateTime dateTimeToUse = new DateTime(year, (int)month, 15);

                    License license = new License { Id = Guid.NewGuid(), ClientId = client.Id, Name = CLIENT_NAME + " " + i, CreatedDate = dateTimeToUse, Status = LicenseStatus.Active, Modules = new List<ModuleType>() };
                    licenseIds.Add(license.Id);

                    //Short Code
                    if (hasShortCode)
                    {
                        license.Modules.Add(ModuleType.SmsGateway);

                        SmsNumberData numberData = new SmsNumberData { Format = MessageFormat.Json, Id = Guid.NewGuid(), Number = SHORTCODE, LicenseId = license.Id, Type = SmsNumberType.ShortCode, RouteType = SmsNumberRouteType.Keyword, Keywords = new List<SmsNumberKeywordItem>() };
                        SmsNumberDataLog numberDataLog = new SmsNumberDataLog(numberData);

                        //Add keywords to the short code as this is a metric we track
                        for (int j = 0; j < numShortCodeKeywords; j++)
                        {
                            SmsKeywordData keywordData = new SmsKeywordData { Keyword = KEYWORD + j, Format = MessageFormat.Json, Id = Guid.NewGuid(), LicenseId = license.Id, Number = numberData.Number, NumberId = numberData.Id };
                            SmsKeywordDataLog keywordDataLog = new SmsKeywordDataLog(keywordData);

                            fakeLayer.DataLayer.AddItem(keywordData);
                            fakeLayer.DataLayer.AddItem(keywordDataLog);

                            SmsNumberKeywordItem keywordItem = new SmsNumberKeywordItem { Keyword = keywordData.Keyword, KeywordId = keywordData.Id };

                            numberData.Keywords.Add(keywordItem);
                        }

                        fakeLayer.DataLayer.AddItem(numberData);
                        fakeLayer.DataLayer.AddItem(numberDataLog);
                    }

                    //Long Code.  We don't need to track long code keywords so we dont need to add them
                    if (longCodeCount > 0)
                    {
                        if (!license.Modules.Contains(ModuleType.SmsGateway))
                            license.Modules.Add(ModuleType.SmsGateway);

                        for (int j = 0; j < longCodeCount; j++)
                        {
                            SmsNumberData numberData = new SmsNumberData
                                {
                                    Format = MessageFormat.Json,
                                    Id = Guid.NewGuid(),
                                    Number = LONGCODE,
                                    LicenseId = license.Id,
                                    Type = SmsNumberType.LongCode,
                                    RouteType = SmsNumberRouteType.Keyword,
                                    Keywords = new List<SmsNumberKeywordItem>()
                                };

                            SmsNumberDataLog numberDataLog = new SmsNumberDataLog(numberData);

                            fakeLayer.DataLayer.AddItem(numberData);
                            fakeLayer.DataLayer.AddItem(numberDataLog);
                        }
                    }

                    //We don't need to add a message log for every message as we only need to update the billing report data record.
                    fakeLayer.DataLayer.AddItem(new SmsBillingReportLicenseData
                        {
                            Id = Guid.NewGuid(),
                            LicenseId = license.Id,
                            IncomingMessageCount = incomingMessageCount,
                            OutgoingMessageCount = outgoingMessageCount,
                            LicenseName = license.Name,
                            Year = year,
                            Month = month
                        });

                    fakeLayer.DataLayer.AddItem(license);

                    licenses.Add(license);
                }
            }

            client.Licenses = licenseIds;

            fakeLayer.DataLayer.AddItem(client);
        }
    }
}
