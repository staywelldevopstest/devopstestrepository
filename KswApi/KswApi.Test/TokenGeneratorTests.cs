﻿using KswApi.Common;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable InconsistentNaming

namespace KswApi.Test
{
	[TestClass]
	public class TokenGeneratorTests
	{
		[TestMethod]
		public void CreateSecret_CreatesSecretOfCorrectLength()
		{
			string secret = TokenGenerator.CreateSecret();
			Assert.AreEqual(secret.Length, 64);
		}

		[TestMethod]
		public void CreateToken_CreatesTokenOfCorrectLength()
		{
			string secret = TokenGenerator.CreateToken();
			Assert.AreEqual(secret.Length, 64);
		}
	}
}
