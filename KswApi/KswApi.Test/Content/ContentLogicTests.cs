﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using KswApi.Indexing;
using KswApi.Indexing.Objects;
using KswApi.Indexing.Queries;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.ContentLogic;
using KswApi.Logic.Framework.Constants;
using KswApi.Poco.Content;
using KswApi.Test.Framework;
using KswApi.Test.Framework.DataGenerators;
using KswApi.Test.Framework.Fakes;
using KswApi.Test.Framework.Objects;
using KswApi.Test.Framework.TestUtilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

// ReSharper disable InconsistentNaming

namespace KswApi.Test.Content
{
    [TestClass]
    public class ContentLogicTests
    {
        #region Private Members

        private const string FAKE_COPYRIGHT_ID = "95bb45b8-0151-429a-a4d7-a1f5011b426e";

        #endregion

        [TestMethod]
        public void CreateContent_CreatesContent()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();
            FakeLayerDataInitialization.Init(fakeLayer);

            Guid bucketId = Guid.NewGuid();

            fakeLayer.DataLayer.Setup(new ContentBucket
                                      {
                                          Id = bucketId,
                                          Type = ContentType.Article,
                                          Status = ObjectStatus.Active,
                                          Slug = "bucket-slug"
                                      });

            ContentLogic logic = new ContentLogic();

            NewContentRequest request = new NewContentRequest
                                         {
                                             Segments = new List<ContentSegmentRequest>
											            {
												            new ContentSegmentRequest
												            {
															     Body = "The body",       
												            }
											            },
                                             Title = "The title",
                                             Slug = "the-slug",
                                             AgeCategories = new List<AgeCategory>
											                 {
												                 AgeCategory.Infant
											                 },
                                             Gender = Gender.All,
                                             OnlineOriginatingSources = new List<OnlineOriginatingSource>
											                            {
												                            new OnlineOriginatingSource
												                            {
													                            Date = DateTime.UtcNow,
																				Title = "source title",
																				Uri = "http://tempuri.com"
												                            }
											                            },
                                             RecommendedSites = new List<RecommendedSite>
											                    {
												                    new RecommendedSite
												                    {
													                    Title = "site title",
																		Uri = "http://tempuri.com"
												                    }
											                    },
                                             Taxonomies = new List<ContentTaxonomyListRequest>
                                                        {
                                                            new ContentTaxonomyListRequest
                                                            { 
                                                                Slug = "icd-9",
                                                                Items = new List<ContentTaxonomyValue>
                                                                {
                                                                        new ContentTaxonomyValue
                                                                        {
                                                                            Value   = "162.4",
                                                                            Source = TaxonomyValueSource.Ksw
                                                                        }
                                                                }
                                                            }
                                                        }
                                         };

            ContentMetadataResponse response = logic.CreateContent(bucketId.ToString(), request);

            FakeDataSet<ContentVersion> metadatas = fakeLayer.DataLayer.GetFakeSet<ContentVersion>();
            FakeDataSet<ContentSegment> segments = fakeLayer.DataLayer.GetFakeSet<ContentSegment>();

            Assert.IsNotNull(response);

            Assert.AreEqual(1, metadatas.Count);
            Assert.AreEqual(1, segments.Count);

            Assert.AreEqual("The title", metadatas[0].Title);

            Assert.IsNotNull(metadatas[0].Segments);
            Assert.AreEqual(1, metadatas[0].Segments.Count);

            Assert.AreEqual("The body", segments[0].Body);
            Assert.AreEqual(metadatas[0].Segments[0].Id, segments[0].Id);
            Assert.IsNotNull(metadatas[0].Paths);
            Assert.AreEqual(4, metadatas[0].Paths.Count);

            Assert.AreEqual("bucket-slug", metadatas[0].Paths[0].BucketIdOrSlug);
            Assert.AreEqual("the-slug", metadatas[0].Paths[0].ContentIdOrSlug);
            Assert.AreEqual(bucketId.ToString(), metadatas[0].Paths[1].BucketIdOrSlug);
            Assert.AreEqual("the-slug", metadatas[0].Paths[1].ContentIdOrSlug);

            Assert.AreEqual("bucket-slug", metadatas[0].Paths[2].BucketIdOrSlug);
            Assert.AreEqual(response.Id.ToString(), metadatas[0].Paths[2].ContentIdOrSlug);
            Assert.AreEqual(bucketId.ToString(), metadatas[0].Paths[3].BucketIdOrSlug);
            Assert.AreEqual(response.Id.ToString(), metadatas[0].Paths[3].ContentIdOrSlug);
        }

        [TestMethod]
        public void CreateContent_NewLanguageVersion_CreatesContent()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            FakeContent content = ContentGenerator.GenerateContent(fakeLayer);
            
            ContentLogic logic = new ContentLogic();

            NewContentRequest request = new NewContentRequest
            {
                MasterId = content.Version.Id.ToString(),
                Segments = new List<ContentSegmentRequest>
											            {
												            new ContentSegmentRequest
												            {
																IdOrSlug = content.Bucket.Segments[0].Id.ToString(),
															    Body = "The language body",   
												            }
											            },
                Title = "The language title",
                InvertedTitle = "Inverted, language title",
                LanguageCode = "fa",
                Slug = "the-language-slug",
                AgeCategories = new List<AgeCategory>
											                 {
												                 AgeCategory.Infant
											                 },
                Gender = Gender.All,
                OnlineOriginatingSources = new List<OnlineOriginatingSource>
											                            {
												                            new OnlineOriginatingSource
												                            {
													                            Date = DateTime.UtcNow,
																				Title = "source title",
																				Uri = "http://tempuri.com"
												                            }
											                            },
                RecommendedSites = new List<RecommendedSite>
											                    {
												                    new RecommendedSite
												                    {
													                    Title = "site title",
																		Uri = "http://tempuri.com"
												                    }
											                    },
                Taxonomies = content.Core.Taxonomies.Select(kvp => new ContentTaxonomyListRequest
                                                                       {
                                                                        Slug   = kvp.Key,
                                                                        Items = kvp.Value.Select(x => new ContentTaxonomyValue
                                                                                                                   {
                                                                                                                       Value = x.Value,
                                                                                                                       Source = x.Source
                                                                                                                   }).ToList()
                                                                       }).ToList()
            };
            

            ContentMetadataResponse response = logic.CreateContent(content.Bucket.Slug, request);

            FakeDataSet<ContentVersion> metadatas = fakeLayer.DataLayer.GetFakeSet<ContentVersion>();
            FakeDataSet<ContentSegment> segments = fakeLayer.DataLayer.GetFakeSet<ContentSegment>();

            Assert.AreEqual(2, metadatas.Count);
            Assert.AreEqual(2, segments.Count);

            Assert.AreEqual("The language title", metadatas[1].Title);

            Assert.IsNotNull(metadatas[1].Segments);
            Assert.AreEqual(1, metadatas[1].Segments.Count);

            Assert.IsNotNull(response.Language);
            Assert.AreEqual(response.Language.Code, "fa");
            Assert.AreEqual(response.Language.Direction, LanguageDirection.RightToLeft);

            Assert.AreEqual("The language body", segments[1].Body);
            Assert.AreEqual(metadatas[1].Segments[0].Id, segments[1].Id);
        }

        [TestMethod]
        public void UpdateContent_NotImplementingRequiredSegment_ThrowsException()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            FakeContent content = ContentGenerator.GenerateContent(fakeLayer);

            ContentLogic logic = new ContentLogic();

            NewContentRequest request = new NewContentRequest
            {
                Segments = new List<ContentSegmentRequest> { new ContentSegmentRequest { Body = "The updated body" } },
                Title = "The updated title",
                AgeCategories = new List<AgeCategory>
											                 {
												                 AgeCategory.Infant
											                 },

                Gender = Gender.All
            };

            ServiceException exception = Expect.Exception<ServiceException>(() => logic.UpdateContent(content.Bucket.Slug, content.Version.Id.ToString(), request));

            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
        }

        [TestMethod]
        public void UpdateContent_ThirdPartyTaxonomyValueRemoved_ThrowsException()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            FakeContent content = ContentGenerator.GenerateContent(fakeLayer);

            ContentLogic logic = new ContentLogic();

            ContentArticleRequest request = new ContentArticleRequest
                                                {
                                                    Segments = new List<ContentSegmentRequest>
                                                                   {
                                                                       new ContentSegmentRequest
                                                                           {IdOrSlug = content.Segment.Id.ToString(), Body = "The updated body"}
                                                                   },
                                                    Title = "The updated title",
                                                    AgeCategories = new List<AgeCategory>
                                                                        {
                                                                            AgeCategory.Infant
                                                                        },

                                                    Gender = Gender.All,
                                                    Taxonomies = new List<ContentTaxonomyListRequest>
                                                                     {
                                                                         new ContentTaxonomyListRequest
                                                                             {
                                                                                 Slug = "icd-9",
                                                                                 Items =
                                                                                     new List<ContentTaxonomyValue>
                                                                                         {

                                                                                             new ContentTaxonomyValue
                                                                                                 {
                                                                                                     Value = "402.4",
                                                                                                     Source = TaxonomyValueSource.Ksw
                                                                                                 },
                                                                                             new ContentTaxonomyValue
                                                                                                 {
                                                                                                     Value = "462.4",
                                                                                                     Source = TaxonomyValueSource.Ksw
                                                                                                 }
                                                                                         }
                                                                             }
                                                                     }
                                                };

            ServiceException exception =
                Expect.Exception<ServiceException>(() => logic.UpdateContent(content.Bucket.Slug, content.Version.Id.ToString(), request));
            Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
        }

        //UpdateTaxonomiesBySource
        [TestMethod]
        public void UpdateTaxonomiesBySource_UpdatesTaxonomies()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            FakeContent content = ContentGenerator.GenerateContent(fakeLayer);

            var oldTaxonomy = content.Core.Taxonomies.ToList();

            ContentLogic logic = new ContentLogic();

            const TaxonomyValueSource source = TaxonomyValueSource.ThreeM;
            
            var taxonomiesRequest = new List<ContentTaxonomyListRequest>
                                        {
                                            new ContentTaxonomyListRequest
                                                {
                                                    Slug = "icd-9",
                                                    Items = new List<ContentTaxonomyValue>
                                                                         {
                                                                             new ContentTaxonomyValue
                                                                                 {
                                                                                     Value = "852.4",
                                                                                     Source = source
                                                                                 },
                                                                             new ContentTaxonomyValue
                                                                                 {
                                                                                     Value = "162.4",
                                                                                     Source = source
                                                                                 },
                                                                             new ContentTaxonomyValue
                                                                                 {
                                                                                     Value = "999.4",
                                                                                     Source = source
                                                                                 }
                                                                         }
                                                }
                                        };
            logic.UpdateTaxonomies(content.Bucket.Id.ToString(), content.Version.Id.ToString(), source, taxonomiesRequest);

            FakeDataSet<ContentCore> core = fakeLayer.DataLayer.GetFakeSet<ContentCore>();

            // test source specific taxonomies are the same
            var expectedSourceTaxonomies = taxonomiesRequest.SelectMany(x => x.Items.Select(y => new { x.Slug, y.Value, y.Source })).ToList();
            var actualSourceTaxonomies = core.Single().Taxonomies.SelectMany(x => x.Value.Select(y => new { Slug = x.Key, y.Value, y.Source })).Where(z => z.Source == source).ToList();
            Assert.IsFalse(actualSourceTaxonomies.Union(expectedSourceTaxonomies).Except(actualSourceTaxonomies.Intersect(expectedSourceTaxonomies)).Any());
            
            // test that taxonomies from other sources unchanged
            var expectedTaxonomiesFromOtherSources = oldTaxonomy.SelectMany(x => x.Value.Select(y => new { Slug = x.Key, y.Value, y.Source })).Where(z => z.Source != source).ToList();
            var actualTaxonomiesFromOtherSources = core.Single().Taxonomies.SelectMany(x => x.Value.Select(y => new { Slug = x.Key, y.Value, y.Source })).Where(z => z.Source != source).ToList();
            Assert.IsFalse(actualTaxonomiesFromOtherSources.Union(expectedTaxonomiesFromOtherSources).Except(actualTaxonomiesFromOtherSources.Intersect(expectedTaxonomiesFromOtherSources)).Any());
        }

        [TestMethod]
        public void GetLanguages_GetsLanguages()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            FakeContent content = ContentGenerator.GenerateContent(fakeLayer);

            content.Core.Versions.Add(new ContentVersionReference { Id = Guid.NewGuid(), Language = "fa", Slug = "l-slug" });

            ContentLogic logic = new ContentLogic();

            LanguageList fullList = logic.GetLanguages(null, null);

            LanguageList newList = logic.GetLanguages(content.Bucket.Slug, content.Version.Slug);

            Assert.IsNotNull(fullList);
            Assert.IsNotNull(newList);

            Assert.AreEqual(184, fullList.Items.Count);
            Assert.AreEqual(182, newList.Items.Count);

            Assert.IsTrue(fullList.Items.Any(item => item.Code == "en"));
            Assert.IsTrue(fullList.Items.Any(item => item.Code == "fa"));

            Assert.IsFalse(newList.Items.Any(item => item.Code == "en"));
            Assert.IsFalse(newList.Items.Any(item => item.Code == "fa"));
        }

        [TestMethod]
        public void UpdateContent_UpdatesContent()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            FakeContent content = ContentGenerator.GenerateContent(fakeLayer);

            ContentLogic logic = new ContentLogic();

            ContentArticleRequest request = new ContentArticleRequest
            {
                Segments = new List<ContentSegmentRequest> { new ContentSegmentRequest { IdOrSlug = content.Segment.Id.ToString(), Body = "The updated body" } },
                Title = "The updated title",
                AgeCategories = new List<AgeCategory>
											                 {
												                 AgeCategory.Infant
											                 },

                Gender = Gender.All,
                Taxonomies = new List<ContentTaxonomyListRequest>
                                 {
                                    new ContentTaxonomyListRequest
                                    { 
                                        Slug = "icd-9",
                                        Items = new List<ContentTaxonomyValue>
                                        {
                                            new ContentTaxonomyValue
                                            {
                                                Value = "162.4", 
                                                Source = TaxonomyValueSource.Ksw
                                            },
                                            new ContentTaxonomyValue
                                            {
                                                Value = "678.9", 
                                                Source = TaxonomyValueSource.ThirdParty
                                            },
                                            new ContentTaxonomyValue
                                            {
                                                Value = "555.4", 
                                                Source = TaxonomyValueSource.Ksw
                                            },
                                            new ContentTaxonomyValue
                                            {
                                                Value = "462.4", 
                                                Source = TaxonomyValueSource.ThreeM
                                            },
                                            new ContentTaxonomyValue
                                            {
                                                Value = "852.4", 
                                                Source = TaxonomyValueSource.ThreeM
                                            }
                                        }
                                    }
                                 }
            };

            logic.UpdateContent(content.Bucket.Slug, content.Version.Id.ToString(), request);

            FakeDataSet<ContentVersion> metadatas = fakeLayer.DataLayer.GetFakeSet<ContentVersion>();
            FakeDataSet<ContentSegment> segments = fakeLayer.DataLayer.GetFakeSet<ContentSegment>();
            FakeDataSet<ContentCore> core = fakeLayer.DataLayer.GetFakeSet<ContentCore>();
            
            var expectedTaxonomies = request.Taxonomies.SelectMany(x => x.Items.Select(y => new {x.Slug, y.Value, y.Source})).ToList();
            var actualTaxonomies = core.Single().Taxonomies.SelectMany(x => x.Value.Select(y => new { Slug = x.Key, y.Value, y.Source })).ToList();
            Assert.IsFalse(actualTaxonomies.Union(expectedTaxonomies).Except(actualTaxonomies.Intersect(expectedTaxonomies)).Any());

            Assert.AreEqual(1, metadatas.Count);
            Assert.AreEqual(2, segments.Count);

            Assert.AreEqual("The updated title", metadatas[0].Title);
            Assert.AreEqual("the-slug", metadatas[0].Slug);

            Assert.IsNotNull(metadatas[0].Segments);
            Assert.AreEqual(1, metadatas[0].Segments.Count);

            Assert.AreEqual("The updated body", segments[1].Body);
            Assert.AreEqual(metadatas[0].Segments[0].Id, segments[1].Id);
        }

        [TestMethod]
        public void DeleteContent_DeletesContent()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();
            FakeLayerDataInitialization.Init(fakeLayer);

            Guid contentId = Guid.NewGuid();
            Guid segmentId = Guid.NewGuid();

            Guid bucketId = Guid.NewGuid();
            Guid coreId = Guid.NewGuid();



            fakeLayer.DataLayer.Setup(new ContentBucket
            {
                Id = bucketId,
                Type = ContentType.Article
            });


            fakeLayer.DataLayer.Setup(new ContentVersion
            {
                Id = contentId,
                Title = "The title",
                Status = ObjectStatus.Active,
                Segments = new List<ContentSegmentItem>
						   {
							   new ContentSegmentItem
								   {
									   Id = segmentId
								   }
						   },
                CoreId = coreId,
                IsMaster = true,
                Paths = new List<ContentPath> { new ContentPath { BucketIdOrSlug = bucketId.ToString(), ContentIdOrSlug = contentId.ToString() } }
            });

            fakeLayer.DataLayer.Setup(new ContentCore
            {
                Id = coreId,
                Versions = new List<ContentVersionReference>
				           {
					           new ContentVersionReference
					           {
						           Id = contentId
					           }
				           },
                Taxonomies = new Dictionary<string, List<TaxonomyValue>>
                                 {
                                     {
                                         "icd-9", 
                                         new List<TaxonomyValue>
                                             {
                                                    new TaxonomyValue
                                                        {
                                                            Value = "162.4", 
                                                            Source = TaxonomyValueSource.Ksw
                                                        }
                                             }
                                     }
                                     
                                 }
            });

            fakeLayer.DataLayer.Setup(new ContentSegment
            {
                Id = segmentId,
                ContentId = contentId,
                Body = "The body"
            });

            ContentLogic logic = new ContentLogic();

            ContentResponse response = logic.DeleteContent(bucketId.ToString(), contentId.ToString(), false);

            FakeDataSet<ContentVersion> metadatas = fakeLayer.DataLayer.GetFakeSet<ContentVersion>();
            FakeDataSet<ContentSegment> segments = fakeLayer.DataLayer.GetFakeSet<ContentSegment>();

            Assert.AreEqual(1, metadatas.Count);
            Assert.AreEqual(1, segments.Count);
            Assert.AreEqual(ObjectStatus.Deleted, metadatas[0].Status);
            Assert.IsNotNull(response);
            Assert.AreEqual("The title", response.Title);
        }

        [TestMethod]
        public void GetContent_With_Copyright_Year_Template_Applied()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            FakeContent content = ContentGenerator.GenerateContent(fakeLayer);

            ContentLogic logic = new ContentLogic();

            ContentArticleResponse response = logic.GetContent(content.Bucket.Slug, content.Version.Id.ToString(), true, null, true);

            Assert.AreNotEqual(content.Copyright.Value, response.Copyright);
            Assert.IsFalse(response.Copyright.Contains("{{year}}"));
            Assert.AreEqual(content.Copyright.Id, response.CopyrightId);
            Assert.IsNotNull(response);
            Assert.AreEqual("The title", response.Title);
            Assert.IsNotNull(response.Segments);
            Assert.AreEqual(1, response.Segments.Count);
            Assert.AreEqual("The body", response.Segments[0].Body);
        }

        [TestMethod]
        public void GetContent_With_Content_OverriddenCopyright()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            FakeContent content = ContentGenerator.GenerateContent(fakeLayer);

            Copyright copyright = FakeCopyright();

            fakeLayer.DataLayer.AddItem(copyright);

            content.Core.CopyrightId = copyright.Id;

            ContentLogic logic = new ContentLogic();

			ContentArticleResponse response = logic.GetContent(content.Bucket.Slug, content.Version.Id.ToString(), true, null, true);

            Assert.AreEqual(copyright.Value, response.Copyright);
            Assert.AreEqual(copyright.Id, response.CopyrightId);
            Assert.IsNotNull(response);
            Assert.AreEqual("The title", response.Title);
            Assert.IsNotNull(response.Segments);
            Assert.AreEqual(1, response.Segments.Count);
            Assert.AreEqual("The body", response.Segments[0].Body);
        }

        [TestMethod]
        public void GetContent_With_Null_BucketCopyright_And_Null_ContentCopyright()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            FakeContent content = ContentGenerator.GenerateContent(fakeLayer);

            content.Bucket.CopyrightId = null;
            content.Core.CopyrightId = null;

            ContentLogic logic = new ContentLogic();

			ContentArticleResponse response = logic.GetContent(content.Bucket.Slug, content.Version.Id.ToString(), true, null, true);

            Assert.IsNull(response.CopyrightId);
            Assert.IsNotNull(response);
            Assert.AreEqual("The title", response.Title);
            Assert.IsNotNull(response.Segments);
            Assert.AreEqual(1, response.Segments.Count);
            Assert.AreEqual("The body", response.Segments[0].Body);
        }

        [TestMethod]
        public void GetContent_With_Bucket_OverridenCopyright()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            FakeContent content = ContentGenerator.GenerateContent(fakeLayer);
            
            Copyright copyright = FakeCopyright();

            fakeLayer.DataLayer.AddItem(copyright);

            content.Core.CopyrightId = null;
            content.Bucket.CopyrightId = copyright.Id;

            ContentLogic logic = new ContentLogic();

			ContentArticleResponse response = logic.GetContent(content.Bucket.Slug, content.Version.Id.ToString(), true, null, true);

            Assert.AreEqual(copyright.Value, response.Copyright);
            Assert.IsNull(response.CopyrightId);
            Assert.IsNotNull(response);
            Assert.AreEqual("The title", response.Title);
            Assert.IsNotNull(response.Segments);
            Assert.AreEqual(1, response.Segments.Count);
            Assert.AreEqual("The body", response.Segments[0].Body);
        }

        [TestMethod]
        public void GetContent_List_GetsContent()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            FakeContent content = ContentGenerator.GenerateContent(fakeLayer);

            fakeLayer.MockIndex.Setup(
                item => item.Search(It.IsAny<int>(), It.IsAny<int>(), It.IsAny<SearchQuery>(), null, null, It.IsAny<List<SearchSort>>(), IndexType.Content))
                .Returns(new SearchResponse
                             {
                                 Hits = new SearchListResponse
                                            {
                                                Total = 1,
                                                Items = new List<SearchItemResponse>
													        {
														        new SearchItemResponse
															        {
																		Id = content.Version.Id,
																		Type = IndexType.Content
																	}
													        }
                                            }
                             });

            ContentLogic logic = new ContentLogic();

	        ContentSearchRequest request = new ContentSearchRequest
	                                       {
		                                       Offset = 0,
		                                       Count = 10,
											   IncludeDrafts = true
	                                       };

            ContentList response = logic.SearchContent(request, null);

            Assert.IsNotNull(response);
            Assert.AreEqual(1, response.Total);
            Assert.IsNotNull(response.Items);
            Assert.AreEqual(1, response.Items.Count);
            Assert.AreEqual(content.Version.Id, response.Items[0].Id);
            Assert.AreEqual("The title", response.Items[0].Title);
        }

        [TestMethod]
        public void GetNewSlug_ReturnsSlug()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            AddContent(fakeLayer, "bucket-slug", "slug", 23);

            ContentLogic logic = new ContentLogic();

            SlugResponse response = logic.GetSlug("bucket-slug", "slug", null);

            Assert.IsNotNull(response);
            Assert.AreEqual("slug-23", response.Value);
        }

		[TestMethod]
		public void AddHistoryContent_AddsHistory()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			FakeContent content = ContentGenerator.GenerateContent(fakeLayer);

			ContentLogic logic = new ContentLogic();

			ContentArticleRequest request = new ContentArticleRequest
			{
				Segments = new List<ContentSegmentRequest> { new ContentSegmentRequest { IdOrSlug = content.Segment.Id.ToString(), Body = "The updated body" } },
				Title = "The updated title",
				AgeCategories = new List<AgeCategory>
											                 {
												                 AgeCategory.Infant
											                 },

				Gender = Gender.All,
				Taxonomies = new List<ContentTaxonomyListRequest>
                                 {
                                    new ContentTaxonomyListRequest
                                    { 
                                        Slug = "icd-9",
                                        Items = new List<ContentTaxonomyValue>
                                        {
                                            new ContentTaxonomyValue
                                            {
                                                Value = "162.4", 
                                                Source = TaxonomyValueSource.Ksw
                                            },
                                            new ContentTaxonomyValue
                                            {
                                                Value = "678.9", 
                                                Source = TaxonomyValueSource.ThirdParty
                                            },
                                            new ContentTaxonomyValue
                                            {
                                                Value = "555.4", 
                                                Source = TaxonomyValueSource.Ksw
                                            },
                                            new ContentTaxonomyValue
                                            {
                                                Value = "462.4", 
                                                Source = TaxonomyValueSource.ThreeM
                                            },
                                            new ContentTaxonomyValue
                                            {
                                                Value = "852.4", 
                                                Source = TaxonomyValueSource.ThreeM
                                            }
                                        }
                                    }
                                 }
			};

			logic.AddAutosaveHistoryContent(content.Bucket.Slug, content.Version.Id.ToString(), request);

			FakeDataSet<ContentVersion> metadatas = fakeLayer.DataLayer.GetFakeSet<ContentVersion>();
			FakeDataSet<ContentSegment> segments = fakeLayer.DataLayer.GetFakeSet<ContentSegment>();
			FakeDataSet<ContentCore> core = fakeLayer.DataLayer.GetFakeSet<ContentCore>();
			FakeDataSet<ContentHistory> history = fakeLayer.DataLayer.GetFakeSet<ContentHistory>();
			FakeDataSet<ContentRevision> revisions = fakeLayer.DataLayer.GetFakeSet<ContentRevision>();

			var expectedTaxonomies = request.Taxonomies.SelectMany(x => x.Items.Select(y => new { x.Slug, y.Value, y.Source })).ToList();
			var actualTaxonomies = core.Single().Taxonomies.SelectMany(x => x.Value.Select(y => new { Slug = x.Key, y.Value, y.Source })).ToList();
			Assert.IsFalse(actualTaxonomies.Union(expectedTaxonomies).Except(actualTaxonomies.Intersect(expectedTaxonomies)).Any());

			Assert.AreEqual(1, metadatas.Count);
			Assert.AreEqual(2, segments.Count);
			Assert.AreEqual(1, history.Count);
			Assert.AreEqual(1, revisions.Count);
			Assert.IsNotNull(revisions[0].Version);
			Assert.IsNotNull(revisions[0].Core);

			Assert.AreEqual("The updated title", revisions[0].Version.Title);
			Assert.AreEqual("the-slug", revisions[0].Version.Slug);

			Assert.IsNotNull(revisions[0].Version.Segments);
			Assert.AreEqual(1, revisions[0].Version.Segments.Count);

			Assert.AreEqual("The updated body", segments[1].Body);
			Assert.AreEqual(segments[1].ContentId, content.Version.Id);
			Assert.AreEqual(revisions[0].Version.Segments[0].SegmentId, segments[0].Id);
			Assert.AreEqual(segments[1].Id, revisions[0].Version.Segments[0].Id);
		}

		[TestMethod]
		public void CreateContent_NoCustomAttributes_StoresAsExpected()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();
			FakeLayerDataInitialization.Init(fakeLayer);

			Guid bucketId = Guid.NewGuid();

			fakeLayer.DataLayer.Setup(new ContentBucket
			{
				Id = bucketId,
				Type = ContentType.Article,
				Status = ObjectStatus.Active,
				Slug = "bucket-slug"
			});

			ContentLogic logic = new ContentLogic();

			NewContentRequest request = new NewContentRequest
			{
				Title = "The title",
				Slug = "the-slug",
				AgeCategories = new List<AgeCategory> { AgeCategory.Infant },
				Gender = Gender.All
			};

			ContentMetadataResponse response = logic.CreateContent(bucketId.ToString(), request);

			FakeDataSet<ContentCore> core = fakeLayer.DataLayer.GetFakeSet<ContentCore>();

			Assert.IsNotNull(response);

			Assert.AreEqual(1, core.Count);

			Assert.IsNull(core[0].CustomAttributes);
		}

		[TestMethod]
		public void CreateContent_WithCustomAttributes_StoresAsExpected()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();
			FakeLayerDataInitialization.Init(fakeLayer);

			Guid bucketId = Guid.NewGuid();

			fakeLayer.DataLayer.Setup(new ContentBucket
			{
				Id = bucketId,
				Type = ContentType.Article,
				Status = ObjectStatus.Active,
				Slug = "bucket-slug"
			});

			ContentLogic logic = new ContentLogic();

			NewContentRequest request = new NewContentRequest
			{
				Title = "The title",
				Slug = "the-slug",
				AgeCategories = new List<AgeCategory> { AgeCategory.Infant },
				Gender = Gender.All,
				CustomAttributes = new List<CustomAttribute>
				{
					new CustomAttribute {
						Name = "Name",
						CustomAttributeValues = new List<CustomAttributeContent>
						{
							new CustomAttributeContent { Values = "Value" }
						}
					},
				}
			};

			ContentMetadataResponse response = logic.CreateContent(bucketId.ToString(), request);

			FakeDataSet<ContentCore> core = fakeLayer.DataLayer.GetFakeSet<ContentCore>();

			Assert.IsNotNull(response);

			Assert.AreEqual(1, core.Count);

			Assert.IsNotNull(core[0].CustomAttributes);
			Assert.AreEqual(1, core[0].CustomAttributes.Count);
			Assert.AreEqual("Name", core[0].CustomAttributes.Keys.First());

			List<string> values = core[0].CustomAttributes.Values.First();
			Assert.IsNotNull(values);
			Assert.AreEqual(1, values.Count);
			Assert.AreEqual("Value", values[0]);
		}

		[TestMethod]
		public void CreateContent_CustomAttributesHaveDuplicateNames_ThrowsExpectedException()
		{
			// arrange
			FakeLayer fakeLayer = FakeLayer.Setup();
			FakeLayerDataInitialization.Init(fakeLayer);

			Guid bucketId = Guid.NewGuid();

			fakeLayer.DataLayer.Setup(new ContentBucket
			{
				Id = bucketId,
				Type = ContentType.Article,
				Status = ObjectStatus.Active,
				Slug = "bucket-slug"
			});

			ContentLogic logic = new ContentLogic();

			NewContentRequest request = new NewContentRequest
			{
				Title = "The title",
				Slug = "the-slug",
				AgeCategories = new List<AgeCategory> { AgeCategory.Infant },
				Gender = Gender.All,
				CustomAttributes = new List<CustomAttribute>
				{
					new CustomAttribute {
						Name = "DuplicateName",
						CustomAttributeValues = new List<CustomAttributeContent>
						{
							new CustomAttributeContent { Values = "Value" }
						}
					},
					new CustomAttribute {
						Name = "DuplicateName",
						CustomAttributeValues = new List<CustomAttributeContent>
						{
							new CustomAttributeContent { Values = "Value" }
						}
					},
				}
			};

			// act / assert
			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Content.DUPLICATE_CUSTOM_ATTRIBUTE_NAME,
				typeof(ServiceException),
				() => logic.CreateContent(bucketId.ToString(), request));
		}

		[TestMethod]
		public void CreateContent_CustomAttributeHasMissingValue_ThrowsExpectedException()
		{
			// arrange
			FakeLayer fakeLayer = FakeLayer.Setup();
			FakeLayerDataInitialization.Init(fakeLayer);

			Guid bucketId = Guid.NewGuid();

			fakeLayer.DataLayer.Setup(new ContentBucket
			{
				Id = bucketId,
				Type = ContentType.Article,
				Status = ObjectStatus.Active,
				Slug = "bucket-slug"
			});

			ContentLogic logic = new ContentLogic();

			NewContentRequest request = new NewContentRequest
			{
				Title = "The title",
				Slug = "the-slug",
				AgeCategories = new List<AgeCategory> { AgeCategory.Infant },
				Gender = Gender.All,
				CustomAttributes = new List<CustomAttribute>
				{
					new CustomAttribute { Name = "InvalidName" }
				}
			};

			// act / assert
			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Content.CUSTOM_ATTRIBUTE_MUST_HAVE_VALUE,
				typeof(ServiceException),
				() => logic.CreateContent(bucketId.ToString(), request));
		}

		[TestMethod]
		public void CreateContent_CustomAttributeHasEmptyValue_ThrowsExpectedException()
		{
			// arrange
			FakeLayer fakeLayer = FakeLayer.Setup();
			FakeLayerDataInitialization.Init(fakeLayer);

			Guid bucketId = Guid.NewGuid();

			fakeLayer.DataLayer.Setup(new ContentBucket
			{
				Id = bucketId,
				Type = ContentType.Article,
				Status = ObjectStatus.Active,
				Slug = "bucket-slug"
			});

			ContentLogic logic = new ContentLogic();

			NewContentRequest request = new NewContentRequest
			{
				Title = "The title",
				Slug = "the-slug",
				AgeCategories = new List<AgeCategory> { AgeCategory.Infant },
				Gender = Gender.All,
				CustomAttributes = new List<CustomAttribute>
				{
					new CustomAttribute
					{
						Name = "Name",
						CustomAttributeValues = new List<CustomAttributeContent>
						{
							new CustomAttributeContent { Values = string.Empty }
						}
					}
				}
			};

			// act / assert
			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Content.INVALID_CUSTOM_ATTRIBUTE_VALUE,
				typeof(ServiceException),
				() => logic.CreateContent(bucketId.ToString(), request));
		}

		[TestMethod]
		public void CreateContent_CustomAttributeHasInvalidName_ThrowsExpectedException()
		{
			// arrange
			FakeLayer fakeLayer = FakeLayer.Setup();
			FakeLayerDataInitialization.Init(fakeLayer);

			Guid bucketId = Guid.NewGuid();

			fakeLayer.DataLayer.Setup(new ContentBucket
			{
				Id = bucketId,
				Type = ContentType.Article,
				Status = ObjectStatus.Active,
				Slug = "bucket-slug"
			});

			ContentLogic logic = new ContentLogic();

			NewContentRequest request = new NewContentRequest
			{
				Title = "The title",
				Slug = "the-slug",
				AgeCategories = new List<AgeCategory> { AgeCategory.Infant },
				Gender = Gender.All,
				CustomAttributes = new List<CustomAttribute>
				{
					new CustomAttribute
					{
						Name = "Invalid_Name",
						CustomAttributeValues = new List<CustomAttributeContent>
						{
							new CustomAttributeContent { Values = "value" }
						}
					}
				}
			};

			// act / assert
			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Content.INVALID_CUSTOM_ATTRIBUTE_NAME,
				typeof(ServiceException),
				() => logic.CreateContent(bucketId.ToString(), request));
		}

		[TestMethod]
		public void GetContent_NoCustomAttributes_ReturnsNullInResponse()
		{
			// arrange
			FakeLayer fakeLayer = FakeLayer.Setup();
			FakeContent content = ContentGenerator.GenerateContent(fakeLayer);
			ContentLogic logic = new ContentLogic();

			// act
			ContentArticleResponse response = logic.GetContent(content.Bucket.Slug, content.Version.Id.ToString(), true, null, true);

			// assert
			Assert.IsNull(response.CustomAttributes);
		}

		[TestMethod]
		public void GetContent_WithCustomAttributes_ReturnsThemInResponse()
		{
			// arrange
			FakeLayer fakeLayer = FakeLayer.Setup();
			FakeContent content = ContentGenerator.GenerateContent(fakeLayer);
			content.Core.CustomAttributes = new Dictionary<string, List<string>>
			{
				{ "Name", new List<string> { "Value" } }
			};
			ContentLogic logic = new ContentLogic();

			// act
			ContentArticleResponse response = logic.GetContent(content.Bucket.Slug, content.Version.Id.ToString(), true, null, true);

			// assert
			Assert.IsNotNull(response.CustomAttributes);
			Assert.AreEqual(1, response.CustomAttributes.Count);
			Assert.AreEqual("Name", response.CustomAttributes[0].Name);
			Assert.IsNotNull(response.CustomAttributes[0].CustomAttributeValues);
			Assert.AreEqual(1, response.CustomAttributes[0].CustomAttributeValues.Count);
			Assert.IsNotNull(response.CustomAttributes[0].CustomAttributeValues[0]);
			Assert.AreEqual("Value", response.CustomAttributes[0].CustomAttributeValues[0].Values);
		}

        #region Private Helper Methods

        private Copyright FakeCopyright()
        {
            return new Copyright
                       {
                           DateAdded = DateTime.UtcNow,
                           DateModified = DateTime.UtcNow,
                           Id = Guid.Parse(FAKE_COPYRIGHT_ID),
                           Value = "test copyright"
                       };
        }

        private void FakeSetup(out ContentBucket contentBucket, out ContentVersion contentMetaData, out ContentSegment contentSegment)//, Guid contentId, Guid segmentId, Guid bucketId)
        {
            Guid contentId = Guid.NewGuid();
            Guid segmentId = Guid.NewGuid();

            contentBucket = new ContentBucket
                                {
                                    Id = Guid.NewGuid(),
                                    Type = ContentType.Article
                                };

            contentMetaData = new ContentVersion
                                  {
                                      Id = contentId,
                                      BucketId = contentBucket.Id,
                                      Title = "The title",
                                      Slug = "the-slug",
                                      Status = ObjectStatus.Active,
                                      Segments = new List<ContentSegmentItem>
						                             {
							                             new ContentSegmentItem
								                             {
									                             Id = segmentId
								                             }
						                             }
                                  };

            contentSegment = new ContentSegment
                                 {
                                     Id = segmentId,
                                     ContentId = contentId,
                                     Body = "The body"
                                 };
        }

        private void AddContent(FakeLayer fakeLayer, string bucketSlug, string slugPrefix, int count)
        {
            Guid bucketId = Guid.NewGuid();

            fakeLayer.DataLayer.Setup(new ContentBucket
                                      {
                                          Id = bucketId,
                                          Name = "Bucket Name",
                                          Slug = bucketSlug
                                      });

            for (int i = 0; i < count; i++)
            {
                string currentSlug = slugPrefix + (i == 0 ? "" : "-" + i.ToString());
                fakeLayer.DataLayer.AddItem(new ContentVersion
                                        {
                                            Id = Guid.NewGuid(),
                                            BucketId = bucketId,
                                            Slug = currentSlug,
                                            Status = ObjectStatus.Active,
                                            Paths = new List<ContentPath>
											        {
												        new ContentPath {BucketIdOrSlug = bucketSlug, ContentIdOrSlug = currentSlug}
											        }
                                        });
            }
        }

        #endregion
    }
}

