﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.ContentLogic;
using KswApi.Poco.Content;
using KswApi.Test.Framework;
using KswApi.Test.Framework.DataGenerators;
using KswApi.Test.Framework.Fakes;
using KswApi.Test.Framework.Objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable InconsistentNaming
namespace KswApi.Test.Content
{
	[TestClass]
	public class ContentBucketLogicTests
	{
		[TestMethod]
		public void CreateBucket_Succeeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			Guid originId = Guid.NewGuid();
			fake.DataLayer.Setup(new ContentOrigin { Id = originId, Name = "Origin Name" });

			BucketLogic logic = new BucketLogic();

			ContentBucketCreateRequest request = new ContentBucketCreateRequest
										   {
											   Type = ContentType.Article,
											   LegacyId = 5,
											   Name = "My Name",
											   ReadOnly = true,
											   Slug = "a-valid-slug",
											   OriginId = originId,
											   Segments = new List<ContentBucketSegmentRequest>
				                                          {
					                                          new ContentBucketSegmentRequest
					                                          {
						                                          Required = true,
						                                          Name = "Segment Name",
																  Slug = "segment-slug"
					                                          }
				                                          },
											   Constraints =
												   new ContentBucketConstraint
												   {
													   Length = 21
												   }
										   };

			ContentBucketResponse response = logic.CreateBucket(request);


			FakeDataSet<ContentBucket> set = fake.DataLayer.GetFakeSet<ContentBucket>();

			// validate the db
			Assert.AreEqual(1, set.Count);

			ContentBucket bucket = set.First();
			Assert.AreNotEqual(Guid.Empty, bucket.Id);
			Assert.AreEqual(ContentType.Article, bucket.Type);
			Assert.AreEqual(5, bucket.LegacyId);
			Assert.AreEqual("My Name", bucket.Name);
			Assert.AreEqual(true, bucket.ReadOnly);
			Assert.AreEqual(ObjectStatus.Active, bucket.Status);
			Assert.AreEqual(originId, bucket.OriginId);

			Assert.IsNotNull(bucket.Segments);
			Assert.AreEqual(1, bucket.Segments.Count);
			Assert.AreEqual(true, bucket.Segments[0].Required);
			Assert.AreEqual("Segment Name", bucket.Segments[0].Name);
			Assert.AreEqual("segment-slug", bucket.Segments[0].Slug);
			Assert.IsNotNull(bucket.Constraints);
			Assert.AreEqual(21, bucket.Constraints.Length);

			// validate the response
			Assert.IsNotNull(response);
			Assert.AreNotEqual(Guid.Empty, response.Id);
			Assert.AreEqual(ContentType.Article, response.Type);
			Assert.AreEqual(5, response.LegacyId);
			Assert.AreEqual("My Name", response.Name);
			Assert.AreEqual(true, response.ReadOnly);
			Assert.AreEqual(ObjectStatus.Active, response.Status);
			Assert.AreEqual(originId, response.OriginId);
			Assert.AreEqual("Origin Name", response.OriginName);

			Assert.IsNotNull(response.Segments);
			Assert.AreEqual(1, response.Segments.Count);
			Assert.AreEqual(true, response.Segments[0].Required);
			Assert.AreEqual("Segment Name", response.Segments[0].Name);
			Assert.AreEqual("segment-slug", response.Segments[0].Slug);
			Assert.IsNotNull(response.Constraints);
			Assert.AreEqual(21, response.Constraints.Length);
		}

		[TestMethod]
		public void CreateBucket_NoBucket_ThrowsException()
		{
			FakeLayer fake = FakeLayer.Setup();

			BucketLogic logic = new BucketLogic();

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.CreateBucket(null));

			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void CreateBucket_NoName_ThrowsException()
		{
			FakeLayer fake = FakeLayer.Setup();

			BucketLogic logic = new BucketLogic();

			ContentBucketCreateRequest request = new ContentBucketCreateRequest
			{
				Type = ContentType.Article,
				LegacyId = 5,
				ReadOnly = true,
				Slug = "an invalid-slug",
				Segments = new List<ContentBucketSegmentRequest>
						                                          {
							                                          new ContentBucketSegmentRequest
								                                          {
									                                          Required = true,
									                                          Name = "Segment Name"
								                                          }
						                                          }
			};

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.CreateBucket(request));

			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void CreateBucket_NoSlug_ThrowsException()
		{
			FakeLayer fake = FakeLayer.Setup();

			BucketLogic logic = new BucketLogic();

			ContentBucketCreateRequest request = new ContentBucketCreateRequest
			{
				Type = ContentType.Article,
				LegacyId = 5,
				Name = "My Name",
				ReadOnly = true,
				Segments = new List<ContentBucketSegmentRequest>
						                                          {
							                                          new ContentBucketSegmentRequest
								                                          {
									                                          Required = true,
									                                          Name = "Segment Name"
								                                          }
						                                          }
			};

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.CreateBucket(request));

			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void CreateBucket_InvalidSlug_ThrowsException()
		{
			FakeLayer.Setup();

			BucketLogic logic = new BucketLogic();

			ContentBucketCreateRequest request = new ContentBucketCreateRequest
			{
				Type = ContentType.Article,
				LegacyId = 5,
				Name = "My Name",
				ReadOnly = true,
				Slug = "an invalid-slug",
				Segments = new List<ContentBucketSegmentRequest>
						                                          {
							                                          new ContentBucketSegmentRequest
								                                          {
									                                          Required = true,
									                                          Name = "Segment Name"
								                                          }
						                                          }
			};

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.CreateBucket(request));

			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void CreateBucket_With_Default_Segment_Created()
		{
			FakeLayer fake = FakeLayer.Setup();

			Guid originId = Guid.NewGuid();
			fake.DataLayer.Setup(new ContentOrigin { Id = originId, Name = "Origin Name" });

			BucketLogic logic = new BucketLogic();

			ContentBucketCreateRequest request = new ContentBucketCreateRequest
			{
				Type = ContentType.Article,
				LegacyId = 5,
				Name = "My Name",
				ReadOnly = true,
				Slug = "a-valid-slug",
				OriginId = originId,
				Constraints =
					new ContentBucketConstraint
					{
						Length = 21
					}
			};

			ContentBucketResponse response = logic.CreateBucket(request);

			FakeDataSet<ContentBucket> set = fake.DataLayer.GetFakeSet<ContentBucket>();

			// validate the db
			Assert.AreEqual(1, set.Count);

			ContentBucket bucket = set.First();
			Assert.AreNotEqual(Guid.Empty, bucket.Id);
			Assert.AreEqual(ContentType.Article, bucket.Type);
			Assert.AreEqual(5, bucket.LegacyId);
			Assert.AreEqual("My Name", bucket.Name);
			Assert.AreEqual(true, bucket.ReadOnly);
			Assert.AreEqual(ObjectStatus.Active, bucket.Status);
			Assert.AreEqual(originId, bucket.OriginId);

			Assert.IsNotNull(bucket.Segments);
			Assert.AreEqual(1, bucket.Segments.Count);
			Assert.AreEqual(false, bucket.Segments[0].Required);
			Assert.AreEqual("Default", bucket.Segments[0].Name);
			Assert.AreEqual("default", bucket.Segments[0].Slug);
			Assert.IsNotNull(bucket.Constraints);
			Assert.AreEqual(21, bucket.Constraints.Length);

			// validate the response
			Assert.IsNotNull(response);
			Assert.AreNotEqual(Guid.Empty, response.Id);
			Assert.AreEqual(ContentType.Article, response.Type);
			Assert.AreEqual(5, response.LegacyId);
			Assert.AreEqual("My Name", response.Name);
			Assert.AreEqual(true, response.ReadOnly);
			Assert.AreEqual(ObjectStatus.Active, response.Status);
			Assert.AreEqual(originId, response.OriginId);
			Assert.AreEqual("Origin Name", response.OriginName);

			Assert.IsNotNull(response.Segments);
			Assert.AreEqual(1, response.Segments.Count);
			Assert.AreEqual(false, response.Segments[0].Required);
			Assert.AreEqual("Default", response.Segments[0].Name);
			Assert.AreEqual("default", response.Segments[0].Slug);
			Assert.IsNotNull(response.Constraints);
			Assert.AreEqual(21, response.Constraints.Length);
		}

		[TestMethod]
		public void UpdateBucket_Succeeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			Guid id = Guid.NewGuid();
			Guid originId1 = Guid.NewGuid(), originId2 = Guid.NewGuid();
			Guid segmentId = Guid.NewGuid();

			fake.DataLayer.Setup(
				new ContentOrigin { Id = originId1, Name = "Origin Name" },
				new ContentOrigin { Id = originId2, Name = "Changed Origin Name" }
			);

			BucketLogic logic = new BucketLogic();

			ContentBucket original = new ContentBucket
									 {
										 Id = id,
										 Type = ContentType.Article,
										 LegacyId = 5,
										 Name = "My Name",
										 ReadOnly = true,
										 Status = ObjectStatus.Active,
										 Slug = "a-valid-slug",
										 OriginId = originId1,
										 Segments = new List<ContentBucketSegment>
				                                    {
					                                    new ContentBucketSegment
					                                    {
															Id = segmentId,
						                                    Required = true,
						                                    Name = "Segment Name",
															Slug = "segment-slug"
					                                    }
				                                    },
										 Constraints =
											 new ContentBucketConstraint
											 {
												 Length = 21
											 }
									 };

			fake.DataLayer.Setup(original);

            ContentBucketUpdateRequest request = new ContentBucketUpdateRequest
										   {
											   LegacyId = 7,
											   Name = "Updated Name",
											   ReadOnly = false,
											   Type = ContentType.Article,
											   OriginId = originId1,
											   Segments = new List<ContentBucketSegmentRequest>
				                                          {
					                                          new ContentBucketSegmentRequest
					                                          {
																  Id = segmentId,
						                                          Required = false,
						                                          Name = "Updated Name"
					                                          }
				                                          },
											   Constraints =
												   new ContentBucketConstraint
												   {
													   Length = 23
												   }

										   };

			ContentBucketResponse response = logic.UpdateBucket(id.ToString(), request);

			// validate db
			FakeDataSet<ContentBucket> set = fake.DataLayer.GetFakeSet<ContentBucket>();

			Assert.AreEqual(1, set.Count);

			ContentBucket bucket = set.First();

			Assert.AreEqual(id, bucket.Id);
			Assert.AreEqual(ContentType.Article, bucket.Type);
			Assert.AreEqual(7, bucket.LegacyId);
			Assert.AreEqual("Updated Name", bucket.Name);
			Assert.AreEqual(false, bucket.ReadOnly);
			Assert.AreEqual(ObjectStatus.Active, bucket.Status);
			Assert.AreEqual(originId1, bucket.OriginId);
			Assert.IsNotNull(bucket.Segments);
			Assert.AreEqual(1, bucket.Segments.Count);
			Assert.AreEqual(false, bucket.Segments[0].Required);
			Assert.AreEqual("Updated Name", bucket.Segments[0].Name);
			Assert.AreEqual("segment-slug", bucket.Segments[0].Slug);
			Assert.IsNotNull(bucket.Constraints);
			Assert.AreEqual(23, bucket.Constraints.Length);

			// validate response
			Assert.AreEqual(id, response.Id);
			Assert.AreEqual(ContentType.Article, response.Type);
			Assert.AreEqual(7, response.LegacyId);
			Assert.AreEqual("Updated Name", response.Name);
			Assert.AreEqual(false, response.ReadOnly);
			Assert.AreEqual(ObjectStatus.Active, response.Status);
			Assert.AreEqual(originId1, response.OriginId);
			Assert.AreEqual("Origin Name", response.OriginName);
			Assert.IsNotNull(response.Segments);
			Assert.AreEqual(1, response.Segments.Count);
			Assert.AreEqual(false, response.Segments[0].Required);
			Assert.AreEqual("Updated Name", response.Segments[0].Name);
			Assert.AreEqual("segment-slug", response.Segments[0].Slug);
			Assert.IsNotNull(response.Constraints);
			Assert.AreEqual(23, response.Constraints.Length);
		}

		[TestMethod]
		public void UpdateBucket_Creates_Default_Segment_If_None_Present()
		{
			FakeLayer fake = FakeLayer.Setup();

			Guid id = Guid.NewGuid();
			Guid originId1 = Guid.NewGuid(), originId2 = Guid.NewGuid();
			Guid segmentId = Guid.NewGuid();

			fake.DataLayer.Setup(
				new ContentOrigin { Id = originId1, Name = "Origin Name" },
				new ContentOrigin { Id = originId2, Name = "Changed Origin Name" }
			);

			BucketLogic logic = new BucketLogic();

			ContentBucket original = new ContentBucket
			{
				Id = id,
				Type = ContentType.Article,
				LegacyId = 5,
				Name = "My Name",
				ReadOnly = true,
				Status = ObjectStatus.Active,
				Slug = "a-valid-slug",
				OriginId = originId1,
				Segments = new List<ContentBucketSegment>
				                                    {
					                                    new ContentBucketSegment
					                                    {
															Id = segmentId,
						                                    Required = true,
						                                    Name = "Segment Name",
															Slug = "segment-slug"
					                                    }
				                                    },
				Constraints =
					new ContentBucketConstraint
					{
						Length = 21
					}
			};

			fake.DataLayer.Setup(original);

            ContentBucketUpdateRequest request = new ContentBucketUpdateRequest
			{
				LegacyId = 7,
				Name = "Updated Name",
				ReadOnly = false,
				Type = ContentType.Article,
				OriginId = originId1,
				Constraints =
					new ContentBucketConstraint
					{
						Length = 23
					}
			};

			ContentBucketResponse response = logic.UpdateBucket(id.ToString(), request);

			// validate db
			FakeDataSet<ContentBucket> set = fake.DataLayer.GetFakeSet<ContentBucket>();

			Assert.AreEqual(1, set.Count);

			ContentBucket bucket = set.First();

			Assert.AreEqual(id, bucket.Id);
			Assert.AreEqual(ContentType.Article, bucket.Type);
			Assert.AreEqual(7, bucket.LegacyId);
			Assert.AreEqual("Updated Name", bucket.Name);
			Assert.AreEqual(false, bucket.ReadOnly);
			Assert.AreEqual(ObjectStatus.Active, bucket.Status);
			Assert.AreEqual(originId1, bucket.OriginId);
			Assert.IsNotNull(bucket.Segments);
			Assert.AreEqual(1, bucket.Segments.Count);
			Assert.AreEqual(false, bucket.Segments[0].Required);
			Assert.AreEqual("Default", bucket.Segments[0].Name);
			Assert.AreEqual("default", bucket.Segments[0].Slug);
			Assert.IsNotNull(bucket.Constraints);
			Assert.AreEqual(23, bucket.Constraints.Length);

			// validate response
			Assert.AreEqual(id, response.Id);
			Assert.AreEqual(ContentType.Article, response.Type);
			Assert.AreEqual(7, response.LegacyId);
			Assert.AreEqual("Updated Name", response.Name);
			Assert.AreEqual(false, response.ReadOnly);
			Assert.AreEqual(ObjectStatus.Active, response.Status);
			Assert.AreEqual(originId1, response.OriginId);
			Assert.AreEqual("Origin Name", response.OriginName);
			Assert.IsNotNull(response.Segments);
			Assert.AreEqual(1, response.Segments.Count);
			Assert.AreEqual(false, response.Segments[0].Required);
			Assert.AreEqual("Default", response.Segments[0].Name);
			Assert.AreEqual("default", response.Segments[0].Slug);
			Assert.IsNotNull(response.Constraints);
			Assert.AreEqual(23, response.Constraints.Length);
		}

		[TestMethod]
		public void UpdateBucket_ChangeType_ThrowsException()
		{
			FakeLayer fake = FakeLayer.Setup();

			Guid id = Guid.NewGuid();

			BucketLogic logic = new BucketLogic();

			ContentBucket original = new ContentBucket
									 {
										 Id = id,
										 Type = ContentType.Article,
										 LegacyId = 5,
										 Name = "My Name",
										 ReadOnly = true,
										 Status = ObjectStatus.Active,
										 Slug = "a-valid-slug",
										 Segments = new List<ContentBucketSegment>
				                                    {
					                                    new ContentBucketSegment
					                                    {
						                                    Required = true,
						                                    Name = "Segment Name"
					                                    }
				                                    },
										 Constraints =
											 new ContentBucketConstraint
											 {
												 Length = 21
											 }
									 };

			fake.DataLayer.Setup(original);

            ContentBucketUpdateRequest request = new ContentBucketUpdateRequest
			{
				LegacyId = 7,
				Name = "Updated Name",
				Type = ContentType.Text,
				ReadOnly = false,
				Segments = new List<ContentBucketSegmentRequest>
						                                          {
							                                          new ContentBucketSegmentRequest
								                                          {
									                                          Required = false,
									                                          Name = "Updated Name"
								                                          }
						                                          }
			};

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.UpdateBucket(id.ToString(), request));

			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void GetBucket_ById_Succeeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			Guid id = Guid.NewGuid();
			Guid originId = Guid.NewGuid();
			fake.DataLayer.Setup(new ContentOrigin { Id = originId, Name = "Origin Name" });

			BucketLogic logic = new BucketLogic();

			ContentBucket original = new ContentBucket
									 {
										 Id = id,
										 Type = ContentType.Article,
										 LegacyId = 5,
										 Name = "My Name",
										 ReadOnly = true,
										 Status = ObjectStatus.Active,
										 Slug = "a-valid-slug",
										 OriginId = originId,
										 Segments = new List<ContentBucketSegment>
				                                    {
					                                    new ContentBucketSegment
					                                    {
						                                    Required = true,
						                                    Name = "Segment Name"
					                                    }
				                                    },
										 Constraints =
											 new ContentBucketConstraint
											 {
												 Length = 21
											 }
									 };

			fake.DataLayer.Setup(original);

			ContentBucketResponse response = logic.GetBucket(id.ToString());

			Assert.AreEqual(id, response.Id);
			Assert.AreEqual(ContentType.Article, response.Type);
			Assert.AreEqual(5, response.LegacyId);
			Assert.AreEqual("My Name", response.Name);
			Assert.AreEqual(true, response.ReadOnly);
			Assert.AreEqual(ObjectStatus.Active, response.Status);
			Assert.AreEqual(originId, response.OriginId);
			Assert.AreEqual("Origin Name", response.OriginName);
			Assert.IsNotNull(response.Segments);
			Assert.AreEqual(1, response.Segments.Count);
			Assert.AreEqual(true, response.Segments[0].Required);
			Assert.AreEqual("Segment Name", response.Segments[0].Name);
			Assert.IsNotNull(response.Constraints);
			Assert.AreEqual(21, response.Constraints.Length);
		}

		[TestMethod]
		public void GetBucket_BySlug_Succeeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			Guid id = Guid.NewGuid();
			Guid originId = Guid.NewGuid();

			fake.DataLayer.Setup(new ContentOrigin { Id = originId, Name = "Origin Name" });

			BucketLogic logic = new BucketLogic();

			ContentBucket original = new ContentBucket
									 {
										 Id = id,
										 Type = ContentType.Article,
										 LegacyId = 5,
										 Name = "My Name",
										 ReadOnly = true,
										 Status = ObjectStatus.Active,
										 Slug = "a-valid-slug",
										 OriginId = originId,
										 Segments = new List<ContentBucketSegment>
				                                    {
					                                    new ContentBucketSegment
					                                    {
						                                    Required = true,
						                                    Name = "Segment Name"
					                                    }
				                                    },
										 Constraints =
											 new ContentBucketConstraint
											 {
												 Length = 21
											 }
									 };

			fake.DataLayer.Setup(original);

			ContentBucketResponse response = logic.GetBucket("a-valid-slug");

			Assert.AreEqual(id, response.Id);
			Assert.AreEqual(ContentType.Article, response.Type);
			Assert.AreEqual(5, response.LegacyId);
			Assert.AreEqual("My Name", response.Name);
			Assert.AreEqual(true, response.ReadOnly);
			Assert.IsNotNull(response.Segments);
			Assert.AreEqual(1, response.Segments.Count);
			Assert.AreEqual(ObjectStatus.Active, response.Status);
			Assert.AreEqual(originId, response.OriginId);
			Assert.AreEqual("Origin Name", response.OriginName);
			Assert.AreEqual(true, response.Segments[0].Required);
			Assert.AreEqual("Segment Name", response.Segments[0].Name);
			Assert.IsNotNull(response.Constraints);
			Assert.AreEqual(21, response.Constraints.Length);
		}

		[TestMethod]
		public void DeleteBucket_Succeeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			FakeContent content = ContentGenerator.GenerateContent(fake); 

			BucketLogic logic = new BucketLogic();

			ContentBucketResponse response = logic.DeleteBucket(content.Bucket.Id.ToString());

			FakeDataSet<ContentBucket> set = fake.DataLayer.GetFakeSet<ContentBucket>();
			FakeDataSet<ContentVersion> contentSet = fake.DataLayer.GetFakeSet<ContentVersion>();

			Assert.AreEqual(1, set.Count);
			Assert.AreEqual(ObjectStatus.Deleted, set[0].Status);

			Assert.AreEqual(content.Bucket.Id, response.Id);
			Assert.AreEqual(ContentType.Article, response.Type);
			Assert.AreEqual(content.Bucket.LegacyId, response.LegacyId);
			Assert.AreEqual(content.Bucket.Name, response.Name);
			Assert.AreEqual(content.Bucket.ReadOnly, response.ReadOnly);
			Assert.IsNotNull(response.Segments);
			Assert.AreEqual(1, response.Segments.Count);
			Assert.AreEqual(ObjectStatus.Deleted, response.Status);
			Assert.AreEqual(content.Origin.Id, response.OriginId);
			Assert.AreEqual(content.Origin.Name, response.OriginName);
			Assert.AreEqual(true, response.Segments[0].Required);
			Assert.AreEqual("Segment Name", response.Segments[0].Name);
			Assert.IsNotNull(response.Constraints);
			Assert.AreEqual(21, response.Constraints.Length);

			Assert.IsTrue(contentSet.Contains(content.Version));
			Assert.IsTrue(contentSet.Any(x => x.Status == ObjectStatus.Deleted));
		}

        
	}
}
