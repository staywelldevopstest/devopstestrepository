﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.ContentLogic;
using KswApi.Poco.Content;
using KswApi.Test.Framework;
using KswApi.Test.Framework.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

// ReSharper disable InconsistentNaming

namespace KswApi.Test.Content
{
	[TestClass]
	public class ImageLogicTests
	{
		[TestMethod]
		public void Create_Succeeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			fake.Settings.ServiceUri = "http://service.com";
			fake.Settings.WebPortalBaseUri = "http://portal.com";

			ImageLogic logic = new ImageLogic();

			ContentBucket bucket = GenerateBucket(fake);

			Stream stream = Resource.GetJpeg();

			FakeHttpFileCollection files = new FakeHttpFileCollection { new FakeHttpFile("filename", null, (int)stream.Length, stream) };

			ImageDetailResponse response = logic.Create(bucket.Slug, null, files);

			FakeDataSet<ImageDetail> details = fake.DataLayer.GetFakeSet<ImageDetail>();

			Assert.IsNotNull(response);
			Assert.AreEqual(response.Format, ImageFormatType.Jpg);
			Assert.AreEqual(100, response.Width);
			Assert.AreEqual(100, response.Height);
			Assert.AreEqual(string.Format("http://portal.com/Service/Get/Content/bucket-slug/Images/{0}", response.Id), response.Uri);

			Assert.AreEqual(1, details.Count);
			ImageDetail detail = details[0];

			Assert.AreEqual(detail.Id, response.Id);
			
			Assert.IsNull(detail.Slug);
			
			Assert.AreEqual(ObjectStatus.Uploading, detail.Status);
		}

		[TestMethod]
		public void UpdateImageDetails_Succeeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			fake.Settings.ServiceUri = "http://service.com";
			fake.Settings.WebPortalBaseUri = "http://portal.com";

			ImageLogic logic = new ImageLogic();

			ContentBucket bucket = GenerateBucket(fake);

			Stream stream = Resource.GetJpeg();

			ImageDetailResponse original = logic.Create(bucket.Slug, new StreamRequest(stream), null);

			ImageDetailRequest request = new ImageDetailRequest
			                             {
											 Slug = "the-slug",
											 Title = "The title",
											 LegacyId = "31",
											 InvertedTitle = "Inverted",
											 Blurb = "The blurb",
											 AlternateTitles = new List<string> {"Alt 1", "Alt 2"}
			                             };

			ImageDetailResponse response = logic.UpdateDetails(bucket.Slug, original.Id.ToString(), request);

			FakeDataSet<ImageDetail> details = fake.DataLayer.GetFakeSet<ImageDetail>();

			Assert.IsNotNull(response);
			
			Assert.AreEqual(1, details.Count);
			ImageDetail detail = details[0];
			Assert.AreEqual(detail.Id, response.Id);

			Assert.AreEqual("the-slug", response.Slug);
			Assert.AreEqual("The title", response.Title);
			Assert.AreEqual("31", response.LegacyId);
			Assert.AreEqual("Inverted", response.InvertedTitle);
			Assert.AreEqual("The blurb", response.Blurb);
			Assert.IsNotNull(response.AlternateTitles);
			Assert.AreEqual(2, response.AlternateTitles.Count);
			Assert.AreEqual("Alt 1", response.AlternateTitles[0]);
			Assert.AreEqual("Alt 2", response.AlternateTitles[1]);

			Assert.AreEqual("the-slug", detail.Slug);
			Assert.AreEqual("The title", detail.Title);
			Assert.AreEqual("31", detail.LegacyId);
			Assert.AreEqual("Inverted", detail.InvertedTitle);
			Assert.AreEqual("The blurb", detail.Blurb);
			Assert.IsNotNull(detail.AlternateTitles);
			Assert.AreEqual(2, detail.AlternateTitles.Count);
			Assert.AreEqual("Alt 1", detail.AlternateTitles[0]);
			Assert.AreEqual("Alt 2", detail.AlternateTitles[1]);
			Assert.AreEqual(ObjectStatus.Active, detail.Status);
		}

		[TestMethod]
		public void UpdateImageDetails_ReplaceImage_Succeeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			ImageDetailResponse existing = Setup(fake);

			ImageLogic logic = new ImageLogic();

			Stream stream = Resource.GetPng();

			ImageDetailResponse newImage = logic.Create(existing.Bucket.Slug, new StreamRequest(stream), null);

			ImageDetailRequest request = new ImageDetailRequest
			{
				Slug = "the-slug",
				Replacement = newImage.Id,
				Title = "The title",
				LegacyId = "31",
				InvertedTitle = "Inverted",
				Blurb = "The blurb",
				AlternateTitles = new List<string> { "Alt 1", "Alt 2" }
			};

			ImageDetailResponse response = logic.UpdateDetails(existing.Bucket.Slug, existing.Slug, request);

			FakeDataSet<ImageDetail> details = fake.DataLayer.GetFakeSet<ImageDetail>();

			Assert.IsNotNull(response);

			Assert.AreEqual(1, details.Count);
			ImageDetail detail = details[0];
			Assert.AreEqual(detail.Id, response.Id);

			Assert.AreEqual("the-slug", response.Slug);
			Assert.AreEqual("The title", response.Title);
			Assert.AreEqual("31", response.LegacyId);
			Assert.AreEqual("Inverted", response.InvertedTitle);
			Assert.AreEqual("The blurb", response.Blurb);
			Assert.IsNotNull(response.AlternateTitles);
			Assert.AreEqual(2, response.AlternateTitles.Count);
			Assert.AreEqual("Alt 1", response.AlternateTitles[0]);
			Assert.AreEqual("Alt 2", response.AlternateTitles[1]);

			Assert.AreEqual("the-slug", detail.Slug);
			Assert.AreEqual("The title", detail.Title);
			Assert.AreEqual("31", detail.LegacyId);
			Assert.AreEqual("Inverted", detail.InvertedTitle);
			Assert.AreEqual("The blurb", detail.Blurb);
			Assert.IsNotNull(detail.AlternateTitles);
			Assert.AreEqual(2, detail.AlternateTitles.Count);
			Assert.AreEqual("Alt 1", detail.AlternateTitles[0]);
			Assert.AreEqual("Alt 2", detail.AlternateTitles[1]);
			Assert.AreEqual(ObjectStatus.Active, detail.Status);

			StreamResponse streamResponse = logic.GetImage(response.Bucket.Slug, response.Slug, null);

			Image image = Image.FromStream(streamResponse);

			Assert.IsNotNull(image);
			Assert.AreEqual(ImageFormat.Png, image.RawFormat);
			Assert.AreEqual(100, image.Width);
			Assert.AreEqual(100, image.Height);
		}

		[TestMethod]
		public void GetImage_Succeeds()
		{
			FakeLayer fake = FakeLayer.Setup();
			
			ImageDetailResponse existing = Setup(fake);

			ImageLogic logic = new ImageLogic();

			Stream ms = new MemoryStream();

			StreamResponse response = new StreamResponse(ms);

			response = logic.GetImage(existing.Bucket.Slug, existing.Slug, response);

			Assert.IsNotNull(response);

			Image image = Image.FromStream(response);

			Assert.AreEqual(ImageFormat.Jpeg, image.RawFormat);
			Assert.AreEqual(100, image.Width);
			Assert.AreEqual(100, image.Height);
		}

		[TestMethod]
		public void GetImage_TypeConversionSucceeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			ImageDetailResponse existing = Setup(fake);

			ImageLogic logic = new ImageLogic();

			Stream ms = new MemoryStream();

			StreamResponse response = new StreamResponse(ms);

			response = logic.GetImage(existing.Bucket.Slug, existing.Slug + ".png", response);

			Assert.IsNotNull(response);

			Image image = Image.FromStream(response);

			Assert.AreEqual(ImageFormat.Png, image.RawFormat);
			Assert.AreEqual(100, image.Width);
			Assert.AreEqual(100, image.Height);
		}

		[TestMethod]
		public void GetImage_SizeConversionSucceeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			ImageDetailResponse existing = Setup(fake);

			ImageLogic logic = new ImageLogic();

			Stream ms = new MemoryStream();

			StreamResponse response = new StreamResponse(ms);

			response = logic.GetImage(existing.Bucket.Slug, existing.Slug + ".300x200", response);

			Assert.IsNotNull(response);

			Image image = Image.FromStream(response);

			Assert.AreEqual(ImageFormat.Jpeg, image.RawFormat);
			Assert.AreEqual(200, image.Width);
			Assert.AreEqual(200, image.Height);
		}

		[TestMethod]
		public void GetImage_TypeConversionWithSizeConversionSucceeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			ImageDetailResponse existing = Setup(fake);

			ImageLogic logic = new ImageLogic();

			Stream ms = new MemoryStream();

			StreamResponse response = new StreamResponse(ms);

			response = logic.GetImage(existing.Bucket.Slug, existing.Slug + ".300x200.png", response);

			Assert.IsNotNull(response);

			Image image = Image.FromStream(response);

			Assert.AreEqual(ImageFormat.Png, image.RawFormat);
			Assert.AreEqual(200, image.Width);
			Assert.AreEqual(200, image.Height);
		}

		[TestMethod]
		public void DeleteImage_Succeeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			ImageDetailResponse existing = Setup(fake);

			ImageLogic logic = new ImageLogic();


			ImageDetailResponse response = logic.DeleteImage(existing.Bucket.Slug, existing.Slug);

			Assert.IsNotNull(response);

			FakeDataSet<ImageDetail> details = fake.DataLayer.GetFakeSet<ImageDetail>();

			Assert.AreEqual(1, details.Count);
			ImageDetail detail = details[0];
			Assert.AreEqual(detail.Id, response.Id);

			Assert.AreEqual("the-slug", response.Slug);
			Assert.AreEqual("The title", response.Title);
			Assert.AreEqual("31", response.LegacyId);
			Assert.AreEqual("Inverted", response.InvertedTitle);
			Assert.AreEqual("The blurb", response.Blurb);
			Assert.IsNotNull(response.AlternateTitles);
			Assert.AreEqual(2, response.AlternateTitles.Count);
			Assert.AreEqual("Alt 1", response.AlternateTitles[0]);
			Assert.AreEqual("Alt 2", response.AlternateTitles[1]);

			Assert.AreEqual("the-slug", detail.Slug);
			Assert.AreEqual("The title", detail.Title);
			Assert.AreEqual("31", detail.LegacyId);
			Assert.AreEqual("Inverted", detail.InvertedTitle);
			Assert.AreEqual("The blurb", detail.Blurb);
			Assert.IsNotNull(detail.AlternateTitles);
			Assert.AreEqual(2, detail.AlternateTitles.Count);
			Assert.AreEqual("Alt 1", detail.AlternateTitles[0]);
			Assert.AreEqual("Alt 2", detail.AlternateTitles[1]);
			Assert.AreEqual(ObjectStatus.Deleted, detail.Status);
		}

		[TestMethod]
		public void DeleteImage_ByObject_Succeeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			Setup(fake);

			ImageLogic logic = new ImageLogic();

			FakeDataSet<ImageDetail> details = fake.DataLayer.GetFakeSet<ImageDetail>();

			ImageDetail detail = details[0];

			ContentResponse response = logic.DeleteImage(detail);

			Assert.IsNotNull(response);

			Assert.AreEqual(1, details.Count);

			detail = details[0];
			
			Assert.AreEqual(detail.Id, response.Id);

			Assert.AreEqual("the-slug", response.Slug);
			Assert.AreEqual("The title", response.Title);
			Assert.AreEqual("31", response.LegacyId);
			Assert.AreEqual("Inverted", response.InvertedTitle);
			Assert.AreEqual("The blurb", response.Blurb);
			Assert.IsNotNull(response.AlternateTitles);
			Assert.AreEqual(2, response.AlternateTitles.Count);
			Assert.AreEqual("Alt 1", response.AlternateTitles[0]);
			Assert.AreEqual("Alt 2", response.AlternateTitles[1]);

			Assert.AreEqual("the-slug", detail.Slug);
			Assert.AreEqual("The title", detail.Title);
			Assert.AreEqual("31", detail.LegacyId);
			Assert.AreEqual("Inverted", detail.InvertedTitle);
			Assert.AreEqual("The blurb", detail.Blurb);
			Assert.IsNotNull(detail.AlternateTitles);
			Assert.AreEqual(2, detail.AlternateTitles.Count);
			Assert.AreEqual("Alt 1", detail.AlternateTitles[0]);
			Assert.AreEqual("Alt 2", detail.AlternateTitles[1]);
			Assert.AreEqual(ObjectStatus.Deleted, detail.Status);
		}

		[TestMethod]
		public void GetNewSlug_ReturnsSlug()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			AddImages(fakeLayer, "bucket-slug", "slug", 23);

			ImageLogic logic = new ImageLogic();

			SlugResponse response = logic.GetImageSlug("bucket-slug", null, "slug", null);

			Assert.IsNotNull(response);

			Assert.AreEqual("slug-23", response.Value);
		}


		#region Private Helper Methods

		private ImageDetailResponse Setup(FakeLayer fake)
		{
			ImageLogic logic = new ImageLogic();

			ContentBucket bucket = GenerateBucket(fake);

			Stream stream = Resource.GetJpeg();

			ImageDetailResponse original = logic.Create(bucket.Slug, new StreamRequest(stream), null);

			ImageDetailRequest request = new ImageDetailRequest
			{
				Slug = "the-slug",
				Title = "The title",
				LegacyId = "31",
				InvertedTitle = "Inverted",
				Blurb = "The blurb",
				AlternateTitles = new List<string> { "Alt 1", "Alt 2" }
			};

			return logic.UpdateDetails(bucket.Slug, original.Id.ToString(), request);
		}

		private ContentBucket GenerateBucket(FakeLayer fake)
		{
			ContentOrigin origin = new ContentOrigin { Id = Guid.NewGuid(), Name = "Origin Name" };
			fake.DataLayer.Setup(origin);

			ContentBucket bucket = new ContentBucket
			{
				Id = Guid.NewGuid(),
				Slug = "bucket-slug",
				Type = ContentType.Image,
				OriginId = origin.Id,
				Name = "The Bucket",
				CaseInsensitiveName = "the bucket",
				Status = ObjectStatus.Active,
				LegacyId = 5,
				ReadOnly = true,
				Constraints = new ContentBucketConstraint
				{
					Length = 21
				}
			};

			fake.DataLayer.Setup(bucket);

			return bucket;
		}

		private void AddImages(FakeLayer fakeLayer, string bucketSlug, string slugPrefix, int count)
		{
			Guid bucketId = Guid.NewGuid();

			fakeLayer.DataLayer.Setup(new ContentBucket
			{
				Id = bucketId,
				Name = "Bucket Name",
				Slug = bucketSlug
			});

			for (int i = 0; i < count; i++)
			{
				string currentSlug = slugPrefix + (i == 0 ? "" : "-" + i.ToString());
				fakeLayer.DataLayer.AddItem(new ImageDetail
				{
					Id = Guid.NewGuid(),
					BucketId = bucketId,
					Slug = currentSlug,
					Status = ObjectStatus.Active,
					Paths = new List<ContentPath>
											        {
												        new ContentPath {BucketIdOrSlug = bucketSlug, ContentIdOrSlug = currentSlug}
											        }
				});
			}
		}
		#endregion

	}
}
