﻿using KswApi.Interface.Objects.Content;
using KswApi.Logic.ContentLogic;
using KswApi.Test.Framework.DataGenerators;
using KswApi.Test.Framework.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable InconsistentNaming

namespace KswApi.Test.Content
{
	[TestClass]
	public class TopicLogicTests
	{
		[TestMethod]
		public void GetTopic_RootTopic_Succeeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			CollectionGenerator.GenerateCollection(fake);

			TopicLogic logic = new TopicLogic();

			TopicResponse response = logic.GetTopic("collection-1", false, false, null);

			Assert.IsNotNull(response);
			Assert.IsNull(response.Items);
			Assert.AreEqual("Collection 1", response.Title);
			Assert.AreEqual("collection-1", response.Slug);
		}

		[TestMethod]
		public void GetTopic_RootTopic_WithChildren_Succeeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			CollectionGenerator.GenerateCollection(fake);

			TopicLogic logic = new TopicLogic();

			TopicResponse response = logic.GetTopic("collection-1", true, false, null);

			Assert.IsNotNull(response);
			Assert.IsNotNull(response.Items);
			Assert.AreEqual("Collection 1", response.Title);
			Assert.AreEqual("collection-1", response.Slug);
			Assert.AreEqual(1, response.Items.Count);
			Assert.AreEqual(response.Items[0].Slug, "topic-1");
			Assert.AreEqual(response.Items[0].Title, "Topic 1");
		}

		[TestMethod]
		public void GetTopic_RootTopic_Recursive_Succeeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			CollectionGenerator.GenerateCollection(fake);

			TopicLogic logic = new TopicLogic();

			TopicResponse response = logic.GetTopic("collection-1", false, true, null);

			Assert.IsNotNull(response);
			Assert.IsNotNull(response.Items);
			Assert.AreEqual("Collection 1", response.Title);
			Assert.AreEqual("collection-1", response.Slug);
			Assert.AreEqual(2, response.Items.Count);
			Assert.AreEqual(response.Items[0].Slug, "topic-1");
			Assert.AreEqual(response.Items[0].Title, "Topic 1");
			Assert.AreEqual(response.Items[1].Slug, "topic-2");
			Assert.AreEqual(response.Items[1].Title, "Topic 2");
		}

		[TestMethod]
		public void GetTopic_RootTopic_WithChildren_Recursive_Succeeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			CollectionGenerator.GenerateCollection(fake);

			TopicLogic logic = new TopicLogic();

			TopicResponse response = logic.GetTopic("collection-1", true, true, null);

			Assert.IsNotNull(response);
			Assert.IsNotNull(response.Items);
			Assert.AreEqual("Collection 1", response.Title);
			Assert.AreEqual("collection-1", response.Slug);
			Assert.AreEqual(2, response.Items.Count);
			Assert.AreEqual(response.Items[0].Slug, "topic-1");
			Assert.AreEqual(response.Items[0].Title, "Topic 1");
			Assert.AreEqual(response.Items[1].Slug, "topic-2");
			Assert.AreEqual(response.Items[1].Title, "Topic 2");
		}
	}
}
