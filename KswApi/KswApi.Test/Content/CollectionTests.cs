﻿using System;
using System.Collections.Generic;
using System.Linq;
using KswApi.Common.Objects;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.ContentLogic;
using KswApi.Poco.Content;
using KswApi.Test.Framework.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable InconsistentNaming

namespace KswApi.Test.Content
{
	[TestClass]
	public class CollectionTests
	{
		#region Collection

		[TestMethod]
		public void CreateCollection_Succeeds()
		{
			FakeLayer fake = FakeLayer.Setup();

			CollectionLogic logic = new CollectionLogic();

			DateTime date = new DateTime(2013, 5, 13).ToUniversalTime();

			fake.DataLayer.Setup(new ContentBucket
			{
				Id = Guid.NewGuid(),
				Type = ContentType.Article,
				Status = ObjectStatus.Active,
				Slug = "bucket-slug"
			});

            CollectionCreateRequest request = new CollectionCreateRequest
			{
				Title = "the title",
				Expires = date,
				Slug = "the-slug",
				Items = new List<CollectionItemRequest> { CreateCollectionItemRequest(CollectionItemType.Topic), CreateCollectionItemRequest(CollectionItemType.Content) }
			};

			AccessToken token = new AccessToken { };
			CollectionResponse response = logic.CreateCollection(request, token);

			Assert.IsNotNull(response);
			Assert.AreEqual("the title", response.Title);
			Assert.AreEqual(date, response.Expires);
			Assert.AreEqual("the-slug", response.Slug);

			FakeDataSet<Collection> collectionSet = fake.DataLayer.GetFakeSet<Collection>();
			FakeDataSet<Topic> topicSet = fake.DataLayer.GetFakeSet<Topic>();
			Assert.AreEqual(1, collectionSet.Count);

			Assert.AreEqual(4, topicSet.Count);

			Collection collection = collectionSet[0];
			Topic topic = topicSet[0];
			Assert.IsNotNull(collection);
			Assert.AreEqual("the title", topic.Title);
			Assert.AreEqual(date, collection.Expires);
			Assert.AreEqual("the-slug", topic.Slug);

			Assert.IsTrue(AreCollectionItemsEqual(request.Items, response.Items));
		}

		#endregion

		#region CollectionItemRequest

		public bool AreCollectionItemsEqual(List<CollectionItemRequest> requestItems, List<CollectionItemResponse> responseItems)
		{
			if (requestItems == null && responseItems == null) return true;
			if (requestItems == null ^ responseItems == null) return false;

			if (requestItems.Count != responseItems.Count) return false;

			for (int i = 0; i < requestItems.Count; i++)
			{
				// TODO: Use reflection
				CollectionItemRequest requestItem = requestItems[i];
				CollectionItemResponse responseItem = responseItems[i];

				if (requestItem.Description != responseItem.Description)
					return false;
				if ((requestItem.Disabled ?? false) != (responseItem.Disabled ?? false))
					return false;
				if (requestItem.FlagComment != responseItem.FlagComment)
					return false;
				if (requestItem.Flagged != (responseItem.Flagged ?? false))
					return false;
				if (requestItem.ImageUri != responseItem.ImageUri)
					return false;
				if (requestItem.ItemReference == null)
				{
				    if (responseItem.Id != default(Guid))
				        return false;

				    if (!string.IsNullOrEmpty(responseItem.Slug))
				        return false;

					if (responseItem.Bucket != null)
						return false;
				}
				else
				{
					if (requestItem.ItemReference.IdOrSlug != responseItem.Id.ToString() && requestItem.ItemReference.IdOrSlug != responseItem.Slug)
						return false;

				    if (requestItem.ItemReference.BucketIdOrSlug == null)
				    {
				        if (responseItem.Bucket != null)
				            return false;
				    }
				    else
				    {
				        if (responseItem.Bucket == null)
				            return false;

				        if (requestItem.ItemReference.BucketIdOrSlug != responseItem.Bucket.Id.ToString() &&
				            requestItem.ItemReference.IdOrSlug != responseItem.Slug)
				            return false;
				    }
				}
				
				if (requestItem.Title != responseItem.Title)
					return false;
				if (requestItem.Type != responseItem.Type)
					return false;

				requestItem.Items = requestItem.Items ?? new List<CollectionItemRequest>();
				responseItem.Items = responseItem.Items ?? new List<CollectionItemResponse>();

				if (requestItem.Items.Count != responseItem.Items.Count)
					return false;

				if (!AreCollectionItemsEqual(requestItem.Items, responseItem.Items))
					return false;
			}

			return true;
		}

		public CollectionItemRequest CreateCollectionItemRequest(CollectionItemType type)
		{
			switch (type)
			{
				case CollectionItemType.Content:
					return CreateContentCollectionItem();
				case CollectionItemType.Topic:
					return CreateTopicCollectionItem(numChildren: 2);
				case CollectionItemType.Sort:
					return CreateSortCollectionItem();
				default:
					throw new ArgumentException();
			}
		}

		public CollectionItemRequest CreateContentCollectionItem()
		{
			// TODO: use data generators instead
			ContentLogic contentLogic = new ContentLogic();
			contentLogic.CreateContent("bucket-slug", new NewContentRequest
			{
				Slug = "slug",
				Title = "title",
				AgeCategories = new List<AgeCategory>
                    {
                        AgeCategory.Infant
                    },
				Gender = Gender.All,
				Publish = true

			});

			return new CollectionItemRequest
			{
				ItemReference = new ItemReference
								   {
									   IdOrSlug = "slug",
									   BucketIdOrSlug = "bucket-slug"
								   },
				Type = CollectionItemType.Content,
				ImageUri = null,
				Flagged = true,
				FlagComment = "flag-comment",
				Disabled = false,
				Description = null,
				Title = "title",
				Items = null
			};
		}

		public CollectionItemRequest CreateTopicCollectionItem(string prefix = null, int? numChildren = null)
		{
			return new CollectionItemRequest
			{
				ItemReference = new ItemReference
				{
					IdOrSlug = ""
				},
				Type = CollectionItemType.Topic,
				ImageUri = "image-uri",
				Flagged = true,
				FlagComment = "flag-comment",
				Disabled = false,
				Description = "description",
				Title = "sub-topic" + prefix ?? "",
				Items = numChildren.HasValue
					? new List<CollectionItemRequest>(Enumerable.Range(0, numChildren.Value).Select(x => CreateTopicCollectionItem(x.ToString())))
					: null
			};
		}

		public CollectionItemRequest CreateSortCollectionItem()
		{
			throw new NotImplementedException();
		}

		#endregion
	}
}
