﻿using System;
using KswApi.Interface.Objects;
using KswApi.Logic;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable InconsistentNaming

namespace KswApi.Test
{
	[TestClass]
	public class RootLogicTests
	{
		[TestMethod]
		public void GetServiceInformation_ReturnsServiceInformation()
		{
			RootLogic rootLogic = new RootLogic();
			ServiceInformation serviceInformation = rootLogic.GetServiceInformation();
			
			Assert.IsNotNull(serviceInformation);
			Assert.AreEqual(string.Format("Copyright © {0} Krames StayWell except where otherwise noted.", DateTime.UtcNow.Year), serviceInformation.Copyright);
		}

	}
}
