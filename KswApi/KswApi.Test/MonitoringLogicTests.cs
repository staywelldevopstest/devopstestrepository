﻿
using KswApi.Interface.Objects;
using KswApi.Logic;
using KswApi.Test.Framework.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

// ReSharper disable InconsistentNaming

namespace KswApi.Test
{
    [TestClass]
    public class MonitoringLogicTests
    {
        [TestMethod]
        public void ServerStatus_Successful()
        {
            SetupLogs();

            MonitoringLogic logic = new MonitoringLogic();

            MonitoringStatus status = logic.CheckStatus();

            Assert.IsNotNull(status);
            Assert.IsTrue(status.DatabaseStatus);
            Assert.IsTrue(status.ServiceStatus);
            Assert.IsTrue(status.CacheStatus);
        }

        private FakeLayer SetupLogs()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            FakeDataSet<Log> logs = fakeLayer.DataLayer.GetFakeSet<Log>();

            for (int i = 0; i < 50; i++)
            {
                logs.Add(new Log
                {
                    Id = Guid.NewGuid(),
                    Type = LogItemType.Operation,
                    SubType = LogItemSubType.Operation
                });
            }


            for (int i = 0; i < 25; i++)
            {
                logs.Add(new Log
                {
                    Id = Guid.NewGuid(),
                    Type = LogItemType.Notification,
                    SubType = LogItemSubType.Exception
                });
            }

            return fakeLayer;
        }
    }
}
