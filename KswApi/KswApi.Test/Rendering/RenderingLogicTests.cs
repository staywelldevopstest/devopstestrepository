﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using KswApi.Common.Configuration;
using KswApi.Common.Objects;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.Content;
using KswApi.Interface.Objects.Rendering;
using KswApi.Interface.Rendering;
using KswApi.Ioc;
using KswApi.Logic.Rendering;
using KswApi.Poco.Content;
using KswApi.Test.Framework;
using KswApi.Test.Framework.DataGenerators;
using KswApi.Test.Framework.Fakes;
using KswApi.Test.Framework.Objects;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace KswApi.Test.Rendering
{
	[TestClass]
	public class RenderingLogicTests
	{
		private Mock<IRenderingModeFulfiller> _mockRenderingFulfiller;
		private Mock<IRenderingModeFulfillerFactory> _mockRenderingFulfillerFactory;

		[TestInitialize]
		public void TestInitialize()
		{
			_mockRenderingFulfillerFactory = new Mock<IRenderingModeFulfillerFactory>();
			_mockRenderingFulfiller = new Mock<IRenderingModeFulfiller>();
			_mockRenderingFulfillerFactory.Setup(m => m.GetRenderer(It.IsAny<RenderMode>()))
				.Returns(_mockRenderingFulfiller.Object);
			Dependency.Set(_mockRenderingFulfillerFactory.Object);
		}

		[TestMethod]
		public void Render_NullRequest_ThrowsServiceException()
		{
			FakeStreamResponse stream = new FakeStreamResponse();
			ServiceException exception = Expect.Exception<ServiceException>(() => new RenderingLogic().Render(null, new AccessToken(), stream));
			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void Render_NoTemplatesOrContentSpecified_ThrowsServiceException()
		{
			FakeStreamResponse stream = new FakeStreamResponse();
			ServiceException exception = Expect.Exception<ServiceException>(() => new RenderingLogic().Render(new RenderRequest(), new AccessToken(), stream));
			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
			_mockRenderingFulfiller.Verify(m => m.Render(
				It.IsAny<RenderRequest>(),
				It.IsAny<KswApiTemplate>(),
				It.IsAny<IList<ContentArticleResponse>>(),
				It.IsAny<IList<ContentArticleResponse>>()),
				Times.Never());
		}

		//[TestMethod]
		//public void Render_NonNullRequest_DoesNotThrowsServiceException()
		//{
		//	// arrange
		//	FakeStreamResponse stream = new FakeStreamResponse();
		//	//RenderRequest request = new RenderRequest();
		//	//request.Template = new List<ItemReference>
		//	//{
		//	//	new ItemReference { Bucket = "Bucket", Id = "Id" },
		//	//	RenderMode = RenderMode.Print,
		//	//	RecipientIdentifier = "1",
		//	//};
		//	RenderRequest request = new RenderRequest
		//	{
		//		Content = new List<ItemReference>
		//		{
		//			new ItemReference
		//			{
		//				Bucket = "Bucket",
		//				Id = "Id"
		//			}
		//		},
		//		RenderMode = RenderMode.Print,
		//		RecipientIdentifier = "1",
		//		//From = "from",
		//		//To = "to",
		//		//Age = 30,
		//		//Gender = Gender.Male
		//	};

		//	// act
		//	RenderResponse renderResponse = new RenderingLogic().Render(request, new AccessToken(), stream);

		//	// assert
		//	Assert.IsNotNull(renderResponse);
		//}

		[TestMethod]
		public void Render_ContentIdSpecified_LeveragesContent()
		{
			// arrange
			FakeLayer fakeLayer = FakeLayer.Setup();
			SetupTaxonomyTypes(fakeLayer);
			FakeContent content = ContentGenerator.GenerateContent(fakeLayer);
			RenderRequest request = new RenderRequest
			{
				Content = new List<ItemReference>
				{
					new ItemReference
					{
						BucketIdOrSlug = content.Bucket.Slug,
						IdOrSlug = content.Version.Slug
					}
				},
				RenderMode = RenderMode.Print,
				RecipientIdentifier = "1",
				From = "from",
				To = "to",
				Age = 30,
				Gender = Gender.Male
			};
			FakeStreamResponse stream = new FakeStreamResponse();
			IList<ContentArticleResponse> actualContent = null;
			_mockRenderingFulfiller.Setup(m => m.Render(
				It.IsAny<RenderRequest>(),
				It.IsAny<KswApiTemplate>(),
				It.IsAny<IList<ContentArticleResponse>>(),
				It.IsAny<IList<ContentArticleResponse>>()))
				.Callback<RenderRequest, KswApiTemplate, IList<ContentArticleResponse>,
				IList<ContentArticleResponse>>(
				(r, t, t2, c) => { actualContent = c; });
			fakeLayer.Settings.Renderers = new List<RenderingConfiguration>();
			fakeLayer.Settings.Renderers.Add(new RenderingConfiguration
			{
				Mode = "Print",
				Options = new Dictionary<string, string> { { "outputPath", @"C:\DropFolderTemp" } },
				Type = null
			});

			// act
			new RenderingLogic().Render(request, new AccessToken(), stream);

			// assert
			Assert.IsNotNull(actualContent);
			Assert.AreEqual(1, actualContent.Count);
			Assert.AreEqual("The body", actualContent[0].Segments[0].Body);
		}

		[TestMethod]
		public void Render_FromNotSpecified_DoesNotCreateEntryInKswApiTemplate()
		{
			// arrange
			FakeLayer fakeLayer = FakeLayer.Setup();
			SetupTaxonomyTypes(fakeLayer);
			FakeContent content = ContentGenerator.GenerateContent(fakeLayer);
			RenderRequest request = new RenderRequest
			{
				Content = new List<ItemReference>
				{
					new ItemReference
					{
						BucketIdOrSlug = content.Bucket.Slug,
						IdOrSlug = content.Version.Slug
					}
				},
				RenderMode = RenderMode.Print,
				RecipientIdentifier = "1",
			};
			FakeStreamResponse stream = new FakeStreamResponse();
			KswApiTemplate actualKswApiTemplate = null;
			_mockRenderingFulfiller.Setup(m => m.Render(
				It.IsAny<RenderRequest>(),
				It.IsAny<KswApiTemplate>(),
				It.IsAny<IList<ContentArticleResponse>>(),
				It.IsAny<IList<ContentArticleResponse>>()))
				.Callback<RenderRequest, KswApiTemplate, IList<ContentArticleResponse>,
				IList<ContentArticleResponse>>(
				(r, t, t2, c) => { actualKswApiTemplate = t; });
			fakeLayer.Settings.Renderers = new List<RenderingConfiguration>();
			fakeLayer.Settings.Renderers.Add(new RenderingConfiguration
			{
				Mode = "Print",
				Options = new Dictionary<string, string> { { "outputPath", @"C:\DropFolderTemp" } },
				Type = null
			});

			// act
			new RenderingLogic().Render(request, new AccessToken(), stream);

			// assert
			Assert.IsNotNull(actualKswApiTemplate);
			Assert.IsFalse(actualKswApiTemplate.Fields.Any(f => f.Name == "From"));
		}

		private string GetStreamResponseAsString(StreamResponse stream)
		{
			stream.Position = 0;
			using (StreamReader reader = new StreamReader(stream, Encoding.UTF8))
				return reader.ReadToEnd();
		}

		private void SetupTaxonomyTypes(FakeLayer fakeLayer)
		{
			fakeLayer.DataLayer.Setup(
				new TaxonomyType { Name = "ICD-9", Slug = "icd-9", Regex = @"^(V\d{2}(\.\d{1,2})?|\d{3}(\.\d{1,2})?|E\d{3}(\.\d)?)$" },
				new TaxonomyType { Name = "ICD-10 PCS", Slug = "icd-10pcs", Regex = @"^[A-TV-Z][0-9][A-Z0-9]\.[A-Z0-9]{4}$" }
				);
		}
	}
}

