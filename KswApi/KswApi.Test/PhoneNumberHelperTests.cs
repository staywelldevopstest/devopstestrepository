﻿using KswApi.Logic.Framework.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable InconsistentNaming

namespace KswApi.Test
{
	[TestClass]
	public class PhoneNumberHelperTests
	{
		[TestMethod]
		public void GetStandardNumber_TenDigits_Success()
		{
			string number = PhoneNumberHelper.GetStandardNumber("1234567890");

			Assert.AreEqual(number, "1234567890");
		}

		[TestMethod]
		public void GetStandardNumber_ElevenDigitsStartingWithOne_Success()
		{
			string number = PhoneNumberHelper.GetStandardNumber("12345678900");

			Assert.AreEqual(number, "2345678900");
		}

		[TestMethod]
		public void GetStandardNumber_ElevenDigitsNotStartingWithOne_Success()
		{
			string number = PhoneNumberHelper.GetStandardNumber("22345678900");

			Assert.AreEqual(number, "22345678900");
		}

		[TestMethod] public void GetStandardNumber_TwelveDigitsStartingWithPlusOne_Success()
		{
			string number = PhoneNumberHelper.GetStandardNumber("+12345678900");

			Assert.AreEqual(number, "2345678900");
		}
	}
}
