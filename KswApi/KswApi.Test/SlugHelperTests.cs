﻿using KswApi.Utility.Helpers;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable InconsistentNaming

namespace KswApi.Test
{
	[TestClass]
	public class SlugHelperTests
	{
		[TestMethod]
		public void SlugHelper_CreateSlug_Succeeds()
		{
			string slug = SlugHelper.CreateSlug("Wellness Library");
			Assert.AreEqual("wellness-library", slug);
		}
	}
}
