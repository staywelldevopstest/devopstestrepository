﻿using KswApi.Common.Objects;
using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logic;
using KswApi.Logic.Exceptions;
using KswApi.Poco.Administration;
using KswApi.Repositories.Objects;
using KswApi.RestrictedInterface.Objects;
using KswApi.Test.Framework;
using KswApi.Test.Framework.Fakes;
using KswApi.Test.Framework.TestUtilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;

// ReSharper disable InconsistentNaming

namespace KswApi.Test
{
	[TestClass]
	public class OAuthLogicTests
	{
		[TestMethod]
		public void ValidateClient_ValidRequestSucceeds()
		{
			const string state = "testState";
			const string redirectUrl = "http://myurl.com";
			Guid identifier = Guid.NewGuid();

			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.Setup(new Application
			{
				Id = identifier,
				Name = "test",
				RedirectUri = redirectUrl
			});

			OAuthLogic logic = new OAuthLogic();
			ApplicationValidationResponse response = logic.GetApplicationValidation(
				"code",
				identifier.ToString(),
				"HTTPS://MYURL.COM",
				null,
				state);

			Assert.IsNotNull(response);
			Assert.IsNull(response.Warning);
		}

		[TestMethod]
		public void ValidateClient_InvalidScopeFails()
		{
			Guid guid = Guid.NewGuid();
			const string scope = "This is an \"invalid scope";
			OAuthLogic logic = new OAuthLogic();

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.GetApplicationValidation(
				"code",
				guid.ToString(),
				"HTTPS://MYURL.COM",
				scope,
				null
			));

			Assert.IsNotNull(exception);
			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}


		[TestMethod]
		public void ValidateClient_ResponseTypeNotCodeFails()
		{
			Guid identifier = Guid.NewGuid();
			const string redirectUrl = "http://myurl.com";
			const string state = "testState";

			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.Setup(new Application
			{
				Id = identifier,
				Name = "test",
				RedirectUri = redirectUrl
			});

			OAuthLogic logic = new OAuthLogic();

			ServiceRedirectException exception = Expect.Exception<ServiceRedirectException>(() => logic.GetApplicationValidation(
						"notcode",
						identifier.ToString(),
						redirectUrl,
						null,
						state));

			Assert.IsNotNull(exception);
			Regex regex = new Regex(@"http\:\/\/myurl\.com\?error=unsupported_response_type.*");
			Assert.IsTrue(regex.IsMatch(exception.RedirectUri));
		}

		[TestMethod]
		public void ValidateClient_RedirectUrlNotMatchingFails()
		{
			const string state = "testState";
			const string redirectUrl = "http://myurl.com";
			Guid identifier = Guid.NewGuid();

			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.Setup(new Application
			{
				Id = identifier,
				Name = "test",
				RedirectUri = redirectUrl
			});

			OAuthLogic logic = new OAuthLogic();
			ServiceException exception = Expect.Exception<ServiceException>(() => logic.GetApplicationValidation(
						"code",
						identifier.ToString(),
						"http://myotherurl.com",
						null,
						state));

			Assert.IsNotNull(exception);
			Assert.AreEqual(exception.StatusCode, HttpStatusCode.BadRequest);
		}

		[TestMethod]
		public void ValidateClient_RedirectUriWithFragmentFails()
		{
			const string state = "testState";
			OAuthLogic logic = new OAuthLogic();

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.GetApplicationValidation(
						"code",
						Guid.Empty.ToString(),
						"http://myurl.com#something",
						null,
						state));

			Assert.IsNotNull(exception);
			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void CreateAuthorizationGrant_ValidRequestSucceeds()
		{
			const string state = "testState";
			const string redirectUrl = "http://myurl.com";
			Guid identifier = Guid.NewGuid();

			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.Setup(new Application
			{
				Id = identifier,
				Name = "test",
				RedirectUri = redirectUrl
			});

			fakeLayer.UserAuthenticator.Add("test", "test", GetTestUser());

			OAuthLogic logic = new OAuthLogic();
			AuthorizationGrantResponse response = logic.CreateAuthorizationGrant(new AuthorizationGrantRequest
																		{
																			ResponseType = "code",
																			ApplicationId = identifier.ToString(),
																			RedirectUri = "HTTPS://MYURL.COM",
																			UserName = "test",
																			Password = "test",
																			Scope = null,
																			State = state
																		});

			Assert.IsNotNull(response);
			Assert.IsNotNull(response.RedirectUri);
			Regex regex = new Regex(@"HTTPS\:\/\/MYURL\.COM\?code\=([a-zA-Z0-9.-]{64})\&state\=testState");
			Assert.IsTrue(regex.IsMatch(response.RedirectUri));
		}

		[TestMethod]
		public void CreateAuthorizationGrant_ValidRequestUpdatesDatabaseAndCache()
		{
			const string state = "testState";
			const string redirectUrl = "http://myurl.com";
			Guid identifier = Guid.NewGuid();

			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.Setup(new Application
			{
				Id = identifier,
				Name = "test",
				RedirectUri = redirectUrl
			});

			fakeLayer.UserAuthenticator.Add("test", "test", GetTestUser());

			OAuthLogic logic = new OAuthLogic();

			AuthorizationGrantResponse response = logic.CreateAuthorizationGrant(new AuthorizationGrantRequest
				{
					ResponseType = "code",
					ApplicationId = identifier.ToString(),
					RedirectUri = "HTTPS://MYURL.COM",
					UserName = "test",
					Password = "test",
					Scope = null,
					State = state
				});

			Assert.AreEqual(1, fakeLayer.DataLayer.CommitCount);

			Regex regex = new Regex(@"HTTPS\:\/\/MYURL\.COM\?code\=([a-zA-Z0-9.-]{64})\&state\=testState");
			Match match = regex.Match(response.RedirectUri);
			string code = match.Groups[1].Value;

			AuthorizationGrant grantFromDatabase = fakeLayer.DataLayer.GetFakeSet<AuthorizationGrant>().SingleOrDefault();

			Assert.IsNotNull(grantFromDatabase);
			Assert.AreEqual(code, grantFromDatabase.Code);
			Assert.AreEqual(grantFromDatabase.State, "testState");
		}

		[TestMethod]
		public void CreateAuthorizationGrant_InvalidScopeFails()
		{
			Guid guid = Guid.NewGuid();
			const string scope = @"This is an \invalid scope";

			OAuthLogic logic = new OAuthLogic();

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.CreateAuthorizationGrant(new AuthorizationGrantRequest
			{
				ResponseType = "code",
				ApplicationId = guid.ToString(),
				RedirectUri = "HTTPS://MYURL.COM",
				UserName = "test",
				Password = "test",
				Scope = scope,
				State = null
			}));

			Assert.IsNotNull(exception);
			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void CreateAccessToken_UnknownGrantType_Fails()
		{
			Guid guid = Guid.NewGuid();
			OAuthLogic logic = new OAuthLogic();

			OAuthTokenRequest request = new OAuthTokenRequest
									   {
										   grant_type = "unknown_grant_type",
										   client_id = guid.ToString(),
										   client_secret = "This is the client secret"
									   };

			OAuthException exception = Expect.Exception<OAuthException>(() => logic.CreateAccessToken(request, new NameValueCollection(), null));

			Assert.IsNotNull(exception);
			Assert.AreEqual(OAuthErrorCode.UnsupportedGrantType, exception.ErrorCode);
			Assert.AreEqual("unsupported_grant_type", exception.ErrorCodeString);
			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void CreateAccessToken_AuthorizationCode_ValidRequestSucceeds()
		{
			Guid identifier = Guid.NewGuid();
			const string clientSecret = "This is the client secret";

			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.Setup(new Application
			{
				Id = identifier,
				Name = "Test",
				RedirectUri = null,
				Secret = clientSecret
			});

			fakeLayer.DataLayer.Setup(new AuthorizationGrant
											{
												Id = identifier,
												Code = "The authorization code",
												RedirectUri = "http://myurl.com",
												Expiration = DateTime.UtcNow.AddMinutes(3),
												UserDetails = GetTestUser()
											});

			OAuthLogic logic = new OAuthLogic();

			OAuthTokenRequest request = new OAuthTokenRequest
			{
				grant_type = "authorization_code",
				code = "The authorization code",
				client_id = identifier.ToString(),
				client_secret = "This is the client secret",
				redirect_uri = "http://myurl.com"
			};

			OAuthTokenResponse response = logic.CreateAccessToken(request, new NameValueCollection(), null);

			Assert.IsNotNull(response);

			Regex regex = new Regex(@"[a-zA-Z0-9.-]{64}");
			Assert.IsTrue(regex.IsMatch(response.access_token));
			Assert.AreEqual(response.token_type, "bearer");
		}

		[TestMethod]
		public void CreateAccessToken_AuthorizationCode_AuthorizationNotFoundFails()
		{
			Guid identifier = Guid.NewGuid();
			const string clientSecret = "This is the client secret";

			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.Setup(new Application
			{
				Id = identifier,
				Name = "Test",
				RedirectUri = null,
				Secret = clientSecret
			});

			fakeLayer.DataLayer.Setup(new AuthorizationGrant
			{
				Id = identifier,
				Code = "The authorization code",
				RedirectUri = "http://myurl.com",
				Expiration = DateTime.UtcNow.AddMinutes(3)
			});

			OAuthLogic logic = new OAuthLogic();

			OAuthTokenRequest request = new OAuthTokenRequest
			{
				grant_type = "authorization_code",
				code = "Not the authorization code",
				client_id = identifier.ToString(),
				client_secret = "This is the client secret",
				redirect_uri = "http://myurl.com"
			};

			OAuthException exception = Expect.Exception<OAuthException>(() => logic.CreateAccessToken(request, new NameValueCollection(), null));

			Assert.IsNotNull(exception);
			Assert.AreEqual(OAuthErrorCode.InvalidGrant, exception.ErrorCode);
			Assert.AreEqual("invalid_grant", exception.ErrorCodeString);
			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void CreateAccessToken_AuthorizationCode_WrongSecretFails()
		{
			Guid identifier = Guid.NewGuid();
			const string clientSecret = "This is the client secret";

			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.Setup(new Application
			{
				Id = identifier,
				Name = "Test",
				RedirectUri = null,
				Secret = clientSecret
			});

			fakeLayer.DataLayer.Setup(new AuthorizationGrant
			{
				Id = identifier,
				Code = "The authorization code",
				RedirectUri = "http://myurl.com",
				Expiration = DateTime.UtcNow.AddMinutes(3)
			});

			OAuthLogic logic = new OAuthLogic();

			OAuthTokenRequest request = new OAuthTokenRequest
			{
				grant_type = "authorization_code",
				code = "The authorization code",
				client_secret = "This is not the client secret",
				redirect_uri = "http://myurl.com"
			};

			OAuthException exception = Expect.Exception<OAuthException>(() => logic.CreateAccessToken(request, new NameValueCollection(), null));

			Assert.IsNotNull(exception);
			Assert.AreEqual(OAuthErrorCode.InvalidClient, exception.ErrorCode);
			Assert.AreEqual("invalid_client", exception.ErrorCodeString);
			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void CreateAccessToken_AuthorizationCode_DifferentRedirectUriFails()
		{
			Guid identifier = Guid.NewGuid();
			const string clientSecret = "This is the client secret";

			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.Setup(new Application
			{
				Id = identifier,
				Name = "Test",
				RedirectUri = null,
				Secret = clientSecret
			});

			fakeLayer.DataLayer.Setup(new AuthorizationGrant
			{
				Id = identifier,
				Code = "The authorization code",
				RedirectUri = "http://myurl.com",
				Expiration = DateTime.UtcNow.AddMinutes(3)
			});

			OAuthLogic logic = new OAuthLogic();

			OAuthTokenRequest request = new OAuthTokenRequest
			{
				grant_type = "authorization_code",
				code = "The authorization code",
				client_id = identifier.ToString(),
				client_secret = "This is the client secret",
				redirect_uri = "http://myotherurl.com"
			};

			OAuthException exception = Expect.Exception<OAuthException>(() => logic.CreateAccessToken(request, new NameValueCollection(), null));

			Assert.IsNotNull(exception);
			Assert.AreEqual(OAuthErrorCode.InvalidRequest, exception.ErrorCode);
			Assert.AreEqual("invalid_request", exception.ErrorCodeString);
			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void CreateAccessToken_ClientCredentials_ThrowsNoLicenseException()
		{
			Guid identifier = Guid.NewGuid();
			const string clientSecret = "This is the client secret";

			FakeLayer fakeLayer = FakeLayer.Setup();
			fakeLayer.DataLayer.Setup(new Application
													{
														Id = identifier,
														Name = "Test",
														LicenseId = Guid.NewGuid(),
														RedirectUri = null,
														Secret = clientSecret
													});

			OAuthLogic logic = new OAuthLogic();

			OAuthTokenRequest request = new OAuthTokenRequest
			{
				grant_type = "client_credentials",
				client_id = identifier.ToString(),
				client_secret = "This is the client secret",
				redirect_uri = "http://myotherurl.com"
			};

			ExceptionAssertionUtility.AssertExceptionThrown("License not found", typeof(ServiceException), () => logic.CreateAccessToken(request, new NameValueCollection(), null));
		}

		[TestMethod]
		public void CreateAccessToken_ClientCredentials_ValidRequestSucceeds()
		{
			Guid identifier = Guid.NewGuid();
			const string clientSecret = "This is the client secret";

			FakeLayer fakeLayer = FakeLayer.Setup();
			fakeLayer.DataLayer.Setup(new Application
			{
				Id = identifier,
				Name = "Test",
				RedirectUri = null,
				Secret = clientSecret
			});

			fakeLayer.DataLayer.Setup(new License
				{
					Applications = new List<Guid> { identifier }
				});

			OAuthLogic logic = new OAuthLogic();

			OAuthTokenRequest request = new OAuthTokenRequest
			{
				grant_type = "client_credentials",
				client_id = identifier.ToString(),
				client_secret = "This is the client secret",
				redirect_uri = "http://myotherurl.com"
			};

			OAuthTokenResponse response = logic.CreateAccessToken(request, new NameValueCollection(), null);

			Assert.IsNotNull(response);
			Regex regex = new Regex(@"[a-zA-Z0-9.-]{64}");
			Assert.IsTrue(regex.IsMatch(response.access_token));
			Assert.AreEqual(response.token_type, "bearer");
			Assert.IsNull(response.refresh_token);
		}

		[TestMethod]
		public void CreateAccessToken_ClientCredentials_WrongSecretFails()
		{
			Guid identifier = Guid.NewGuid();
			const string clientSecret = "This is the client secret";

			FakeLayer fakeLayer = FakeLayer.Setup();
			fakeLayer.DataLayer.Setup(new Application
			{
				Id = identifier,
				Name = "Test",
				RedirectUri = null,
				Secret = clientSecret
			});

			OAuthLogic logic = new OAuthLogic();

			OAuthTokenRequest request = new OAuthTokenRequest
			{
				grant_type = "client_credentials",
				client_id = identifier.ToString(),
				client_secret = "This is not the client secret"
			};

			OAuthException exception = Expect.Exception<OAuthException>(() => logic.CreateAccessToken(request, new NameValueCollection(), null));

			Assert.IsNotNull(exception);
			Assert.AreEqual(OAuthErrorCode.InvalidClient, exception.ErrorCode);
			Assert.AreEqual("invalid_client", exception.ErrorCodeString);
			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void CreateAccessToken_ClientCredentials_InvalidScopeFails()
		{
			Guid guid = Guid.NewGuid();
			const string scope = @"This is an \invalid scope";
			OAuthLogic logic = new OAuthLogic();

			OAuthTokenRequest request = new OAuthTokenRequest
			{
				grant_type = "client_credentials",
				client_id = guid.ToString(),
				client_secret = "This is the client secret",
				scope = scope
			};

			OAuthException exception = Expect.Exception<OAuthException>(() => logic.CreateAccessToken(request, new NameValueCollection(), null));

			Assert.IsNotNull(exception);
			Assert.AreEqual(OAuthErrorCode.InvalidScope, exception.ErrorCode);
			Assert.AreEqual("invalid_scope", exception.ErrorCodeString);
			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void AuthorizeOperation_PublicOperation_Succeeds()
		{
			FakeOperation operation = new FakeOperation(typeof(FakeService), "PublicOperation");

			OAuthLogic logic = new OAuthLogic();

			AllowAttribute allowed = new AllowAttribute
										 {
											 ClientType = ClientType.Public
										 };

			logic.AuthorizeOperation("123.123.123", null, null, allowed);
		}

		[TestMethod]
		public void AuthorizeOperation_AuthorizationOperationWithValidAccessToken_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid identifier = Guid.NewGuid();
			Guid cliendId = Guid.NewGuid();

			AccessToken accessToken = new AccessToken
										  {
											  Id = identifier,
											  ApplicationId = cliendId,
											  Token = "the_token",
											  Expiration = DateTime.UtcNow.AddDays(1),
											  TokenType = AccessTokenType.AuthorizationClient
										  };
			Application client = new Application
								{
									Id = cliendId,
									Addresses = new List<string>() { "123.123.123" }
								};

			AllowAttribute allowed = new AllowAttribute
			{
				ClientType = ClientType.Authentication
			};

			OAuthLogic logic = new OAuthLogic();

			logic.AuthorizeOperation("123.123.123", accessToken, client, allowed);
		}

		[TestMethod]
		public void AuthorizeOperation_AuthorizationOperationWithUserAccessToken_Fails()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid identifier = Guid.NewGuid();
			Guid guid = Guid.NewGuid();

			AccessToken accessToken = new AccessToken
										  {
											  Id = identifier,
											  ApplicationId = guid,
											  Token = "the_token",
											  Expiration = DateTime.UtcNow.AddDays(1),
											  TokenType = AccessTokenType.InternalUser
										  };

			Application client = new Application
								{
									Id = guid,
									Addresses = new List<string>() { "123.123.123" }
								};

			AllowAttribute allowed = new AllowAttribute
			{
				ClientType = ClientType.Authentication
			};


			OAuthLogic logic = new OAuthLogic();

			try
			{
				logic.AuthorizeOperation("123.123.123", accessToken, client, allowed);
			}
			catch (ServiceException exception)
			{
				Assert.AreEqual(exception.StatusCode, HttpStatusCode.Forbidden);

				return;
			}

			Assert.Fail();
		}

		[TestMethod]
		public void AuthorizeOperation_AuthorizationOperationWithValidAccessTokenAndValidRights_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid identifier = Guid.NewGuid();
			Guid clientId = Guid.NewGuid();

			AccessToken accessToken = new AccessToken
										  {
											  Id = identifier,
											  ApplicationId = clientId,
											  Token = "the_token",
											  Expiration = DateTime.UtcNow.AddDays(1),
											  Rights = new List<string>() { "TestRight" }
										  };

			Application client = new Application
								{
									Id = clientId,
									Addresses = new List<string>() { "123.123.123" }
								};

			AllowAttribute allowed = new AllowAttribute
			{
				ClientType = ClientType.Internal
			};


			OAuthLogic logic = new OAuthLogic();

			logic.AuthorizeOperation("123.123.123", accessToken, client, allowed);
		}

		[TestMethod]
		public void AuthorizeOperation_AuthorizationOperationWithValidAccessTokenAndInvalidRights_Fail()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid clientGuid = Guid.NewGuid();
			Guid identifier = Guid.NewGuid();

			AccessToken accessToken = new AccessToken
										  {
											  Id = identifier,
											  ApplicationId = clientGuid,
											  Token = "the_token",
											  Expiration = DateTime.UtcNow.AddDays(1),
											  Rights = new List<string>() { "NotRight" }
										  };

			Application client = new Application
								{
									Id = clientGuid,
									Addresses = new List<string>() { "123.123.123" }
								};

			AllowAttribute allowed = new AllowAttribute
			{
				ClientType = ClientType.Internal,
				Rights = "TestRight"
			};


			OAuthLogic logic = new OAuthLogic();

			try
			{
				logic.AuthorizeOperation("123.123.123", accessToken, client, allowed);
				Assert.Fail();
			}
			catch (ServiceException exception)
			{
				Assert.AreEqual(exception.StatusCode, HttpStatusCode.Forbidden);
			}
		}

		[TestMethod]
		public void AuthorizeOperation_AuthorizationOperationWithExpiredAccessToken_ReturnsUnauthorized()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid clientGuid = Guid.NewGuid();
			Guid identifier = Guid.NewGuid();

			AccessToken accessToken = new AccessToken
										  {
											  Id = identifier,
											  ApplicationId = clientGuid,
											  Token = "the_token"
										  };
			Application client = new Application
								{
									Id = clientGuid,
									Addresses = new List<string>() { "123.123.123" }
								};

			fakeLayer.DataLayer.Setup(accessToken);

			AllowAttribute allowed = new AllowAttribute
			{
				ClientType = ClientType.Authentication
			};

			OAuthLogic logic = new OAuthLogic();

			try
			{
				logic.AuthorizeOperation("123.123.123", accessToken, client, allowed);
				Assert.Fail("Authorization should fail");
			}
			catch (ServiceException exception)
			{
				Assert.AreEqual(exception.StatusCode, HttpStatusCode.Unauthorized);
			}
		}

		[TestMethod]
		public void AuthorizeOperation_AuthorizationOperationWithInvalidAccessToken_ReturnsUnauthorized()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid clientGuid = Guid.NewGuid();
			Guid identifier = Guid.NewGuid();

			Application client = new Application
								{
									Id = clientGuid,
									Addresses = new List<string>() { "123.123.123" }
								};

			AllowAttribute allowed = new AllowAttribute
			{
				ClientType = ClientType.Authentication
			};

			OAuthLogic logic = new OAuthLogic();

			try
			{
				logic.AuthorizeOperation("123.123.123", null, client, allowed);
				Assert.Fail("Authorization should fail");
			}
			catch (ServiceException exception)
			{
				Assert.AreEqual(exception.StatusCode, HttpStatusCode.Unauthorized);
			}
		}

		[TestMethod]
		public void AuthorizeOperation_AuthorizationOperationWithValidAccessTokenAndWrongAddress_ReturnsForbidden()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid clientGuid = Guid.NewGuid();
			Guid identifier = Guid.NewGuid();

			AccessToken accessToken = new AccessToken
										  {
											  Id = identifier,
											  ApplicationId = clientGuid,
											  Token = "the_token",
											  Expiration = DateTime.UtcNow.AddDays(1)
										  };

			Application client = new Application
								{
									Id = clientGuid,
									Addresses = new List<string> { "123.123.123" }
								};

			AllowAttribute allowed = new AllowAttribute
			{
				ClientType = ClientType.Authentication
			};

			OAuthLogic logic = new OAuthLogic();

			try
			{
				logic.AuthorizeOperation("123.123.122", accessToken, client, allowed);
				Assert.Fail("Authorization should fail");
			}
			catch (ServiceException exception)
			{
				Assert.AreEqual(exception.StatusCode, HttpStatusCode.Forbidden);
			}
		}

		[TestMethod]
		public void CreateAuthorizationAccessToken_ValidRequest_Succeeds()
		{
			Guid identifier = Guid.NewGuid();
			const string clientSecret = "This is the client secret";

			FakeLayer fakeLayer = FakeLayer.Setup();
			fakeLayer.DataLayer.Setup(new Application
			{
				Id = identifier,
				Name = "Test",
				RedirectUri = null,
				Secret = clientSecret,
				Type = ApplicationType.AdministrationPortal,
				Addresses = new List<string> { "123.123.123" }
			});

			OAuthLogic logic = new OAuthLogic();
			OAuthTokenResponse response = logic.CreateAccessToken(new OAuthTokenRequest { grant_type = "client_credentials", client_id = identifier.ToString(), client_secret = "This is the client secret" }, null, "123.123.123");

			Assert.IsNotNull(response);
			Regex regex = new Regex(@"[a-zA-Z0-9.-]{64}");
			Assert.IsTrue(regex.IsMatch(response.access_token));
			Assert.IsNull(response.refresh_token);
		}

		[TestMethod]
		public void CreateAuthorizationAccessToken_WrongClientAddress_Fails()
		{
			Guid identifier = Guid.NewGuid();
			const string clientSecret = "This is the client secret";

			FakeLayer fakeLayer = FakeLayer.Setup();
			fakeLayer.DataLayer.Setup(new Application
			{
				Id = identifier,
				Name = "Test",
				RedirectUri = null,
				Secret = clientSecret,
				Addresses = new List<string> { "123.123.123" }
			});

			OAuthLogic logic = new OAuthLogic();

			try
			{
				logic.CreateAccessToken(
					new OAuthTokenRequest
						{
							grant_type = "client_credentials",
							client_id = identifier.ToString(),
							client_secret = "This is the client secret"
						},
				null,
				"123.123.121");

				Assert.Fail("Should throw exception.");
			}
			catch (ServiceException exception)
			{
				Assert.AreEqual(exception.StatusCode, HttpStatusCode.Forbidden);
			}
		}

		[TestMethod]
		public void GetAccessTokenFromHeaders_ValidToken_Succeeds()
		{
			NameValueCollection headers = new NameValueCollection();
			headers["Authorization"] = "Bearer the_token";

			FakeLayer fakeLayer = FakeLayer.Setup();
			fakeLayer.DataLayer.Setup(new AccessToken
											{
												Token = "the_token"
											});

			OAuthLogic logic = new OAuthLogic();
			AccessToken accessToken = logic.GetAccessTokenFromHeaders(headers);

			Assert.IsNotNull(accessToken);
			Assert.AreEqual(accessToken.Token, "the_token");
		}

		[TestMethod]
		public void GetClientByAccessToken_ValidToken_Succeeds()
		{
			Guid clientGuid = Guid.NewGuid();

			FakeLayer fakeLayer = FakeLayer.Setup();
			fakeLayer.DataLayer.Setup(new Application
			{
				Id = clientGuid,
				Name = "Test",
				RedirectUri = null,
				Addresses = new List<string> { "123.123.123" }
			});

			AccessToken token = new AccessToken
									{
										ApplicationId = clientGuid
									};

			OAuthLogic logic = new OAuthLogic();
			Application client = logic.GetApplicationByAccessToken(token);

			Assert.IsNotNull(client);
			Assert.AreEqual(client.Name, "Test");
		}

		[TestMethod]
		public void CreateAccessToken_ValidRefreshToken_Succeeds()
		{
			Guid guid = Guid.NewGuid();

			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.Settings.AccessToken.Expiration = TimeSpan.FromSeconds(123);

			fakeLayer.DataLayer.Setup(new RefreshToken
			{
				Token = "Valid refresh token",
				Expiration = DateTime.UtcNow.AddSeconds(30)
			});

			fakeLayer.DataLayer.Setup(new Application
											{
												Id = guid,
												Secret = "This is the client secret"
											});


			OAuthTokenRequest request = new OAuthTokenRequest
			{
				grant_type = "refresh_token",
				client_id = guid.ToString(),
				client_secret = "This is the client secret",
				refresh_token = "Valid refresh token"
			};

			OAuthLogic logic = new OAuthLogic();

			OAuthTokenResponse response = logic.CreateAccessToken(request, new NameValueCollection(), null);

			Assert.IsNotNull(response);
			Assert.IsTrue(!string.IsNullOrEmpty(response.access_token));
			Assert.AreEqual(123, response.expires_in);
		}

		[TestMethod]
		public void CreateAccessToken_InvalidRefreshToken_Fails()
		{
			Guid guid = Guid.NewGuid();

			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.Settings.AccessToken.Expiration = TimeSpan.FromSeconds(123);

			fakeLayer.DataLayer.Setup(new RefreshToken
			{
				Token = "Valid refresh token",
				Expiration = DateTime.UtcNow.AddSeconds(30)
			});

			fakeLayer.DataLayer.Setup(new Application
			{
				Id = guid,
				Secret = "This is the client secret"
			});


			OAuthTokenRequest request = new OAuthTokenRequest
			{
				grant_type = "refresh_token",
				client_id = guid.ToString(),
				client_secret = "This is the client secret",
				refresh_token = "Invalid refresh token"
			};

			OAuthLogic logic = new OAuthLogic();

			OAuthException exception = Expect.Exception<OAuthException>(() => logic.CreateAccessToken(request, new NameValueCollection(), null));

			Assert.IsNotNull(exception);
			Assert.AreEqual(OAuthErrorCode.InvalidGrant, exception.ErrorCode);
			Assert.AreEqual("invalid_grant", exception.ErrorCodeString);
		}

		[TestMethod]
		public void CreateAccessToken_ExpiredRefreshToken_Fails()
		{
			Guid guid = Guid.NewGuid();

			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.Settings.AccessToken.Expiration = TimeSpan.FromSeconds(123);

			fakeLayer.DataLayer.Setup(new RefreshToken
			{
				Token = "Valid refresh token",
				Expiration = DateTime.UtcNow.AddSeconds(-1)
			});

			fakeLayer.DataLayer.Setup(new Application
			{
				Id = guid,
				Secret = "This is the client secret"
			});


			OAuthTokenRequest request = new OAuthTokenRequest
			{
				grant_type = "refresh_token",
				client_id = guid.ToString(),
				client_secret = "This is the client secret",
				refresh_token = "Valid refresh token"
			};

			OAuthLogic logic = new OAuthLogic();

			OAuthException exception = Expect.Exception<OAuthException>(() => logic.CreateAccessToken(request, new NameValueCollection(), null));

			Assert.IsNotNull(exception);
			Assert.AreEqual(OAuthErrorCode.InvalidGrant, exception.ErrorCode);
			Assert.AreEqual("invalid_grant", exception.ErrorCodeString);
		}

		private UserAuthenticationDetails GetTestUser()
		{
			return new UserAuthenticationDetails
			{
				Id = Guid.NewGuid(),
				EmailAddress = "tuser@test.net",
				FirstName = "Test",
				LastName = "User",
				MiscInfo = "CN=test_admin,OU=KSWAPI,OU=Salt Lake,DC=medimedia,DC=com",
				Roles = new List<string> { "Test Group" }
			};
		}

		[TestMethod]
		public void ValidateAccessToken_Succeeds()
		{
			Guid applicationId = Guid.NewGuid();
			Guid tokenId = Guid.NewGuid();
			string tokenValue = "TheTokenValue";

			FakeLayer fake = FakeLayer.Setup();
			fake.DataLayer.Setup(new Application
			                     {
									 Id = applicationId
			                     });

			fake.DataLayer.Setup(new AccessToken
								 {
									 Id = tokenId,
									 Token = tokenValue,
									 State = "the state",
									 ApplicationId = applicationId,
									 Expiration = DateTime.UtcNow + TimeSpan.FromMinutes(10),
									 Rights = new List<string> { "test right" }
								 });

			OAuthLogic logic = new OAuthLogic();

			ValidationResponse response = logic.ValidateAccessToken(tokenValue);

			Assert.IsNotNull(response);
			Assert.AreEqual(true, response.Valid);
			Assert.AreEqual("the state", response.State);
			Assert.IsNotNull(response.Rights);
			Assert.AreEqual(1, response.Rights.Count);
			Assert.AreEqual("test right", response.Rights[0]);
		}

		[TestMethod]
		public void ValidateAccessToken_ExpiredToken_ReturnsInvalid()
		{
			Guid applicationId = Guid.NewGuid();
			Guid tokenId = Guid.NewGuid();
			string tokenValue = "TheTokenValue";

			FakeLayer fake = FakeLayer.Setup();
			fake.DataLayer.Setup(new Application
			{
				Id = applicationId
			});

			fake.DataLayer.Setup(new AccessToken
			{
				Id = tokenId,
				Token = tokenValue,
				State = "the state",
				ApplicationId = applicationId,
				Expiration = DateTime.UtcNow - TimeSpan.FromMinutes(1)
			});

			OAuthLogic logic = new OAuthLogic();

			ValidationResponse response = logic.ValidateAccessToken(tokenValue);

			Assert.IsNotNull(response);
			Assert.AreEqual(false, response.Valid);
			Assert.AreEqual(null, response.State);
			Assert.IsNull(response.Rights);
		}

		[TestMethod]
		public void ValidateAccessToken_TokenDoesNotMatchApplication_ReturnsInvalid()
		{
			Guid applicationId = Guid.NewGuid();
			Guid tokenId = Guid.NewGuid();
			string tokenValue = "TheTokenValue";

			FakeLayer fake = FakeLayer.Setup();
			fake.DataLayer.Setup(new Application
			{
				Id = applicationId
			});

			fake.DataLayer.Setup(new AccessToken
			{
				Id = tokenId,
				Token = tokenValue,
				State = "the state",
				ApplicationId = Guid.NewGuid(),
				Expiration = DateTime.UtcNow - TimeSpan.FromMinutes(1)
			});

			OAuthLogic logic = new OAuthLogic();

			ValidationResponse response = logic.ValidateAccessToken(tokenValue);

			Assert.IsNotNull(response);
			Assert.AreEqual(false, response.Valid);
			Assert.AreEqual(null, response.State);
		}
	}
}
