﻿using KswApi.Common.Objects;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects.SmsGateway;
using KswApi.Logic.SmsLogic;
using KswApi.Poco.Sms;
using KswApi.Repositories.Enums;
using KswApi.Repositories.Objects;
using KswApi.Test.Framework.Fakes;
using KswApi.Test.Framework.TestUtilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Linq;

// ReSharper disable InconsistentNaming

namespace KswApi.Test
{
    [TestClass]
    public class SmsGatewayMessageLogicTests
    {
        private const string TEST_KEYWORD = "TESTWORD";
        private const string TEST_KEYWORD2 = "TESTWORD2";
        private const string TEST_KEYWORD3 = "TESTWORD3";

        private const string WEBSITE = "test.com/testurl";

        private const string SUBSCRIBER_NUM = "8015555555";
        private const string SHORT_CODE = "55555";
        private const string LONG_CODE = "8011234567";

        [TestMethod]
        public void SendMessage_MessageIsNull_ThrowsException()
        {
            SmsMessageLogic logic = new SmsMessageLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("No message was specified in the message body.", typeof(ServiceException), () => logic.SendSmsMessage(null, null));
        }

        //[TestMethod]
        //public void SendMessage_SuspendedStoppedSession_ThrowsException()
        //{
        //    FakeLayer fakeLayer = FakeLayer.Setup();

        //    SmsMessageLogic logic = new SmsMessageLogic();

        //    Guid sessionId = Guid.NewGuid();

        //    fakeLayer.DataLayer.Setup(new SmsSession { Id = sessionId, State = SmsSessionState.Suspended });

        //    SmsIncomingMessage message = new SmsIncomingMessage { From = SHORT_CODE, To = SUBSCRIBER_NUM, Body = "Test", Keyword = TEST_KEYWORD };

        //    ExceptionAssertionUtility.AssertExceptionThrown("Unable to send a message to a subscriber who has opt-ed out of the program.", typeof(ServiceException), () => logic.SendSmsMessage(message));
        //}

        [TestMethod]
        public void SendMessage_GoodSession_SendsMessage()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            SmsMessageLogic logic = new SmsMessageLogic();

            Guid sessionId = Guid.NewGuid();

            Guid licenseId = Guid.NewGuid();

            fakeLayer.DataLayer.Setup(new SmsSession { Id = sessionId, State = SmsSessionState.Active, ServiceNumber = SHORT_CODE, SubscriberNumber = SUBSCRIBER_NUM, Keyword = TEST_KEYWORD });
            fakeLayer.DataLayer.Setup(new SmsKeywordData { Id = Guid.NewGuid(), Keyword = TEST_KEYWORD, Number = SHORT_CODE, LicenseId = licenseId });

            IncomingMessage message = new IncomingMessage { From = SHORT_CODE, To = SUBSCRIBER_NUM, Body = "Test", Keyword = TEST_KEYWORD };

            AccessToken token = new AccessToken
            {
                LicenseId = licenseId
            };


            logic.SendSmsMessage(message, token);

            fakeLayer.MockSmsService.Verify(service => service.SendMessage(SUBSCRIBER_NUM, SHORT_CODE, "Test"), Times.Once());
        }

        //[TestMethod]
        //public void SendMessage_LongMessage_BreaksUpMessage()
        //{
        //    FakeLayer fakeLayer = FakeLayer.Setup();

        //    SmsMessageLogic logic = new SmsMessageLogic();

        //    Guid sessionId = Guid.NewGuid();

        //    const string longMessage = "01234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789";

        //    fakeLayer.DataLayer.Setup(new SmsSession { Id = sessionId, State = SmsSessionState.Active, ServiceNumber = SHORT_CODE, SubscriberNumber = SUBSCRIBER_NUM });

        //    SmsIncomingMessage message = new SmsIncomingMessage { From = SHORT_CODE, To = SUBSCRIBER_NUM, Body = longMessage };

        //    logic.SendSmsMessage(message);

        //    fakeLayer.MockSmsService.Verify(service => service.SendMessage(SUBSCRIBER_NUM, SHORT_CODE, "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789"), Times.Once());
        //    fakeLayer.MockSmsService.Verify(service => service.SendMessage(SUBSCRIBER_NUM, SHORT_CODE, "0123456789"), Times.Once());
        //}

        [TestMethod]
        public void SendMessage_CatchAll_SendsMessageCreatesSubscriberAndSession()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            SmsMessageLogic logic = new SmsMessageLogic();

            Guid numberId = Guid.NewGuid();

            Guid licenseId = Guid.NewGuid();

            fakeLayer.DataLayer.Setup(new SmsNumberData
                                          {
                                              Format = MessageFormat.Json,
                                              Id = numberId,
                                              Keywords = null,
                                              LicenseId = licenseId,
                                              Number = LONG_CODE,
                                              RouteType = SmsNumberRouteType.CatchAll,
                                              Type = SmsNumberType.LongCode,
                                              Website = WEBSITE
                                          });

            IncomingMessage message = new IncomingMessage { From = LONG_CODE, To = SUBSCRIBER_NUM, Body = "Test", Keyword = null };

            AccessToken token = new AccessToken
            {
                LicenseId = licenseId
            };

            logic.SendSmsMessage(message, token);

            fakeLayer.MockSmsService.Verify(service => service.SendMessage(SUBSCRIBER_NUM, LONG_CODE, "Test"), Times.Once());
            FakeDataSet<SmsSubscriber> subscribers = fakeLayer.DataLayer.GetFakeSet<SmsSubscriber>();
            FakeDataSet<SmsSession> sessions = fakeLayer.DataLayer.GetFakeSet<SmsSession>();

            Assert.AreEqual(1, subscribers.Count);
            Assert.AreEqual(1, sessions.Count);

            SmsSubscriber subscriber = subscribers.First();
            SmsSession session = sessions.First();

            Assert.IsNotNull(subscriber.Sessions);
            Assert.AreEqual(1, subscriber.Sessions.Count);
            Assert.IsNull(subscriber.Sessions[0].Keyword);
            Assert.AreEqual(SmsSubscriberState.Normal, subscriber.State);
            Assert.AreEqual(SUBSCRIBER_NUM, subscriber.SubscriberNumber);
            Assert.AreEqual(LONG_CODE, subscriber.ServiceNumber);

            Assert.IsNull(session.Keyword);
            Assert.AreEqual(SmsSessionOrigin.Client, session.Origin);
            Assert.AreEqual(SUBSCRIBER_NUM, session.SubscriberNumber);
            Assert.AreEqual(LONG_CODE, session.ServiceNumber);
            Assert.AreEqual(SmsSessionState.Active, session.State);
        }

        [TestMethod]
        public void SendMessage_NoSessionNoFromAndTo_ThrowsException()
        {
            FakeLayer.Setup();

            SmsMessageLogic logic = new SmsMessageLogic();

            IncomingMessage message = new IncomingMessage { Body = "Test" };

            ExceptionAssertionUtility.AssertExceptionThrown("From or To must be specified.", typeof(ServiceException), () => logic.SendSmsMessage(message, null));
        }

        [TestMethod]
        public void SendMessage_EmptyKeywordNoCatchAll_ThrowsException()
        {
            FakeLayer.Setup();

            SmsMessageLogic logic = new SmsMessageLogic();

            IncomingMessage message = new IncomingMessage { From = SHORT_CODE, To = SUBSCRIBER_NUM, Body = "Test" };

            Guid licenseId = Guid.NewGuid();

            AccessToken token = new AccessToken
                                    {
                                        LicenseId = licenseId
                                    };

            ExceptionAssertionUtility.AssertExceptionThrown("You must have a catch-all in order to not supply a keyword", typeof(ServiceException), () => logic.SendSmsMessage(message, token));
        }

        //[TestMethod]
        //public void SendMessage_EmptyKeywordCatchAllGoodSession_SendsOutgoingMessage()
        //{
        //    FakeLayer fakeLayer = FakeLayer.Setup();

        //    SmsMessageLogic logic = new SmsMessageLogic();

        //    Guid sessionId = Guid.Empty;

        //    SmsSession session1 = new SmsSession { Id = Guid.NewGuid(), State = SmsSessionState.Active, SubscriberNumber = SUBSCRIBER_NUM };
        //    SmsSession session2 = new SmsSession { Id = Guid.NewGuid(), State = SmsSessionState.Active, SubscriberNumber = SUBSCRIBER_NUM };
        //    SmsSession session3 = new SmsSession { Id = Guid.NewGuid(), State = SmsSessionState.Active, SubscriberNumber = SUBSCRIBER_NUM };

        //    fakeLayer.DataLayer.AddItem(session1);
        //    fakeLayer.DataLayer.AddItem(session2);
        //    fakeLayer.DataLayer.AddItem(session3);

        //    SmsNumberData catchAll = new SmsNumberData
        //                                   {
        //                                       Id = Guid.NewGuid(),
        //                                       Number = SHORT_CODE,
        //                                       Website = "http://tempuri.com"
        //                                   };

        //    fakeLayer.DataLayer.AddItem(catchAll);

        //    SmsSubscriber subscriber = new SmsSubscriber
        //    {
        //        Id = Guid.NewGuid(),
        //        SubscriberNumber = SUBSCRIBER_NUM,
        //        ServiceNumber = SHORT_CODE,
        //        Sessions = new List<SmsSubscriberSessionItem>
        //                       {
        //                           new SmsSubscriberSessionItem
        //                            {
        //                                SessionId = session1.Id
        //                            },
        //                            new SmsSubscriberSessionItem
        //                            {
        //                                SessionId = session2.Id
        //                            },
        //                            new SmsSubscriberSessionItem
        //                            {
        //                                SessionId = session3.Id
        //                            }
        //                       }
        //    };

        //    fakeLayer.DataLayer.AddItem(subscriber);

        //    fakeLayer.DataLayer.Setup(new SmsNumberData { Id = Guid.NewGuid(), RouteType = SmsNumberRouteType.CatchAll, Number = SHORT_CODE });

        //    SmsIncomingMessage message = new SmsIncomingMessage { SessionId = sessionId, From = SHORT_CODE, To = SUBSCRIBER_NUM, Body = "Test" };

        //    logic.SendSmsMessage(message);

        //    Assert.AreEqual(subscriber.Sessions.Count, 4);
        //    Assert.AreEqual(subscriber.Sessions[1].SessionId, session1.Id);
        //    Assert.AreEqual(subscriber.Sessions[2].SessionId, session2.Id);
        //    Assert.AreEqual(subscriber.Sessions[3].SessionId, session3.Id);

        //    Assert.IsTrue(session1.State == SmsSessionState.Active);
        //    Assert.IsTrue(session2.State == SmsSessionState.Active);
        //    Assert.IsTrue(session3.State == SmsSessionState.Active);
        //}

        //[TestMethod]
        //public void SendMessage_KeywordSuppliedNoKeywordData_ThrowsException()
        //{
        //    FakeLayer.Setup();

        //    SmsMessageLogic logic = new SmsMessageLogic();

        //    Guid sessionId = Guid.Empty;

        //    SmsIncomingMessage message = new SmsIncomingMessage { SessionId = sessionId, From = SHORT_CODE, To = SUBSCRIBER_NUM, Keyword = TEST_KEYWORD, Body = "Test" };

        //    ExceptionAssertionUtility.AssertExceptionThrown("From and Keyword must match a keyword configuration for the module.", typeof(ServiceException), () => logic.SendSmsMessage(message));
        //}

        //[TestMethod]
        //public void SendMessage_KeywordSuppliedNoSession_StartsNewSession()
        //{
        //    FakeLayer fakeLayer = FakeLayer.Setup();

        //    SmsMessageLogic logic = new SmsMessageLogic();

        //    Guid sessionId = Guid.Empty;

        //    SmsIncomingMessage message = new SmsIncomingMessage { SessionId = sessionId, From = SHORT_CODE, To = SUBSCRIBER_NUM, Keyword = TEST_KEYWORD, Body = "Test" };
        //    fakeLayer.DataLayer.Setup(new SmsKeywordData { Number = SHORT_CODE, Keyword = TEST_KEYWORD });

        //    logic.SendSmsMessage(message);
        //}

        [TestMethod]
        public void ReceiveMessage_StopNoSubscriptions_SendsMessage()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            fakeLayer.DataLayer.Setup(new SmsNumberData { Id = Guid.NewGuid(), Number = SHORT_CODE, LicenseId = Guid.NewGuid(), Keywords = new List<SmsNumberKeywordItem> { new SmsNumberKeywordItem { Keyword = TEST_KEYWORD } } });

            SmsMessageLogic logic = new SmsMessageLogic();

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, SHORT_CODE, "Stop");
        }

        [TestMethod]
        public void ReceiveMessage_StopMultipleSubscriptions_SendsMessage()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            fakeLayer.DataLayer.Setup(new SmsNumberData { Id = Guid.NewGuid(), Number = SHORT_CODE, LicenseId = Guid.NewGuid(), Keywords = new List<SmsNumberKeywordItem> { new SmsNumberKeywordItem { Keyword = TEST_KEYWORD } } });

            fakeLayer.DataLayer.AddItem(new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Active });
            fakeLayer.DataLayer.AddItem(new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Inactive });
            fakeLayer.DataLayer.AddItem(new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Inactive });

            SmsMessageLogic logic = new SmsMessageLogic();

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, SHORT_CODE, "Stop");
        }

        [TestMethod]
        public void ReceiveMessage_StopSingleSubscription_SendsMessage()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            SmsSession session = new SmsSession
                {
                    Id = Guid.NewGuid(),
                    SubscriberNumber = SUBSCRIBER_NUM,
                    ServiceNumber = SHORT_CODE,
                    State = SmsSessionState.Active,
                    Keyword = TEST_KEYWORD
                };

            fakeLayer.DataLayer.AddItem(session);

            SmsSubscriber subscriber = new SmsSubscriber
            {
                Id = Guid.NewGuid(),
                SubscriberNumber = SUBSCRIBER_NUM,
                ServiceNumber = SHORT_CODE,
                Sessions = new List<SmsSubscriberSessionItem>
							   {
								   new SmsSubscriberSessionItem
									{
										SessionId = session.Id
									}
							   }
            };

            fakeLayer.DataLayer.AddItem(subscriber);

            fakeLayer.DataLayer.Setup(new SmsNumberData { Id = Guid.NewGuid(), Number = SHORT_CODE, LicenseId = Guid.NewGuid(), Keywords = new List<SmsNumberKeywordItem> { new SmsNumberKeywordItem { Keyword = TEST_KEYWORD } } });

            fakeLayer.DataLayer.AddItem(new SmsKeywordData { Keyword = TEST_KEYWORD, Number = SHORT_CODE });

            SmsMessageLogic logic = new SmsMessageLogic();

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, SHORT_CODE, "Stop");

            Assert.IsTrue(session.State == SmsSessionState.Stopped);
        }

        [TestMethod]
        public void ReceiveMessage_StopAllMultipleSubscriptions_SendsMessage()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            fakeLayer.DataLayer.Setup(new SmsNumberData { Id = Guid.NewGuid(), Number = SHORT_CODE, LicenseId = Guid.NewGuid(), Keywords = new List<SmsNumberKeywordItem> { new SmsNumberKeywordItem { Keyword = TEST_KEYWORD } } });

            SmsSession session1 = new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Active, Keyword = TEST_KEYWORD };
            SmsSession session2 = new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Active, Keyword = TEST_KEYWORD2 };
            SmsSession session3 = new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Active, Keyword = TEST_KEYWORD3 };

            fakeLayer.DataLayer.AddItem(session1);
            fakeLayer.DataLayer.AddItem(session2);
            fakeLayer.DataLayer.AddItem(session3);

            SmsSubscriber subscriber = new SmsSubscriber
            {
                Id = Guid.NewGuid(),
                SubscriberNumber = SUBSCRIBER_NUM,
                ServiceNumber = SHORT_CODE,
                Sessions = new List<SmsSubscriberSessionItem>
							   {
								   new SmsSubscriberSessionItem
									{
										SessionId = session1.Id
									},
									new SmsSubscriberSessionItem
									{
										SessionId = session2.Id
									},
									new SmsSubscriberSessionItem
									{
										SessionId = session3.Id
									}
							   }
            };

            fakeLayer.DataLayer.AddItem(subscriber);

            fakeLayer.DataLayer.AddItem(new SmsKeywordData { Keyword = TEST_KEYWORD, Number = SHORT_CODE });
            fakeLayer.DataLayer.AddItem(new SmsKeywordData { Keyword = TEST_KEYWORD2, Number = SHORT_CODE });
            fakeLayer.DataLayer.AddItem(new SmsKeywordData { Keyword = TEST_KEYWORD3, Number = SHORT_CODE });

            SmsMessageLogic logic = new SmsMessageLogic();

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, SHORT_CODE, "stop all");

            Assert.IsTrue(session1.State == SmsSessionState.Stopped);
            Assert.IsTrue(session2.State == SmsSessionState.Stopped);
            Assert.IsTrue(session3.State == SmsSessionState.Stopped);
        }

        [TestMethod]
        public void ReceiveMessage_StopWithMultipleSubscriptions_SendsMessage()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            fakeLayer.DataLayer.Setup(new SmsNumberData { Id = Guid.NewGuid(), Number = SHORT_CODE, LicenseId = Guid.NewGuid(), Keywords = new List<SmsNumberKeywordItem> { new SmsNumberKeywordItem { Keyword = TEST_KEYWORD } } });

            SmsSession session1 = new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Active, Keyword = TEST_KEYWORD };
            SmsSession session2 = new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Active, Keyword = TEST_KEYWORD2 };
            SmsSession session3 = new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Active, Keyword = TEST_KEYWORD3 };

            fakeLayer.DataLayer.AddItem(session1);
            fakeLayer.DataLayer.AddItem(session2);
            fakeLayer.DataLayer.AddItem(session3);

            SmsSubscriber subscriber = new SmsSubscriber
            {
                Id = Guid.NewGuid(),
                SubscriberNumber = SUBSCRIBER_NUM,
                ServiceNumber = SHORT_CODE,
                Sessions = new List<SmsSubscriberSessionItem>
							   {
								   new SmsSubscriberSessionItem
									{
										SessionId = session1.Id,
										Keyword = "Keyword1"
									},
									new SmsSubscriberSessionItem
									{
										SessionId = session2.Id,
										Keyword = "Keyword2"
									},
									new SmsSubscriberSessionItem
									{
										SessionId = session3.Id,
										Keyword = "Keyword3"
									}
							   }
            };

            fakeLayer.DataLayer.AddItem(subscriber);

            fakeLayer.DataLayer.AddItem(new SmsKeywordData { Keyword = TEST_KEYWORD, Number = SHORT_CODE });
            fakeLayer.DataLayer.AddItem(new SmsKeywordData { Keyword = TEST_KEYWORD2, Number = SHORT_CODE });
            fakeLayer.DataLayer.AddItem(new SmsKeywordData { Keyword = TEST_KEYWORD3, Number = SHORT_CODE });

            SmsMessageLogic logic = new SmsMessageLogic();

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, SHORT_CODE, "stop");

            fakeLayer.MockSmsService.Verify(service => service.SendMessage(SUBSCRIBER_NUM, SHORT_CODE, "KSW: Which program to stop?\nKeyword1\nKeyword2\nKeyword3\nSTOP ALL"));
            Assert.AreEqual(subscriber.State, SmsSubscriberState.AwaitingStopResponse);
        }

        [TestMethod]
        public void ReceiveMessage_KeywordAfterStopWithMultipleSubscriptions_SendsMessage()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            SmsSession session1 = new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Active, Keyword = TEST_KEYWORD };
            SmsSession session2 = new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Active, Keyword = TEST_KEYWORD2 };
            SmsSession session3 = new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Active, Keyword = TEST_KEYWORD3 };

            fakeLayer.DataLayer.AddItem(session1);
            fakeLayer.DataLayer.AddItem(session2);
            fakeLayer.DataLayer.AddItem(session3);

            SmsSubscriber subscriber = new SmsSubscriber
            {
                Id = Guid.NewGuid(),
                SubscriberNumber = SUBSCRIBER_NUM,
                ServiceNumber = SHORT_CODE,
                State = SmsSubscriberState.AwaitingStopResponse,
                Sessions = new List<SmsSubscriberSessionItem>
							   {
								   new SmsSubscriberSessionItem
									{
										SessionId = session1.Id,
										Keyword = TEST_KEYWORD
									},
									new SmsSubscriberSessionItem
									{
										SessionId = session2.Id,
										Keyword = TEST_KEYWORD2
									},
									new SmsSubscriberSessionItem
									{
										SessionId = session3.Id,
										Keyword = TEST_KEYWORD3
									}
							   }
            };

            fakeLayer.DataLayer.AddItem(subscriber);

            fakeLayer.DataLayer.AddItem(new SmsKeywordData { Keyword = TEST_KEYWORD, Number = SHORT_CODE, Website = WEBSITE });
            fakeLayer.DataLayer.AddItem(new SmsKeywordData { Keyword = TEST_KEYWORD2, Number = SHORT_CODE, Website = WEBSITE });
            fakeLayer.DataLayer.AddItem(new SmsKeywordData { Keyword = TEST_KEYWORD3, Number = SHORT_CODE, Website = WEBSITE });

            SmsMessageLogic logic = new SmsMessageLogic();

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, SHORT_CODE, TEST_KEYWORD);

            fakeLayer.MockSmsService.Verify(service => service.SendMessage(SUBSCRIBER_NUM, SHORT_CODE, string.Format("You will receive no more messages from KSW: {0} program.\n\nTxt HELP for help.", TEST_KEYWORD)), Times.Once());
            fakeLayer.MockSmsMessageForwarder.Verify(forwarder => forwarder.SendMessage(It.IsAny<MessageFormat>(), WEBSITE, It.IsAny<OutgoingMessage>()), Times.Once());

            Assert.AreEqual(subscriber.State, SmsSubscriberState.Normal);
        }

        [TestMethod]
        public void ReceiveMessage_InvalidKeywordAfterStopWithMultipleSubscriptions_ResetsSubscriber()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            SmsSession session1 = new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Active, Keyword = TEST_KEYWORD };
            SmsSession session2 = new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Active, Keyword = TEST_KEYWORD2 };
            SmsSession session3 = new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Active, Keyword = TEST_KEYWORD3 };

            fakeLayer.DataLayer.AddItem(session1);
            fakeLayer.DataLayer.AddItem(session2);
            fakeLayer.DataLayer.AddItem(session3);

            SmsSubscriber subscriber = new SmsSubscriber
            {
                Id = Guid.NewGuid(),
                SubscriberNumber = SUBSCRIBER_NUM,
                ServiceNumber = SHORT_CODE,
                State = SmsSubscriberState.AwaitingStopResponse,
                Sessions = new List<SmsSubscriberSessionItem>
							   {
								   new SmsSubscriberSessionItem
									{
										SessionId = session1.Id,
										Keyword = TEST_KEYWORD
									},
									new SmsSubscriberSessionItem
									{
										SessionId = session2.Id,
										Keyword = TEST_KEYWORD2
									},
									new SmsSubscriberSessionItem
									{
										SessionId = session3.Id,
										Keyword = TEST_KEYWORD3
									}
							   }
            };

            fakeLayer.DataLayer.AddItem(subscriber);

            fakeLayer.DataLayer.AddItem(new SmsKeywordData { Keyword = TEST_KEYWORD, Number = SHORT_CODE, Website = WEBSITE });
            fakeLayer.DataLayer.AddItem(new SmsKeywordData { Keyword = TEST_KEYWORD2, Number = SHORT_CODE, Website = WEBSITE });
            fakeLayer.DataLayer.AddItem(new SmsKeywordData { Keyword = TEST_KEYWORD3, Number = SHORT_CODE, Website = WEBSITE });

            SmsMessageLogic logic = new SmsMessageLogic();

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, SHORT_CODE, "INALID_KEYWORD");

            // test that no messages are sent
            fakeLayer.MockSmsService.Verify(service => service.SendMessage(SUBSCRIBER_NUM, SHORT_CODE, It.IsAny<string>()), Times.Never());

            Assert.AreEqual(subscriber.State, SmsSubscriberState.Normal);
        }

        [TestMethod]
        public void ReceiveMessage_HelpWithNoSessions_SendsMessage()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            Guid licenseId = Guid.NewGuid();

            fakeLayer.DataLayer.Setup(new SmsNumberData { Id = Guid.NewGuid(), Number = SHORT_CODE, LicenseId = licenseId, Keywords = new List<SmsNumberKeywordItem> { new SmsNumberKeywordItem { Keyword = TEST_KEYWORD } } });
			fakeLayer.DataLayer.Setup(new SmsSession() { DateAdded = DateTime.UtcNow, Id = Guid.NewGuid(), Keyword = TEST_KEYWORD, LicenseId = licenseId, ServiceNumber = SHORT_CODE, SubscriberNumber = SUBSCRIBER_NUM, State = SmsSessionState.Active, LastRequestDate = DateTime.UtcNow });

            SmsMessageLogic logic = new SmsMessageLogic();

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, SHORT_CODE, "help");
        }

        [TestMethod]
        public void ReceiveMessage_HelpWithSessions_SendsMessage()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            Guid licenseId = Guid.NewGuid();

            fakeLayer.DataLayer.Setup(new SmsNumberData { Id = Guid.NewGuid(), Number = SHORT_CODE, LicenseId = licenseId, Keywords = new List<SmsNumberKeywordItem> { new SmsNumberKeywordItem { Keyword = TEST_KEYWORD } } });
			fakeLayer.DataLayer.Setup(new SmsSession() { DateAdded = DateTime.UtcNow, Id = Guid.NewGuid(), Keyword = TEST_KEYWORD, LicenseId = licenseId, ServiceNumber = SHORT_CODE, SubscriberNumber = SUBSCRIBER_NUM, State = SmsSessionState.Active, LastRequestDate = DateTime.UtcNow });

            SmsSession session1 = new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Active, Keyword = TEST_KEYWORD };

            fakeLayer.DataLayer.AddItem(session1);

            SmsMessageLogic logic = new SmsMessageLogic();

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, SHORT_CODE, "help");
        }

        [TestMethod]
        public void ReceiveMessage_HasValidKeyword_NoSession_StartsNewSession()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            fakeLayer.DataLayer.AddItem(new SmsKeywordData { Keyword = TEST_KEYWORD, Number = SHORT_CODE, Website = WEBSITE });

            SmsMessageLogic logic = new SmsMessageLogic();

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, SHORT_CODE, TEST_KEYWORD);

            FakeDataSet<SmsSession> sessions = fakeLayer.DataLayer.GetFakeSet<SmsSession>();

            Assert.IsTrue(sessions.Count > 0);
        }

        [TestMethod]
        public void ReceiveMessage_HasInvalidKeyword_ReceivesIncomingMessage()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            fakeLayer.DataLayer.Setup(new SmsKeywordData { Keyword = TEST_KEYWORD, Number = SHORT_CODE });
            fakeLayer.DataLayer.Setup(new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Active, Keyword = TEST_KEYWORD });

            SmsMessageLogic logic = new SmsMessageLogic();

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, SHORT_CODE, TEST_KEYWORD);

            FakeDataSet<SmsSession> sessions = fakeLayer.DataLayer.GetFakeSet<SmsSession>();

            Assert.IsTrue(sessions.Count > 0);
        }

        [TestMethod]
        public void ReceiveMessage_HasValidKeyword_ExistingStoppedSession_CreatesNewSession()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            fakeLayer.DataLayer.Setup(new SmsKeywordData { Keyword = TEST_KEYWORD, Number = SHORT_CODE, Website = WEBSITE });
            fakeLayer.DataLayer.Setup(new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Stopped, Keyword = TEST_KEYWORD });

            SmsMessageLogic logic = new SmsMessageLogic();

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, SHORT_CODE, TEST_KEYWORD);

            FakeDataSet<SmsSession> sessions = fakeLayer.DataLayer.GetFakeSet<SmsSession>();

            Assert.AreEqual(2, sessions.Count);

            Assert.IsTrue(sessions[0].State == SmsSessionState.Stopped);
            Assert.IsTrue(sessions[1].State == SmsSessionState.Active);
        }

        [TestMethod]
        public void ReceiveMessage_HasValidKeyword_ExistingActiveSession_CreatesNewSession()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            fakeLayer.DataLayer.Setup(new SmsKeywordData { Keyword = TEST_KEYWORD, Number = SHORT_CODE, Website = WEBSITE });
            fakeLayer.DataLayer.Setup(new SmsSession { Id = Guid.NewGuid(), SubscriberNumber = SUBSCRIBER_NUM, ServiceNumber = SHORT_CODE, State = SmsSessionState.Active, Keyword = TEST_KEYWORD });

            SmsMessageLogic logic = new SmsMessageLogic();

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, SHORT_CODE, TEST_KEYWORD);

            FakeDataSet<SmsSession> sessions = fakeLayer.DataLayer.GetFakeSet<SmsSession>();

            Assert.AreEqual(2, sessions.Count);

            Assert.IsTrue(sessions[0].State == SmsSessionState.Inactive);
            Assert.IsTrue(sessions[1].State == SmsSessionState.Active);
        }

        [TestMethod]
        public void ReceiveMessage_HelpMessage_SendsHelpMessage()
        {
            FakeLayer fake = FakeLayer.Setup();

            SmsMessageLogic logic = new SmsMessageLogic();

            Guid licenseId = Guid.NewGuid();

            fake.DataLayer.Setup(new SmsNumberData { Id = Guid.NewGuid(), Number = SHORT_CODE, LicenseId = licenseId, Keywords = new List<SmsNumberKeywordItem> { new SmsNumberKeywordItem { Keyword = TEST_KEYWORD } } });
			fake.DataLayer.Setup(new SmsSession() { DateAdded = DateTime.UtcNow, Id = Guid.NewGuid(), Keyword = TEST_KEYWORD, LicenseId = licenseId, ServiceNumber = SHORT_CODE, SubscriberNumber = SUBSCRIBER_NUM, State = SmsSessionState.Active, LastRequestDate = DateTime.UtcNow });

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, SHORT_CODE, "Help");

            fake.MockSmsService.Verify(service => service.SendMessage(SUBSCRIBER_NUM, SHORT_CODE, "Krames StayWell (KSW): Providing relevant, timely, health edu.\nMsg&Data Rates May Apply. 1 msg/day.\nContact: admin.kramesstaywell.com/help. Reply STOP to cancel"));
        }

        [TestMethod]
        public void ReceiveMessage_CatchAll_ForwardsMessageCreatesSubscriberAndSession()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            SmsMessageLogic logic = new SmsMessageLogic();

            Guid numberId = Guid.NewGuid();

            Guid licenseId = Guid.NewGuid();

            fakeLayer.DataLayer.Setup(new SmsNumberData
            {
                Format = MessageFormat.Json,
                Id = numberId,
                Keywords = null,
                LicenseId = licenseId,
                Number = LONG_CODE,
                RouteType = SmsNumberRouteType.CatchAll,
                Type = SmsNumberType.LongCode,
                Website = WEBSITE
            });

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, LONG_CODE, TEST_KEYWORD);

            fakeLayer.MockSmsMessageForwarder.Verify(forwarder => forwarder.SendMessage(It.IsAny<MessageFormat>(), WEBSITE, It.IsAny<OutgoingMessage>()), Times.Once());

            FakeDataSet<SmsSubscriber> subscribers = fakeLayer.DataLayer.GetFakeSet<SmsSubscriber>();
            FakeDataSet<SmsSession> sessions = fakeLayer.DataLayer.GetFakeSet<SmsSession>();

            Assert.AreEqual(1, subscribers.Count);
            Assert.AreEqual(1, sessions.Count);

            SmsSubscriber subscriber = subscribers.First();
            SmsSession session = sessions.First();

            Assert.IsNotNull(subscriber.Sessions);
            Assert.AreEqual(1, subscriber.Sessions.Count);
            Assert.IsNull(subscriber.Sessions[0].Keyword);
            Assert.AreEqual(SmsSubscriberState.Normal, subscriber.State);
            Assert.AreEqual(SUBSCRIBER_NUM, subscriber.SubscriberNumber);
            Assert.AreEqual(LONG_CODE, subscriber.ServiceNumber);

            Assert.IsNull(session.Keyword);
            Assert.AreEqual(SmsSessionOrigin.Subscriber, session.Origin);
            Assert.AreEqual(SUBSCRIBER_NUM, session.SubscriberNumber);
            Assert.AreEqual(LONG_CODE, session.ServiceNumber);
            Assert.AreEqual(SmsSessionState.Active, session.State);
        }

        [TestMethod]
        public void ReceiveMessage_StopCatchAll_ForwardsMessage()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            SmsMessageLogic logic = new SmsMessageLogic();

            Guid numberId = Guid.NewGuid();

            Guid licenseId = Guid.NewGuid();

            Guid sessionId = Guid.NewGuid();

            Guid subscriberId = Guid.NewGuid();

            fakeLayer.DataLayer.Setup(new SmsNumberData
            {
                Format = MessageFormat.Json,
                Id = numberId,
                Keywords = null,
                LicenseId = licenseId,
                Number = LONG_CODE,
                RouteType = SmsNumberRouteType.CatchAll,
                Type = SmsNumberType.LongCode,
                Website = WEBSITE
            });

            fakeLayer.DataLayer.Setup(new SmsSession
            {
                Id = sessionId,
                Keyword = null,
                State = SmsSessionState.Active,
                ServiceNumber = LONG_CODE,
                SubscriberNumber = SUBSCRIBER_NUM,
                Origin = SmsSessionOrigin.Subscriber
            });

            fakeLayer.DataLayer.Setup(new SmsSubscriber
            {
                Id = subscriberId,
                Sessions = new List<SmsSubscriberSessionItem> { new SmsSubscriberSessionItem { Keyword = null, SessionId = sessionId } },
                State = SmsSubscriberState.Normal,
                ServiceNumber = LONG_CODE,
                SubscriberNumber = SUBSCRIBER_NUM
            });

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, LONG_CODE, "stop");

            fakeLayer.MockSmsMessageForwarder.Verify(forwarder => forwarder.SendMessage(It.IsAny<MessageFormat>(), WEBSITE, It.IsAny<OutgoingMessage>()), Times.Once());

            FakeDataSet<SmsSubscriber> subscribers = fakeLayer.DataLayer.GetFakeSet<SmsSubscriber>();
            FakeDataSet<SmsSession> sessions = fakeLayer.DataLayer.GetFakeSet<SmsSession>();

            Assert.AreEqual(0, subscribers.Count);
            Assert.AreEqual(1, sessions.Count);

            SmsSession session = sessions.First();

            Assert.IsNull(session.Keyword);
            Assert.AreEqual(SmsSessionOrigin.Subscriber, session.Origin);
            Assert.AreEqual(SUBSCRIBER_NUM, session.SubscriberNumber);
            Assert.AreEqual(LONG_CODE, session.ServiceNumber);
            Assert.AreEqual(SmsSessionState.Stopped, session.State);
        }

        [TestMethod]
        public void ReceiveMessage_StopAllCatchAll_ForwardsMessage()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            SmsMessageLogic logic = new SmsMessageLogic();

            Guid numberId = Guid.NewGuid();

            Guid licenseId = Guid.NewGuid();

            Guid sessionId = Guid.NewGuid();

            Guid subscriberId = Guid.NewGuid();

            fakeLayer.DataLayer.Setup(new SmsNumberData
            {
                Format = MessageFormat.Json,
                Id = numberId,
                LicenseId = licenseId,
                Number = LONG_CODE,
                RouteType = SmsNumberRouteType.CatchAll,
                Type = SmsNumberType.LongCode,
                Website = WEBSITE,
                Keywords = new List<SmsNumberKeywordItem> { new SmsNumberKeywordItem { Keyword = TEST_KEYWORD } }
            });

            fakeLayer.DataLayer.Setup(new SmsSession
            {
                Id = sessionId,
                Keyword = null,
                State = SmsSessionState.Active,
                ServiceNumber = LONG_CODE,
                SubscriberNumber = SUBSCRIBER_NUM,
                Origin = SmsSessionOrigin.Subscriber
            });

            fakeLayer.DataLayer.Setup(new SmsSubscriber
            {
                Id = subscriberId,
                Sessions = new List<SmsSubscriberSessionItem> { new SmsSubscriberSessionItem { Keyword = null, SessionId = sessionId } },
                State = SmsSubscriberState.Normal,
                ServiceNumber = LONG_CODE,
                SubscriberNumber = SUBSCRIBER_NUM
            });

            logic.ProcessSmsMessage(string.Empty, string.Empty, SUBSCRIBER_NUM, LONG_CODE, "stop all");

            fakeLayer.MockSmsMessageForwarder.Verify(forwarder => forwarder.SendMessage(It.IsAny<MessageFormat>(), WEBSITE, It.IsAny<OutgoingMessage>()), Times.Once());

            FakeDataSet<SmsSubscriber> subscribers = fakeLayer.DataLayer.GetFakeSet<SmsSubscriber>();
            FakeDataSet<SmsSession> sessions = fakeLayer.DataLayer.GetFakeSet<SmsSession>();

            Assert.AreEqual(0, subscribers.Count);
            Assert.AreEqual(1, sessions.Count);

            SmsSession session = sessions.First();

            Assert.IsNull(session.Keyword);
            Assert.AreEqual(SmsSessionOrigin.Subscriber, session.Origin);
            Assert.AreEqual(SUBSCRIBER_NUM, session.SubscriberNumber);
            Assert.AreEqual(LONG_CODE, session.ServiceNumber);
            Assert.AreEqual(SmsSessionState.Stopped, session.State);
        }
    }
}
