﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Cryptography;
using System.Xml;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.Rendering;
using KswApi.Interface.Objects.SmsGateway;
using KswApi.RestrictedInterface.Objects;
using KswApi.Test.Framework;
using KswApi.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable InconsistentNaming

namespace KswApi.Test
{
	[TestClass]
	public class CustomXmlSerializerTests
	{
		private const string SERIALIZED_LONG_CODE = "<LongCode><RouteType>Keyword</RouteType><Format>Json</Format><Number>1234</Number><Keywords><Keyword><Value>kw</Value><Website>http://website.com</Website><Format>Json</Format></Keyword></Keywords></LongCode>";

		[TestMethod]
		public void Serialize_LongCode_SerializesCorrectly()
		{
			LongCode number = new LongCode
								   {
									   Number = "1234",
									   RouteType = SmsNumberRouteType.Keyword,
									   Keywords = new List<SmsKeyword>
						                              {
							                              new SmsKeyword
								                              {
									                              Value = "kw",
									                              Website = "http://website.com"
								                              }
						                              }
								   };

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(LongCode));

			MemoryStream ms = new MemoryStream();

			XmlWriter writer = GetWriter(ms);

			serializer.Serialize(writer, number);

			writer.Flush();

			string xmlString = GetXml(ms);

			Assert.AreEqual(SERIALIZED_LONG_CODE, xmlString);
		}

		[TestMethod]
		public void Deserialize_SmsLongCode_DeserializesCorrectly()
		{
			XmlTextReader reader = new XmlTextReader(new StringReader(SERIALIZED_LONG_CODE));

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(LongCode));

			LongCode deserialized = (LongCode)serializer.Deserialize(reader);

			Assert.IsNotNull(deserialized);
			Assert.AreEqual(deserialized.Number, "1234");
			Assert.IsNotNull(deserialized.Keywords);
			Assert.AreEqual(deserialized.Keywords.Count, 1);
			Assert.AreEqual(deserialized.Keywords[0].Value, "kw");
			Assert.AreEqual(deserialized.Keywords[0].Website, "http://website.com");
			Assert.AreEqual(deserialized.Keywords[0].Format, MessageFormat.Json);
		}

		[TestMethod]
		public void Deserialize_FormattedSmsNumber_DeserializesCorrectly()
		{
			string xml = @"
				<LongCode>
					<Number>1234</Number>
					<Keywords>
						<Keyword>
							<Value>kw</Value>
							<Website>http://website.com</Website>
							<Format>Json</Format>
						</Keyword>
					</Keywords>
					<RouteType>Keyword</RouteType>
					<Format>Json</Format>
				</LongCode>
				";

			XmlTextReader reader = new XmlTextReader(new StringReader(xml));

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(LongCode));

			LongCode deserialized = (LongCode)serializer.Deserialize(reader);

			Assert.IsNotNull(deserialized);
			Assert.AreEqual(deserialized.Number, "1234");
			Assert.IsNotNull(deserialized.Keywords);
			Assert.AreEqual(deserialized.Keywords.Count, 1);
			Assert.AreEqual(deserialized.Keywords[0].Value, "kw");
			Assert.AreEqual(deserialized.Keywords[0].Website, "http://website.com");
			Assert.AreEqual(deserialized.Keywords[0].Format, MessageFormat.Json);
		}

		[TestMethod]
		public void Deserialize_License_DeserializesCorrectly()
		{
			string licenseText =
			@"
			<License>
				<Name>JMA License xml</Name>
				<ParentLicenseId></ParentLicenseId>
				<ClientId>23a10250-bb8f-4dbf-a8bd-a13c04e2fb7a</ClientId>
				<Notes>JMA</Notes>
			</License>
			";
			XmlTextReader reader = new XmlTextReader(new StringReader(licenseText));

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(License));

			License license = (License)serializer.Deserialize(reader);

			Assert.IsNotNull(license);
			Assert.AreEqual(license.Name, "JMA License xml");
			Assert.IsNull(license.ParentLicenseId);
			Assert.AreEqual(license.ClientId, new Guid("23a10250-bb8f-4dbf-a8bd-a13c04e2fb7a"));
			Assert.AreEqual(license.Notes, "JMA");
		}

		[TestMethod]
		public void Deserialize_LicenseWithParent_DeserializesCorrectly()
		{
			string licenseText =
			@"
			<License>
				<Name>JMA License xml</Name>
				<ParentLicenseId>12345678-1234-1234-1234-123456789012</ParentLicenseId>
				<ClientId>23a10250-bb8f-4dbf-a8bd-a13c04e2fb7a</ClientId>
				<Notes>JMA</Notes>
			</License>
			";
			XmlTextReader reader = new XmlTextReader(new StringReader(licenseText));

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(License));

			License license = (License)serializer.Deserialize(reader);

			Assert.IsNotNull(license);
			Assert.AreEqual(license.Name, "JMA License xml");
			Assert.IsNotNull(license.ParentLicenseId);
			Assert.AreEqual(license.ParentLicenseId.Value, new Guid("12345678-1234-1234-1234-123456789012"));
			Assert.AreEqual(license.ClientId, new Guid("23a10250-bb8f-4dbf-a8bd-a13c04e2fb7a"));
			Assert.AreEqual(license.Notes, "JMA");
		}

		[TestMethod]
		public void Serialize_License_SerializesCorrectly()
		{
			Guid client = Guid.NewGuid();

			License license = new License
			{
				Name = "LicenseName",
				ClientId = client
			};

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(License));

			MemoryStream ms = new MemoryStream();

			XmlWriter writer = GetWriter(ms);

			serializer.Serialize(writer, license);

			writer.Flush();

			string xmlString = GetXml(ms);

			Assert.AreEqual(xmlString, string.Format("<License><Id>00000000-0000-0000-0000-000000000000</Id><CreatedDate>0001-01-01T00:00:00Z</CreatedDate><Status>Active</Status><Name>LicenseName</Name><ClientId>{0}</ClientId></License>", client));
		}

		[TestMethod]
		public void Serialize_LicenseWithParent_SerializesCorrectly()
		{
			Guid parent = Guid.NewGuid();
			Guid client = Guid.NewGuid();

			License license = new License
								  {
									  Name = "LicenseName",
									  ParentLicenseId = parent,
									  ClientId = client,
									  Status = LicenseStatus.Inactive
								  };

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(License));

			MemoryStream ms = new MemoryStream();

			XmlWriter writer = GetWriter(ms);

			serializer.Serialize(writer, license);

			writer.Flush();

			string xmlString = GetXml(ms);

			Assert.AreEqual(xmlString, string.Format("<License><Id>00000000-0000-0000-0000-000000000000</Id><CreatedDate>0001-01-01T00:00:00Z</CreatedDate><Status>Inactive</Status><Name>LicenseName</Name><ClientId>{1}</ClientId><ParentLicenseId>{0}</ParentLicenseId></License>", parent, client));
		}

		[TestMethod]
		public void Serialize_Application_SerializesCorrectly()
		{
			Application application = new Application
										  {
											  Addresses = new List<string> { "123.123.123", "::1" },
											  Type = ApplicationType.Public,
											  EnableJsonp = true,
											  JsonpWebsites = new List<string> { "http://google.com", "http://localhost"}
										  };

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(Application));

			MemoryStream ms = new MemoryStream();

			XmlWriter writer = GetWriter(ms);

			serializer.Serialize(writer, application);

			writer.Flush();

			string xmlString = GetXml(ms);

			Assert.AreEqual(xmlString, "<Application><Id>00000000-0000-0000-0000-000000000000</Id><DateAdded>0001-01-01T00:00:00Z</DateAdded><Type>Public</Type><EnableJsonp>true</EnableJsonp><JsonpWebsites><JsonpWebsite>http://google.com</JsonpWebsite><JsonpWebsite>http://localhost</JsonpWebsite></JsonpWebsites><Addresses><Address>123.123.123</Address><Address>::1</Address></Addresses></Application>");
		}

		[TestMethod]
		public void Deserialize_Application_DeserializesCorrectly()
		{
			string xml =
			@"<Application>
				<Name>My Name</Name>
				<Addresses>
					<Address>123.123.123</Address>
					<Address>::1</Address>
				</Addresses>
				<DateAdded>3-4-1980 3:20</DateAdded>
			</Application>";

			XmlTextReader reader = new XmlTextReader(new StringReader(xml));

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(Application));

			Application application = (Application)serializer.Deserialize(reader);

			Assert.IsNotNull(application);
			Assert.AreEqual(application.Name, "My Name");
			Assert.AreEqual(application.Id, Guid.Empty);
			Assert.IsNotNull(application.Addresses);
			Assert.AreEqual(application.Addresses.Count, 2);
			Assert.AreEqual(application.Addresses[0], "123.123.123");
			Assert.AreEqual(application.Addresses[1], "::1");
			Assert.AreEqual(application.DateAdded, new DateTime(1980, 3, 4, 3, 20 , 0).ToUniversalTime());
		}

		[TestMethod]
		public void Deserialize_ApplicationValidationRequest_DeserializesCorrectly()
		{
			string xml =
			@"<ApplicationValidation>
				<ResponseType>code</ResponseType>
				<ApplicationId>29eeff76-85d0-4f70-94ed-a49072524692</ApplicationId>
				<RedirectUri>http://10.10.20.158/KswApi.Portal/Dashboard</RedirectUri>
				<Scope></Scope>
				<State></State>
			</ApplicationValidation>";

			XmlTextReader reader = new XmlTextReader(new StringReader(xml));

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(ApplicationValidationRequest));

			ApplicationValidationRequest request = (ApplicationValidationRequest)serializer.Deserialize(reader);

			Assert.IsNotNull(request);
			Assert.AreEqual(request.ResponseType, "code");
			Assert.AreEqual(request.ApplicationId, "29eeff76-85d0-4f70-94ed-a49072524692");
			Assert.AreEqual(request.RedirectUri, "http://10.10.20.158/KswApi.Portal/Dashboard");
		}

		[TestMethod]
		public void Serialize_Application_SerializesAndDeserializesCorrectly()
		{
			string xml =
			@"<Application>
				<Name>JMA Test Application xml</Name>
				<Type>Confidential</Type>
				<RedirectUri>http://10.10.20.158/KswApi.Portal/Dashboard</RedirectUri>
				<Site>www.ea.ucr.staywell.com</Site>
				<Addresses>
					<Address>10.10.7.68</Address>
				</Addresses>
				<EnableJsonp>true</EnableJsonp>
				<JsonpWebsites>
					<JsonpWebsite>http://google.com</JsonpWebsite>
				</JsonpWebsites>
				<Description>JMA Test Desc</Description>
			</Application>";

			XmlTextReader reader = new XmlTextReader(new StringReader(xml));

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(Application));

			Application application = (Application)serializer.Deserialize(reader);

			MemoryStream ms = new MemoryStream();

			XmlWriter writer = GetWriter(ms);

			serializer.Serialize(writer, application);

			writer.Flush();

			string returnedXml = GetXml(ms);

			Assert.AreEqual(returnedXml, "<Application><Id>00000000-0000-0000-0000-000000000000</Id><DateAdded>0001-01-01T00:00:00Z</DateAdded><Name>JMA Test Application xml</Name><RedirectUri>http://10.10.20.158/KswApi.Portal/Dashboard</RedirectUri><Site>www.ea.ucr.staywell.com</Site><Description>JMA Test Desc</Description><Type>Confidential</Type><EnableJsonp>true</EnableJsonp><JsonpWebsites><JsonpWebsite>http://google.com</JsonpWebsite></JsonpWebsites><Addresses><Address>10.10.7.68</Address></Addresses></Application>");
		}

		[TestMethod]
		public void Serialize_ShortCode_SerializesAndDeserializesCorrectly()
		{
			string xml =
			@"<ShortCode>
				<Number>98517</Number>
				<Keywords>
					<Keyword>
						<Value>JMAxml</Value>
						<Website>www.jma.com</Website>
						<Format>Json</Format>
					</Keyword>
					<Keyword>
						<Value>JMA2xml</Value>
						<Website>http://www.jma2.com</Website>
						<Format>Xml</Format>
					</Keyword>
				</Keywords>
			</ShortCode>";

			XmlTextReader reader = new XmlTextReader(new StringReader(xml));

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(ShortCode));

			ShortCode number = (ShortCode)serializer.Deserialize(reader);

			MemoryStream ms = new MemoryStream();

			XmlWriter writer = GetWriter(ms);

			serializer.Serialize(writer, number);

			writer.Flush();

			string returnedXml = GetXml(ms);

			Assert.AreEqual(returnedXml, "<ShortCode><Number>98517</Number><Keywords><Keyword><Value>JMAxml</Value><Website>www.jma.com</Website><Format>Json</Format></Keyword><Keyword><Value>JMA2xml</Value><Website>http://www.jma2.com</Website><Format>Xml</Format></Keyword></Keywords></ShortCode>");
		}

		[TestMethod]
		public void Serialize_LongCode_SerializesAndDeserializesCorrectly()
		{
			string xml =
			@"<LongCode>
				<Number>5005550006</Number>
				<Keywords>
					<Keyword>
						<Value>JMA</Value>
						<Website>www.jma.com</Website>
						<Format>Json</Format>
					</Keyword>
					<Keyword>
						<Value>JMA2</Value>
						<Website>http://www.jma2.com</Website>
						<Format>Xml</Format>
					</Keyword>
				</Keywords>
				<RouteType>Keyword</RouteType>
				<Format>Xml</Format>
			</LongCode>";

			XmlTextReader reader = new XmlTextReader(new StringReader(xml));

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(LongCode));

			LongCode number = (LongCode)serializer.Deserialize(reader);

			MemoryStream ms = new MemoryStream();

			XmlWriter writer = GetWriter(ms);

			serializer.Serialize(writer, number);

			writer.Flush();

			string returnedXml = GetXml(ms);

			Assert.AreEqual("<LongCode><RouteType>Keyword</RouteType><Format>Xml</Format><Number>5005550006</Number><Keywords><Keyword><Value>JMA</Value><Website>www.jma.com</Website><Format>Json</Format></Keyword><Keyword><Value>JMA2</Value><Website>http://www.jma2.com</Website><Format>Xml</Format></Keyword></Keywords></LongCode>", returnedXml);
		}

		[TestMethod]
		public void Serialize_Enum_SerializedAndDeserializesCorrectly()
		{
			SmsMessageAction action = SmsMessageAction.Stop;

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(SmsMessageAction));

			MemoryStream ms = new MemoryStream();

			XmlWriter writer = GetWriter(ms);

			serializer.Serialize(writer, action);

			writer.Flush();

			string returnedXml = GetXml(ms);

			XmlTextReader reader = new XmlTextReader(new StringReader(returnedXml));

			SmsMessageAction returnedAction = (SmsMessageAction) serializer.Deserialize(reader);

			Assert.AreEqual("<SmsMessageAction>Stop</SmsMessageAction>", returnedXml);
			Assert.AreEqual(action, returnedAction);
		}

		[TestMethod]
		public void Deserialize_InvalidRootElement_ThrowsException()
		{
			string xml = @"<NotValid></NotValid>";

			XmlTextReader reader = new XmlTextReader(new StringReader(xml));

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(LongCode));

			SerializationException exception = Expect.Exception<SerializationException>(() => serializer.Deserialize(reader));
		}

		[TestMethod]
		public void Deserialize_InvalidEnumValue_ExceptionIncludesPropertyName()
		{
			const string xml = @"<LongCode><Format>json</Format></LongCode>";

			XmlTextReader reader = new XmlTextReader(new StringReader(xml));

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(LongCode));

			SerializationException exception = Expect.Exception<SerializationException>(() => serializer.Deserialize(reader));
			Assert.AreEqual("Invalid value for parameter: Format.", exception.Message);
		}

		[TestMethod]
		public void Deserialize_NullableEnumFieldValueProvided_Success()
		{
			const string xml = @"<Render><RenderMode>Email</RenderMode></Render>";

			XmlTextReader reader = new XmlTextReader(new StringReader(xml));

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(RenderRequest));

			RenderRequest deserialized = serializer.Deserialize(reader) as RenderRequest;

			Assert.IsNotNull(deserialized);
			Assert.AreEqual(RenderMode.Email, deserialized.RenderMode);
		}

		[TestMethod]
		public void Deserialize_NullableEnumFieldNoValue_ParsedValueIsNull()
		{
			const string xml = @"<Render></Render>";

			XmlTextReader reader = new XmlTextReader(new StringReader(xml));

			CustomXmlSerializer serializer = new CustomXmlSerializer(typeof(RenderRequest));

			RenderRequest deserialized = serializer.Deserialize(reader) as RenderRequest;

			Assert.IsNotNull(deserialized);
			Assert.IsNull(deserialized.RenderMode);
		}

		#region Private Helper Methods

		private XmlWriter GetWriter(MemoryStream ms)
		{
			XmlWriterSettings xmlWriterSettings = new XmlWriterSettings { OmitXmlDeclaration = true };

			return XmlWriter.Create(ms, xmlWriterSettings);
		}

		private string GetXml(MemoryStream ms)
		{
			ms.Position = 0;

			StreamReader reader = new StreamReader(ms);

			return reader.ReadToEnd();
		}

		#endregion

	}
}
