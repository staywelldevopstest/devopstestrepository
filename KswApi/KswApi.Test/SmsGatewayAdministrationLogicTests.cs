﻿using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.SmsGateway;
using KswApi.Logic.Framework;
using KswApi.Logic.SmsLogic;
using KswApi.Poco.Sms;
using KswApi.Repositories.Objects;
using KswApi.Test.Framework.Fakes;
using KswApi.Test.Framework.TestUtilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using Moq;

// ReSharper disable InconsistentNaming

namespace KswApi.Test
{
    [TestClass]
    public class SmsGatewayAdministrationLogicTests
    {
        private const string SHORT_CODE = "13579";
	    private const string LONG_CODE = "5152345872";

        [TestMethod]
        public void EnsureShortCodes_Successful()
        {
            SmsAdministrationLogic logic = new SmsAdministrationLogic();

            FakeLayer.Setup();

            logic.EnsureShortCodes();
        }

        [TestMethod]
        public void GetModule_NoNumbers_Successful()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();
            ModuleProvider.InitializeModules();

            Guid licenseId = Guid.NewGuid();

            License license = new License();
            license.Id = licenseId;
            license.Modules = new List<ModuleType> { ModuleType.SmsGateway };

            fakeLayer.DataLayer.Setup(license);

            SmsAdministrationLogic logic = new SmsAdministrationLogic();

            SmsGatewayModule module = logic.GetModule(licenseId.ToString());

            Assert.IsTrue(module != null);
        }

        [TestMethod]
        public void GetModule_WithNumbers_Successful()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();
            ModuleProvider.InitializeModules();

            License license = GetLicense(fakeLayer, true);

            SmsNumberData number = GetShortcodeSmsNumberData(license.Id, fakeLayer);
            SmsKeyword keyword = GetSmsKeyword(fakeLayer);

            SmsAdministrationLogic logic = new SmsAdministrationLogic();

            SmsGatewayModule module = logic.GetModule(license.Id.ToString());

            Assert.IsTrue(module != null);
        }

        [TestMethod]
        public void GetShortCode_Successful()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();
            ModuleProvider.InitializeModules();

            License license = GetLicense(fakeLayer, true);
            SmsNumberData numberData = GetShortcodeSmsNumberData(license.Id, fakeLayer);
            SmsKeyword keyword = GetSmsKeyword(fakeLayer);

            SmsAdministrationLogic logic = new SmsAdministrationLogic();

            ShortCode number = logic.GetShortCode(license.Id.ToString(), numberData.Number);

            Assert.IsTrue(number != null);
        }

        [TestMethod]
        public void CreateShortCode_Successful()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();
            ModuleProvider.InitializeModules();

            License license = GetLicense(fakeLayer, true);

            SmsAdministrationLogic logic = new SmsAdministrationLogic();

            GetSmsAvailableNumber(fakeLayer);

            ShortCode numberToCreate = GetShortcodeSmsNumber(true);

            ShortCode number = logic.CreateShortCode(license.Id.ToString(), numberToCreate);

            Assert.IsTrue(number != null);
        }

        [TestMethod]
        public void UpdateShortCode_NoShortCode_ThrowsException()
        {
            SmsAdministrationLogic logic = new SmsAdministrationLogic();

            FakeLayer fakeLayer = FakeLayer.Setup();
            ModuleProvider.InitializeModules();

            License license = GetLicense(fakeLayer, true);

            ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => logic.UpdateShortCode(license.Id.ToString(), null, null));
        }

        [TestMethod]
        public void UpdateShortCode_Successful()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();
            ModuleProvider.InitializeModules();

            License license = GetLicense(fakeLayer, true);
            ShortCode number = GetShortcodeSmsNumber(true);
            SmsNumberData numberData = GetShortcodeSmsNumberData(license.Id, fakeLayer);
            GetSmsAvailableNumber(fakeLayer);
			GetSmsKeywordData(license.Id, numberData, fakeLayer);

            SmsAdministrationLogic logic = new SmsAdministrationLogic();

            ShortCode updatedNumber = logic.UpdateShortCode(license.Id.ToString(), number.Number, number);

            Assert.IsTrue(updatedNumber != null);
        }

        [TestMethod]
        public void DeleteShortCode_Successful()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();
            ModuleProvider.InitializeModules();

            License license = GetLicense(fakeLayer, true);
			ShortCode number = GetShortcodeSmsNumber(true);

			SmsNumberData numberData = GetShortcodeSmsNumberData(license.Id, fakeLayer);
            GetSmsAvailableNumber(fakeLayer);
            GetSmsKeywordData(license.Id, numberData, fakeLayer);

            SmsAdministrationLogic logic = new SmsAdministrationLogic();

            logic.DeleteShortCode(license.Id.ToString(), number.Number);
        }

		[TestMethod]
		public void PurchaseLongCode_Successful()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();
			ModuleProvider.InitializeModules();

			License license = GetLicense(fakeLayer, true);

			SmsAdministrationLogic logic = new SmsAdministrationLogic();

			GetSmsAvailableNumber(fakeLayer);

			LongCode numberToCreate = new LongCode
				                           {
											   Number = LONG_CODE,
											   RouteType = SmsNumberRouteType.CatchAll
				                           };


			LongCode number = logic.PurchaseLongCode(license.Id.ToString(), numberToCreate);

			Assert.IsTrue(number != null);

			fakeLayer.MockSmsService.Verify(service => service.PurchaseLongCode(LONG_CODE), Times.Once());
		}

		[TestMethod]
		public void UpdateLongCode_NoLongCode_ThrowsException()
		{
			SmsAdministrationLogic logic = new SmsAdministrationLogic();

			FakeLayer fakeLayer = FakeLayer.Setup();
			ModuleProvider.InitializeModules();

			License license = GetLicense(fakeLayer, true);

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => logic.UpdateLongCode(license.Id.ToString(), null, null));
		}

		[TestMethod]
		public void UpdateLongCode_Successful()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();
			ModuleProvider.InitializeModules();

			License license = GetLicense(fakeLayer, true);
			LongCode number = GetLongcodeSmsNumber(true);
			SmsNumberData numberData = GetLongcodeSmsNumberData(license.Id, fakeLayer);
			GetSmsAvailableNumber(fakeLayer);
			GetSmsKeywordData(license.Id, numberData, fakeLayer);

			SmsAdministrationLogic logic = new SmsAdministrationLogic();

			LongCode updatedNumber = logic.UpdateLongCode(license.Id.ToString(), number.Number, number);

			Assert.IsTrue(updatedNumber != null);
		}

		[TestMethod]
		public void DeleteLongCode_Successful()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();
			ModuleProvider.InitializeModules();

			License license = GetLicense(fakeLayer, true);
			LongCode number = GetLongcodeSmsNumber(true);

			SmsNumberData numberData = GetLongcodeSmsNumberData(license.Id, fakeLayer);
			GetSmsAvailableNumber(fakeLayer);
			GetSmsKeywordData(license.Id, numberData, fakeLayer);

			SmsAdministrationLogic logic = new SmsAdministrationLogic();

			logic.DeleteLongCode(license.Id.ToString(), number.Number);

			fakeLayer.MockSmsService.Verify(service => service.ReleaseLongCode(LONG_CODE), Times.Once());
		}

        [TestMethod]
        public void DeleteModuleForLicense_DeletesNumbersAndKeywords()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();
            ModuleProvider.InitializeModules();

            License license = GetLicense(fakeLayer, true);
			ShortCode number = GetShortcodeSmsNumber(true);

			SmsNumberData numberData = GetShortcodeSmsNumberData(license.Id, fakeLayer);
            GetSmsAvailableNumber(fakeLayer);
            GetSmsKeywordData(license.Id, numberData, fakeLayer);

			SmsAdministrationLogic logic = new SmsAdministrationLogic();

            logic.DeleteModuleForLicense(license);

	        Assert.AreEqual(0, fakeLayer.DataLayer.GetFakeSet<SmsNumberData>().Count);
			Assert.AreEqual(0, fakeLayer.DataLayer.GetFakeSet<SmsKeywordData>().Count);
        }

        #region Private Methods

        private License GetLicense(FakeLayer fakeLayer, bool addSmsGatewayModule)
        {
            License license = new License { Id = Guid.NewGuid() };

            if (addSmsGatewayModule)
                license.Modules = new List<ModuleType> { ModuleType.SmsGateway };

            fakeLayer.DataLayer.Setup(license);

            return license;
        }

        private SmsNumberData GetShortcodeSmsNumberData(Guid licenseId, FakeLayer fakeLayer)
        {
            SmsNumberData number = new SmsNumberData { Id = Guid.NewGuid(), LicenseId = licenseId, Number = SHORT_CODE, Type = SmsNumberType.ShortCode, Keywords = new List<SmsNumberKeywordItem>() };

            fakeLayer.DataLayer.Setup(number);

            return number;
        }

		private SmsNumberData GetLongcodeSmsNumberData(Guid licenseId, FakeLayer fakeLayer)
		{
			SmsNumberData number = new SmsNumberData { Id = Guid.NewGuid(), LicenseId = licenseId, Number = LONG_CODE, Type = SmsNumberType.LongCode, Keywords = new List<SmsNumberKeywordItem>() };

			fakeLayer.DataLayer.Setup(number);

			return number;
		}

        private SmsKeyword GetSmsKeyword(FakeLayer fakeLayer)
        {
            SmsKeyword keyword = new SmsKeyword { Value = "TestWord1", Website = "http://testwebsite.com/serviceurl" };

            fakeLayer.DataLayer.Setup(keyword);

            return keyword;
        }

        private ShortCode GetShortcodeSmsNumber(bool withKeywords)
        {
			ShortCode number = new ShortCode();
			
            number.Number = SHORT_CODE;

			if (withKeywords)
            {
                number.Keywords = new List<SmsKeyword>();

                number.Keywords.Add(new SmsKeyword { Format = MessageFormat.Json, Value = "TestWord1", Website = "http://testWebsite.com/serviceurl" });
                number.Keywords.Add(new SmsKeyword { Format = MessageFormat.Json, Value = "TestWord2", Website = "http://testWebsite.com/serviceurl" });
                number.Keywords.Add(new SmsKeyword { Format = MessageFormat.Json, Value = "TestWord3", Website = "http://testWebsite.com/serviceurl" });
                number.Keywords.Add(new SmsKeyword { Format = MessageFormat.Json, Value = "TestWord4", Website = "http://testWebsite.com/serviceurl" });
            }

            return number;
        }

		private LongCode GetLongcodeSmsNumber(bool withKeywords)
		{
			LongCode number = new LongCode();

			number.Number = LONG_CODE;
			//number.Type = SmsNumberType.ShortCode;
			number.RouteType = SmsNumberRouteType.Keyword;

			if (withKeywords)
			{
				number.Keywords = new List<SmsKeyword>();

				number.Keywords.Add(new SmsKeyword { Format = MessageFormat.Json, Value = "TestWord1", Website = "http://testWebsite.com/serviceurl" });
				number.Keywords.Add(new SmsKeyword { Format = MessageFormat.Json, Value = "TestWord2", Website = "http://testWebsite.com/serviceurl" });
				number.Keywords.Add(new SmsKeyword { Format = MessageFormat.Json, Value = "TestWord3", Website = "http://testWebsite.com/serviceurl" });
				number.Keywords.Add(new SmsKeyword { Format = MessageFormat.Json, Value = "TestWord4", Website = "http://testWebsite.com/serviceurl" });
			}

			return number;
		}

        private SmsSharedNumber GetSmsAvailableNumber(FakeLayer fakeLayer)
        {
            SmsSharedNumber number = new SmsSharedNumber();

            number.Id = Guid.NewGuid();
            number.Number = SHORT_CODE;
            number.Type = SmsNumberType.ShortCode;

            fakeLayer.DataLayer.Setup(number);

            return number;
        }

        private SmsKeywordData GetSmsKeywordData(Guid licenseId, SmsNumberData number, FakeLayer fakeLayer)
        {
            SmsKeywordData data = new SmsKeywordData();

            data.Id = Guid.NewGuid();
            data.Keyword = "TestKeyword1";
            data.LicenseId = licenseId;
            data.Number = number.Number;
	        data.NumberId = number.Id;

            fakeLayer.DataLayer.Setup(data);

            return data;
        }

        #endregion
    }
}
