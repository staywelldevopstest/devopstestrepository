﻿//using System.Collections.Generic;
//using System.Linq;
//using KswApi.Caching;
//using KswApi.Common.Interfaces;
//using KswApi.Logic.Framework.Extensions;
//using KswApi.Test.Framework.Data;
//using KswApi.Test.Framework.Fakes;
//using KswApi.Test.Framework.Objects;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
//using KswApi.Ioc;
//using KswApi.Logic.Framework;

//// ReSharper disable InconsistentNaming

//namespace KswApi.Test
//{
//    [TestClass]
//    public class CacheTests
//    {
//        [TestMethod]
//        public void Set_CreatesCorrectKeyAndAssignsValue()
//        {
//            const string testKey = "TheKey";
//            const string testString = "This is the test value.";

//            FakeCache cache = new FakeCache();

//            Dependency.Set<ICache>(cache);

//            TestValue value = new TestValue { Key = testKey, Value = testString };

//            Cache.Set(value, item => item.Key);

//            Assert.AreEqual(cache.Count, 1);

//            KeyValuePair<string, object> cacheItem = cache.First();

//            Assert.AreEqual("TestValue/Key/TheKey", cacheItem.Key);
//            Assert.AreEqual(testString, ((TestValue)cacheItem.Value).Value);
//        }

//        [TestMethod]
//        public void Get_RetrievesCorrectValue()
//        {
//            const string testKey = "TheKey";
//            const string testString = "This is the test value.";

//            FakeCache cache = new FakeCache();

//            Dependency.Set<ICache>(cache);

//            TestValue value = new TestValue { Key = testKey, Value = testString };

//            Cache.Set(value, item => item.Key);

//            TestValue retrieveValue = Cache.Get<TestValue>(item => item.Key == testKey);

//            Assert.IsNotNull(retrieveValue);
//            Assert.AreEqual(testKey, retrieveValue.Key);
//            Assert.AreEqual(testString, retrieveValue.Value);
//        }

//        [TestMethod]
//        public void ExtensionGet_CachesValue()
//        {
//            const string testKey = "TheKey";
//            const string testString = "This is the test value.";

//            FakeLayer fakeLayer = FakeLayer.Setup();

//            FakeDbSet<TestValue> fakeSet = fakeLayer.DataProvider.GetDbSet<TestValue>();

//            TestValue value = new TestValue { Key = testKey, Value = testString };

//            fakeSet.Add(value);

//            TestValueContext context = new TestValueContext();
//            TestValue retrieveValue = context.TestValues.Get(item => item.Key == testKey);

//            Assert.IsNotNull(retrieveValue);
//            Assert.AreEqual(testKey, retrieveValue.Key);
//            Assert.AreEqual(testString, retrieveValue.Value);

//            Assert.IsTrue(fakeLayer.Cache.Contains("TestValue/Key/TheKey"));

//            TestValue cachedValue = Cache.Get<TestValue>(item => item.Key == testKey);
//            Assert.IsNotNull(cachedValue);
//            Assert.AreEqual(testKey, cachedValue.Key);
//            Assert.AreEqual(testString, cachedValue.Value);
//        }

//        [TestMethod]
//        public void ExtensionRemove_RemovesCacheAndDatabaseValue()
//        {
//            const string testKey = "TheKey";
//            const string testString = "This is the test value.";

//            FakeLayer fakeLayer = FakeLayer.Setup();

//            FakeDbSet<TestValue> fakeSet = fakeLayer.DataProvider.GetDbSet<TestValue>();

//            TestValue value = new TestValue { Key = testKey, Value = testString };

//            fakeSet.Add(value);

//            Cache.Set(value, item => item.Key);

//            TestValueContext context = new TestValueContext();

//            context.TestValues.Remove(value, item => item.Key);

//            Assert.AreEqual(fakeSet.Count, 0);
//            Assert.AreEqual(fakeLayer.Cache.Count, 0);
//        }
//    }
//}
