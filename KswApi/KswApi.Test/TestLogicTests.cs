﻿using System;
using KswApi.Common.Interfaces;
using KswApi.Interface.Objects;
using KswApi.Ioc;
using KswApi.Poco.Tasks;
using KswApi.Repositories.Objects;
using KswApi.Test.Framework.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KswApi.Logic;
using Moq;

// ReSharper disable InconsistentNaming

namespace KswApi.Test
{
	[TestClass]
	public class TestLogicTests
	{
		private const string ECHO_STRING = "Echo Test";

		[TestMethod]
		public void Ping_ReturnsValidDateTime()
		{
			TestLogic testLogic = new TestLogic();
			PingData pingData = testLogic.GetPingData();
			
			DateTime dateTime;

			Assert.IsNotNull(pingData);
			Assert.IsNotNull(pingData.CurrentDateAndTime);
			Assert.IsTrue(DateTime.TryParse(pingData.CurrentDateAndTime, out dateTime));
		}

		[TestMethod]
		public void Error_ThrowsError()
		{
			TestLogic testLogic = new TestLogic();

			try
			{
				testLogic.CreateError();
			}
			catch (Exception ex)
			{				
				Assert.IsTrue(ex.Message == "An error has occurred, intentionally.");
			}			
		}

		[TestMethod]
		public void ClearCache_ClearsTheCache()
		{
			Mock<ICache> mockCache = new Mock<ICache>();
			Dependency.Set(mockCache.Object);
			mockCache.Setup(item => item.Clear());
			TestLogic testLogic = new TestLogic();

			testLogic.ClearCache();

			mockCache.Verify(cache => cache.Clear(), Times.Once());
		}

		[TestMethod]
		public void GetCacheTestData_HasCachedData_DoesNotAccessDatabase()
		{
			TestData cachedData = new TestData { Id = Guid.Empty, Key = "CacheTest", Data = "Cached data" };

			FakeLayer fakeLayer = FakeLayer.Setup();
			
			fakeLayer.Cache.Set(cachedData, item => item.Key);
			fakeLayer.Cache.Commit();

			TestLogic testLogic = new TestLogic();

			CacheTestData data = testLogic.GetCacheTestData();

			Assert.AreEqual(cachedData.Data, data.Data);
			Assert.AreEqual(data.Source, "Cache");
		}

		[TestMethod]
		public void GetCacheTestData_NoCachedData_AccessesDatabase()
		{
			const string testString = "This is the test string";

			FakeLayer fakeLayer = FakeLayer.Setup();
			fakeLayer.DataLayer.Setup(new TestData { Data = testString, Id = Guid.Empty });

			TestLogic testLogic = new TestLogic();

			CacheTestData data = testLogic.GetCacheTestData();
			Assert.AreEqual(data.Data, testString);
			Assert.AreEqual(data.Source, "Database");
		}
	}
}
