﻿using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logic;
using KswApi.Logic.Framework.Constants;
using KswApi.Test.Framework.Fakes;
using KswApi.Test.Framework.TestUtilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Net;

// ReSharper disable InconsistentNaming

namespace KswApi.Test
{
    [TestClass]
    public class LogLogicTests
    {

        [TestMethod]
        public void GetLogs_WithFilter_Succeeds()
        {
            SetupLogs();

            LogLogic logic = new LogLogic();

            LogList logs = logic.GetLogs(0, 10, "Date-Descending", "Type eq notification", null);

            Assert.IsNotNull(logs);
            Assert.AreEqual(logs.Items.Count, 10);
            Assert.AreEqual(logs.Total, 25);
        }

        [TestMethod]
        public void GetLogs_WithFilterMaxPageSize_ThrowsException()
        {
            SetupLogs();

            LogLogic logic = new LogLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("Count is greater than the maximum page size.",
                                                            typeof(ServiceException),
                                                            () =>
                                                            logic.GetLogs(0, Constant.MAXIMUM_PAGE_SIZE + 1,
                                                                          "Date-Descending", "Type eq notification",
                                                                          null));
        }

        [TestMethod]
        public void GetLogs_WithNotificationFilter_Succeeds()
        {
            SetupLogs();

            LogLogic logic = new LogLogic();

            LogList logs = logic.GetLogs(0, 10, "Date-Ascending", "Type eq notification", null);

            Assert.IsNotNull(logs);
            Assert.AreEqual(logs.Items.Count, 10);
            Assert.AreEqual(logs.Total, 25);
        }

        [TestMethod]
        public void GetLog_Succeeds()
        {
            Guid guid = Guid.NewGuid();

            FakeLayer fakeLayer = FakeLayer.Setup();

            fakeLayer.DataLayer.Setup(new Log { Id = guid, Message = "Test Message" });

            LogLogic logic = new LogLogic();

            Log log = logic.GetLog(guid.ToString());

            Assert.IsNotNull(log);

            Assert.AreEqual(log.Message, "Test Message");
        }

        [TestMethod]
        public void GetLog_LogNotFound_Throws404()
        {
            Guid guid = Guid.NewGuid();

            FakeLayer fakeLayer = FakeLayer.Setup();

            LogLogic logic = new LogLogic();

            try
            {
                logic.GetLog(guid.ToString());
                Assert.Fail("Should throw an exception");
            }
            catch (ServiceException exception)
            {
                Assert.AreEqual(exception.StatusCode, HttpStatusCode.NotFound);
            }
        }

        #region Private Helper Methods

        private FakeLayer SetupLogs()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            FakeDataSet<Log> logs = fakeLayer.DataLayer.GetFakeSet<Log>();

            for (int i = 0; i < 50; i++)
            {
                logs.Add(new Log
                {
                    Id = Guid.NewGuid(),
                    Type = LogItemType.Operation,
                    SubType = LogItemSubType.Operation
                });
            }

            for (int i = 0; i < 25; i++)
            {
                logs.Add(new Log
                {
                    Id = Guid.NewGuid(),
                    Type = LogItemType.Notification,
                    SubType = LogItemSubType.Exception
                });
            }

            return fakeLayer;
        }

        #endregion Private Helper Methods
    }
}
