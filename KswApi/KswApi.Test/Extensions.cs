﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KswApi.Test
{
    public static class Extensions
    {
        public static bool CollectionsEqualByValues<T>(this IEnumerable<T> x, IEnumerable<T> y)
        {
            if (ReferenceEquals(x, y))
                return true;

            if (x == null ^ y == null)
                return false;

            List<T> xl = x.ToList();
            List<T> yl = y.ToList();

            if (xl.Count != yl.Count)
                return false;

            var result = !xl.Where((t, i) => !t.EqualByValues(yl[i])).Any();
            return result;
        }

        // TODO: guard against recursive loops
        public static bool EqualByValues<T>(this T x, T y)
        {
            // Non-value types that do not implement IEquatable:
            if (ReferenceEquals(x, y))
                return true;

            if (x == null ^ y == null)
                return false;

            // IEquatable
            if (typeof(IEquatable<T>).IsAssignableFrom(typeof(T))) // || type.IsPrimitive || type.IsValueType;
            {
                return x.Equals(y);
            }

            // IEnumerable
            if (typeof(IEnumerable).IsAssignableFrom(typeof(T)))
            {
                // dynamic to the rescue
                return CollectionsEqualByValues((dynamic)x, (dynamic)y);
            }

            PropertyInfo[] properties = typeof(T).GetProperties();

            // If T is object, there are no properties to compare => return false since ReferenceEquals from above is false
            if (properties.Length == 0)
                return false;

            foreach (PropertyInfo property in properties)
            {
                // dynamic to the rescue again
                dynamic px = property.GetValue(x);
                dynamic py = property.GetValue(y);
                bool result = EqualByValues(px, py);

                if (!result)
                    return false;
            }
            return true;
        }
    }

    #region Tests
    /*
    [TestClass]
    public class ExtensionsTests
    {
        [TestMethod]
        public void TestPropertiesEqual()
        {
            DateTime now = DateTime.Now;
            Dictionary<string, DateTime> dict1 = new Dictionary<string, DateTime> {{"hi", now}};
            Dictionary<string, DateTime> dict2 = new Dictionary<string, DateTime> {{"hi", now}};
            Dictionary<string, DateTime> dict3 = new Dictionary<string, DateTime> {{"hi", now.AddMonths(13)}};
            Dictionary<string, DateTime> dict4 = new Dictionary<string, DateTime> {{"hip", now}};

            Assert.IsTrue(dict1.EqualByValues(dict2));
            Assert.IsFalse(dict1.EqualByValues(dict3));
            Assert.IsFalse(dict1.EqualByValues(dict4));

            List<Dictionary<string, DateTime>> l1 = new List<Dictionary<string, DateTime>> {dict1};
            List<Dictionary<string, DateTime>> l2 = new List<Dictionary<string, DateTime>> {dict2};
            List<Dictionary<string, DateTime>> l3 = new List<Dictionary<string, DateTime>> {dict3};
            List<Dictionary<string, DateTime>> l4 = new List<Dictionary<string, DateTime>> {dict4};

            Assert.IsTrue(l1.EqualByValues(l2));
            Assert.IsFalse(l1.EqualByValues(l3));
            Assert.IsFalse(l1.EqualByValues(l4));

            List<string> ls1 = new List<string> {"a"};
            List<string> ls2 = new List<string> {"a"};
            Assert.IsTrue(ls1.EqualByValues(ls2));

            int i = 5;
            int j = 5;
            Assert.IsTrue(i.EqualByValues(j));
        }
    }
    */
    #endregion
    
}
