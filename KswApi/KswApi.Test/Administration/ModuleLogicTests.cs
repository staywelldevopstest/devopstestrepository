﻿using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logic.Administration;
using KswApi.Logic.Framework;
using KswApi.Logic.Framework.Objects;
using KswApi.Test.Framework.Fakes;
using KswApi.Test.Framework.TestUtilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

// ReSharper disable InconsistentNaming

namespace KswApi.Test.Administration
{
    [TestClass]
    public class ModuleLogicTests
    {
        private const string READ_RIGHT = "Read";
        private const string MANAGE_RIGHT = "Manage";
        private const string EXTRA_RIGHT = "Extra";

        private string DefaultScope { get { return READ_RIGHT + " " + MANAGE_RIGHT; } }

        private List<string> DefaultScopeList
        {
            get
            {
                List<string> list = new List<string>();

                list.Add(READ_RIGHT);
                list.Add(MANAGE_RIGHT);

                return list;
            }
        }

        public void GetModule_ValidModuleId_ReturnsModule()
        {
            ModuleLogic moduleLogic = new ModuleLogic();

            moduleLogic.InitializeModules();

            ServiceModule result = moduleLogic.GetServiceModule(ModuleType.SmsGateway.ToString());

            Assert.IsTrue(result.Name == "SMS Gateway");
            Assert.IsTrue(result.Type == ModuleType.SmsGateway);
        }

        [TestMethod]
        public void GetModule_InvalidGuid_ThrowsException()
        {
            ModuleLogic moduleLogic = new ModuleLogic();

            moduleLogic.InitializeModules();

            ExceptionAssertionUtility.AssertExceptionThrown("Invalid module ID.", typeof(ServiceException),
                                                            () => moduleLogic.GetServiceModule("basdfasdfasdfasdfasfd"));
        }

        [TestMethod]
        public void GetModule_InvalidModuleId_ReturnsNull()
        {
            ModuleLogic moduleLogic = new ModuleLogic();

            Guid smsModuleId = Guid.Parse("b1544450-a727-466f-8a49-0bd379a48c98");

            moduleLogic.InitializeModules();

            ExceptionAssertionUtility.AssertExceptionThrown("Invalid module ID.", typeof(ServiceException),
                                                            () => moduleLogic.GetServiceModule(smsModuleId.ToString()));
        }

        [TestMethod]
        public void GetModules_Sucessful()
        {
            ModuleLogic moduleLogic = new ModuleLogic();

            FakeLayer.Setup();
            ModuleProvider.InitializeModules();

            ModuleNameList list = moduleLogic.GetModules();

            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void GetLicenseModules_Successful()
        {
            ModuleLogic moduleLogic = new ModuleLogic();

            FakeLayer fakeLayer = FakeLayer.Setup();

            Guid licenseId = Guid.NewGuid();

            License license = new License();
            license.Id = licenseId;

            fakeLayer.DataLayer.Setup(license);
            ModuleProvider.InitializeModules();

            LicenseModuleList list = moduleLogic.GetLicenseModules(licenseId.ToString());

            Assert.IsTrue(list != null);
        }

        [TestMethod]
        public void AddLicenseModule_Succesful()
        {
            ModuleLogic moduleLogic = new ModuleLogic();
            ModuleProvider.InitializeModules();

            FakeLayer fakeLayer = FakeLayer.Setup();

            Guid licenseId = Guid.NewGuid();

            License license = new License();
            license.Id = licenseId;

            fakeLayer.DataLayer.Setup(license);

            ModuleName moduleName = moduleLogic.AddLicenseModule(licenseId.ToString(), ModuleType.SmsGateway.ToString());

            Assert.IsTrue(!string.IsNullOrWhiteSpace(moduleName.Name));
        }

        [TestMethod]
        public void RemoveLicenseModule_Succesful()
        {
            ModuleLogic moduleLogic = new ModuleLogic();
            ModuleProvider.InitializeModules();

            FakeLayer fakeLayer = FakeLayer.Setup();

            Guid licenseId = Guid.NewGuid();

            License license = new License();
            license.Id = licenseId;
            license.Modules = new List<ModuleType>();

            license.Modules.Add(ModuleType.SmsGateway);

            fakeLayer.DataLayer.Setup(license);

            ModuleName moduleName = moduleLogic.RemoveLicenseModule(licenseId.ToString(), ModuleType.SmsGateway.ToString());

            Assert.IsTrue(!string.IsNullOrWhiteSpace(moduleName.Name));
        }

    }
}
