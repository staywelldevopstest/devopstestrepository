﻿using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logic.Administration;
using KswApi.Logic.Framework.Constants;
using KswApi.Poco.Content;
using KswApi.Test.Framework.Fakes;
using KswApi.Test.Framework.TestUtilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace KswApi.Test.Administration
{
	[TestClass]
	public class CopyrightLogicTests
	{
		#region Private Member Variables

		private readonly string fakeCopyrightId = "205c1b1d-9a6d-449d-b596-525b82e84863";
		private DateTime fakeDateAdded = DateTime.Parse("2000-01-01").ToUniversalTime();
		private DateTime fakeDateModified = DateTime.Parse("2000-01-02").ToUniversalTime();

		#endregion

		[TestMethod]
		public void EnsureDefaultCopyright_Successful()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			CopyrightLogic copyrightLogic = new CopyrightLogic();
			copyrightLogic.EnsureDefaultCopyright();

			FakeDataSet<Copyright> copyrights = fakeLayer.DataLayer.GetFakeSet<Copyright>();
			Assert.AreEqual(Constant.Copyright.DEFAULT_COPYRIGHT_ID, copyrights.First().Id.ToString());
		}

		[TestMethod]
		public void CreateCopyright_Fails_With_Null_Request()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			CopyrightLogic copyrightLogic = new CopyrightLogic();
			
			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Copyrights.COPYRIGHT_VALUE_CANNOT_BE_NULL_OR_EMPTY,
				typeof(ServiceException),
				() => copyrightLogic.CreateCopyright(null));
		}

		[TestMethod]
		public void CreateCopyright_Fails_With_Null_Or_Empty_Value()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			CopyrightLogic copyrightLogic = new CopyrightLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Copyrights.COPYRIGHT_VALUE_CANNOT_BE_NULL_OR_EMPTY,
				typeof(ServiceException),
				() => copyrightLogic.CreateCopyright(new CopyrightRequest { Value = null }));

			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Copyrights.COPYRIGHT_VALUE_CANNOT_BE_NULL_OR_EMPTY,
				typeof(ServiceException),
				() => copyrightLogic.CreateCopyright(new CopyrightRequest { Value = string.Empty }));
		}

		[TestMethod]
		public void CreateCopyright_Successful()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			CopyrightLogic copyrightLogic = new CopyrightLogic();

			CopyrightRequest request = new CopyrightRequest {Value = "test"};

			CopyrightResponse response = copyrightLogic.CreateCopyright(request);

			Assert.IsNotNull(response.Id);
			Assert.AreEqual(request.Value, response.Value);
		}

		[TestMethod]
		public void UpdateCopyright_Fails_With_Null_Or_Empty_Id()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			CopyrightLogic copyrightLogic = new CopyrightLogic();

			//If were to use Moq, the second parameter could be any
			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Copyrights.COPYRIGHT_ID_MUST_BE_SPECIFIED,
				typeof(ServiceException),
				() => copyrightLogic.UpdateCopyright(null, null));

			//If were to use Moq, the second parameter could be any
			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Copyrights.COPYRIGHT_ID_MUST_BE_SPECIFIED,
				typeof(ServiceException),
				() => copyrightLogic.UpdateCopyright(string.Empty, null));
		}

		[TestMethod]
		public void UpdateCopyright_Fails_With_Invalid_Id()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			CopyrightLogic copyrightLogic = new CopyrightLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Copyrights.COPYRIGHT_NOT_FOUND,
				typeof(ServiceException),
				() => copyrightLogic.UpdateCopyright("x", null));
		}

		[TestMethod]
		public void UpdateCopyright_Fails_With_Valid_Id_And_Null_Request()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			string valueOriginal = "test original";

			Guid fakeCopyrightIdParsed = Guid.Parse(fakeCopyrightId);
			List<Copyright> copyrights = new List<Copyright>
				                             {
					                             new Copyright
						                             {
							                             Value = valueOriginal,
														 Id = fakeCopyrightIdParsed,
														 DateAdded = fakeDateAdded,
														 DateModified = fakeDateModified
						                             }
				                             };

			fakeLayer.DataLayer.Add<Copyright>(copyrights);

			CopyrightLogic copyrightLogic = new CopyrightLogic();
			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Copyrights.COPYRIGHT_VALUE_CANNOT_BE_NULL_OR_EMPTY,
				typeof(ServiceException),
				() => copyrightLogic.UpdateCopyright(fakeCopyrightId, null));
		}

		[TestMethod]
		public void UpdateCopyright_Fails_With_Valid_Id_And_Null_Or_Empty_Value()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			string valueOriginal = "test original";

			Guid fakeCopyrightIdParsed = Guid.Parse(fakeCopyrightId);
			List<Copyright> copyrights = new List<Copyright>
				                             {
					                             new Copyright
						                             {
							                             Value = valueOriginal,
														 Id = fakeCopyrightIdParsed,
														 DateAdded = fakeDateAdded,
														 DateModified = fakeDateModified
						                             }
				                             };

			fakeLayer.DataLayer.Add(copyrights);

			CopyrightLogic copyrightLogic = new CopyrightLogic();
			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Copyrights.COPYRIGHT_VALUE_CANNOT_BE_NULL_OR_EMPTY,
				typeof(ServiceException),
				() => copyrightLogic.UpdateCopyright(fakeCopyrightId, new CopyrightRequest { Value = null }));
			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Copyrights.COPYRIGHT_VALUE_CANNOT_BE_NULL_OR_EMPTY,
				typeof(ServiceException),
				() => copyrightLogic.UpdateCopyright(fakeCopyrightId, new CopyrightRequest { Value = string.Empty }));
		}

		[TestMethod]
		public void UpdateCopyright_Successful_On_Default_And_Valid_Value()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			string valueOriginal = "test original";
			string valueModified = "test modified";

			Guid defaultIdParsed = Guid.Parse(Constant.Copyright.DEFAULT_COPYRIGHT_ID);
			List<Copyright> copyrights = new List<Copyright>
				                             {
					                             new Copyright
						                             {
							                             Value = valueOriginal,
														 Id = defaultIdParsed,
														 DateAdded = fakeDateAdded,
														 DateModified = fakeDateModified
						                             }
				                             };

			fakeLayer.DataLayer.Add<Copyright>(copyrights);

			CopyrightRequest request = new CopyrightRequest
			{
				Value = valueModified
			};

			CopyrightLogic copyrightLogic = new CopyrightLogic();

			CopyrightResponse response = copyrightLogic.UpdateCopyright("default", request);
			Assert.IsNotNull(response);
			Assert.IsTrue(fakeDateModified.CompareTo(response.DateModified) < 0, "DateModified was not updated with current server time correctly.");
			Assert.AreEqual(valueModified, response.Value, "Value was not overriden with value from request.");
			//Assert.AreEqual(defaultIdParsed, response.Id, "Response Id does not match Id from request.");
		}

		[TestMethod]
		public void UpdateCopyright_Successful()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			string valueOriginal = "test original";
			string valueModified = "test modified";

			Guid fakeCopyrightIdParsed = Guid.Parse(fakeCopyrightId);
			List<Copyright> copyrights = new List<Copyright>
				                             {
					                             new Copyright
						                             {
							                             Value = valueOriginal,
														 Id = fakeCopyrightIdParsed,
														 DateAdded = fakeDateAdded,
														 DateModified = fakeDateModified
						                             }
				                             };

			fakeLayer.DataLayer.Add<Copyright>(copyrights);

			CopyrightRequest request = new CopyrightRequest
			{
				Value = valueModified
			};

			CopyrightLogic copyrightLogic = new CopyrightLogic();

			CopyrightResponse response = copyrightLogic.UpdateCopyright(fakeCopyrightId, request);
			Assert.IsNotNull(response);
			Assert.IsTrue(fakeDateModified.CompareTo(response.DateModified) < 0, "DateModified was not updated with current server time correctly.");
			Assert.AreEqual(valueModified, response.Value, "Value was not overriden with value from request.");
			Assert.AreEqual(fakeCopyrightIdParsed, response.Id, "Response Id does not match Id from request.");
		}

		[TestMethod]
		public void DeleteCopyright_Fails_With_Null_Id()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			CopyrightLogic copyrightLogic = new CopyrightLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Copyrights.COPYRIGHT_ID_MUST_BE_SPECIFIED,
				typeof(ServiceException),
				() => copyrightLogic.DeleteCopyright(null));
		}

		[TestMethod]
		public void DeleteCopyright_Fails_With_Default_Guid_Specified()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			CopyrightLogic copyrightLogic = new CopyrightLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Copyrights.COPYRIGHT_CANNOT_BE_DELETED,
				typeof(ServiceException),
				() => copyrightLogic.DeleteCopyright(Constant.Copyright.DEFAULT_COPYRIGHT_ID));
		}

		[TestMethod]
		public void DeleteCopyright_Fails_With_Default_Specified()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			CopyrightLogic copyrightLogic = new CopyrightLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Copyrights.COPYRIGHT_CANNOT_BE_DELETED,
				typeof(ServiceException),
				() => copyrightLogic.DeleteCopyright("default"));
		}

		[TestMethod]
		public void DeleteCopyright_Fails_With_Invalid_Id()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			CopyrightLogic copyrightLogic = new CopyrightLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(
				ErrorMessage.Copyrights.COPYRIGHT_NOT_FOUND,
				typeof(ServiceException),
				() => copyrightLogic.DeleteCopyright("x"));
		}

		[TestMethod]
		public void DeleteCopright_Successful_No_AssociatedCopyrights()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid fakeCopyrightIdParsed = Guid.Parse(fakeCopyrightId);
			Copyright copyright = new Copyright
				                      {
					                      Value = "test",
					                      Id = fakeCopyrightIdParsed,
					                      DateAdded = fakeDateAdded,
					                      DateModified = fakeDateModified

				                      };

			fakeLayer.DataLayer.AddItem<Copyright>(copyright);

			CopyrightLogic copyrightLogic = new CopyrightLogic();
			CopyrightResponse response = copyrightLogic.DeleteCopyright(fakeCopyrightId);

			FakeDataSet<Copyright> copyrights = fakeLayer.DataLayer.GetFakeSet<Copyright>();

			Assert.IsFalse(copyrights.Contains(copyright));
		}

		[TestMethod]
		public void DeleteCopyright_Sets_BucketCopyrights_To_Null()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid fakeCopyrightIdParsed = Guid.Parse(fakeCopyrightId);
			Copyright copyright = new Copyright
			{
				Value = "test",
				Id = fakeCopyrightIdParsed,
				DateAdded = fakeDateAdded,
				DateModified = fakeDateModified
			};

			ContentBucket bucket = new ContentBucket
				                       {
					                       CopyrightId = fakeCopyrightIdParsed
				                       };

			fakeLayer.DataLayer.AddItem<ContentBucket>(bucket);
			fakeLayer.DataLayer.AddItem<Copyright>(copyright);

			CopyrightLogic copyrightLogic = new CopyrightLogic();
			CopyrightResponse response = copyrightLogic.DeleteCopyright(fakeCopyrightId);

			FakeDataSet<Copyright> copyrights = fakeLayer.DataLayer.GetFakeSet<Copyright>();
			FakeDataSet<ContentBucket> buckets = fakeLayer.DataLayer.GetFakeSet<ContentBucket>();

			Assert.IsFalse(copyrights.Contains(copyright));
			Assert.IsFalse(buckets.Any(x => x.CopyrightId == fakeCopyrightIdParsed));
		}

		[TestMethod]
		public void DeleteCopyrights_Sets_ContentCopyrights_To_Null()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid fakeCopyrightIdParsed = Guid.Parse(fakeCopyrightId);
			Copyright copyright = new Copyright
			{
				Value = "test",
				Id = fakeCopyrightIdParsed,
				DateAdded = fakeDateAdded,
				DateModified = fakeDateModified
			};

			ContentCore master = new ContentCore
			                            {
				CopyrightId = fakeCopyrightIdParsed
			};

			fakeLayer.DataLayer.AddItem(master);
			fakeLayer.DataLayer.AddItem(copyright);

			CopyrightLogic copyrightLogic = new CopyrightLogic();
			CopyrightResponse response = copyrightLogic.DeleteCopyright(fakeCopyrightId);

			FakeDataSet<Copyright> copyrights = fakeLayer.DataLayer.GetFakeSet<Copyright>();
			FakeDataSet<ContentCore> masterSet = fakeLayer.DataLayer.GetFakeSet<ContentCore>();

			Assert.IsFalse(copyrights.Contains(copyright));
			Assert.IsFalse(masterSet.Any(x => x.CopyrightId == fakeCopyrightIdParsed));
		}

		#region Private Helper Methods

		private void Populate(FakeLayer layer)
		{
			
		}

		#endregion
	}
}
