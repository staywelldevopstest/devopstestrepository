﻿using System;
using System.Collections.Generic;
using System.Net;
using KswApi.Common.Objects;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Logic.Administration;
using KswApi.Poco.Enums;
using KswApi.Test.Framework;
using KswApi.Test.Framework.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KswApi.Repositories.Objects;
using KswApi.Test.Framework.TestUtilities;
using KswApi.Logic.Framework.Constants;
using KswApi.Interface.Exceptions;

// ReSharper disable InconsistentNaming

namespace KswApi.Test.Administration
{
	[TestClass]
	public class UserLogicTests
	{
		private const string ACCESS_TOKEN = "3q4897ylksdjhf98734ilu5hkjhndfgx897394yhkjsdhfsed8r7t93y5djfdhgjdf";
		private readonly Guid _clientId = Guid.NewGuid();

		[TestMethod]
		public void GetUserDetails_Successful()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			UserLogic logic = new UserLogic();

			AccessToken accessToken = GetDomainAccessToken(_clientId);

			fakeLayer.DataLayer.Setup(accessToken);

			UserResponse user = logic.GetUserDetails(true, accessToken);

			Assert.IsNotNull(user);
			Assert.IsFalse(string.IsNullOrWhiteSpace(user.FirstName));
			Assert.IsFalse(string.IsNullOrWhiteSpace(user.LastName));
			Assert.IsFalse(string.IsNullOrWhiteSpace(user.EmailAddress));
			Assert.IsFalse(string.IsNullOrWhiteSpace(user.FullName));
		}

		[TestMethod]
		public void GetUserDetails_NoAccessCode_ThrowsUnauthorized()
		{
			FakeLayer.Setup();
			UserLogic logic = new UserLogic();

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.GetUserDetails(true, null));

			Assert.AreEqual(HttpStatusCode.Unauthorized, exception.StatusCode);
		}

		[TestMethod]
		public void GetUserDetails_UserNotFound_ReturnsNull()
		{
			FakeLayer.Setup();

			UserLogic logic = new UserLogic();

			AccessToken accessToken = GetDomainAccessToken(Guid.Empty);

			accessToken.UserDetails = null;

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.GetUserDetails(true, accessToken));

			Assert.AreEqual(HttpStatusCode.Forbidden, exception.StatusCode);
		}

		[TestMethod]
		public void GetUserSettings_Success()
		{
			FakeLayer fake = FakeLayer.Setup();

			AccessToken accessToken = GetClientUserAccessToken(Guid.NewGuid());
			var clientUser = UserStubMatchingAccessToken(accessToken);
			fake.DataLayer.AddItem(clientUser);

			UserLogic logic = new UserLogic();

			UserSettingsResponse userSettings = logic.GetUserSettings(accessToken);
			Assert.IsNotNull(userSettings);
			Assert.AreEqual(clientUser.EmailAddress, userSettings.EmailAddress);
			Assert.AreEqual(clientUser.PhoneNumber, userSettings.PhoneNumber);
			Assert.AreEqual(clientUser.FirstName, userSettings.FirstName);
			Assert.AreEqual(clientUser.LastName, userSettings.LastName);
			Assert.AreEqual(clientUser.Notes, userSettings.Notes);
		}

		[TestMethod]
		public void GetUserSettings_Fails_With_Null_UserId()
		{
			FakeLayer fake = FakeLayer.Setup();

			AccessToken accessToken = GetClientUserAccessToken(null);

			UserLogic logic = new UserLogic();
			ExceptionAssertionUtility.AssertExceptionThrown(ErrorMessage.User.USER_NOT_FOUND, typeof(ServiceException), () => logic.GetUserSettings(accessToken));
		}

		[TestMethod]
		public void GetUserSettings_Fails_When_User_Cannot_Be_Found()
		{
			FakeLayer fake = FakeLayer.Setup();

			AccessToken accessToken = GetClientUserAccessToken(Guid.NewGuid());

			UserLogic logic = new UserLogic();
			ExceptionAssertionUtility.AssertExceptionThrown(ErrorMessage.User.USER_NOT_FOUND, typeof(ServiceException), () => logic.GetUserSettings(accessToken));
		}

		[TestMethod]
		public void UpdateSettings_Success()
		{
			FakeLayer fake = FakeLayer.Setup();

			AccessToken accessToken = GetClientUserAccessToken(Guid.NewGuid());
			var clientUser1 = UserStubMatchingAccessToken(accessToken);

			fake.DataLayer.AddItem(clientUser1);

			UserSettingsRequest request = new UserSettingsRequest
			{
				EmailAddress = "X" + clientUser1.EmailAddress,
				FirstName = "X" + clientUser1.FirstName,
				LastName = "X" + clientUser1.LastName,
				Notes = "X" + clientUser1.Notes,
				PhoneNumber = "X" + clientUser1.PhoneNumber
			};

			UserLogic logic = new UserLogic();

			UserSettingsResponse response = logic.UpdateUserSettings(accessToken, request);
			Assert.AreEqual(request.EmailAddress, response.EmailAddress);
			Assert.AreEqual(request.FirstName, response.FirstName);
			Assert.AreEqual(request.LastName, response.LastName);
			Assert.AreEqual(request.Notes, response.Notes);
			Assert.AreEqual(request.PhoneNumber, response.PhoneNumber);
		}

		[TestMethod]
		public void UpdateUserSettings_Fails_With_Null_UserSettingsRequest()
		{
			FakeLayer fake = FakeLayer.Setup();

			AccessToken accessToken = GetClientUserAccessToken(Guid.NewGuid());
			var clientUser = UserStubMatchingAccessToken(accessToken);
			fake.DataLayer.AddItem(clientUser);

			UserLogic logic = new UserLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(ErrorMessage.User.USER_UPDATE_REQUEST_CANNOT_BE_NULL, typeof(ServiceException), () => logic.UpdateUserSettings(accessToken, null));
		}

		[TestMethod]
		public void UpdateUserSettings_Fails_With_Duplicate_EmailAddress()
		{
			FakeLayer fake = FakeLayer.Setup();

			AccessToken accessToken = GetClientUserAccessToken(Guid.NewGuid());
			var clientUser1 = UserStubMatchingAccessToken(accessToken);
			var clientUser2 = UserStub();

			fake.DataLayer.AddItem(clientUser1);
			fake.DataLayer.AddItem(clientUser2);

			UserSettingsRequest request = UserSettingsRequest();

			UserLogic logic = new UserLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(ErrorMessage.User.ANOTHER_USER_HAS_SPECIFIED_EMAIL, typeof(ServiceException), () => logic.UpdateUserSettings(accessToken, request));
		}

		[TestMethod]
		public void UpdateUserSettings_Fails_With_Null_Or_Whitespace_EmailAddress()
		{
			FakeLayer fake = FakeLayer.Setup();

			AccessToken accessToken = GetClientUserAccessToken(Guid.NewGuid());
			var clientUser1 = UserStubMatchingAccessToken(accessToken);

			fake.DataLayer.AddItem(clientUser1);

			UserSettingsRequest request = UserSettingsRequest();
			request.EmailAddress = " ";

			UserLogic logic = new UserLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(ErrorMessage.User.EMAIL_ADDRESS_CANNOT_BE_NULL_OR_EMPTY, typeof(ServiceException), () => logic.UpdateUserSettings(accessToken, request));
		}

		[TestMethod]
		public void UpdateUserSettings_Fails_Null_Or_Whitespace_FirstName()
		{
			FakeLayer fake = FakeLayer.Setup();

			AccessToken accessToken = GetClientUserAccessToken(Guid.NewGuid());
			var clientUser1 = UserStubMatchingAccessToken(accessToken);

			fake.DataLayer.AddItem(clientUser1);

			UserSettingsRequest request = UserSettingsRequest();
			request.FirstName = " ";

			UserLogic logic = new UserLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(ErrorMessage.User.FIRST_NAME_CANNOT_BE_NULL_OR_EMPTY, typeof(ServiceException), () => logic.UpdateUserSettings(accessToken, request));
		}

		private ClientUser UserStub()
		{
			return new ClientUser
					   {
						   Id = Guid.NewGuid(),
						   ClientId = Guid.NewGuid(),
						   State = ClientUserState.Active,
						   EmailAddress = "test@test.net",
						   FirstName = "Bob",
						   LastName = "Bobalso",
						   PhoneNumber = "123-123-1234",
						   Notes = "notes1"
					   };
		}

		private ClientUser UserStubMatchingAccessToken(AccessToken accessToken)
		{
			return new ClientUser
					   {
						   Id = accessToken.UserDetails.Id,
						   ClientId = Guid.NewGuid(),
						   State = ClientUserState.Active,
						   EmailAddress = "test2@test.net",
						   FirstName = "Bob",
						   LastName = "Bobalso",
						   Notes = "notes",
						   PhoneNumber = "123-123-1234"
					   };
		}

		private UserSettingsRequest UserSettingsRequest()
		{
			return new UserSettingsRequest
					   {
						   EmailAddress = "test@test.net",
						   FirstName = "Joe",
						   LastName = "Doe",
						   Notes = "Testr Notes",
						   PhoneNumber = "800-867-5309"
					   };
		}

		private UserAuthenticationDetails GetTestUser()
		{
			return new UserAuthenticationDetails
				   {
					   Id = Guid.NewGuid(),
					   EmailAddress = "tuser@test.net",
					   FirstName = "Test",
					   LastName = "User",
					   MiscInfo = "CN=test_admin,OU=KSWAPI,OU=Salt Lake,DC=medimedia,DC=com",
					   Roles = new List<string> { "Test Group" },
					   Type = UserType.DomainUser
				   };
		}

		private UserAuthenticationDetails GetTestClientUser()
		{
			return new UserAuthenticationDetails
			{
				Id = Guid.NewGuid(),
				EmailAddress = "tuser@test.net",
				FirstName = "Test",
				LastName = "User",
				MiscInfo = null,
				Roles = new List<string> { "Test Group" },
				Type = UserType.ClientUser
			};
		}

		private AccessToken GetDomainAccessToken(Guid? userId)
		{
			return new AccessToken
					{
						ApplicationId = Guid.Empty,
						Id = Guid.Empty,
						Token = ACCESS_TOKEN,
						TokenType = AccessTokenType.InternalUser,
						Expiration = DateTime.UtcNow + TimeSpan.FromMinutes(5),
						UserDetails = GetTestUser()
					};
		}

		private AccessToken GetClientUserAccessToken(Guid? userId)
		{
			return new AccessToken
			{
				ApplicationId = Guid.Empty,
				Id = Guid.Empty,
				Token = ACCESS_TOKEN,
				TokenType = AccessTokenType.InternalUser,
				Expiration = DateTime.UtcNow + TimeSpan.FromMinutes(5),
				UserDetails = GetTestClientUser()
			};
		}
	}
}
