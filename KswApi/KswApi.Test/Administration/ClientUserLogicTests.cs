﻿using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logic.Administration;
using KswApi.Logic.Framework.Constants;
using KswApi.Poco.Enums;
using KswApi.Repositories.Objects;
using KswApi.Test.Framework.Fakes;
using KswApi.Test.Framework.TestUtilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

// ReSharper disable InconsistentNaming

namespace KswApi.Test.Administration
{
    [TestClass]
    public class ClientUserLogicTests
    {
        private const string FILTER = "PermissionType eq 0 or PermissionType eq 2 or PermissionType 3";

        [TestMethod]
        public void GetClientUsers_CountIsGreaterThanMax_ThrowsException()
        {
            ClientUserLogic logic = new ClientUserLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("Count is greater than the maximum page size.", typeof(ServiceException), () => logic.GetClientUsers(0, Constant.MAXIMUM_PAGE_SIZE + 1, "", "", ""));
        }

        [TestMethod]
        public void GetClientUsers_CountLessThanZero_ThrowsException()
        {
            ClientUserLogic logic = new ClientUserLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("Count is less than zero.", typeof(ServiceException), () => logic.GetClientUsers(0, -1, "", "", ""));
        }

        [TestMethod]
        public void GetClientUsers_OffsetLessThanZero_ThrowsException()
        {
            ClientUserLogic logic = new ClientUserLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("Offset is less than zero.", typeof(ServiceException), () => logic.GetClientUsers(-1, 10, "", "", ""));
        }

        [TestMethod]
        public void GetClientUsers_NoSortNoQuery_Successful()
        {
            FakeLayer fake = FakeLayer.Setup();

            Guid clientId = Guid.NewGuid();

            fake.DataLayer.AddItem(new Client { Id = clientId, ClientName = "Test Client" });
            fake.DataLayer.AddItem(new ClientUser { ClientId = clientId, State = ClientUserState.Active, EmailAddress = "test@test.net", FirstName = "Bob", LastName = "Bobalso" });

            ClientUserLogic logic = new ClientUserLogic();

            ClientUserList clientUsers = logic.GetClientUsers(0, 10, "", "", FILTER);

            Assert.IsTrue(clientUsers.Total == 1);
            Assert.IsTrue(clientUsers.Items.Count == 1);
            Assert.IsTrue(clientUsers.Items[0].FirstName == "Bob");
            Assert.IsTrue(clientUsers.Items[0].LastName == "Bobalso");
            Assert.IsTrue(clientUsers.Items[0].ClientId == clientId);
            Assert.IsTrue(clientUsers.Items[0].ClientName == "Test Client");
            Assert.IsTrue(clientUsers.Items[0].EmailAddress == "test@test.net");
        }

        [TestMethod]
        public void GetClientUsers_SortedQueried_Successful()
        {
            FakeLayer fake = FakeLayer.Setup();

            Guid clientId = Guid.NewGuid();

            fake.DataLayer.AddItem(new Client { Id = clientId, ClientName = "Test Client" });

            fake.DataLayer.AddItem(new ClientUser { ClientId = clientId, State = ClientUserState.Active, EmailAddress = "test1@test.net", FirstName = "Bob", LastName = "Bobalso" });
            fake.DataLayer.AddItem(new ClientUser { ClientId = clientId, State = ClientUserState.Active, EmailAddress = "test2@test.net", FirstName = "Jack", LastName = "Sparrow" });
            fake.DataLayer.AddItem(new ClientUser { ClientId = clientId, State = ClientUserState.Active, EmailAddress = "test3@test.net", FirstName = "Jacob", LastName = "Marley" });

            ClientUserLogic logic = new ClientUserLogic();

            ClientUserList clientUsers = logic.GetClientUsers(0, 10, "J", "LastName", FILTER);

            Assert.IsTrue(clientUsers.Total == 2);
            Assert.IsTrue(clientUsers.Items.Count == 2);

            Assert.IsTrue(clientUsers.Items[0].FirstName == "Jacob");
            Assert.IsTrue(clientUsers.Items[0].LastName == "Marley");
            Assert.IsTrue(clientUsers.Items[0].ClientId == clientId);
            Assert.IsTrue(clientUsers.Items[0].ClientName == "Test Client");
            Assert.IsTrue(clientUsers.Items[0].EmailAddress == "test3@test.net");

            Assert.IsTrue(clientUsers.Items[1].FirstName == "Jack");
            Assert.IsTrue(clientUsers.Items[1].LastName == "Sparrow");
            Assert.IsTrue(clientUsers.Items[1].ClientId == clientId);
            Assert.IsTrue(clientUsers.Items[1].ClientName == "Test Client");
            Assert.IsTrue(clientUsers.Items[1].EmailAddress == "test2@test.net");
        }

        [TestMethod]
        public void GetClientUser_InvalidId_ThrowsException()
        {
            FakeLayer.Setup();

            ClientUserLogic logic = new ClientUserLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("The user was not found.", typeof(ServiceException), () => logic.GetClientUser(string.Empty));
        }

        [TestMethod]
        public void GetClientUser_ValidId_Successful()
        {
            FakeLayer fake = FakeLayer.Setup();

            Guid clientId = Guid.NewGuid();
            Guid userId = Guid.NewGuid();

            fake.DataLayer.AddItem(new ClientUser { Id = userId, ClientId = clientId, State = ClientUserState.Active, EmailAddress = "test@test.net", FirstName = "Bob", LastName = "Bobalso" });
            fake.DataLayer.AddItem(new ClientUser { Id = Guid.NewGuid(), ClientId = clientId, State = ClientUserState.Active, EmailAddress = "test@test.net", FirstName = "Bob", LastName = "Bobalso" });

            ClientUserLogic logic = new ClientUserLogic();

            ClientUserResponse clientUser = logic.GetClientUser(userId.ToString());

            Assert.IsTrue(clientUser.Id == userId);
            Assert.IsTrue(clientUser.FirstName == "Bob");
            Assert.IsTrue(clientUser.LastName == "Bobalso");
            Assert.IsTrue(clientUser.ClientId == clientId);
            Assert.IsTrue(clientUser.EmailAddress == "test@test.net");
        }

        [TestMethod]
        public void GetClientUserByEmail_InvalidEmail_ThrowsException()
        {
            ClientUserLogic logic = new ClientUserLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("Invalid email", typeof(ServiceException), () => logic.GetClientUserByEmail(string.Empty));
        }

        [TestMethod]
        public void GetClientUserByEmail_ValidEmail_Successful()
        {
            FakeLayer fake = FakeLayer.Setup();

            Guid clientId = Guid.NewGuid();
            Guid userId = Guid.NewGuid();

            fake.DataLayer.AddItem(new ClientUser { Id = userId, ClientId = clientId, State = ClientUserState.Active, EmailAddress = "test@test.net", FirstName = "Bob", LastName = "Bobalso" });
            fake.DataLayer.AddItem(new ClientUser { Id = Guid.NewGuid(), ClientId = clientId, State = ClientUserState.Active, EmailAddress = "test2@test.net", FirstName = "Jack", LastName = "Sparrow" });

            ClientUserLogic logic = new ClientUserLogic();

            ClientUserResponse clientUser = logic.GetClientUserByEmail("test@test.net");

            Assert.IsTrue(clientUser.Id == userId);
            Assert.IsTrue(clientUser.FirstName == "Bob");
            Assert.IsTrue(clientUser.LastName == "Bobalso");
            Assert.IsTrue(clientUser.ClientId == clientId);
            Assert.IsTrue(clientUser.EmailAddress == "test@test.net");
        }

        [TestMethod]
        public void CreateClientUser_NullObject_ThrowsException()
        {
            ClientUserLogic logic = new ClientUserLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("Request body is not recognized.", typeof(ServiceException), () => logic.CreateClientUser(null));
        }

        [TestMethod]
        public void CreateClientUser_NoEmail_ThrowsException()
        {
            FakeLayer.Setup();

            ClientUserLogic logic = new ClientUserLogic();

            ClientUserRequest request = new ClientUserRequest
                                            {
                                                ClientId = Guid.NewGuid(),
                                                FirstName = "first",
                                                LastName = "last"
                                            };

            ExceptionAssertionUtility.AssertExceptionThrown("Email address is required.", typeof(ServiceException), () => logic.CreateClientUser(request));
        }

        [TestMethod]
        public void CreateClientUser_InvalidEmail_ThrowsException()
        {
            FakeLayer.Setup();

            ClientUserLogic logic = new ClientUserLogic();

            ClientUserRequest request = new ClientUserRequest
            {
                ClientId = Guid.NewGuid(),
                FirstName = "first",
                LastName = "last",
                EmailAddress = "thisisnotvalid.com"
            };

            ExceptionAssertionUtility.AssertExceptionThrown("Invalid email address.", typeof(ServiceException), () => logic.CreateClientUser(request));
        }

        [TestMethod]
        public void CreateClientUser_InvalidClient_ThrowsException()
        {
            FakeLayer.Setup();

            ClientUserLogic logic = new ClientUserLogic();

            ClientUserRequest user = new ClientUserRequest { EmailAddress = "Test@test.net", FirstName = "dude", LastName = "guy" };

            ExceptionAssertionUtility.AssertExceptionThrown(ErrorMessage.Administration.CLIENT_REQUIRED, typeof(ServiceException), () => logic.CreateClientUser(user));
        }

        [TestMethod]
        public void CreateClientUser_InvalidFirstName_ThrowsException()
        {
            FakeLayer.Setup();

            ClientUserLogic logic = new ClientUserLogic();

            ClientUserRequest user = new ClientUserRequest { EmailAddress = "Test@test.net", ClientId = Guid.NewGuid() };

            ExceptionAssertionUtility.AssertExceptionThrown("Invalid first or last name", typeof(ServiceException), () => logic.CreateClientUser(user));
        }

        [TestMethod]
        public void CreateClientUser_InvalidLastName_ThrowsException()
        {
            FakeLayer.Setup();

            ClientUserLogic logic = new ClientUserLogic();

            ClientUserRequest user = new ClientUserRequest { EmailAddress = "Test@test.net", ClientId = Guid.NewGuid(), FirstName = "Bob" };

            ExceptionAssertionUtility.AssertExceptionThrown("Invalid first or last name", typeof(ServiceException), () => logic.CreateClientUser(user));
        }

        [TestMethod]
        public void CreateClientUser_ValidData_Successful()
        {
            Guid clientId = Guid.NewGuid();
            FakeLayer fake = FakeLayer.Setup();
            fake.DataLayer.GetFakeSet<Client>().Add(new Client { Id = clientId });

            ClientUserLogic logic = new ClientUserLogic();



            ClientUserRequest user = new ClientUserRequest { EmailAddress = "Test@test.net", ClientId = clientId, FirstName = "Bob", LastName = "Bobalso" };

            ClientUserResponse newUser = logic.CreateClientUser(user);

            Assert.IsTrue(newUser.FirstName == user.FirstName);
            Assert.IsTrue(newUser.LastName == user.LastName);
            Assert.IsTrue(newUser.EmailAddress == user.EmailAddress);
            Assert.IsTrue(newUser.ClientId == user.ClientId);
        }

        [TestMethod]
        public void UpdateClientUser_InvalidId_ThrowsException()
        {
            FakeLayer.Setup();

            ClientUserLogic logic = new ClientUserLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("Invalid Id", typeof(ServiceException), () => logic.UpdateClientUser(string.Empty, null));
        }

        [TestMethod]
        public void UpdateClientUser_InvalidClientUser_ThrowsException()
        {
            Guid clientUserId = Guid.NewGuid();
            FakeLayer fake = FakeLayer.Setup();
            fake.DataLayer.GetFakeSet<Client>().Add(new Client { Id = clientUserId });

            ClientUserLogic logic = new ClientUserLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("Request body is not recognized.", typeof(ServiceException), () => logic.UpdateClientUser(clientUserId.ToString(), null));
        }

        [TestMethod]
        public void UpdateClientUser_ClientUserNotFound_ThrowsException()
        {
            Guid clientId = Guid.NewGuid();
            FakeLayer fake = FakeLayer.Setup();
            fake.DataLayer.GetFakeSet<Client>().Add(new Client { Id = clientId });

            ClientUserLogic logic = new ClientUserLogic();

            Guid clientUserId = Guid.NewGuid();

			ClientUserUpdateRequest user = new ClientUserUpdateRequest { EmailAddress = "Test@test.net", ClientId = clientId, FirstName = "Bob", LastName = "Bobalso" };

            ExceptionAssertionUtility.AssertExceptionThrown("User not found", typeof(ServiceException), () => logic.UpdateClientUser(clientUserId.ToString(), user));
        }

        [TestMethod]
        public void UpdateClientUser_InvalidEmailAddress_ThrowsException()
        {
            Guid clientId = Guid.NewGuid();
            FakeLayer fake = FakeLayer.Setup();
            fake.DataLayer.GetFakeSet<Client>().Add(new Client { Id = clientId });

            ClientUserLogic logic = new ClientUserLogic();

            Guid clientUserId = Guid.NewGuid();

            ClientUser user = new ClientUser { Id = clientUserId, EmailAddress = "Test@test.net", ClientId = clientId, FirstName = "Bob", LastName = "Bobalso" };
            ClientUser user2 = new ClientUser { Id = Guid.NewGuid(), EmailAddress = "jsparrow@test.net", ClientId = clientId, FirstName = "Jack", LastName = "Sparrow" };

            //This user is here because if we modify the above users and try to do an update, the object in the data layer is changed also
			ClientUserUpdateRequest user3 = new ClientUserUpdateRequest { EmailAddress = "jsparrow@test.net", ClientId = clientId, FirstName = "Bob", LastName = "Bobalso" };

            fake.DataLayer.AddItem(user);
            fake.DataLayer.AddItem(user2);

            ExceptionAssertionUtility.AssertExceptionThrown("Invalid or duplicate email address", typeof(ServiceException), () => logic.UpdateClientUser(clientUserId.ToString(), user3));
        }

        [TestMethod]
        public void UpdateClientUser_UserNotFound_ThrowsException()
        {
            Guid clientId = Guid.NewGuid();
            FakeLayer fake = FakeLayer.Setup();
            fake.DataLayer.GetFakeSet<Client>().Add(new Client { Id = clientId });

            ClientUserLogic logic = new ClientUserLogic();

            Guid clientUserId = Guid.NewGuid();

			ClientUserUpdateRequest user = new ClientUserUpdateRequest { EmailAddress = "Test@test.net", ClientId = clientId, FirstName = "Bob", LastName = "Bobalso" };

            ExceptionAssertionUtility.AssertExceptionThrown("User not found", typeof(ServiceException), () => logic.UpdateClientUser(clientUserId.ToString(), user));
        }

        [TestMethod]
        public void UpdateClientUser_ValidData_Successful()
        {
            Guid clientId = Guid.NewGuid();
            FakeLayer fake = FakeLayer.Setup();
            fake.DataLayer.GetFakeSet<Client>().Add(new Client { Id = clientId });

            ClientUserLogic logic = new ClientUserLogic();

            Guid clientUserId = Guid.NewGuid();

            ClientUser user = new ClientUser { Id = clientUserId, EmailAddress = "Test@test.net", ClientId = clientId, FirstName = "Bob", LastName = "Bobalso" };

            fake.DataLayer.AddItem(user);

			ClientUserUpdateRequest request = new ClientUserUpdateRequest { EmailAddress = "Test@test.net", ClientId = clientId, FirstName = "Jack", LastName = "Sparrow" };

            logic.UpdateClientUser(clientUserId.ToString(), request);

            ClientUserResponse updatedUser = logic.GetClientUser(clientUserId.ToString());

            Assert.IsTrue(updatedUser.FirstName == user.FirstName);
            Assert.IsTrue(updatedUser.LastName == user.LastName);
        }

        [TestMethod]
        public void DeleteClientUser_InvalidId_ThrowsException()
        {
            ClientUserLogic logic = new ClientUserLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("Invalid Id", typeof(ServiceException), () => logic.DeleteClientUser(string.Empty));
        }

        [TestMethod]
        public void DeleteClientUser_UserNotFound_ThrowsException()
        {
            FakeLayer.Setup();

            ClientUserLogic logic = new ClientUserLogic();

            Guid clientUserId = Guid.NewGuid();

            ExceptionAssertionUtility.AssertExceptionThrown("User not found", typeof(ServiceException), () => logic.DeleteClientUser(clientUserId.ToString()));
        }


        [TestMethod]
        public void DeleteClientUser_ValidData_Successful()
        {
            FakeLayer fake = FakeLayer.Setup();

            ClientUserLogic logic = new ClientUserLogic();

            Guid clientUserId = Guid.NewGuid();

            ClientUser user = new ClientUser { Id = clientUserId, EmailAddress = "Test@test.net", ClientId = Guid.NewGuid(), FirstName = "Bob", LastName = "Bobalso" };

            fake.DataLayer.AddItem(user);

            logic.DeleteClientUser(clientUserId.ToString());
        }

        [TestMethod]
        public void ValidateClientEmail_EmptyEmail_ThrowsException()
        {
            ClientUserLogic logic = new ClientUserLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("No email address was specified.", typeof(ServiceException), () => logic.ValidateClientUserEmail(string.Empty));
        }

        [TestMethod]
        public void ValidateClientEmail_InvalidEmail_ThrowsException()
        {
            ClientUserLogic logic = new ClientUserLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("Invalid email address.", typeof(ServiceException), () => logic.ValidateClientUserEmail("invalid@invalid"));
        }
    }
}
