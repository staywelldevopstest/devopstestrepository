﻿using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logic.Administration;
using KswApi.Test.Framework;
using KswApi.Test.Framework.Fakes;
using KswApi.Test.Framework.TestUtilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;

// ReSharper disable InconsistentNaming

namespace KswApi.Test.Administration
{
	[TestClass]
	public class LicenseLogicTests
	{
		[TestMethod]
		public void CreateLicense_ValidLicense_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Client client = fakeLayer.DataLayer.AddClients(1).First();

			License license = FakeData.License();

			license.ClientId = client.Id;

			LicenseLogic logic = new LicenseLogic();

			logic.CreateLicense(license);

			FakeDataSet<License> set = fakeLayer.DataLayer.GetFakeSet<License>();

			Assert.AreEqual(1, set.Count);
			Assert.AreEqual(license.Name, set[0].Name);
			Assert.AreEqual(license.Notes, set[0].Notes);
			Assert.AreEqual(license.ClientId, set[0].ClientId);
			Assert.AreEqual(LicenseStatus.Active, set[0].Status);
			Assert.IsNull(set[0].ParentLicenseId);
			Assert.IsNull(set[0].Children);
			Assert.IsNull(set[0].Applications);
			Assert.IsNull(set[0].Modules);
			Assert.AreNotEqual(default(Guid), set[0].Id);
			Assert.AreNotEqual(default(DateTime), set[0].CreatedDate);
		}

		[TestMethod]
		public void CreateLicense_ValidLicenseWitParentLicense_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Client client = fakeLayer.DataLayer.AddClients(1).First();

			License parent = fakeLayer.DataLayer.AddLicenses(1).First();

			parent.ClientId = client.Id;

			License license = FakeData.License(1);

			license.ClientId = client.Id;

			license.ParentLicenseId = parent.Id;

			LicenseLogic logic = new LicenseLogic();

			logic.CreateLicense(license);

			FakeDataSet<License> set = fakeLayer.DataLayer.GetFakeSet<License>();

			Assert.AreEqual(2, set.Count);
			Assert.AreEqual(license.Name, set[1].Name);
			Assert.AreEqual(license.Notes, set[1].Notes);
			Assert.AreEqual(license.ClientId, set[1].ClientId);
			Assert.AreEqual(license.ParentLicenseId, set[1].ParentLicenseId);
			Assert.AreEqual(LicenseStatus.Active, set[1].Status);
			Assert.IsNull(set[1].Children);
			Assert.IsNull(set[1].Applications);
			Assert.IsNull(set[1].Modules);
			Assert.AreNotEqual(default(Guid), set[1].Id);
			Assert.AreNotEqual(default(DateTime), set[1].CreatedDate);
			Assert.IsNotNull(set[0].Children);
			Assert.AreEqual(1, set[0].Children.Count);
			Assert.AreEqual(set[1].Id, set[0].Children[0]);
		}

		[TestMethod]
		public void CreateLicense_NoLicense_ThrowsServiceException()
		{
			LicenseLogic logic = new LicenseLogic();

			ExceptionAssertionUtility.AssertExceptionThrown("No license is specified in the message body.", typeof(ServiceException), () => logic.CreateLicense(null));
		}

		[TestMethod]
		public void CreateLicense_NoLicenseName_ThrowsServiceException()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Client client = fakeLayer.DataLayer.AddClients(1).First();

			License license = FakeData.License(1);

			license.ClientId = client.Id;
			license.Name = string.Empty;

			LicenseLogic logic = new LicenseLogic();

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.CreateLicense(license));

			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void CreateLicense_LicenseNameTooLong_ThrowsServiceException()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Client client = fakeLayer.DataLayer.AddClients(1).First();

			License license = FakeData.License(1);

			license.ClientId = client.Id;
			//Just needs to be more than 100 characters
			license.Name = "askdjhfalksjdhfalksjdfhlaskjdfa;lksdjf;alksdjf;alskdjf;alskdjf;alskdjf;alskdfj;alskdfj;aslkdfj;alskdjf;alksdjf;";

			LicenseLogic logic = new LicenseLogic();

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.CreateLicense(license));

			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void CreateLicense_NoClient_ThrowsServiceException()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			License license = FakeData.License(1);

			license.ClientId = Guid.Empty;

			LicenseLogic logic = new LicenseLogic();

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.CreateLicense(license));

			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void CreateLicense_InvalidParent_ThrowsServiceException()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Client client = fakeLayer.DataLayer.AddClients(1).First();

			License notParent = fakeLayer.DataLayer.AddLicenses(1).First();

			notParent.ClientId = client.Id;

			License license = FakeData.License(1);

			license.ClientId = client.Id;

			license.ParentLicenseId = Guid.NewGuid();

			LicenseLogic logic = new LicenseLogic();

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.CreateLicense(license));

			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void GetLicenses_NoFilter_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.AddLicenses(9);

			LicenseLogic logic = new LicenseLogic();

			LicenseList list = logic.GetLicenses(3, 200, null, null, null);

			Assert.IsNotNull(list);
			Assert.AreEqual(9, list.Total);
			Assert.AreEqual(3, list.Offset);
			Assert.AreEqual(6, list.Items.Count);

			// should sort by name
			for (int i = 0; i < 6; i++)
			{
				Assert.AreEqual("Name" + (i + 4), list.Items[i].Name);
			}
		}

		[TestMethod]
		public void GetLicenses_NoFilter_SortByNameDescending_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.AddLicenses(9);

			LicenseLogic logic = new LicenseLogic();

			LicenseList list = logic.GetLicenses(0, 200, null, null, "Name desc");

			Assert.IsNotNull(list);
			Assert.AreEqual(9, list.Total);
			Assert.AreEqual(0, list.Offset);
			Assert.AreEqual(9, list.Items.Count);

			// should sort by name
			for (int i = 0; i < 9; i++)
			{
				Assert.AreEqual("Name" + (9 - i), list.Items[i].Name);
			}
		}

		[TestMethod]
		public void GetLicenses_WitFilter_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.AddLicenses(10);

			LicenseLogic logic = new LicenseLogic();

			LicenseList list = logic.GetLicenses(0, 200, "Name7", null, null);

			Assert.IsNotNull(list);
			Assert.AreEqual(1, list.Total);
			Assert.AreEqual(0, list.Offset);
			Assert.AreEqual(1, list.Items.Count);
			Assert.AreEqual("Name7", list.Items[0].Name);
		}

		[TestMethod]
		public void GetLicenses_CountTooLarge_ThrowsException()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.AddLicenses(10);

			LicenseLogic logic = new LicenseLogic();

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.GetLicenses(0, 201, null, null, null));

			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void GetLicenses_OffsetLessThanZero_ThrowsException()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.AddLicenses(10);

			LicenseLogic logic = new LicenseLogic();

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.GetLicenses(-1, 200, null, null, null));

			Assert.AreEqual(HttpStatusCode.BadRequest, exception.StatusCode);
		}

		[TestMethod]
		public void GetLicenses_GetLicense_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			License license = FakeData.License();

			fakeLayer.DataLayer.Setup(license);

			LicenseLogic logic = new LicenseLogic();

			License returnLicense = logic.GetLicense(license.Id.ToString());

			Assert.IsNotNull(returnLicense);
			Assert.AreSame(license, returnLicense);
		}

		[TestMethod]
		public void GetLicenses_GetLicenseWithEmptyId_ThrowsException()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			LicenseLogic logic = new LicenseLogic();

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.GetLicense(""));

			Assert.AreEqual(HttpStatusCode.NotFound, exception.StatusCode);
		}

		[TestMethod]
		public void GetLicenses_GetLicenseWithInvalidId_ThrowsException()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			LicenseLogic logic = new LicenseLogic();

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.GetLicense("12345678"));

			Assert.AreEqual(HttpStatusCode.NotFound, exception.StatusCode);
		}

		[TestMethod]
		public void GetLicenses_GetLicenseWithNotFoundId_ThrowsException()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();


			License license = FakeData.License();

			fakeLayer.DataLayer.Setup(license);

			LicenseLogic logic = new LicenseLogic();

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.GetLicense(Guid.NewGuid().ToString()));

			Assert.AreEqual(HttpStatusCode.NotFound, exception.StatusCode);
		}

		[TestMethod]
		public void UpdateLicense_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			List<Client> clients = fakeLayer.DataLayer.AddClients(1);

			License license = FakeData.License();
			license.ClientId = clients[0].Id;

			fakeLayer.DataLayer.Setup(license);

			LicenseLogic logic = new LicenseLogic();

			License newLicense = new License();
			newLicense.Name = "My new name";
			newLicense.Notes = "New notes";
			newLicense.ClientId = clients[0].Id;

			License returnLicense = logic.UpdateLicense(license.Id.ToString(), newLicense);

			Assert.IsNotNull(returnLicense);
			Assert.AreEqual(newLicense.Name, returnLicense.Name);
			Assert.AreEqual(newLicense.Notes, returnLicense.Notes);

			FakeDataSet<License> set = fakeLayer.DataLayer.GetFakeSet<License>();

			Assert.AreEqual(1, set.Count);
			Assert.AreEqual(newLicense.Name, set[0].Name);
			Assert.AreEqual(newLicense.Notes, set[0].Notes);
			Assert.AreEqual(clients[0].Id, set[0].ClientId);
		}

		[TestMethod]
		public void UpdateLicense_ChangeClientId_ThrowsException()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			List<Client> clients = fakeLayer.DataLayer.AddClients(2);

			License license = FakeData.License();
			license.ClientId = clients[0].Id;

			fakeLayer.DataLayer.Setup(license);

			LicenseLogic logic = new LicenseLogic();

			License newLicense = new License();
			newLicense.ClientId = clients[1].Id;
			newLicense.Name = "My new name";
			newLicense.Notes = "New notes";

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.UpdateLicense(license.Id.ToString(), newLicense));

			Assert.AreEqual(exception.StatusCode, HttpStatusCode.BadRequest);
		}

		[TestMethod]
		public void UpdateLicense_HasInvalidParentLicense_ThrowsException()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			List<Client> clients = fakeLayer.DataLayer.AddClients(1);

			License license = FakeData.License();
			license.ClientId = clients[0].Id;

			fakeLayer.DataLayer.Setup(license);

			LicenseLogic logic = new LicenseLogic();

			License newLicense = new License();
			newLicense.ClientId = clients[0].Id;
			newLicense.Name = "My new name";
			newLicense.Notes = "New notes";
			newLicense.ParentLicenseId = Guid.NewGuid();

			ExceptionAssertionUtility.AssertExceptionThrown("Invalid parent license.", typeof(ServiceException), () => logic.UpdateLicense(license.Id.ToString(), newLicense));
		}

		[TestMethod]
		public void UpdateLicense_NullLicense_ThrowsException()
		{
			LicenseLogic licenseLogic = new LicenseLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("The license ID was not found.", typeof(ServiceException), () => licenseLogic.UpdateLicense(string.Empty, null));
		}

		[TestMethod]
		public void UpdateLicense_InvalidLicenseId_ThrowsException()
		{
			LicenseLogic licenseLogic = new LicenseLogic();

			License license = new License();

			ExceptionAssertionUtility.AssertExceptionThrown("The license ID was not found.", typeof(ServiceException), () => licenseLogic.UpdateLicense(string.Empty, license));
		}

		[TestMethod]
		public void UpdateLicense_LicenseNameEmpty_ThrowsException()
		{
			LicenseLogic licenseLogic = new LicenseLogic();

			License license = new License();
			license.Id = Guid.NewGuid();

			Guid licenseId = license.Id;

			ExceptionAssertionUtility.AssertExceptionThrown("License name is required.", typeof(ServiceException), () => licenseLogic.UpdateLicense(licenseId.ToString(), license));
		}

		[TestMethod]
		public void UpdateLicense_LicenseNameTooLong_ThrowsException()
		{
			LicenseLogic licenseLogic = new LicenseLogic();

			License license = new License();
			license.Id = Guid.NewGuid();
			license.Name =
				"alksjdhdflajksdhflaksjdhflaksjdhflkasjdhflaskjdhflaksdjhfalskjdhflaskdjhflaskjdhflaksjdhflaksjdhflaksjdhflaksjdhf";

			Guid licenseId = license.Id;

			ExceptionAssertionUtility.AssertExceptionThrown("License name cannot be longer than 100 characters.", typeof(ServiceException), () => licenseLogic.UpdateLicense(licenseId.ToString(), license));
		}

		[TestMethod]
		public void UpdateLicense_NoClientAssigned_ThrowsException()
		{
			LicenseLogic licenseLogic = new LicenseLogic();

			License license = new License();
			license.Id = Guid.NewGuid();
			license.Name = "Test License Name";

			Guid licenseId = license.Id;

			ExceptionAssertionUtility.AssertExceptionThrown("A license must be assigned to a client.", typeof(ServiceException), () => licenseLogic.UpdateLicense(licenseId.ToString(), license));
		}

		[TestMethod]
		public void GetLicenses_DeleteLicense_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Client client = FakeData.Client();
			License license = FakeData.License();
			license.ClientId = client.Id;

			fakeLayer.DataLayer.Setup(client);
			fakeLayer.DataLayer.Setup(license);

			LicenseLogic logic = new LicenseLogic();

			License returnLicense = logic.DeleteLicense(license.Id.ToString());

			Assert.IsNotNull(returnLicense);
			Assert.AreEqual(license.Id, returnLicense.Id);
			Assert.AreEqual(license.ClientId, returnLicense.ClientId);
			Assert.AreEqual(license.Name, returnLicense.Name);
			Assert.AreEqual(license.Notes, returnLicense.Notes);
            Assert.IsTrue(license.Status == LicenseStatus.Inactive);
		}

		[TestMethod]
		public void CreateLicenseApplication_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Client client = FakeData.Client();
			License license = FakeData.License();
			license.ClientId = client.Id;

			fakeLayer.DataLayer.Setup(client);
			fakeLayer.DataLayer.Setup(license);

			LicenseLogic logic = new LicenseLogic();

			Application application = FakeData.Application();

			Application returnApplication = logic.CreateLicenseApplication(license.Id.ToString(), application);

			Assert.IsNotNull(returnApplication);
			Assert.AreEqual(application.Name, returnApplication.Name);
			Assert.AreEqual(application.Description, returnApplication.Description);

			FakeDataSet<Application> set = fakeLayer.DataLayer.GetFakeSet<Application>();

			Assert.AreEqual(1, set.Count);
		}

		[TestMethod]
		public void CreateLicenseApplication_InvalidLicense_ThrowsException()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Client client = FakeData.Client();
			License license = FakeData.License();
			license.ClientId = client.Id;

			fakeLayer.DataLayer.Setup(client);

			LicenseLogic logic = new LicenseLogic();

			Application application = FakeData.Application();

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.CreateLicenseApplication(license.Id.ToString(), application));

			Assert.AreEqual(HttpStatusCode.NotFound, exception.StatusCode);

			FakeDataSet<Application> set = fakeLayer.DataLayer.GetFakeSet<Application>();

			Assert.AreEqual(0, set.Count);
		}

		[TestMethod]
		public void GetLicenseApplications_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Client client = FakeData.Client();
			License license = FakeData.License();
			license.ClientId = client.Id;

			fakeLayer.DataLayer.Setup(client);
			fakeLayer.DataLayer.Setup(license);

			LicenseLogic logic = new LicenseLogic();

			List<Application> applications = FakeData.Applications(1);

			foreach (Application application in applications)
				logic.CreateLicenseApplication(license.Id.ToString(), application);

			ApplicationList returnedApplications = logic.GetLicenseApplications(license.Id.ToString());

			Assert.IsNotNull(returnedApplications);
			Assert.AreEqual(1, returnedApplications.Total);
			Assert.AreEqual(1, returnedApplications.Items.Count);
		}

		[TestMethod]
		public void GetLicenseApplication_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Client client = FakeData.Client();
			License license = FakeData.License();
			license.ClientId = client.Id;

			fakeLayer.DataLayer.Setup(client);
			fakeLayer.DataLayer.Setup(license);

			LicenseLogic logic = new LicenseLogic();

			Application application = logic.CreateLicenseApplication(license.Id.ToString(), FakeData.Application());

			Application returnedApplication = logic.GetLicenseApplication(license.Id.ToString(), application.Id.ToString());

			Assert.IsNotNull(returnedApplication);
			Assert.AreEqual(application.Id, returnedApplication.Id);
			Assert.AreEqual(application.Name, returnedApplication.Name);
		}

		[TestMethod]
		public void GetLicenseByApplication_NullApplicationId_ThrowsException()
		{
			LicenseLogic logic = new LicenseLogic();

			ExceptionAssertionUtility.AssertExceptionThrown("Application Id was not specified.", typeof(ServiceException), () => logic.GetLicenseByApplication(string.Empty));
		}

		[TestMethod]
		public void GetLicenseByApplication_InvalidApplicationId_ThrowsException()
		{
			LicenseLogic logic = new LicenseLogic();

			ExceptionAssertionUtility.AssertExceptionThrown("Application Id is invalid.", typeof(ServiceException), () => logic.GetLicenseByApplication("alskdjfa;lskfjdaslkdfj"));
		}

		[TestMethod]
		public void UpdateLicenseApplication_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Client client = FakeData.Client();
			License license = FakeData.License();
			license.ClientId = client.Id;

			fakeLayer.DataLayer.Setup(client);
			fakeLayer.DataLayer.Setup(license);

			LicenseLogic logic = new LicenseLogic();

			Application application = logic.CreateLicenseApplication(license.Id.ToString(), FakeData.Application());

			Application toUpdateApplication = new Application
												  {
													  Name = "New Name",
													  Description = "New Description"
												  };

			Application returnedApplication = logic.UpdateLicenseApplication(license.Id.ToString(), application.Id.ToString(), toUpdateApplication);

			Assert.IsNotNull(returnedApplication);
			Assert.AreEqual(application.Id, returnedApplication.Id);
			Assert.AreEqual(toUpdateApplication.Name, returnedApplication.Name);
			Assert.AreEqual(toUpdateApplication.Description, returnedApplication.Description);
		}

		[TestMethod]
		public void DeleteLicenseApplication_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Client client = FakeData.Client();
			License license = FakeData.License();
			license.ClientId = client.Id;

			fakeLayer.DataLayer.Setup(client);
			fakeLayer.DataLayer.Setup(license);

			LicenseLogic logic = new LicenseLogic();

			Application application = logic.CreateLicenseApplication(license.Id.ToString(), FakeData.Application());

			Application returnedApplication = logic.DeleteLicenseApplication(license.Id.ToString(), application.Id.ToString());

			Assert.IsNotNull(returnedApplication);
			Assert.AreEqual(application.Id, returnedApplication.Id);
			Assert.AreEqual(application.Name, returnedApplication.Name);
			Assert.AreEqual(application.Description, returnedApplication.Description);

			FakeDataSet<Application> set = fakeLayer.DataLayer.GetFakeSet<Application>();

			Assert.AreEqual(0, set.Count);
		}

		[TestMethod]
		public void DeleteLicense_InvalidClientId_ThrowsException()
		{
			LicenseLogic licenseLogic = new LicenseLogic();

			ExceptionAssertionUtility.AssertExceptionThrown("The license ID was not found.", typeof(ServiceException), () => licenseLogic.DeleteLicense(string.Empty));
		}

		[TestMethod]
		public void DeleteLicense_LicenseNotFound_ThrowsException()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Client client = FakeData.Client();
			License license = FakeData.License();
			license.ClientId = client.Id;

			fakeLayer.DataLayer.Setup(client);

			LicenseLogic logic = new LicenseLogic();

			ServiceException exception = Expect.Exception<ServiceException>(() => logic.DeleteLicense(license.Id.ToString()));

			Assert.AreEqual(HttpStatusCode.NotFound, exception.StatusCode);
		}

		[TestMethod]
		public void GetClientLicenseDetail_NullClientId_ThrowsException()
		{
			LicenseLogic licenseLogic = new LicenseLogic();

			ExceptionAssertionUtility.AssertExceptionThrown("No client was specified.", typeof(ServiceException), () => licenseLogic.GetClientLicenseDetail(string.Empty, null));
		}

		[TestMethod]
		public void GetClientLicenseDetail_InvalidClientId_ThrowsException()
		{
			LicenseLogic licenseLogic = new LicenseLogic();

			ExceptionAssertionUtility.AssertExceptionThrown("Invalid client ID.", typeof(ServiceException), () => licenseLogic.GetClientLicenseDetail("alskdjhfalsdkfhdjf", null));
		}

		[TestMethod]
		public void GetClientLicenseDetail_ValidClientNotFound_ThrowsException()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Client client = FakeData.Client();
			client.Id = Guid.NewGuid();

			fakeLayer.DataLayer.Setup(client);

			LicenseLogic logic = new LicenseLogic();

			ExceptionAssertionUtility.AssertExceptionThrown("Client was not found.", typeof(ServiceException), () => logic.GetClientLicenseDetail(Guid.NewGuid().ToString(), string.Empty));
		}

		[TestMethod]
		public void GetClientLicenseDetail_NoLicenseFilter_Successful()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid clientId = Guid.NewGuid();

			Client client = FakeData.Client();

			client.Id = clientId;

			License license = FakeData.License();
			License license2 = FakeData.License();
			License license3 = FakeData.License();
			License license4 = FakeData.License();

			license.ClientId = clientId;
			license.Id = Guid.NewGuid();
			license.Name = "License 1";
			license.ParentLicenseId = null;

			license2.ClientId = clientId;
			license2.Id = Guid.NewGuid();
			license2.Name = "License 2";
			license2.ParentLicenseId = null;

			license3.ClientId = clientId;
			license3.Id = Guid.NewGuid();
			license3.Name = "License 3";
			license3.ParentLicenseId = null;

			license4.ClientId = clientId;
			license4.Id = Guid.NewGuid();
			license4.Name = "License 4";
			license4.ParentLicenseId = license.Id;

			Application app = FakeData.Application();

			app.Id = Guid.NewGuid();
			app.Name = "App 1";
			app.Secret = "aslkdjfhalskjdfhalksjdfhf";

			license4.Applications = new List<Guid>();
			license4.Applications.Add(app.Id);

			fakeLayer.DataLayer.Setup(client);
			fakeLayer.DataLayer.AddItem(license);
			fakeLayer.DataLayer.AddItem(license2);
			fakeLayer.DataLayer.AddItem(license3);
			fakeLayer.DataLayer.AddItem(license4);
			fakeLayer.DataLayer.Setup(app);

			LicenseLogic logic = new LicenseLogic();

			ClientLicenseDetail detail = logic.GetClientLicenseDetail(clientId.ToString(), string.Empty);

			Assert.IsTrue(detail.ClientId == clientId);
			Assert.IsTrue(detail.LicenseCount == 4);
		}

		[TestMethod]
		public void GetClientLicenseDetail_LicenseFilter_Successful()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid clientId = Guid.NewGuid();

			Client client = FakeData.Client();

			client.Id = clientId;

			License license = FakeData.License();
			License license2 = FakeData.License();
			License license3 = FakeData.License();
			License license4 = FakeData.License();

			license.ClientId = clientId;
			license.Id = Guid.NewGuid();
			license.Name = "License 1";
			license.ParentLicenseId = null;

			license2.ClientId = clientId;
			license2.Id = Guid.NewGuid();
			license2.Name = "License 2";
			license2.ParentLicenseId = license.Id;

			license3.ClientId = clientId;
			license3.Id = Guid.NewGuid();
			license3.Name = "License 3";
			license3.ParentLicenseId = license.Id;

			license4.ClientId = clientId;
			license4.Id = Guid.NewGuid();
			license4.Name = "License 4";
			license4.ParentLicenseId = license.Id;

			fakeLayer.DataLayer.Setup(client);
			fakeLayer.DataLayer.AddItem(license);
			fakeLayer.DataLayer.AddItem(license2);
			fakeLayer.DataLayer.AddItem(license3);
			fakeLayer.DataLayer.AddItem(license4);

			LicenseLogic logic = new LicenseLogic();

			ClientLicenseDetail detail = logic.GetClientLicenseDetail(clientId.ToString(), "License");

			Assert.IsTrue(detail.ClientId == clientId);
			Assert.IsTrue(detail.LicenseCount == 4);
		}
	}
}
