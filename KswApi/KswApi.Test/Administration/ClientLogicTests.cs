﻿using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logic;
using KswApi.Logic.Administration;
using KswApi.Test.Framework.Fakes;
using KswApi.Test.Framework.TestUtilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

// ReSharper disable InconsistentNaming

namespace KswApi.Test.Administration
{
    [TestClass]
    public class ClientLogicTests
    {
        #region Private Constants

        private const int NUM_ITEMS_PER_PAGE = 10;
        private const int TOTAL_ITEMS_TO_CREATE = 25;
        private const string TEST_NOTES = "Test Notes";
        private const string PHONE = "801-555-5555";

        #endregion

        #region Test Methods

        [TestMethod]
        public void GetClients_NotFiltered_Successful()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            fakeLayer.DataLayer.AddClients(TOTAL_ITEMS_TO_CREATE);

            ClientLogic clientLogic = new ClientLogic();

            ClientList clientList = clientLogic.GetClients(0, NUM_ITEMS_PER_PAGE, string.Empty, string.Empty);

            Assert.IsTrue(clientList.Items.Count == NUM_ITEMS_PER_PAGE, "Page count not equal");
            Assert.IsTrue(clientList.Total == TOTAL_ITEMS_TO_CREATE, "Total count not equal");
        }

        [TestMethod]
        public void GetClients_Filtered_Successful()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            List<Client> clients = GetTestClients();

            fakeLayer.DataLayer.AddClients(clients);

            ClientLogic clientLogic = new ClientLogic();

            ClientList clientList = clientLogic.GetClients(0, NUM_ITEMS_PER_PAGE, "Test", string.Empty);

            Assert.IsTrue(clientList.Items.Count == 1, "Page count not equal");
        }

        [TestMethod]
        public void GetClients_FilteredDeletedSuccessful()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            List<Client> clients = GetTestClients();

            fakeLayer.DataLayer.AddClients(clients);

            ClientLogic clientLogic = new ClientLogic();

            ClientList clientList = clientLogic.GetClients(0, NUM_ITEMS_PER_PAGE, string.Empty, string.Empty);

            Assert.IsTrue(clientList.Items.Count == 2, "Page count not equal");
        }

        [TestMethod]
        public void GetClient_ById_Successful()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            List<Client> clients = GetTestClients();

            Guid clientId = clients[0].Id;

            fakeLayer.DataLayer.AddClients(clients);

            ClientLogic clientLogic = new ClientLogic();

            ClientResponse client = clientLogic.GetClient(clientId.ToString());

            Assert.IsTrue(client.Id == clientId);
        }

        [TestMethod]
        public void GetClient_InvalidId_ThrowsException()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            List<Client> clients = GetTestClients();

            string clientId = "asdfija;slkdfja;slkdfja;slkdfja;sldkfj";

            fakeLayer.DataLayer.AddClients(clients);

            ClientLogic clientLogic = new ClientLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("Invalid client Id", typeof(ServiceException), () => clientLogic.GetClient(clientId.ToString()));
        }

        [TestMethod]
        public void CreateClient_Successful()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            ClientLogic clientLogic = new ClientLogic();

			ClientResponse client = clientLogic.CreateClient(new ClientRequest { ClientName = "Test Client", ContactName = "Contact Name", ContactEmail = "TestEmail@test.com", ContactPhone = PHONE });

	        FakeDataSet<Client> clients = fakeLayer.DataLayer.GetFakeSet<Client>();

			Assert.IsNotNull(clients);
			Assert.AreEqual(1, clients.Count);
	        
			Client savedClient = clients[0];
			Assert.AreEqual("Test Client", savedClient.ClientName);
			Assert.AreEqual("test client", savedClient.CaseInsensitiveName);
			Assert.AreEqual("Contact Name", savedClient.ContactName);
			Assert.AreEqual("TestEmail@test.com", savedClient.ContactEmail);
			Assert.AreEqual("801-555-5555", savedClient.ContactPhone);
        }

        [TestMethod]
        public void CreateClient_ClientIsNull_ThrowsException()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();
            ClientLogic clientLogic = new ClientLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("The client was empty", typeof(ServiceException), () =>
                clientLogic.CreateClient(null));
        }

        [TestMethod]
        public void CreateClient_ClientNameEmpty_ThrowsException()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();
            ClientLogic clientLogic = new ClientLogic();

            ExceptionAssertionUtility.AssertExceptionThrown("The client name was not specified", typeof(ServiceException), () =>
				clientLogic.CreateClient(new ClientRequest { ClientName = "", ContactName = "Contact Name", ContactEmail = "Test Email" }));
        }

        // Commented out as requirements changed.  May need again later
        //[TestMethod]
        //public void CreateClient_ClientContactNameEmpty_ThrowsException()
        //{
        //    FakeLayer fakeLayer = FakeLayer.Setup();
        //    ClientLogic clientLogic = new ClientLogic();

        //    ExceptionAssertionUtility.AssertExceptionThrown("The client did not specify a primary contact name", typeof(ServiceException), () =>
        //        clientLogic.CreateClient(new Client { ClientName = "Test Client", ContactName = "", ContactEmail = "Test Email" }));
        //}

        //[TestMethod]
        //public void CreateClient_ClientContactEmailEmpty_ThrowsException()
        //{
        //    FakeLayer fakeLayer = FakeLayer.Setup();
        //    ClientLogic clientLogic = new ClientLogic();

        //    ExceptionAssertionUtility.AssertExceptionThrown("The client did not specify a primary contact email", typeof(ServiceException), () =>
        //        clientLogic.CreateClient(new Client { ClientName = "Test Client", ContactName = "Contact Name", ContactEmail = "" }));
        //}

        [TestMethod]
        public void UpdateClient_Successful()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            List<Client> clients = GetTestClients();

            fakeLayer.DataLayer.AddClients(clients);

            Client client = clients[0];

            ClientLogic clientLogic = new ClientLogic();

            client.Notes = TEST_NOTES;

			ClientResponse newClient = clientLogic.UpdateClient(client.Id.ToString(), client);

            Assert.IsTrue(client.Notes == newClient.Notes, "Updated Notes are not equal");
        }

        [TestMethod]
        public void DeleteClient_Sucessful()
        {
            FakeLayer fakeLayer = FakeLayer.Setup();

            List<Client> clients = GetTestClients();

            fakeLayer.DataLayer.AddClients(clients);

            Client client = clients[0];

            ClientLogic clientLogic = new ClientLogic();

			ClientResponse deletedClient = clientLogic.DeleteClient(client.Id.ToString());

            Assert.IsTrue(deletedClient.Status == Interface.Enums.ClientStatus.Deleted, "Client Status is not set to deleted");
        }

        #endregion

        #region Private Methods

        private List<Client> GetTestClients()
        {
            List<Client> clients = new List<Client>();

            clients.Add(new Client { ClientName = "Test Client", ContactName = "Test Name", ContactEmail = "TestEmail@email.com", Id = Guid.NewGuid(), Status = ClientStatus.Active, Licenses = new List<Guid>() });
            clients.Add(new Client { ClientName = "Deleted Client", ContactName = "Test Name", ContactEmail = "TestEmail@email.com", Id = Guid.NewGuid(), Status = ClientStatus.Deleted });
            clients.Add(new Client { ClientName = "Disabled Client", ContactName = "Test Name", ContactEmail = "TestEmail@email.com", Id = Guid.NewGuid(), Status = ClientStatus.Disabled });
            clients.Add(new Client { ClientName = "Active Client", ContactName = "Test Name", ContactEmail = "TestEmail@email.com", Id = Guid.NewGuid(), Status = ClientStatus.Active });

            return clients;
        }

        #endregion
    }
}
