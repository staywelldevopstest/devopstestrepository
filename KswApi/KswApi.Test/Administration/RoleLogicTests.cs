﻿using System;
using System.Collections.Generic;
using KswApi.Interface.Objects;
using KswApi.Logic.Administration;
using KswApi.Logic.Framework;
using KswApi.Repositories.Interfaces;
using KswApi.Test.Framework.Fakes;
using KswApi.Test.Framework.TestUtilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using KswApi.Interface.Exceptions;
using Moq;

// ReSharper disable InconsistentNaming

namespace KswApi.Test.Administration
{
	[TestClass]
	public class RoleLogicTests
	{
		#region Private Constants

		private const string INVALID_ROLE_ID = "InvalidRole";
		private const string NEW_ROLE = "TestRole";
		private const string ADMIN_ROLE = "Admin";
		private const string CONTENT_MANAGER_ROLE = "Content Manager";
		private const string EDITOR_ROLE = "Editor";
		private const string INDEXER_ROLE = "Indexer";
		private const string BROWSER_ROLE = "Browser";

		private const string CREATE_RIGHT = "Create";
		private const string INVALID_RIGHT = "BadRight";

		#endregion

		#region Private Properties

		private List<string> _rights;
		private List<Role> _roles;
		private FakeLayer _fakeLayer;

		#endregion

		#region Test Methods
		[TestMethod]
		public void GetAllRights_Successful()
		{
			SetupAll();

			RoleLogic roleLogic = new RoleLogic();
			RightList list = roleLogic.GetAllRights();

			Assert.IsTrue(list.Items.Count == _rights.Count);
			Assert.IsTrue(list.Items.Contains(_rights[0]));
		}

		[TestMethod]
		public void GetAllRoles_ValidOffsetAndCount_Successful()
		{
			SetupAll();

			RoleLogic roleLogic = new RoleLogic();

			RoleList list = roleLogic.GetAllRoles();
			Assert.IsNotNull(list);
			Assert.IsNotNull(list.Items);
			Assert.IsTrue(list.Items[0].Name == _roles[0].Name);
			Assert.IsTrue(list.Items[1].Name == _roles[1].Name);
			Assert.IsTrue(list.Items[2].Name == _roles[2].Name);
			Assert.IsTrue(list.Items[3].Name == _roles[3].Name);
			Assert.IsTrue(list.Items[4].Name == _roles[4].Name);
		}

		[TestMethod]
		public void GetRole_NoRoleId_ThrowsServiceException()
		{
			SetupAll();

			RoleLogic roleLogic = new RoleLogic();

			ExceptionAssertionUtility.AssertExceptionThrown("No role ID was specified", typeof(ServiceException), () => roleLogic.GetRole(string.Empty));
		}

		[TestMethod]
		public void GetRole_InvalidRoleId_ThrowsServiceException()
		{
			SetupAll();

			RoleLogic roleLogic = new RoleLogic();

			ExceptionAssertionUtility.AssertExceptionThrown("The specified role ID does not exist", typeof(ServiceException), () => roleLogic.GetRole(INVALID_ROLE_ID));
		}

		[TestMethod]
		public void GetRole_ValidRoleId_Successful()
		{
			SetupAll();

			RoleLogic roleLogic = new RoleLogic();

			Role role = roleLogic.GetRole(ADMIN_ROLE);

			Assert.IsTrue(role.Name == ADMIN_ROLE);
		}

		[TestMethod]
		public void CreateRole_EmptyRole_ValidateInputRole_ThrowsServiceException()
		{
			RoleLogic roleLogic = new RoleLogic();

			ExceptionAssertionUtility.AssertExceptionThrown("The role was empty", typeof(ServiceException), () => roleLogic.CreateRole(null));
		}

		[TestMethod]
		public void CreateRole_RoleHasNoName_ValidateInputRole_ThrowsServiceException()
		{
			RoleLogic roleLogic = new RoleLogic();

			ExceptionAssertionUtility.AssertExceptionThrown("The role did not specify a name", typeof(ServiceException), () => roleLogic.CreateRole(new Role()));
		}

		[TestMethod]
		public void CreateRole_ContainsInvalidRight_ValidateInputRole_ThrowServiceException()
		{
			SetupAll();

			RoleLogic roleLogic = new RoleLogic();

			ExceptionAssertionUtility.AssertExceptionThrown("The list of rights contains an invalid right: BadRight.", typeof(ServiceException), () => roleLogic.CreateRole(new Role { Name = ADMIN_ROLE, Rights = new List<string> { INVALID_RIGHT } }));
		}

		[TestMethod]
		public void CreateRole_DuplicateRole_ThrowsServiceException()
		{
			SetupAll();

			RoleLogic roleLogic = new RoleLogic();

			ExceptionAssertionUtility.AssertExceptionThrown("The role name is a duplicate", typeof(ServiceException), () => roleLogic.CreateRole(new Role { Name = ADMIN_ROLE, Rights = new List<string> { CREATE_RIGHT } }));
		}

		[TestMethod]
		public void CreateRole_ValidRole_Successful()
		{
			SetupAll();

			RoleLogic roleLogic = new RoleLogic();

			Role role = roleLogic.CreateRole(new Role { Name = NEW_ROLE, Rights = new List<string> { CREATE_RIGHT } });

			Assert.IsTrue(role.Name == NEW_ROLE);
			//Assert.Fail("Need to fix this unit test");
		}

		[TestMethod]
		public void UpdateRole_RoleIdNotSpecified_ThrowsServiceException()
		{
			SetupAll();

			RoleLogic roleLogic = new RoleLogic();

			ExceptionAssertionUtility.AssertExceptionThrown("No role ID was specified", typeof(ServiceException), () => roleLogic.UpdateRole(string.Empty, new Role { Name = ADMIN_ROLE, Rights = new List<string> { CREATE_RIGHT } }));
		}

		[TestMethod]
		public void UpdateRole_RoleDoesNotExist_ThrowsServiceException()
		{
			SetupAll();

			RoleLogic roleLogic = new RoleLogic();

			ExceptionAssertionUtility.AssertExceptionThrown("The specified role ID does not exist.", typeof(ServiceException), () => roleLogic.UpdateRole(NEW_ROLE, new Role { Name = NEW_ROLE, Rights = new List<string> { CREATE_RIGHT } }));
		}

		[TestMethod]
		public void UpdateRole_ValidRole_Successful()
		{
			SetupAll();

			RoleLogic roleLogic = new RoleLogic();

			Role role = roleLogic.UpdateRole(ADMIN_ROLE, new Role { Name = NEW_ROLE, Rights = new List<string> { CREATE_RIGHT } });

			Assert.IsTrue(role.Name == NEW_ROLE);
		}

		[TestMethod]
		public void DeleteRole_RoleIdNotSpecified_ThrowsServiceException()
		{
			SetupAll();

			RoleLogic roleLogic = new RoleLogic();

			ExceptionAssertionUtility.AssertExceptionThrown("No role ID was specified", typeof(ServiceException), () => roleLogic.DeleteRole(string.Empty));
		}

		[TestMethod]
		public void DeleteRole_Successful()
		{
			SetupAll();

			RoleLogic roleLogic = new RoleLogic();

			Role role = roleLogic.DeleteRole(ADMIN_ROLE);

			Assert.IsTrue(role.Name == ADMIN_ROLE);
		}

		#endregion

		#region Private Methods

		private void SetupAll()
		{
			SetupRights();
			SetupRoles();
			SetupFakeLayer();
		}

		private void SetupFakeLayer()
		{
			_fakeLayer = FakeLayer.Setup();
			Mock<IRepository> repository = new Mock<IRepository>();

			//_fakeLayer.DataContext.Setup(_roles);
			_fakeLayer.DataLayer.Setup(repository);
			_fakeLayer.DataLayer.GetFakeSet<Role>().Add(_roles[0]);
			_fakeLayer.DataLayer.GetFakeSet<Role>().Add(_roles[1]);
			_fakeLayer.DataLayer.GetFakeSet<Role>().Add(_roles[2]);
			_fakeLayer.DataLayer.GetFakeSet<Role>().Add(_roles[3]);
			_fakeLayer.DataLayer.GetFakeSet<Role>().Add(_roles[4]);
		}

		private void SetupRoles()
		{
			_roles = new List<Role>
			{
			    new Role { Id = Guid.Empty, Description = ADMIN_ROLE, Name = ADMIN_ROLE, Rights = new List<string> {CREATE_RIGHT} },
			    new Role { Id = Guid.Empty, Description = CONTENT_MANAGER_ROLE, Name = CONTENT_MANAGER_ROLE, Rights = new List<string> {CREATE_RIGHT} },
			    new Role { Id = Guid.Empty, Description = EDITOR_ROLE, Name = EDITOR_ROLE, Rights = new List<string> {CREATE_RIGHT} },
			    new Role { Id = Guid.Empty, Description = INDEXER_ROLE, Name = INDEXER_ROLE, Rights = new List<string> {CREATE_RIGHT} },
			    new Role { Id = Guid.Empty, Description = BROWSER_ROLE, Name = BROWSER_ROLE, Rights = new List<string> {CREATE_RIGHT} }
			};
		}

		private void SetupRights()
		{
			RightManager.Clear();

			_rights = new List<string> { CREATE_RIGHT };

			foreach (string right in _rights)
				RightManager.AddRight(right);
		}

		#endregion
	}
}
