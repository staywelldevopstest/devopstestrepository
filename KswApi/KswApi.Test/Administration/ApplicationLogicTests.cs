﻿using KswApi.Common;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects;
using KswApi.Logic.Administration;
using KswApi.Logic.Framework.Constants;
using KswApi.Test.Framework.Fakes;
using KswApi.Test.Framework.TestUtilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

// ReSharper disable InconsistentNaming

namespace KswApi.Test.Administration
{
	[TestClass]
	public class ApplicationLogicTests
	{
		private const string APP_NAME = "Test App";

		[TestMethod]
		public void GetApplication_NoApplicationId_ThrowsException()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => appLogic.GetApplication(string.Empty));
		}

		[TestMethod]
		public void GetApplication_InvalidGuid_ThrowsException()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => appLogic.GetApplication("asdklfja;sldkfj"));
		}

		[TestMethod]
		public void GetApplication_AppNotFound_ThrowsException()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			FakeLayer.Setup();

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => appLogic.GetApplication(Guid.NewGuid().ToString()));
		}

		[TestMethod]
		public void GetApplication_Successful()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid appId = Guid.NewGuid();

			Application app = new Application { Id = appId };

			fakeLayer.DataLayer.Setup(app);

			appLogic.GetApplication(appId.ToString());
		}

		[TestMethod]
		public void GetApplications_CountGreaterThanMax_ThrowsException()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => appLogic.GetApplications(0, Constant.MAXIMUM_PAGE_SIZE + 1, string.Empty, string.Empty));
		}

		[TestMethod]
		public void GetApplications_OffsetLessThanZero_ThrowsException()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => appLogic.GetApplications(-1, 0, string.Empty, string.Empty));
		}

		[TestMethod]
		public void GetApplication_NullOrderBy_Successful()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.AddApplications(10);

			appLogic.GetApplications(0, 10, string.Empty, string.Empty);
		}

		[TestMethod]
		public void GetApplication_OrderByNoFilter_Successful()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.AddApplications(10);

			appLogic.GetApplications(0, 10, string.Empty, "Name ASC");
		}

		[TestMethod]
		public void GetApplication_OrderByWithFilter_Successful()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			FakeLayer fakeLayer = FakeLayer.Setup();

			fakeLayer.DataLayer.AddApplications(10);

			appLogic.GetApplications(0, 10, "Name eq ''", "Name ASC");
		}

		[TestMethod]
		public void CreateApplication_NullApplication_ThrowsException()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => appLogic.CreateApplication(null, null));
		}

		[TestMethod]
		public void CreateApplication_NoAppName_ThrowsException()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			Application app = new Application();

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => appLogic.CreateApplication(null, app));
		}

		[TestMethod]
		public void CreateApplication_NameTooLong_ThrowsException()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			Application app = new Application();
			app.Name = "asdfkjasdlkfja;sldkjfa;slkdfja;sldkfja;sldkfja;sldkfja;sldkfja;sdlkfja;sdlkfja;sdlkfja;sdlkfja;sldkfja;sdlkfja;sdlkfja;sldkf";

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => appLogic.CreateApplication(null, app));
		}

		[TestMethod]
		public void CreateApplication_Successful()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			FakeLayer.Setup();

			Application app = new Application();
			app.Name = APP_NAME;

			appLogic.CreateApplication(null, app);
		}

		[TestMethod]
		public void UpdateApplication_NullApplication_ThrowsException()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => appLogic.UpdateApplication(null, string.Empty, null));
		}

		[TestMethod]
		public void UpdateApplication_InvalidAppId_ThrowsException()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			Application app = new Application();

			app.Name = APP_NAME;
			app.Secret = "app secret specified";

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => appLogic.UpdateApplication(null, string.Empty, app));
		}

		[TestMethod]
		public void UpdateApplication_DifferentAppId_ThrowsExcpetion()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			Application app = new Application();
			app.Id = Guid.NewGuid();
			app.Name = APP_NAME;
			app.Secret = "app secret specified";

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => appLogic.UpdateApplication(null, Guid.NewGuid().ToString(), app));
		}

		[TestMethod]
		public void UpdateApplication_NullAppName_ThrowsException()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			Guid appId = Guid.NewGuid();

			Application app = new Application();
			app.Id = appId;
			app.Name = string.Empty;
			app.Secret = "app secret specified";

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => appLogic.UpdateApplication(null, appId.ToString(), app));
		}

		[TestMethod]
		public void UpdateApplication_AppNameTooLong_ThrowsException()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			Guid appId = Guid.NewGuid();

			Application app = new Application();
			app.Id = appId;
			app.Name = "askjdhflkasjdhfskdjf;laksdjf;lkasdjf;laksdjf;laksdjf;alskdjf;aslkdfj;aslkdfja;slkdfja;slkdfj;alskdfja;sldkfja;slkdj";
			app.Secret = "app secret specified";

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => appLogic.UpdateApplication(null, appId.ToString(), app));
		}

		[TestMethod]
		public void UpdateApplication_AppNotFound_ThrowsException()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			FakeLayer.Setup();

			Guid appId = Guid.NewGuid();

			Application app = new Application();
			app.Id = appId;
			app.Name = APP_NAME;

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => appLogic.UpdateApplication(null, appId.ToString(), app));
		}

		[TestMethod]
		public void UpdateApplication_Successful()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid appId = Guid.NewGuid();

			Application app = new Application();
			app.Id = appId;
			app.Name = APP_NAME;
			app.Secret = "app secret specified";

			fakeLayer.DataLayer.Setup(app);

			app.Name = APP_NAME + APP_NAME;

			appLogic.UpdateApplication(null, appId.ToString(), app);
		}

		[TestMethod]
		public void ResetApplicationSecret_Successful()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid appId = Guid.NewGuid();
			string appSecret = TokenGenerator.CreateSecret();

			Application app = new Application();
			app.Id = appId;
			app.Name = APP_NAME;
			app.Secret = appSecret;
			fakeLayer.DataLayer.Setup(app);

			ApplicationLogic applicationLogic = new ApplicationLogic();
			Application result = applicationLogic.ResetApplicationSecret(null, appId.ToString());

			Assert.IsNotNull(result.Secret);
			Assert.AreNotEqual(appSecret, result.Secret);
		}

		[TestMethod]
		public void ResetApplicationSecret_Fails_With_Not_Found_Application()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid appId = Guid.NewGuid();
			string appSecret = TokenGenerator.CreateSecret();

			Application app = new Application();
			app.Id = appId;
			app.Name = APP_NAME;
			app.Secret = appSecret;
			fakeLayer.DataLayer.Setup(app);

			ApplicationLogic applicationLogic = new ApplicationLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => applicationLogic.ResetApplicationSecret(null, appId.ToString() + "x"));
		}

		[TestMethod]
		public void DeleteApplication_InvalidAppId_ThrowsException()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => appLogic.DeleteApplication(string.Empty));
		}

		[TestMethod]
		public void DeleteApplication_AppNotFound_ThrowsException()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			FakeLayer.Setup();

			ExceptionAssertionUtility.AssertExceptionThrown(typeof(ServiceException), () => appLogic.DeleteApplication(Guid.NewGuid().ToString()));
		}

		[TestMethod]
		public void DeleteApplication_Successful()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid appId = Guid.NewGuid();

			Application app = new Application();
			app.Id = appId;
			app.Name = APP_NAME;
			app.Secret = "app secret specified";

			fakeLayer.DataLayer.Setup(app);

			appLogic.DeleteApplication(appId.ToString());
		}

		[TestMethod]
		public void EnsurePortalApplication_AppIsCurrent()
		{
			ApplicationLogic appLogic = new ApplicationLogic();

			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid appId = new Guid(Constant.WEB_PORTAL_APPLICATION_ID);

			Application app = new Application();
			app.Id = appId;
			app.Name = APP_NAME;
			app.Secret = "app secret specified";

			fakeLayer.DataLayer.Setup(app);

			appLogic.EnsurePortalApplication();
		}

		[TestMethod]
		public void EnsurePortalApplication_Successful()
		{ }
	}
}
