﻿using System;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Logic.Administration;
using KswApi.Poco.Enums;
using KswApi.Repositories.Objects;
using KswApi.Test.Framework.Fakes;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable InconsistentNaming

namespace KswApi.Test.Administration
{
	[TestClass]
	public class PasswordLogicTests
	{
		[TestMethod]
		public void ValidatePasswordToken_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid userId = Guid.NewGuid();

			fakeLayer.DataLayer.Setup(new PasswordToken
			                          {
				                          DateAdded = DateTime.UtcNow,
										  Token = "the_token",
										  UserId = userId,
										  UserType = UserType.ClientUser
			                          });

			fakeLayer.DataLayer.Setup(new ClientUser
			                          {
				                          Id = userId,
										  FirstName = "first name",
										  State = ClientUserState.Active
			                          });

			PasswordLogic logic = new PasswordLogic();

			logic.ValidatePasswordToken("the_token");
		}

		[TestMethod]
		public void SetClientUserPassword_Succeeds()
		{
			FakeLayer fakeLayer = FakeLayer.Setup();

			Guid userId = Guid.NewGuid();

			fakeLayer.DataLayer.Setup(new PasswordToken
			{
				DateAdded = DateTime.UtcNow,
				Token = "the_token",
				UserId = userId,
				UserType = UserType.ClientUser
			});

			fakeLayer.DataLayer.Setup(new ClientUser
			{
				Id = userId,
				FirstName = "first name",
				State = ClientUserState.Active,
				EmailAddress = "user@user.com"
			});

			PasswordLogic logic = new PasswordLogic();

			logic.SetClientUserPassword(new PasswordRequest
			                            {
				                            Password = "P@ssword1",
											PasswordConfirmation = "P@ssword1",
											Token = "the_token"
			                            });


		}
	}
}
