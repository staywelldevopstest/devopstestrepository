﻿using System;
using System.Collections.Generic;
using KswApi.Collections;
using Microsoft.VisualStudio.TestTools.UnitTesting;

// ReSharper disable InconsistentNaming
namespace KswApi.Test
{
	[TestClass]
	public class CollectionTests
	{
		[TestMethod]
		public void PriorityQueue_OrdersElements()
		{
			List<int> values = new List<int> { 5, 2, 3, 7, 9, 0, 1, 1, 7, 4 };

			PriorityQueue<int> queue = new PriorityQueue<int>();

			foreach (int value in values)
				queue.Push(value);

			int previous = queue.Peek();

			int count = values.Count;
			
			for (int i = 0; i < count; i++)
			{
				Assert.AreEqual(count - i, queue.Count);
				int current = queue.Pop();
				Assert.IsTrue(previous <= current);
				values.Remove(current);
				previous = current;
			}

			Assert.AreEqual(0, queue.Count);
		}

		[TestMethod]
		public void PriorityQueue_RemoveRandomNumberTest()
		{
			Random random = new Random();

			int count = random.Next(5, 100);

			List<int> values = new List<int>(count);

			for (int i = 0; i < count; i++)
			{
				values.Add(random.Next());
			}

			PriorityQueue<int> queue = new PriorityQueue<int>();

			foreach (int value in values)
				queue.Push(value);

			for (int i = 0; i < count; i++)
			{
				int current = random.Next(0, count - i - 1);
				Assert.AreEqual(count - i, queue.Count);
				Assert.IsTrue(queue.Remove(values[current]));
				values.RemoveAt(current);
			}

			Assert.AreEqual(0, queue.Count);
		}

	}
}
