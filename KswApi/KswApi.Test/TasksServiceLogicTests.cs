﻿//using KswApi.Common.Configuration;
//using KswApi.Common.Objects;
//using KswApi.Interface.Enums;
//using KswApi.Interface.Objects;
//using KswApi.Ioc;
//using KswApi.Logic.Framework;
//using KswApi.Logic.Framework.Constants;
//using KswApi.Logic.Tasks;
//using KswApi.Repositories.Objects;
//using KswApi.Test.Framework.Fakes;
//using Microsoft.VisualStudio.TestTools.UnitTesting;

//// ReSharper disable InconsistentNaming

//namespace KswApi.Test
//{
//	[TestClass]
//	public class TasksServiceLogicTests
//	{
//		[TestMethod]
//		public void GetServiceSettings_Successful()
//		{
//			TasksServiceLogic logic = new TasksServiceLogic();

//			Settings settings = new Settings();

//			settings.AccessTokenCleanupDaysToRun = "6";
//			settings.AccessTokenCleanupDuration = 0;
//			settings.AccessTokenCleanupRecurringPattern = 0;
//			settings.AccessTokenCleanupStartTime = "2:00 AM";
//			settings.AccessTokenCleanupTaskActive = true;
//			settings.AccessTokenCleanupTaskType = 2;

//			settings.SmsSessionCleanupDaysToRun = "6";
//			settings.SmsSessionCleanupDuration = 0;
//			settings.SmsSessionCleanupRecurringPattern = 0;
//			settings.SmsSessionCleanupStartTime = "2:00 AM";
//			settings.SmsSessionCleanupTaskActive = true;
//			settings.SmsSessionCleanupTaskType = 3;

//			settings.AuthGrantCleanupDaysToRun = "6";
//			settings.AuthGrantCleanupDuration = 0;
//			settings.AuthGrantCleanupRecurringPattern = 0;
//			settings.AuthGrantCleanupStartTime = "2:00 AM";
//			settings.AuthGrantCleanupTaskActive = true;
//			settings.AuthGrantCleanupTaskType = 4;

//			Dependency.Set(settings);

//			TaskProvider.InitializeTasks();

//			TaskServiceSettings result = logic.GetServiceSettings();

//			Assert.IsTrue(result != null);
//			Assert.IsTrue(result.NumberOfThreads == Constant.MAX_NUM_TASK_THREADS);
//			Assert.IsTrue(result.HeartBeatInMinutes == Constant.HEART_BEAT_IN_MINUTES);
//			Assert.IsTrue(result.Tasks.Count > 0);
//		}

//		[TestMethod]
//		public void ServiceCheck_Successful()
//		{
//			TasksServiceLogic logic = new TasksServiceLogic();

//			Assert.IsTrue(logic.ServiceCheck().IsServiceUp);
//		}

//		[TestMethod]
//		public void GetTaskSettings_Successful()
//		{
//			Settings settings = new Settings();

//			settings.AccessTokenCleanupDaysToRun = "6";
//			settings.AccessTokenCleanupDuration = 0;
//			settings.AccessTokenCleanupRecurringPattern = 0;
//			settings.AccessTokenCleanupStartTime = "2:00 AM";
//			settings.AccessTokenCleanupTaskActive = true;
//			settings.AccessTokenCleanupTaskType = 4;

//			settings.SmsSessionCleanupDaysToRun = "6";
//			settings.SmsSessionCleanupDuration = 0;
//			settings.SmsSessionCleanupRecurringPattern = 0;
//			settings.SmsSessionCleanupStartTime = "2:00 AM";
//			settings.SmsSessionCleanupTaskActive = true;
//			settings.SmsSessionCleanupTaskType = 2;

//			settings.AuthGrantCleanupDaysToRun = "6";
//			settings.AuthGrantCleanupDuration = 0;
//			settings.AuthGrantCleanupRecurringPattern = 0;
//			settings.AuthGrantCleanupStartTime = "2:00 AM";
//			settings.AuthGrantCleanupTaskActive = true;
//			settings.AuthGrantCleanupTaskType = 3;

//			Dependency.Set(settings);

//			TaskProvider.InitializeTasks();

//			TasksServiceLogic logic = new TasksServiceLogic();

//			Task task = logic.GetTaskSettings(TaskType.SmsSessionCleanup);

//			Assert.IsTrue(task.Active);
//			Assert.IsTrue(task.TaskName == Constant.SMS_SESSION_TASK_NAME);

//			task = logic.GetTaskSettings(TaskType.AccessTokenCleanup);

//			Assert.IsTrue(task.Active);
//			Assert.IsTrue(task.TaskName == Constant.ACCESS_TOKEN_TASK_NAME);

//			task = logic.GetTaskSettings(TaskType.AuthorizationGrantCleanup);

//			Assert.IsTrue(task.Active);
//			Assert.IsTrue(task.TaskName == Constant.AUTH_GRANT_TASK_NAME);
//		}

//		[TestMethod]
//		public void DeleteSmsSession_NoContinue_Successful()
//		{
//			TasksServiceLogic logic = new TasksServiceLogic();

//			FakeLayer fakeLayer = FakeLayer.Setup();

//			fakeLayer.DataLayer.AddSmsSessions(true, 25);
//			fakeLayer.DataLayer.AddSmsSessions(false, 25);

//			TaskStepResponse response = logic.DoTaskStep(TaskType.SmsSessionCleanup);

//			Assert.IsNotNull(response);
//			Assert.IsFalse(response.Continue);
//			Assert.AreEqual(25, fakeLayer.DataLayer.GetFakeSet<SmsSession>().Count);
//		}

//		[TestMethod]
//		public void DeleteSmsSession_Continue_Successful()
//		{
//			TasksServiceLogic logic = new TasksServiceLogic();

//			FakeLayer fakeLayer = FakeLayer.Setup();

//			fakeLayer.DataLayer.AddSmsSessions(true, 105);
//			fakeLayer.DataLayer.AddSmsSessions(false, 25);

//			TaskStepResponse response = logic.DoTaskStep(TaskType.SmsSessionCleanup);

//			Assert.IsNotNull(response);
//			Assert.IsTrue(response.Continue);
//			Assert.AreEqual(80, fakeLayer.DataLayer.GetFakeSet<SmsSession>().Count);
//		}

//		[TestMethod]
//		public void DeleteAccessToken_NoContinue_Successful()
//		{
//			TasksServiceLogic logic = new TasksServiceLogic();

//			FakeLayer fakeLayer = FakeLayer.Setup();

//			fakeLayer.DataLayer.AddAccessTokens(true, 25);
//			fakeLayer.DataLayer.AddAccessTokens(false, 25);

//			TaskStepResponse response = logic.DoTaskStep(TaskType.AccessTokenCleanup);

//			Assert.IsNotNull(response);
//			Assert.IsFalse(response.Continue);
//			Assert.AreEqual(25, fakeLayer.DataLayer.GetFakeSet<AccessToken>().Count);
//		}

//		[TestMethod]
//		public void DeleteAccessToken_Continue_Successful()
//		{
//			TasksServiceLogic logic = new TasksServiceLogic();

//			FakeLayer fakeLayer = FakeLayer.Setup();

//			fakeLayer.DataLayer.AddAccessTokens(true, 105);
//			fakeLayer.DataLayer.AddAccessTokens(false, 25);

//			TaskStepResponse response = logic.DoTaskStep(TaskType.AccessTokenCleanup);

//			Assert.IsNotNull(response);
//			Assert.IsTrue(response.Continue);
//			Assert.AreEqual(80, fakeLayer.DataLayer.GetFakeSet<AccessToken>().Count);
//		}

//		[TestMethod]
//		public void DeleteAuthorizationGrant_NoContinue_Successful()
//		{
//			TasksServiceLogic logic = new TasksServiceLogic();

//			FakeLayer fakeLayer = FakeLayer.Setup();

//			fakeLayer.DataLayer.AddAuthorizationGrants(true, 25);
//			fakeLayer.DataLayer.AddAuthorizationGrants(false, 25);

//			TaskStepResponse response = logic.DoTaskStep(TaskType.AuthorizationGrantCleanup);

//			Assert.IsNotNull(response);
//			Assert.IsFalse(response.Continue);
//			Assert.AreEqual(25, fakeLayer.DataLayer.GetFakeSet<AuthorizationGrant>().Count);
//		}

//		[TestMethod]
//		public void DeleteAuthorizationGrant_Continue_Successful()
//		{
//			TasksServiceLogic logic = new TasksServiceLogic();

//			FakeLayer fakeLayer = FakeLayer.Setup();

//			fakeLayer.DataLayer.AddAuthorizationGrants(true, 105);
//			fakeLayer.DataLayer.AddAuthorizationGrants(false, 25);

//			TaskStepResponse response = logic.DoTaskStep(TaskType.AuthorizationGrantCleanup);

//			Assert.IsNotNull(response);
//			Assert.IsTrue(response.Continue);
//			Assert.AreEqual(80, fakeLayer.DataLayer.GetFakeSet<AuthorizationGrant>().Count);
//		}

//	}
//}
