﻿using System.Collections.Generic;
using KswApi.Interface.Enums;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects.Rendering;
using KswApi.Logic.Rendering;
using KswApi.Test.Framework.TestUtilities;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KswApi.Test
{
	[TestClass]
	public class TextTemplateTests
	{
		[TestMethod]
		public void Resolve_OneValue_IsSuccessful()
		{
			IList<TemplateField> values = new List<TemplateField>
			{
				new TemplateField("FirstName", "John")
			};
			Assert.AreEqual("Hello John.",
				new TextTemplate().Resolve("Hello {{FirstName}}.", values));
		}

		[TestMethod]
		public void Resolve_TwoValues_IsSuccessful()
		{
			IList<TemplateField> values = new List<TemplateField>
			{
				new TemplateField("FirstName", "John"),
				new TemplateField("LastName", "Smith")
			};
			Assert.AreEqual("Hello John Smith.", new TextTemplate().Resolve("Hello {{FirstName}} {{LastName}}.", values));
		}

		[TestMethod]
		public void Resolve_ValueForTemplatableFieldNotProvided_ExceptionThrown()
		{
			IList<TemplateField> values = new List<TemplateField>
			{
				new TemplateField("FirstName", "John")
			};

			ExceptionAssertionUtility.AssertExceptionThrown(
				"Value not provided for field(s): LastName.",
				typeof(ServiceException),
				() => {
					new TextTemplate().Resolve("Hello {{FirstName}} {{LastName}}.", values);
					Assert.Fail("Exception expected.");
				});
		}

		[TestMethod]
		public void Resolve_TwoValuesNotProvided_ExceptionMessageContainsBothFieldNames()
		{
			IList<TemplateField> values = new List<TemplateField>();

			ExceptionAssertionUtility.AssertExceptionThrown(
				"Value not provided for field(s): FirstName, LastName.",
				typeof(ServiceException),
				() =>
				{
					new TextTemplate().Resolve("Hello {{FirstName}} {{LastName}}.", values);
					Assert.Fail("Exception expected.");
				});
		}

		[TestMethod]
		public void Resolve_DecisionTemplateCasingDifference_IsSuccessful()
		{
			IList<TemplateField> values = new List<TemplateField>
			{
				new TemplateField("FirstName", "John"),
				new TemplateField("LastName", "Smith"),
				new TemplateField("Age", 30),
				new TemplateField("Gender", "M"),
				new TemplateField("EducationLevel", "SomeCollege")
			};
			Assert.AreEqual("Hello John Smith. I see that you are male.",
				new TextTemplate().Resolve(
					"Hello {{FirstName}} {{LastName}}. I see that you are {{IF Gender == \"M\"}}male{{ELSE}}female{{/IF}}.", values));
		}

		[TestMethod]
		public void Resolve_DecisionTemplateWithGender_IsSuccessful()
		{
			IList<TemplateField> values = new List<TemplateField>
			{
				new TemplateField("FirstName", "John"),
				new TemplateField("LastName", "Smith"),
				new TemplateField("Age", 30),
				new TemplateField("Gender", "M"),
				new TemplateField("EducationLevel", "SomeCollege")
			};
			Assert.AreEqual("Hello John Smith. I see that you are male.",
				new TextTemplate().Resolve(
					"Hello {{FirstName}} {{LastName}}. I see that you are {{if Gender == \"M\"}}male{{else}}female{{/if}}.", values));
		}

		[TestMethod]
		public void Resolve_DecisionTemplateNoElse_IsSuccessful()
		{
			IList<TemplateField> values = new List<TemplateField>
			{
				new TemplateField("FirstName", "John"),
				new TemplateField("LastName", "Smith"),
				new TemplateField("Age", 30),
				new TemplateField("Gender", "M"),
				new TemplateField("EducationLevel", "SomeCollege")
			};
			Assert.AreEqual("Hello John Smith. I see that you have attended some college.",
				new TextTemplate().Resolve(
					"Hello {{FirstName}} {{LastName}}. I see that you have attended {{if EducationLevel == \"SomeCollege\"}}some college{{/if}}.",
					values));
		}

		[TestMethod]
		public void Resolve_DecisionTemplateNoElseNoMatch_InsertsNothing()
		{
			IList<TemplateField> values = new List<TemplateField>
			{
				new TemplateField("FirstName", "John"),
				new TemplateField("LastName", "Smith"),
				new TemplateField("Age", 30),
				new TemplateField("Gender", "M"),
				new TemplateField("EducationLevel", "HighSchool")
			};
			Assert.AreEqual("Hello John Smith. I see that you have attended .",
				new TextTemplate().Resolve(
					"Hello {{FirstName}} {{LastName}}. I see that you have attended {{if EducationLevel == \"SomeCollege\"}}some college{{/if}}.",
					values));
		}

		[TestMethod]
		public void Resolve_DecisionTemplateWithTwoFormulas_IsSuccessful()
		{
			IList<TemplateField> values = new List<TemplateField>
			{
				new TemplateField("FirstName", "John"),
				new TemplateField("LastName", "Smith"),
				new TemplateField("Age", 30),
				new TemplateField("Gender", "M"),
				new TemplateField("EducationLevel", "SomeCollege")
			};
			Assert.AreEqual("Hello John Smith. I see that you are a 30 year old male and have attended some college.",
				new TextTemplate().Resolve(
					"Hello {{FirstName}} {{LastName}}. I see that you are a {{Age}} year old {{if Gender == \"M\"}}male{{else}}female{{/if}} and have attended {{if EducationLevel == \"SomeCollege\"}}some college{{/if}}.",
					values));
		}

		[TestMethod]
		public void Resolve_DecisionTemplateBooleanValue_IsSuccessful()
		{
			IList<TemplateField> values = new List<TemplateField>
			{
				new TemplateField("FirstName", "John"),
				new TemplateField("LastName", "Smith"),
				new TemplateField("Age", 30),
				new TemplateField("Gender", "M"),
				new TemplateField("PresenceOfChildren", true)
			};
			Assert.AreEqual("Hello John Smith. You have a family.",
				new TextTemplate().Resolve(
					"Hello {{FirstName}} {{LastName}}. You {{if PresenceOfChildren}}have{{else}}do not have{{/if}} a family.",
					values));
		}

		[TestMethod]
		public void Resolve_DecisionTemplateIntegerValue_IsSuccessful()
		{
			IList<TemplateField> values = new List<TemplateField>
			{
				new TemplateField("FirstName", "John"),
				new TemplateField("LastName", "Smith"),
				new TemplateField("Age", 30),
				new TemplateField("Gender", "M"),
				new TemplateField("EducationLevel", "SomeCollege")
			};
			Assert.AreEqual("Hello John Smith. Your age group is 18-30.",
				new TextTemplate().Resolve(
					"Hello {{FirstName}} {{LastName}}. Your age group is {{if Age >= 18 && Age <= 30}}18-30{{/if}}.",
					values));
		}

		[TestMethod]
		public void Resolve_DecisionTemplateImageExample_IsSuccessful()
		{
			IList<TemplateField> values = new List<TemplateField>
			{
				new TemplateField("FirstName", "John"),
				new TemplateField("LastName", "Smith"),
				new TemplateField("Age", 30),
				new TemplateField("Gender", "M"),
				new TemplateField("EducationLevel", "SomeCollege")
			};
			Assert.AreEqual("Hello John Smith. Your personalized image is 'Male18-30.jpg'.",
				new TextTemplate().Resolve(
					"Hello {{FirstName}} {{LastName}}. Your personalized image is '{{if Gender == \"M\"}}Male{{else}}Female{{/if}}{{if Age >= 18 && Age <= 30}}18-30{{/if}}.jpg'.",
					values));
		}

		[TestMethod]
		public void Resolve_DecisionTemplateGenderEnumeration_IsSuccessful()
		{
			IList<TemplateField> values = new List<TemplateField>
			{
				new TemplateField("Gender", Gender.Male)
			};
			Assert.AreEqual("I see that you are male.",
				new TextTemplate().Resolve(
					"I see that you are {{if Gender == \"Male\"}}male{{else}}female{{/if}}.",
					values));
		}

		[TestMethod]
		public void Resolve_DecisionTemplateComplexImageExample_IsSuccessful()
		{
			const string TEMPLATE = "{{if Gender == \"M\"}}Male{{else}}Female{{/if}}{{if Age >= 18 && Age <= 30}}18-30{{/if}}{{if Age >= 31 && Age <= 50}}31-50{{/if}}{{if Age >= 51}}51p{{/if}}";

			Assert.AreEqual("Male18-30", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("Age", 30), new TemplateField("Gender", "M") }));
			Assert.AreEqual("Male31-50", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("Age", 31), new TemplateField("Gender", "M") }));
			Assert.AreEqual("Male51p", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("Age", 51), new TemplateField("Gender", "M") }));
			Assert.AreEqual("Female18-30", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("Age", 30), new TemplateField("Gender", "F") }));
			Assert.AreEqual("Female31-50", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("Age", 31), new TemplateField("Gender", "F") }));
			Assert.AreEqual("Female51p", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("Age", 51), new TemplateField("Gender", "F") }));
		}

		[TestMethod]
		public void Resolve_EmbeddedDecisionTemplates_IsSuccessful()
		{
			const string TEMPLATE = "{{if PresenceOfChildren}}Family{{else}}{{if Gender == \"M\"}}Male{{else}}Female{{/if}}{{if Age >= 18 && Age <= 30}}18-30{{/if}}{{if Age >= 31 && Age <= 50}}31-50{{/if}}{{if Age >= 51}}51p{{/if}}{{/if}}";

			Assert.AreEqual("Male18-30", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", false), new TemplateField("Age", 30), new TemplateField("Gender", "M") }));
			Assert.AreEqual("Family", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", true), new TemplateField("Age", 31), new TemplateField("Gender", "M") }));
			Assert.AreEqual("Male51p", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", false), new TemplateField("Age", 51), new TemplateField("Gender", "M") }));
			Assert.AreEqual("Female18-30", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", false), new TemplateField("Age", 30), new TemplateField("Gender", "F") }));
			Assert.AreEqual("Female31-50", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", false), new TemplateField("Age", 31), new TemplateField("Gender", "F") }));
			Assert.AreEqual("Family", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", true), new TemplateField("Age", 51), new TemplateField("Gender", "F") }));
		}

		[TestMethod]
		public void Resolve_DecisionTemplatesWithNewLines_IsSuccessful()
		{
			const string TEMPLATE =
@"{{if PresenceOfChildren}}
	Family
{{else}}
	{{if Gender == ""M""}}
		Male
	{{else}}
		Female
	{{/if}}
	{{if Age >= 18 && Age <= 30}}
		18-30
	{{/if}}
	{{if Age >= 31 && Age <= 50}}
		31-50
	{{/if}}
	{{if Age >= 51}}
		51p
	{{/if}}
{{/if}}";

			Assert.AreEqual("Male18-30", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", false), new TemplateField("Age", 30), new TemplateField("Gender", "M") }));
			Assert.AreEqual("Family", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", true), new TemplateField("Age", 31), new TemplateField("Gender", "M") }));
			Assert.AreEqual("Male51p", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", false), new TemplateField("Age", 51), new TemplateField("Gender", "M") }));
			Assert.AreEqual("Female18-30", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", false), new TemplateField("Age", 30), new TemplateField("Gender", "F") }));
			Assert.AreEqual("Female31-50", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", false), new TemplateField("Age", 31), new TemplateField("Gender", "F") }));
			Assert.AreEqual("Family", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", true), new TemplateField("Age", 51), new TemplateField("Gender", "F") }));
		}

		[TestMethod]
		public void Resolve_AlternateImageDecisionTemplate_IsSuccessful()
		{
			const string TEMPLATE =
@"{{if PresenceOfChildren}}
	Family
{{else}}
	{{if Gender == ""M""}}
		{{if Age >= 18 && Age <= 30}}
			Male18-30
		{{/if}}
		{{if Age >= 31 && Age <= 50}}
			Male31-50
		{{/if}}
		{{if Age >= 51}}
			Male51p
		{{/if}}
	{{else}}
		{{if Age >= 18 && Age <= 30}}
			Female18-30
		{{/if}}
		{{if Age >= 31 && Age <= 50}}
			Female31-50
		{{/if}}
		{{if Age >= 51}}
			Female51p
		{{/if}}
	{{/if}}
{{/if}}";

			Assert.AreEqual("Male18-30", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", false), new TemplateField("Age", 30), new TemplateField("Gender", "M") }));
			Assert.AreEqual("Family", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", true), new TemplateField("Age", 31), new TemplateField("Gender", "M") }));
			Assert.AreEqual("Male51p", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", false), new TemplateField("Age", 51), new TemplateField("Gender", "M") }));
			Assert.AreEqual("Female18-30", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", false), new TemplateField("Age", 30), new TemplateField("Gender", "F") }));
			Assert.AreEqual("Female31-50", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", false), new TemplateField("Age", 31), new TemplateField("Gender", "F") }));
			Assert.AreEqual("Family", new TextTemplate().Resolve(TEMPLATE, new List<TemplateField> { new TemplateField("PresenceOfChildren", true), new TemplateField("Age", 51), new TemplateField("Gender", "F") }));
		}

		[TestMethod]
		public void Resolve_DocumentFromKswApi_IsSuccessful()
		{
			const string TEMPLATE =
@"<p>Hello&nbsp;{{FirstName}}&nbsp;{{LastName}}.&nbsp;I&nbsp;see&nbsp;that&nbsp;you&nbsp;are&nbsp;a&nbsp;{{Age}}&nbsp;year&nbsp;old&nbsp;{{if&nbsp;Gender&nbsp;==&nbsp;""M""}}male{{else}}female{{/if}}&nbsp;and&nbsp;have&nbsp;attended&nbsp;{{if&nbsp;EducationLevel&nbsp;==&nbsp;""SomeCollege""}}some&nbsp;college{{/if}}.</p>
<pre>{{if&nbsp;PresenceOfChildren}}Family{{else}}{{if&nbsp;Gender&nbsp;==&nbsp;""M""}}Male{{else}}Female{{/if}}{{if&nbsp;Age&nbsp;&gt;=&nbsp;18&nbsp;&amp;&amp;&nbsp;Age&nbsp;&lt;=&nbsp;30}}18-30{{/if}}{{if&nbsp;Age&nbsp;&gt;=&nbsp;31&nbsp;&amp;&amp;&nbsp;Age&nbsp;&lt;=&nbsp;50}}31-50{{/if}}{{if&nbsp;Age&nbsp;&gt;=&nbsp;51}}51p{{/if}}{{/if}}</pre>
<pre>&lt;img ImageId=""{{if&nbsp;PresenceOfChildren}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Family{{else}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{if&nbsp;Gender&nbsp;==&nbsp;""M""}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Male&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{else}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Female&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{/if}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{if&nbsp;Age&nbsp;&gt;=&nbsp;18&nbsp;&amp;&amp;&nbsp;Age&nbsp;&lt;=&nbsp;30}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 18-30&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{/if}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{if&nbsp;Age&nbsp;&gt;=&nbsp;31&nbsp;&amp;&amp;&nbsp;Age&nbsp;&lt;=&nbsp;50}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 31-50&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{/if}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{if&nbsp;Age&nbsp;&gt;=&nbsp;51}}&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 51p&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; {{/if}}{{/if}}"" /&gt;</pre>
<pre><br /><br />{{if&nbsp;PresenceOfChildren}}
	Family
{{else}}
	{{if&nbsp;Gender&nbsp;==&nbsp;""M""}}
		Male
	{{else}}
		Female
	{{/if}}
	{{if&nbsp;Age&nbsp;&gt;=&nbsp;18&nbsp;&amp;&amp;&nbsp;Age&nbsp;&lt;=&nbsp;30}}
		18-30
	{{/if}}
	{{if&nbsp;Age&nbsp;&gt;=&nbsp;31&nbsp;&amp;&amp;&nbsp;Age&nbsp;&lt;=&nbsp;50}}
		31-50
	{{/if}}
	{{if&nbsp;Age&nbsp;&gt;=&nbsp;51}}
		51p
	{{/if}}
{{/if}}</pre>";

			List<TemplateField> fields = new List<TemplateField>
			{
				new TemplateField("FirstName", "John"),
				new TemplateField("LastName", "Smith"),
				new TemplateField("EducationLevel", "SomeCollege"),
				new TemplateField("PresenceOfChildren", false),
				new TemplateField("Age", 30),
				new TemplateField("Gender", "M")
			};

			const string EXPECTED = @"<p>Hello&nbsp;John&nbsp;Smith.&nbsp;I&nbsp;see&nbsp;that&nbsp;you&nbsp;are&nbsp;a&nbsp;30&nbsp;year&nbsp;old&nbsp;male&nbsp;and&nbsp;have&nbsp;attended&nbsp;some&nbsp;college.</p>
<pre>Male18-30</pre>
<pre>&lt;img ImageId=""Male18-30"" /&gt;</pre>
<pre><br /><br />Male18-30</pre>";

			Assert.AreEqual(EXPECTED, new TextTemplate().Resolve(TEMPLATE, fields));
		}

		[TestMethod]
		public void Resolve_ResultValuesWithEmbeddedQuotes_IsSuccessful()
		{
			const string TEMPLATE = @"{{if&nbsp;Gender&nbsp;==&nbsp;""Male""}}<img style=""font-family: Verdana, Arial, Helvetica, sans-serif;"" src=""http://localhost/kswapi/Content/9f8957b2-748e-466a-b9e8-a2810004316b/pictures/Images/big-jellyfish"" alt=""Big Jellyfish"" />{{else}}<img style=""font-family: Verdana, Arial, Helvetica, sans-serif;"" src=""http://localhost/kswapi/Content/9f8957b2-748e-466a-b9e8-a2810004316b/pictures/Images/square-jellyfish-image"" alt=""Square Jellyfish Image"" />{{/if}}";

			List<TemplateField> fields = new List<TemplateField>
			{
				new TemplateField("FirstName", "John"),
				new TemplateField("LastName", "Smith"),
				new TemplateField("EducationLevel", "SomeCollege"),
				new TemplateField("PresenceOfChildren", false),
				new TemplateField("Age", 30),
				new TemplateField("Gender", Gender.Male)
			};

			const string EXPECTED = @"<img style=""font-family: Verdana, Arial, Helvetica, sans-serif;"" src=""http://localhost/kswapi/Content/9f8957b2-748e-466a-b9e8-a2810004316b/pictures/Images/big-jellyfish"" alt=""Big Jellyfish"" />";

			Assert.AreEqual(EXPECTED, new TextTemplate().Resolve(TEMPLATE, fields));
		}

		//[TestCategory("Performance")]
		//[TestMethod]
		//public void Resolve_FiveValues500Times_IsSmokingFast()
		//{
		//	for (int i = 0; i < 500; i++)
		//	{
		//		IList<TemplateField> values = new List<TemplateField>
		//		{
		//			new TemplateField("FirstName", "John"),
		//			new TemplateField("LastName", "Smith"),
		//			new TemplateField("Age", 30),
		//			new TemplateField("Gender", "M"),
		//			new TemplateField("EducationLevel", "SomeCollege"),
		//		};
		//		Assert.AreEqual("Hello John Smith. I see that you are a 30 year old M and have attended SomeCollege.",
		//			new TextTemplate().Resolve(
		//				"Hello {{FirstName}} {{LastName}}. I see that you are a {{Age}} year old {{Gender}} and have attended {{EducationLevel}}.",
		//				values));
		//	}
		//}

		//[TestCategory("Performance")]
		//[TestMethod]
		//public void Resolve_DecisionTemplateWithGender500Times_IsSuperFast()
		//{
		//	for (int i = 0; i < 500; i++)
		//	{
		//		IList<TemplateField> values = new List<TemplateField>
		//		{
		//			new TemplateField("FirstName", "John"),
		//			new TemplateField("LastName", "Smith"),
		//			new TemplateField("Age", 30),
		//			new TemplateField("Gender", "M"),
		//			new TemplateField("EducationLevel", "SomeCollege"),
		//		};
		//		Assert.AreEqual("Hello John Smith. I see that you are male.",
		//			new TextTemplate().Resolve(
		//				"Hello {{FirstName}} {{LastName}}. I see that you are {{if Gender == \"M\"}}male{{else}}female{{/if}}.", values));
		//	}
		//}
	}
}