﻿using Microsoft.Practices.Unity;

namespace KswApi.Ioc.LifetimeManagers
{
	public class ObjectPersistenceLifetimeManager : LifetimeManager
	{
		private object _value;

		public override object GetValue()
		{
			return _value;
		}

		public override void SetValue(object newValue)
		{
			_value = newValue;
		}

		public override void RemoveValue()
		{
		}
	}
}
