﻿using System;
using KswApi.Ioc.LifetimeManagers;
using Microsoft.Practices.Unity;

namespace KswApi.Ioc
{
	public static class Dependency
	{
		private static readonly IUnityContainer Container;

		static Dependency()
		{
			Container = new UnityContainer();
		}

		public static T Get<T>()
		{
			return Container.Resolve<T>();
		}

		public static object Get(Type type)
		{
			return Container.Resolve(type);
		}

		public static void Set<T>(T value)
		{
			Container.RegisterInstance(value, new ObjectPersistenceLifetimeManager());
		}

		public static void Set<TInterface, TType>() where TType : TInterface
		{
			Container.RegisterType<TInterface, TType>();
		}

		public static bool Has<TInterface>()
		{
			return Container.IsRegistered<TInterface>();
		}
	}
}
