﻿using KswApi.Interface.Objects;
using System;
using System.Reflection;

namespace KswApi.Test.Framework.Fakes
{
    public class FakeOperation
    {
        public FakeOperation(Type type, string method)
        {
            MethodInfo = type.GetMethod(method);
        }

        public MethodInfo MethodInfo { get; private set; }
        public StreamResponse StreamResponse { get; private set; }
    }
}
