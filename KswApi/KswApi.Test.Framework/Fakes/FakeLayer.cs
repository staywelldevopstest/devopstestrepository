﻿using KswApi.Caching;
using KswApi.Common.Configuration;
using KswApi.Common.Interfaces;
using KswApi.Data.Mongo;
using KswApi.Indexing;
using KswApi.Ioc;
using KswApi.Logging.Interfaces;
using KswApi.Logging.Logs;
using KswApi.Logic.Framework.Interfaces;
using KswApi.Repositories.Interfaces;
using Moq;

namespace KswApi.Test.Framework.Fakes
{
    public class FakeLayer
    {
        private FakeLayer()
        {

        }

        public static FakeLayer Setup()
        {
            FakeLayer fake = new FakeLayer
                            {
                                Cache = new FakeCache(),
                                DataLayer = new FakeDataSetProvider(),
                                Settings = new Settings(),
                                UserAuthenticator = new FakeUserAuthenticator(),
								BinaryStoreFactory = new FakeBinaryStoreFactory()
                            };

            fake.MockSmsService = new Mock<ISmsService>();
            fake.MockSmsMessageForwarder = new Mock<ISmsMessageForwarder>();
			fake.MockIndex = new Mock<IIndex>();

            Dependency.Set<ICache>(fake.Cache);
            Dependency.Set(fake.Settings);
            Dependency.Set<IUserAuthenticator>(fake.UserAuthenticator);
            Dependency.Set<ILog>(new RepositoryLog());
			Dependency.Set<IBinaryStoreFactory>(fake.BinaryStoreFactory);

            Dependency.Set(fake.MockSmsService.Object);
            Dependency.Set(fake.MockSmsMessageForwarder.Object);
			Dependency.Set(fake.MockIndex.Object);

            IRepositoryFactory dataLayerRepositoryFactory = new MongoRepositoryFactory(fake.DataLayer);
            IRepositoryFactory cachedRepositoryFactory = new RepositoryCacheFactory(dataLayerRepositoryFactory);
            Dependency.Set(cachedRepositoryFactory);

            return fake;
        }

        public FakeCache Cache { get; private set; }
        public FakeDataSetProvider DataLayer { get; private set; }
        public Settings Settings { get; private set; }
        public FakeUserAuthenticator UserAuthenticator { get; private set; }
		public FakeBinaryStoreFactory BinaryStoreFactory { get; private set; }
		
		public Mock<ISmsService> MockSmsService { get; private set; }
		public Mock<IIndex> MockIndex { get; private set; }
        public Mock<ISmsMessageForwarder> MockSmsMessageForwarder { get; private set; }
    }
}
