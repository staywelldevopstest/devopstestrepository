﻿using KswApi.Common.Objects;
using KswApi.Interface.Objects;
using KswApi.Poco.Administration;
using KswApi.Poco.Sms;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;

namespace KswApi.Test.Framework.Fakes
{
    public static class FakeDataLayerExtensions
    {
        #region Public Extension Methods

        public static List<Client> AddClients(this FakeDataSetProvider provider, int count)
        {
            return Add(provider, FakeData.Client, count);
        }

        public static List<Client> AddClients(this FakeDataSetProvider provider, List<Client> clients)
        {
            return Add(provider, clients);
        }

        public static List<License> AddLicenses(this FakeDataSetProvider provider, int count)
        {
            return Add(provider, FakeData.License, count);
        }

        public static List<Application> AddApplications(this FakeDataSetProvider provider, int count)
        {
            return Add(provider, FakeData.Application, count);
        }

        public static List<SmsSession> AddSmsSessions(this FakeDataSetProvider provider, bool expired, int count)
        {
            return Add(provider, FakeData.SmsSession, expired, count);
        }

        public static List<AccessToken> AddAccessTokens(this FakeDataSetProvider provider, bool expired, int count)
        {
            return Add(provider, FakeData.AccessToken, expired, count);
        }

        public static List<AuthorizationGrant> AddAuthorizationGrants(this FakeDataSetProvider provider, bool expired,
                                                                      int count)
        {
            return Add(provider, FakeData.AuthorizationGrant, expired, count);
        }

        #endregion Public Extension Methods

        #region Private Helper Methods

        private static List<T> Add<T>(FakeDataSetProvider provider, Func<bool, int, T> creator, bool boolValue, int count) where T : class, new()
        {
            List<T> list = new List<T>();

            int start = provider.Count<T>() + 1;

            for (int i = start; i < start + count; i++)
                list.Add(creator(boolValue, i));

            provider.Add(list);

            return list;
        }

        private static List<T> Add<T>(FakeDataSetProvider provider, Func<int, T> creator, int count) where T : class, new()
        {
            List<T> list = new List<T>();

            int start = provider.Count<T>() + 1;

            for (int i = start; i < start + count; i++)
                list.Add(creator(i));

            provider.Add(list);

            return list;
        }

        private static List<T> Add<T>(FakeDataSetProvider provider, List<T> objectData) where T : class, new()
        {
            provider.Add(objectData);

            return objectData;
        }

        #endregion Private Helper Methods
    }
}
