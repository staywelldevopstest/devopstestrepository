﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using KswApi.Data.Mongo;

namespace KswApi.Test.Framework.Fakes
{
	/// <summary>
	/// This allows us to enumerate through a data set query
	/// and add/modify the data set without throwing exceptions,
	/// similar to how the database would handle the results
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class FakeReentrantDataset<T> : IDataSet<T>
	{
		private readonly FakeDataSet<T> _dataSet;
		private readonly List<T> _reentrantItems;
		private readonly IQueryable<T> _queryable;

		public FakeReentrantDataset(FakeDataSet<T> dataSet)
		{
			_dataSet = dataSet;
			_reentrantItems = new List<T>(dataSet);
			_queryable = _reentrantItems.AsQueryable();
		}

		public IEnumerator<T> GetEnumerator()
		{
			return _reentrantItems.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _reentrantItems.GetEnumerator();
		}

		public Expression Expression { get { return _queryable.Expression; } }
		public Type ElementType { get { return _queryable.ElementType; } }
		public IQueryProvider Provider { get { return _queryable.Provider; } }

		public IEnumerable<Guid> IdsWhere(Expression<Func<T, bool>> expression)
		{
			return _dataSet.IdsWhere(expression);
		}	

		public T PropertiesOf(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] properties)
		{
			return _dataSet.PropertiesOf(expression, properties);
		}

		public IEnumerable<T> Or(IEnumerable<Expression<Func<T, bool>>> expressions)
		{
			return _dataSet.Or(expressions);
		}

		public IEnumerable<T> And(IEnumerable<Expression<Func<T, bool>>> expressions)
		{
			return _dataSet.And(expressions);
		}

		public void Add(T item)
		{
			_dataSet.Add(item);
		}

		public bool TryAdd(T item)
		{
			return _dataSet.TryAdd(item);
		}

		public void Update(T item)
		{
			_dataSet.Update(item);
		}

		public long Update<TMember>(Expression<Func<T, bool>> expression, Expression<Func<T, TMember>> memberExpression, TMember value)
		{
			return _dataSet.Update(expression, memberExpression, value);
		}

		public void Upsert(T item)
		{
			_dataSet.Upsert(item);
		}

		public void Increment(Expression<Func<T, bool>> expression, Expression<Func<T, int>> propertyExpression, int count)
		{
			_dataSet.Increment(expression, propertyExpression, count);
		}

		public void Increment(Expression<Func<T, bool>> expression, Expression<Func<T, long>> propertyExpression, long count)
		{
			_dataSet.Increment(expression, propertyExpression, count);
		}

		public void Remove(Guid id)
		{
			_dataSet.Remove(id);
		}

		public void Remove(Expression<Func<T, bool>> expression)
		{
			_dataSet.Remove(expression);
		}
	}
}
