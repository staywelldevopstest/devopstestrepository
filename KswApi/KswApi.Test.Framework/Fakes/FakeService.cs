﻿using KswApi.Test.Framework.Interfaces;

namespace KswApi.Test.Framework.Fakes
{
	public class FakeService : IFakeService
	{
		public void UnmarkedOperation()
		{
		}

		public void PublicOperation()
		{
		}

		public void AuthenticationOperation()
		{
		}

		public void InternalOperation()
		{
			
		}
	}
}
