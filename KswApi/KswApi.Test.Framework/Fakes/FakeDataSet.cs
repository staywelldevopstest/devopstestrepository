﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using KswApi.Data.Mongo;
using KswApi.Common.Extensions;

namespace KswApi.Test.Framework.Fakes
{
	public class FakeDataSet<T> : IDataSet<T>, IList<T>
	{
		private readonly List<T> _items;
		private readonly IQueryable<T> _queryable;

		public FakeDataSet(params T[] items)
		{
			_items = new List<T>(items);
			_queryable = _items.AsQueryable();
		}

		public FakeReentrantDataset<T> CreateReentrant()
		{
			return new FakeReentrantDataset<T>(this);
		}

		public IEnumerator<T> GetEnumerator()
		{
			return _items.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _items.GetEnumerator();
		}

		public Expression Expression { get { return _queryable.Expression; } }
		public Type ElementType { get { return _queryable.ElementType; } }
		public IQueryProvider Provider { get { return _queryable.Provider; } }

		public IEnumerable<Guid> IdsWhere(Expression<Func<T, bool>> expression)
		{
			PropertyInfo info = typeof(T).GetProperty("Id");
			Func<T, bool> func = expression.Compile();
			return _items.Where(func).Select(item => (Guid)info.GetValue(item));
		}

		public T PropertiesOf(Expression<Func<T, bool>> expression, params Expression<Func<T, object>>[] properties)
		{
			Func<T, bool> func = expression.Compile();
			T value = _items.FirstOrDefault(func);
			if (Equals(value, default(T)))
				return default(T);
			ConstructorInfo constructor = typeof (T).GetConstructor(Type.EmptyTypes);
			if (constructor == null)
				throw new InvalidOperationException("Type doesn't have a default constructor.");
			T result = (T)constructor.Invoke(null);
			foreach (PropertyInfo property in properties.Select(item => item.GetProperty()))
				property.SetValue(result, property.GetValue(value));

			return result;
		}

		public IEnumerable<T> Or(IEnumerable<Expression<Func<T, bool>>> expressions)
		{
			List<Func<T, bool>> funcs = expressions.Select(item => item.Compile()).ToList();
			return _items.Where(item => funcs.Any(func => func(item)));
		}

		public IEnumerable<T> And(IEnumerable<Expression<Func<T, bool>>> expressions)
		{
			List<Func<T, bool>> funcs = expressions.Select(item => item.Compile()).ToList();
			return _items.Where(item => funcs.All(func => func(item)));
		}

		public void Add(T item)
		{
			// set the "Id" property if available
			PropertyInfo property = typeof(T).GetProperty("Id", typeof(Guid));
			if (property != null && property.CanRead && property.CanWrite)
			{
				Guid id = (Guid)property.GetValue(item, null);
				if (id == Guid.Empty)
					property.SetValue(item, Guid.NewGuid(), null);
			}

			_items.Add(item);
		}

		public bool TryAdd(T item)
		{
			int index = GetIndexOf(item);

			if (index >= 0)
				return false;

			_items.Add(item);

			return true;
		}

		public void AddRange(IEnumerable<T> items)
		{
			_items.AddRange(items);
		}

		public void Update(T item)
		{
			int index = GetIndexOf(item);

			_items[index] = item;
		}

		public long Update<TMember>(Expression<Func<T, bool>> expression, Expression<Func<T, TMember>> memberExpression, TMember value)
		{
			Func<T, bool> func = expression.Compile();

			Predicate<T> predicate = func.Invoke;

			long count = 0;
			foreach (T item in _items.FindAll(predicate))
			{
				memberExpression.GetProperty().SetValue(item, value);
				count++;
			}

			return count;
		}

		public void Upsert(T item)
		{
			int index = GetIndexOf(item);

			if (index >= 0)
				_items[index] = item;
			else
				_items.Add(item);
		}

		public void Increment(Expression<Func<T, bool>> expression, Expression<Func<T, int>> propertyExpression, int count)
		{
			Func<T, bool> func = expression.Compile();

			Predicate<T> predicate = func.Invoke;

			foreach (T item in _items.FindAll(predicate))
			{
				PropertyInfo info = propertyExpression.GetProperty();
				info.SetValue(item, ((int)info.GetValue(info)) + count);
				count++;
			}
		}

		public void Increment(Expression<Func<T, bool>> expression, Expression<Func<T, long>> propertyExpression, long count)
		{
			Func<T, bool> func = expression.Compile();

			Predicate<T> predicate = func.Invoke;

			foreach (T item in _items.FindAll(predicate))
			{
				PropertyInfo info = propertyExpression.GetProperty();
				info.SetValue(item, ((long)info.GetValue(info)) + count);
				count++;
			}
		}

		public int Count
		{
			get
			{
				return _items.Count;
			}
		}

		private int GetIndexOf(Guid id)
		{
			PropertyInfo propertyInfo = typeof(T).GetProperty("Id");
			if (propertyInfo == null)
				throw new ArgumentException("Type specified for a data set does not have an Id property.");

			if (propertyInfo.PropertyType != typeof(Guid))
				throw new ArgumentException("Id property for an entity is not a Guid.");

			for (int i = 0; i < _items.Count; i++)
			{
				if (((Guid)propertyInfo.GetValue(_items[i], null)) == id)
					return i;
			}

			throw new ArgumentException("Item was not found.");
		}

		private int GetIndexOf(T item)
		{
			PropertyInfo propertyInfo = typeof(T).GetProperty("Id");
			if (propertyInfo == null)
				throw new ArgumentException("Type specified for a data set does not have an Id property.");

			if (propertyInfo.PropertyType != typeof(Guid))
				throw new ArgumentException("Id property for an entity is not a Guid.");

			Guid id = (Guid)propertyInfo.GetValue(item, null);

			for (int i = 0; i < _items.Count; i++)
			{
				if (((Guid)propertyInfo.GetValue(_items[i], null)) == id)
					return i;
			}

			throw new ArgumentException("Item was not found.");
		}

		public void Remove(Guid id)
		{
			int index = GetIndexOf(id);

			_items.RemoveAt(index);
		}

		public void Remove(Expression<Func<T, bool>> expression)
		{
			Func<T, bool> func = expression.Compile();
			Predicate<T> predicate = func.Invoke;
			_items.RemoveAll(predicate);
		}

		public int IndexOf(T item)
		{
			return _items.IndexOf(item);
		}

		public void Insert(int index, T item)
		{
			_items.Insert(index, item);
		}

		public void RemoveAt(int index)
		{
			_items.RemoveAt(index);
		}

		public T this[int index]
		{
			get
			{
				return _items[index];
			}
			set
			{
				_items[index] = value;
			}
		}


		public void Clear()
		{
			_items.Clear();
		}

		public bool Contains(T item)
		{
			return _items.Contains(item);
		}

		public void CopyTo(T[] array, int arrayIndex)
		{
			_items.CopyTo(array, arrayIndex);
		}

		public bool IsReadOnly
		{
			get { return false; }
		}

		public bool Remove(T item)
		{
			return _items.Remove(item);
		}
	}
}
