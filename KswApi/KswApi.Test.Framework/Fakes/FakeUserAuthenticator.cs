﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using KswApi.Common.Objects;
using KswApi.Logic.Framework.Interfaces;
using KswApi.Poco.Administration;

namespace KswApi.Test.Framework.Fakes
{
	public class FakeUserAuthenticator : IUserAuthenticator
	{
		private readonly Dictionary<string, FakeDetails> _users = new Dictionary<string, FakeDetails>(); 

		public FakeUserAuthenticator()
		{
		}

		public FakeUserAuthenticator(string userName, string password, UserAuthenticationDetails details)
		{
			Add(userName, password, details);
		}
		
		public void Add(string userName, string password, UserAuthenticationDetails details)
		{
			_users[userName] = new FakeDetails
			                   {
				                   Password = password,
				                   UserDetails = details
			                   };
		}

		#region Implementation of IUserAuthenticator

		public UserAuthenticationDetails Authenticate(string userName, string password)
		{
			FakeDetails details;
			if (!_users.TryGetValue(userName, out details))
				return null;

			if (password != details.Password)
				return null;

			return details.UserDetails;
		}

		private class FakeDetails
		{
			public string Password { get; set; }
			public UserAuthenticationDetails UserDetails { get; set; }
		};

		#endregion
	}
}
