﻿using KswApi.Data.Mongo;
using System;
using System.Collections.Generic;

namespace KswApi.Test.Framework.Fakes
{
    public class  FakeDataSetProvider : IDataSetProvider
    {
        private readonly Dictionary<Type, object> _sets = new Dictionary<Type, object>();

        public IDataSet<T> GetSet<T>()
        {
            object obj;
            if (_sets.TryGetValue(typeof(T), out obj))
				return ((FakeDataSet<T>)obj).CreateReentrant();

            // create an empty dbset
            FakeDataSet<T> fakeSet = new FakeDataSet<T>();

            _sets[typeof(T)] = fakeSet;

            return fakeSet.CreateReentrant();
        }

        public void Setup<TEntity>(params TEntity[] items) where TEntity : class, new()
        {
            _sets[typeof(TEntity)] = new FakeDataSet<TEntity>(items);
        }

        public FakeDataSet<T> GetFakeSet<T>() where T : class, new()
        {
			object obj;
			if (_sets.TryGetValue(typeof(T), out obj))
				return ((FakeDataSet<T>)obj);

			// create an empty dbset
			FakeDataSet<T> fakeSet = new FakeDataSet<T>();

			_sets[typeof(T)] = fakeSet;

			return fakeSet;
        }

        public void AddItem<TEntity>(TEntity item) where TEntity : class, new()
        {
            FakeDataSet<TEntity> set = GetFakeSet<TEntity>();

            set.Add(item);
        }

        public void Add<TEntity>(IEnumerable<TEntity> items) where TEntity : class, new()
        {
            FakeDataSet<TEntity> set = GetFakeSet<TEntity>();

            set.AddRange(items);
        }

        public int Count<TEntity>() where TEntity : class, new()
        {
            FakeDataSet<TEntity> set = GetFakeSet<TEntity>();

            return set.Count;
        }

        public void Commit()
        {
            CommitCount++;
        }

        public int CommitCount { get; private set; }
	}
}
