﻿using System.Collections.Generic;
using KswApi.Repositories.Interfaces;

namespace KswApi.Test.Framework.Fakes
{
	public class FakeBinaryStoreFactory : IBinaryStoreFactory
	{
		private readonly Dictionary<string, FakeBinaryStore> _stores = new Dictionary<string, FakeBinaryStore>();
		#region Implementation of IBinaryStoreFactory

		public IBinaryStore GetStore(string name)
		{
			FakeBinaryStore store;
			
			if (_stores.TryGetValue(name, out store))
				return store;
			
			store = new FakeBinaryStore();

			_stores[name] = store;

			return store;
		}

		#endregion
	}
}
