﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Web;
using System.Linq;

namespace KswApi.Test.Framework.Fakes
{
	public class FakeHttpFileCollection : HttpFileCollectionBase
	{
		public void Add(FakeHttpFile file)
		{
			BaseAdd(file.FileName, file);
		}

		public override string[] AllKeys
		{
			get
			{
				return BaseGetAllKeys().ToArray();
			}
		}

		public override int Count
		{
			get { return BaseGetAllValues().Length; }
		}

		public override HttpPostedFileBase this[string name]
		{
			get { return (HttpPostedFileBase)BaseGet(name); }
		}

		public override HttpPostedFileBase Get(int index)
		{
			return (HttpPostedFileBase)BaseGet(index);
		}

		public override HttpPostedFileBase this[int index]
		{
			get { return (HttpPostedFileBase)BaseGet(index); }
		}

		public override HttpPostedFileBase Get(string name)
		{
			return (HttpPostedFileBase) BaseGet(name);
		}

		public override string GetKey(int index)
		{
			return BaseGetKey(index);
		}
	}
}
