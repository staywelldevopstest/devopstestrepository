﻿using System;
using System.Collections;
using System.Collections.Generic;
using KswApi.Common.Interfaces;
using KswApi.Caching;

namespace KswApi.Test.Framework.Fakes
{
	public class FakeCache : Cache, ICache, IEnumerable<KeyValuePair<string, object>>
	{
		readonly Dictionary<string, object> _dictionary = new Dictionary<string,object>();

		public int Count
		{
			get { return _dictionary.Count; }
		}

		public void Clear()
		{
			_dictionary.Clear();	
		}

		public void Set<T>(string key, T value)
		{
			_dictionary[key] = value;
		}

		public void Set<T>(string key, T value, TimeSpan expiration)
		{
			_dictionary[key] = value;
		}

		public T Get<T>(string key)
		{
			object value;

			if (_dictionary.TryGetValue(key, out value))
				return (T) value;

			return default(T);
		}

		public bool Contains(string key)
		{
			return (_dictionary.ContainsKey(key));
		}

		public void Remove(string key)
		{
			_dictionary.Remove(key);
		}

		public IEnumerator<KeyValuePair<string, object>> GetEnumerator()
		{
			return _dictionary.GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return _dictionary.GetEnumerator();
		}
	}
}
