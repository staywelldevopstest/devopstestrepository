﻿using KswApi.Common.Objects;
using KswApi.Interface.Objects;
using KswApi.Poco.Administration;
using KswApi.Poco.Sms;
using KswApi.Repositories.Enums;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;

namespace KswApi.Test.Framework.Fakes
{
    public static class FakeData
    {
        public static Client Client(int parameter = 1)
        {
            return new Client
                {
                    ClientName = "ClientName" + parameter,
                    ClientWebSite = string.Format("http://clientsite{0}.com", parameter),
                    ContactEmail = string.Format("contactemail{0}@clientsite{0}.com", parameter),
                    ContactName = "Contact" + parameter,
                    ContactPhone = string.Format("{0}{0}{0}{0}{0}{0}{0}{0}{0}{0}", (parameter % 9) + 1),
                    DateAdded = DateTime.UtcNow,
                    Id = Guid.NewGuid(),
                    Notes = string.Format("Notes " + parameter)
                };
        }

        public static SmsSession SmsSession(bool expired, int parameter = 1)
        {
            return new SmsSession
                {
					DateAdded = DateTime.UtcNow.AddYears(expired ? -2 : 0),
                    Id = Guid.NewGuid(),
                    Keyword = string.Empty,
					LastRequestDate = DateTime.UtcNow.AddMonths(expired ? -19 : 0),
                    LicenseId = Guid.NewGuid(),
                    Origin = SmsSessionOrigin.Client,
                    ServiceNumber = "98517",
                    State = SmsSessionState.Active,
                    SubscriberNumber = string.Format("{0}{0}{0}{0}{0}{0}{0}{0}{0}{0}", (parameter % 9) + 1)
                };
        }

        public static AuthorizationGrant AuthorizationGrant(bool expired, int parameter = 1)
        {
			return new AuthorizationGrant
				{
					Code = string.Empty,
					Expiration = DateTime.UtcNow.AddDays(expired ? -1 : 1),
					Id = Guid.NewGuid(),
					RedirectUri = string.Format("http://clientsite{0}.com", parameter),
					UserDetails = new UserAuthenticationDetails
					              {
						              Id = Guid.NewGuid()
					              }
				};
        }

        public static AccessToken AccessToken(bool expired, int parameter = 1)
        {
            return new AccessToken
                {
                    ApplicationId = Guid.NewGuid(),
					Expiration = DateTime.UtcNow.AddHours(expired ? -1 : +1),
                    Id = Guid.NewGuid(),
                    TokenType = AccessTokenType.Client
                };
        }

        public static License License(int parameter = 1)
        {
            return new License
                {
                    ClientId = Guid.NewGuid(),
                    CreatedDate = DateTime.UtcNow,
                    ParentLicenseId = null,
                    Id = Guid.NewGuid(),
                    Name = string.Format("Name{0}", parameter),
                    Notes = string.Format("Notes {0}", parameter)
                };
        }

        public static Application Application(int parameter = 1)
        {
            return new Application
            {
                Id = Guid.NewGuid(),
                Name = string.Format("Name{0}", parameter),
                Description = string.Format("Description{0}", parameter)
            };
        }

        public static List<Application> Applications(int count, int parameter = 1)
        {
            List<Application> applications = new List<Application>();
            for (int i = 0; i < count; i++)
            {
                applications.Add(Application(parameter++));
            }
            return applications;
        }
    }
}
