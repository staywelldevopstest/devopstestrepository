﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KswApi.Interface.Objects;
using KswApi.Repositories.Interfaces;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KswApi.Test.Framework.Fakes
{
	class FakeBinaryStore : IBinaryStore
	{
		private readonly Dictionary<Guid, Item> _items = new Dictionary<Guid,Item>(); 

		#region Implementation of IBinaryStore

		public bool Save(Guid id, Stream stream, string contentType, DateTime time)
		{
			MemoryStream ms = new MemoryStream();
			stream.CopyTo(ms);

			Item item = new Item
			            {
				            Id = id,
				            Buffer = ms.ToArray(),
				            ContentType = contentType,
				            Time = time
			            };

			_items[id] = item;

			return true;
		}

		public StreamResponse Retrieve(Guid id)
		{
			Item item;
			if (!_items.TryGetValue(id, out item))
				throw new Exception("Fake binary store retrieve: id does not exist.");

			return new StreamResponse(new MemoryStream(item.Buffer));
		}

		public void Delete(Guid id)
		{
			_items.Remove(id);
		}

		public void Rename(Guid source, Guid destination)
		{
			Item item;
			if (!_items.TryGetValue(source, out item))
				throw new Exception("Fake binary store rename: source id does not exist.");

			_items.Remove(source);
			_items[destination] = item;
		}

		#endregion

		private class Item
		{
			public Guid Id { get; set; }
			public byte[] Buffer { get; set; }
			public string ContentType { get; set; }
			public DateTime Time { get; set; }
		}
	}
}
