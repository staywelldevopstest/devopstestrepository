﻿using KswApi.Interface.Objects;
using System.IO;

namespace KswApi.Test.Framework.Fakes
{
    public class FakeStreamResponse : StreamResponse
    {
		public FakeStreamResponse()
			: base(new MemoryStream())
		{
			
		}
    }
}
