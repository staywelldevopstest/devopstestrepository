﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace KswApi.Test.Framework.Fakes
{
	public class FakeHttpFile : HttpPostedFileBase
	{
		private readonly int _contentLength;
		private readonly string _contentType;
		private readonly string _fileName;
		private readonly Stream _stream;

		public FakeHttpFile(string fileName, string contentType, int contentLength, Stream stream)
		{
			_fileName = fileName;
			_contentType = contentType;
			_contentLength = contentLength;
			_stream = stream;
		}

		public override int ContentLength { get { return _contentLength; } }
		public override string ContentType { get { return _contentType; } }
		public override string FileName { get { return _fileName; } }
		public override Stream InputStream { get { return _stream; } }
		public override void SaveAs(string filename)
		{
			using (FileStream fs = File.OpenWrite(filename))
			{
				InputStream.CopyTo(fs);
			}
		}
	}
}
