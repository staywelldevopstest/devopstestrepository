﻿using KswApi.Interface.Attributes;
using KswApi.Interface.Enums;
using KswApi.Logic.Enums;

namespace KswApi.Test.Framework.Interfaces
{
	public interface IFakeService
	{
		void UnmarkedOperation();

		[Allow(ClientType = ClientType.Public)]
		void PublicOperation();

		[Allow(ClientType = ClientType.Authentication)]
		void AuthenticationOperation();

		[Allow(ClientType = ClientType.Internal, Rights = "TestRight")]
		void InternalOperation();
	}
}
