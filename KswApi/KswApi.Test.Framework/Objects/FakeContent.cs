﻿using KswApi.Poco.Content;
using KswApi.Repositories.Objects;

namespace KswApi.Test.Framework.Objects
{
	public class FakeContent
	{
		public ContentOrigin Origin { get; set; }
		public ContentVersion Version { get; set; }
		public ContentBucket Bucket { get; set; }
		public ContentCore Core { get; set; }
		public Copyright Copyright { get; set; }
		public ContentSegment Segment { get; set; }
	}
}
