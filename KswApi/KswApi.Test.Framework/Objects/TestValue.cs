﻿namespace KswApi.Test.Framework.Objects
{
	public class TestValue
	{
		public string Key { get; set; }
		public string Value { get; set; }
	}
}
