﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KswApi.Test.Framework
{
	public static class Expect
	{
		public static T Exception<T>(Action call) where T : Exception
		{
			try
			{
				call();
			}
			catch (T exception)
			{
				return exception;
			}

			Assert.Fail(string.Format("Did not throw exception of type {0}.", typeof(T).Name));

			return null;
		}
	}
}
