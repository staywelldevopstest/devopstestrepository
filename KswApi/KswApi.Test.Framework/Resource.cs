﻿using System.IO;
using System.Reflection;

namespace KswApi.Test.Framework
{
	public static class Resource
	{
		public static Stream GetJpeg()
		{
			return Assembly.GetExecutingAssembly().GetManifestResourceStream("KswApi.Test.Framework.Resources.Test.jpg");
		}

		public static Stream GetPng()
		{
			return Assembly.GetExecutingAssembly().GetManifestResourceStream("KswApi.Test.Framework.Resources.Test.png");
		}

	}
}
