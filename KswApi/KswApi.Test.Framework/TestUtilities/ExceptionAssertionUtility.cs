﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KswApi.Test.Framework.TestUtilities
{
	public static class ExceptionAssertionUtility
	{
		public static void AssertExceptionThrown(Type exceptionType, Action methodToInvoke)
		{
			AssertExceptionThrown(string.Empty, exceptionType, methodToInvoke);
		}

		public static void AssertExceptionThrown(string messageToCheck, Type exceptionType, Action methodToInvoke)
		{
			try
			{
				methodToInvoke.Invoke();
			}
			catch (Exception ex)
			{
				if (ex.GetType() != exceptionType)
				{
					Assert.Fail(string.Format("Expected exception of type '{0}' but type '{1}' occurred with a message of '{2}'.", exceptionType.Name, ex.GetType(), ex.Message));
				}

				if (string.IsNullOrWhiteSpace(messageToCheck) || ex.Message.Contains(messageToCheck))
				{
					// expected
					return;
				}

				Assert.Fail(string.Format("Unexpected exception encountered. Message: was [{0}], expected [{1}].", ex.Message, messageToCheck));
			}

			Assert.Fail("No exception occurred.");
		}
	}
}
