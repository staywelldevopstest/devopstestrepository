﻿using KswApi.Poco.Content;
using KswApi.Test.Framework.Fakes;

namespace KswApi.Test.Framework.DataGenerators
{
    public static class FakeLayerDataInitialization
    {
        public static void Init(FakeLayer fakeLayer)
        {
            SetupTaxonomyTypes(fakeLayer);
        }

        public static void SetupTaxonomyTypes(FakeLayer fakeLayer)
        {
            fakeLayer.DataLayer.Setup(
                new TaxonomyType { Name = "ICD-9", Slug = "icd-9", Regex = @"^(V\d{2}(\.\d{1,2})?|\d{3}(\.\d{1,2})?|E\d{3}(\.\d)?)$" },
                new TaxonomyType { Name = "ICD-10 PCS", Slug = "icd-10-pcs", Regex = @"^[A-TV-Z][0-9][A-Z0-9]\.[A-Z0-9]{4}$" }
                );
        }
    }
}
