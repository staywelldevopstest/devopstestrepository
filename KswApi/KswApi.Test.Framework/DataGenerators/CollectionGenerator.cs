﻿using System;
using System.Collections.Generic;
using KswApi.Interface.Objects.Content;
using KswApi.Poco.Content;
using KswApi.Test.Framework.Fakes;

namespace KswApi.Test.Framework.DataGenerators
{
	public static class CollectionGenerator
	{
		public static void GenerateCollection(FakeLayer fake)
		{
			Collection collection = new Collection
									{
										Id = Guid.NewGuid()
									};

			Topic[] topics = new[]
			                 {
				                 new Topic
				                 {
					                 Id = Guid.NewGuid(),
									 Slug = "topic-1",
									 Title = "Topic 1"
				                 },
								 new Topic
								 {
					                 Id = Guid.NewGuid(),
									 Slug = "topic-2",
									 Title = "Topic 2"
				                 },
								 new Topic
								 {
									 Id = collection.Id,
									 Slug = "collection-1",
									 Title = "Collection 1"
								 }
			                 };

			Guid collectionId = Guid.NewGuid();

			// hierarchal: 1 inside 0 inside collection (2)
			CollectionStructure[] structures = new[]
			                                   {
				                                   new CollectionStructure
				                                   {
													   Id = topics[0].Id,
													   ItemId = topics[0].Id,
													   Path = new List<Guid> { collection.Id, topics[0].Id },
					                                   ParentId = topics[2].Id,
													   Type = CollectionItemType.Topic,
													   CollectionId = collectionId
				                                   },
												   new CollectionStructure
				                                   {
													   Id = topics[1].Id,
													   ItemId = topics[1].Id,
													   Path = new List<Guid> { collection.Id, topics[1].Id },
					                                   ParentId = topics[0].Id,
													   Type = CollectionItemType.Topic,
													   CollectionId = collectionId
				                                   },
												   new CollectionStructure
				                                   {
													   Id = collection.Id,
													   ItemId = collection.Id,
													   Children = new List<Guid> {topics[0].Id, topics[1].Id},
													   Path = new List<Guid> { collection.Id },
													   CollectionId = collectionId,
													   Type = CollectionItemType.Collection
				                                   }
			                                   };

			fake.DataLayer.Setup(collection);
			fake.DataLayer.Setup(topics);
			fake.DataLayer.Setup(structures);
		}
	}
}
