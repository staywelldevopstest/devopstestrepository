﻿using System;
using System.Collections.Generic;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.Content;
using KswApi.Poco.Content;
using KswApi.Test.Framework.Fakes;
using KswApi.Test.Framework.Objects;

namespace KswApi.Test.Framework.DataGenerators
{
	public static class ContentGenerator
	{
		public static FakeContent GenerateContent(FakeLayer fakeLayer)
		{
            FakeLayerDataInitialization.Init(fakeLayer);

			Guid contentId = Guid.NewGuid();
			Guid segmentId = Guid.NewGuid();
			Guid masterId = Guid.NewGuid();
			Guid copyrightId = Guid.NewGuid();
			Guid originId = Guid.NewGuid();

			FakeContent content = new FakeContent();

			content.Origin = new ContentOrigin { Id = originId, Name = "Origin Name" };

			content.Bucket = new ContentBucket
			{
				Id = Guid.NewGuid(),
				Slug = "bucket-slug",
				Type = ContentType.Article,
				OriginId = originId,
				Name = "The Bucket",
				CaseInsensitiveName = "the bucket",
				Status = ObjectStatus.Active,
				LegacyId = 5,
				ReadOnly = true,
				Constraints = new ContentBucketConstraint
							  {
								  Length = 21
							  },
				Segments = new List<ContentBucketSegment>
				           {
					           new ContentBucketSegment
					           {
						           Id = segmentId,
								   Name = "Segment Name",
								   Slug = "segment-name",
								   Required = true
					           }
				           }
			};

			content.Version = new ContentVersion
			{
				Id = contentId,
				CoreId = masterId,
				IsMaster = true,
				BucketId = content.Bucket.Id,
				Title = "The title",
				Slug = "the-slug",
				Status = ObjectStatus.Active,
				Language = "en",
				Segments = new List<ContentSegmentItem>
						                             {
							                             new ContentSegmentItem
								                             {
									                             Id = segmentId
								                             }
						                             },
				Paths = new List<ContentPath>
				        {
					        new ContentPath {BucketIdOrSlug = content.Bucket.Slug, ContentIdOrSlug = "the-slug"},
							new ContentPath {BucketIdOrSlug = content.Bucket.Id.ToString(), ContentIdOrSlug = "the-slug"},
							new ContentPath {BucketIdOrSlug = content.Bucket.Slug, ContentIdOrSlug = contentId.ToString()},
							new ContentPath {BucketIdOrSlug = content.Bucket.Id.ToString(), ContentIdOrSlug = contentId.ToString()},
				        }
			};

            content.Core = new ContentCore
            {
                Id = masterId,
                MasterId = contentId,
                AgeCategories = new List<AgeCategory>
				                                       {
					                                       AgeCategory.Adult
				                                       },
                Gender = Gender.Female,
                Authors = new List<string> { "Author 1", "Author 2" },
                FleschKincaidReadingLevel = "0-30",
                GunningFogReadingLevel = "4-5",
                OnlineEditors = new List<string> { "Editor 1", "Editor 2" },
                OnlineOriginatingSources = new List<OnlineOriginatingSource>
				                                                  {
					                                                  new OnlineOriginatingSource
					                                                  {
						                                                  Date = DateTime.Now,
						                                                  Source = "Source",
						                                                  Title = "Source Title",
						                                                  Uri = "google.com"
					                                                  }
				                                                  },
                LastReviewedDate = DateTime.Now,
                OnlineMedicalReviewers = new List<string> { "Reviewer 1", "Reviewer 2" },
                PrintOriginatingSources = new List<PrintOriginatingSource>
				                                                 {
					                                                 new PrintOriginatingSource
					                                                 {
						                                                 Date = DateTime.Now,
						                                                 Title = "Print Title"
					                                                 }
				                                                 },
                RecommendedSites = new List<RecommendedSite>
				                                          {
					                                          new RecommendedSite
					                                          {
						                                          Title = "Site Title",
						                                          Uri = "tempuri.com"
					                                          }
				                                          },
                CopyrightId = copyrightId,
                Versions = new List<ContentVersionReference> { new ContentVersionReference { Id = content.Version.Id, Language = content.Version.Language, Slug = content.Version.Slug } },
                Taxonomies = new Dictionary<string, List<TaxonomyValue>>()
                                { 
                                     {
                                         "icd-9", 
                                         new List<TaxonomyValue>
                                             {
                                                    new TaxonomyValue
                                                    {
                                                        Value = "162.4", 
                                                        Source = TaxonomyValueSource.Ksw
                                                    },
                                                    new TaxonomyValue
                                                    {
                                                        Value = "678.9", 
                                                        Source = TaxonomyValueSource.ThirdParty
                                                    },
                                                    new TaxonomyValue
                                                    {
                                                        Value = "462.4", 
                                                        Source = TaxonomyValueSource.Ksw
                                                    },
                                                    new TaxonomyValue
                                                    {
                                                        Value = "462.4", 
                                                        Source = TaxonomyValueSource.ThreeM
                                                    },
                                                    new TaxonomyValue
                                                    {
                                                        Value = "852.4", 
                                                        Source = TaxonomyValueSource.ThreeM
                                                    }
                                             }
                                    }
                                 }
                            };

			content.Segment = new ContentSegment
			{
				Id = segmentId,
				ContentId = contentId,
				Body = "The body"
			};

			content.Copyright = new Copyright
			{
				Id = copyrightId,
				DateAdded = DateTime.UtcNow,
				DateModified = DateTime.UtcNow,
				Value = "test copyright with current year as {{year}}"
			};

			fakeLayer.DataLayer.Setup(content.Origin);
			fakeLayer.DataLayer.Setup(content.Bucket);
			fakeLayer.DataLayer.Setup(content.Version);
			fakeLayer.DataLayer.Setup(content.Core);
			fakeLayer.DataLayer.Setup(content.Segment);
			fakeLayer.DataLayer.Setup(content.Copyright);

			return content;
		}
    }
}
