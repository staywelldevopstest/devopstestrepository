﻿namespace KswApi.Examples
{
	public interface IExample<out T>
	{
		T Value { get; }
		string Description { get; }
	}
}
