﻿using System;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.Content;

namespace KswApi.Examples.Internal.Users
{
	class UserDetailsResponseExample : Example<UserDetailsResponse>
	{
		#region Overrides of Example<UserDetailsResponse>

		public override UserDetailsResponse Value
		{
			get
			{
				return new UserDetailsResponse
				       {
						   Id = new Guid("cfed06fd-63cf-47ff-be57-b0c20e52cfa2"),
						   FullName = "Charlie Croker",
						   Type = UserType.DomainUser
				       };
			}
		}

		#endregion
	}
}
