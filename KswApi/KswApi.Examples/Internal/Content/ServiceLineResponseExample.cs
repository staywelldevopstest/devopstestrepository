﻿using System.Collections.Generic;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.Content;

namespace KswApi.Examples.Internal.Content
{
	class ServiceLineResponseExample : Example<List<HierarchicalTaxonomyResponse>>
	{
		#region Overrides of Example<List<HierarchicalTaxonomyResponse>>

		public override List<HierarchicalTaxonomyResponse> Value
		{
			get
			{
				return new List<HierarchicalTaxonomyResponse>
				       {
					       new HierarchicalTaxonomyResponse
					       {
						       Slug = "immunology",
							   Path = "adult/asthma",
							   PathValues = new List<string> {"Adult", "Asthma", "Immunology"},
							   Type = HierarchicalTaxonomyType.Serviceline,
							   Value = "Immunology"
					       }
				       };
			}
		}

		#endregion
	}
}
