﻿namespace KswApi.Examples.Internal
{
	public abstract class Example<T> : IExample<T>
	{
		public abstract T Value { get; }
		public virtual string Description { get { return string.Empty; } }
	}
}
