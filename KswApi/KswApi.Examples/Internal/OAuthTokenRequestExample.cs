﻿using System;
using KswApi.Interface.Objects;

namespace KswApi.Examples.Internal
{
	class OAuthTokenRequestExample : Example<OAuthTokenRequest>
	{
		#region Overrides of ExampleBase<OAuthTokenRequest>

		public override OAuthTokenRequest Value
		{
			get
			{
				return new OAuthTokenRequest
					   {
						   client_id = "96a7f783-1492-49f7-bb28-d7b7bec7b47a",
						   client_secret = "4A3EQWC0fI1cyfGKSnn3RZfZXSNR5fJ8lfzAm7rK2jT5PyLDPpIPmsft33bbYpY5",
						   grant_type = "client_credentials",
						   code = "N9NIjfM8FkSAuP8LbgvDFLQ0aZdQj375Tj8Y2ZYVXZuVh9ZUAKGUb7qA7m2IcAcV",
						   redirect_uri = "https://admin.kramesstaywell.com",
						   refresh_token = "J46kfj3LOUWdgOlraoOhDf8qwdYbBhRFP7NzpfQcri93HoP5SRaYzkr1qBGfw8JO",
						   scope = "Read_Content"
					   };
			}
		}

		#endregion
	}
}
