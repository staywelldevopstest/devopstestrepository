﻿using System;

namespace KswApi.Examples.Internal
{
	static class Default
	{
		public static DateTime DateTime
		{
			get
			{
				return new DateTime(2013, 5, 30, 3, 37, 21, 253);
			}
		}

		public static DateTime ExpirationDateTime
		{
			get
			{
				return new DateTime(2014, 3, 27, 4, 11, 15, 123);
			}
		}

		public static DateTime AddedDateTime
		{
			get
			{
				return new DateTime(2013, 1, 15, 8, 21, 51, 524);
			}
		}

		public static DateTime ModifiedDateTime
		{
			get
			{
				return new DateTime(2013, 4, 7, 13, 18, 42, 356);
			}
		}
	}
}
