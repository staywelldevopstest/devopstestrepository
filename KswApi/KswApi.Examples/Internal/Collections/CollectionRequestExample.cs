﻿using KswApi.Interface.Objects.Content;

namespace KswApi.Examples.Internal.Collections
{
	public class CollectionRequestExample : Example<CollectionRequest>
	{
		#region Overrides of Example<CollectionRequest>

		public override CollectionRequest Value
		{
			get
			{
				return new CollectionRequest
				       {
						   Title = "Collection Title",
						   Description = "Collection Description",
						   Expires = Default.ExpirationDateTime,
						   ImageUri = "https://api.kramesstaywell.com/content/my-images/images/collection-image.jpg"
						   
				       };
			}
		}

		#endregion
	}
}
