﻿using System;
using System.Collections.Generic;
using KswApi.Examples.Internal.Buckets;
using KswApi.Examples.Internal.Content;
using KswApi.Examples.Internal.Users;
using KswApi.Interface.Objects.Content;

namespace KswApi.Examples.Internal.Collections
{
	public class CollectionResponseExample : Example<CollectionResponse>
	{
		#region Overrides of Example<CollectionResponse>

		public override CollectionResponse Value
		{
			get
			{
				return new CollectionResponse
					   {
						   Id = new Guid("3dd8897a-ca29-4443-89e1-196bf25c1226"),
						   CreatedBy = new UserDetailsResponseExample().Value,
						   DateAdded = Default.AddedDateTime,
						   DateModified = Default.ModifiedDateTime,
						   Expires = Default.ExpirationDateTime,
						   Slug = "collection-slug",
						   ImageUri = "https://api.kramesstaywell.com/content/my-images/images/collection-image.jpg",
						   Title = "Collection Title",
						   Description = "Collection Description",
						   LicenseCount = 3,
						   Items = new List<CollectionItemResponse>
					               {
						               new CollectionItemResponse
						               {
							               Id = new Guid("6775c41b-9245-43d6-960b-b5fa17e2bf38"),
							               Title = "A Collection Topic",
							               Slug = "a-collection-topic",
							               Description = "Collection Topic Description",
							               Type = CollectionItemType.Topic,
							               Flagged = true,
							               FlagComment = "Notes about the reason the topic was flagged",
							               ImageUri = "https://api.kramesstaywell.com/content/my-images/images/topic-image.jpg",
							               Items = new List<CollectionItemResponse>
							                       {
								                       new CollectionItemResponse
								                       {
									                       Id = new Guid("4bbefc53-0adb-4ecf-a3c4-6ad15d4e48cf"),
									                       Title = "Collection Content Number One",
									                       Slug = "collection-content-number-one",
									                       Description = "Blurb for Collection Content Number One",
									                       Bucket = new BucketReferenceExample().Value,
									                       Type = CollectionItemType.Content,
									                       Flagged = true,
									                       FlagComment = "Notes about the reason the content was flagged",
									                       ServiceLines = new ServiceLineResponseExample().Value
								                       },
								                       new CollectionItemResponse
								                       {
									                       Id = new Guid("05dd6f96-0130-40dd-9fb0-871edba3ae14"),
									                       Title = "Collection Content Number Two",
									                       Slug = "collection-content-number-two",
									                       Description = "Blurb for Collection Content Number Two",
									                       Bucket = new BucketReferenceExample().Value,
									                       Type = CollectionItemType.Content,
														   Disabled = true
								                       }
							                       }
						               }
					               }
					   };
			}
		}

		#endregion
	}
}
