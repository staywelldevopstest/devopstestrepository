﻿using System;
using KswApi.Interface.Objects.Content;

namespace KswApi.Examples.Internal.Buckets
{
	class BucketReferenceExample : Example<ContentBucketReference>
	{
		#region Overrides of Example<ContentBucketReference>

		public override ContentBucketReference Value
		{
			get
			{
				return new ContentBucketReference
					   {
						   Id = new Guid("a0506f7a-e84f-4e01-a02f-ab3d919de2e7"),
						   Name = "Bucket Name",
						   Slug = "bucket-slug"
					   };
			}
		}

		#endregion
	}
}
