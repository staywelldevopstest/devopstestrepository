﻿namespace KswApi.Replicator
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.sourceUrl = new System.Windows.Forms.TextBox();
			this.sourceConnect = new System.Windows.Forms.Button();
			this.sourceBucketList = new System.Windows.Forms.ListView();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.sourceApplicationId = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.sourceApplicationSecret = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.replicate = new System.Windows.Forms.Button();
			this.checkBox1 = new System.Windows.Forms.CheckBox();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.destinationApplicationSecret = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.destinationConnect = new System.Windows.Forms.Button();
			this.destinationApplicationId = new System.Windows.Forms.TextBox();
			this.label5 = new System.Windows.Forms.Label();
			this.destinationUrl = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.destinationBucketList = new System.Windows.Forms.ListView();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(6, 22);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(71, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Service URL:";
			// 
			// sourceUrl
			// 
			this.sourceUrl.Location = new System.Drawing.Point(127, 19);
			this.sourceUrl.Name = "sourceUrl";
			this.sourceUrl.Size = new System.Drawing.Size(183, 20);
			this.sourceUrl.TabIndex = 1;
			this.sourceUrl.Text = "https://api.kramesstaywell.com";
			// 
			// sourceConnect
			// 
			this.sourceConnect.Location = new System.Drawing.Point(343, 38);
			this.sourceConnect.Name = "sourceConnect";
			this.sourceConnect.Size = new System.Drawing.Size(75, 23);
			this.sourceConnect.TabIndex = 4;
			this.sourceConnect.Text = "Connect";
			this.sourceConnect.UseVisualStyleBackColor = true;
			this.sourceConnect.Click += new System.EventHandler(this.sourceConnect_Click);
			// 
			// sourceBucketList
			// 
			this.sourceBucketList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.sourceBucketList.CheckBoxes = true;
			this.sourceBucketList.GridLines = true;
			this.sourceBucketList.Location = new System.Drawing.Point(12, 129);
			this.sourceBucketList.Name = "sourceBucketList";
			this.sourceBucketList.Size = new System.Drawing.Size(445, 153);
			this.sourceBucketList.TabIndex = 5;
			this.sourceBucketList.UseCompatibleStateImageBehavior = false;
			this.sourceBucketList.View = System.Windows.Forms.View.List;
			// 
			// groupBox1
			// 
			this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox1.Controls.Add(this.sourceApplicationSecret);
			this.groupBox1.Controls.Add(this.label4);
			this.groupBox1.Controls.Add(this.sourceConnect);
			this.groupBox1.Controls.Add(this.sourceApplicationId);
			this.groupBox1.Controls.Add(this.label3);
			this.groupBox1.Controls.Add(this.sourceUrl);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(12, 12);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(445, 102);
			this.groupBox1.TabIndex = 6;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Source";
			// 
			// sourceApplicationId
			// 
			this.sourceApplicationId.Location = new System.Drawing.Point(127, 45);
			this.sourceApplicationId.Name = "sourceApplicationId";
			this.sourceApplicationId.Size = new System.Drawing.Size(183, 20);
			this.sourceApplicationId.TabIndex = 3;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 48);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(76, 13);
			this.label3.TabIndex = 2;
			this.label3.Text = "Application ID:";
			// 
			// sourceApplicationSecret
			// 
			this.sourceApplicationSecret.Location = new System.Drawing.Point(127, 71);
			this.sourceApplicationSecret.Name = "sourceApplicationSecret";
			this.sourceApplicationSecret.PasswordChar = '*';
			this.sourceApplicationSecret.Size = new System.Drawing.Size(183, 20);
			this.sourceApplicationSecret.TabIndex = 5;
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 74);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(96, 13);
			this.label4.TabIndex = 4;
			this.label4.Text = "Application Secret:";
			// 
			// splitContainer1
			// 
			this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.sourceBucketList);
			this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.destinationBucketList);
			this.splitContainer1.Panel2.Controls.Add(this.groupBox2);
			this.splitContainer1.Size = new System.Drawing.Size(1044, 421);
			this.splitContainer1.SplitterDistance = 460;
			this.splitContainer1.TabIndex = 7;
			// 
			// replicate
			// 
			this.replicate.Location = new System.Drawing.Point(12, 450);
			this.replicate.Name = "replicate";
			this.replicate.Size = new System.Drawing.Size(75, 23);
			this.replicate.TabIndex = 8;
			this.replicate.Text = "Replicate ->";
			this.replicate.UseVisualStyleBackColor = true;
			this.replicate.Click += new System.EventHandler(this.replicate_Click);
			// 
			// checkBox1
			// 
			this.checkBox1.AutoSize = true;
			this.checkBox1.Location = new System.Drawing.Point(12, 427);
			this.checkBox1.Name = "checkBox1";
			this.checkBox1.Size = new System.Drawing.Size(108, 17);
			this.checkBox1.TabIndex = 9;
			this.checkBox1.Text = "Replace Buckets";
			this.checkBox1.UseVisualStyleBackColor = true;
			// 
			// groupBox2
			// 
			this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.groupBox2.Controls.Add(this.destinationApplicationSecret);
			this.groupBox2.Controls.Add(this.label2);
			this.groupBox2.Controls.Add(this.destinationConnect);
			this.groupBox2.Controls.Add(this.destinationApplicationId);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.destinationUrl);
			this.groupBox2.Controls.Add(this.label6);
			this.groupBox2.Location = new System.Drawing.Point(12, 12);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(554, 102);
			this.groupBox2.TabIndex = 7;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Destination";
			// 
			// destinationApplicationSecret
			// 
			this.destinationApplicationSecret.Location = new System.Drawing.Point(127, 71);
			this.destinationApplicationSecret.Name = "destinationApplicationSecret";
			this.destinationApplicationSecret.PasswordChar = '*';
			this.destinationApplicationSecret.Size = new System.Drawing.Size(183, 20);
			this.destinationApplicationSecret.TabIndex = 5;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(6, 74);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(96, 13);
			this.label2.TabIndex = 4;
			this.label2.Text = "Application Secret:";
			// 
			// destinationConnect
			// 
			this.destinationConnect.Location = new System.Drawing.Point(343, 38);
			this.destinationConnect.Name = "destinationConnect";
			this.destinationConnect.Size = new System.Drawing.Size(75, 23);
			this.destinationConnect.TabIndex = 4;
			this.destinationConnect.Text = "Connect";
			this.destinationConnect.UseVisualStyleBackColor = true;
			this.destinationConnect.Click += new System.EventHandler(this.destinationConnect_Click);
			// 
			// destinationApplicationId
			// 
			this.destinationApplicationId.Location = new System.Drawing.Point(127, 45);
			this.destinationApplicationId.Name = "destinationApplicationId";
			this.destinationApplicationId.Size = new System.Drawing.Size(183, 20);
			this.destinationApplicationId.TabIndex = 3;
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 48);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(76, 13);
			this.label5.TabIndex = 2;
			this.label5.Text = "Application ID:";
			// 
			// destinationUrl
			// 
			this.destinationUrl.Location = new System.Drawing.Point(127, 19);
			this.destinationUrl.Name = "destinationUrl";
			this.destinationUrl.Size = new System.Drawing.Size(183, 20);
			this.destinationUrl.TabIndex = 1;
			this.destinationUrl.Text = "https://staging-api.kramesstaywell.com";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(6, 22);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(71, 13);
			this.label6.TabIndex = 0;
			this.label6.Text = "Service URL:";
			// 
			// destinationBucketList
			// 
			this.destinationBucketList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.destinationBucketList.CheckBoxes = true;
			this.destinationBucketList.GridLines = true;
			this.destinationBucketList.Location = new System.Drawing.Point(12, 129);
			this.destinationBucketList.Name = "destinationBucketList";
			this.destinationBucketList.Size = new System.Drawing.Size(446, 153);
			this.destinationBucketList.TabIndex = 8;
			this.destinationBucketList.UseCompatibleStateImageBehavior = false;
			this.destinationBucketList.View = System.Windows.Forms.View.List;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1045, 530);
			this.Controls.Add(this.checkBox1);
			this.Controls.Add(this.replicate);
			this.Controls.Add(this.splitContainer1);
			this.Name = "Form1";
			this.Text = "Replicator";
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox sourceUrl;
		private System.Windows.Forms.Button sourceConnect;
		private System.Windows.Forms.ListView sourceBucketList;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.TextBox sourceApplicationSecret;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox sourceApplicationId;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.Button replicate;
		private System.Windows.Forms.CheckBox checkBox1;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.TextBox destinationApplicationSecret;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Button destinationConnect;
		private System.Windows.Forms.TextBox destinationApplicationId;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.TextBox destinationUrl;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.ListView destinationBucketList;
	}
}

