﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows.Forms;
using KswApi.Exceptions;
using KswApi.Interface.Exceptions;
using KswApi.Interface.Objects.Content;

namespace KswApi.Replicator
{
	public partial class Form1 : Form
	{
		private ApiClient _sourceClient;
		private ApiClient _destinationClient;
		public Form1()
		{
			InitializeComponent();
		}

		private void sourceConnect_Click(object sender, EventArgs e)
		{
			_sourceClient = new ApiClient(sourceUrl.Text, sourceApplicationId.Text, sourceApplicationSecret.Text);

			GetBuckets(_sourceClient, sourceBucketList);
		}

		private void destinationConnect_Click(object sender, EventArgs e)
		{
			_destinationClient = new ApiClient(destinationUrl.Text, destinationApplicationId.Text, destinationApplicationSecret.Text);

			GetBuckets(_destinationClient, destinationBucketList);
		}

		private void GetBuckets(ApiClient client, ListView view)
		{
			try
			{
				DoGetBuckets(client, view);
			}
			catch (OAuthException exception)
			{
				MessageBox.Show(exception.ErrorDescription);
			}
			catch (ServiceException exception)
			{
				MessageBox.Show(exception.Message);
			}
		}

		private void DoGetBuckets(ApiClient client, ListView view)
		{
			view.Clear();

			int offset = 0;

			while (true)
			{
				ContentBucketList list = client.Buckets.SearchBuckets(new BucketSearchRequest { Offset = offset, Count = 100 });

				if (list.Items.Count == 0)
					break;

				foreach (ContentBucketResponse response in list.Items)
				{
					view.Items.Add(
						new ListViewItem
						{
							Text = response.Name,
							Tag = response
						});
				}

				offset += 100;
			}
		}

		private void ReplaceBucket(Guid id)
		{

		}

		private void replicate_Click(object sender, EventArgs e)
		{
			try
			{
				Replicate();
			}
			catch (ServiceException exception)
			{
				MessageBox.Show(exception.Message);
			}
		}

		private bool CheckConnection(ApiClient client)
		{
			try
			{
				client.Test.Ping();
				return true;
			}
			catch (OAuthException exception)
			{
				MessageBox.Show(exception.ErrorDescription);
				return false;
			}
		}

		private void Replicate()
		{
			ApiClient sourceClient = new ApiClient(sourceUrl.Text, sourceApplicationId.Text, sourceApplicationSecret.Text);
			ApiClient destinationClient = new ApiClient(destinationUrl.Text, destinationApplicationId.Text, destinationApplicationSecret.Text);

			// check the connections

			if (!CheckConnection(sourceClient) || !CheckConnection(destinationClient))
				return;

			// create the buckets
			foreach (ListViewItem item in sourceBucketList.CheckedItems)
			{
				ContentBucketResponse bucket = (ContentBucketResponse) item.Tag;
				try
				{
					destinationClient.Buckets.GetBucket(bucket.Slug);
					continue;
				}
				catch (ServiceException exception)
				{
					if (exception.StatusCode != HttpStatusCode.NotFound)
						throw;
				}

				destinationClient.Buckets.CreateBucket(new ContentBucketCreateRequest
				                                       {
					                                       Slug = bucket.Slug,
					                                       Constraints = bucket.Constraints,
					                                       Description = bucket.Description,
					                                       LegacyId = bucket.LegacyId,
					                                       Name = bucket.Name,
					                                       Type = bucket.Type,
					                                       OriginId = bucket.OriginId,
					                                       ReadOnly = bucket.ReadOnly,
					                                       Segments =
						                                       bucket.Segments == null
							                                       ? null
							                                       : bucket.Segments.Select(segment =>
							                                                                new ContentBucketSegmentRequest
							                                                                {
								                                                                Name = segment.Name,
								                                                                Required = segment.Required,
								                                                                Slug = segment.Slug
							                                                                }).ToList()
				                                       });
			}

			foreach (ListViewItem item in sourceBucketList.CheckedItems)
			{
				ContentBucketResponse bucket = (ContentBucketResponse)item.Tag;

				int offset = 0;

				while (true)
				{
					ContentBucketResponse destinationBucket = destinationClient.Buckets.GetBucket(bucket.Slug);

					ContentList list = sourceClient.Content.SearchContent(new ContentSearchRequest
													   {
														   Offset = offset,
														   Count = 100,
														   Buckets = new List<string> { bucket.Id.ToString() }
													   });

					offset += 100;

					if (list.Items.Count == 0)
						break;

					foreach (ContentResponse searchResponse in list.Items)
					{
						ContentArticleResponse contentResponse = sourceClient.Content.GetContent(searchResponse.Bucket.Slug, searchResponse.Slug, true, false, null);
						if (destinationClient.Content.GetSlug(bucket.Slug, null, contentResponse.Slug).Value != contentResponse.Slug)
							continue;

						destinationClient.Content.CreateContent(bucket.Slug, new NewContentRequest
																			 {
																				 AgeCategories = contentResponse.AgeCategories,
																				 AlternateTitles = contentResponse.AlternateTitles,
																				 Slug = contentResponse.Slug,
																				 Title = contentResponse.Title,
																				 LegacyId = contentResponse.LegacyId,
																				 PostingDate = contentResponse.PostingDate,
																				 OnlineMedicalReviewers = contentResponse.OnlineEditors,
																				 OnlineEditors = contentResponse.OnlineEditors,
																				 LastReviewedDate = contentResponse.LastReviewedDate,
																				 OnlineOriginatingSources = contentResponse.OnlineOriginatingSources,
																				 Blurb = contentResponse.Blurb,
																				 Authors = contentResponse.Authors,
																				 FleschKincaidReadingLevel = contentResponse.FleschKincaidReadingLevel,
																				 Gender = contentResponse.Gender,
																				 CopyrightId = null,
																				 GunningFogReadingLevel = contentResponse.GunningFogReadingLevel,
																				 InvertedTitle = contentResponse.InvertedTitle,
																				 PrintOriginatingSources = contentResponse.PrintOriginatingSources,
																				 Publish = true,
																				 RecommendedSites = contentResponse.RecommendedSites,
																				 Servicelines = contentResponse.Servicelines == null ? null : contentResponse.Servicelines.Select(serviceLine => new HierarchicalTaxonomyRequest
																				                                                                   {
																					                                                                   Path = serviceLine.Path,
																																					   Slug = serviceLine.Slug,
																																					   Type = serviceLine.Type
																				                                                                   }).ToList(),
																				 Taxonomies = contentResponse.Taxonomies == null ? null : contentResponse.Taxonomies.Select(taxonomy => new ContentTaxonomyListRequest
																																		   {
																																			   Slug = taxonomy.Slug,
																																			   Items = taxonomy.Items
																																		   }).ToList(),
																				 Segments = contentResponse.Segments == null ? null : contentResponse.Segments.Select(segment => new ContentSegmentRequest
																																	   {
																																		   IdOrSlug = segment.Id == null ? null
																																				: destinationBucket.Segments.First(bucketSegment => bucketSegment.Slug.ToLower() == segment.Slug.ToLower()).Id.ToString(),
																																		   Body = segment.Body,
																																		   CustomName = segment.CustomName
																																	   }).ToList()
																			 });
					}
				}
			}
		}
	}
}
