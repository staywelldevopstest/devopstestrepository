﻿namespace KswApi.Net
{
	public enum MessageFormat
	{
		Default,
		Json,
		Xml,
		Form,
		Jsonp
	}
}
