﻿using System.IO;

namespace KswApi.Net
{
	public class ObservingStream : Stream
	{
		private readonly MemoryStream _writeStream;
		private readonly MemoryStream _readStream;

		private readonly Stream _stream;

		public ObservingStream(Stream stream)
		{
			_stream = stream;
			_readStream = new MemoryStream();
			_writeStream = new MemoryStream();
		}

		public MemoryStream ReadStream
		{
			get { return _readStream; }
		}

		public MemoryStream WriteStream
		{
			get { return _writeStream; }
		}

		public override void Flush()
		{
			_stream.Flush();
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			return _stream.Seek(offset, origin);
		}

		public override void SetLength(long value)
		{
			_stream.SetLength(value);
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			int length = _stream.Read(buffer, offset, count);
			_readStream.Write(buffer, offset, length);
			return length;
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			_stream.Write(buffer, offset, count);
			_writeStream.Write(buffer, offset, count);
		}

		public override bool CanRead
		{
			get { return _stream.CanRead; }
		}

		public override bool CanSeek
		{
			get { return _stream.CanSeek; }
		}

		public override bool CanWrite
		{
			get { return _stream.CanWrite; }
		}

		public override long Length
		{
			get { return _stream.Length; }
		}

		public override long Position { get { return _stream.Position; } set { _stream.Position = value; } }
	}
}
