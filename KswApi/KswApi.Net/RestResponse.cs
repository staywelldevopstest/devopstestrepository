﻿using System.Collections.Specialized;
using System.Net;
using System.Text;

namespace KswApi.Net
{
	public class RestResponse
	{
		private byte[] _request;
		private byte[] _response;

		public RestResponse()
		{
			
		}

		public HttpStatusCode StatusCode { get; set; }
		public string ReasonPhrase { get; set; }
		public NameValueCollection Headers { get; set; }
		public string Body { get; set; }

		internal void SetRawRequest(byte[] request)
		{
			_request = request;
		}

		internal void SetRawResponse(byte[] response)
		{
			_response = response;
		}

		public string GetRawRequest()
		{
			if (_request == null)
				return null;

			return Encoding.UTF8.GetString(_request);
		}

		public string GetRawResponse()
		{
			if (_response == null)
				return null;

			return Encoding.UTF8.GetString(_response);
		}
	}
}
