﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Security;
using System.Net.Sockets;
using System.Text;
using System.Web;

namespace KswApi.Net
{
	public class RestClient
	{
		private const int DEFAULT_RECEIVE_TIMEOUT = 0;
		private const int DEFAULT_SEND_TIMEOUT = 0;

		public RestClient()
		{
			ReceiveTimeout = TimeSpan.FromMilliseconds(DEFAULT_RECEIVE_TIMEOUT);
			SendTimeout = TimeSpan.FromMilliseconds(DEFAULT_SEND_TIMEOUT);
		}

		public TimeSpan ReceiveTimeout { get; set; }
		public TimeSpan SendTimeout { get; set; }

		public RestResponse Send(RestRequest request)
		{
			Uri uri = new Uri(request.Uri, UriKind.Absolute);

			TcpClient client = new TcpClient();

			client.ReceiveTimeout = (int)ReceiveTimeout.TotalMilliseconds;
			client.SendTimeout = (int)SendTimeout.TotalMilliseconds;

			client.Connect(uri.Host, GetPort(uri));

			Stream stream = client.GetStream();

			if (IsSecureScheme(uri))
			{
				SslStream ssl = new SslStream(stream, true, (sender, certificate, chain, errors) => true);
				ssl.AuthenticateAsClient(uri.Host);
				stream = ssl;
			}

			bool observe = request.Observe;

			if (observe)
				stream = new ObservingStream(stream);

			WriteRequest(stream, request.Method, uri, request.Headers, request.Body);

			return ProcessResponse(stream);
		}

		int GetContentLength(NameValueCollection headers)
		{
			foreach (string key in headers)
			{
				if (string.Compare(key, "Content-Length", StringComparison.OrdinalIgnoreCase) == 0)
				{
					int length;

					if (!int.TryParse(headers[key], out length))
						throw new HttpException("Invalid response.");

					return length;
				}
			}

			return 0;
		}

		private RestResponse ProcessResponse(Stream stream)
		{
			RestResponse response = new RestResponse();

			ReadProtocolHeader(stream, response);

			ReadHeaders(stream, response);

			int contentLength = GetContentLength(response.Headers);
			
			if (contentLength > 0)
			{
				byte[] body = new byte[contentLength];
				char[] content = new char[contentLength];

				int read = 0;

				while (read < contentLength)
				{
					int count = stream.Read(body, read, contentLength - read);
					if (count == 0)
						break;
					read += count;
				}

				response.Body = Encoding.UTF8.GetString(body, 0, read);
			}

			stream.Close();

			if (stream is ObservingStream)
			{
				ObservingStream observingStream = (ObservingStream)stream;

				response.SetRawRequest(observingStream.WriteStream.ToArray());
				response.SetRawResponse(observingStream.ReadStream.ToArray());
			}

			return response;
		}

		private void ReadHeaders(Stream stream, RestResponse response)
		{
			response.Headers = new NameValueCollection();

			string line = ReadLine(stream);

			if (line == null)
				throw new HttpException("Invalid response.");

			line = ReadLine(stream);

			if (line == null)
				throw new HttpException("Invalid response.");

			if (line.Length == 0)
				return;

			while (true)
			{
				int index = line.IndexOf(':');
				if (index < 0)
					throw new HttpException("Invalid response.");

				string headerName = line.Substring(0, index);
				string headerValue = line.Substring(index + 1);

				while (true)
				{
					line = ReadLine(stream);

					if (line == null)
						throw new HttpException("Invalid response.");

					if (line.Length == 0)
					{
						response.Headers.Add(headerName, headerValue);
						return;
					}

					if (!char.IsWhiteSpace(line[0]))
					{
						response.Headers.Add(headerName, headerValue);
						break;
					}

					headerValue += line;
				}
			}
		}

		private void ReadProtocolHeader(Stream stream, RestResponse response)
		{
			string header = ReadLine(stream);

			if (header == null)
				throw new HttpException("Invalid response.");

			int index = header.IndexOf(' ');
			if (index < 0)
				throw new HttpException("Invalid response.");

			if (header.Substring(0, index) != "HTTP/1.1")
				throw new HttpException("Invalid response.");

			int index2 = header.IndexOf(' ', index + 1);

			if (index2 < 0)
				throw new HttpException("Invalid response.");

			int statusCode;
			if (!int.TryParse(header.Substring(index + 1, index2 - index - 1), out statusCode))
				throw new HttpException("Invalid response.");

			response.StatusCode = (HttpStatusCode) statusCode;

			response.ReasonPhrase = header.Substring(index2 + 1);
		}

		private string ReadLine(Stream stream)
		{
			MemoryStream ms = new MemoryStream();
			while (true)
			{
				int b = stream.ReadByte();
				if (b == '\r')
					continue;
				if (b == -1 || b == '\n')
					return Encoding.UTF8.GetString(ms.ToArray());

				ms.WriteByte((byte) b);
			}
		}

		int GetPort(Uri uri)
		{
			if (!uri.IsDefaultPort)
				return uri.Port;
			switch (uri.Scheme.ToLower())
			{
				case "http":
					return 80;
				case "https":
					return 443;
				default:
					throw new ArgumentException("Unknown URI scheme.");
			}
		}

		bool IsSecureScheme(Uri uri)
		{
			switch (uri.Scheme.ToLower())
			{
				case "http":
					return false;
				case "https":
					return true;
				default:
					throw new ArgumentException("Unknown URI scheme.");
			}
		}

		private void WriteRequest(Stream stream, string method, Uri uri, NameValueCollection headers, string body)
		{
			StreamWriter writer = new StreamWriter(stream);

			writer.Write(method);
			writer.Write(' ');
			writer.Write(uri.PathAndQuery);
			writer.WriteLine(" HTTP/1.1");

			if (headers == null)
				headers = new NameValueCollection();

			headers.Remove("Host");
			headers.Add("Host", uri.Host);

			if (body != null)
			{
				headers["Content-Length"] = Encoding.UTF8.GetByteCount(body).ToString();
			}
			else if (method == HttpMethod.Post.Method || method == HttpMethod.Put.Method)
			{
				headers["Content-Length"] = "0";
			}

			foreach (string header in headers)
			{
				writer.Write(header);
				writer.Write(": ");
				writer.WriteLine(headers[header]);
			}

			writer.WriteLine();

			if (body != null)
				writer.Write(body);

			writer.Flush();
		}

	}
}
