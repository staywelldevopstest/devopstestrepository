﻿using System.Collections.Specialized;

namespace KswApi.Net
{
	public class RestRequest
	{
		public string Method { get; set; }
		public string Uri { get; set; }
		public NameValueCollection Headers { get; set; }
		public bool Observe { get; set; }
		public string Body { get; set; }
	}
}
