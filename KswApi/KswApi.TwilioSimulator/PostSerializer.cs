﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Text;
using System.Web;

namespace KswApi.TwilioSimulator
{
	public static class PostSerializer
	{
		public static NameValueCollection GetPostVariables(Stream stream)
		{
			StreamReader reader = new StreamReader(stream);
			string post = reader.ReadToEnd();
			string[] parameters = post.Split('&');
			NameValueCollection results = new NameValueCollection();
			foreach (string parameter in parameters)
			{
				string name = parameter;
				string value = null;
				int offset = parameter.IndexOf('=');

				if (offset >= 0)
				{
					name = parameter.Substring(0, offset);
					value = parameter.Substring(offset + 1);
					name = Uri.UnescapeDataString(name);
					value = Uri.UnescapeDataString(value);
				}

				results[name] = value;
			}

			return results;
		}

		public static byte[] Serialize(NameValueCollection parameters)
		{
			MemoryStream stream = new MemoryStream();
			StreamWriter writer = new StreamWriter(stream, Encoding.UTF8);

			bool first = true;
			foreach (string key in parameters)
			{
				if (first)
					first = false;
				else
					writer.Write('&');

				writer.Write(HttpUtility.UrlEncode(key));
				writer.Write('=');
				writer.Write(HttpUtility.UrlEncode(parameters[key]));
			}

			writer.Close();
			byte[] data = stream.ToArray();
			return data;
		}
	}
}
