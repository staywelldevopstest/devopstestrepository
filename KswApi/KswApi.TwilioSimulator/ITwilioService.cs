﻿using System.IO;
using System.ServiceModel;
using System.ServiceModel.Web;

namespace KswApi.TwilioSimulator
{
	[ServiceContract]
	public interface ITwilioService
	{
		[OperationContract]
		[WebInvoke]
		void SendTwilio(Stream stream);
	}
}
