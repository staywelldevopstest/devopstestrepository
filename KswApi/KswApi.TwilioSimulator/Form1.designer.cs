﻿namespace KswApi.TwilioSimulator
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.serviceTextBox = new System.Windows.Forms.TextBox();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.sendSmsButton = new System.Windows.Forms.Button();
			this.bodyTextBox = new System.Windows.Forms.TextBox();
			this.toTextBox = new System.Windows.Forms.TextBox();
			this.fromTextBox = new System.Windows.Forms.TextBox();
			this.accountIdTextBox = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.smsIdTextBox = new System.Windows.Forms.TextBox();
			this.tabControl1 = new System.Windows.Forms.TabControl();
			this.resultTextBox = new System.Windows.Forms.TextBox();
			this.twilioAddress = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.startButton = new System.Windows.Forms.Button();
			this.tabPage1.SuspendLayout();
			this.tabControl1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(102, 13);
			this.label1.TabIndex = 1;
			this.label1.Text = "Dispatcher Address:";
			// 
			// serviceTextBox
			// 
			this.serviceTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.serviceTextBox.Location = new System.Drawing.Point(120, 12);
			this.serviceTextBox.Name = "serviceTextBox";
			this.serviceTextBox.Size = new System.Drawing.Size(402, 20);
			this.serviceTextBox.TabIndex = 2;
			this.serviceTextBox.Text = "http://localhost/KswApi/SmsGateway/EndPoints/Twilio.xml";
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.sendSmsButton);
			this.tabPage1.Controls.Add(this.bodyTextBox);
			this.tabPage1.Controls.Add(this.toTextBox);
			this.tabPage1.Controls.Add(this.fromTextBox);
			this.tabPage1.Controls.Add(this.accountIdTextBox);
			this.tabPage1.Controls.Add(this.label6);
			this.tabPage1.Controls.Add(this.label5);
			this.tabPage1.Controls.Add(this.label4);
			this.tabPage1.Controls.Add(this.label3);
			this.tabPage1.Controls.Add(this.label2);
			this.tabPage1.Controls.Add(this.smsIdTextBox);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(502, 192);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "SMS";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// sendSmsButton
			// 
			this.sendSmsButton.Location = new System.Drawing.Point(112, 157);
			this.sendSmsButton.Name = "sendSmsButton";
			this.sendSmsButton.Size = new System.Drawing.Size(75, 23);
			this.sendSmsButton.TabIndex = 10;
			this.sendSmsButton.Text = "Send";
			this.sendSmsButton.UseVisualStyleBackColor = true;
			this.sendSmsButton.Click += new System.EventHandler(this.sendSmsButton_Click);
			// 
			// bodyTextBox
			// 
			this.bodyTextBox.Location = new System.Drawing.Point(112, 131);
			this.bodyTextBox.Name = "bodyTextBox";
			this.bodyTextBox.Size = new System.Drawing.Size(253, 20);
			this.bodyTextBox.TabIndex = 9;
			this.bodyTextBox.Text = "KEWORD";
			// 
			// toTextBox
			// 
			this.toTextBox.Location = new System.Drawing.Point(112, 106);
			this.toTextBox.Name = "toTextBox";
			this.toTextBox.Size = new System.Drawing.Size(253, 20);
			this.toTextBox.TabIndex = 8;
			this.toTextBox.Text = "98517";
			// 
			// fromTextBox
			// 
			this.fromTextBox.Location = new System.Drawing.Point(112, 80);
			this.fromTextBox.Name = "fromTextBox";
			this.fromTextBox.Size = new System.Drawing.Size(253, 20);
			this.fromTextBox.TabIndex = 7;
			this.fromTextBox.Text = "1234";
			// 
			// accountIdTextBox
			// 
			this.accountIdTextBox.Location = new System.Drawing.Point(112, 54);
			this.accountIdTextBox.Name = "accountIdTextBox";
			this.accountIdTextBox.Size = new System.Drawing.Size(253, 20);
			this.accountIdTextBox.TabIndex = 6;
			this.accountIdTextBox.Text = "12341234";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(75, 134);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(34, 13);
			this.label6.TabIndex = 5;
			this.label6.Text = "Body:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(86, 109);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(23, 13);
			this.label5.TabIndex = 4;
			this.label5.Text = "To:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(73, 83);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(33, 13);
			this.label4.TabIndex = 3;
			this.label4.Text = "From:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(44, 57);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(62, 13);
			this.label3.TabIndex = 2;
			this.label3.Text = "Account Id:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(44, 27);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(58, 13);
			this.label2.TabIndex = 1;
			this.label2.Text = "Unique ID:";
			// 
			// smsIdTextBox
			// 
			this.smsIdTextBox.Location = new System.Drawing.Point(112, 24);
			this.smsIdTextBox.Name = "smsIdTextBox";
			this.smsIdTextBox.Size = new System.Drawing.Size(253, 20);
			this.smsIdTextBox.TabIndex = 0;
			this.smsIdTextBox.Text = "123456789";
			// 
			// tabControl1
			// 
			this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tabControl1.Controls.Add(this.tabPage1);
			this.tabControl1.Location = new System.Drawing.Point(12, 114);
			this.tabControl1.Name = "tabControl1";
			this.tabControl1.SelectedIndex = 0;
			this.tabControl1.Size = new System.Drawing.Size(510, 218);
			this.tabControl1.TabIndex = 3;
			// 
			// resultTextBox
			// 
			this.resultTextBox.Location = new System.Drawing.Point(16, 338);
			this.resultTextBox.Multiline = true;
			this.resultTextBox.Name = "resultTextBox";
			this.resultTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.resultTextBox.Size = new System.Drawing.Size(502, 171);
			this.resultTextBox.TabIndex = 4;
			this.resultTextBox.WordWrap = false;
			// 
			// twilioAddress
			// 
			this.twilioAddress.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.twilioAddress.Location = new System.Drawing.Point(120, 38);
			this.twilioAddress.Name = "twilioAddress";
			this.twilioAddress.Size = new System.Drawing.Size(402, 20);
			this.twilioAddress.TabIndex = 6;
			this.twilioAddress.Text = "http://localhost:8090";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(12, 41);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(78, 13);
			this.label7.TabIndex = 5;
			this.label7.Text = "Twilio Address:";
			// 
			// startButton
			// 
			this.startButton.Location = new System.Drawing.Point(120, 64);
			this.startButton.Name = "startButton";
			this.startButton.Size = new System.Drawing.Size(75, 23);
			this.startButton.TabIndex = 7;
			this.startButton.Text = "Start";
			this.startButton.UseVisualStyleBackColor = true;
			this.startButton.Click += new System.EventHandler(this.startButton_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(534, 521);
			this.Controls.Add(this.startButton);
			this.Controls.Add(this.twilioAddress);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.resultTextBox);
			this.Controls.Add(this.tabControl1);
			this.Controls.Add(this.serviceTextBox);
			this.Controls.Add(this.label1);
			this.Name = "Form1";
			this.Text = "Twilio Simulator";
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.tabControl1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox serviceTextBox;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabControl tabControl1;
		private System.Windows.Forms.TextBox smsIdTextBox;
		private System.Windows.Forms.Button sendSmsButton;
		private System.Windows.Forms.TextBox bodyTextBox;
		private System.Windows.Forms.TextBox toTextBox;
		private System.Windows.Forms.TextBox fromTextBox;
		private System.Windows.Forms.TextBox accountIdTextBox;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.TextBox resultTextBox;
		private System.Windows.Forms.TextBox twilioAddress;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Button startButton;
	}
}

