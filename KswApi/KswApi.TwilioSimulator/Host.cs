﻿using System;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.ServiceModel.Web;
using System.Threading;

namespace KswApi.TwilioSimulator
{
	public class Host
	{
		private readonly ManualResetEvent _stop = new ManualResetEvent(false);

		public void Start(Uri uri)
		{
			Thread thread = new Thread(HostThread);
			thread.Start(uri);
		}

		private void HostThread(object obj)
		{
			Uri uri = obj as Uri;
			if (uri == null)
				return;

			try
			{
				RunHost(uri);
			}
			catch (Exception ex)
			{
				Program.Form.ReportError(ex.Message);
			}
		}

		private void RunHost(Uri uri)
		{
			WebServiceHost host = null;
			try
			{
				host = new WebServiceHost(typeof(TwilioService), uri);
				//host.Faulted += OnHostFaulted;
				ServiceMetadataBehavior behavior = new ServiceMetadataBehavior();
				behavior.HttpGetEnabled = true;
				behavior.MetadataExporter.PolicyVersion = PolicyVersion.Policy15;
				host.Description.Behaviors.Add(behavior);
				host.Open();
				_stop.WaitOne();
			}
			catch (TimeoutException)
			{
				if (host != null)
					host.Abort();
				throw;
			}
			catch (CommunicationException)
			{
				if (host != null)
					host.Abort();
				throw;
			}
			finally
			{
				if (host != null)
					host.Close();
			}
		}

		public void Stop()
		{
			_stop.Set();
		}
	}
}
