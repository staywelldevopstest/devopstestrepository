﻿using System.Collections.Specialized;

namespace KswApi.TwilioSimulator
{
	public class TwilioService : ITwilioService
	{
		public void SendTwilio(System.IO.Stream stream)
		{
			NameValueCollection parameters = PostSerializer.GetPostVariables(stream);

			string to = parameters["To"],
				from = parameters["From"],
				body = parameters["Body"];
			string result = string.Format("-> [To: {0}] [From: {1}] [Body: {2}]", to, from, body);
			
			Program.Form.ReportResult(result);
		}
	}
}
