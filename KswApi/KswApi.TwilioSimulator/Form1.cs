﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Windows.Forms;
using System.Net;
using System.Xml.Linq;

namespace KswApi.TwilioSimulator
{
	public partial class Form1 : Form
	{
		private readonly Host _host = new Host();
		private bool _running = false;

		public Form1()
		{
			InitializeComponent();
		}

		private void sendSmsButton_Click(object sender, EventArgs e)
		{
			try
			{
				SendMessage();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void SendMessage()
		{
			NameValueCollection parameters = new NameValueCollection();

			ReportResult(string.Format("<- [SmsSid: {0}] [accountSid: {1}] [from: {2}] [to: {3}] [body: {4}]", smsIdTextBox.Text, accountIdTextBox.Text, fromTextBox.Text, toTextBox.Text, bodyTextBox.Text));

			parameters.Add("SmsSid", smsIdTextBox.Text);
			parameters.Add("accountSid", accountIdTextBox.Text);
			parameters.Add("from", fromTextBox.Text);
			parameters.Add("to", toTextBox.Text);
			parameters.Add("body", bodyTextBox.Text);

			byte[] request = GetPostParameters(parameters);

			WebClient client = new WebClient();
			
			client.Headers.Add("Content-Type", "application/x-www-form-urlencoded");

			byte[] response = client.UploadData(serviceTextBox.Text, request);

			MemoryStream stream = new MemoryStream(response);

			XDocument doc = XDocument.Load(stream);
			XElement body = doc.Element("Response");
			XElement sms = body.Element("Sms");

			resultTextBox.AppendText("->");
			
			if (sms != null)
				resultTextBox.AppendText(sms.Value);

			resultTextBox.AppendText("\r\n");
		}

		private byte[] GetPostParameters(NameValueCollection parameters)
		{
			StringBuilder builder = new StringBuilder();

			bool first = true;
			foreach (string key in parameters)
			{
				if (first)
					first = false;
				else
					builder.Append('&');

				builder.Append(HttpUtility.UrlEncode(key));
				builder.Append('=');
				builder.Append(HttpUtility.UrlEncode(parameters[key]));
			}

			return Encoding.UTF8.GetBytes(builder.ToString());
		}

		public void ReportError(string message)
		{
			if (InvokeRequired)
			{
				Invoke(new Action<string>(ReportError), message);
				return;
			}

			MessageBox.Show(message);
		}

		public void ReportResult(string message)
		{
			if (InvokeRequired)
			{
				Invoke(new Action<string>(ReportResult), message);
				return;
			}

			resultTextBox.AppendText(message);
			resultTextBox.AppendText("\r\n");
		}

		private void startButton_Click(object sender, EventArgs e)
		{
			try
			{
				if (_running)
					StopService();
				else
					StartService();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void StartService()
		{
			Uri uri;
			if (!Uri.TryCreate(twilioAddress.Text, UriKind.Absolute, out uri))
			{
				MessageBox.Show("The address is not valid.");
				return;
			}

			_host.Start(uri);

			_running = true;
			startButton.Text = "Stop";
			twilioAddress.ReadOnly = true;
		}

		private void StopService()
		{
			_host.Stop();

			_running = false;
			startButton.Text = "Start";
			twilioAddress.ReadOnly = false;
		}
	}
}
