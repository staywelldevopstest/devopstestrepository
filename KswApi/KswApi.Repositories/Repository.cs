﻿using KswApi.Repositories.Enums;
using KswApi.Repositories.Framework;
using KswApi.Repositories.Groups;
using System;
using KswApi.Repositories.Interfaces;

namespace KswApi.Repositories
{
    public class Repository : RepositoryRoot
    {
        private readonly Lazy<AdministrationGroup> _administration;
        private readonly Lazy<AuthorizationGroup> _authorization;
        private readonly Lazy<SmsGatewayGroup> _smsGateway;
        private readonly Lazy<ContentGroup> _content;
        private readonly Lazy<TasksGroup> _taskStatus;
		private readonly Lazy<ConfigurationGroup> _configuration;

        public Repository()
            : base(RepositoryType.Main)
        {
            _administration = new Lazy<AdministrationGroup>(() => new AdministrationGroup(this));
            _authorization = new Lazy<AuthorizationGroup>(() => new AuthorizationGroup(this));
            _smsGateway = new Lazy<SmsGatewayGroup>(() => new SmsGatewayGroup(this));
            _content = new Lazy<ContentGroup>(() => new ContentGroup(this));
            _taskStatus = new Lazy<TasksGroup>(() => new TasksGroup(this));
			_configuration = new Lazy<ConfigurationGroup>(() => new ConfigurationGroup(this));
        }

        public AdministrationGroup Administration { get { return _administration.Value; } }
        public AuthorizationGroup Authorization { get { return _authorization.Value; } }
        public SmsGatewayGroup SmsGateway { get { return _smsGateway.Value; } }
        public ContentGroup Content { get { return _content.Value; } }
        public TasksGroup Tasks { get { return _taskStatus.Value; } }
		public ConfigurationGroup Configuration { get { return _configuration.Value; } }
    }
}
