﻿namespace KswApi.Repositories.Enums
{
    public enum RepositoryType
    {
        Main,
        Test,
        Log,
        Client,
        SmsMessageLog
    }
}
