﻿namespace KswApi.Repositories.Enums
{
	public enum SortDirection
	{
		Ascending,
		Descending
	}
}
