﻿using KswApi.Repositories.Framework;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Interfaces.Tasks;

namespace KswApi.Repositories.Groups
{
    public class TasksGroup : RepositoryGroup
    {
        internal TasksGroup(RepositoryGroup parent)
            : base(parent)
        {
        }

        public ITaskStatusRepository TaskStatus { get { return Get<ITaskStatusRepository>(); } }
		public IExecutingTaskRepository ExecutingTasks { get { return Get<IExecutingTaskRepository>(); } }
    }
}
