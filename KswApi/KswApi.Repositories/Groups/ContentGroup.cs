﻿using KswApi.Repositories.Constants;
using KswApi.Repositories.Framework;
using KswApi.Repositories.Interfaces;
using KswApi.Repositories.Interfaces.Content;

namespace KswApi.Repositories.Groups
{
	public class ContentGroup : RepositoryGroup
	{
		public ContentGroup(RepositoryGroup parent)
			: base(parent)
		{
		}

		public IContentModuleRepository ContentModule { get { return Get<IContentModuleRepository>(); } }
		public IContentVersionRepository Versions { get { return Get<IContentVersionRepository>(); } }
		public IContentPublishedRepository Published { get { return Get<IContentPublishedRepository>(); } }
		public IContentCoreRepository Cores { get { return Get<IContentCoreRepository>(); } }
		public IContentBucketRepository Buckets { get { return Get<IContentBucketRepository>(); } }
		public IContentOriginRepository Origins { get { return Get<IContentOriginRepository>(); } }
		public IContentSegmentRepository Segments { get { return Get<IContentSegmentRepository>(); } }
		public IContentRevisionRepository Revisions { get { return Get<IContentRevisionRepository>(); } }
		public IContentHistoryRepository History { get { return Get<IContentHistoryRepository>(); } }
		public IImageDetailRepository ImageDetails { get { return Get<IImageDetailRepository>(); } }
		public ITaxonomyTypeRepository TaxonomyTypes { get { return Get<ITaxonomyTypeRepository>(); } }
		public IHierarchicalTaxonomyRepository HierarchicalTaxonomy { get { return Get<IHierarchicalTaxonomyRepository>(); } }
		public IBinaryStore ImageStore { get { return GetStore(Constant.IMAGE_STORE_NAME); } }
        public IBinaryStore CollectionImageStore { get { return GetStore(Constant.COLLECTION_IMAGE_STORE_NAME); } }
		public IThreeMItemRepository ThreeMItems { get { return Get<IThreeMItemRepository>(); } }
        public ICollectionRepository Collections { get { return Get<ICollectionRepository>(); } }
		public ITopicRepository Topics { get { return Get<ITopicRepository>(); } }
        public ICollectionRevisionRepository CollectionRevisions { get { return Get<ICollectionRevisionRepository>(); } }
		public ICollectionStructureRepository CollectionStructures { get { return Get<ICollectionStructureRepository>(); } }
		public ILicensedCollectionRepository LicensedCollections { get { return Get<ILicensedCollectionRepository>(); } }
	}
}
