﻿using KswApi.Repositories.Framework;
using KswApi.Repositories.Interfaces;

namespace KswApi.Repositories.Groups
{
    public class SmsGatewayGroup : RepositoryGroup
    {
        public SmsGatewayGroup(RepositoryGroup parent)
            : base(parent)
        {
        }

        public ISmsNumberDataRepository Numbers { get { return Get<ISmsNumberDataRepository>(); } }
        public ISmsKeywordDataRepository Keywords { get { return Get<ISmsKeywordDataRepository>(); } }
        public ISmsKeywordDataLogRepository KeywordDataLogs { get { return Get<ISmsKeywordDataLogRepository>(); } }
        public ISmsAvailableNumberRepository AvailableNumbers { get { return Get<ISmsAvailableNumberRepository>(); } }
        public ISmsSessionRepository Sessions { get { return Get<ISmsSessionRepository>(); } }
        public ISmsSubscriberRepository Subscribers { get { return Get<ISmsSubscriberRepository>(); } }
        public ISmsMessageLogRepository MessageLogs { get { return Get<ISmsMessageLogRepository>(); } }
        public ISmsNumberDataLogRepository NumberDataLogs { get { return Get<ISmsNumberDataLogRepository>(); } }
        public ISmsBillingReportDataRepository BillingReportData { get { return Get<ISmsBillingReportDataRepository>(); } }
    }
}
