﻿using KswApi.Repositories.Framework;
using KswApi.Repositories.Interfaces;

namespace KswApi.Repositories.Groups
{
	public class ConfigurationGroup : RepositoryGroup
	{
		internal ConfigurationGroup(RepositoryGroup parent)
			: base(parent)
		{
		}

		public IConfigurationValueRepository ConfigurationValues { get { return Get<IConfigurationValueRepository>(); } }
	}
}
