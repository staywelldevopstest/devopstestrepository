﻿using KswApi.Repositories.Framework;
using KswApi.Repositories.Interfaces;

namespace KswApi.Repositories.Groups
{
    public class AdministrationGroup : RepositoryGroup
    {
        internal AdministrationGroup(RepositoryGroup parent)
            : base(parent)
        {
        }

        public IRoleRepository Roles { get { return Get<IRoleRepository>(); } }

        public IApplicationRepository Applications { get { return Get<IApplicationRepository>(); } }

        public IClientRepository Clients { get { return Get<IClientRepository>(); } }

        public IClientUserRepository ClientUsers { get { return Get<IClientUserRepository>(); } }

        public IDomainUserRepository DomainUsers { get { return Get<IDomainUserRepository>(); } }

		public ICopyrightRepository Copyrights { get { return Get<ICopyrightRepository>(); } }

		public ILoginAttemptRepository LoginAttempts { get { return Get<ILoginAttemptRepository>(); } }

		public IPasswordTokenRepository PasswordTokens { get { return Get<IPasswordTokenRepository>(); } }

        public ILicenseRepository Licenses { get { return Get<ILicenseRepository>(); } }
    }
}
