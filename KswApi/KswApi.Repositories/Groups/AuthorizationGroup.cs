﻿using KswApi.Repositories.Framework;
using KswApi.Repositories.Interfaces;

namespace KswApi.Repositories.Groups
{
	public class AuthorizationGroup : RepositoryGroup
	{
		internal AuthorizationGroup(RepositoryGroup parent)
			: base(parent)
		{
		}

		public IAuthorizationGrantRepository AuthorizationGrants { get { return Get<IAuthorizationGrantRepository>(); } }
		public IAccessTokenRepository AccessTokens { get { return Get<IAccessTokenRepository>(); } }
		public IRefreshTokenRepository RefreshTokens { get { return Get<IRefreshTokenRepository>(); } }
	}
}
