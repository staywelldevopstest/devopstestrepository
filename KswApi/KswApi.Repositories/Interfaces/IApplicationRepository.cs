﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using KswApi.Interface.Objects;
using KswApi.Repositories.Objects;

namespace KswApi.Repositories.Interfaces
{
	public interface IApplicationRepository
	{
		void Add(Application client);
		void Update(Application client);
		Application GetById(Guid id);

		IEnumerable<Application> GetApplications(int offset, int count, SortOption<Application> sort);
		IEnumerable<Application> GetApplications(int offset, int count, Expression<Func<Application, bool>> expression, SortOption<Application> sortOption);
		int Count();
		int Count(Expression<Func<Application, bool>> expression);
		void Delete(Application application);
		void DeleteByLicenseId(Guid licenseId);
	}
}
