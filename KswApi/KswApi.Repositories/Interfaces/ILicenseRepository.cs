﻿using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;

namespace KswApi.Repositories.Interfaces
{
	public interface ILicenseRepository
	{
		void Add(License license);
		void Update(License license);
		void Delete(License license);
		License GetById(Guid id);
		License GetByApplicationId(Guid id);
		IEnumerable<License> GetByStatus(LicenseStatus status, int offset, int count, SortOption<License> sort);
		IEnumerable<License> GetByQueryAndStatus(string query, LicenseStatus status, int offset, int count, SortOption<License> sortOption);
		IEnumerable<License> GetByClientIdAndStatus(Guid clientId, LicenseStatus status, SortOption<License> sortOption);
		IEnumerable<License> GetByClientIdQueryAndStatus(Guid clientId, string query, LicenseStatus status, int offset, int count, SortOption<License> sortOption);
		IEnumerable<License> GetByClientIdCreatedDateAndStatus(Guid clientId, DateTime createdDate, LicenseStatus status);
		int CountByStatus(LicenseStatus status);
		int CountByQueryAndStatus(string query, LicenseStatus status);
		int CountByClientIdAndStatus(Guid clientId, LicenseStatus status);
		int CountByClientIdQueryAndStatus(Guid clientId, string query, LicenseStatus status);
	}
}
