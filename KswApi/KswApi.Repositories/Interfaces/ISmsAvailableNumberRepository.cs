﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.SmsGateway;

namespace KswApi.Repositories.Interfaces
{
	public interface ISmsAvailableNumberRepository
	{
		void Add(SmsSharedNumber number);
		void Update(SmsSharedNumber number);
		void Delete(SmsSharedNumber number);
		SmsSharedNumber GetByNumber(string number);
		IEnumerable<SmsSharedNumber> Get(Expression<Func<SmsSharedNumber, bool>> expression);
	}
}
