﻿using KswApi.Poco.Administration;
using System;
using System.Collections.Generic;

namespace KswApi.Repositories.Interfaces
{
    public interface IDomainUserRepository
    {
        void Add(DomainUser domainUser);
        void Update(DomainUser domainUser);
        //void Delete(Guid id);
        DomainUser GetById(Guid id);
        IEnumerable<DomainUser> GetByIds(List<Guid> ids);
    }
}
