﻿using KswApi.Repositories.Enums;

namespace KswApi.Repositories.Interfaces
{
	public interface IRepositoryFactory
	{
		IRepository GetRepository(RepositoryType type);
	}
}
