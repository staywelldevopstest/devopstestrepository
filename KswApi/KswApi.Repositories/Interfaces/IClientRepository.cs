﻿using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;

namespace KswApi.Repositories.Interfaces
{
    public interface IClientRepository
    {
        void Add(Client client);
        void Update(Client client);
        bool Contains(string clientName, ClientStatus status);
        Client GetById(Guid id);

        IEnumerable<Client> GetByStatus(ClientStatus status, int offset, int count, SortOption<Client> sort);
        IEnumerable<Client> GetByDateAdded(DateTime dateAdded);
        IEnumerable<Client> GetByDateAddedAndStatus(DateTime dateAdded, ClientStatus status);
        IEnumerable<Client> GetByQueryAndStatus(string query, ClientStatus status, int offset, int count, SortOption<Client> sort);

        int Count(ClientStatus status);
        int Count(string query, ClientStatus status);
    }
}
