﻿using KswApi.Poco.Sms;
using KswApi.Repositories.Objects;
using System;

namespace KswApi.Repositories.Interfaces
{
    public interface ISmsKeywordDataLogRepository
    {
        void Add(SmsKeywordDataLog keywordDataLog);
        void Update(SmsKeywordDataLog keywordDataLog);

        SmsKeywordDataLog GetById(Guid id);

        SmsKeywordDataLog GetByKeywordId(Guid keywordId);

        int Count(Guid licenseId, string number, DateTime startDate, DateTime endDate);
    }
}