﻿using KswApi.Poco.Tasks;

namespace KswApi.Repositories.Interfaces.Tasks
{
	public interface IExecutingTaskRepository
	{
		void Add<TData>(ExecutingTask<TData> task);
		ExecutingTask<TData> GetById<TData>(string id);
		bool Acquire<TData>(string id);
		void Update<TData>(string id, TData data);
		void Release(string id);
		void Delete(string id);
	}
}
