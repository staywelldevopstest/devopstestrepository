﻿using KswApi.Interface.Enums;
using KswApi.Poco.Sms;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;

namespace KswApi.Repositories.Interfaces
{
    public interface ISmsNumberDataRepository
    {
        void Add(SmsNumberData number);
        void Update(SmsNumberData number);
        void Delete(SmsNumberData number);
        IEnumerable<SmsNumberData> GetByLicenseId(Guid licenseId);
        SmsNumberData GetByNumberAndLicenseId(string number, Guid licenseId);
        SmsNumberData GetById(Guid id);
        IEnumerable<SmsNumberData> GetByNumber(string number);
        SmsNumberData GetOneByNumber(string number);
        IEnumerable<SmsNumberData> GetByNumberAndRouteType(string number, SmsNumberRouteType routeType);
    }
}
