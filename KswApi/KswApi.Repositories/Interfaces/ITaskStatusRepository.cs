﻿using KswApi.Poco.Tasks;
using KswApi.Repositories.Objects;

namespace KswApi.Repositories.Interfaces
{
    public interface ITaskStatusRepository
    {
        void Add(TaskStatusData statusData);
    }
}
