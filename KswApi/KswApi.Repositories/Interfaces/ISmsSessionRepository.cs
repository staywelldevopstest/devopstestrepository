﻿using KswApi.Poco.Sms;
using KswApi.Repositories.Enums;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;

namespace KswApi.Repositories.Interfaces
{
    public interface ISmsSessionRepository
    {
        void Add(SmsSession session);
        void Update(SmsSession session);
        void Delete(SmsSession session);
        void Delete(Guid id);

        SmsSession GetById(Guid id);

        IEnumerable<SmsSession> GetByNumbersAndState(string subscriberNumber, string serviceNumber, SmsSessionState state);
        IEnumerable<SmsSession> GetByNumbersAndKeywordAndState(string subscriberNumber, string serviceNumber, string keyword, SmsSessionState state);
        IEnumerable<SmsSession> GetByServiceNumberAndKeywordAndState(string serviceNumber, string keyword, SmsSessionState state);
		IEnumerable<SmsSession> GetByLastRequest(DateTime expiration, int count);
    }
}
