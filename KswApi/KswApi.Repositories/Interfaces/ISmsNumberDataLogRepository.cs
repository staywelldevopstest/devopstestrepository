﻿using KswApi.Poco.Sms;
using KswApi.Repositories.Objects;
using System;

namespace KswApi.Repositories.Interfaces
{
    public interface ISmsNumberDataLogRepository
    {
        void Add(SmsNumberDataLog numberDataLog);
        void Update(SmsNumberDataLog numberDataLog);

        SmsNumberDataLog Get(Guid numberId);

        int Count(Guid licenseId, Guid numberId, DateTime startDate, DateTime endDate);
        int Count(Guid licenseId, DateTime startDate, DateTime endDate);
    }
}
