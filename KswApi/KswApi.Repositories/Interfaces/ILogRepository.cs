﻿using KswApi.Interface.Objects;
using KswApi.Poco.Logging;
using KswApi.Repositories.Enums;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace KswApi.Repositories.Interfaces
{
    public interface ILogRepository
    {
        void Add(Log log);
        Log Get(Guid logId);

        IEnumerable<Log> Get(int offset, int count, LogSortOption sort);
        IEnumerable<Log> Get(int offset, int count, Expression<Func<Log, bool>> expression, LogSortOption sort);
        IEnumerable<Log> Get(int offset, int count, bool includeWarnings, bool includeErrors, bool includeInfo, DateTime fromDate, DateTime toDate, string clientId, string licenseId, LogSortOption sort);

        int Count();
        int Count(Expression<Func<Log, bool>> expression);
    }
}
