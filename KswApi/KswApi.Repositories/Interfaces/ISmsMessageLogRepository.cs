﻿using KswApi.Poco.Sms;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;

namespace KswApi.Repositories.Interfaces
{
    public interface ISmsMessageLogRepository
    {
        void Add(SmsMessageLog messageLog);
        SmsMessageLog Get(Guid messageLogId);

        IEnumerable<SmsMessageLog> GetByLicense(Guid licenseId);
    }
}
