﻿using KswApi.Common.Objects;
using System;

namespace KswApi.Repositories.Interfaces
{
    public interface IAccessTokenRepository
    {
        AccessToken GetByToken(string token);

        void Add(AccessToken token);
        void Delete(AccessToken token);
        void Delete(Guid id);
	    void DeleteByUserId(Guid id);
		void DeleteByExpiration(DateTime expiration);
	    void Update(AccessToken token);
    }
}
