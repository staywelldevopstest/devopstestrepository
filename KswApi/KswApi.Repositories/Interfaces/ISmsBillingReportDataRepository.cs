﻿using KswApi.Interface.Enums;
using KswApi.Poco.Sms;
using KswApi.Repositories.Objects;
using System;

namespace KswApi.Repositories.Interfaces
{
    public interface ISmsBillingReportDataRepository
    {
        void Add(SmsBillingReportLicenseData reportData);
        void Update(SmsBillingReportLicenseData reportData);
        SmsBillingReportLicenseData Get(Guid licenseId, Month month, int year);
    }
}
