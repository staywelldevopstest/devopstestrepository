﻿using KswApi.Poco.Content;
using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;

namespace KswApi.Repositories.Interfaces
{
	public interface ICopyrightRepository
	{
		void Add(Copyright copyright);
		void Update(Copyright copyright);
		void Delete(Guid id);
		Copyright GetCopyright(Guid id);

		IEnumerable<Copyright> GetCopyrights(int offset, int count, string query, List<Guid> exclude);
		IEnumerable<Copyright> GetCopyrights(int offset, int count, List<Guid> exclude);

		int Count(List<Guid> exclude);
		int Count(string query, List<Guid> exclude);
	}
}
