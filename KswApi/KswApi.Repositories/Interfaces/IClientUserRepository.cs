﻿using KswApi.Repositories.Objects;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace KswApi.Repositories.Interfaces
{
    public interface IClientUserRepository
    {
        void Add(ClientUser clientUser);
        void Update(ClientUser clientUser);
        void Delete(Guid id);
        ClientUser GetUser(Guid id);
        ClientUser GetUserByEmail(string email);
        bool ExistsByEmail(string email);

        IEnumerable<ClientUser> GetClientUsers(int offset, int count, string query, SortOption<ClientUser> sort, Expression<Func<ClientUser, bool>> filter);
        IEnumerable<ClientUser> GetClientUsers(int offset, int count, SortOption<ClientUser> sort, Expression<Func<ClientUser, bool>> filter);

        IEnumerable<ClientUser> GetByClientId(Guid clientId);

        int Count(Expression<Func<ClientUser, bool>> filter);
        int Count(string query, Expression<Func<ClientUser, bool>> filter);
    }
}
