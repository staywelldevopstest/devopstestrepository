﻿namespace KswApi.Repositories.Interfaces
{
	public interface IBinaryStoreFactory
	{
		IBinaryStore GetStore(string name);
	}
}
