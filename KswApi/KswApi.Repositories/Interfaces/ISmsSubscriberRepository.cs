﻿using System;
using KswApi.Poco.Sms;
using KswApi.Repositories.Objects;

namespace KswApi.Repositories.Interfaces
{
	public interface ISmsSubscriberRepository
	{
		void Add(SmsSubscriber subscriber);
		SmsSubscriber GetByNumbers(string subscriberNumber, string serviceNumber);
		SmsSubscriber GetBySessionId(Guid sessionId);
		void Update(SmsSubscriber subscriber);
		void Delete(SmsSubscriber subscriber);
	}
}
