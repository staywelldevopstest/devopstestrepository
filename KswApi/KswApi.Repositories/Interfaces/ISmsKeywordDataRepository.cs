﻿using System;
using System.Collections.Generic;
using KswApi.Poco.Sms;
using KswApi.Repositories.Objects;

namespace KswApi.Repositories.Interfaces
{
	public interface ISmsKeywordDataRepository
	{
			void Add(SmsKeywordData number);
			void Update(SmsKeywordData number);
			void Delete(SmsKeywordData number);
			SmsKeywordData GetByNumberAndKeyword(string number, string keyword);
			IEnumerable<SmsKeywordData> GetByNumberAndLicenseId(string number, Guid license);
			IEnumerable<SmsKeywordData> GetByNumberId(Guid numberId);
			SmsKeywordData GetById(Guid id);
	}
}
