﻿using System.Collections.Generic;
using KswApi.Poco;
using KswApi.Poco.Enums;

namespace KswApi.Repositories.Interfaces
{
	public interface IConfigurationValueRepository
	{
		ConfigurationValue<TData> Get<TData>(string id);
		void Add<TData>(ConfigurationValue<TData> value);
		void Update<TData>(ConfigurationValue<TData> value);
        //IEnumerable<ConfigurationValue<TData>> GetByType<TData>(ConfigurationValueType type);
	}
}
