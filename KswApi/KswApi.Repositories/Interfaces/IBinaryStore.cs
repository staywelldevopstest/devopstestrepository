﻿using KswApi.Interface.Objects;
using System;
using System.IO;

namespace KswApi.Repositories.Interfaces
{
	public interface IBinaryStore
	{
		bool Save(Guid id, Stream stream, string contentType, DateTime time);
		StreamResponse Retrieve(Guid id);
		void Delete(Guid id);
		void Rename(Guid source, Guid destination);
	}
}
