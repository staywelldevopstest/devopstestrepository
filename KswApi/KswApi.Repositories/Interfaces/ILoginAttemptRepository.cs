﻿using KswApi.Repositories.Objects;

namespace KswApi.Repositories.Interfaces
{
	public interface ILoginAttemptRepository
	{
		void Add(LoginAttempt loginAttempt);
	}
}
