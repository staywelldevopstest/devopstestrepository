﻿using System;
using System.Collections.Generic;
using KswApi.Repositories.Objects;

namespace KswApi.Repositories.Interfaces
{
	public interface IPasswordTokenRepository
	{
		PasswordToken GetById(Guid id);
		IEnumerable<PasswordToken> GetByUserId(Guid id);
		PasswordToken GetByToken(string token);
		void Add(PasswordToken passwordToken);
		void DeleteById(Guid id);
	}
}
