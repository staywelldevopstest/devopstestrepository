﻿namespace KswApi.Repositories.Interfaces
{
    public interface IRepository
    {
        T GetRepository<T>() where T : class;
        void Commit();
    }
}
