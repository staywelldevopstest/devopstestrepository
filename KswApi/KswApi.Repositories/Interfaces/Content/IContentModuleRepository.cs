﻿using KswApi.Interface.Objects.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KswApi.Repositories.Interfaces.Content
{
	public interface IContentModuleRepository
	{
		void Add(ContentModule contentModule);
		void Update(ContentModule contentModule);
		void Delete(ContentModule contentModule);

		ContentModule GetById(Guid id);
		ContentModule GetByLicenseId(Guid licenseId);
		IEnumerable<ContentModule> GetByBucketId(Guid bucketId);
	}
}
