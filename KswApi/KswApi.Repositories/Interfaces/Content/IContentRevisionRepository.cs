﻿using System;
using KswApi.Poco.Content;

namespace KswApi.Repositories.Interfaces.Content
{
	public interface IContentRevisionRepository
	{
		void Add(ContentRevision content);
		void DeleteById(Guid id);
		ContentRevision GetById(Guid id);
	}
}
