﻿using System;
using System.Collections.Generic;
using KswApi.Interface.Objects;
using KswApi.Poco.Content;

namespace KswApi.Repositories.Interfaces.Content
{
	public interface IContentPublishedRepository
	{
		void Add(ContentPublished content);
		ContentPublished GetById(Guid id);
		void Update(ContentPublished content);
		bool Exists(Guid id);
		IEnumerable<ContentPublished> GetByIds(List<Guid> ids);
		IEnumerable<ContentPublished> GetAll();
		ContentPublished GetByPath(string bucketIdOrSlug, string contentIdOrSlug);
		IEnumerable<ContentPublished> GetByBucketIdsAndSlugs(List<IdAndSlug> idsAndSlugs);
		void DeleteById(Guid id);
		ContentPublished GetByBucketAndLegacyId(Guid bucketId, string legacyId);
	}
}
