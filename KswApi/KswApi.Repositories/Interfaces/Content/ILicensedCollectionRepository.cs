﻿using System;
using System.Collections.Generic;
using KswApi.Poco.Content;

namespace KswApi.Repositories.Interfaces.Content
{
	public interface ILicensedCollectionRepository
	{
		IEnumerable<LicensedCollection> GetByLicenseId(Guid id);
		IEnumerable<LicensedCollection> GetByLicenseIdAndCollectionIds(Guid licenseId, List<Guid> collectionIds);
		void Add(LicensedCollection collection);
		void DeleteByLicenseId(Guid id);
		void DeleteByCollectionId(Guid id);
		void DeleteByLicenseIdAndCollectionId(Guid licenseId, Guid collectionId);
		int CountByCollectionId(Guid id);
	}
}
