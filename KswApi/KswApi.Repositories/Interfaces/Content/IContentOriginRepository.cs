﻿using System;
using System.Collections.Generic;
using KswApi.Poco.Content;

namespace KswApi.Repositories.Interfaces.Content
{
	public interface IContentOriginRepository
	{
		void Add(ContentOrigin origin);
		ContentOrigin GetById(Guid id);
		void Update(ContentOrigin origin);
		void Delete(ContentOrigin origin);
		IEnumerable<ContentOrigin> GetAll();
		IEnumerable<ContentOrigin> GetByQuery(string query);
	}
}
