﻿using System.Collections.Generic;
using KswApi.Poco.Content;

namespace KswApi.Repositories.Interfaces.Content
{
    public interface ITaxonomyTypeRepository
    {
        void Clear();
        void Add(TaxonomyType taxonomyType);
        IEnumerable<TaxonomyType> GetTaxonomyTypes();
        TaxonomyType GetTaxonomyTypeBySlug(string slug);
    }
}
