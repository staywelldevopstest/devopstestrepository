﻿using System;
using System.Collections.Generic;
using KswApi.Interface.Enums;
using KswApi.Poco.Content;

namespace KswApi.Repositories.Interfaces.Content
{
	public interface IContentBucketRepository
	{
		void Add(ContentBucket bucket);
		IEnumerable<ContentBucket> GetByStatus(ObjectStatus status, int offset, int count);
		ContentBucket GetByLegacyId(int id);
		IEnumerable<ContentBucket> GetByStatusAndSearch(ObjectStatus status, ContentBucketSearch search, int offset, int count);
		IEnumerable<ContentBucket> GetByCopyrightIdForAllStatuses(Guid copyrightId);
		ContentBucket GetById(Guid id);
        IEnumerable<ContentBucket> GetByIds(IEnumerable<Guid> ids);
		ContentBucket GetByStatusAndSlug(ObjectStatus status, string slug);
		void Update(ContentBucket bucket);
		void Delete(ContentBucket bucket);
		int CountByIdsAndStatus(List<Guid> ids, ObjectStatus status);
		int CountByStatus(ObjectStatus status);
		int CountByStatusAndSearch(ObjectStatus status, ContentBucketSearch search);

        IEnumerable<ContentBucket> GetBySlugs(IEnumerable<string> slugs);
    }
}

