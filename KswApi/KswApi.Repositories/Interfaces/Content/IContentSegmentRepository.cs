﻿using System;
using System.Collections.Generic;
using KswApi.Poco.Content;

namespace KswApi.Repositories.Interfaces.Content
{
	public interface IContentSegmentRepository
	{
		void Add(ContentSegment content);
		void Update(ContentSegment content);
		void DeleteById(Guid id);
		ContentSegment GetById(Guid id);
		IEnumerable<ContentSegment> GetByContentId(Guid contentId);
	}
}
