﻿using KswApi.Poco.Content;
using System;
using System.Collections.Generic;

namespace KswApi.Repositories.Interfaces.Content
{
	public interface IImageDetailRepository
	{
		void Add(ImageDetail contentImageMeta);
		void Update(ImageDetail contentImageMeta);
		ImageDetail GetById(Guid id);
		IEnumerable<ImageDetail> GetByIds(List<Guid> ids);
		ImageDetail GetBySlug(string slug);
		ImageDetail GetByPath(string bucketIdOrSlug, string imageIdOrSlug);
		void DeleteById(Guid id);
		IEnumerable<ImageDetail> GetAll();
		IEnumerable<ImageDetail> GetByBucketId(Guid id);
	}
}
