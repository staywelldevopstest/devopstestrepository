﻿using System;
using KswApi.Poco.Content;

namespace KswApi.Repositories.Interfaces.Content
{
    public interface ICollectionRevisionRepository
    {
        void Add(CollectionRevision collection);
        CollectionRevision GetById(Guid id);
        CollectionRevision GetByCollectionId(Guid id);
    }
}
