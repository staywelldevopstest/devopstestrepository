﻿using System;
using System.Collections.Generic;
using KswApi.Poco.Content;

namespace KswApi.Repositories.Interfaces.Content
{
	public interface ICollectionStructureRepository
	{
		void Add(CollectionStructure structure);
		void Update(CollectionStructure structure);
		CollectionStructure GetById(Guid id);
		IEnumerable<CollectionStructure> GetByPathElement(Guid id);
		void DeleteByIds(List<Guid> ids);
		IEnumerable<CollectionStructure> GetWithChildrenById(Guid id);
		bool ExistsByItemIdAndCollectionId(Guid itemId, List<Guid> collectionIds);
		IEnumerable<CollectionStructure> GetByItemId(Guid id);
		IEnumerable<CollectionStructure> GetByItemIds(List<Guid> ids);
	}
}
