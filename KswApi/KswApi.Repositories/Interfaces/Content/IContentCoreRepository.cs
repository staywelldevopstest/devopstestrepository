﻿using System;
using System.Collections.Generic;
using KswApi.Poco.Content;

namespace KswApi.Repositories.Interfaces.Content
{
	public interface IContentCoreRepository
	{
		void Add(ContentCore master);
		void Update(ContentCore master);
		ContentCore GetById(Guid id);
		IEnumerable<ContentCore> GetByIds(IEnumerable<Guid> ids);
		IEnumerable<ContentCore> GetByCopyrightId(Guid copyrightId);
		void DeleteById(Guid id);
		IEnumerable<ContentCore> GetAll();
	}
}
