﻿using System;
using System.Collections.Generic;
using KswApi.Poco.Content;

namespace KswApi.Repositories.Interfaces.Content
{
    public interface ICollectionRepository
    {
        IEnumerable<Collection> GetByIds(List<Guid> ids);
        void Add(Collection collection);
        Collection GetById(Guid guid);
        void Update(Collection collection);
        void Delete(Collection collection);
        IEnumerable<Collection> GetAll();
	    void IncrementLicenseCount(Guid id, int count);
    }
}
