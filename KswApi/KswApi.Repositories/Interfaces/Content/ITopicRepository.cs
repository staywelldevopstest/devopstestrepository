﻿using System;
using System.Collections.Generic;
using KswApi.Poco.Content;

namespace KswApi.Repositories.Interfaces.Content
{
	public interface ITopicRepository
	{
		void Add(Topic topic);
		void Update(Topic topic);
		IEnumerable<Topic> GetByIds(List<Guid> ids);
		Topic GetBySlug(string slug);
		IEnumerable<Topic> GetBySlugs(List<string> slugs);
		bool Exists(string slug);
		void DeleteByIds(List<Guid> ids);
		Topic GetById(Guid guid);
	}
}
