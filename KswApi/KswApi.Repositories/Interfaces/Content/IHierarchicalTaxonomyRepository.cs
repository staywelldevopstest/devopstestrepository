﻿using System.Collections.Generic;
using KswApi.Interface.Enums;
using KswApi.Poco.Content;

namespace KswApi.Repositories.Interfaces.Content
{
    public interface IHierarchicalTaxonomyRepository
    {
        void Add(HierarchicalTaxonomy hierarchicalTaxonomy);
        void Clear(HierarchicalTaxonomyType type);
        IEnumerable<HierarchicalTaxonomy> GetHierarchicalTaxonomiesByPath(HierarchicalTaxonomyType type, string path);
        HierarchicalTaxonomy GetHierarchicalTaxonomyBySlugAndPath(HierarchicalTaxonomyType type, string slug, string path);
        IEnumerable<HierarchicalTaxonomy> GetHierarchicalTaxonomiesByFullPaths(List<string> fullPaths);
    }
}
