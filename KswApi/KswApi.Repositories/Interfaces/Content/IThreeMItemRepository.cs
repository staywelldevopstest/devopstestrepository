﻿using System.Collections.Generic;
using KswApi.Poco.ThreeM;

namespace KswApi.Repositories.Interfaces.Content
{
	public interface IThreeMItemRepository
	{
		void Upsert(ThreeMItem item);
		IEnumerable<ThreeMItem> Get(long previousItem);
		void Delete(long id);
	}
}
