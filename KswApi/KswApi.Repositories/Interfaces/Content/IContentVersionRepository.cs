﻿using System;
using System.Collections.Generic;
using KswApi.Poco.Content;

namespace KswApi.Repositories.Interfaces.Content
{
	public interface IContentVersionRepository
	{
		void Add(ContentVersion content);
		void Update(ContentVersion content);
		void Delete(ContentVersion content);
		ContentVersion GetById(Guid id);
		ContentVersion GetByPath(string bucketIdOrSlug, string contentIdOrSlug);

		IEnumerable<ContentVersion> GetByTitle(int offset, int count);
		IEnumerable<ContentVersion> GetByIds(IEnumerable<Guid> ids);
		IEnumerable<ContentVersion> GetByBucketId(Guid bucketId);
		ContentVersion GetByBucketAndLegacyId(Guid bucketId, string legacyId);
		IEnumerable<ContentVersion> GetByBucketIdsAndSlugs(List<IdAndSlug> idsAndSlugs);

		int Count();
		IEnumerable<ContentVersion> GetAll();
	}
}
