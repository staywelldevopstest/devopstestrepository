﻿using System;
using System.Collections.Generic;
using KswApi.Interface.Objects.Content;
using KswApi.Poco.Content;

namespace KswApi.Repositories.Interfaces.Content
{
    public interface IContentHistoryRepository
	{
		void Add(ContentHistory history);
		void DeleteById(Guid id);
		ContentHistory GetById(Guid id);
		IEnumerable<ContentHistory> GetByContentId(Guid id);
        ContentHistory GetByContentIdRevisionTypeAndOnOrBeforeDate(Guid id, ContentRevisionType[] revisionTypes, DateTime onOrBefore);
        IEnumerable<ContentHistory> GetByTimeAndType(DateTime time, ContentRevisionType type, int count);
	}
}
