﻿using System;
using KswApi.Repositories.Objects;

namespace KswApi.Repositories.Interfaces
{
	public interface IRefreshTokenRepository
	{
		RefreshToken GetByToken(string token);
		void Add(RefreshToken token);
		void DeleteByUserId(Guid id);
		void Delete(RefreshToken token);
		void DeleteByExpiration(DateTime expiration);
		void Delete(Guid id);
	}
}
