﻿using System;
using System.Collections.Generic;
using KswApi.Interface.Objects;
using KswApi.Repositories.Objects;

namespace KswApi.Repositories.Interfaces
{
	public interface IRoleRepository
	{
		Role GetById(Guid id);
		Role GetByName(string name);
		IEnumerable<Role> GetAll();
		bool Contains(string roleName);
		void Add(Role role);
		void Update(Role role);
		void Delete(Role role);
	}
}
