﻿using KswApi.Poco.Administration;
using System;
using System.Collections.Generic;

namespace KswApi.Repositories.Interfaces
{
    public interface IAuthorizationGrantRepository
    {
        void Add(AuthorizationGrant grant);
        void Delete(AuthorizationGrant grant);
        void Delete(Guid id);

        AuthorizationGrant GetByCode(string code);
        AuthorizationGrant GetById(Guid id);

        IEnumerable<AuthorizationGrant> GetByExpiration(DateTime expiration, int count);
    }
}
