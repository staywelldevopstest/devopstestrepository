﻿using System;
using System.Collections.Generic;
using System.Reflection;

namespace KswApi.Repositories
{
	public class RepositoryBuilder
	{
		private const int MAXIMUM_TYPE_RECURSION = 5;

		private static readonly List<Type> RepositoryTypes = new List<Type>();

		#region Public Properties

        public IEnumerable<Type> GetRepositoryTypes()
        {
            return new List<Type>(RepositoryTypes);
        }

		#endregion Public Properties

		#region Static Initialization

		static RepositoryBuilder()
		{
			AddRepositoryProperties(typeof(Repository));
			AddRepositoryProperties(typeof(TestRepository));
			AddRepositoryProperties(typeof(LogRepository));
		}

		private static void AddRepositoryProperties(Type type, int recursionDepth = 0)
		{
			if (recursionDepth >= MAXIMUM_TYPE_RECURSION)
				throw new InvalidOperationException("The entity repository contains cyclic references or a reference depth greater than the maximum allowed.");

			PropertyInfo[] properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

			foreach (PropertyInfo property in properties)
			{
				Type propertyType = property.PropertyType;

				if (propertyType.IsInterface)
				{
					if (!RepositoryTypes.Contains(propertyType))
						RepositoryTypes.Add(propertyType);

					continue;
				}

				AddRepositoryProperties(propertyType);
			}

		}

		#endregion Static Initialization
	}
}
