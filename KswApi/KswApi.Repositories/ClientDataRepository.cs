﻿using KswApi.Repositories.Enums;
using KswApi.Repositories.Framework;
using KswApi.Repositories.Interfaces;

namespace KswApi.Repositories
{
	public class ClientDataRepository : RepositoryRoot
	{
		public ClientDataRepository()
			: base(RepositoryType.Client)
		{
		}

		public IClientRepository Clients { get { return Get<IClientRepository>(); } }
	}
}
