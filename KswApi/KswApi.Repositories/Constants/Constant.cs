﻿namespace KswApi.Repositories.Constants
{
	public static class Constant
	{
		public const string IMAGE_STORE_NAME = "ImageFiles";
        public const string COLLECTION_IMAGE_STORE_NAME = "CollectionImageFiles";
	}
}
