﻿using System;
using System.Linq.Expressions;
using System.Reflection;
using KswApi.Common.Extensions;
using KswApi.Repositories.Enums;

namespace KswApi.Repositories.Objects
{
	public class SortOption<T> : SortOption
	{
		public PropertyInfo Property { get; set; }
		public Expression Expression { get; set; }

		public static SortOption<T> FromProperty(Expression<Func<T, object>> propertyExpression, SortDirection direction = SortDirection.Ascending)
		{
			PropertyInfo property = propertyExpression.GetProperty();
			return new SortOption<T>
				       {
					       Property = property,
						   SortField = property.Name,
					       Direction = direction
				       };
		}

		public static SortOption<T> FromProperty(Expression<Func<T, object>> propertyExpression, Expression<Func<T, object>> emporsonatedProperty, SortDirection direction = SortDirection.Ascending)
		{
			PropertyInfo property = propertyExpression.GetProperty();
			PropertyInfo emporsonated = emporsonatedProperty.GetProperty();

			return new SortOption<T>
			{
				Property = property,
				SortField = emporsonated.Name,
				Direction = direction
			};
		}

	}
}
