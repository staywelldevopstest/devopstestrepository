﻿using KswApi.Repositories.Enums;

namespace KswApi.Repositories.Objects
{
	public class SortOption
	{
		public SortOption()
		{
			
		}

		public SortOption(string sortField, SortDirection direction)
		{
			SortField = sortField;
			Direction = direction;
		}

		public string SortField { get; set; }
		public SortDirection Direction { get; set; }

		public override string ToString()
		{
			if (string.IsNullOrEmpty(SortField))
				return Direction.ToString();

			return SortField + ' ' + Direction.ToString();
		}
	}
}
