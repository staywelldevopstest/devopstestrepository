﻿using KswApi.Repositories.Enums;
using KswApi.Repositories.Framework;
using KswApi.Repositories.Interfaces;

namespace KswApi.Repositories
{
    public class SmsMessageLogRepository : RepositoryRoot
    {
        public SmsMessageLogRepository()
            : base(RepositoryType.SmsMessageLog)
        {
        }

        public ISmsMessageLogRepository SmsMessageLogs { get { return Get<ISmsMessageLogRepository>(); } }
    }
}
