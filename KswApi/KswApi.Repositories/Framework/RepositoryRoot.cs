﻿using KswApi.Repositories.Enums;

namespace KswApi.Repositories.Framework
{
	public class RepositoryRoot : RepositoryGroup
	{
		public RepositoryRoot(RepositoryType repositoryType) : base(repositoryType)
		{
		}

		public void Commit()
		{
			Repository.Commit();
		}
	}
}
