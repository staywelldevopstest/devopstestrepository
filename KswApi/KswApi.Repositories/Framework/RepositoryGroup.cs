﻿using System;
using KswApi.Ioc;
using KswApi.Repositories.Enums;
using KswApi.Repositories.Interfaces;

namespace KswApi.Repositories.Framework
{
	public class RepositoryGroup
	{
		protected RepositoryGroup(RepositoryType repositoryType)
		{
			IRepositoryFactory factory = Dependency.Get<IRepositoryFactory>();

			Repository = factory.GetRepository(repositoryType);
		}

		public RepositoryGroup(RepositoryGroup parent)
		{
			if (parent == null)
				throw new ArgumentNullException("parent");

			Repository = parent.Repository;
		}

		protected TRepository Get<TRepository>() where TRepository : class
		{
			return Repository.GetRepository<TRepository>();
		}

		protected IBinaryStore GetStore(string name)
		{
			IBinaryStoreFactory factory = Dependency.Get<IBinaryStoreFactory>();
			return factory.GetStore(name);
		}

		protected IRepository Repository { get; private set; }
	}
}
