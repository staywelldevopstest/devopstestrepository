﻿using KswApi.Repositories.Enums;
using KswApi.Repositories.Framework;
using KswApi.Repositories.Interfaces;

namespace KswApi.Repositories
{
	public class LogRepository : RepositoryRoot
	{
		public LogRepository() : base(RepositoryType.Log)
		{
		}

		public ILogRepository Logs { get { return Get<ILogRepository>(); } }
	}
}
