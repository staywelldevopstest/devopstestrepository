﻿using KswApi.Repositories.Enums;
using KswApi.Repositories.Framework;
using KswApi.Repositories.Interfaces;

namespace KswApi.Repositories
{
	public class TestRepository : RepositoryRoot
	{
		public TestRepository()
			: base(RepositoryType.Test)
		{
		}

		public ITestDataRepository TestData { get { return Get<ITestDataRepository>(); } }
	}
}
