﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace KswApi.Xml
{
	/// <summary>
	/// This class exists because of XmlSerializer's handling of 
	/// null values, which inserts xsi:nill values into the xml,
	/// and there is no way to prevent it.
	/// </summary>
	public class CustomXmlSerializer
	{
		private const string INVALID_XML = "The xml is invalid.";
		private const string INVALID_ROOT_ELEMENT = "The xml root element is invalid.";
		private const string INVALID_PARAMETER = "Invalid value for parameter: {0}.";

		private readonly Type _type;
		private Dictionary<Type, Dictionary<string, PropertyInfo>> _typeDictionary;

		public CustomXmlSerializer(Type type)
		{
			_type = type;
		}

		#region Public Methods

		public void Serialize(XmlWriter writer, object o)
		{
			writer.WriteStartDocument();

			if (o == null && !_type.IsPrimitive)
			{
				writer.WriteStartElement(GetElementName(_type, null, null));
				writer.WriteEndElement();
				return;
			}

			WriteObject(writer, o, _type, null, null);

			writer.WriteEndDocument();
		}

		public object Deserialize(XmlReader reader)
		{
			try
			{
				while (true)
				{
					reader.Read();

					if (reader.NodeType == XmlNodeType.Element)
						break;

					if (reader.NodeType != XmlNodeType.Whitespace && reader.NodeType != XmlNodeType.Comment)
						throw new SerializationException(INVALID_XML);
				}

				if (reader.Name != GetElementName(_type, null, null))
					throw new SerializationException(INVALID_ROOT_ELEMENT);


				return ReadObject(reader, _type, null, null);
			}
			catch (XmlException)
			{
				throw new SerializationException(INVALID_XML);
			}
		}

		#endregion

		#region Private Methods

		#region Write Methods

		private void WriteObject(XmlWriter writer, object o, Type type, PropertyInfo property, string name)
		{
			if (o == null)
				return;

			if (type.IsPrimitive || type == typeof(string))
			{
				WritePrimitive(writer, o, type, property, name);
				return;
			}

			if (type.IsEnum)
			{
				WriteEnum(writer, o, type, property, name);
				return;
			}

			if (type.IsValueType)
			{
				WriteValueType(writer, o, type, property, name);
				return;
			}

			WriteClass(writer, o, type, property, name);
		}

		private void WritePrimitive(XmlWriter writer, object o, Type type, PropertyInfo property, string name)
		{
			writer.WriteStartElement(GetElementName(type, property, name));
			writer.WriteValue(o);
			writer.WriteEndElement();
		}

		private void WriteEnum(XmlWriter writer, object o, Type type, PropertyInfo property, string name)
		{
			writer.WriteStartElement(GetElementName(type, property, name));
			writer.WriteValue(o.ToString());
			writer.WriteEndElement();
		}

		private void WriteValueType(XmlWriter writer, object o, Type type, PropertyInfo property, string name)
		{
			if (type == typeof(Guid))
			{
				Guid guid = (Guid)o;

				writer.WriteStartElement(GetElementName(type, property, name));
				writer.WriteRaw(XmlConvert.ToString(guid));
				writer.WriteEndElement();
			}
			else if (type == typeof(DateTime))
			{
				DateTime dateTime = (DateTime)o;

				writer.WriteStartElement(GetElementName(type, property, name));
				writer.WriteRaw(XmlConvert.ToString(dateTime, XmlDateTimeSerializationMode.Utc));
				writer.WriteEndElement();
			}
			else if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
			{
				WriteObject(writer, o, Nullable.GetUnderlyingType(type), property, name);
			}
			else
			{
				WriteClass(writer, o, type, property, name);
			}
		}

		private void WriteClass(XmlWriter writer, object o, Type type, PropertyInfo property, string name)
		{
			if (typeof(ICollection).IsAssignableFrom(type))
			{
				// handle any collections here
				Type listInterface = type.GetInterfaces().FirstOrDefault(
					item => item.IsGenericType && item.GetGenericTypeDefinition() == typeof(IList<>));

				if (listInterface != null)
				{
					WriteList(writer, o, type, listInterface, property, name);
					return;
				}

				Type dictionaryInterface = type.GetInterfaces().FirstOrDefault(
					item => item.IsGenericType && item.GetGenericTypeDefinition() == typeof(IDictionary<,>));

				if (dictionaryInterface != null)
				{
					WriteDictionary(writer, o, type, dictionaryInterface, property, name);
					return;
				}
			}

			writer.WriteStartElement(GetElementName(type, property, name));

			PropertyInfo[] properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

			foreach (PropertyInfo childProperty in properties)
			{
				if (!childProperty.CanRead || !childProperty.CanWrite)
					continue;

				if (Attribute.GetCustomAttribute(childProperty, typeof(XmlIgnoreAttribute)) != null)
					continue;

				WriteObject(writer, childProperty.GetValue(o, null), childProperty.PropertyType, childProperty, null);
			}

			writer.WriteEndElement();
		}

		private void WriteList(XmlWriter writer, object o, Type type, Type listInterface, PropertyInfo property, string name)
		{
			writer.WriteStartElement(GetElementName(type, property, name));

			Type genericType = listInterface.GetGenericArguments()[0];

			string elementName = GetEnumerableElementName(genericType, property);

			foreach (object child in (IEnumerable)o)
			{
				if (child == null)
				{
					// make sure we don't re-order lists, null values have to stay in
					writer.WriteStartElement(elementName);
					writer.WriteEndElement();
					continue;
				}

				WriteObject(writer, child, genericType, null, elementName);
			}

			writer.WriteEndElement();
		}

		private void WriteDictionary(XmlWriter writer, object o, Type type, Type dictionaryInterface, PropertyInfo property, string name)
		{
			writer.WriteStartElement(GetElementName(type, property, name));

			Type[] types = dictionaryInterface.GetGenericArguments();
			Type genericTypeKey = types[0];
			Type genericTypeValue = types[0];

			string elementName = GetEnumerableElementName(type, property);


			foreach (DictionaryEntry child in (IDictionary)o)
			{
				writer.WriteStartElement(elementName);
				WriteObject(writer, child.Key, genericTypeKey, null, "Key");
				WriteObject(writer, child.Value, genericTypeValue, null, "Value");
				writer.WriteEndElement();
			}


			writer.WriteEndElement();
		}

		#endregion

		#region Read Methods

		private object ReadObject(XmlReader reader, Type type, PropertyInfo property, string name)
		{
			if (_typeDictionary == null)
				_typeDictionary = new Dictionary<Type, Dictionary<string, PropertyInfo>>();

			if (type.IsPrimitive)
			{
				if (!reader.Read())
					throw new SerializationException(INVALID_XML);

				return reader.ReadContentAs(type, null);
			}

			if (type == typeof(string))
			{
				if (!reader.Read())
					return null;

				if (reader.NodeType == XmlNodeType.EndElement)
					return string.Empty;

				return reader.ReadContentAsString();
			}

			if (type.IsEnum)
			{
				return ReadEnum(reader, type, property, name);
			}

			if (type.IsValueType)
			{
				return ReadValueType(reader, type, property, name);
			}

			return ReadClass(reader, type, property);
		}

		private object ReadEnum(XmlReader reader, Type type, PropertyInfo property, string name)
		{
			if (!reader.Read())
				throw new SerializationException(INVALID_XML);

			string value = reader.ReadContentAsString();

			if (string.IsNullOrEmpty(value))
				return null;

			FieldInfo field = type.GetFields().FirstOrDefault(item => item.Name == value);

			if (field == null)
				throw new SerializationException(string.Format(INVALID_PARAMETER, name));

			return field.GetRawConstantValue();
		}

		private object ReadValueType(XmlReader reader, Type type, PropertyInfo property, string name)
		{
			if (type == typeof(Guid))
			{
				if (!reader.Read())
					throw new SerializationException(INVALID_XML);

				if (reader.NodeType == XmlNodeType.EndElement)
					return null;

				string value = reader.ReadContentAsString();

				Guid result;

				if (!Guid.TryParse(value, out result))
					throw new SerializationException(string.Format(INVALID_PARAMETER, name));

				return result;
			}

			if (type == typeof(DateTime))
			{
				if (!reader.Read())
					throw new SerializationException(INVALID_XML);

				if (reader.NodeType == XmlNodeType.EndElement)
					return null;

				string value = reader.ReadContentAsString();

				DateTime result;

				if (!DateTime.TryParse(value, out result))
					throw new SerializationException(string.Format(INVALID_PARAMETER, name));

				return result.ToUniversalTime();
			}

			if (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>))
			{
				Type underlyingType = Nullable.GetUnderlyingType(type);
				object readObject = ReadObject(reader, underlyingType, property, name);

				// special handling for nullable enumerations--the integer values can't be directly boxed to the
				// nullable enumeration.
				return underlyingType.IsEnum ? Enum.Parse(underlyingType, readObject.ToString()) : readObject;
			}

			return ReadClass(reader, type, property);
		}

		private object ReadClass(XmlReader reader, Type type, PropertyInfo property)
		{
			if (typeof(ICollection).IsAssignableFrom(type))
			{
				// handle any collection classes here
				Type listInterface = type.GetInterfaces().FirstOrDefault(
						item => item.IsGenericType && item.GetGenericTypeDefinition() == typeof(IList<>));

				if (listInterface != null)
					return ReadList(reader, type, listInterface, property, null);
			}

			ConstructorInfo constructor = type.GetConstructor(Type.EmptyTypes);

			if (constructor == null)
				throw new InvalidOperationException(string.Format("Type {0} has no public parameterless constructor.", type.Name));

			object result = constructor.Invoke(null);

			while (reader.Read() && reader.NodeType != XmlNodeType.EndElement)
			{
				if (reader.NodeType == XmlNodeType.Whitespace || reader.NodeType == XmlNodeType.Comment)
					continue;

				if (reader.NodeType != XmlNodeType.Element)
					throw new SerializationException(INVALID_XML);

				string name = reader.LocalName;

				PropertyInfo childProperty = GetProperty(type, name);

				object value = ReadObject(reader, childProperty.PropertyType, childProperty, name);

				if (value != null)
					childProperty.SetValue(result, value, null);
			}

			return result;
		}

		private object ReadList(XmlReader reader, Type type, Type enumerableInterface, PropertyInfo property, string name)
		{
			Type genericType = enumerableInterface.GetGenericArguments()[0];

			ConstructorInfo constructor = type.GetConstructor(Type.EmptyTypes);

			if (constructor == null)
				throw new InvalidOperationException(string.Format("Type {0} has no public parameterless constructor.", type.Name));

			IList result = (IList)constructor.Invoke(null);

			if (reader.IsEmptyElement)
				return result;

			while (true)
			{
				if (!reader.Read())
					throw new SerializationException(INVALID_XML);

				if (reader.NodeType == XmlNodeType.EndElement)
					return result;

				if (reader.NodeType == XmlNodeType.Whitespace || reader.NodeType == XmlNodeType.Comment)
					continue;

				if (reader.NodeType != XmlNodeType.Element)
					throw new SerializationException(INVALID_XML);

				result.Add(ReadObject(reader, genericType, null, null));
			}
		}

		#endregion

		#region Helper Methods

		private string GetEnumerableElementName(Type type, PropertyInfo property)
		{
			if (property != null)
			{
				XmlArrayItemAttribute element = (XmlArrayItemAttribute)Attribute.GetCustomAttribute(property, typeof(XmlArrayItemAttribute));
				if (element != null && !string.IsNullOrEmpty(element.ElementName))
					return element.ElementName;
			}

			XmlRootAttribute rootAttribute = (XmlRootAttribute)Attribute.GetCustomAttribute(type, typeof(XmlRootAttribute));
			if (rootAttribute != null && !string.IsNullOrEmpty(rootAttribute.ElementName))
				return rootAttribute.ElementName;

			XmlTypeAttribute typeAttribute = (XmlTypeAttribute)Attribute.GetCustomAttribute(type, typeof(XmlTypeAttribute));
			if (typeAttribute != null && !string.IsNullOrEmpty(typeAttribute.TypeName))
				return typeAttribute.TypeName;

			return type.Name;
		}

		private PropertyInfo GetProperty(Type type, string name)
		{
			Dictionary<string, PropertyInfo> table;
			if (!_typeDictionary.TryGetValue(type, out table))
			{
				table = GetPropertyTable(type);

				_typeDictionary[type] = table;
			}

			PropertyInfo property;

			if (!table.TryGetValue(name, out property))
				throw new SerializationException(string.Format("Unknown element: {0}.", name));

			return property;
		}

		private Dictionary<string, PropertyInfo> GetPropertyTable(Type type)
		{
			Dictionary<string, PropertyInfo> result = new Dictionary<string, PropertyInfo>();

			PropertyInfo[] properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

			foreach (PropertyInfo childProperty in properties)
			{
				if (!childProperty.CanRead || !childProperty.CanWrite)
					continue;

				if (Attribute.GetCustomAttribute(childProperty, typeof(XmlIgnoreAttribute)) != null)
					continue;

				result[GetElementName(childProperty.PropertyType, childProperty, null)] = childProperty;
			}

			return result;
		}

		private string GetElementName(Type type, PropertyInfo property, string name)
		{
			if (!string.IsNullOrEmpty(name))
				return name;

			if (property != null)
			{
				XmlElementAttribute elementAttribute = (XmlElementAttribute)Attribute.GetCustomAttribute(property, typeof(XmlElementAttribute));
				if (elementAttribute != null && !string.IsNullOrEmpty(elementAttribute.ElementName))
					return elementAttribute.ElementName;

				XmlArrayAttribute arrayAttribute = (XmlArrayAttribute)Attribute.GetCustomAttribute(property, typeof(XmlArrayAttribute));
				if (arrayAttribute != null && !string.IsNullOrEmpty(arrayAttribute.ElementName))
					return arrayAttribute.ElementName;
			}

			XmlRootAttribute rootAttribute = (XmlRootAttribute)Attribute.GetCustomAttribute(type, typeof(XmlRootAttribute));
			if (rootAttribute != null && !string.IsNullOrEmpty(rootAttribute.ElementName))
				return rootAttribute.ElementName;

			XmlTypeAttribute typeAttribute = (XmlTypeAttribute)Attribute.GetCustomAttribute(type, typeof(XmlTypeAttribute));
			if (typeAttribute != null && !string.IsNullOrEmpty(typeAttribute.TypeName))
				return typeAttribute.TypeName;
			if (property != null)
				return property.Name;

			if (!type.IsGenericType)
				return type.Name;

			return GetGenericTypeName(type);
		}

		private string GetGenericTypeName(Type type)
		{
			StringBuilder builder = new StringBuilder(type.Name.Split('`')[0]);

			builder.Append("Of");

			bool first = true;

			foreach (Type childType in type.GetGenericArguments())
			{
				if (!first)
					builder.Append("And");
				else
					first = false;

				builder.Append(GetElementName(childType, null, null));
			}

			return builder.ToString();
		}

		#endregion

		#endregion
	}
}
