﻿using System.ServiceModel;
using KswApi.ExternalServices.Framework.WcfServices;
using Moq;

namespace KswApi.Test.IntegrationTest.WcfServices
{
	public class MockClientChannelFactory<T> : IClientChannelFactory<T> where T : class, IClientChannel
	{
		public Mock<T> MockClientChannel { get; set; }

		public MockClientChannelFactory()
		{
			MockClientChannel = new Mock<T>();
			MockClientChannel.SetupAllProperties();
		}

		public void Open()
		{
			MockClientChannel.SetupGet(mock => mock.State).Returns(CommunicationState.Opened);
		}

		public void Close()
		{
			MockClientChannel.SetupGet(mock => mock.State).Returns(CommunicationState.Closed);
		}

		public void Abort() { }

		public T CreateChannel()
		{
			return MockClientChannel.Object;
		}
	}
}