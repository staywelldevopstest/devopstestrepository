﻿using System.Collections.Generic;
using KswApi.Common.Configuration;
using KswApi.ExternalServices.Framework.WcfServices;
using Kswapi.ExternalServices.Rendering.XmPie;
using Kswapi.ExternalServices.XmPie.Account;
using Kswapi.ExternalServices.XmPie.Campaign;
using Kswapi.ExternalServices.XmPie.DataSource;
using Kswapi.ExternalServices.XmPie.DataSourcePlanUtils;
using Kswapi.ExternalServices.XmPie.Document;
using Kswapi.ExternalServices.XmPie.Job;
using Kswapi.ExternalServices.XmPie.JobTicket;
using Kswapi.ExternalServices.XmPie.PlanUtils;
using Kswapi.ExternalServices.XmPie.Production;
using KswApi.Interface.Objects.Content;
using KswApi.Interface.Objects.Rendering;
using KswApi.Interface.Rendering;
using KswApi.Test.Framework.Fakes;
using KswApi.Test.IntegrationTest.WcfServices;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace KswApi.Test.IntegrationTest.Rendering
{
	[TestClass]
	public class XmPieRenderingModeFulfillerTests
	{
		private const string USER_NAME = @"medimedia\kswtest";
		private const string PASSWORD = "kram3sstayw3ll";

		private FakeLayer _fakeLayer;
		private MockClientChannelFactory<Account_SSPSoapChannel> _mockAccountChannelFactory;
		private MockClientChannelFactory<Campaign_SSPSoapChannel> _mockCampaignChannelFactory;
		private MockClientChannelFactory<DataSource_SSPSoapChannel> _mockDataSourceChannelFactory;
		private MockClientChannelFactory<DataSourcePlanUtils_SSPSoapChannel> _mockDataSourcePlanUtilsChannelFactory;
		private MockClientChannelFactory<Document_SSPSoapChannel> _mockDocumentChannelFactory;
		private MockClientChannelFactory<Job_SSPSoapChannel> _mockJobChannelFactory;
		private MockClientChannelFactory<JobTicket_SSPSoapChannel> _mockJobTicketChannelFactory;
		private MockClientChannelFactory<PlanUtils_SSPSoapChannel> _mockPlanUtilsChannelFactory;
		private MockClientChannelFactory<Production_SSPSoapChannel> _mockProductionChannelFactory;

		[TestInitialize]
		public void TestInitialize()
		{
			_fakeLayer = FakeLayer.Setup();
			_fakeLayer.Settings.Renderers = new List<RenderingConfiguration>(new[]
				{
					new RenderingConfiguration
					{
						Mode = "Print",
						Options = new Dictionary<string, string>
						{
							{ "userName", USER_NAME },
							{ "password", PASSWORD },
							{ "sharePath", @"\\ya-tiuprod-01\DropFiles" },
							{ "shareUserName", "XMPieShareUser" },
							{ "sharePassword", "k4eP5281" },
							{ "physicalPath", @"D:\DropFiles" },
							{ "xmpieAccount", "TestAccount" },
						},
						Type = typeof(XmPieRenderingModeFulfiller)
					}
				});
		}

		[TestCategory("Integration")]
		[TestMethod]
		public void GenerateProof_Success_ReturnsValidId()
		{
			//FakeLayer.Setup();
			//Settings settings = new Settings();
			//settings.Renderers.Add(new RenderingConfiguration
			//{
			//	Options = new Dictionary<string, string>
			//	{
			//		{ "userName", "medimedia\\kswtest" },
			//		{ "password", "kram3sstayw3ll" }
			//	}
			//});

			//MockClientChannelFactory<Job_SSPSoapChannel> mockJobChannelFactory = new MockClientChannelFactory<Job_SSPSoapChannel>();

			//MockClientChannelFactory<JobTicket_SSPSoapChannel> mockJobTicketChannelFactory = new MockClientChannelFactory<JobTicket_SSPSoapChannel>();
			//string jobTicketId = jobTicketService.CreateNewTicketForDocument(createNewTicketForDocumentRequest).Body.CreateNewTicketForDocumentResult;

			//MockClientChannelFactory<Production_SSPSoapChannel> mockProductionChannelFactory = new MockClientChannelFactory<Production_SSPSoapChannel>();
			//mockProductionChannelFactory.MockClientChannel.Setup(m => m.SubmitJob(It.IsAny<SubmitJobRequest>())).Returns(new SubmitJobResponse(new SubmitJobResponseBody("1")));

			//Mock<IXmPieServices> mockXmPieServices = new Mock<IXmPieServices>();
			//mockXmPieServices.Setup(m => m.GenerateProof(It.IsAny<string>(), It.IsAny<ProofType>())).Returns(new byte[256]);

			//XmPieServices xmPieServices = new XmPieServices(
			//	new Service<Job_SSPSoapChannel>(mockJobChannelFactory),
			//	new Service<JobTicket_SSPSoapChannel>(mockJobTicketChannelFactory),
			//	new Service<Production_SSPSoapChannel>(mockProductionChannelFactory),
			//	"userName",
			//	"password");

			//XmPieServices xmPieServices = new XmPieServices(
			//	new Service<Job_SSPSoapChannel>(new MockClientChannelFactory<Job_SSPSoapChannel>()),
			//	new Service<JobTicket_SSPSoapChannel>(new MockClientChannelFactory<JobTicket_SSPSoapChannel>()),
			//	new Service<Production_SSPSoapChannel>(new MockClientChannelFactory<Production_SSPSoapChannel>()),
			//	"userName",
			//	"password");

			// arrange
			IRenderingModeFulfiller fulfiller = CreateFulfillerWithMockXmPieServices();
			_mockAccountChannelFactory.MockClientChannel.Setup(
				m => m.GetID(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns("1");
			_mockCampaignChannelFactory.MockClientChannel.Setup(
				m => m.GetDocuments(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new[] { "1" });
			_mockDataSourceChannelFactory.MockClientChannel.Setup(m =>
				m.CreateNewFromZip(It.IsAny<CreateNewFromZipRequest>())).Returns(new CreateNewFromZipResponse("1"));
			_mockDocumentChannelFactory.MockClientChannel.Setup(m =>
				m.GetProperties(It.IsAny<Kswapi.ExternalServices.XmPie.Document.GetPropertiesRequest>())).Returns(
				new Kswapi.ExternalServices.XmPie.Document.GetPropertiesResponse(new[]
				{
					new Kswapi.ExternalServices.XmPie.Document.Property { m_Name = "documentType", m_Value = "INDD" }
				}));
			_mockJobChannelFactory.MockClientChannel.Setup(m =>
				m.GetStatus(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(1);
			_mockJobTicketChannelFactory.MockClientChannel.Setup(m => m.CreateNewTicketForDocument(
				It.IsAny<CreateNewTicketForDocumentRequest>())).Returns(
				new CreateNewTicketForDocumentResponse(new CreateNewTicketForDocumentResponseBody("1")));
			_mockJobTicketChannelFactory.MockClientChannel.Setup(m => m.AddDestinationByID(
				It.IsAny<AddDestinationByIDRequest>())).Returns(
				new AddDestinationByIDResponse(new AddDestinationByIDResponseBody(true)));
			_mockJobTicketChannelFactory.MockClientChannel.Setup(m =>
				m.SetOutputInfo(It.IsAny<SetOutputInfoRequest>())).Returns(new SetOutputInfoResponse(new SetOutputInfoResponseBody(true)));
			_mockJobTicketChannelFactory.MockClientChannel.Setup(m =>
				m.SetJobType(It.IsAny<SetJobTypeRequest>())).Returns(new SetJobTypeResponse(new SetJobTypeResponseBody(true)));
			_mockProductionChannelFactory.MockClientChannel.Setup(m =>
				m.SubmitJob(It.IsAny<SubmitJobRequest>())).Returns(new SubmitJobResponse(new SubmitJobResponseBody("1")));
			_mockProductionChannelFactory.MockClientChannel.Setup(m =>
				m.SubmitJob(It.IsAny<SubmitJobRequest>())).Returns(new SubmitJobResponse(new SubmitJobResponseBody("1")));
			RenderRequest request = new RenderRequest();
			KswApiTemplate kswApiTemplate = new KswApiTemplate();
			IList<ContentArticleResponse> allTemplates = new List<ContentArticleResponse>();
			IList<ContentArticleResponse> allContent = new List<ContentArticleResponse>();

			// act
			RenderResponse response = fulfiller.Render(request, kswApiTemplate, allTemplates, allContent);

			// assert
			Assert.IsNotNull(response);
		}

		//[TestCategory("Integration")]
		//[TestMethod]
		//public void SubmitPrintJob_WithMockAndSuccess_ReturnsExpectedId()
		//{
		//	// arrange
		//	IRenderingModeFulfiller fulfiller = CreateFulfillerWithMockXmPieServices();
		//	_mockAccountChannelFactory.MockClientChannel.Setup(
		//		m => m.GetID(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns("1");
		//	_mockCampaignChannelFactory.MockClientChannel.Setup(
		//		m => m.GetDocuments(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(new[] { "1" });
		//	_mockDataSourceChannelFactory.MockClientChannel.Setup(m =>
		//		m.CreateNewFromZip(It.IsAny<CreateNewFromZipRequest>())).Returns(new CreateNewFromZipResponse("1"));
		//	_mockDocumentChannelFactory.MockClientChannel.Setup(m =>
		//		m.GetProperties(It.IsAny<Kswapi.ExternalServices.XmPie.Document.GetPropertiesRequest>())).Returns(
		//		new Kswapi.ExternalServices.XmPie.Document.GetPropertiesResponse(new[]
		//	{
		//		new Kswapi.ExternalServices.XmPie.Document.Property { m_Name = "documentType", m_Value = "INDD" }
		//	}));
		//	_mockJobChannelFactory.MockClientChannel.Setup(m =>
		//		m.GetStatus(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).Returns(1);
		//	_mockJobTicketChannelFactory.MockClientChannel.Setup(m => m.CreateNewTicketForDocument(
		//		It.IsAny<CreateNewTicketForDocumentRequest>())).Returns(
		//		new CreateNewTicketForDocumentResponse(new CreateNewTicketForDocumentResponseBody("1")));
		//	_mockJobTicketChannelFactory.MockClientChannel.Setup(m => m.AddDestinationByID(
		//		It.IsAny<AddDestinationByIDRequest>())).Returns(
		//		new AddDestinationByIDResponse(new AddDestinationByIDResponseBody(true)));
		//	_mockJobTicketChannelFactory.MockClientChannel.Setup(m =>
		//		m.SetOutputInfo(It.IsAny<SetOutputInfoRequest>())).Returns(new SetOutputInfoResponse(new SetOutputInfoResponseBody(true)));
		//	_mockJobTicketChannelFactory.MockClientChannel.Setup(m =>
		//		m.SetJobType(It.IsAny<SetJobTypeRequest>())).Returns(new SetJobTypeResponse(new SetJobTypeResponseBody(true)));
		//	_mockProductionChannelFactory.MockClientChannel.Setup(m =>
		//		m.SubmitJob(It.IsAny<SubmitJobRequest>())).Returns(new SubmitJobResponse(new SubmitJobResponseBody("1")));
		//	_mockProductionChannelFactory.MockClientChannel.Setup(m =>
		//		m.SubmitJob(It.IsAny<SubmitJobRequest>())).Returns(new SubmitJobResponse(new SubmitJobResponseBody("1")));

		//	// act
		//	string id = fulfiller.SubmitPrintJob("61");

		//	// assert
		//	Assert.AreEqual("1", id);
		//}

		//[Ignore] // Increments counters with XMPie, only run manually
		//[TestCategory("Integration")]
		//[TestMethod]
		//public void UpdateDataSource_WithActualAndSuccess_ReturnsValidId()
		//{
		//	// arrange
		//	IRenderingModeFulfiller fulfiller = CreateFulfillerWithRealXmPieServices();

		//	// upload campaign file
		//	//string dataSourceId = xmPieServices.ChangeDataSource("61");

		//	// change data source

		//	// act
		//	string result = fulfiller.ChangeDataSource("Sample", @"C:\DropFolderTemp\DataSource.csv");

		//	// assert
		//	int id = Convert.ToInt32(result);
		//	Assert.IsTrue(id > 0, "Expected Result to be numeric and greater than zero.");
		//}

		//[Ignore] // Sumbits an actual job with XMPie, only run manually
		//[TestCategory("Integration")]
		//[TestMethod]
		//public void SubmitPrintJob_WithActualAndSuccess_ReturnsValidId()
		//{
		//	// arrange
		//	IRenderingModeFulfiller fulfiller = CreateFulfillerWithRealXmPieServices();

		//	// upload campaign file
		//	//string dataSourceId = xmPieServices.ChangeDataSource("61");

		//	// change data source

		//	// act
		//	string result = fulfiller.SubmitPrintJob("64");

		//	// assert
		//	int id = Convert.ToInt32(result);
		//	Assert.IsTrue(id > 0, "Expected Result to be numeric and greater than zero.");
		//}

//		[Ignore] // depends on an actual failed job to exist on the uProduce server
//		[TestCategory("Integration")]
//		[TestMethod]
//		public void GetStatus_FailedJob_ReturnsExpectedStatus()
//		{
//			// arrange
//			IRenderingModeFulfiller fulfiller = CreateFulfillerWithRealXmPieServices();

//			// upload campaign file
//			//string dataSourceId = xmPieServices.ChangeDataSource("61");

//			// change data source

//			// act
//			JobStatus jobStatus = fulfiller.GetStatus("10345");

//			// assert
//			Assert.AreEqual(JobState.Failed, jobStatus.State);
//			var messages = jobStatus.Messages.ToList();
//			Assert.AreEqual(2, messages.Count());
//			Assert.AreEqual(@"There is a type mismatch between the field types of the recipients filter and the Plan file Recipient Information Schema.<br>
//The mismatch(es) occurred between (<i>Plan type - filter type</i>):<br>
//PresenceOfChildren(boolean - number).
//", messages[0]);
//			Assert.AreEqual(@"Fatal generic system error: XMPieProductionInitialize. exception - Unspecified error
//", messages[1]);

//		}

		// Remote connection strings do not work easily, specific directory
		// structure required as well that will not be easy to mimic within
		// KSW API

		//[TestCategory("Integration")]
		//[TestMethod]
		//public void OleDbConnection_LocalExcel()
		//{
		//	const string CONNECTION_STRING = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Users\Jason\Documents\Jobs\Engage360\xmpie\People.xlsx;Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"";";
		//	const string COMMAND = "SELECT FirstName FROM [People$];";
		//	using (OleDbConnection connection = new OleDbConnection(CONNECTION_STRING))
		//	{
		//		connection.Open();
		//		IDataReader reader = new OleDbCommand(COMMAND, connection).ExecuteReader();
		//		reader.Read();
		//		Assert.AreEqual("Muriel", reader[0]);
		//		reader.Close();
		//	}
		//}

		//[TestCategory("Integration")]
		//[TestMethod]
		//public void OleDbConnection_LocalText()
		//{
		//	const string CONNECTION_STRING = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=C:\Users\Jason\Documents\Jobs\Engage360\xmpie\;Extended Properties=""text;HDR=Yes"";";
		//	const string COMMAND = "SELECT FirstName FROM People.txt;";
		//	using (OleDbConnection connection = new OleDbConnection(CONNECTION_STRING))
		//	{
		//		connection.Open();
		//		IDataReader reader = new OleDbCommand(COMMAND, connection).ExecuteReader();
		//		reader.Read();
		//		Assert.AreEqual("Muriel", reader[0]);
		//		reader.Close();
		//	}
		//}

		//[TestCategory("Integration")]
		//[TestMethod]
		//public void OleDbConnection_RemoteText()
		//{
		//	//const string CONNECTION_STRING = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=http://www.andrewpatton.com/;Extended Properties=""text;HDR=Yes"";";
		//	const string CONNECTION_STRING = @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=http://extranet.who.int/tme/;Extended Properties=""text;HDR=Yes"";";
		//	//const string CONNECTION_STRING = @"Provider=MS Remote;Remote Provider=Microsoft.Jet.OLEDB.4.0;Remote Server=http://www.andrewpatton.com/;Extended Properties=""text;HDR=Yes"";";
		//	const string COMMAND = "SELECT [Common Name] FROM [generateCSV.asp?ds=dictionary];";
		//	//const string COMMAND = "SELECT [Common Name] FROM countrylist.csv;";
		//	using (OleDbConnection connection = new OleDbConnection(CONNECTION_STRING))
		//	{
		//		connection.Open();
		//		IDataReader reader = new OleDbCommand(COMMAND, connection).ExecuteReader();
		//		reader.Read();
		//		Assert.AreEqual("Afghanistan", reader[0]);
		//		reader.Close();
		//	}
		//}

		//[TestCategory("Integration")]
		//[TestMethod]
		//public void OleDbConnection_RemoteExcel()
		//{
		//	const string CONNECTION_STRING = @"Provider=Microsoft.Jet.OLEDB.4.0;Locaiton=portal.atmedicausa.com;Data Source=""/sites/krames/clinicalmessaging/Shared Documents/Beta_Docs/Consumer CRM Competitive Market 2013.xlsx"";Extended Properties=""Excel 8.0;HDR=Yes;IMEX=1"";User ID='jrichmond';Password='TY7f';";
		//	const string COMMAND = "SELECT [Tea Leaves Health] FROM [Sheet1$];";
		//	using (OleDbConnection connection = new OleDbConnection(CONNECTION_STRING))
		//	{
		//		connection.Open();
		//		IDataReader reader = new OleDbCommand(COMMAND, connection).ExecuteReader();
		//		reader.Read();
		//		reader.Read();
		//		Assert.AreEqual("YES", reader[0]);
		//		reader.Close();
		//	}
		//}

		private IRenderingModeFulfiller CreateFulfillerWithMockXmPieServices()
		{
			_mockAccountChannelFactory = new MockClientChannelFactory<Account_SSPSoapChannel>();
			_mockCampaignChannelFactory = new MockClientChannelFactory<Campaign_SSPSoapChannel>();
			_mockDataSourceChannelFactory = new MockClientChannelFactory<DataSource_SSPSoapChannel>();
			_mockDataSourcePlanUtilsChannelFactory = new MockClientChannelFactory<DataSourcePlanUtils_SSPSoapChannel>();
			_mockDocumentChannelFactory = new MockClientChannelFactory<Document_SSPSoapChannel>();
			_mockJobChannelFactory = new MockClientChannelFactory<Job_SSPSoapChannel>();
			_mockJobTicketChannelFactory = new MockClientChannelFactory<JobTicket_SSPSoapChannel>();
			_mockPlanUtilsChannelFactory = new MockClientChannelFactory<PlanUtils_SSPSoapChannel>();
			_mockProductionChannelFactory = new MockClientChannelFactory<Production_SSPSoapChannel>();
			return new XmPieRenderingModeFulfiller(
				new Service<Account_SSPSoapChannel>(_mockAccountChannelFactory),
				new Service<Campaign_SSPSoapChannel>(_mockCampaignChannelFactory),
				new Service<DataSource_SSPSoapChannel>(_mockDataSourceChannelFactory),
				new Service<DataSourcePlanUtils_SSPSoapChannel>(_mockDataSourcePlanUtilsChannelFactory),
				new Service<Document_SSPSoapChannel>(_mockDocumentChannelFactory),
				new Service<Job_SSPSoapChannel>(_mockJobChannelFactory),
				new Service<JobTicket_SSPSoapChannel>(_mockJobTicketChannelFactory),
				new Service<PlanUtils_SSPSoapChannel>(_mockPlanUtilsChannelFactory),
				new Service<Production_SSPSoapChannel>(_mockProductionChannelFactory),
				_fakeLayer.Settings);
		}

		private IRenderingModeFulfiller CreateFulfillerWithRealXmPieServices()
		{
			return new XmPieRenderingModeFulfiller(
				XmPieServiceHelpers.CreateXmPieService<Account_SSPSoapChannel>("Account"),
				XmPieServiceHelpers.CreateXmPieService<Campaign_SSPSoapChannel>("Campaign"),
				XmPieServiceHelpers.CreateXmPieService<DataSource_SSPSoapChannel>("DataSource"),
				XmPieServiceHelpers.CreateXmPieService<DataSourcePlanUtils_SSPSoapChannel>("DataSourcePlanUtils"),
				XmPieServiceHelpers.CreateXmPieService<Document_SSPSoapChannel>("Document"),
				XmPieServiceHelpers.CreateXmPieService<Job_SSPSoapChannel>("Job"),
				XmPieServiceHelpers.CreateXmPieService<JobTicket_SSPSoapChannel>("JobTicket"),
				XmPieServiceHelpers.CreateXmPieService<PlanUtils_SSPSoapChannel>("PlanUtils"),
				XmPieServiceHelpers.CreateXmPieService<Production_SSPSoapChannel>("Production"),
				_fakeLayer.Settings);
		}
	}
}