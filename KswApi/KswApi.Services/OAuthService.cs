﻿using KswApi.Interface;
using KswApi.Interface.Objects;
using KswApi.Logic;
using KswApi.Service.Framework;

namespace KswApi.Services
{
	public class OAuthService : IOAuthService
	{
		public OAuthTokenResponse CreateAccessToken(OAuthTokenRequest request)
		{
			OAuthLogic logic = new OAuthLogic();

			return logic.CreateAccessToken(request, Operation.Current.Headers, Operation.Current.SourceAddress);
		}
	}
}
