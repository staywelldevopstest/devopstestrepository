﻿using KswApi.Interface;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.ContentLogic;

namespace KswApi.Services
{
	public class LanguageService : ILanguageService
	{
		public LanguageList GetLanguages(string bucketIdOrSlug, string contentIdOrSlug)
		{
			ContentLogic logic = new ContentLogic();
			return logic.GetLanguages(bucketIdOrSlug, contentIdOrSlug);
		}
	}
}
