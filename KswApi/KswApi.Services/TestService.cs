﻿using KswApi.Interface;
using KswApi.Interface.Objects;
using KswApi.Logging;
using KswApi.Logic;
using System.ServiceModel;
using System.ServiceModel.Activation;
using KswApi.Service.Framework;

namespace KswApi.Services
{
	[MessageContract]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, Name = "Test", Namespace = "http://www.kramesstaywell.com")]
	public class TestService : ITestService
	{
		public PingData Ping()
		{
			TestLogic testLogic = new TestLogic();

			return testLogic.GetPingData();
		}

		public void Error()
		{
			TestLogic testLogic = new TestLogic();

			testLogic.CreateError();
		}

		public StreamResponse Stream(StreamRequest request)
		{
			TestLogic logic = new TestLogic();

			StreamResponse response = Operation.Current.StreamResponse;
			
			logic.Stream(response, request);
			
			return response;
		}
	}
}