﻿using System.Linq;
using KswApi.Interface;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.ContentLogic;

namespace KswApi.Services
{
    public class ServicelinesService: IServicelinesService
    {
        public HierarchicalTaxonomyList SearchServicelines(string path)
        {
            HierarchicalTaxonomyLogic logic = new HierarchicalTaxonomyLogic();
            return logic.GetHierarchicalTaxonomiesByPath(HierarchicalTaxonomyType.Serviceline, path);
        }

        public HierarchicalTaxonomyResponse GetServicelines(string slug, string path)
        {
            HierarchicalTaxonomyLogic logic = new HierarchicalTaxonomyLogic();
            return logic.GetHierarchicalTaxonomy(HierarchicalTaxonomyType.Serviceline, slug, path);
        }
    }
}
