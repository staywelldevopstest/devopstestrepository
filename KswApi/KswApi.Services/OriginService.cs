﻿using KswApi.Interface;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.ContentLogic;

namespace KswApi.Services
{
	public class OriginService : IOriginService
	{
		#region Origins

		public ContentOriginList GetBucketOrigins()
		{
			BucketLogic logic = new BucketLogic();
			return logic.GetBucketOrigins();
		}

		#endregion
	}
}
