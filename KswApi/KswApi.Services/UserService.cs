﻿using KswApi.Interface;
using KswApi.Interface.Objects;
using KswApi.Logic.Administration;
using KswApi.Service.Framework;

namespace KswApi.Services
{
	public class UserService : IUserService
	{
		public AuthenticatedUserResponse GetCurrentUser(bool startTimer)
		{
			UserLogic userLogic = new UserLogic();

			return userLogic.GetUserDetails(startTimer, Operation.Current.AccessToken);
		}

		public UserSettingsResponse GetCurrentUserSettings()
		{
			UserLogic userLogic = new UserLogic();
			return userLogic.GetUserSettings(Operation.Current.AccessToken);
		}

		public UserSettingsResponse UpdateCurrentUserSettings(UserSettingsRequest userSettings)
		{
			UserLogic userLogic = new UserLogic();
			return userLogic.UpdateUserSettings(Operation.Current.AccessToken, userSettings);
		}

		public ClientLicenseDetail GetUserLicenseDetails()
		{
			UserLogic userLogic = new UserLogic();

			return userLogic.GetUserLicenseDetails(Operation.Current.AccessToken);
		}
	}
}
