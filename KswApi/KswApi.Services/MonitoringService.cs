﻿using System.Collections.Generic;
using System.Web;
using KswApi.Interface;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.Monitoring;
using KswApi.Logic;

namespace KswApi.Services
{
	public class MonitoringService : IMonitoringService
	{
		public MonitoringStatus ServerStatus()
		{
			MonitoringLogic logic = new MonitoringLogic();

			return logic.CheckStatus();
		}

		public ServerModuleList Modules()
		{
			HttpApplication application = HttpContext.Current.ApplicationInstance;

			HttpModuleCollection modules = application.Modules;

			return new ServerModuleList
				   {
					   Total = modules.Count,
					   Items = new List<string>(modules.AllKeys)
				   };
		}
	}
}