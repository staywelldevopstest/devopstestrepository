﻿using KswApi.Interface;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Logic.Tasks;
using System;

namespace KswApi.Services
{
    public class TasksService : ITasksService
    {
        #region Settings and General Methods

        public TaskServiceSettings GetServiceSettings()
        {
            TasksServiceLogic logic = new TasksServiceLogic();

            return logic.GetServiceSettings();
        }

        public Task GetTaskSettings(TaskType taskType)
        {
            TasksServiceLogic logic = new TasksServiceLogic();

            return logic.GetTaskSettings(taskType);
        }

        public void StartedTask(Task task)
        {
            TasksServiceLogic logic = new TasksServiceLogic();

            logic.StartedTask(task);
        }

        public void FinishedTask(Task task)
        {
            TasksServiceLogic logic = new TasksServiceLogic();

            logic.EndedTask(task);
        }

        public void LogTaskException(Task task, Exception ex)
        {
            TasksServiceLogic logic = new TasksServiceLogic();

            logic.LogTaskException(task, ex);
        }

        public ServiceCheck ServiceCheck()
        {
            TasksServiceLogic logic = new TasksServiceLogic();

            return logic.ServiceCheck();
        }

	    #endregion

        #region Task Specific Methods

		public TaskStepResponse DoTaskStep(TaskType type)
		{
			TasksServiceLogic logic = new TasksServiceLogic();

			return logic.DoTaskStep(type);
		}
      

        #endregion
    }
}
