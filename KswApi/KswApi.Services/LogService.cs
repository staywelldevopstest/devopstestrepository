﻿using KswApi.Interface;
using KswApi.Interface.Objects;
using KswApi.Logic;

namespace KswApi.Services
{
    public class LogService : ILogService
    {
        public LogList GetLogs(int offset, int count, string filter, string sort)
        {
            LogLogic logLogic = new LogLogic();

            return logLogic.GetLogs(offset, count, sort, filter, null);
        }

        public Log GetLog(string logId)
        {
            LogLogic logLogic = new LogLogic();

            return logLogic.GetLog(logId);
        }
    }
}