﻿using KswApi.Interface;
using KswApi.Interface.Objects.Rendering;
using KswApi.Logic.Rendering;
using KswApi.Service.Framework;

namespace KswApi.Services
{
	public class RenderingService : IRenderingService
	{
		public RenderResponse Render(RenderRequest request)
		{
			RenderingLogic logic = new RenderingLogic();
			return logic.Render(request, Operation.Current.AccessToken, Operation.Current.StreamResponse);
		}
	}
}
