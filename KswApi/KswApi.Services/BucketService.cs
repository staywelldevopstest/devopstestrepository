﻿using KswApi.Interface;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.ContentLogic;

namespace KswApi.Services
{
	public class BucketService : IBucketService
	{
		#region Buckets

		public ContentBucketResponse CreateBucket(ContentBucketCreateRequest bucket)
		{
			BucketLogic logic = new BucketLogic();
			return logic.CreateBucket(bucket);
		}

		public ContentBucketList SearchBuckets(BucketSearchRequest request)
		{
			BucketLogic logic = new BucketLogic();
			return logic.SearchBuckets(request);
		}

		public ContentBucketResponse GetBucket(string idOrSlug)
		{
			BucketLogic logic = new BucketLogic();
			return logic.GetBucket(idOrSlug);
		}

        public ContentBucketResponse UpdateBucket(string idOrSlug, ContentBucketUpdateRequest bucket)
		{
			BucketLogic logic = new BucketLogic();
			return logic.UpdateBucket(idOrSlug, bucket);
		}

		public ContentBucketResponse DeleteBucket(string idOrSlug)
		{
			BucketLogic logic = new BucketLogic();
			return logic.DeleteBucket(idOrSlug);
		}

	    public SlugResponse GetSlug(string title, string slug)
	    {
	        BucketLogic logic = new BucketLogic();
	        return logic.GetSlug(slug, title);
	    }

		#endregion
	}
}
