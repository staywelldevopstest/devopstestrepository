﻿using KswApi.Interface;
using KswApi.Interface.Objects;
using KswApi.Logic;

namespace KswApi.Services
{
	public class SecurityService : ISecurityService
	{
		#region Implementation of ISecurityService

		public ValidationResponse ValidateAccessToken(string token)
		{
			OAuthLogic logic = new OAuthLogic();

			return logic.ValidateAccessToken(token);			
		}

		#endregion
	}
}
