﻿using KswApi.Interface;
using KswApi.Interface.Objects;
using KswApi.Logic;

namespace KswApi.Services
{
	class RootService : IRootService
	{
		public ServiceInformation GetServiceInformation()
		{
			RootLogic rootLogic = new RootLogic();
			return rootLogic.GetServiceInformation();
		}
	}
}
