﻿using KswApi.Interface;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.ContentLogic;
using KswApi.Service.Framework;

namespace KswApi.Services
{
    public class CollectionService : ICollectionService
    {
        public CollectionResponse CreateCollection(CollectionCreateRequest collection)
        {
            CollectionLogic logic = new CollectionLogic();
            return logic.CreateCollection(collection, Operation.Current.AccessToken);
        }

        public CollectionListResponse SearchCollections(CollectionSearchRequest request)
        {
            CollectionLogic logic = new CollectionLogic();
            return logic.SearchCollections(request, Operation.Current.AccessToken);
        }

        public CollectionResponse GetCollection(string idOrSlug, bool? includeChildren, bool? recursive)
        {
            CollectionLogic logic = new CollectionLogic();
            return logic.GetCollection(idOrSlug, includeChildren, recursive, Operation.Current.AccessToken);
        }

        public CollectionResponse UpdateCollection(string idOrSlug, CollectionRequest collection)
        {
            CollectionLogic logic = new CollectionLogic();
            return logic.UpdateCollection(idOrSlug, collection, Operation.Current.AccessToken);
        }

        public CollectionResponse DeleteCollection(string idOrSlug)
        {
            CollectionLogic logic = new CollectionLogic();
            return logic.DeleteCollection(idOrSlug, Operation.Current.AccessToken);
        }

        public SlugResponse GetSlug(string title, string slug)
        {
            CollectionLogic logic = new CollectionLogic();
            return logic.GetSlug(slug, title);
        }
    }
}