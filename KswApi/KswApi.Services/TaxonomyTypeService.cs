﻿using KswApi.Interface;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.ContentLogic;

namespace KswApi.Services
{
    public class TaxonomyTypeService: ITaxonomyTypeService
    {
        public TaxonomyTypeList SearchTaxonomyTypes()
        {
            TaxonomyTypeLogic logic = new TaxonomyTypeLogic();
            return logic.SearchTaxonomyTypes();
        }

        public TaxonomyTypeResponse GetTaxonomyType(string slug)
        {
            TaxonomyTypeLogic logic = new TaxonomyTypeLogic();
            return logic.GetTaxonomyType(slug);
        }
    }
}
