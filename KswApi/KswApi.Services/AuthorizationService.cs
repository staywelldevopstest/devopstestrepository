﻿using KswApi.Interface.Objects;
using KswApi.Logic;
using KswApi.Logic.Administration;
using KswApi.RestrictedInterface;
using KswApi.RestrictedInterface.Objects;
using System.ServiceModel;
using System.ServiceModel.Activation;
using KswApi.Service.Framework;

namespace KswApi.Services
{
    [MessageContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, Name = "Authorization", Namespace = "http://www.kramesstaywell.com")]
    public class AuthorizationService : IAuthorizationService
    {
		public ApplicationValidationResponse GetApplicationValidation(string responseType, string applicationId, string redirectUri, string scope, string state)
        {
            OAuthLogic authorizationLogic = new OAuthLogic();

			return authorizationLogic.GetApplicationValidation(responseType, applicationId, redirectUri, scope, state);
        }

        public AuthorizationGrantResponse CreateAuthorizationGrant(AuthorizationGrantRequest request)
        {
            OAuthLogic authorizationLogic = new OAuthLogic();

            return authorizationLogic.CreateAuthorizationGrant(request);
        }

		public void SetClientUserPassword(PasswordRequest request)
		{
			PasswordLogic logic = new PasswordLogic();

			logic.SetClientUserPassword(request);
		}

		public void ValidateClientUserPasswordToken(string token)
		{
			PasswordLogic logic = new PasswordLogic();

			logic.ValidatePasswordToken(token);
		}

		public void ResetClientUserPassword(PasswordResetRequest request)
		{
			PasswordLogic logic = new PasswordLogic();

			logic.ResetClientUserPassword(request);
		}

	    public KeepAliveResponse KeepAlive(int idleMilliseconds)
	    {
		    OAuthLogic logic = new OAuthLogic();

		    return logic.KeepAlive(idleMilliseconds, Operation.Current.AccessToken);
	    }
    }
}
