﻿using System.ServiceModel;
using System.ServiceModel.Activation;
using KswApi.Interface;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects.SmsGateway;
using KswApi.Logic.SmsLogic;

namespace KswApi.Services.SmsGateway
{
	[MessageContract]
	[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
	[ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, Name = "Administration", Namespace = "http://www.kramesstaywell.com")]
	public class SmsGatewayAdministrationService : ISmsGatewayAdministrationService
	{
		#region Sms Module
		
		public SmsGatewayModule GetSmsGateway(string licenseId)
		{
			SmsAdministrationLogic logic = new SmsAdministrationLogic();

			return logic.GetModule(licenseId);
		}

		public ShortCode GetShortCode(string licenseId, string shortCodeId)
		{
			SmsAdministrationLogic logic = new SmsAdministrationLogic();

			return logic.GetShortCode(licenseId, shortCodeId);
		}

		public ShortCode CreateShortCode(string licenseId, ShortCode shortCode)
		{
			SmsAdministrationLogic logic = new SmsAdministrationLogic();

			return logic.CreateShortCode(licenseId, shortCode);
		}

		public ShortCode UpdateShortCode(string licenseId, string shortCodeNumber, ShortCode shortCode)
		{
			SmsAdministrationLogic logic = new SmsAdministrationLogic();

			return logic.UpdateShortCode(licenseId, shortCodeNumber, shortCode);
		}

		public ShortCode DeleteShortCode(string licenseId, string shortCodeNumber)
		{
			SmsAdministrationLogic logic = new SmsAdministrationLogic();

			return logic.DeleteShortCode(licenseId, shortCodeNumber);
		}

		public AvailableLongCodeList SearchLongCodes(string licenseId, int offset, int count, string filter, SmsLongCodeFilterType filterType)
		{
			SmsAdministrationLogic logic = new SmsAdministrationLogic();
			
			return logic.SearchLongCodes(licenseId, offset, count, filter, filterType);
		}

		public LongCode GetLongCode(string licenseId, string longCodeNumber)
		{
			SmsAdministrationLogic logic = new SmsAdministrationLogic();

			return logic.GetLongCode(licenseId, longCodeNumber);
		}

		public LongCode PurchaseLongCode(string licenseId, LongCode longCode)
		{
			SmsAdministrationLogic logic = new SmsAdministrationLogic();

			return logic.PurchaseLongCode(licenseId, longCode);
		}

		public LongCode UpdateLongCode(string licenseId, string longCodeNumber, LongCode longCode)
		{
			SmsAdministrationLogic logic = new SmsAdministrationLogic();

			return logic.UpdateLongCode(licenseId, longCodeNumber, longCode);
		}

		public LongCode DeleteLongCode(string licenseId, string longCodeNumber)
		{
			SmsAdministrationLogic logic = new SmsAdministrationLogic();

			return logic.DeleteLongCode(licenseId, longCodeNumber);
		}

		#endregion Sms Module
	}
}