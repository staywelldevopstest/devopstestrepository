﻿using System;
using KswApi.Interface;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.SmsGateway;
using KswApi.Logging;
using KswApi.Logic.SmsLogic;
using KswApi.Service.Framework;
using KswApi.ThirdPartyEndpoints.Interfaces;
using KswApi.ThirdPartyEndpoints.Objects;

namespace KswApi.Services.SmsGateway
{
	public class SmsGatewayService : ISmsGatewayService, ITwilioEndpoint
	{
		public IncomingMessage SendMessage(IncomingMessage message)
		{
			SmsMessageLogic logic = new SmsMessageLogic();

			return logic.SendSmsMessage(message, Operation.Current.AccessToken);
		}

		public TwilioResponse TwilioEndpoint(string smsSid, string accountSid, string from, string to, string body)
		{
			SmsMessageLogic logic = new SmsMessageLogic();

			logic.ProcessSmsMessage(smsSid, accountSid, from, to, body);

			return new TwilioResponse();
		}
	}
}
