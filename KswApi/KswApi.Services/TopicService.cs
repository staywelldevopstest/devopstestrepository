﻿using KswApi.Interface;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.ContentLogic;
using KswApi.Service.Framework;

namespace KswApi.Services
{
	public class TopicService : ITopicService
	{
		#region Implementation of ITopicService

		public TopicResponse GetTopic(string idOrSlug, bool? includeChildren, bool? recursive)
		{
			TopicLogic logic = new TopicLogic();
			return logic.GetTopic(idOrSlug, includeChildren, recursive, Operation.Current.AccessToken);
		}

		#endregion
	}
}
