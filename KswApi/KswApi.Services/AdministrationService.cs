﻿using KswApi.Interface;
using KswApi.Interface.Objects;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.Administration;
using KswApi.Logic.ContentLogic;
using System.ServiceModel;
using System.ServiceModel.Activation;

namespace KswApi.Services
{
    [MessageContract]
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    [ServiceBehavior(InstanceContextMode = InstanceContextMode.PerCall, Name = "Administration", Namespace = "http://www.kramesstaywell.com")]
    public class AdministrationService : IAdministrationService
    {
        #region Role Methods

        public Role CreateRole(RoleRequest role)
        {
            RoleLogic roleLogic = new RoleLogic();
            return roleLogic.CreateRole(role);
        }

        public RoleList GetRoles()
        {
            RoleLogic roleLogic = new RoleLogic();
            return roleLogic.GetAllRoles();
        }

        public Role GetRole(string roleId)
        {
            RoleLogic roleLogic = new RoleLogic();
            return roleLogic.GetRole(roleId);
        }

        public Role UpdateRole(string roleId, RoleRequest role)
        {
            RoleLogic roleLogic = new RoleLogic();
            return roleLogic.UpdateRole(roleId, role);
        }

        public Role DeleteRole(string roleId)
        {
            RoleLogic roleLogic = new RoleLogic();
            return roleLogic.DeleteRole(roleId);
        }

        public RightList GetRights()
        {
            RoleLogic roleLogic = new RoleLogic();

            return roleLogic.GetAllRights();
        }

        #endregion Role Methods

        #region Client Methods

        public ClientList GetClients(int offset, int count, string query, string sort)
        {
            ClientLogic logic = new ClientLogic();

            return logic.GetClients(offset, count, query, sort);
        }

        public ClientResponse GetClient(string clientId)
        {
            ClientLogic logic = new ClientLogic();

            return logic.GetClient(clientId);
        }

        public ClientResponse CreateClient(ClientRequest client)
        {
            ClientLogic logic = new ClientLogic();

            return logic.CreateClient(client);
        }

        public ClientResponse UpdateClient(string clientId, ClientRequest client)
        {
            ClientLogic logic = new ClientLogic();

            return logic.UpdateClient(clientId, client);
        }

        public ClientResponse DeleteClient(string clientId)
        {
            ClientLogic logic = new ClientLogic();

            return logic.DeleteClient(clientId);
        }

	    public LicenseList GetLicenses(int offset, int count, string query, string clientId, string orderBy)
        {
            LicenseLogic logic = new LicenseLogic();

            return logic.GetLicenses(offset, count, query, clientId, orderBy);
        }

        public AvailableParentLicenseList GetAvailableParentLicenses(string clientId, string licenseId, string orderBy)
        {
            LicenseLogic logic = new LicenseLogic();

            return logic.GetAvailableParentLicenses(clientId, licenseId, orderBy);
        }

        #endregion Client Methods

        #region License Methods

        public License GetLicense(string licenseId)
        {
            LicenseLogic logic = new LicenseLogic();

            return logic.GetLicense(licenseId);
        }

        public License CreateLicense(LicenseRequest license)
        {
            LicenseLogic logic = new LicenseLogic();

            return logic.CreateLicense(license);
        }

        public License UpdateLicense(string licenseId, LicenseRequest license)
        {
            LicenseLogic logic = new LicenseLogic();

            return logic.UpdateLicense(licenseId, license);
        }

        public License DeleteLicense(string licenseId)
        {
            LicenseLogic logic = new LicenseLogic();

            return logic.DeleteLicense(licenseId);
        }

        public ClientLicenseDetail GetClientLicenseDetail(string clientId, string search)
        {
            LicenseLogic logic = new LicenseLogic();

            return logic.GetClientLicenseDetail(clientId, search);
        }

        public ApplicationList GetLicenseApplications(string licenseId)
        {
            LicenseLogic logic = new LicenseLogic();

            return logic.GetLicenseApplications(licenseId);
        }

        public Application CreateLicenseApplication(string licenseId, ApplicationRequest application)
        {
            LicenseLogic logic = new LicenseLogic();

            return logic.CreateLicenseApplication(licenseId, application);
        }

        public Application DeleteLicenseApplication(string licenseId, string applicationId)
        {
            LicenseLogic logic = new LicenseLogic();

            return logic.DeleteLicenseApplication(licenseId, applicationId);
        }

        public Application GetLicenseApplication(string licenseId, string applicationId)
        {
            LicenseLogic logic = new LicenseLogic();

            return logic.GetLicenseApplication(licenseId, applicationId);
        }

        public Application UpdateLicenseApplication(string licenseId, string applicationId, ApplicationRequest application)
        {
            LicenseLogic logic = new LicenseLogic();

            return logic.UpdateLicenseApplication(licenseId, applicationId, application);
        }

		public Application ResetLicenseApplicationSecret(string licenseId, string applicationId)
		{
			LicenseLogic logic = new LicenseLogic();

			return logic.ResetLicenseApplicationSecret(licenseId, applicationId);
		}
		
		#endregion License Methods

        #region Module Methods

        public ModuleNameList GetModules()
        {
            ModuleLogic logic = new ModuleLogic();

            return logic.GetModules();
        }

        public LicenseModuleList GetLicenseModules(string licenseId)
        {
            ModuleLogic logic = new ModuleLogic();

            return logic.GetLicenseModules(licenseId);
        }

        public ModuleName AddLicenseModule(string licenseId, string moduleId)
        {
            ModuleLogic logic = new ModuleLogic();

            return logic.AddLicenseModule(licenseId, moduleId);
        }

        public ModuleName RemoveLicenseModule(string licenseId, string moduleId)
        {
            ModuleLogic logic = new ModuleLogic();

            return logic.RemoveLicenseModule(licenseId, moduleId);
        }

		public CollectionListResponse GetLicenseCollections(string licenseId, CollectionSearchRequest request)
		{
			ContentModuleLogic logic = new ContentModuleLogic();

			return logic.GetCollectionsInLicense(licenseId, request);
		}

		public UnpagedCollectionListResponse AddLicenseCollections(string licenseId, CollectionIdListRequest collectionList)
		{
			ContentModuleLogic logic = new ContentModuleLogic();

			return logic.AddCollectionsToLicense(licenseId, collectionList);
		}

		public CollectionResponse RemoveLicenseCollection(string licenseId, string collectionId)
		{
			ContentModuleLogic logic = new ContentModuleLogic();

			return logic.RemoveCollectionFromLicense(licenseId, collectionId);
		}

        #endregion Module Methods

		#region Copyright Methods

		public CopyrightList GetCopyrights(int offset, int count, string query, bool includeBucketCount)
		{
			CopyrightLogic logic = new CopyrightLogic();

			return logic.GetCopyrightList(offset, count, query, includeBucketCount);
		}

		public CopyrightResponse CreateCopyright(CopyrightRequest copyright)
		{
			CopyrightLogic logic = new CopyrightLogic();

			return logic.CreateCopyright(copyright);
		}

		public CopyrightResponse UpdateCopyright(string copyrightId, CopyrightRequest copyright)
		{
			CopyrightLogic logic = new CopyrightLogic();

			return logic.UpdateCopyright(copyrightId, copyright);
		}

		
		public CopyrightResponse DeleteCopyright(string copyrightId)
		{
			CopyrightLogic logic = new CopyrightLogic();

			return logic.DeleteCopyright(copyrightId);
		}

		#endregion

		#region Client User Management Methods

		public ClientUserList GetClientUsers(int offset, int count, string query, string sort, string filter)
        {
            ClientUserLogic logic = new ClientUserLogic();

            return logic.GetClientUsers(offset, count, query, sort, filter);
        }

        public ClientUserResponse GetClientUser(string userIdOrEmail)
        {
            ClientUserLogic logic = new ClientUserLogic();

            return logic.GetClientUser(userIdOrEmail);
        }

        public ClientUserResponse CreateClientUser(ClientUserRequest clientUser)
        {
            ClientUserLogic logic = new ClientUserLogic();

            return logic.CreateClientUser(clientUser);
        }

		public ClientUserResponse UpdateClientUser(string clientUserId, ClientUserUpdateRequest clientUser)
        {
            ClientUserLogic logic = new ClientUserLogic();

            return logic.UpdateClientUser(clientUserId, clientUser);
        }

	    public void UnlockClientUser(string userIdOrEmail)
	    {
			ClientUserLogic logic = new ClientUserLogic();

			logic.UnlockClientUser(userIdOrEmail);
	    }

	    public ClientUserResponse DeleteClientUser(string clientUserId)
        {
            ClientUserLogic logic = new ClientUserLogic();

            return logic.DeleteClientUser(clientUserId);
        }

        public ClientUserValidation ValidateClientUserEmail(string emailAddress)
        {
            ClientUserLogic logic = new ClientUserLogic();

            return logic.ValidateClientUserEmail(emailAddress);
        }

	    #endregion

		#region License Buckets

		public UnpagedContentBucketList GetLicenseBuckets(string licenseId)
		{
			ContentModuleLogic logic = new ContentModuleLogic();

			return logic.GetBucketsAssignedToLicense(licenseId);
		}

		public UnpagedContentBucketList UpdateLicenseBuckets(string licenseId, ContentBucketIdListRequest bucketList)
		{
			ContentModuleLogic logic = new ContentModuleLogic();

			return logic.UpdateBucketsAssignedToLicense(licenseId, bucketList);
		}

	    #endregion
	}
}
