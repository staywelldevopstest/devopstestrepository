﻿using System;
using System.Collections.Generic;
using System.Web;
using KswApi.Interface;
using KswApi.Interface.Objects.Content;
using KswApi.Logic.ContentLogic;
using KswApi.Service.Framework;
using KswApi.Interface.Objects;

namespace KswApi.Services
{
	public class ContentService : IContentService
	{
		#region Content

		public ContentList SearchContent(ContentSearchRequest request)
		{
			ContentLogic logic = new ContentLogic();

			return logic.SearchContent(request, Operation.Current.Application);
		}

		public ContentArticleResponse GetContent(string bucketIdOrSlug, string idOrSlug, bool includeBody, bool draft, DateTime? time)
		{
			ContentLogic logic = new ContentLogic();
            return logic.GetContent(bucketIdOrSlug, idOrSlug, includeBody, Operation.Current.AccessToken, draft, time);
		}

		public ContentMetadataResponse CreateContent(string bucketIdOrSlug, NewContentRequest content)
		{
			ContentLogic logic = new ContentLogic();
			return logic.CreateContent(bucketIdOrSlug, content);
		}

		public ContentMetadataResponse UpdateContent(string bucketIdOrSlug, string idOrSlug, ContentArticleRequest content)
		{
			ContentLogic logic = new ContentLogic();
			return logic.UpdateContent(bucketIdOrSlug, idOrSlug, content);
		}

		public ContentResponse DeleteContent(string bucketIdOrSlug, string idOrSlug, bool? publishedOnly)
		{
			ContentLogic logic = new ContentLogic();
			return logic.DeleteContent(bucketIdOrSlug, idOrSlug, publishedOnly);
		}

		public SlugResponse GetSlug(string bucketIdOrSlug, string slug, string title)
		{
			ContentLogic logic = new ContentLogic();
			return logic.GetSlug(bucketIdOrSlug, slug, title);
		}

		#endregion

		#region History

		public ContentHistoryItemResponse AddHistoryContent(string bucketIdOrSlug, string contentIdOrSlug, NewContentRequest content)
		{
			ContentLogic logic = new ContentLogic();
			return logic.AddAutosaveHistoryContent(bucketIdOrSlug, contentIdOrSlug, content);
		}

		public ContentHistoryResponse GetHistory(string bucketIdOrSlug, string contentIdOrSlug)
		{
			ContentLogic logic = new ContentLogic();
			return logic.GetHistory(bucketIdOrSlug, contentIdOrSlug);
		}

		public ContentArticleResponse GetHistoryContent(string bucketIdOrSlug, string contentIdOrSlug, string id, bool? includeBody)
		{
			ContentLogic logic = new ContentLogic();
			return logic.GetHistoryContent(bucketIdOrSlug, contentIdOrSlug, id, includeBody);
		}

		#endregion

		#region Images

		public ImageDetailResponse CreateImage(string bucketIdOrSlug, StreamRequest body)
		{
			ImageLogic logic = new ImageLogic();
			return logic.Create(bucketIdOrSlug, body, new HttpFileCollectionWrapper(HttpContext.Current.Request.Files));
		}

		public StreamResponse GetLicenseImage(string licenseId, string bucketIdOrSlug, string imageIdOrSlug)
		{
			ImageLogic logic = new ImageLogic();
			return logic.GetLicenseImage(licenseId, bucketIdOrSlug, imageIdOrSlug, Operation.Current.StreamResponse);
		}

		public StreamResponse GetImage(string bucketIdOrSlug, string imageIdOrSlug)
		{
			ImageLogic logic = new ImageLogic();
			return logic.GetImage(bucketIdOrSlug, imageIdOrSlug, Operation.Current.StreamResponse);
		}

		public ImageDetailResponse UpdateImageDetails(string bucketIdOrSlug, string imageIdOrSlug, ImageDetailRequest request)
		{
			ImageLogic logic = new ImageLogic();
			return logic.UpdateDetails(bucketIdOrSlug, imageIdOrSlug, request);
		}

		public ImageDetailResponse GetImageDetails(string bucketIdOrSlug, string imageIdOrSlug)
		{
			ImageLogic logic = new ImageLogic();
			return logic.GetImageDetails(bucketIdOrSlug, imageIdOrSlug);
		}

		public SlugResponse GetImageSlug(string bucketIdOrSlug, string idOrSlug, string slug, string title)
		{
			ImageLogic logic = new ImageLogic();
			return logic.GetImageSlug(bucketIdOrSlug, idOrSlug, slug, title);
		}

		public ImageDetailResponse DeleteImage(string bucketIdOrSlug, string imageIdOrSlug)
		{
			ImageLogic logic = new ImageLogic();
			return logic.DeleteImage(bucketIdOrSlug, imageIdOrSlug);
		}

		#endregion

		#region Taxonomies

		public TaxonomyListResponse SearchTaxonomies(string bucketIdOrSlug, string contentIdOrSlug)
		{
			ContentLogic logic = new ContentLogic();
			return logic.SearchTaxonomies(bucketIdOrSlug, contentIdOrSlug);
		}

		#endregion
	}
}
