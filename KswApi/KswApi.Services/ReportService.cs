﻿using KswApi.Interface;
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Logic.Report;
using KswApi.Service.Framework;

namespace KswApi.Services
{
    public class ReportService : IReportsService
    {
        public SmsBillingReportDataResponse GetSmsBillingReportData(Month month, int year)
        {
            ReportLogic logic = new ReportLogic();

            return logic.GetSmsBillingReportData(month, year);
        }

		public StreamResponse GetSmsBillingReportCsvExport(Month month, int year)
        {
            ReportLogic logic = new ReportLogic();

            logic.GetSmsBillingReportCsvExport(Operation.Current.StreamResponse, month, year);

			return Operation.Current.StreamResponse;
        }
    }
}
