﻿using KswApi.Interface.Enums;
using KswApi.Interface.Objects;
using KswApi.Tasks.Interfaces;

namespace KswApi.Tasks
{
    public class TrimAuthorizationGrants : TaskBase
    {
        public override void Run(ApiClient client, IScheduler scheduler)
        {
            try
            {
                client.Tasks.StartedTask(Task);

				while (Continue)
				{
					TaskStepResponse response = client.Tasks.DoTaskStep(TaskType.AuthorizationGrantCleanup);

					if (!response.Continue)
						break;
				}
            }
            finally
            {
                client.Tasks.FinishedTask(Task);
            }
        }
    }
}
