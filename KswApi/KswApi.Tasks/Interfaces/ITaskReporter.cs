﻿using System;

namespace KswApi.Tasks.Interfaces
{
	interface ITaskReporter
	{
		void ReportException(Exception exception);
	}
}
