﻿using System;
using KswApi.Interface.Objects;

namespace KswApi.Tasks.Interfaces
{
	public interface IScheduler
	{
		void Schedule<T>(DateTime time) where T : ITask;
		void Schedule<T>(TimeSpan time) where T : ITask;
		void Schedule<T>(Task task) where T : ITask;
	}
}
