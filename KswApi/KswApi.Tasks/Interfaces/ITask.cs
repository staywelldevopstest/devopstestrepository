﻿using System;
using KswApi.Interface.Objects;

namespace KswApi.Tasks.Interfaces
{
	public interface ITask
	{
		void Run(ApiClient client, IScheduler scheduler);
		void Stop();
		DateTime EndTime { get; set; }
		Task Task { get; set; }
	}

}
