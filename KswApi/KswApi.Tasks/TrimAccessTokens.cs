﻿
using KswApi.Interface.Enums;
using KswApi.Interface.Objects;

namespace KswApi.Tasks
{
    public class TrimAccessTokens : TaskBase
    {
        public override void Run(ApiClient client, Interfaces.IScheduler scheduler)
        {
            try
            {
                client.Tasks.StartedTask(Task);

                while (Continue)
                {
	                TaskStepResponse response = client.Tasks.DoTaskStep(TaskType.AccessTokenCleanup);

					if (!response.Continue)
						break;

                };
            }
            finally
            {
                client.Tasks.FinishedTask(Task);
            }
        }
    }
}
