﻿using System;
using KswApi.Interface.Objects;
using KswApi.Tasks.Interfaces;

namespace KswApi.Tasks
{
	public abstract class TaskBase : ITask
	{
		private volatile bool _continue = true;

		public abstract void Run(ApiClient client, IScheduler scheduler);
		
		public void Stop()
		{
			_continue = false;
		}

		public bool Continue
		{
			get { return _continue && EndTime > DateTime.UtcNow; }
		}

		public DateTime EndTime { get; set; }

		public Task Task { get; set; }
	}
}
