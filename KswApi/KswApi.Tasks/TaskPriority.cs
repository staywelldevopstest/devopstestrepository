﻿namespace KswApi.AuxiliaryService.Framework
{
	public enum TaskPriority
	{
		None,
		Immediate,
		Timer
	}
}
