﻿using System;
using KswApi.Tasks.Interfaces;

namespace KswApi.Tasks
{
	public class ConsoleWrite : TaskBase
	{
		public override void Run(ApiClient client, IScheduler scheduler)
		{
			Console.WriteLine("Task ran at {0}", DateTime.Now);
		}
	}
}
