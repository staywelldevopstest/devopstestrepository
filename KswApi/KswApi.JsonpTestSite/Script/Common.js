﻿$(function() {
	Startup.start();
});

var Startup = (function() {
	var self = {
		start: start,
		onResult: onResult
	};

	function start() {
		$('#result').empty();
		$('#submit').click(submit);
	}
	
	function submit() {
		var text = $('#jsonpCall').val();
		var separator = '?';
		if (text.indexOf('?') > 0) {
			separator = '&';
		}
		text += separator + 'callback=Startup.onResult';
		text += '&applicationId=' + $('#appId').val();
		text += '&random=' + Math.random();
		var tag = '<script type="text/javascript" src="' + text + '"></script>';
		$('body').append(tag);
	}
	
	function onResult(data) {
		$('#result').val(JSON.stringify(data));
	}

	return self;
})();