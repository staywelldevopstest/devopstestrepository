﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="KswApi.JsonpTestSite.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title></title>
	<script type="text/javascript" src="Script/jquery-1.8.2.min.js"></script>
	<script type="text/javascript" src="Script/Common.js"></script>
	<style>
		.jsonpCall {
			width: 250px;
		}

		.result {
			width: 500px;
			height: 200px;
			resize: none;
		}

		.appId {
			width: 100px;
		}

		a {
			color: #33a;
			font-weight: bold;
		}
	</style>
</head>
<body>
	<div>
		<label>JSONP call: </label>
		<input id="jsonpCall" type="text" class="jsonpCall" />
	</div>
		<div>
		<label>Application ID: </label>
		<input id="appId" type="text" class="appId" />
		<a href="javascript:;" id="submit">submit</a>
	</div>

	<div>
		<textarea id="result" class="result"></textarea>
	</div>
</body>
</html>
