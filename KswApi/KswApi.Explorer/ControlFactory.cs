﻿using System;

namespace KswApi.Explorer
{
	public static class ControlFactory
	{
		public static IParameterControl CreateControlForType(Type type)
		{
			if (type.IsEnum)
				return new EnumParameterControl();
			if (type.IsPrimitive || type == typeof(string) || type == typeof(Guid) || type == typeof(DateTime))
				return new DefaultParameterControl();
			if (type.IsClass || (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>)))
				return new ObjectParameterControl();
			
			throw new ArgumentException(string.Format("Cannot create control for Type: {0}.", type));
		}
	}
}
