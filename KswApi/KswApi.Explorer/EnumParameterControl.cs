﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using KswApi.Reflection.Objects;

namespace KswApi.Explorer
{
	public partial class EnumParameterControl : UserControl, IParameterControl
	{
		public event Action<IParameterControl> Changed;

		private Type _parameterType;
		private OperationParameterInfo _parameter;

		public EnumParameterControl()
		{
			InitializeComponent();
		}

		public OperationParameterInfo OperationParameter
		{
			get { return _parameter; }
			set
			{
				_parameter = value;

				ClearDropDown();

				if (value == null)
					return;

				label.Text = value.CodeName;

				_parameterType = value.Type;

				PopulateDropDown();
			}
		}

		public object Value
		{
			get
			{
				return Enum.Parse(_parameterType, (string)dropDown.SelectedItem);
			}
			set
			{
				if (value == null)
				{
					dropDown.SelectedIndex = 0;
				}
				else
				{
					dropDown.SelectedItem = value.ToString();	
				}
			}
		}

		private void ClearDropDown()
		{
			dropDown.Items.Clear();
		}

		private void PopulateDropDown()
		{
			string[] values = Enum.GetNames(_parameterType);
			foreach (string value in values)
			{
				dropDown.Items.Add(value);
			}
		}

		private void dropDown_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (Changed != null)
				Changed(this);
		}
	}
}
