﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using KswApi.Reflection.Objects;

namespace KswApi.Explorer
{
	public partial class ObjectParameterControl : UserControl, IParameterControl
	{
		private readonly Dictionary<string, IParameterControl> _parameterControls = new Dictionary<string, IParameterControl>();

		public event Action<IParameterControl> Changed;

		private Type _parameterType;
		private OperationParameterInfo _parameter;
		private object _value;

		public ObjectParameterControl()
		{
			InitializeComponent();
		}

		public object Value
		{
			get { return _value; }
			set
			{
				_value = value;

				UpdateControls(value);
			}
		}

		public OperationParameterInfo OperationParameter
		{
			get { return _parameter; }
			set
			{
				_parameter = value;

				ClearControls();

				if (value != null)
				{
					_parameterType = value.Type;

					label.Text = value.CodeName + ':';
				}
				else
				{
					_parameterType = default(Type);

					label.Text = string.Empty;
				}

				nullCheckBox.Checked = _value == null;

				UpdateControls();
			}
		}

		private void UpdateControls(object value)
		{
			if (value == null)
			{
				nullCheckBox.Checked = true;
				UpdateControls();
				return;
			}

			nullCheckBox.Checked = false;
			
			UpdateControls();

			PropertyInfo[] properties = _parameterType.GetProperties(BindingFlags.Public | BindingFlags.Instance);

			foreach (PropertyInfo property in properties)
			{
				IParameterControl parameterControl;

				if (!_parameterControls.TryGetValue(property.Name, out parameterControl))
					continue;

				object propertyValue = property.GetValue(value);

				parameterControl.Value = propertyValue;
			}
		}


		private void ClearControls()
		{
			flowLayout.Controls.Clear();
		}

		private void CreateControlsForType(Type type)
		{
			_parameterControls.Clear();

			if (typeof(IEnumerable).IsAssignableFrom(type))
				return;

			if (IsNullable(type))
			{
				CreateControlsForNullableType(type);

				return;
			}

			PropertyInfo[] properties = type.GetProperties(BindingFlags.Public | BindingFlags.Instance);

			foreach (PropertyInfo property in properties)
			{
				IParameterControl parameterControl = ControlFactory.CreateControlForType(property.PropertyType);

				parameterControl.OperationParameter = new OperationParameterInfo
														  {
															  CodeName = property.Name,
															  WebName = property.Name,
															  Type = property.PropertyType
														  };

				parameterControl.Changed += ParameterControlOnChanged;

				_parameterControls[property.Name] = parameterControl;

				Control control = (Control)parameterControl;

				flowLayout.Controls.Add(control);
			}
		}

		private void CreateControlsForNullableType(Type type)
		{
			Type underlyingType = Nullable.GetUnderlyingType(type);

			IParameterControl parameterControl = ControlFactory.CreateControlForType(underlyingType);

			parameterControl.OperationParameter = new OperationParameterInfo
			                                      {
				                                      CodeName = "Value",
				                                      Type = underlyingType
			                                      };

			parameterControl.Changed += ParameterControlOnChanged;

			Control control = (Control)parameterControl;

			flowLayout.Controls.Add(control);
		}

		private void ParameterControlOnChanged(IParameterControl parameterControl)
		{
			if (_parameterType.IsGenericType && _parameterType.GetGenericTypeDefinition() == typeof(Nullable<>))
			{
				_value = parameterControl.Value;

				if (Changed != null)
					Changed(this);
				
				return;
			}

			if (_value == null)
				throw new Exception("Attempted to set a value on a null object.");

			PropertyInfo property = _parameterType.GetProperty(parameterControl.OperationParameter.CodeName);

			if (property == null)
				throw new Exception(string.Format("Could not find property: {0}.", parameterControl.OperationParameter.CodeName));

			property.SetValue(_value, parameterControl.Value);

			if (Changed != null)
				Changed(this);
		}

		private void UpdateHeight()
		{
			int labelHeight = label.Height;
			int layoutHeight = flowLayout.Height;

			Height = Math.Max(labelHeight, layoutHeight);
		}

		private void UpdateControls()
		{
			ClearControls();

			if (!nullCheckBox.Checked)
				CreateControlsForType(_parameterType);

			UpdateHeight();
		}

		private void nullCheckBox_CheckedChanged(object sender, EventArgs e)
		{
			if (nullCheckBox.Checked)
			{
				_value = null;
			}
			else
			{
				if (_parameterType.IsClass)
				{
					ConstructorInfo constructor = _parameterType.GetConstructor(Type.EmptyTypes);

					if (constructor == null)
						throw new Exception(string.Format("Type does not have a parameterless constructor: {0}.", _parameterType.Name));

					_value = constructor.Invoke(null);
				}
				else
				{
					if (IsNullable(_parameterType))
						_value = DefaultValue.Get(Nullable.GetUnderlyingType(_parameterType));
				}
			}

			UpdateControls();
		}

		private bool IsNullable(Type type)
		{
			return type.IsGenericType && type.GetGenericTypeDefinition() == typeof (Nullable<>);
		}
	}
}
