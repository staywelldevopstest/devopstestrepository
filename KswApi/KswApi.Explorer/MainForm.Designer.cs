﻿namespace KswApi.Explorer
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.treeView = new System.Windows.Forms.TreeView();
			this.panel1 = new System.Windows.Forms.Panel();
			this.uriViewRadioButton = new System.Windows.Forms.RadioButton();
			this.methodSignatureViewRadioButton = new System.Windows.Forms.RadioButton();
			this.splitContainer2 = new System.Windows.Forms.SplitContainer();
			this.refererTextBox = new System.Windows.Forms.TextBox();
			this.label10 = new System.Windows.Forms.Label();
			this.callbackFunctionTextBox = new System.Windows.Forms.TextBox();
			this.label9 = new System.Windows.Forms.Label();
			this.formatSelector = new System.Windows.Forms.ComboBox();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.fullUriTextBox = new System.Windows.Forms.TextBox();
			this.httpVerbTextBox = new System.Windows.Forms.TextBox();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.label3 = new System.Windows.Forms.Label();
			this.serviceUriTextBox = new System.Windows.Forms.TextBox();
			this.clearButton = new System.Windows.Forms.Button();
			this.sendButton = new System.Windows.Forms.Button();
			this.parameterGroupBox = new System.Windows.Forms.GroupBox();
			this.operationLayoutPanel = new System.Windows.Forms.FlowLayoutPanel();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.uriSignatureTextBox = new System.Windows.Forms.TextBox();
			this.methodSignatureTextBox = new System.Windows.Forms.TextBox();
			this.tabControl = new System.Windows.Forms.TabControl();
			this.tabPage1 = new System.Windows.Forms.TabPage();
			this.requestRawTextBox = new System.Windows.Forms.TextBox();
			this.tabPage2 = new System.Windows.Forms.TabPage();
			this.responseRawTextBox = new System.Windows.Forms.TextBox();
			this.menuStrip1 = new System.Windows.Forms.MenuStrip();
			this.authorizationToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
			this.splitContainer3 = new System.Windows.Forms.SplitContainer();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.label8 = new System.Windows.Forms.Label();
			this.refreshTokenTextBox = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.expiresTextBox = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.tokenTextBox = new System.Windows.Forms.TextBox();
			this.tokenListView = new System.Windows.Forms.ListView();
			this.panel2 = new System.Windows.Forms.Panel();
			this.deleteButton = new System.Windows.Forms.Button();
			this.userTokenButton = new System.Windows.Forms.Button();
			this.portalTokenButton = new System.Windows.Forms.Button();
			this.refreshTokenButton = new System.Windows.Forms.Button();
			this.applicationTokenButton = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
			this.splitContainer2.Panel1.SuspendLayout();
			this.splitContainer2.Panel2.SuspendLayout();
			this.splitContainer2.SuspendLayout();
			this.parameterGroupBox.SuspendLayout();
			this.tabControl.SuspendLayout();
			this.tabPage1.SuspendLayout();
			this.tabPage2.SuspendLayout();
			this.menuStrip1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).BeginInit();
			this.splitContainer3.Panel1.SuspendLayout();
			this.splitContainer3.Panel2.SuspendLayout();
			this.splitContainer3.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.panel2.SuspendLayout();
			this.SuspendLayout();
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.groupBox2);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
			this.splitContainer1.Size = new System.Drawing.Size(680, 625);
			this.splitContainer1.SplitterDistance = 229;
			this.splitContainer1.TabIndex = 0;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.treeView);
			this.groupBox2.Controls.Add(this.panel1);
			this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox2.Location = new System.Drawing.Point(0, 0);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(229, 625);
			this.groupBox2.TabIndex = 2;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Operations";
			// 
			// treeView
			// 
			this.treeView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.treeView.FullRowSelect = true;
			this.treeView.HideSelection = false;
			this.treeView.Location = new System.Drawing.Point(6, 15);
			this.treeView.Name = "treeView";
			this.treeView.ShowLines = false;
			this.treeView.Size = new System.Drawing.Size(217, 583);
			this.treeView.TabIndex = 0;
			// 
			// panel1
			// 
			this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.panel1.Controls.Add(this.uriViewRadioButton);
			this.panel1.Controls.Add(this.methodSignatureViewRadioButton);
			this.panel1.Location = new System.Drawing.Point(6, 602);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(268, 23);
			this.panel1.TabIndex = 1;
			// 
			// uriViewRadioButton
			// 
			this.uriViewRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.uriViewRadioButton.AutoSize = true;
			this.uriViewRadioButton.Enabled = false;
			this.uriViewRadioButton.Location = new System.Drawing.Point(105, 2);
			this.uriViewRadioButton.Name = "uriViewRadioButton";
			this.uriViewRadioButton.Size = new System.Drawing.Size(70, 17);
			this.uriViewRadioButton.TabIndex = 1;
			this.uriViewRadioButton.Text = "URI View";
			this.uriViewRadioButton.UseVisualStyleBackColor = true;
			// 
			// methodSignatureViewRadioButton
			// 
			this.methodSignatureViewRadioButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.methodSignatureViewRadioButton.AutoSize = true;
			this.methodSignatureViewRadioButton.Checked = true;
			this.methodSignatureViewRadioButton.Location = new System.Drawing.Point(12, 2);
			this.methodSignatureViewRadioButton.Name = "methodSignatureViewRadioButton";
			this.methodSignatureViewRadioButton.Size = new System.Drawing.Size(87, 17);
			this.methodSignatureViewRadioButton.TabIndex = 0;
			this.methodSignatureViewRadioButton.TabStop = true;
			this.methodSignatureViewRadioButton.Text = "Method View";
			this.methodSignatureViewRadioButton.UseVisualStyleBackColor = true;
			// 
			// splitContainer2
			// 
			this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer2.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer2.Location = new System.Drawing.Point(0, 0);
			this.splitContainer2.Name = "splitContainer2";
			this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
			// 
			// splitContainer2.Panel1
			// 
			this.splitContainer2.Panel1.Controls.Add(this.refererTextBox);
			this.splitContainer2.Panel1.Controls.Add(this.label10);
			this.splitContainer2.Panel1.Controls.Add(this.callbackFunctionTextBox);
			this.splitContainer2.Panel1.Controls.Add(this.label9);
			this.splitContainer2.Panel1.Controls.Add(this.formatSelector);
			this.splitContainer2.Panel1.Controls.Add(this.label5);
			this.splitContainer2.Panel1.Controls.Add(this.label4);
			this.splitContainer2.Panel1.Controls.Add(this.fullUriTextBox);
			this.splitContainer2.Panel1.Controls.Add(this.httpVerbTextBox);
			this.splitContainer2.Panel1.Controls.Add(this.progressBar);
			this.splitContainer2.Panel1.Controls.Add(this.label3);
			this.splitContainer2.Panel1.Controls.Add(this.serviceUriTextBox);
			this.splitContainer2.Panel1.Controls.Add(this.clearButton);
			this.splitContainer2.Panel1.Controls.Add(this.sendButton);
			this.splitContainer2.Panel1.Controls.Add(this.parameterGroupBox);
			this.splitContainer2.Panel1.Controls.Add(this.label2);
			this.splitContainer2.Panel1.Controls.Add(this.label1);
			this.splitContainer2.Panel1.Controls.Add(this.uriSignatureTextBox);
			this.splitContainer2.Panel1.Controls.Add(this.methodSignatureTextBox);
			// 
			// splitContainer2.Panel2
			// 
			this.splitContainer2.Panel2.Controls.Add(this.tabControl);
			this.splitContainer2.Size = new System.Drawing.Size(447, 625);
			this.splitContainer2.SplitterDistance = 366;
			this.splitContainer2.TabIndex = 0;
			// 
			// refererTextBox
			// 
			this.refererTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.refererTextBox.Location = new System.Drawing.Point(289, 339);
			this.refererTextBox.Name = "refererTextBox";
			this.refererTextBox.Size = new System.Drawing.Size(143, 20);
			this.refererTextBox.TabIndex = 18;
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(241, 342);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(42, 13);
			this.label10.TabIndex = 17;
			this.label10.Text = "Referer";
			// 
			// callbackFunctionTextBox
			// 
			this.callbackFunctionTextBox.Location = new System.Drawing.Point(107, 339);
			this.callbackFunctionTextBox.Name = "callbackFunctionTextBox";
			this.callbackFunctionTextBox.Size = new System.Drawing.Size(95, 20);
			this.callbackFunctionTextBox.TabIndex = 16;
			this.callbackFunctionTextBox.TextChanged += new System.EventHandler(this.callbackFunctionTextBox_TextChanged);
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(6, 342);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(95, 13);
			this.label9.TabIndex = 15;
			this.label9.Text = "Callback Function:";
			// 
			// formatSelector
			// 
			this.formatSelector.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.formatSelector.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.formatSelector.FormattingEnabled = true;
			this.formatSelector.Items.AddRange(new object[] {
            "Default",
            "JSON",
            "XML",
            "JSONP",
            "Form"});
			this.formatSelector.Location = new System.Drawing.Point(54, 311);
			this.formatSelector.Name = "formatSelector";
			this.formatSelector.Size = new System.Drawing.Size(67, 21);
			this.formatSelector.TabIndex = 14;
			this.formatSelector.SelectedIndexChanged += new System.EventHandler(this.formatSelector_SelectedIndexChanged);
			// 
			// label5
			// 
			this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 314);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(42, 13);
			this.label5.TabIndex = 13;
			this.label5.Text = "Format:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(6, 67);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(48, 13);
			this.label4.TabIndex = 12;
			this.label4.Text = "Full URI:";
			// 
			// fullUriTextBox
			// 
			this.fullUriTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.fullUriTextBox.Location = new System.Drawing.Point(57, 64);
			this.fullUriTextBox.Name = "fullUriTextBox";
			this.fullUriTextBox.ReadOnly = true;
			this.fullUriTextBox.Size = new System.Drawing.Size(378, 20);
			this.fullUriTextBox.TabIndex = 11;
			// 
			// httpVerbTextBox
			// 
			this.httpVerbTextBox.Location = new System.Drawing.Point(57, 38);
			this.httpVerbTextBox.Name = "httpVerbTextBox";
			this.httpVerbTextBox.ReadOnly = true;
			this.httpVerbTextBox.Size = new System.Drawing.Size(54, 20);
			this.httpVerbTextBox.TabIndex = 10;
			// 
			// progressBar
			// 
			this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.progressBar.Location = new System.Drawing.Point(289, 309);
			this.progressBar.MarqueeAnimationSpeed = 10;
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(143, 23);
			this.progressBar.Style = System.Windows.Forms.ProgressBarStyle.Marquee;
			this.progressBar.TabIndex = 9;
			this.progressBar.Visible = false;
			// 
			// label3
			// 
			this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(6, 286);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(68, 13);
			this.label3.TabIndex = 8;
			this.label3.Text = "Service URI:";
			// 
			// serviceUriTextBox
			// 
			this.serviceUriTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.serviceUriTextBox.Location = new System.Drawing.Point(80, 283);
			this.serviceUriTextBox.Name = "serviceUriTextBox";
			this.serviceUriTextBox.Size = new System.Drawing.Size(352, 20);
			this.serviceUriTextBox.TabIndex = 7;
			this.serviceUriTextBox.Text = "http://10.10.20.158/kswapi";
			this.serviceUriTextBox.TextChanged += new System.EventHandler(this.serviceUriTextBox_TextChanged);
			// 
			// clearButton
			// 
			this.clearButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.clearButton.Location = new System.Drawing.Point(208, 309);
			this.clearButton.Name = "clearButton";
			this.clearButton.Size = new System.Drawing.Size(75, 23);
			this.clearButton.TabIndex = 6;
			this.clearButton.Text = "Clear";
			this.clearButton.UseVisualStyleBackColor = true;
			// 
			// sendButton
			// 
			this.sendButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.sendButton.Location = new System.Drawing.Point(127, 309);
			this.sendButton.Name = "sendButton";
			this.sendButton.Size = new System.Drawing.Size(75, 23);
			this.sendButton.TabIndex = 5;
			this.sendButton.Text = "Send";
			this.sendButton.UseVisualStyleBackColor = true;
			this.sendButton.Click += new System.EventHandler(this.sendButton_Click);
			// 
			// parameterGroupBox
			// 
			this.parameterGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.parameterGroupBox.Controls.Add(this.operationLayoutPanel);
			this.parameterGroupBox.Location = new System.Drawing.Point(6, 90);
			this.parameterGroupBox.Name = "parameterGroupBox";
			this.parameterGroupBox.Size = new System.Drawing.Size(429, 187);
			this.parameterGroupBox.TabIndex = 4;
			this.parameterGroupBox.TabStop = false;
			this.parameterGroupBox.Text = "Parameters";
			// 
			// operationLayoutPanel
			// 
			this.operationLayoutPanel.AutoScroll = true;
			this.operationLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.operationLayoutPanel.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.operationLayoutPanel.Location = new System.Drawing.Point(3, 16);
			this.operationLayoutPanel.Name = "operationLayoutPanel";
			this.operationLayoutPanel.Size = new System.Drawing.Size(423, 168);
			this.operationLayoutPanel.TabIndex = 0;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(25, 41);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(29, 13);
			this.label2.TabIndex = 3;
			this.label2.Text = "URI:";
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(8, 15);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(46, 13);
			this.label1.TabIndex = 2;
			this.label1.Text = "Method:";
			// 
			// uriSignatureTextBox
			// 
			this.uriSignatureTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.uriSignatureTextBox.Location = new System.Drawing.Point(117, 38);
			this.uriSignatureTextBox.Name = "uriSignatureTextBox";
			this.uriSignatureTextBox.ReadOnly = true;
			this.uriSignatureTextBox.Size = new System.Drawing.Size(318, 20);
			this.uriSignatureTextBox.TabIndex = 1;
			// 
			// methodSignatureTextBox
			// 
			this.methodSignatureTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.methodSignatureTextBox.Location = new System.Drawing.Point(57, 12);
			this.methodSignatureTextBox.Name = "methodSignatureTextBox";
			this.methodSignatureTextBox.ReadOnly = true;
			this.methodSignatureTextBox.Size = new System.Drawing.Size(378, 20);
			this.methodSignatureTextBox.TabIndex = 0;
			// 
			// tabControl
			// 
			this.tabControl.Controls.Add(this.tabPage1);
			this.tabControl.Controls.Add(this.tabPage2);
			this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl.Location = new System.Drawing.Point(0, 0);
			this.tabControl.Name = "tabControl";
			this.tabControl.SelectedIndex = 0;
			this.tabControl.Size = new System.Drawing.Size(447, 255);
			this.tabControl.TabIndex = 0;
			// 
			// tabPage1
			// 
			this.tabPage1.Controls.Add(this.requestRawTextBox);
			this.tabPage1.Location = new System.Drawing.Point(4, 22);
			this.tabPage1.Name = "tabPage1";
			this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage1.Size = new System.Drawing.Size(439, 229);
			this.tabPage1.TabIndex = 0;
			this.tabPage1.Text = "Request";
			this.tabPage1.UseVisualStyleBackColor = true;
			// 
			// requestRawTextBox
			// 
			this.requestRawTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.requestRawTextBox.Location = new System.Drawing.Point(3, 3);
			this.requestRawTextBox.Multiline = true;
			this.requestRawTextBox.Name = "requestRawTextBox";
			this.requestRawTextBox.ReadOnly = true;
			this.requestRawTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.requestRawTextBox.Size = new System.Drawing.Size(433, 223);
			this.requestRawTextBox.TabIndex = 0;
			this.requestRawTextBox.WordWrap = false;
			// 
			// tabPage2
			// 
			this.tabPage2.Controls.Add(this.responseRawTextBox);
			this.tabPage2.Location = new System.Drawing.Point(4, 22);
			this.tabPage2.Name = "tabPage2";
			this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
			this.tabPage2.Size = new System.Drawing.Size(401, 229);
			this.tabPage2.TabIndex = 1;
			this.tabPage2.Text = "Response";
			this.tabPage2.UseVisualStyleBackColor = true;
			// 
			// responseRawTextBox
			// 
			this.responseRawTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.responseRawTextBox.Location = new System.Drawing.Point(3, 3);
			this.responseRawTextBox.Multiline = true;
			this.responseRawTextBox.Name = "responseRawTextBox";
			this.responseRawTextBox.ReadOnly = true;
			this.responseRawTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.responseRawTextBox.Size = new System.Drawing.Size(395, 223);
			this.responseRawTextBox.TabIndex = 1;
			this.responseRawTextBox.WordWrap = false;
			// 
			// menuStrip1
			// 
			this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.authorizationToolStripMenuItem});
			this.menuStrip1.Location = new System.Drawing.Point(0, 0);
			this.menuStrip1.Name = "menuStrip1";
			this.menuStrip1.Size = new System.Drawing.Size(880, 24);
			this.menuStrip1.TabIndex = 1;
			this.menuStrip1.Text = "menuStrip1";
			// 
			// authorizationToolStripMenuItem
			// 
			this.authorizationToolStripMenuItem.Name = "authorizationToolStripMenuItem";
			this.authorizationToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
			this.authorizationToolStripMenuItem.Text = "&Options";
			// 
			// splitContainer3
			// 
			this.splitContainer3.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer3.FixedPanel = System.Windows.Forms.FixedPanel.Panel1;
			this.splitContainer3.Location = new System.Drawing.Point(0, 24);
			this.splitContainer3.Name = "splitContainer3";
			// 
			// splitContainer3.Panel1
			// 
			this.splitContainer3.Panel1.Controls.Add(this.groupBox1);
			// 
			// splitContainer3.Panel2
			// 
			this.splitContainer3.Panel2.Controls.Add(this.splitContainer1);
			this.splitContainer3.Size = new System.Drawing.Size(880, 625);
			this.splitContainer3.SplitterDistance = 196;
			this.splitContainer3.TabIndex = 2;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.label8);
			this.groupBox1.Controls.Add(this.refreshTokenTextBox);
			this.groupBox1.Controls.Add(this.label7);
			this.groupBox1.Controls.Add(this.expiresTextBox);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.tokenTextBox);
			this.groupBox1.Controls.Add(this.tokenListView);
			this.groupBox1.Controls.Add(this.panel2);
			this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.groupBox1.Location = new System.Drawing.Point(0, 0);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(196, 625);
			this.groupBox1.TabIndex = 1;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Access Tokens";
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(6, 67);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(47, 13);
			this.label8.TabIndex = 8;
			this.label8.Text = "Refresh:";
			// 
			// refreshTokenTextBox
			// 
			this.refreshTokenTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.refreshTokenTextBox.Location = new System.Drawing.Point(59, 67);
			this.refreshTokenTextBox.Name = "refreshTokenTextBox";
			this.refreshTokenTextBox.ReadOnly = true;
			this.refreshTokenTextBox.Size = new System.Drawing.Size(131, 20);
			this.refreshTokenTextBox.TabIndex = 7;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(6, 41);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(44, 13);
			this.label7.TabIndex = 6;
			this.label7.Text = "Expires:";
			// 
			// expiresTextBox
			// 
			this.expiresTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.expiresTextBox.Location = new System.Drawing.Point(59, 41);
			this.expiresTextBox.Name = "expiresTextBox";
			this.expiresTextBox.ReadOnly = true;
			this.expiresTextBox.Size = new System.Drawing.Size(131, 20);
			this.expiresTextBox.TabIndex = 5;
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(6, 15);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(41, 13);
			this.label6.TabIndex = 4;
			this.label6.Text = "Token:";
			// 
			// tokenTextBox
			// 
			this.tokenTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tokenTextBox.Location = new System.Drawing.Point(59, 15);
			this.tokenTextBox.Name = "tokenTextBox";
			this.tokenTextBox.ReadOnly = true;
			this.tokenTextBox.Size = new System.Drawing.Size(131, 20);
			this.tokenTextBox.TabIndex = 3;
			// 
			// tokenListView
			// 
			this.tokenListView.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tokenListView.HideSelection = false;
			this.tokenListView.Location = new System.Drawing.Point(6, 90);
			this.tokenListView.Name = "tokenListView";
			this.tokenListView.Size = new System.Drawing.Size(184, 380);
			this.tokenListView.TabIndex = 1;
			this.tokenListView.UseCompatibleStateImageBehavior = false;
			this.tokenListView.View = System.Windows.Forms.View.List;
			this.tokenListView.SelectedIndexChanged += new System.EventHandler(this.tokenListView_SelectedIndexChanged);
			// 
			// panel2
			// 
			this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.panel2.Controls.Add(this.deleteButton);
			this.panel2.Controls.Add(this.userTokenButton);
			this.panel2.Controls.Add(this.portalTokenButton);
			this.panel2.Controls.Add(this.refreshTokenButton);
			this.panel2.Controls.Add(this.applicationTokenButton);
			this.panel2.Location = new System.Drawing.Point(6, 476);
			this.panel2.Name = "panel2";
			this.panel2.Size = new System.Drawing.Size(184, 149);
			this.panel2.TabIndex = 0;
			// 
			// deleteButton
			// 
			this.deleteButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.deleteButton.Enabled = false;
			this.deleteButton.Location = new System.Drawing.Point(3, 122);
			this.deleteButton.Name = "deleteButton";
			this.deleteButton.Size = new System.Drawing.Size(178, 23);
			this.deleteButton.TabIndex = 4;
			this.deleteButton.Text = "Delete";
			this.deleteButton.UseVisualStyleBackColor = true;
			this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
			// 
			// userTokenButton
			// 
			this.userTokenButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.userTokenButton.Enabled = false;
			this.userTokenButton.Location = new System.Drawing.Point(3, 64);
			this.userTokenButton.Name = "userTokenButton";
			this.userTokenButton.Size = new System.Drawing.Size(178, 23);
			this.userTokenButton.TabIndex = 3;
			this.userTokenButton.Text = "User Token";
			this.userTokenButton.UseVisualStyleBackColor = true;
			this.userTokenButton.Click += new System.EventHandler(this.userTokenButton_Click);
			// 
			// portalTokenButton
			// 
			this.portalTokenButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.portalTokenButton.Location = new System.Drawing.Point(3, 6);
			this.portalTokenButton.Name = "portalTokenButton";
			this.portalTokenButton.Size = new System.Drawing.Size(178, 23);
			this.portalTokenButton.TabIndex = 2;
			this.portalTokenButton.Text = "Portal Token";
			this.portalTokenButton.UseVisualStyleBackColor = true;
			this.portalTokenButton.Click += new System.EventHandler(this.portalTokenButton_Click);
			// 
			// refreshTokenButton
			// 
			this.refreshTokenButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.refreshTokenButton.Enabled = false;
			this.refreshTokenButton.Location = new System.Drawing.Point(3, 93);
			this.refreshTokenButton.Name = "refreshTokenButton";
			this.refreshTokenButton.Size = new System.Drawing.Size(178, 23);
			this.refreshTokenButton.TabIndex = 1;
			this.refreshTokenButton.Text = "Refresh Token";
			this.refreshTokenButton.UseVisualStyleBackColor = true;
			this.refreshTokenButton.Click += new System.EventHandler(this.refreshTokenButton_Click);
			// 
			// applicationTokenButton
			// 
			this.applicationTokenButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.applicationTokenButton.Location = new System.Drawing.Point(3, 35);
			this.applicationTokenButton.Name = "applicationTokenButton";
			this.applicationTokenButton.Size = new System.Drawing.Size(178, 23);
			this.applicationTokenButton.TabIndex = 0;
			this.applicationTokenButton.Text = "Application Token";
			this.applicationTokenButton.UseVisualStyleBackColor = true;
			this.applicationTokenButton.Click += new System.EventHandler(this.applicationTokenButton_Click);
			// 
			// MainForm
			// 
			this.AcceptButton = this.sendButton;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(880, 649);
			this.Controls.Add(this.splitContainer3);
			this.Controls.Add(this.menuStrip1);
			this.MainMenuStrip = this.menuStrip1;
			this.Name = "MainForm";
			this.Text = "KSW API Explorer";
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.splitContainer2.Panel1.ResumeLayout(false);
			this.splitContainer2.Panel1.PerformLayout();
			this.splitContainer2.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
			this.splitContainer2.ResumeLayout(false);
			this.parameterGroupBox.ResumeLayout(false);
			this.tabControl.ResumeLayout(false);
			this.tabPage1.ResumeLayout(false);
			this.tabPage1.PerformLayout();
			this.tabPage2.ResumeLayout(false);
			this.tabPage2.PerformLayout();
			this.menuStrip1.ResumeLayout(false);
			this.menuStrip1.PerformLayout();
			this.splitContainer3.Panel1.ResumeLayout(false);
			this.splitContainer3.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer3)).EndInit();
			this.splitContainer3.ResumeLayout(false);
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.panel2.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.TreeView treeView;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.RadioButton uriViewRadioButton;
		private System.Windows.Forms.RadioButton methodSignatureViewRadioButton;
		private System.Windows.Forms.SplitContainer splitContainer2;
		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.TabPage tabPage1;
		private System.Windows.Forms.TabPage tabPage2;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox uriSignatureTextBox;
		private System.Windows.Forms.TextBox methodSignatureTextBox;
		private System.Windows.Forms.GroupBox parameterGroupBox;
		private System.Windows.Forms.Button sendButton;
		private System.Windows.Forms.Button clearButton;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.TextBox serviceUriTextBox;
		private System.Windows.Forms.TextBox requestRawTextBox;
		private System.Windows.Forms.TextBox responseRawTextBox;
		private System.Windows.Forms.ProgressBar progressBar;
		private System.Windows.Forms.TextBox httpVerbTextBox;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.TextBox fullUriTextBox;
		private System.Windows.Forms.ComboBox formatSelector;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.FlowLayoutPanel operationLayoutPanel;
		private System.Windows.Forms.MenuStrip menuStrip1;
		private System.Windows.Forms.ToolStripMenuItem authorizationToolStripMenuItem;
		private System.Windows.Forms.SplitContainer splitContainer3;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Panel panel2;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Button refreshTokenButton;
		private System.Windows.Forms.Button applicationTokenButton;
		private System.Windows.Forms.Button portalTokenButton;
		private System.Windows.Forms.Button deleteButton;
		private System.Windows.Forms.Button userTokenButton;
		private System.Windows.Forms.ListView tokenListView;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.TextBox tokenTextBox;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox expiresTextBox;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.TextBox refreshTokenTextBox;
		private System.Windows.Forms.TextBox callbackFunctionTextBox;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.TextBox refererTextBox;
		private System.Windows.Forms.Label label10;
	}
}

