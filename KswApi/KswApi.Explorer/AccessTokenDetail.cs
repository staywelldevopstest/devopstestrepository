﻿using System;

namespace KswApi.Explorer
{
	class AccessTokenDetail
	{
		public string Token { get; set; }
		public string RefreshToken { get; set; }
		public string AuthorizationServer { get; set; }
		public int ExpiresIn { get; set; }
		public DateTime CreationTime { get; set; }
		public DateTime LastAccessedTime { get; set; }
		public string ApplicationId { get; set; }
		public string ApplicationSecret { get; set; }
	}
}
