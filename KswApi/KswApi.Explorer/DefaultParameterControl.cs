﻿using System;
using System.Windows.Forms;
using KswApi.Reflection.Objects;

namespace KswApi.Explorer
{
	public partial class DefaultParameterControl : UserControl, IParameterControl
	{
		public event Action<IParameterControl> Changed;

		private Type _parameterType;
		private OperationParameterInfo _parameter;

		public DefaultParameterControl()
		{
			InitializeComponent();
		}

		public OperationParameterInfo OperationParameter
		{
			get { return _parameter; }
			set
			{
				_parameter = value;

				if (value != null)
				{
					_parameterType = value.Type;

					label.Text = value.CodeName + ':';

					// temporarily disable updates so we can set the default value

					Value = DefaultValue.Get(_parameterType);
				}
				else
				{
					_parameterType = default(Type);

					label.Text = string.Empty;
				}
			}
		}

		public object Value
		{
			get
			{
				try
				{
					if (_parameterType.IsPrimitive && string.IsNullOrEmpty(valueTextBox.Text))
						return null;

					if (_parameterType == typeof(Guid))
					{
						Guid guid;
						if (Guid.TryParse(valueTextBox.Text, out guid))
							return guid;
						return null;
					}

					return Convert.ChangeType(valueTextBox.Text, _parameterType);
				}
				catch (InvalidCastException)
				{
					return null;
				}
				catch (FormatException)
				{
					return null;
				}
			}
			set
			{
				valueTextBox.Text = value == null ? string.Empty : value.ToString();
			}
		}

		private void valueTextBox_TextChanged(object sender, EventArgs e)
		{
			if (Changed != null)
				Changed(this);
		}
	}
}
