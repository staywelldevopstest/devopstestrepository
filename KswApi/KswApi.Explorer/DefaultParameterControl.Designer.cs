﻿namespace KswApi.Explorer
{
	partial class DefaultParameterControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label = new System.Windows.Forms.Label();
			this.valueTextBox = new System.Windows.Forms.TextBox();
			this.SuspendLayout();
			// 
			// label
			// 
			this.label.AutoSize = true;
			this.label.Dock = System.Windows.Forms.DockStyle.Left;
			this.label.Location = new System.Drawing.Point(0, 0);
			this.label.Name = "label";
			this.label.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
			this.label.Size = new System.Drawing.Size(35, 16);
			this.label.TabIndex = 0;
			this.label.Text = "label1";
			// 
			// valueTextBox
			// 
			this.valueTextBox.Dock = System.Windows.Forms.DockStyle.Fill;
			this.valueTextBox.Location = new System.Drawing.Point(35, 0);
			this.valueTextBox.Name = "valueTextBox";
			this.valueTextBox.Size = new System.Drawing.Size(316, 20);
			this.valueTextBox.TabIndex = 1;
			this.valueTextBox.TextChanged += new System.EventHandler(this.valueTextBox_TextChanged);
			// 
			// DefaultParameterControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.valueTextBox);
			this.Controls.Add(this.label);
			this.Name = "DefaultParameterControl";
			this.Size = new System.Drawing.Size(351, 21);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label;
		private System.Windows.Forms.TextBox valueTextBox;
	}
}
