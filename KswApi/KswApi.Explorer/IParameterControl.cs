﻿using System;
using KswApi.Reflection.Objects;

namespace KswApi.Explorer
{
	public interface IParameterControl
	{
		object Value { get; set; }
		OperationParameterInfo OperationParameter { get; set; }
		event Action<IParameterControl> Changed;
	}
}
