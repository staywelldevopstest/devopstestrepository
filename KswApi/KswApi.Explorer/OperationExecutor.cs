﻿using System.Collections;
using System.Reflection;
using System.Runtime.Serialization;
using System.Text.RegularExpressions;
using KswApi.Net;
using KswApi.Xml;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using KswApi.Reflection.Objects;
using Formatting = Newtonsoft.Json.Formatting;

namespace KswApi.Explorer
{
    class OperationExecutor
    {
        public event Action<OperationRequest> Request;
        public event Action<OperationResponse> Response;
        public event Action<string> Error;
        public event Action Finished;

        public void Execute(string serviceLocation, OperationInfo operation, MessageFormat receiveFormat, NameValueCollection headers, string callbackFunction = null)
        {
            Task.Factory.StartNew(() => ExecuteTask(serviceLocation, operation, receiveFormat, headers, callbackFunction));
        }

        public string GetCompletUri(string serviceLocation, OperationInfo operation, MessageFormat receiveFormat, string callbackFunction = null)
        {
            if (serviceLocation == null)
                serviceLocation = string.Empty;

            serviceLocation = serviceLocation.TrimEnd('/');

            if (operation == null)
                return serviceLocation;

            string operationPath = operation.UriSignature.TrimStart('/');

            NameValueCollection query = null;

            if (!string.IsNullOrEmpty(callbackFunction) && receiveFormat == MessageFormat.Jsonp)
            {
                query = new NameValueCollection { { "callback", callbackFunction } };
            }

            foreach (OperationParameterInfo parameter in operation.Parameters)
            {
                object value;

                operation.Values.TryGetValue(parameter.CodeName, out value);

                if (Regex.IsMatch(operationPath, @"{\**" + parameter.CodeName + "}"))
                {
                    operationPath = Regex.Replace(operationPath, @"{\**" + parameter.CodeName + "}", value == null ? string.Empty : value.ToString());
                    continue;
                }

                if (value == null || operation.Verb != "GET")
                    continue;

                Type parameterType = parameter.Type;

                if (parameterType.IsGenericType && parameterType.GetGenericTypeDefinition() == typeof(Nullable<>))
                    parameterType = parameterType.GetGenericArguments()[0];

                if (parameterType.IsPrimitive || parameterType == typeof(string) || parameterType == typeof(Guid) || parameterType.IsEnum || parameterType.IsArray || parameterType == typeof(DateTime))
                {
                    string stringValue = value.ToString();

                    if (string.IsNullOrEmpty(stringValue))
                        continue;

                    if (query == null)
                        query = new NameValueCollection();

                    query.Add(parameter.WebName, stringValue);
                }
                else if (parameterType.IsClass)
                {
                    foreach (PropertyInfo propertyInfo in parameter.Type.GetProperties(BindingFlags.Public | BindingFlags.Instance))
                    {
                        if (!propertyInfo.CanWrite)
                            continue;

                        Type propertyType = propertyInfo.PropertyType;

                        object objectValue = propertyInfo.GetValue(value);

                        if (objectValue == null)
                            continue;

                        if (propertyType.IsPrimitive || propertyType == typeof(string) || propertyType == typeof(Guid) || propertyType.IsEnum || propertyType.IsArray)
                        {
                            DataMemberAttribute attribute = propertyInfo.GetCustomAttribute<DataMemberAttribute>();
                            string name = attribute != null && !string.IsNullOrEmpty(attribute.Name)
                                              ? attribute.Name
                                              : propertyInfo.Name;

                            string stringValue = !propertyType.IsArray
                                               ? objectValue.ToString()
                                               : string.Join(",", ((IEnumerable)objectValue).Cast<object>().ToArray());

                            if (query == null)
                                query = new NameValueCollection();

                            query.Add(name, stringValue);
                        }
                    }

                }

            }

            StringBuilder builder = new StringBuilder(serviceLocation);

            builder.Append('/');

            builder.Append(operationPath);

            switch (receiveFormat)
            {
                case MessageFormat.Json:
                    builder.Append(".json");
                    break;
                case MessageFormat.Xml:
                    builder.Append(".xml");
                    break;
                case MessageFormat.Jsonp:
                    builder.Append(".jsonp");
                    break;
            }

            if (query == null)
                return builder.ToString();

            if (!operationPath.Contains("?"))
                builder.Append('?');
            else
                builder.Append('&');

            bool first = true;

            foreach (string key in query)
            {
                if (first)
                    first = false;
                else
                    builder.Append('&');

                builder.Append(HttpUtility.UrlEncode(key));
                builder.Append('=');
                builder.Append(HttpUtility.UrlEncode(query[key]));
            }

            return builder.ToString();
        }

        private void ExecuteTask(string serviceLocation, OperationInfo operation, MessageFormat receiveFormat, NameValueCollection headers, string callbackFunction)
        {
            try
            {
				Send(serviceLocation, operation, receiveFormat, headers, callbackFunction);
            }
            catch (Exception exception)
            {
                // get the inner-most exception
                while (exception.InnerException != null)
                    exception = exception.InnerException;

                Action<string> error = Error;
                if (error != null)
                    error(exception.Message);
            }
            finally
            {
                Action finished = Finished;

                if (finished != null)
                    finished();
            }
        }

        private void Send(string serviceLocation, OperationInfo operation, MessageFormat format, NameValueCollection headers, string callbackFunction)
        {
            // get the real path
            Uri tempuri;

            if (!Uri.TryCreate(serviceLocation, UriKind.Absolute, out tempuri))
                throw new Exception("Invalid service location, must be an absolute URI.");

			string uri = GetCompletUri(serviceLocation, operation, format, callbackFunction);

            if (headers == null)
                headers = new NameValueCollection();

            if (format == MessageFormat.Form)
                headers.Add("Content-Type", "application/x-www-form-urlencoded");

            RestClient client = new RestClient
                                    {
                                        ReceiveTimeout = TimeSpan.FromSeconds(240),
                                        SendTimeout = TimeSpan.FromSeconds(240)
                                    };

            RestResponse response = client.Send(new RestRequest
                            {
                                Uri = uri,
                                Method = operation.Verb,
                                Headers = headers,
                                Body = GetBody(operation, format),
                                Observe = true
                            });

            OperationRequest operationRequest = new OperationRequest
                {
                    Raw = response.GetRawRequest()
                };


            Action<OperationRequest> onRequest = Request;

            if (onRequest != null)
                onRequest(operationRequest);

            OperationResponse operationResponse = new OperationResponse
                {
                    Raw = response.GetRawResponse(),
                    Body = response.Body,
                    Headers = response.Headers,
                    ReasonPhrase = response.ReasonPhrase,
                    StatusCode = response.StatusCode,
                    Operation = operation
                };

            Action<OperationResponse> onResponse = Response;

            if (onResponse != null)
                onResponse(operationResponse);

        }

        private bool IsMethodWithBody(string method)
        {
            return method == HttpMethod.Post.Method || method == HttpMethod.Put.Method;
        }

        private string GetBody(OperationInfo operation, MessageFormat format)
        {
            if (!IsMethodWithBody(operation.Verb))
                return null;

            switch (format)
            {
                case MessageFormat.Default:
                case MessageFormat.Json:
                    return GetJsonBody(operation);
                case MessageFormat.Xml:
                    return GetXmlBody(operation);
                case MessageFormat.Form:
                    return GetFormBody(operation);
                default:
                    throw new ArgumentOutOfRangeException("format");
            }
        }

        private OperationParameterInfo GetBodyParameter(OperationInfo operation)
        {
            return operation.Parameters.FirstOrDefault(item => (item.Type.IsClass && !(item.Type == typeof(string))));
        }

        private string GetJsonBody(OperationInfo operation)
        {
            if (operation == null)
                return null;

            OperationParameterInfo bodyParameter = GetBodyParameter(operation);

            if (bodyParameter == null)
                return null;

            object value;

            if (!operation.Values.TryGetValue(bodyParameter.CodeName, out value))
                return null;

            if (value == null)
                return null;

            JsonSerializer serializer = new JsonSerializer();

            serializer.Formatting = Formatting.Indented;
            serializer.NullValueHandling = NullValueHandling.Ignore;
            serializer.Converters.Add(new StringEnumConverter());

            MemoryStream stream = new MemoryStream();

            StreamWriter writer = new StreamWriter(stream);

            serializer.Serialize(writer, value);

            writer.Flush();

            string serializedValue = Encoding.UTF8.GetString(stream.ToArray());

            return serializedValue;
        }

        private string GetXmlBody(OperationInfo operation)
        {
            OperationParameterInfo bodyParameter = GetBodyParameter(operation);

            if (operation == null)
                return null;

            object value;

            if (!operation.Values.TryGetValue(bodyParameter.CodeName, out value))
                return null;

            if (value == null)
                return null;

            CustomXmlSerializer serializer = new CustomXmlSerializer(bodyParameter.Type);

            XmlWriterSettings xmlWriterSettings = new XmlWriterSettings { OmitXmlDeclaration = true };

            MemoryStream stream = new MemoryStream();

            XmlWriter writer = new CustomXmlWriter(stream, xmlWriterSettings);

            serializer.Serialize(writer, value);

            writer.Flush();

            string serializedValue = Encoding.UTF8.GetString(stream.ToArray());

            return serializedValue;
        }

        private string GetFormBody(OperationInfo operation)
        {
            StringBuilder builder = new StringBuilder();

            bool first = true;

            foreach (OperationParameterInfo parameter in operation.Parameters)
            {
                object value;

                operation.Values.TryGetValue(parameter.CodeName, out value);

                if (value == null)
                    continue;

				if (parameter.Type.IsClass && parameter.Type != typeof(string) && !parameter.Type.IsGenericType)
				{
					NameValueCollection properties = GetFormObject(parameter, value);
					foreach (string name in properties)
					{
						if (first)
							first = false;
						else
							builder.Append('&');

						builder.Append(HttpUtility.UrlEncode(name));
						builder.Append('=');
						builder.Append(HttpUtility.UrlEncode(properties[name]));
					}

					continue;
				}

                string stringValue = value.ToString();

                if (string.IsNullOrEmpty(stringValue))
                    continue;

                if (first)
                    first = false;
                else
                    builder.Append('&');

                builder.Append(HttpUtility.UrlEncode(parameter.WebName));
                builder.Append('=');
                builder.Append(HttpUtility.UrlEncode(stringValue));
            }

            return builder.ToString();
        }

		private NameValueCollection GetFormObject(OperationParameterInfo parameter, object value)
		{
			NameValueCollection result = new NameValueCollection();
			foreach (PropertyInfo property in parameter.Type.GetProperties(BindingFlags.Instance | BindingFlags.Public))
			{
				if (!property.CanRead || !property.CanWrite)
					continue;

				object propertyValue = property.GetValue(value);
				if (propertyValue == null)
					continue;

				string stringValue = propertyValue.ToString();

				if (string.IsNullOrEmpty(stringValue))
					continue;

				result.Add(property.Name, stringValue);
			}

			return result;
		}
    }
}
