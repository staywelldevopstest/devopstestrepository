﻿using System.Windows.Forms;

namespace KswApi.Explorer
{
	public partial class ApplicationTokenForm : Form
	{
		public ApplicationTokenForm()
		{
			InitializeComponent();
		}

		public string ApplicationId
		{
			get { return idTextBox.Text; }
			set { idTextBox.Text = value; }
		}

		public string ApplicationSecret
		{
			get { return secretTextBox.Text; }
			set { secretTextBox.Text = value; }
		}
	}
}
