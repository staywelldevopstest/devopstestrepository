﻿using System;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Reflection;
using System.Web;
using KswApi.Interface;
using KswApi.Interface.Objects;
using KswApi.Net;
using KswApi.Reflection;
using KswApi.Reflection.Objects;
using KswApi.RestrictedInterface;
using KswApi.RestrictedInterface.Objects;
using Newtonsoft.Json;

namespace KswApi.Explorer
{
	class AuthorizationExecutor
	{
		public void GetPortalToken(string serviceUri, string clientId, string clientSecret, Action<AccessTokenDetail, object> callback, Action<string, object> error, object state = null)
		{
			ServiceReflector reflector = new ServiceReflector();
			MethodInfo method = GetMethodInfo<IOAuthService>(item => item.CreateAccessToken(null));
			OperationInfo operation = reflector.GetOperation(method);
			ParameterInfo parameter = method.GetParameters().First();
			OAuthTokenRequest request = new OAuthTokenRequest
			{
				grant_type = "client_credentials",
				client_id = clientId,
				client_secret = clientSecret
			};

			operation.Values[parameter.Name] = request;

			OperationCallback callbackObject = new OperationCallback
				                                   {
					                                   Callback = callback,
					                                   Error = error,
					                                   ServiceUri = serviceUri,
					                                   Operation = operation,
													   State = state,
													   ApplicationId = clientId,
													   ApplicationSecret = clientSecret
				                                   };

			callbackObject.Execute();
		}

		public void GetApplicationToken(string serviceUri, string clientId, string clientSecret, Action<AccessTokenDetail, object> callback, Action<string, object> error, AccessTokenDetail accessToken, object state = null)
		{
			ServiceReflector reflector = new ServiceReflector();
			MethodInfo method = GetMethodInfo<IOAuthService>(item => item.CreateAccessToken(null));
			OperationInfo operation = reflector.GetOperation(method);
			ParameterInfo parameter = method.GetParameters().First();
			OAuthTokenRequest request = new OAuthTokenRequest
			{
				client_id = clientId,
				client_secret = clientSecret,
				grant_type = "client_credentials"
			};

			operation.Values[parameter.Name] = request;

			OperationCallback callbackObject = new OperationCallback
			{
				Callback = callback,
				Error = error,
				ServiceUri = serviceUri,
				Operation = operation,
				AccessToken = accessToken,
				State = state
			};

			callbackObject.Execute();
		}

		public void GetUserToken(string serviceUri, string userName, string password, Action<AccessTokenDetail, object> callback, Action<string, object> error, AccessTokenDetail accessToken, object state = null)
		{
			ServiceReflector reflector = new ServiceReflector();
			MethodInfo method = GetMethodInfo<IAuthorizationService>(item => item.CreateAuthorizationGrant(null));
			OperationInfo operation = reflector.GetOperation(method);
			ParameterInfo parameter = method.GetParameters().First();
			AuthorizationGrantRequest request = new AuthorizationGrantRequest
			{
				UserName = userName,
				Password = password,
				ResponseType = "code",
				RedirectUri = null,
				ApplicationId = accessToken != null ? accessToken.ApplicationId : null
			};

			operation.Values[parameter.Name] = request;

			OperationCallback callbackObject = new OperationCallback
			{
				Callback = callback,
				Error = error,
				ServiceUri = serviceUri,
				Operation = operation,
				AccessToken = accessToken,
				State = state
			};

			callbackObject.Execute();
		}

		private static MethodInfo GetMethodInfo<T>(Expression<Action<T>> expression)
		{
			MethodCallExpression methodExpression = expression.Body as MethodCallExpression;

			if (methodExpression == null)
				throw new ArgumentException("Invalid method expression.");

			return methodExpression.Method;
		}

		private class OperationCallback
		{
			private OperationExecutor _executor;

			public Action<AccessTokenDetail, object> Callback { get; set; }
			public Action<string, object> Error { get; set; }
			public object State { get; set; }
			public string RedirectUri { get; set; }
			public string ServiceUri { get; set; }
			public OperationInfo Operation { get; set; }
			public AccessTokenDetail AccessToken { get; set; }
			public string ApplicationId { get; set; }
			public string ApplicationSecret { get; set; }

			public void Execute(bool includeAuthorization = true)
			{
				_executor = new OperationExecutor();
				_executor.Finished += ExecutorOnFinished;
				_executor.Error += ExecutorOnError;
				_executor.Response += ExecutorOnResponse;

				NameValueCollection headers = new NameValueCollection();
				if (includeAuthorization && AccessToken != null)
				{
					headers["Authorization"] = string.Format("Bearer {0}", AccessToken.Token);
				}
				
				_executor.Execute(ServiceUri, Operation, MessageFormat.Json, headers);
			}

			private void ExecutorOnResponse(OperationResponse response)
			{
				if (response.StatusCode != HttpStatusCode.OK)
				{
					SendError(response);
					return;
				}
			
				JsonSerializer serializer = new JsonSerializer();
				
				StringReader reader = new StringReader(response.Body);
				
				object value = serializer.Deserialize(reader, response.Operation.Method.ReturnType);

				if (Operation.Method.ReturnType == typeof(OAuthTokenResponse))
				{
					HandleOAuthToken((OAuthTokenResponse)value);
				}
				else if (Operation.Method.ReturnType == typeof(AuthorizationGrantResponse))
				{
					HandleAuthorizationGrantResponse((AuthorizationGrantResponse)value);
				}
				else
				{
					Error("Unrecognized result.", State);
				}
			}

			private void HandleAuthorizationGrantResponse(AuthorizationGrantResponse response)
			{
				if (response.IsValid)
					HandleOAuthRedirect(response.RedirectUri);
				else
					Error(response.Error, State);
			}

			private string GetAuthorizationGrant(string redirect)
			{
				string[] split = redirect.Split('?');
				if (split.Length != 2)
					return null;
				split = split[1].Split('&');
				string parameter = split.FirstOrDefault(item => item.StartsWith("code", StringComparison.OrdinalIgnoreCase));
				if (string.IsNullOrEmpty(parameter))
					return null;
				split = parameter.Split('=');
				if (split.Length != 2)
					return null;
				return HttpUtility.UrlDecode(split[1]);
			}

			private void HandleOAuthRedirect(string redirectUri)
			{
				string grant = GetAuthorizationGrant(redirectUri);
				if (string.IsNullOrEmpty(grant))
				{
					Error("Invalid grant received.", State);
					return;
				}

				string applicationId = null, applicationSecret = null;

				if (AccessToken != null)
				{
					applicationId = AccessToken.ApplicationId;
					applicationSecret = AccessToken.ApplicationSecret;
				}

				ServiceReflector reflector = new ServiceReflector();
				MethodInfo method = GetMethodInfo<IOAuthService>(item => item.CreateAccessToken(null));
				OperationInfo operation = reflector.GetOperation(method);
				ParameterInfo parameter = method.GetParameters().First();
				OAuthTokenRequest request = new OAuthTokenRequest
				{
					code = grant,
					client_id = applicationId,
					client_secret = applicationSecret,
					grant_type = "authorization_code"
				};

				operation.Values[parameter.Name] = request;


				OperationCallback callbackObject = new OperationCallback
					                                   {
														   Callback = Callback,
														   Error = Error,
														   Operation = operation,
														   AccessToken = AccessToken,
														   ServiceUri = ServiceUri,
														   State = State,
														   ApplicationId = applicationId,
														   ApplicationSecret = applicationSecret
					                                   };

				callbackObject.Execute(false);
			}

			private void HandleOAuthToken(OAuthTokenResponse response)
			{
				DateTime now = DateTime.UtcNow;

				AccessTokenDetail token = new AccessTokenDetail
				{
					CreationTime = now,
					LastAccessedTime = now,
					ExpiresIn = response.expires_in,
					Token = response.access_token,
					RefreshToken = response.refresh_token,
					ApplicationId = ApplicationId,
					ApplicationSecret = ApplicationSecret
				};

				Callback(token, State);
			}

			private void SendError(OperationResponse response)
			{
				JsonSerializer serializer = new JsonSerializer();
				StringReader reader = new StringReader(response.Body);

				if (response.Operation.Method.ReturnType == typeof(OAuthTokenResponse))
				{
					OAuthError oauthResponse = (OAuthError)serializer.Deserialize(reader, typeof(OAuthError));
					Error(string.IsNullOrEmpty(oauthResponse.error_description) ? response.ReasonPhrase : oauthResponse.error_description, State);
				}
				else
				{
					Error error = (Error) serializer.Deserialize(reader, typeof (Error));
					Error(error.Details, State);
				}
			}

			private void ExecutorOnFinished()
			{
				_executor.Finished -= ExecutorOnFinished;
				_executor.Error -= ExecutorOnError;
				_executor.Response -= ExecutorOnResponse;
				_executor = null;
			}

			private void ExecutorOnError(string error)
			{
				Error(error, State);
			}
		}
	}
}
