﻿using System.Collections.Specialized;
using System.Net;
using KswApi.Reflection.Objects;

namespace KswApi.Explorer
{
	class OperationResponse
	{
		public string Raw { get; set; }
		public HttpStatusCode StatusCode { get; set; }
		public string ReasonPhrase { get; set; }
		public NameValueCollection Headers { get; set; }
		public string Body { get; set; }
		public object ReturnValue { get; set; }
		public OperationInfo Operation { get; set; }
	}
}
