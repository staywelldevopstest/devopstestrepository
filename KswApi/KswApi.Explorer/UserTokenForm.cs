﻿using System.Windows.Forms;

namespace KswApi.Explorer
{
	public partial class UserTokenForm : Form
	{
		public UserTokenForm()
		{
			InitializeComponent();
		}

		public string UserId
		{
			get { return idTextBox.Text; }
			set { idTextBox.Text = value; }
		}

		public string Password
		{
			get { return secretTextBox.Text; }
			set { secretTextBox.Text = value; }
		}
	}
}
