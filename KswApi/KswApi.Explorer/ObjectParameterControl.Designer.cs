﻿namespace KswApi.Explorer
{
	partial class ObjectParameterControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label = new System.Windows.Forms.Label();
			this.flowLayout = new System.Windows.Forms.FlowLayoutPanel();
			this.panel1 = new System.Windows.Forms.Panel();
			this.nullCheckBox = new System.Windows.Forms.CheckBox();
			this.panel1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label
			// 
			this.label.AutoSize = true;
			this.label.Dock = System.Windows.Forms.DockStyle.Left;
			this.label.Location = new System.Drawing.Point(0, 0);
			this.label.Name = "label";
			this.label.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
			this.label.Size = new System.Drawing.Size(35, 16);
			this.label.TabIndex = 0;
			this.label.Text = "label1";
			// 
			// flowLayout
			// 
			this.flowLayout.AutoSize = true;
			this.flowLayout.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
			this.flowLayout.Location = new System.Drawing.Point(38, 22);
			this.flowLayout.Name = "flowLayout";
			this.flowLayout.Size = new System.Drawing.Size(442, 0);
			this.flowLayout.TabIndex = 1;
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.nullCheckBox);
			this.panel1.Controls.Add(this.label);
			this.panel1.Location = new System.Drawing.Point(3, 3);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(477, 22);
			this.panel1.TabIndex = 2;
			// 
			// nullCheckBox
			// 
			this.nullCheckBox.AutoSize = true;
			this.nullCheckBox.Dock = System.Windows.Forms.DockStyle.Left;
			this.nullCheckBox.Location = new System.Drawing.Point(35, 0);
			this.nullCheckBox.Name = "nullCheckBox";
			this.nullCheckBox.Size = new System.Drawing.Size(44, 22);
			this.nullCheckBox.TabIndex = 1;
			this.nullCheckBox.Text = "Null";
			this.nullCheckBox.UseVisualStyleBackColor = true;
			this.nullCheckBox.CheckedChanged += new System.EventHandler(this.nullCheckBox_CheckedChanged);
			// 
			// ObjectParameterControl
			// 
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
			this.AutoSize = true;
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.flowLayout);
			this.Name = "ObjectParameterControl";
			this.Size = new System.Drawing.Size(483, 28);
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label;
		private System.Windows.Forms.FlowLayoutPanel flowLayout;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.CheckBox nullCheckBox;

	}
}
