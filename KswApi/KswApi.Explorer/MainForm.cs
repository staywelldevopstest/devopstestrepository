﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Windows.Forms;
using KswApi.Explorer.Properties;
using KswApi.Interface;
using KswApi.Net;
using KswApi.Reflection;
using KswApi.Reflection.Objects;
using KswApi.RestrictedInterface;
using KswApi.ThirdPartyEndpoints.Interfaces;

namespace KswApi.Explorer
{
	public partial class MainForm : Form
	{
		private const string PORTAL_TOKEN_NAME = "Portal Token";
		private const string USER_TOKEN_NAME = "User Token";
		private const string APPLICATION_TOKEN_NAME = "Application Token";

		private const string DEFAULT_PORTAL_TOKEN_ID = "29eeff76-85d0-4f70-94ed-a49072524692";
		private const string DEFAULT_PORTAL_SECRET = "MxSVFBGt0W5EP-EXhAHZstWidOFSyyy.yC.R3opR4dsHboGycTy9aANpShQuybGp";

		private readonly OperationExecutor _executor = new OperationExecutor();

		private readonly Dictionary<string, IParameterControl> _parameterControls = new Dictionary<string, IParameterControl>();
		private OperationInfo _operation = null;

		public MainForm()
		{
			_executor.Error += ExecutorOnError;
			_executor.Request += ExecutorOnRequest;
			_executor.Response += ExecutorOnResponse;
			_executor.Finished += ExecutorOnFinished;

			InitializeComponent();

			formatSelector.SelectedIndex = 0;

			tabControl.SelectedIndex = 1;

			treeView.AfterSelect += TreeViewOnAfterSelect;

			InitializeTreeView();

			InitializeTokenView();

			InitializeSettings();

			UpdateFullUri();
		}

		private void EnterWaitingState()
		{
			treeView.Enabled = false;
			sendButton.Enabled = false;
			clearButton.Enabled = false;
			formatSelector.Enabled = false;
			progressBar.Visible = true;
		}

		private void LeaveWaitingState()
		{
			treeView.Enabled = true;
			sendButton.Enabled = true;
			clearButton.Enabled = true;
			formatSelector.Enabled = true;
			progressBar.Visible = false;
		}

		#region Execution Events

		private void ExecutorOnFinished()
		{
			if (InvokeRequired)
			{
				Invoke(new Action(ExecutorOnFinished));
				return;
			}

			LeaveWaitingState();
		}

		private void InitializeSettings()
		{
			string setting = Settings.Default.ServiceUri;
			if (!string.IsNullOrEmpty(setting))
				serviceUriTextBox.Text = setting;
		}


		private void ExecutorOnResponse(OperationResponse operationResponse)
		{
			if (InvokeRequired)
			{
				Invoke(new Action<OperationResponse>(ExecutorOnResponse), operationResponse);
				return;
			}

			responseRawTextBox.Text = operationResponse.Raw;
		}

		private void ExecutorOnRequest(OperationRequest operationRequest)
		{
			if (InvokeRequired)
			{
				Invoke(new Action<OperationRequest>(ExecutorOnRequest), operationRequest);
				return;
			}

			requestRawTextBox.Text = operationRequest.Raw;
		}

		private void ExecutorOnError(string error)
		{
			if (InvokeRequired)
			{
				Invoke(new Action<string>(ExecutorOnError), error);
				return;
			}

			MessageBox.Show(error);
		}

		#endregion Execution Events


		private void TreeViewOnAfterSelect(object sender, TreeViewEventArgs treeViewEventArgs)
		{
			if (_operation != null)
				UpdateOperation();

			ClearOperation();

			OperationInfo operation = treeViewEventArgs.Node.Tag as OperationInfo;

			_operation = operation;

			UpdateFullUri();

			if (operation == null)
				return;

			PopulateSignatures(operation);

			try
			{
				PopulateOperationView(operation);
			}
			catch (Exception exception)
			{
				MessageBox.Show(exception.Message);
			}

			sendButton.Enabled = true;
		}

		private void InitializeTokenView()
		{

			tokenListView.Items.Add(new ListViewItem
			{
				Text = "<None>",
				Focused = true
			});
		}


		private void ClearOperation()
		{
			_operation = null;
			sendButton.Enabled = false;

			foreach (IParameterControl control in _parameterControls.Values)
				control.Changed -= ControlOnChanged;

			_parameterControls.Clear();
			operationLayoutPanel.Controls.Clear();
			methodSignatureTextBox.Clear();
			uriSignatureTextBox.Clear();
		}

		private void InitializeTreeView()
		{
			treeView.Nodes.Clear();

			treeView.Nodes.Add(CreateNodes<IOAuthService>("OAuth Endpoint"));
			treeView.Nodes.Add(CreateNodes<IAuthorizationService>("Authorization Service"));
			treeView.Nodes.Add(CreateNodes<ApiClient>("Api Service"));
			treeView.Nodes.Add(CreateNodes<ITestService>("Test Service"));
			treeView.Nodes.Add(CreateNodes<ITwilioEndpoint>("Twilio Endpoint"));
		}

		TreeNode CreateNodes<T>(string name)
		{
			ServiceReflector reflector = new ServiceReflector();

			ServiceInfo service = reflector.ReflectService(typeof(T), name);

			return CreateNodes(service, name);
		}

		TreeNode CreateNodes(ServiceInfo service, string name)
		{
			TreeNode node = new TreeNode(name);

			node.Tag = service;

			foreach (ServiceInfo childService in service.Services)
				node.Nodes.Add(CreateNodes(childService, childService.CodeName));

			foreach (OperationInfo operation in service.Operations)
				node.Nodes.Add(new TreeNode
					               {
									   Text = operation.MethodName,
									   Tag = operation
					               });

			return node;
		}

		private void PopulateSignatures(OperationInfo operation)
		{
			methodSignatureTextBox.Text = operation.MethodSignature;

			httpVerbTextBox.Text = operation.Verb;

			uriSignatureTextBox.Text = operation.UriSignature;
		}

		private void PopulateOperationView(OperationInfo operation)
		{
			foreach (OperationParameterInfo parameter in operation.Parameters)
			{
				IParameterControl parameterControl = GetControlForParameter(parameter, null);

				if (parameterControl == null)
					continue;

				parameterControl.Changed += ControlOnChanged;

				object value;
				if (operation.Values.TryGetValue(parameter.CodeName, out value))
					parameterControl.Value = value;

				_parameterControls[parameter.CodeName] = parameterControl;

				Control control = (Control)parameterControl;

				operationLayoutPanel.Controls.Add(control);
			}
		}

		private void ControlOnChanged(IParameterControl parameterControl)
		{
			if (_operation == null)
				return;

			_operation.Values[parameterControl.OperationParameter.CodeName] = parameterControl.Value;

			UpdateFullUri();
		}

		private IParameterControl GetControlForParameter(OperationParameterInfo parameter, object value)
		{
			IParameterControl control = ControlFactory.CreateControlForType(parameter.Type);

			if (control == null)
				return null;
			control.OperationParameter = parameter;
			control.Value = value;

			return control;
		}

		private void UpdateOperation()
		{
			if (_operation == null)
				return;

			foreach (KeyValuePair<string, IParameterControl> parameter in _parameterControls)
			{
				_operation.Values[parameter.Key] = parameter.Value.Value;
			}
		}

		private void sendButton_Click(object sender, EventArgs e)
		{
			if (_operation == null)
				return;

			UpdateOperation();

			EnterWaitingState();

			MessageFormat format = GetMessageFormat();

			_executor.Execute(serviceUriTextBox.Text, _operation, format, GetHeaders(format), format == MessageFormat.Jsonp ? callbackFunctionTextBox.Text : null);
		}

		private MessageFormat GetMessageFormat()
		{
			switch (formatSelector.SelectedItem.ToString())
			{
				case "XML":
					return MessageFormat.Xml;
				case "JSON":
					return MessageFormat.Json;
				case "Form":
					return MessageFormat.Form;
				case "JSONP":
					return MessageFormat.Jsonp;
				default:
					return MessageFormat.Default;
			}
		}

		private void UpdateFullUri()
		{
			string path = _executor.GetCompletUri(serviceUriTextBox.Text, _operation, GetMessageFormat(), callbackFunctionTextBox.Text);

			fullUriTextBox.Text = path;
		}

		private void serviceUriTextBox_TextChanged(object sender, EventArgs e)
		{
			UpdateFullUri();
			Settings.Default.ServiceUri = serviceUriTextBox.Text;
			Settings.Default.Save();
		}

		private void formatSelector_SelectedIndexChanged(object sender, EventArgs e)
		{
			UpdateFullUri();
		}

		private void portalTokenButton_Click(object sender, EventArgs e)
		{
			ApplicationTokenForm form = new ApplicationTokenForm();

			form.ApplicationId = string.IsNullOrEmpty(Settings.Default.PortalId)
									 ? DEFAULT_PORTAL_TOKEN_ID
									 : Settings.Default.PortalId;

			form.ApplicationSecret = string.IsNullOrEmpty(Settings.Default.PortalSecret)
									 ? DEFAULT_PORTAL_SECRET
									 : Settings.Default.PortalSecret;

			if (form.ShowDialog() == DialogResult.OK)
			{
				Settings.Default.PortalId = form.ApplicationId;
				Settings.Default.PortalSecret = form.ApplicationSecret;
				Settings.Default.Save();

				EnterWaitingState();

				AuthorizationExecutor executor = new AuthorizationExecutor();

				executor.GetPortalToken(serviceUriTextBox.Text,
					form.ApplicationId,
					form.ApplicationSecret,
					AuthorizationCallback,
					ErrorCallback,
					PORTAL_TOKEN_NAME);
			}
		}

		private NameValueCollection GetHeaders(MessageFormat format)
		{
			string token = tokenTextBox.Text;

			if (string.IsNullOrEmpty(token))
				return null;

			NameValueCollection headers = new NameValueCollection();

			headers.Add("Authorization", string.Format("Bearer {0}", token));

			if (format == MessageFormat.Jsonp)
				headers.Add("Referer", refererTextBox.Text);

			return headers;
		}

		private AccessTokenDetail GetAccessToken()
		{
			if (tokenListView.SelectedItems.Count == 0)
				return null;

			return (AccessTokenDetail)tokenListView.SelectedItems[0].Tag;
		}

		private void applicationTokenButton_Click(object sender, EventArgs e)
		{
			ApplicationTokenForm form = new ApplicationTokenForm();

			form.ApplicationId = Settings.Default.ApplicationId;
			form.ApplicationSecret = Settings.Default.ApplicationSecret;

			if (form.ShowDialog() == DialogResult.OK)
			{
				Settings.Default.ApplicationId = form.ApplicationId;
				Settings.Default.ApplicationSecret = form.ApplicationSecret;
				Settings.Default.Save();

				EnterWaitingState();

				AuthorizationExecutor executor = new AuthorizationExecutor();
				executor.GetApplicationToken(serviceUriTextBox.Text,
					form.ApplicationId,
					form.ApplicationSecret,
					AuthorizationCallback,
					ErrorCallback,
					null,
					APPLICATION_TOKEN_NAME);
			}
		}

		private void userTokenButton_Click(object sender, EventArgs e)
		{
			UserTokenForm form = new UserTokenForm();

			if (form.ShowDialog() == DialogResult.OK)
			{
				EnterWaitingState();

				AuthorizationExecutor executor = new AuthorizationExecutor();
				executor.GetUserToken(serviceUriTextBox.Text,
					form.UserId,
					form.Password,
					AuthorizationCallback,
					ErrorCallback,
					GetAccessToken(),
					USER_TOKEN_NAME);
			}
		}

		private void refreshTokenButton_Click(object sender, EventArgs e)
		{

		}

		private void deleteButton_Click(object sender, EventArgs e)
		{
			if (tokenListView.SelectedItems.Count == 0)
				return;

			if (tokenListView.SelectedItems[0].Tag == null)
				return;

			tokenListView.Items.Remove(tokenListView.SelectedItems[0]);
		}

		private void ErrorCallback(string error, object state)
		{
			if (InvokeRequired)
			{
				Invoke(new Action<string, object>(ErrorCallback), error, state);
				return;
			}

			MessageBox.Show(error);
			LeaveWaitingState();
		}

		private void AuthorizationCallback(AccessTokenDetail accessToken, object state)
		{
			if (InvokeRequired)
			{
				Invoke(new Action<AccessTokenDetail, object>(AuthorizationCallback), accessToken, state);
				return;
			}

			tokenListView.Items.Add(new ListViewItem
			{
				Text = (string)state,
				Tag = accessToken
			});
			LeaveWaitingState();
		}

		private void tokenListView_SelectedIndexChanged(object sender, EventArgs e)
		{
			tokenTextBox.Clear();
			expiresTextBox.Clear();

			if (tokenListView.SelectedItems.Count == 0)
				return;

			ListViewItem item = tokenListView.SelectedItems[0];

			AccessTokenDetail token = item.Tag as AccessTokenDetail;

			if (token == null)
			{
				userTokenButton.Enabled = false;
				refreshTokenButton.Enabled = false;
				deleteButton.Enabled = false;
				return;
			}

			refreshTokenButton.Enabled = !string.IsNullOrEmpty(token.RefreshToken);

			tokenTextBox.Text = token.Token;
			expiresTextBox.Text = token.ExpiresIn.ToString();
			refreshTokenTextBox.Text = token.RefreshToken;

			if (item.Text == PORTAL_TOKEN_NAME)
			{
				userTokenButton.Enabled = true;
			}
			else
			{
				userTokenButton.Enabled = false;
			}

			deleteButton.Enabled = true;
		}

		private void callbackFunctionTextBox_TextChanged(object sender, EventArgs e)
		{
			UpdateFullUri();
		}
	}
}
