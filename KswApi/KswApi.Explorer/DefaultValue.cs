﻿using System;

namespace KswApi.Explorer
{
	public static class DefaultValue
	{
		public static object Get(Type type)
		{
			if (type == typeof(string) || type.IsClass || (type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>)))
				return null;

			return Activator.CreateInstance(type);
		}
	}
}
