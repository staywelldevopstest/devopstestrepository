﻿namespace KswApi.Explorer
{
	partial class EnumParameterControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label = new System.Windows.Forms.Label();
			this.dropDown = new System.Windows.Forms.ComboBox();
			this.SuspendLayout();
			// 
			// label
			// 
			this.label.AutoSize = true;
			this.label.Dock = System.Windows.Forms.DockStyle.Left;
			this.label.Location = new System.Drawing.Point(0, 0);
			this.label.Name = "label";
			this.label.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
			this.label.Size = new System.Drawing.Size(35, 16);
			this.label.TabIndex = 1;
			this.label.Text = "label1";
			// 
			// dropDown
			// 
			this.dropDown.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dropDown.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.dropDown.FormattingEnabled = true;
			this.dropDown.Location = new System.Drawing.Point(35, 0);
			this.dropDown.Name = "dropDown";
			this.dropDown.Size = new System.Drawing.Size(366, 21);
			this.dropDown.TabIndex = 2;
			this.dropDown.SelectedIndexChanged += new System.EventHandler(this.dropDown_SelectedIndexChanged);
			// 
			// EnumParameterControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.dropDown);
			this.Controls.Add(this.label);
			this.Name = "EnumParameterControl";
			this.Size = new System.Drawing.Size(401, 22);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label;
		private System.Windows.Forms.ComboBox dropDown;

	}
}
