﻿using KswApi.Framework;
using KswApi.Interface;
using KswApi.Interface.Objects;
using KswApi.Objects;
using KswApi.RestrictedInterface;
using KswApi.RestrictedInterface.Objects;

namespace KswApi
{
	public class AuthorizationClient : ServiceClient
	{
		private readonly string _authServerUri;
		private readonly string _clientId;
		private readonly string _clientSecret;

		public AuthorizationClient(string authServerUri, string serviceUri, string clientId, string clientSecret)
			: base(serviceUri, clientId, clientSecret, new InProcSingleTokenStore())
		{
			_authServerUri = authServerUri;
			_clientId = clientId;
			_clientSecret = clientSecret;
		}

		public IAuthorizationService Authorization { get { return GetService<IAuthorizationService>(); } }

		#region Protected Methods

		//protected override AccessToken GetAccessToken(AccessToken accessToken)
		//{
		//	OAuthTokenResponse response = GetService<IOAuthService>().CreateAccessToken(new OAuthTokenRequest
		//		{
		//			grant_type = "client_credentials",
		//			client_id = _clientId,
		//			client_secret = _clientSecret
		//		});

		//	return new AccessToken
		//			   {
		//				   AuthorizationServer = _authServerUri,
		//				   ExpiresIn = response.expires_in,
		//				   Token = response.access_token
		//			   };
		//}

		#endregion Protected Methods

	}
}
