﻿using System;
using KswApi.Interfaces;
using KswApi.Objects;

namespace KswApi.Framework
{
	class InProcSingleTokenStore : ITokenStore
	{
		private static AccessToken _token;

		public AccessToken GetToken()
		{
			return _token;
		}

		public void SetToken(AccessToken value)
		{
			_token = value;
		}

		public void RemoveToken()
		{
			_token = null;
		}

		public bool KeepAlive()
		{
			return true;
		}
	}
}
