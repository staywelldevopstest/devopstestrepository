﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KswApi.HttpReceiver
{
	// ReSharper disable InconsistentNaming
	public partial class Form1 : Form
	{
		private readonly Host _host = new Host();
		private bool _running;
		private int _current = 0;

		public Form1()
		{
			InitializeComponent();
			_host.Error += Error;
			_host.Message += Message;
		}

		public void Error(string error)
		{
			if (InvokeRequired)
			{
				Invoke(new Action<string>(Error), error);
				return;
			}

			MessageBox.Show(error);
		}

		public void Message(string method, string message)
		{
			if (InvokeRequired)
			{
				Invoke(new Action<string, string>(Message), method, message);
				return;
			}

			ListViewItem item = new ListViewItem();
			
			_current++;

			item.Text = string.Format("{0}: {1}", _current, method);
			item.Tag = message;

			list.Items.Add(item);
		}

		private void list_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (list.SelectedItems.Count == 0)
			{
				details.Clear();
				return;
			}
				

			details.Text = list.SelectedItems[0].Tag as string;
		}

		private void startButton_Click(object sender, EventArgs e)
		{
			try
			{
				if (_running)
					StopService();
				else
					StartService();
			}
			catch (Exception ex)
			{
				MessageBox.Show(ex.Message);
			}
		}

		private void StartService()
		{
			Uri uri;
			if (!Uri.TryCreate(addressBox.Text, UriKind.Absolute, out uri))
			{
				MessageBox.Show("The address is not valid.");
				return;
			}

			_host.Start(uri);

			_running = true;
			startButton.Text = "Stop";
			addressBox.ReadOnly = true;
		}

		private void StopService()
		{
			_host.Stop();

			_running = false;
			startButton.Text = "Start";
			addressBox.ReadOnly = false;
		}
	}
}
