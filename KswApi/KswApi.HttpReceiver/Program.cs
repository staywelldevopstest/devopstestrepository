﻿using System;
using System.Windows.Forms;

namespace KswApi.HttpReceiver
{
	static class Program
	{
		public static Form1 Form { get; private set; }

		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Form = new Form1();
			Application.Run(Form);
		}
	}
}
