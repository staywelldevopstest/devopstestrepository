﻿using System;
using System.IO;
using System.Net;
using System.Threading;

namespace KswApi.HttpReceiver
{
	public class Host
	{
		private readonly ManualResetEvent _stop = new ManualResetEvent(false);

		public Action<string> Error;
		public Action<string, string> Message;
		
		private readonly HttpListener _listener = new HttpListener();

		public void Start(Uri uri)
		{
			Thread thread = new Thread(HostThread);
			thread.Start(uri);
		}

		private void HostThread(object obj)
		{
			Uri uri = obj as Uri;
			if (uri == null)
				return;

			try
			{
				RunHost(uri);
			}
			catch (Exception ex)
			{
				if (Error != null)
					Error(ex.Message);
			}
		}

		private void RunHost(Uri uri)
		{
			_listener.Prefixes.Clear();

			string uriString = uri.ToString();
			
			_listener.Prefixes.Add(uriString);

			_listener.Start();

			while (true)
			{
				HttpListenerContext context = _listener.GetContext();

				TextReader reader = new StreamReader(context.Request.InputStream);

				string message = reader.ReadToEnd();

				if (Message != null)
					Message(context.Request.HttpMethod.ToString(), message);

				context.Response.StatusCode = 200;
				context.Response.Close();
			}
		}

		public void Stop()
		{
			_listener.Stop();
		}
	}
}
