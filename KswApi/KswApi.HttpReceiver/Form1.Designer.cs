﻿namespace KswApi.HttpReceiver
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.addressBox = new System.Windows.Forms.TextBox();
			this.startButton = new System.Windows.Forms.Button();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.list = new System.Windows.Forms.ListView();
			this.details = new System.Windows.Forms.TextBox();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 19);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(48, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Address:";
			// 
			// addressBox
			// 
			this.addressBox.Location = new System.Drawing.Point(66, 16);
			this.addressBox.Name = "addressBox";
			this.addressBox.Size = new System.Drawing.Size(316, 20);
			this.addressBox.TabIndex = 1;
			this.addressBox.Text = "http://10.10.10.135:8050";
			// 
			// startButton
			// 
			this.startButton.Location = new System.Drawing.Point(66, 42);
			this.startButton.Name = "startButton";
			this.startButton.Size = new System.Drawing.Size(75, 23);
			this.startButton.TabIndex = 2;
			this.startButton.Text = "Start";
			this.startButton.UseVisualStyleBackColor = true;
			this.startButton.Click += new System.EventHandler(this.startButton_Click);
			// 
			// splitContainer1
			// 
			this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.splitContainer1.Location = new System.Drawing.Point(0, 71);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.list);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.details);
			this.splitContainer1.Size = new System.Drawing.Size(662, 603);
			this.splitContainer1.SplitterDistance = 220;
			this.splitContainer1.TabIndex = 3;
			// 
			// list
			// 
			this.list.Dock = System.Windows.Forms.DockStyle.Fill;
			this.list.FullRowSelect = true;
			this.list.HideSelection = false;
			this.list.Location = new System.Drawing.Point(0, 0);
			this.list.MultiSelect = false;
			this.list.Name = "list";
			this.list.Size = new System.Drawing.Size(220, 603);
			this.list.TabIndex = 0;
			this.list.UseCompatibleStateImageBehavior = false;
			this.list.View = System.Windows.Forms.View.List;
			this.list.SelectedIndexChanged += new System.EventHandler(this.list_SelectedIndexChanged);
			// 
			// details
			// 
			this.details.BackColor = System.Drawing.SystemColors.Window;
			this.details.Dock = System.Windows.Forms.DockStyle.Fill;
			this.details.Location = new System.Drawing.Point(0, 0);
			this.details.Multiline = true;
			this.details.Name = "details";
			this.details.ReadOnly = true;
			this.details.ScrollBars = System.Windows.Forms.ScrollBars.Both;
			this.details.Size = new System.Drawing.Size(438, 603);
			this.details.TabIndex = 0;
			this.details.WordWrap = false;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(661, 674);
			this.Controls.Add(this.splitContainer1);
			this.Controls.Add(this.startButton);
			this.Controls.Add(this.addressBox);
			this.Controls.Add(this.label1);
			this.Name = "Form1";
			this.Text = "KSW API HTTP Receiver";
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			this.splitContainer1.Panel2.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox addressBox;
		private System.Windows.Forms.Button startButton;
		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.TextBox details;
		private System.Windows.Forms.ListView list;
	}
}

